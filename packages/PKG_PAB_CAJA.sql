CREATE OR REPLACE /* Formatted on 18-08-2020 17:45:24 (QP5 v5.115.810.9015) */
PACKAGE DBO_PAB.PKG_PAB_CAJA
IS
   --
   v_NOM_PCK     VARCHAR2 (30) := 'PKG_PAB_CAJA';
   ERR_CODE      NUMBER := 0;
   ERR_MSG       VARCHAR2 (300);
   v_DES         VARCHAR2 (300);
   v_DES_BIT     VARCHAR2 (100);
   p_s_mensaje   VARCHAR2 (400);
   v_cero        NUMBER := 0;

   v_dia_ini     DATE := TRUNC (SYSDATE) + INTERVAL '1' MINUTE;
   v_dia_fin DATE
         := TRUNC (SYSDATE) + INTERVAL '23' HOUR + INTERVAL '59' MINUTE ;

   /***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_INS_PYN_MNT_CJA
   -- Objetivo: Procedimiento que inserta movimientos de multinomeda para flujo de egreso e ingreso
   -- desde WS TIBCO al cargar n�mina
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_DETLL_MVNTO_CAJA
   -- Fecha: 22/08/2018
   -- Autor: JBARBOZA
   -- Input:
   -- p_NUM_FOL_OPE        -> Folio Operaci�n de ingreso
   -- p_FEC_INS_OPE        -> Fecha de inserci�n
   -- p_TPO_OPE            -> Tipo Operaci�n (Cod de concepto del movimiento)
   -- p_COD_BCO_DTN        -> BIC Banco destino
   -- p_COD_BCO_ORG        -> BIC Banco Origen
   -- p_NUM_CTA_CTE        -> N�mero Cuenta Corriente
   -- p_COD_DVI            -> Moneda
   -- p_FLG_ING_EGR        -> Flag indicador de flujo de ingreso o egreso
   -- p_IMP_OPE            -> Monto
   -- p_FEC_VTA            -> Fecha Valuta
   -- p_COD_SIS_ENT        -> Canal
   -- p_NUM_DOC_EPS        -> Rut Empresa
   -- p_COD_TPO_DOC_EPS    -> Digito Rut Empresa
   -- p_DSC_SIS_ENT        -> Descripci�n de sistema
   -- p_COD_USR            -> Usuario Tibco
   -- p_COD_USR_SIS        -> Usuario Sistema
   -- Output:
   -- p_ERROR              DETERMINA SI HUBO UN ERROR EN LA EJECUCION : 1 SI OCURRIO UN  ERROR DE LO CONTRARIO SERA 0
   -- p_NUM_FOL_OPE        VARIABLE QUE DEVUELVE EL NUMERO DE FOLIO DE LA OPERACI�N
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: 22/08/2018  : SE REALIZO UNA MODIFICACI�N ,EL CUAL SE DESPLAZO TODAS LA ACCIONES
                                   RELACIONADA A LA ACTUALIZACI�N AL PROCEDMIENTO SP_PAB_ACT_SALDO_MN_MX.
                                   (POR LO TANTO SE CAMBIO LA LO�GICA DEL MANEJO DE LOS DATOS )
   --***********************************************************************************************/
   PROCEDURE SP_PAB_INS_PYN_MNT_CJA (p_num_fol_nmn       IN     NUMBER,
                                     p_flg_ing_egr       IN     NUMBER,
                                     p_cod_bco           IN     CHAR,
                                     p_num_cta_cte       IN     VARCHAR2,
                                     p_tpo_ope           IN     CHAR,
                                     p_cod_dvi           IN     CHAR,
                                     p_imp_ope           IN     NUMBER,
                                     p_fec_vta           IN     DATE,
                                     p_gls_sis_org       IN     VARCHAR2,
                                     p_gls_mvt           IN     VARCHAR2,
                                     p_cod_sis_ent       IN     CHAR,
                                     p_num_doc_eps       IN     CHAR,
                                     p_cod_tpo_doc_eps   IN     CHAR,
                                     p_cod_usr           IN     VARCHAR,
                                     p_num_fol_ope          OUT NUMBER,
                                     p_error                OUT NUMBER);

   -- ***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_INS_CRTLA_CARGO_ABONO
   -- Objetivo: Procedimiento almacenado que inserta Movimientos de cargo o abono.(MT 900 y 910)
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_CBCRA_CRTLA
   -- Fecha: 12/10/17
   -- Autor: Santander
   -- Input:
   -- p_FEC_ING_CTL    -> Fecha de ingreso cartola
   -- p_HOR_ING_CTL    -> Hora de ingreso
   -- p_NUM_FOL_CTL    -> Numero de folio cartola
   -- p_NUM_REF_SWF_CTL-> Referencia cartola
   -- p_COD_MT_SWF     -> C�digo mt de cartola
   -- p_COD_DVI        -> Moneda de movimiento
   -- p_NUM_CTA_CTE    -> Numero de cuenta corriente
   -- p_COD_BCO_ORG    -> Banco origen cartola
   -- p_FLG_DEB_CRE_INI-> Flag debito o cr�dito inicial
   -- p_IMP_ING_CTL    -> Monto ingreso cartola
   -- p_IMP_EGR_CTL    -> Monto egreso cartola
   -- p_IMP_INI_CTL    -> Monto incio cartola
   -- p_IMP_FNL_CTL    -> Monto final cartola
   -- p_FLG_DEB_CRE_FNL-> Flag debito o cr�dito final
   -- p_FLG_ING_EGR    -> Flag de ingreso o egreso
   -- Output:
   -- v_NUM_FOL_OPE    ->Folio de MT ingresado
   -- v_FEC_ING_OPE    ->Fecha de mt ingresado
   -- p_ERROR-> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ?ltimos cambios>
   -- ***********************************************************************************************
   PROCEDURE Sp_PAB_INS_CRTLA_CARGO_ABONO (p_COD_MT_SWF    IN     NUMBER,
                                           p_COD_DVI       IN     CHAR,
                                           p_COD_BCO_DTN   IN     VARCHAR2,
                                           p_COD_BCO_ORG   IN     VARCHAR2,
                                           p_COD_BCO_ITD   IN     CHAR,
                                           p_NUM_SEQ_IFD   IN     NUMBER,
                                           p_FEC_ING_IFD   IN     DATE,
                                           p_HOR_ING_IFD   IN     NUMBER,
                                           p_NUM_REF_SWF   IN     CHAR,
                                           p_NUM_CTA_CTE   IN     VARCHAR2,
                                           p_FEC_VTA       IN     DATE,
                                           p_IMP_OPE       IN     NUMBER,
                                           p_GLS_TRN       IN     VARCHAR2,
                                           p_NUM_REF_EXT   IN     CHAR,
                                           --------------------------------
                                           p_NUM_DOC_BFC   IN     VARCHAR2,
                                           p_NOM_BFC       IN     VARCHAR2,
                                           ---------------------------------
                                           p_NUM_FOL_OPE      OUT NUMBER,
                                           p_FEC_ING_OPE      OUT TIMESTAMP,
                                           p_ERROR            OUT NUMBER);

   --************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_INS_CBCRA_CRTLA_MT
   -- Objetivo: Procedimiento que inserta cartola - Movimiento de entrada y salida de cuentas corrientes de bancos corresponsales
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_CBCRA_CRTLA
   -- Fecha: 03/11/17
   -- Autor: Santander
   -- Input:
   -- p_COD_BCO_ORG
   -- p_NUM_CTA_CTE
   -- p_NUM_FOL_CTL
   -- p_COD_DVI
   -- p_NUM_REF_SWF_CTL
   -- p_FEC_ING_CTL
   -- p_HOR_ING_CTL
   -- p_COD_MT_SWF
   -- p_IM-- p_INI_CTL
   -- p_IM-- p_ING_CTL
   -- p_IM-- p_EGR_CTL
   -- p_IM-- p_FNL_CTL
   -- p_FLG_DEB_CRE_INI
   -- p_FLG_DEB_CRE_FNL
   -- p_FLG_INI_FNL
   -- Output:
   -- p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE Sp_PAB_INS_CBCRA_CRTLA_MT (p_COD_BCO_ORG       IN     VARCHAR2,
                                        p_NUM_CTA_CTE       IN     VARCHAR2,
                                        p_NUM_FOL_CTL       IN     NUMBER,
                                        p_COD_DVI           IN     CHAR,
                                        p_NUM_REF_SWF_CTL   IN     VARCHAR2,
                                        p_FEC_ING_CTL       IN     DATE,
                                        p_HOR_ING_CTL       IN     NUMBER,
                                        p_COD_MT_SWF        IN     VARCHAR2,
                                        p_IMP_INI_CTL       IN     NUMBER,
                                        p_IMP_ING_CTL       IN     NUMBER,
                                        p_IMP_EGR_CTL       IN     NUMBER,
                                        p_IMP_FNL_CTL       IN     NUMBER,
                                        p_FLG_DEB_CRE_INI   IN     CHAR,
                                        p_FLG_DEB_CRE_FNL   IN     CHAR,
                                        p_FLG_INI_FNL       IN     NUMBER,
                                        p_ERROR                OUT NUMBER);



   --************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_INS_DETLL_CRTLA_MT
   -- Objetivo: Procedimiento que inserta cartola - Movimiento de entrada y salida de cuentas corrientes de bancos corresponsales
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_DETLL_CRTLA
   -- Fecha: 03/11/17
   -- Autor: Santander
   -- Input:
   -- p_NUM_FOL_CTL
   -- p_FEC_TRN_OPE
   -- p_NUM_REF_SWF_OPE
   -- p_COD_TRN_OPE
   -- p_NUM_REF_ITN_CTA
   -- p_FLG_DEB_CRE
   -- p_IM-- p_TRN
   -- p_GLS_TRN
   -- p_NUM_CTA_CTE
   -- p_FEC_ING_CTL
   -- Output:
   -- p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE Sp_PAB_INS_DETLL_CRTLA_MT (p_NUM_FOL_CTL       IN     NUMBER,
                                        p_FEC_TRN_OPE       IN     TIMESTAMP,
                                        p_NUM_REF_SWF_OPE   IN     VARCHAR2,
                                        p_COD_TRN_OPE       IN     VARCHAR2,
                                        p_NUM_REF_ITN_CTA   IN     VARCHAR2,
                                        p_FLG_DEB_CRE       IN     VARCHAR2,
                                        p_IMP_TRN           IN     NUMBER,
                                        p_GLS_TRN           IN     VARCHAR2,
                                        p_NUM_CTA_CTE       IN     VARCHAR2,
                                        p_COD_BCO_ORG       IN     VARCHAR2,
                                        p_COD_DVI           IN     VARCHAR2,
                                        p_FEC_ING_CTL       IN     DATE,
                                        p_ERROR                OUT NUMBER);


   --**********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_CNA_CJA_FIL
   -- Objetivo: Retorna totales de saldos intrad�a, 24 y 48
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_CAJA_FLIAL
   -- Fecha: 04/10/17
   -- Autor: Santander
   -- Input:
   -- N/A
   -- Output:

   -- p_ERROR  ->  Indicada si el procedimiento
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de utimos cambios>
   --***********************************************************************************************
   PROCEDURE SP_PAB_CNA_SLD_CJA (p_FEC_VAL       IN     DATE,
                                 p_NUM_CTA_CTE   IN     VARCHAR2,
                                 p_COD_DIV       IN     CHAR,
                                 p_COD_BCO       IN     VARCHAR2,
                                 p_SDO_ING          OUT NUMBER,
                                 p_SDO_EGRE         OUT NUMBER,
                                 p_SDO_TOT          OUT NUMBER,
                                 p_ERROR            OUT NUMBER);

   --************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_ACT_CRTLA_MX
   -- Objetivo: Procedimiento que actualiza saldo de cartola MT940 MT950.
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_CBCRA_CRTLA, PABS_DT_DETLL_CRTLA, PABS_DT_DETLL_SALDO_CAJA_MN_MX
   -- Fecha: 17/10/17
   -- Autor: Santander
   -- Input:
   -- p_NUM_CTA_CTE -> N�mero de cuenta corriente
   -- p_COD_BCO_ORG -> C�digo banco
   -- p_FEC_ING_CTL -> Fecha de ingreso cartola
   -- Output:
   -- p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE Sp_PAB_ACT_CRTLA_MX (p_NUM_CTA_CTE   IN     VARCHAR2,
                                  p_COD_BCO_ORG   IN     VARCHAR2,
                                  p_FEC_ING_CTL   IN     DATE,
                                  p_COD_DVI       IN     VARCHAR2,
                                  p_ERROR            OUT NUMBER);

   --************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_INS_SLD_CRPSL
   -- Objetivo: Procedimiento que INSERTA registro saldo corresponsales en tabla saldo
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas:
   -- Fecha: 17/10/17
   -- Autor: Santander
   -- Input:
   -- p_FEC_ING_CTL  FECHA INGRESO CARTOLA
   -- p_COD_BCO_ORG  BANCO CORRESPONSAL
   -- p_NUM_CTA_CTE  NUMERO DE CUENTA CORRESPONSAL
   -- p_COD_DVI      MONEDA ASOCIADA A CUENTA CORRESPONSAL
   -- Output:
   -- p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE Sp_PAB_INS_SLD_CRPSL (p_COD_BCO_ORG   IN     CHAR,
                                   p_NUM_CTA_CTE   IN     VARCHAR2,
                                   p_COD_DVI       IN     CHAR,
                                   p_ERROR            OUT NUMBER);

   --************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_ELI_SLD_CRPSL
   -- Objetivo: Procedimiento que ELIMINA registro saldo corresponsales en tabla saldo
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas:
   -- Fecha: 17/10/17
   -- Autor: Santander
   -- Input:
   -- p_FEC_ING_CTL  FECHA INGRESO CARTOLA
   -- p_COD_BCO_ORG  BANCO CORRESPONSAL
   -- p_NUM_CTA_CTE  NUMERO DE CUENTA CORRESPONSAL
   -- p_COD_DVI      MONEDA ASOCIADA A CUENTA CORRESPONSAL
   -- Output:
   -- p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE Sp_PAB_ELI_SLD_CRPSL (p_COD_BCO_ORG   IN     CHAR,
                                   p_NUM_CTA_CTE   IN     VARCHAR2,
                                   p_COD_DVI       IN     CHAR,
                                   p_ERROR            OUT NUMBER);

   --************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_RNO_FEC_PYN
   -- Objetivo: Procedimiento que retorna las fechas de proyeccion para MX
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas:
   -- Fecha: 19/03/18
   -- Autor: SANTANDER
   -- Input:
   -- p_FEC_ING_CTL  FECHA INGRESO CARTOLA
   -- p_COD_BCO_ORG  BANCO CORRESPONSAL
   -- p_NUM_CTA_CTE  NUMERO DE CUENTA CORRESPONSAL
   -- p_COD_DVI      MONEDA ASOCIADA A CUENTA CORRESPONSAL
   -- Output:
   -- p_FEC_DIA      RETORNO DE FECHA ACTUAL
   -- p_FEC_24H      RETORNO DE FECHA 24h
   -- p_FEC_48H      RETORNO DE FECHA 48h
   -- p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE Sp_PAB_RNO_FEC_PYN (p_COD_BCO_ORG   IN     CHAR,
                                 p_NUM_CTA_CTE   IN     VARCHAR2,
                                 p_COD_DVI       IN     CHAR,
                                 p_FEC_DIA          OUT DATE,
                                 p_FEC_24H          OUT DATE,
                                 p_FEC_48H          OUT DATE,
                                 p_ERROR            OUT NUMBER);

   --************************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_ACT_SALDO_MN_MX
   -- Objetivo: PROCEDIMIENTO QUE REALIZA LAS ACTUALIZACIONES DE LOS SALDOS PARA LA CARGA DE PROYECCIONES  DE INGRESO Y  EL EGRESO.
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas:PABS_DT_DETLL_MVNTO_CAJA ,PABS_DT_DETLL_SALDO_CAJA_MN_MX
   -- Fecha: 22/08/2018
   -- Autor: JBARBOZA
   -- Input:  N/A.
   -- Output:
   -- p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE sp_pab_act_saldo_mn_mx (p_error OUT NUMBER);

   /********************************************************************************************************
   FUNCION/PROCEDIMIENTO: SP_BUS_PRYCN_MNMX.
   OBJETIVO             : MUESTRA EL DETALLE DE LA PROYECCI�N CARGADA PARA MONEDA NACIONAL Y MONEDA
                          EXTRANJERA SEG�N �REA.
   SISTEMA              : DBO_PAB.
   BASE DE DATOS        : DBO_PAB.
   TABLAS USADAS        : PABS_DT_DETLL_MVNTO_CAJA.
                          PABS_DT_SISTM_ENTRD_SALID.
                          TCDTBAI.
   FECHA                : 13/02/19.
   AUTOR                : VAR.
   INPUT                : P_AREA    := �REA.
                          P_FLG_MRD := MONEDA.
                            0 := MN - 1 := MX.
   OUTPUT               : P_CURSOR  := DATA.
                          P_ERROR   := INDICADOR DE ERRORES.
   OBSERVACIONES
   FECHA       USUARIO    DETALLE
   13/02/19    VAR        CREACI�N.
   ********************************************************************************************************/
   PROCEDURE SP_BUS_PRYCN_MNMX (P_AREA      IN     CHAR,
                                P_FLG_MRD   IN     CHAR,
                                P_CURSOR       OUT SYS_REFCURSOR,
                                P_ERROR        OUT NUMBER);
END PKG_PAB_CAJA;
/


CREATE OR REPLACE /* Formatted on 18-08-2020 17:45:26 (QP5 v5.115.810.9015) */
PACKAGE BODY DBO_PAB.PKG_PAB_CAJA
IS
   /************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_INS_PYN_MNT_CJA
   -- Objetivo: Procedimiento que inserta movimientos de multinomeda para flujo de egreso e ingreso
   -- desde WS TIBCO al cargar nomina
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_DETLL_MVNTO_CAJA
   -- Fecha: 22/08/2018
   -- Autor: JBARBOZA
   -- Input:
   -- p_NUM_FOL_OPE        -> Folio Operacion de ingreso
   -- p_FEC_INS_OPE        -> Fecha de insercion
   -- p_TPO_OPE            -> Tipo Operacion (Cod de concepto del movimiento)
   -- p_COD_BCO_DTN        -> BIC Banco destino
   -- p_COD_BCO_ORG        -> BIC Banco Origen
   -- p_NUM_CTA_CTE        -> Numero Cuenta Corriente
   -- p_COD_DVI            -> Moneda
   -- p_FLG_ING_EGR        -> Flag indicador de flujo de ingreso o egreso
   -- p_IMP_OPE            -> Monto
   -- p_FEC_VTA            -> Fecha Valuta'NUM_CTA_CTE'
   -- p_COD_SIS_ENT        -> Canal
   -- p_NUM_DOC_EPS        -> Rut Empresa
   -- p_COD_TPO_DOC_EPS    -> Digito Rut Empresa
   -- p_DSC_SIS_ENT        -> Descripcion de sistema
   -- p_COD_USR            -> Usuario Tibco
   -- p_COD_USR_SIS        -> Usuario Sistema
   -- Output:
   -- p_ERROR              DETERMINA SI HUBO UN ERROR EN LA EJECUCION : 1 SI OCURRIO UN  ERROR DE LO CONTRARIO SERA 0
   -- p_NUM_FOL_OPE        VARIABLE QUE DEVUELVE EL NUMERO DE FOLIO DE LA OPERACION
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: 22/08/2018  : SE REALIZO UNA MODIFICACION ,EL CUAL SE DESPLAZO TODAS LA ACCIONES
                                   RELACIONADA A LA ACTUALIZACION AL PROCEDMIENTO SP_PAB_ACT_SALDO_MN_MX.
                                   (POR LO TANTO SE CAMBIO LA LOOGICA DEL MANEJO DE LOS DATOS )
   --***********************************************************************************************/
   PROCEDURE SP_PAB_INS_PYN_MNT_CJA (p_num_fol_nmn       IN     NUMBER,
                                     p_flg_ing_egr       IN     NUMBER,
                                     p_cod_bco           IN     CHAR,
                                     p_num_cta_cte       IN     VARCHAR2,
                                     p_tpo_ope           IN     CHAR,
                                     p_cod_dvi           IN     CHAR,
                                     p_imp_ope           IN     NUMBER,
                                     p_fec_vta           IN     DATE,
                                     p_gls_sis_org       IN     VARCHAR2,
                                     p_gls_mvt           IN     VARCHAR2,
                                     p_cod_sis_ent       IN     CHAR,
                                     p_num_doc_eps       IN     CHAR,
                                     p_cod_tpo_doc_eps   IN     CHAR,
                                     p_cod_usr           IN     VARCHAR,
                                     p_num_fol_ope          OUT NUMBER,
                                     p_error                OUT NUMBER)
   IS
      --Seteo de Variables
      v_nom_sp            VARCHAR2 (30) := 'SP_PAB_INS_PYN_MNT_CJA';
      v_num_fol_ope       NUMBER (12) := spk_pab_ope_cja_am.NEXTVAL;
      v_cod_bco_dtn       CHAR (11) := pkg_pab_constantes.v_bic_banco_santander;
      v_cod_bco_org       CHAR (11) := pkg_pab_constantes.v_bic_banco_santander;
      v_cod_bco_org_dtn   CHAR (11);
      v_COD_BCO           CHAR (11);
      v_num_doc_eps       CHAR (11);
      v_cod_tpo_doc_eps   CHAR (1);
      v_fec_ins_ope       DATE;
      v_fec_24hs          DATE;
      v_fec_48hs          DATE;
      v_cod_dvi           CHAR (3);
      v_val_fec           NUMBER := 0;
      v_num_cta_cte       VARCHAR2 (35);
      v_gls_mtv_ntf       VARCHAR2 (300);
      v_num_val_reg       VARCHAR (35);
      v_fecha_err EXCEPTION;
      v_rut_cta_err EXCEPTION;
      v_fecha_ant_err EXCEPTION;
   BEGIN
      --Obtenemos la fecha de valuta a 48 Horas para saber cual es la maxima fecha que se puede ingresar.
      v_fec_ins_ope := TRUNC (SYSDATE);
      v_fec_24hs := v_fec_ins_ope + 1;
      PKG_PAB_UTILITY.SP_PAB_DEV_DIA_HABIL_SIG (
         PKG_PAB_CONSTANTES.v_STR_CAL_CL,
         v_fec_24hs
      );
      v_fec_48hs := v_fec_24hs + 1;
      PKG_PAB_UTILITY.SP_PAB_DEV_DIA_HABIL_SIG (
         PKG_PAB_CONSTANTES.v_STR_CAL_CL,
         v_fec_48hs
      );

      -- se agrega cambio a mayusculas para comparaciones en clausulas Where
      v_cod_bco := UPPER (p_cod_bco);

      -- Se asigna banco segun flujo
      IF p_flg_ing_egr = pkg_pab_constantes.v_flg_ing
      THEN
         v_cod_bco_org := v_cod_bco;
      ELSE
         v_cod_bco_dtn := v_cod_bco;
      END IF;

      -- VALIDO SI MONEDA NACIONAL
      -- se agrega cambio a mayusculas para comparaciones en clausulas Where
      v_cod_dvi := UPPER (p_cod_dvi);

      -- SE VALIDA QUE LA FECHA VALUTA SEA MAYOR A LA FECHA ACTUAL
      IF p_fec_vta < v_fec_ins_ope                                -- inf nuevo
      THEN
         v_gls_mtv_ntf :=
               'Para la nomina Proyeccion: '
            || p_num_fol_nmn
            || '  Caja MN/MX no se cargara la operacion Banco:'
            || p_cod_bco
            || ' N? Cuenta:'
            || p_num_cta_cte
            || ' Moneda:'
            || p_cod_dvi
            || ' Monto:'
            || p_imp_ope
            || ' Fecha:'
            || TO_CHAR (p_fec_vta, 'dd-mm-yyyy')
            || ' por tener fecha valuta menor a la del dia';

         RAISE v_fecha_ant_err;
      END IF;

      --registros Caja Nacional
      IF v_cod_dvi = pkg_pab_constantes.v_str_dvi_clp
      THEN
         -- Valida que no sea mayor a la fecha del dia
         IF p_fec_vta > v_fec_ins_ope
         THEN
            v_gls_mtv_ntf :=
                  'Para la nomina Proyeccion: '
               || p_num_fol_nmn
               || '  Caja MN/MX no se cargara la operacion Banco:'
               || p_cod_bco
               || ' N? Cuenta:'
               || p_num_cta_cte
               || ' Moneda:'
               || p_cod_dvi
               || ' Monto:'
               || p_imp_ope
               || ' Fecha:'
               || TO_CHAR (p_fec_vta, 'dd-mm-yyyy')
               || ' por tener fecha valuta mayor a la del dia';

            RAISE v_fecha_err;
         END IF;
      ELSE
         -- SE DEBE VALIDAR LA CUENTA AL MOMENTO DE CARGAR.
         -- SI NO EXISTE, SE DEBE NOTIFICAR QUE NO SE CARGARA
         --
         v_num_cta_cte := TRIM (LEADING 0 FROM p_num_cta_cte);

         --Obtenmos el numero del dia (1...7)..(Sabado 7, Domingo 1)
         v_val_fec := TO_CHAR (p_fec_vta, 'D', 'NLS_DATE_LANGUAGE=SPANISH');


         -- SE DEBE VALIDAR LA CUENTA AL MOMENTO DE CARGAR.
         -- SI NO EXISTE, SE DEBE NOTIFICAR QUE NO SE CARGARA
         BEGIN
            SELECT   num_cta_cte
              INTO   v_num_val_reg
              FROM   pabs_dt_detll_saldo_caja_mn_mx
             WHERE   SUBSTR (cod_bco_org, 0, 8) = SUBSTR (v_cod_bco, 0, 8)
                     AND num_cta_cte = v_num_cta_cte
                     AND cod_dvi = v_cod_dvi;
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               v_gls_mtv_ntf :=
                     'Registro Banco: '
                  || v_cod_bco_dtn
                  || ', Numero de Cuenta : '
                  || v_num_cta_cte
                  || ', Divisa: '
                  || v_cod_dvi
                  || ' no se cargara PROYECCION ya que no existe relacion Cuenta/Bic/Divisa Valida';
               RAISE v_rut_cta_err;
         END;


         --Si es sabado 7
         IF v_val_fec = 7
         THEN                                                         --SABADO
            --Glosa Notificacion
            v_gls_mtv_ntf :=
                  'Para la nomina Proyeccion: '
               || p_num_fol_nmn
               || '  Caja MN/MX no se cargara la operacion Banco:'
               || p_COD_BCO
               || ' N? Cuenta:'
               || p_num_cta_cte
               || ' Moneda:'
               || p_cod_dvi
               || ' Monto:'
               || p_imp_ope
               || ' Fecha:'
               || TO_CHAR (p_fec_vta, 'dd-mm-yyyy')
               || ' por tener fecha valuta dia Sabado';

            --
            RAISE v_fecha_err;
         ELSE
            IF v_val_fec = 1
            THEN                                                     --DOMINGO
               --Glosa Notificacion
               v_gls_mtv_ntf :=
                     'Para la nomina Proyeccion: '
                  || p_num_fol_nmn
                  || '  Caja MN/MX no se cargara la operacion Banco:'
                  || p_cod_bco
                  || ' N? Cuenta:'
                  || p_num_cta_cte
                  || ' Moneda:'
                  || p_cod_dvi
                  || ' Monto:'
                  || p_imp_ope
                  || ' Fecha:'
                  || TO_CHAR (p_fec_vta, 'dd-mm-yyyy')
                  || ' por tener fecha valuta dia Domingo';

               RAISE v_fecha_err;
            END IF;
         END IF;

         IF p_fec_vta > v_fec_48hs
         THEN                                          -- FECHA MAYOR A 48 HRS
            --Glosa Notificacion
            v_gls_mtv_ntf :=
                  'Para la nomina Proyeccion: '
               || p_num_fol_nmn
               || '  Caja MN/MX no se cargara la operacion Banco:'
               || p_cod_bco
               || ' N? Cuenta:'
               || p_num_cta_cte
               || ' Moneda:'
               || p_cod_dvi
               || ' Monto:'
               || p_imp_ope
               || ' Fecha:'
               || TO_CHAR (p_fec_vta, 'dd-mm-yyyy')
               || ' por tener fecha valuta mayor a 48hrs';

            RAISE v_fecha_err;
         END IF;
      END IF;

      --Insertamos movimiento
      BEGIN
         --Inserta registro
         INSERT INTO pabs_dt_detll_mvnto_caja (num_fol_ope,
                                               fec_ins_ope,
                                               num_fol_nmn,
                                               COD_TPO_OPE_AOS,
                                               cod_bco_dtn,
                                               cod_bco_org,
                                               num_cta_cte,
                                               flg_ing_egr,
                                               imp_ope,
                                               fec_vta,
                                               cod_sis_ent,
                                               num_doc_eps,
                                               cod_tpo_doc_eps,
                                               gls_mvt,
                                               gls_sis_org,
                                               cod_dvi)
           VALUES   (v_num_fol_ope,
                     SYSDATE,
                     p_num_fol_nmn,
                     p_tpo_ope,
                     v_cod_bco_dtn,
                     v_cod_bco_org,
                     p_num_cta_cte,          -- limpiamos todos los 0 a la izq
                     p_flg_ing_egr,
                     p_imp_ope,
                     p_fec_vta,
                     p_cod_sis_ent,
                     v_num_doc_eps,
                     v_cod_tpo_doc_eps,
                     p_gls_mvt,
                     p_gls_sis_org,
                     v_cod_dvi);


         p_num_fol_ope := v_num_fol_ope;
      EXCEPTION
         WHEN DUP_VAL_ON_INDEX
         THEN
            pkg_pab_tracking.sp_pab_ins_log_bd (err_code,
                                                v_nom_pck,
                                                err_msg,
                                                v_nom_sp);

            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            pkg_pab_tracking.sp_pab_ins_log_bd (err_code,
                                                v_nom_pck,
                                                err_msg,
                                                v_nom_sp);
            p_error := pkg_pab_constantes.v_error;
         WHEN OTHERS
         THEN
            pkg_pab_tracking.sp_pab_ins_log_bd (err_code,
                                                v_nom_pck,
                                                err_msg,
                                                v_nom_sp);
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            p_s_mensaje := err_code || '-' || err_msg;
            pkg_pab_tracking.sp_pab_ins_log_bd (err_code,
                                                v_nom_pck,
                                                err_msg,
                                                v_nom_sp);
            p_error := pkg_pab_constantes.v_error;
            raise_application_error (-20000, p_s_mensaje);
      END;


      p_ERROR := Pkg_pab_constantes.v_ok;
   EXCEPTION
      WHEN v_fecha_err
      THEN
         --Grabamos notificacion
         pkg_pab_notificacion.sp_pab_ins_ntfcc_almot (
            pkg_pab_constantes.v_cod_tip_cro_vpcjm,
            p_cod_sis_ent,
            0,                                             --p_cod_tpo_rol_aos
            p_imp_ope,
            v_num_fol_ope,
            p_num_fol_nmn,
            v_gls_mtv_ntf,
            p_error
         );

         --Log
         err_msg := SUBSTR (v_gls_mtv_ntf, 1, 300);
         pkg_pab_tracking.sp_pab_ins_log_bd (err_code,
                                             v_nom_pck,
                                             err_msg,
                                             v_nom_sp);

         p_ERROR := PKG_PAB_CONSTANTES.v_OK;
      WHEN v_fecha_ant_err                                            -- nuevo
      THEN
         --Grabamos notificacion
         pkg_pab_notificacion.sp_pab_ins_ntfcc_almot (
            pkg_pab_constantes.v_cod_tip_cro_vpcjm,
            p_cod_sis_ent,
            0,                                             --p_cod_tpo_rol_aos
            p_imp_ope,
            v_num_fol_ope,
            p_num_fol_nmn,
            v_gls_mtv_ntf,
            p_error
         );

         --Log
         err_msg := SUBSTR (v_gls_mtv_ntf, 1, 300);
         pkg_pab_tracking.sp_pab_ins_log_bd (err_code,
                                             v_nom_pck,
                                             err_msg,
                                             v_nom_sp);
      WHEN v_rut_cta_err
      THEN
         --Grabamos notificacion
         pkg_pab_notificacion.sp_pab_ins_ntfcc_almot (
            pkg_pab_constantes.v_cod_tip_cro_vpcjm,
            p_cod_sis_ent,
            0,                                             --p_cod_tpo_rol_aos
            p_imp_ope,
            v_num_fol_ope,
            p_num_fol_nmn,
            v_gls_mtv_ntf,
            p_error
         );

         --Log
         err_msg := SUBSTR (v_gls_mtv_ntf, 1, 300);
         pkg_pab_tracking.sp_pab_ins_log_bd (err_code,
                                             v_nom_pck,
                                             err_msg,
                                             v_nom_sp);

         p_error := pkg_pab_constantes.v_ok;
      WHEN DUP_VAL_ON_INDEX
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         pkg_pab_tracking.sp_pab_ins_log_bd (err_code,
                                             v_nom_pck,
                                             err_msg,
                                             v_nom_sp);
         p_error := pkg_pab_constantes.v_error;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         p_s_mensaje := err_code || '-' || err_msg;
         pkg_pab_tracking.sp_pab_ins_log_bd (err_code,
                                             v_nom_pck,
                                             err_msg,
                                             v_nom_sp);
         p_error := pkg_pab_constantes.v_error;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END Sp_PAB_INS_PYN_MNT_CJA;

   -- ****************************
   -- Funcion/Procedimiento: Sp_PAB_INS_CRTLA_CARGO_ABONO
   -- Objetivo: Procedimiento almacenado que inserta Movimientos de cargo o abono.(MT 900 y 910)
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_DETLL_CARGO_ABONO_CAJA
   -- Fecha: 12/10/17
   -- Autor: Santander
   -- Input:
   -- p_FEC_ING_CTL    -> Fecha de ingreso cartola
   -- p_HOR_ING_CTL    -> Hora de ingreso
   -- p_NUM_FOL_CTL    -> Numero de folio cartola
   -- p_NUM_REF_SWF_CTL-> Referencia cartola
   -- p_COD_MT_SWF     -> Codigo mt de cartola
   -- p_COD_DVI        -> Moneda de movimiento
   -- p_NUM_CTA_CTE    -> Numero de cuenta corriente
   -- p_COD_BCO_ORG    -> Banco origen cartola
   -- p_FLG_DEB_CRE_INI-> Flag debito o credito inicial
   -- p_IMP_ING_CTL    -> Monto ingreso cartola
   -- p_IMP_EGR_CTL    -> Monto egreso cartola
   -- p_IMP_INI_CTL    -> Monto incio cartola
   -- p_IMP_FNL_CTL    -> Monto final cartola
   -- p_FLG_DEB_CRE_FNL-> Flag debito o credito final
   -- p_FLG_ING_EGR    -> Flag de ingreso o egreso
   -- Output:
   -- v_NUM_FOL_OPE    ->Folio de MT ingresado
   -- v_FEC_ING_OPE    ->Fecha de mt ingresado
   -- p_ERROR-> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ?ltimos cambios>
   -- ***********************************************************************************************
   PROCEDURE Sp_PAB_INS_CRTLA_CARGO_ABONO (p_COD_MT_SWF    IN     NUMBER,
                                           p_COD_DVI       IN     CHAR,
                                           p_COD_BCO_DTN   IN     VARCHAR2,
                                           p_COD_BCO_ORG   IN     VARCHAR2,
                                           p_COD_BCO_ITD   IN     CHAR,
                                           p_NUM_SEQ_IFD   IN     NUMBER,
                                           p_FEC_ING_IFD   IN     DATE,
                                           p_HOR_ING_IFD   IN     NUMBER,
                                           p_NUM_REF_SWF   IN     CHAR,
                                           p_NUM_CTA_CTE   IN     VARCHAR2,
                                           p_FEC_VTA       IN     DATE,
                                           p_IMP_OPE       IN     NUMBER,
                                           p_GLS_TRN       IN     VARCHAR2,
                                           p_NUM_REF_EXT   IN     CHAR,
                                           --------------------------------
                                           p_NUM_DOC_BFC   IN     VARCHAR2,
                                           p_NOM_BFC       IN     VARCHAR2,
                                           ---------------------------------
                                           p_NUM_FOL_OPE      OUT NUMBER,
                                           p_FEC_ING_OPE      OUT TIMESTAMP,
                                           p_ERROR            OUT NUMBER)
   IS
      v_NOM_SP            VARCHAR2 (30) := 'Sp_PAB_INS_CRTLA_CARGO_ABONO';
      v_NUM_FOL_OPE       NUMBER := SPK_PAB_MSJ_IAM.NEXTVAL;
      v_FEC_ING_OPE       TIMESTAMP;
      v_FLG_MRD_UTZ_SWF   NUMBER (1);
      v_FLG_CGO_ABN       NUMBER (1);
      v_COD_TPO_OPE_AOS CHAR (8)
            := REGEXP_REPLACE (SUBSTR (TRIM (p_NUM_REF_EXT), 1, 8), '[0-9]') ;
      v_DSC_TPO_OPE_AOS   VARCHAR2 (100);
      v_REF_ABN_PBB       CHAR (8) := 'PBBCCT';
      v_REF_ABN_TRF       CHAR (8) := 'PIECJT';
      v_REF_ABN_COR       CHAR (8) := 'CORRES';
      v_REF_ABN_DCO       CHAR (8) := 'TRFBIDCO';
      v_REF_ABN_DFE       CHAR (8) := 'TRFBIDFE';
      v_REF_ABN_BRF       CHAR (8) := 'TRFBRF';
      v_REF_ABN_TGR       CHAR (8) := 'TRFTGR';
      v_REF_ABN_FMI       CHAR (8) := 'TRFFMI';
      v_COD_BCO_ORG       CHAR (11);
      v_COD_SIS_ENT       CHAR (10);
      v_COD_SIS_SAL       CHAR (10);
      --------------------------------------------------
      v_NOM_BFC           VARCHAR2 (100) := '';
      v_NUM_DOC_BFC_NUM   NUMBER (8) := 0;
      v_NUM_DOC_BFC       VARCHAR2 (15) := '';
      v_COD_TPD_BFC       CHAR (1) := '';
      v_NUM_DOC_ORD       VARCHAR2 (15) := '';
      v_COD_TPD_ORD       CHAR (1) := '';
      v_NUM_CTA_ORD       VARCHAR2 (16) := '';
      v_NOM_ORD           VARCHAR2 (100) := '';
      --------------------------------------------------

      no_exi_case_mt exception;
      mt_no_ges exception;

      v_validacion        NUMBER;
      v_mensaje           VARCHAR2 (200);
   BEGIN
      --Ingreso de operacion
      v_FEC_ING_OPE := SYSTIMESTAMP;
      v_COD_BCO_ORG := p_COD_BCO_ORG;

      --Indica el mercado segun la moneda
      IF p_COD_DVI = PKG_PAB_CONSTANTES.V_STR_DVI_CLP
      THEN
         v_FLG_MRD_UTZ_SWF := PKG_PAB_CONSTANTES.V_FLG_MRD_MN;     -- Nacional
      ELSE
         v_FLG_MRD_UTZ_SWF := PKG_PAB_CONSTANTES.V_FLG_MRD_MX;   -- Extranjero
      END IF;


      --Debemos verificar si se debe insertar el mt900/910
      IF (p_COD_MT_SWF = PKG_PAB_CONSTANTES.V_COD_MT900
          OR p_COD_MT_SWF = PKG_PAB_CONSTANTES.V_COD_MT910)
      THEN
         PKG_PAB_OPERACION.sp_vrfca_num_swf (p_NUM_REF_EXT,
                                             p_COD_MT_SWF,              --NULL
                                             4,
                                             p_FEC_VTA,
                                             v_validacion,
                                             p_ERROR,
                                             v_mensaje);
         DBMS_OUTPUT.put_line ('v_validacion: ' || v_validacion);

         --Segun el mercado verificamos que hacemos
         --Si es mercado extranjero
         IF v_FLG_MRD_UTZ_SWF = PKG_PAB_CONSTANTES.V_FLG_MRD_MX
         THEN
            --Si existe en protocolos o en operaciones detenemos el proceso
            IF (v_validacion = 2 OR v_validacion = 3)
            THEN
               PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (v_validacion,
                                                   v_NOM_PCK,
                                                   v_mensaje,
                                                   v_NOM_SP);
               RAISE mt_no_ges;
            END IF;
         ELSE
            --Si es mercado nacional

            --Si existe en protocolos
            IF (v_validacion = 2)
            THEN
               PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (v_validacion,
                                                   v_NOM_PCK,
                                                   v_mensaje,
                                                   v_NOM_SP);

               RAISE mt_no_ges;
            END IF;
         END IF;
      END IF;

      CASE
         --MT900
         WHEN p_COD_MT_SWF = PKG_PAB_CONSTANTES.V_COD_MT900
         THEN
            --Cargo
            v_FLG_CGO_ABN := PKG_PAB_CONSTANTES.V_FLG_MOV_CGO;

            --Codigo Sistema operacion y sistema entrada
            PKG_PAB_UTILITY.Sp_PAB_OBT_CNL_CON_TPO_OPE (v_COD_TPO_OPE_AOS,
                                                        v_COD_SIS_ENT, --Completo sistema entrada
                                                        v_DSC_TPO_OPE_AOS,
                                                        p_ERROR);

            --Si no encuentra el tipo de operacion
            IF (v_COD_SIS_ENT = PKG_PAB_CONSTANTES.V_STR_TPO_OPE_SIDEN)
            THEN
               v_COD_TPO_OPE_AOS := PKG_PAB_CONSTANTES.V_STR_TPO_OPE_SIDEN;
               v_COD_SIS_ENT := NULL;
            END IF;

            --Se debe analizar segun BIC
            IF (p_COD_BCO_ORG = TRIM (PKG_PAB_CONSTANTES.V_BIC_BCCH))
            THEN
               v_COD_SIS_SAL := PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SAL_LBTR;
            ELSE
               v_COD_SIS_SAL := PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SAL_SWF;
               v_COD_SIS_ENT := PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SE_OOFF;
            END IF;


            --Si es 910 y es dvpdcv salida es combanc
            IF (v_COD_TPO_OPE_AOS = PKG_PAB_CONSTANTES.V_STR_TPO_OPE_DVP)
            THEN
               v_COD_SIS_ENT := PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SAL_CMN;
            END IF;
         --MT910
         WHEN p_COD_MT_SWF = PKG_PAB_CONSTANTES.V_COD_MT910
         THEN
            -- Abono
            v_FLG_CGO_ABN := PKG_PAB_CONSTANTES.V_FLG_MOV_ABN;


            --se agrega que cuando llegue el OPB de Corresponsal, sea producto OPB
            IF     p_COD_DVI = PKG_PAB_CONSTANTES.V_STR_DVI_USD
               AND p_COD_BCO_ORG <> PKG_PAB_CONSTANTES.V_BIC_BCCH
               AND p_COD_BCO_ITD = 'BCECCLRM   '
            THEN
               v_COD_TPO_OPE_AOS := PKG_PAB_CONSTANTES.V_STR_TPO_OPE_OPB;
               v_COD_SIS_SAL := PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SE_OOFF;
               v_DSC_TPO_OPE_AOS := 'Operaciones Financieras';
            ELSE
               --Codigo Sistema operacion y sistema salida
               PKG_PAB_UTILITY.Sp_PAB_OBT_CNL_CON_TPO_OPE (v_COD_TPO_OPE_AOS,
                                                           v_COD_SIS_SAL,
                                                           v_DSC_TPO_OPE_AOS,
                                                           p_ERROR);
            END IF;

            --Si es 910 y es dvpdcv salida es combanc
            IF (v_COD_TPO_OPE_AOS = PKG_PAB_CONSTANTES.V_STR_TPO_OPE_DVP)
            THEN
               v_COD_SIS_SAL := PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SAL_CMN;
            END IF;

            --Si no encuentra el tipo de operacion
            IF (v_COD_SIS_SAL = PKG_PAB_CONSTANTES.V_STR_TPO_OPE_SIDEN)
            THEN
               v_COD_TPO_OPE_AOS := PKG_PAB_CONSTANTES.V_STR_TPO_OPE_SIDEN;
               v_COD_SIS_SAL := NULL;
            END IF;

            --Segun la referencia se realiza abono
            IF    v_COD_TPO_OPE_AOS = v_REF_ABN_PBB
               OR v_COD_TPO_OPE_AOS = v_REF_ABN_TRF
               OR v_COD_TPO_OPE_AOS = v_REF_ABN_COR
               OR v_COD_TPO_OPE_AOS = v_REF_ABN_DCO
               OR v_COD_TPO_OPE_AOS = v_REF_ABN_DFE
               OR v_COD_TPO_OPE_AOS = v_REF_ABN_BRF
               OR v_COD_TPO_OPE_AOS = v_REF_ABN_TGR
               OR v_COD_TPO_OPE_AOS = v_REF_ABN_FMI
            THEN
               --Cuentas Corrientes
               v_COD_SIS_SAL := PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SAL_CTACTE;


               -- se debe separar rut

               PKG_PAB_UTILITY.Sp_PAB_DESCOMp_RUT (p_NUM_DOC_BFC,
                                                   v_NUM_DOC_BFC,
                                                   v_COD_TPD_BFC);

               v_NOM_BFC := p_NOM_BFC;
               v_NUM_DOC_ORD := '97029000';
               v_COD_TPD_ORD := '1';
               v_NUM_CTA_ORD := '34220102314';
               v_NOM_ORD := 'BANCO CENTRAL DE CHILE';
            END IF;

            --Se debe analizar segun BIC
            IF (p_COD_BCO_ORG = TRIM (PKG_PAB_CONSTANTES.V_BIC_BCCH))
            THEN
               v_COD_SIS_ENT := PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SAL_LBTR;
            ELSE
               v_COD_SIS_ENT := PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SAL_SWF;
               v_COD_SIS_SAL := PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SE_OOFF;
            END IF;
         --MT103
         WHEN p_COD_MT_SWF = PKG_PAB_CONSTANTES.V_COD_MT103 OR p_COD_MT_SWF = PKG_PAB_CONSTANTES.V_COD_MT202
         THEN
            IF v_COD_BCO_ORG = PKG_PAB_CONSTANTES.V_BIC_BANCO_SANTANDER
            THEN
               v_FLG_CGO_ABN := PKG_PAB_CONSTANTES.V_FLG_MOV_CGO;
            ELSE
               v_FLG_CGO_ABN := PKG_PAB_CONSTANTES.V_FLG_MOV_ABN;
            END IF;
         ELSE
            RAISE no_exi_case_mt;
      END CASE;



      INSERT INTO PABS_DT_DETLL_CARGO_ABONO_CAJA (NUM_FOL_OPE,
                                                  FEC_ING_OPE,
                                                  FLG_MRD_UTZ_SWF,
                                                  COD_MT_SWF,
                                                  COD_DVI,
                                                  COD_BCO_DTN,
                                                  COD_BCO_ORG,
                                                  COD_BCO_ITD,
                                                  FLG_CGO_ABN,
                                                  NUM_SEQ_IFD,
                                                  FEC_ING_IFD,
                                                  HOR_ING_IFD,
                                                  NUM_REF_SWF,
                                                  NUM_CTA_CTE,
                                                  FEC_VTA,
                                                  IMP_OPE,
                                                  GLS_TRN,
                                                  NUM_REF_EXT,
                                                  COD_SIS_ENT,
                                                  COD_SIS_SAL,
                                                  COD_TPO_OPE_AOS,
                                                  ------------------------
                                                  NUM_DOC_ODN,
                                                  COD_TPD_ODN,
                                                  NOM_ODN,
                                                  NUM_DOC_BFC,
                                                  COD_TPD_BFC,
                                                  NUM_CTA_ODN,
                                                  NOM_BFC)
        VALUES   (v_NUM_FOL_OPE,
                  v_FEC_ING_OPE,
                  v_FLG_MRD_UTZ_SWF,
                  p_COD_MT_SWF,
                  p_COD_DVI,
                  p_COD_BCO_DTN,
                  p_COD_BCO_ORG,
                  p_COD_BCO_ITD,
                  v_FLG_CGO_ABN,
                  p_NUM_SEQ_IFD,
                  p_FEC_ING_IFD,
                  p_HOR_ING_IFD,
                  p_NUM_REF_SWF,
                  p_NUM_CTA_CTE,
                  p_FEC_VTA,
                  p_IMP_OPE,
                  p_GLS_TRN,
                  p_NUM_REF_EXT,
                  v_COD_SIS_ENT,
                  v_COD_SIS_SAL,
                  v_COD_TPO_OPE_AOS,
                  ---------------------------
                  v_NUM_DOC_ORD,
                  v_COD_TPD_ORD,
                  v_NOM_ORD,
                  v_NUM_DOC_BFC,
                  v_COD_TPD_BFC,
                  v_NUM_CTA_ORD,
                  v_NOM_BFC);

      --Variables de salida
      p_NUM_FOL_OPE := v_NUM_FOL_OPE;
      p_FEC_ING_OPE := v_FEC_ING_OPE;

      -- Actualizacion Caja MN
      IF v_FLG_MRD_UTZ_SWF = PKG_PAB_CONSTANTES.V_FLG_MRD_MN
      THEN
         -- Si No es DVP
         IF v_COD_TPO_OPE_AOS <> PKG_PAB_CONSTANTES.V_STR_TPO_OPE_DVP
         THEN
            --Se registra en Saldo Bilaterales y Saldos MN-MX de acuerdo al MT900 o 910
            IF p_COD_MT_SWF = PKG_PAB_CONSTANTES.V_COD_MT900
            THEN                                                      -- Cargo
               -- Inserta operacion en tabla de saldos bilaterales
               PKG_PAB_CAJA_MN.SP_PAB_CALCULA_SALDO_BILATERAL (
                  p_IMP_OPE,
                  PKG_PAB_CONSTANTES.V_BIC_BCCH,
                  PKG_PAB_CONSTANTES.V_FLG_EGR,
                  PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SAL_LBTR,
                  p_ERROR
               );

               --Insertamos para actualizar la posicion del banco central
               PKG_PAB_CAJA_MN.SP_PAB_CAL_SLDO_POS_LBTR (v_cero,
                                                         v_cero,
                                                         v_cero,
                                                         p_IMP_OPE,
                                                         p_ERROR);

               --Insertamos para actualizar saldos MT
               PKG_PAB_CAJA_MN.SP_PAB_CAL_SLDO_MNT_SWF (v_cero,
                                                        v_cero,
                                                        v_cero,
                                                        p_IMP_OPE,
                                                        v_cero,
                                                        v_cero,
                                                        P_ERROR);

               --Si no encuentra el tipo de operacion
               IF (v_COD_SIS_ENT <> PKG_PAB_CONSTANTES.V_STR_TPO_OPE_SIDEN)
               THEN
                  --Calculamos el monto del area
                  PKG_PAB_CAJA_MN.SP_BUS_CALCULA_AREA_MN (v_COD_SIS_ENT,
                                                          0,
                                                          p_IMP_OPE,
                                                          p_ERROR);
               END IF;
            ELSE
               --910

               -- Inserta operacion en tabla de saldos bilaterales
               PKG_PAB_CAJA_MN.SP_PAB_CALCULA_SALDO_BILATERAL (
                  p_IMP_OPE,
                  PKG_PAB_CONSTANTES.V_BIC_BCCH,
                  PKG_PAB_CONSTANTES.V_FLG_ING,
                  PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SAL_LBTR,
                  p_ERROR
               );

               --Insertamos para actualizar la posicion del banco central
               PKG_PAB_CAJA_MN.SP_PAB_CAL_SLDO_POS_LBTR (v_cero,
                                                         v_cero,
                                                         p_IMP_OPE,
                                                         v_cero,
                                                         p_ERROR);

               --Insertamos para actualizar saldos MT
               PKG_PAB_CAJA_MN.SP_PAB_CAL_SLDO_MNT_SWF (v_cero,
                                                        v_cero,
                                                        v_cero,
                                                        v_cero,
                                                        p_IMP_OPE,
                                                        v_cero,
                                                        P_ERROR);

               --Si no encuentra el tipo de operacion
               IF (v_COD_SIS_SAL <> PKG_PAB_CONSTANTES.V_STR_TPO_OPE_SIDEN)
               THEN
                  --Calculamos el monto del area
                  PKG_PAB_CAJA_MN.SP_BUS_CALCULA_AREA_MN (v_COD_SIS_SAL,
                                                          p_IMP_OPE,
                                                          0,
                                                          p_ERROR);
               END IF;
            END IF;
         ELSE
            err_msg :=
                  'Protocolo:'
               || p_COD_MT_SWF
               || ' Referencia:'
               || p_NUM_REF_EXT
               || ' No se procesa Caja MN por ser DVP';
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
         END IF;
      END IF;

      p_ERROR := PKG_PAB_CONSTANTES.v_OK;
   EXCEPTION
      WHEN mt_no_ges
      THEN
         err_code := SQLCODE;
         err_msg :=
               'El MT:'
            || p_COD_MT_SWF
            || ' Referencia:'
            || p_NUM_REF_SWF
            || ' Referencia Ext:'
            || p_NUM_REF_EXT
            || ' no sera procesado por existir ya en altos montos';
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN no_exi_case_mt
      THEN
         err_code := SQLCODE;
         err_msg := 'MT que no se puede procesar';
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN DUP_VAL_ON_INDEX
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         p_s_mensaje := err_code || '-' || err_msg;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END Sp_PAB_INS_CRTLA_CARGO_ABONO;

   --************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_INS_CBCRA_CRTLA_MT
   -- Objetivo: Procedimiento que inserta cartola - Movimiento de entrada y salida de cuentas corrientes de bancos corresponsales
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_CBCRA_CRTLA
   -- Fecha: 03/11/17
   -- Autor: Santander
   -- Input:
   -- p_COD_BCO_ORG
   -- p_NUM_CTA_CTE
   -- p_NUM_FOL_CTL
   -- p_COD_DVI
   -- p_NUM_REF_SWF_CTL
   -- p_FEC_ING_CTL
   -- p_HOR_ING_CTL
   -- p_COD_MT_SWF
   -- p_IM-- p_INI_CTL
   -- p_IM-- p_ING_CTL
   -- p_IM-- p_EGR_CTL
   -- p_IM-- p_FNL_CTL
   -- p_FLG_DEB_CRE_INI
   -- p_FLG_DEB_CRE_FNL
   -- p_FLG_INI_FNL
   -- Output:
   -- p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE Sp_PAB_INS_CBCRA_CRTLA_MT (p_COD_BCO_ORG       IN     VARCHAR2,
                                        p_NUM_CTA_CTE       IN     VARCHAR2,
                                        p_NUM_FOL_CTL       IN     NUMBER,
                                        p_COD_DVI           IN     CHAR,
                                        p_NUM_REF_SWF_CTL   IN     VARCHAR2,
                                        p_FEC_ING_CTL       IN     DATE,
                                        p_HOR_ING_CTL       IN     NUMBER,
                                        p_COD_MT_SWF        IN     VARCHAR2,
                                        p_IMP_INI_CTL       IN     NUMBER,
                                        p_IMP_ING_CTL       IN     NUMBER,
                                        p_IMP_EGR_CTL       IN     NUMBER,
                                        p_IMP_FNL_CTL       IN     NUMBER,
                                        p_FLG_DEB_CRE_INI   IN     CHAR,
                                        p_FLG_DEB_CRE_FNL   IN     CHAR,
                                        p_FLG_INI_FNL       IN     NUMBER,
                                        p_ERROR                OUT NUMBER)
   IS
      v_NOM_SP            VARCHAR2 (50) := 'Sp_PAB_INS_CBCRA_CRTLA_MT';
      v_EXISTE            NUMBER (11) := 0;
      v_IMP_INI_CTL       NUMBER (15, 2);
      v_IMP_FNL_CTL       NUMBER (15, 2);
      v_FLG_DEB_CRE       NUMBER := 0;
      v_FLG_DEB_CRE_INI   NUMBER := 0;

      v_COD_BCO_ORG       CHAR (11);
      v_COD_DVI           CHAR (3);

      v_NUM_CTA_CTE       VARCHAR2 (35);
   BEGIN
      v_NUM_CTA_CTE := TRIM (LEADING 0 FROM p_NUM_CTA_CTE);

      --
      IF p_FLG_DEB_CRE_INI = 'C'
      THEN
         v_FLG_DEB_CRE := PKG_PAB_CONSTANTES.V_FLG_ING;              --Credito
      ELSE
         v_FLG_DEB_CRE := PKG_PAB_CONSTANTES.V_FLG_EGR;               --Debito
      END IF;

      IF p_FLG_DEB_CRE_FNL = 'C'
      THEN
         v_FLG_DEB_CRE_INI := PKG_PAB_CONSTANTES.V_FLG_ING;
      ELSE
         v_FLG_DEB_CRE_INI := PKG_PAB_CONSTANTES.V_FLG_EGR;
      END IF;

      --
      v_COD_BCO_ORG := p_COD_BCO_ORG;
      v_COD_DVI := p_COD_DVI;

      -- Se valida si el la primera cartola o la ultima
      IF p_FLG_INI_FNL = 0
      THEN
         v_IMP_INI_CTL := p_IMP_INI_CTL;
         v_IMP_FNL_CTL := NULL;
      --Ultima Cartola
      ELSIF p_FLG_INI_FNL = 1
      THEN
         v_IMP_INI_CTL := NULL;
         v_IMP_FNL_CTL := p_IMP_FNL_CTL;
      --Cartola Unica
      ELSIF p_FLG_INI_FNL = 2
      THEN
         v_IMP_INI_CTL := p_IMP_INI_CTL;
         v_IMP_FNL_CTL := p_IMP_FNL_CTL;
      --Cartola Intermedia
      ELSIF p_FLG_INI_FNL = 3
      THEN
         v_IMP_INI_CTL := NULL;
         v_IMP_FNL_CTL := NULL;
      END IF;

      --Insertamos Registro de cabecera
      err_msg :=
            'Insertando Cabecera de Cartola: p_NUM_CTA_CTE:'
         || LENGTH (p_NUM_CTA_CTE)
         || ' p_NUM_FOL_CTL:'
         || p_NUM_FOL_CTL
         || ' v_COD_BCO_ORG:'
         || v_COD_BCO_ORG
         || ' p_FEC_ING_CTL:'
         || p_FEC_ING_CTL
         || ' p_NUM_REF_SWF_CTL:'
         || p_NUM_REF_SWF_CTL
         || ' - ';
      PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                          v_NOM_PCK,
                                          err_msg,
                                          v_NOM_SP);

      BEGIN
         INSERT INTO PABS_DT_CBCRA_CRTLA (FEC_ING_CTL,
                                          HOR_ING_CTL,
                                          NUM_FOL_CTL,
                                          NUM_REF_SWF_CTL,
                                          COD_MT_SWF,
                                          COD_DVI,
                                          NUM_CTA_CTE,
                                          COD_BCO_ORG,
                                          FLG_DEB_CRE_INI,
                                          IMP_ING_CTL,
                                          IMP_EGR_CTL,
                                          IMP_INI_CTL,
                                          IMP_FNL_CTL,
                                          FLG_DEB_CRE_FNL,
                                          FLG_ING_EGR)
           VALUES   (p_FEC_ING_CTL,
                     p_HOR_ING_CTL,
                     p_NUM_FOL_CTL,
                     p_NUM_REF_SWF_CTL,
                     p_COD_MT_SWF,
                     p_COD_DVI,
                     v_NUM_CTA_CTE,                           --p_NUM_CTA_CTE,
                     v_COD_BCO_ORG,
                     v_FLG_DEB_CRE,
                     p_IMP_ING_CTL,
                     p_IMP_EGR_CTL,
                     p_IMP_INI_CTL,
                     p_IMP_FNL_CTL,
                     v_FLG_DEB_CRE_INI,
                     p_FLG_INI_FNL);

         --Por ser la primera cartola necesitamos que se reinicien los valores de caja clp
         IF (p_COD_DVI = PKG_PAB_CONSTANTES.V_STR_DVI_CLP)
         THEN
            --Insertamos Registro de cabecera
            err_msg :=
               'Es primera cartola CLP por lo que se reiniciaran los saldos.';
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);

            PKG_PAB_CAJA_MN.SP_REI_SLDO_MN (p_ERROR);
         END IF;
      EXCEPTION
         WHEN DUP_VAL_ON_INDEX
         THEN
            --Ya se inserto la cartola debemos actualizar solo los saldos inicial o final,
            --Banco - Fecha Cartola - Cuenta

            --Insertamos Registro de cabecera
            err_msg := 'Ya existia la cartola se actualiza su informacion';
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);

            UPDATE   PABS_DT_CBCRA_CRTLA
               SET   FEC_ING_CTL = p_FEC_ING_CTL, -- se agrega actualizacion de cartola.
                     IMP_INI_CTL = NVL (v_IMP_INI_CTL, IMP_INI_CTL),
                     IMP_FNL_CTL = NVL (v_IMP_FNL_CTL, IMP_FNL_CTL)
             WHERE       NUM_CTA_CTE = v_NUM_CTA_CTE
                     AND COD_BCO_ORG = v_COD_BCO_ORG
                     AND FEC_ING_CTL = p_FEC_ING_CTL
                     AND COD_DVI = p_COD_DVI;
         WHEN OTHERS
         THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            p_s_mensaje := err_code || '-' || err_msg;
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
      END;

      p_ERROR := PKG_PAB_CONSTANTES.v_ok;
   EXCEPTION
      WHEN DUP_VAL_ON_INDEX
      THEN
         err_code := SQLCODE;
         err_msg :=
               'Error al insertar cabecera cartola de la cuenta: '
            || p_NUM_CTA_CTE
            || ' -> '
            || p_NUM_FOL_CTL;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         p_s_mensaje := err_code || '-' || err_msg;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END Sp_PAB_INS_CBCRA_CRTLA_MT;

   --************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_INS_DETLL_CRTLA_MT
   -- Objetivo: Procedimiento que inserta cartola - Movimiento de entrada y salida de cuentas corrientes de bancos corresponsales
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_DETLL_CRTLA
   -- Fecha: 03/11/17
   -- Autor: Santander
   -- Input:
   -- p_NUM_FOL_CTL
   -- p_FEC_TRN_OPE
   -- p_NUM_REF_SWF_OPE
   -- p_COD_TRN_OPE
   -- p_NUM_REF_ITN_CTA
   -- p_FLG_DEB_CRE
   -- p_IMp_TRN
   -- p_GLS_TRN
   -- p_NUM_CTA_CTE
   -- p_FEC_ING_CTL
   -- Output:
   -- p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE Sp_PAB_INS_DETLL_CRTLA_MT (p_NUM_FOL_CTL       IN     NUMBER,
                                        p_FEC_TRN_OPE       IN     TIMESTAMP,
                                        p_NUM_REF_SWF_OPE   IN     VARCHAR2,
                                        p_COD_TRN_OPE       IN     VARCHAR2,
                                        p_NUM_REF_ITN_CTA   IN     VARCHAR2,
                                        p_FLG_DEB_CRE       IN     VARCHAR2,
                                        p_IMP_TRN           IN     NUMBER,
                                        p_GLS_TRN           IN     VARCHAR2,
                                        p_NUM_CTA_CTE       IN     VARCHAR2,
                                        p_COD_BCO_ORG       IN     VARCHAR2,
                                        p_COD_DVI           IN     VARCHAR2,
                                        p_FEC_ING_CTL       IN     DATE,
                                        p_ERROR                OUT NUMBER)
   IS
      v_NOM_SP        VARCHAR2 (30) := 'Sp_PAB_INS_DETLL_CRTLA_MT';
      v_COD_BCO_ORG   CHAR (11);
      v_NUM_FOL_OPE   NUMBER (12) := SPK_PAB_OPE_CJA_AM.NEXTVAL;
      v_FLG_DEB_CRE   NUMBER := 0;
      v_NUM_CTA_CTE   VARCHAR (35);
      v_COD_DVI       CHAR (3);
   BEGIN
      v_COD_DVI := TO_CHAR (p_COD_DVI);

      IF p_FLG_DEB_CRE = 'C'
      THEN
         v_FLG_DEB_CRE := PKG_PAB_CONSTANTES.V_FLG_ING;              --Credito
      ELSE
         v_FLG_DEB_CRE := PKG_PAB_CONSTANTES.V_FLG_EGR;               --Debito
      END IF;

      v_NUM_CTA_CTE := TRIM (LEADING 0 FROM p_NUM_CTA_CTE);

      BEGIN
         INSERT INTO PABS_DT_DETLL_CRTLA (FEC_ING_CTL,
                                          NUM_FOL_OPE,
                                          FEC_TRN_OPE,
                                          NUM_REF_SWF_OPE,
                                          COD_TRN_OPE,
                                          NUM_REF_ITN_CTA,
                                          FLG_DEB_CRE,
                                          IMP_TRN,
                                          GLS_TRN,
                                          NUM_CTA_CTE,
                                          COD_DVI,
                                          COD_BCO_ORG)
           VALUES   (p_FEC_ING_CTL,
                     v_NUM_FOL_OPE,
                     p_FEC_TRN_OPE,
                     p_NUM_REF_SWF_OPE,
                     p_COD_TRN_OPE,
                     p_NUM_REF_ITN_CTA,
                     v_FLG_DEB_CRE,
                     p_IMP_TRN,
                     p_GLS_TRN,
                     v_NUM_CTA_CTE,
                     v_COD_DVI,
                     SUBSTRB (p_COD_BCO_ORG, 0, 8)             --p_COD_BCO_ORG
                                                  );
      EXCEPTION
         WHEN DUP_VAL_ON_INDEX
         THEN
            err_code := SQLCODE;
            err_msg := '';
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         WHEN OTHERS
         THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            p_s_mensaje := err_code || '-' || err_msg;
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
      END;

      p_ERROR := PKG_PAB_CONSTANTES.v_ok;
   EXCEPTION
      WHEN DUP_VAL_ON_INDEX
      THEN
         err_code := SQLCODE;
         err_msg := '';
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         p_s_mensaje := err_code || '-' || err_msg;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END Sp_PAB_INS_DETLL_CRTLA_MT;



   --**********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_CNA_SLD_CJA
   -- Objetivo: Retorna totales de saldos intradia, 24 y 48
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_DETLL_MVNTO_CAJA
   -- Fecha: 04/10/17
   -- Autor: Santander
   -- Input:
   -- p_FEC_VAL      --> FECHA DE VALUTA OPERACION
   -- p_NUM_CTA_CTE  --> CUENTA CORRIENTE
   -- p_COD_DIV      --> CODIGO DIVISA
   -- p_COD_BCO      --> CODIGO BANCO
   -- N/A
   -- Output:
   -- p_SDO_ING      --> SALGO INGRESOS
   -- p_SDO_EGRE     --> SALDO EGRESOS
   -- p_SDO_TOT      --> SALDO TOTALES
   -- p_ERROR         >  Indicada si el procedimiento
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de utimos cambios>
   --***********************************************************************************************
   PROCEDURE SP_PAB_CNA_SLD_CJA (p_FEC_VAL       IN     DATE,
                                 p_NUM_CTA_CTE   IN     VARCHAR2,
                                 p_COD_DIV       IN     CHAR,
                                 p_COD_BCO       IN     VARCHAR2,
                                 p_SDO_ING          OUT NUMBER,
                                 p_SDO_EGRE         OUT NUMBER,
                                 p_SDO_TOT          OUT NUMBER,
                                 p_ERROR            OUT NUMBER)
   IS
      v_NOM_SP     VARCHAR2 (30) := 'SP_PAB_CNA_SLD_CJA';
      v_COD_DIV    PABS_DT_DETLL_MVNTO_CAJA.COD_DVI%TYPE;          --CHAR (3);
      v_COD_BCO    PABS_DT_DETLL_MVNTO_CAJA.COD_BCO_DTN%TYPE;     --CHAR (11);
      v_FEC_PROC   DATE := TRUNC (SYSDATE);
   BEGIN
      v_COD_DIV := p_COD_DIV;
      v_COD_BCO := p_COD_BCO;

      IF v_COD_DIV = PKG_PAB_CONSTANTES.V_STR_DVI_CLP
      THEN
         BEGIN
            SELECT   SUM (IMP_OPE)
              INTO   p_SDO_EGRE
              FROM   PABS_DT_DETLL_MVNTO_CAJA CJA
             WHERE       CJA.FLG_ING_EGR = PKG_PAB_CONSTANTES.V_FLG_EGR    --0
                     AND                 --CJA.NUM_CTA_CTE = p_NUM_CTA_CTE AND
                        CJA.COD_DVI = v_COD_DIV
                     AND                    -- CJA.COD_BCO_ORG = v_COD_BCO AND
                        CJA.FEC_VTA = p_FEC_VAL;
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               err_code := SQLCODE;
               err_msg :=
                  SUBSTR (SQLERRM, 1, 300)
                  || ' No se pudo encontrar datos en PABS_DT_DETLL_MVNTO_CAJA EGRE: '
                  || p_NUM_CTA_CTE
                  || '   '
                  || p_FEC_VAL;



               PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                   v_NOM_PCK,
                                                   err_msg,
                                                   v_NOM_SP);
               p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            WHEN OTHERS
            THEN
               err_code := SQLCODE;
               err_msg := SUBSTR (SQLERRM, 1, 300);
               PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                   v_NOM_PCK,
                                                   err_msg,
                                                   v_NOM_SP);
               p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
               p_s_mensaje := err_code || '-' || err_msg;
               RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
         END;

         BEGIN
            SELECT   SUM (IMP_OPE)
              INTO   p_SDO_ING
              FROM   PABS_DT_DETLL_MVNTO_CAJA CJA
             WHERE       CJA.FLG_ING_EGR = PKG_PAB_CONSTANTES.V_FLG_ING    --1
                     AND                 --CJA.NUM_CTA_CTE = p_NUM_CTA_CTE AND
                        CJA.COD_DVI = v_COD_DIV
                     AND                     --CJA.COD_BCO_DTN = v_COD_BCO AND
                        CJA.FEC_VTA = p_FEC_VAL;
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               err_code := SQLCODE;
               err_msg :=
                  SUBSTR (SQLERRM, 1, 300)
                  || ' No se pudo encontrar datos en PABS_DT_DETLL_MVNTO_CAJA ING'
                  || p_NUM_CTA_CTE
                  || '   '
                  || p_FEC_VAL;
               PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                   v_NOM_PCK,
                                                   err_msg,
                                                   v_NOM_SP);
               p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            WHEN OTHERS
            THEN
               err_code := SQLCODE;
               err_msg := SUBSTR (SQLERRM, 1, 300);
               PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                   v_NOM_PCK,
                                                   err_msg,
                                                   v_NOM_SP);
               p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
               p_s_mensaje := err_code || '-' || err_msg;
               RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
         END;

         BEGIN
            SELECT   SUM (IMP_OPE)
              INTO   p_SDO_TOT
              FROM   PABS_DT_DETLL_MVNTO_CAJA CJA
             WHERE                       --CJA.NUM_CTA_CTE = p_NUM_CTA_CTE AND
                  CJA.COD_DVI = v_COD_DIV;                               --AND
         --(CJA.COD_BCO_DTN = v_COD_BCO OR CJA.COD_BCO_ORG = v_COD_BCO ); -- INGRESO ES DESTRINO COD_BCO_DTN



         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               err_code := SQLCODE;
               err_msg :=
                  SUBSTR (SQLERRM, 1, 300)
                  || ' No se pudo encontrar datos en PABS_DT_DETLL_MVNTO_CAJA TOT';
               PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                   v_NOM_PCK,
                                                   err_msg,
                                                   v_NOM_SP);
               p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            WHEN OTHERS
            THEN
               err_code := SQLCODE;
               err_msg := SUBSTR (SQLERRM, 1, 300);
               PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                   v_NOM_PCK,
                                                   err_msg,
                                                   v_NOM_SP);
               p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
               p_s_mensaje := err_code || '-' || err_msg;
               RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
         END;
      ELSE                                               ---MOneda extranjera.
         BEGIN
            SELECT   SUM (IMP_OPE)
              INTO   p_SDO_EGRE
              FROM   PABS_DT_DETLL_MVNTO_CAJA CJA
             WHERE       CJA.FLG_ING_EGR = PKG_PAB_CONSTANTES.V_FLG_EGR  --  0
                     AND CJA.NUM_CTA_CTE = p_NUM_CTA_CTE
                     AND CJA.COD_DVI = v_COD_DIV
                     --AND CJA.COD_BCO_ORG = v_COD_BCO
                     AND CJA.COD_BCO_DTN = v_COD_BCO
                     AND CJA.FEC_VTA = p_FEC_VAL;
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               err_code := SQLCODE;
               err_msg :=
                  SUBSTR (SQLERRM, 1, 300)
                  || ' No se pudo encontrar datos en PABS_DT_DETLL_MVNTO_CAJA EGRE: '
                  || p_NUM_CTA_CTE
                  || '   '
                  || p_FEC_VAL;


               PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                   v_NOM_PCK,
                                                   err_msg,
                                                   v_NOM_SP);
               p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            WHEN OTHERS
            THEN
               err_code := SQLCODE;
               err_msg := SUBSTR (SQLERRM, 1, 300);
               PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                   v_NOM_PCK,
                                                   err_msg,
                                                   v_NOM_SP);
               p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
               p_s_mensaje := err_code || '-' || err_msg;
               RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
         END;

         BEGIN
            SELECT   SUM (IMP_OPE)
              INTO   p_SDO_ING
              FROM   PABS_DT_DETLL_MVNTO_CAJA CJA
             WHERE       CJA.FLG_ING_EGR = PKG_PAB_CONSTANTES.V_FLG_ING --   1
                     AND CJA.NUM_CTA_CTE = p_NUM_CTA_CTE
                     AND CJA.COD_DVI = v_COD_DIV
                     --AND CJA.COD_BCO_DTN = v_COD_BCO
                     AND CJA.COD_BCO_ORG = v_COD_BCO
                     AND CJA.FEC_VTA = p_FEC_VAL;
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               err_code := SQLCODE;
               err_msg :=
                  SUBSTR (SQLERRM, 1, 300)
                  || ' No se pudo encontrar datos en PABS_DT_DETLL_MVNTO_CAJA ING'
                  || p_NUM_CTA_CTE
                  || '   '
                  || p_FEC_VAL;
               PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                   v_NOM_PCK,
                                                   err_msg,
                                                   v_NOM_SP);
               p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            WHEN OTHERS
            THEN
               err_code := SQLCODE;
               err_msg := SUBSTR (SQLERRM, 1, 300);
               PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                   v_NOM_PCK,
                                                   err_msg,
                                                   v_NOM_SP);
               p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
               p_s_mensaje := err_code || '-' || err_msg;
               RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
         END;

         BEGIN
            SELECT   SUM (IMP_OPE)
              INTO   p_SDO_TOT
              FROM   PABS_DT_DETLL_MVNTO_CAJA CJA
             WHERE       CJA.NUM_CTA_CTE = p_NUM_CTA_CTE
                     AND CJA.FEC_VTA >= v_FEC_PROC
                     AND CJA.COD_DVI = v_COD_DIV
                     AND (CJA.COD_BCO_DTN = v_COD_BCO
                          OR CJA.COD_BCO_ORG = v_COD_BCO); -- INGRESO ES DESTRINO COD_BCO_DTN
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               err_code := SQLCODE;
               err_msg :=
                  SUBSTR (SQLERRM, 1, 300)
                  || ' No se pudo encontrar datos en PABS_DT_DETLL_MVNTO_CAJA TOT';
               PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                   v_NOM_PCK,
                                                   err_msg,
                                                   v_NOM_SP);
               p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            WHEN OTHERS
            THEN
               err_code := SQLCODE;
               err_msg := SUBSTR (SQLERRM, 1, 300);
               PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                   v_NOM_PCK,
                                                   err_msg,
                                                   v_NOM_SP);
               p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
               p_s_mensaje := err_code || '-' || err_msg;
               RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
         END;
      END IF;

      p_ERROR := PKG_PAB_CONSTANTES.v_OK;
   --                                                                                                                                                                                                                                                                                                                                                                                                                                --Procedimiento ejecutado de forma correcta
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg :=
            SUBSTR (SQLERRM, 1, 300)
            || ' No se pudo encontrar datos en PABS_DT_DETLL_MVNTO_CAJA';
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   --
   END SP_PAB_CNA_SLD_CJA;

   --************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_ACT_CRTLA_MX
   -- Objetivo: Procedimiento que actualiza saldo de cartola MT940 MT950.
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_CBCRA_CRTLA, PABS_DT_DETLL_CRTLA, PABS_DT_DETLL_SALDO_CAJA_MN_MX
   -- Fecha: 17/10/17
   -- Autor: Santander
   -- Input:
   -- p_NUM_CTA_CTE -> Numero de cuenta corriente
   -- p_COD_BCO_ORG -> Codigo banco
   -- p_FEC_ING_CTL -> fecha de ingreso
   -- p_COD_DVI ->  Codigo Divisa
   -- Output:
   -- p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE Sp_PAB_ACT_CRTLA_MX (p_NUM_CTA_CTE   IN     VARCHAR2,
                                  p_COD_BCO_ORG   IN     VARCHAR2,
                                  p_FEC_ING_CTL   IN     DATE,
                                  p_COD_DVI       IN     VARCHAR2,
                                  p_ERROR            OUT NUMBER)
   IS
      v_NOM_SP            VARCHAR2 (30) := 'Sp_PAB_ACT_CRTLA_MX';
      v_IMP_EGR_FIL       NUMBER (15, 2);
      v_IMP_ING_FIL       NUMBER (15, 2);
      v_SDO_IN            NUMBER (15, 2);
      v_SDO_FNL           NUMBER (15, 2);
      v_NUM_CTA           VARCHAR2 (35);
      v_NUM_CTL           NUMBER (12);
      v_SDL_DEB           NUMBER (15, 2);
      v_SDL_CRE           NUMBER (15, 2);
      v_COD_DVI           CHAR (3);
      v_COD_BCO_ORG       CHAR (11);
      v_FEC_INI           DATE;
      v_NUM_CTA_CTE       VARCHAR2 (35);   -- SOLUCION A PROBLEMA DE CARTOLAS.
      --- PARA ACTUALIZAR SALDOS
      v_FEC_INS_OPE       DATE := TRUNC (SYSDATE);        -- FECHA DEL PROCESO
      v_FEC_48HS          DATE := TRUNC (SYSDATE + 2);
      v_FEC_24HS          DATE := TRUNC (SYSDATE + 1);
      v_FEC_48HS_VAL      NUMBER;
      v_FEC_24HS_VAL      NUMBER;
      v_IMP_ING_DIA       NUMBER (15, 2);
      v_IMP_EGR_DIA       NUMBER (15, 2);
      v_IMP_RAL_AOS       NUMBER (15, 2);
      v_IMP_ING_24        NUMBER (15, 2);
      v_IMP_EGR_24        NUMBER (15, 2);
      v_IMP_TOT_24        NUMBER (15, 2);
      v_IMP_ING_48        NUMBER (15, 2);
      v_IMP_EGR_48        NUMBER (15, 2);
      v_IMP_TOT_48        NUMBER (15, 2);
      v_VAL_FEC           NUMBER := 0;
      v_IND_HABIL         NUMBER := 0;
      v_FLG_FECHA         NUMBER := 0;
      v_COD_BCO_ORG_SAL   CHAR (8);
      v_COD_BCO_ORG_COM   CHAR (8);

      v_IMP_RAL_AOS_SAL   NUMBER (15, 2);
      v_IMP_TOT_24_SAL    NUMBER (15, 2);
      v_IMP_TOT_48_SAL    NUMBER (15, 2);
      v_IMP_APR_AOS_SAL   NUMBER (15, 2);
      v_NUM_REF_SWF_CTL   CHAR (16);
   BEGIN
      v_COD_BCO_ORG_COM := p_COD_BCO_ORG;
      v_COD_BCO_ORG := SUBSTR (p_COD_BCO_ORG, 0, 8); -- debemos, para cartolas, solo buscar por 8 caracteres de BIC, PERO CON ESPACIOS 11 CHR.
      v_COD_BCO_ORG_SAL := SUBSTR (p_COD_BCO_ORG, 0, 8);       -- SOLO 8 CHAR.
      v_FEC_INI := TRUNC (p_FEC_ING_CTL);
      v_COD_DVI := TO_CHAR (p_COD_DVI);

      v_NUM_CTA_CTE := TRIM (LEADING 0 FROM p_NUM_CTA_CTE);

      err_msg :=
            'Consultando saldos cartola y Banco:'
         || LENGTH (v_COD_BCO_ORG)
         || ' Cuenta:'
         || v_NUM_CTA_CTE
         || ' Fecha:'
         || TO_CHAR (v_FEC_INI, 'DD-MM-YYYY')
         || 'COD_DVI'
         || v_COD_DVI;
      PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                          v_NOM_PCK,
                                          err_msg,
                                          v_NOM_SP);

      --Consultamos saldos por cartola y banco (cabecera)

      BEGIN
         SELECT   CRTL.IMP_INI_CTL,
                  CRTL.IMP_FNL_CTL,
                  CRTL.NUM_CTA_CTE,
                  CRTL.NUM_FOL_CTL,
                  CRTL.COD_DVI,
                  CRTL.FEC_ING_CTL,
                  CRTL.NUM_REF_SWF_CTL
           INTO   v_SDO_IN,
                  v_SDO_FNL,
                  v_NUM_CTA,
                  v_NUM_CTL,
                  v_COD_DVI,
                  v_FEC_INI,
                  v_NUM_REF_SWF_CTL
           FROM   PABS_DT_CBCRA_CRTLA CRTL
          WHERE       CRTL.NUM_CTA_CTE = v_NUM_CTA_CTE
                  AND CRTL.COD_BCO_ORG = v_COD_BCO_ORG
                  AND CRTL.FEC_ING_CTL = v_FEC_INI
                  AND CRTL.COD_DVI = v_COD_DVI;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            err_code := SQLCODE;
            err_msg :=
                  --        'No se pudo obtener la informacion de la cuenta:'
                  p_NUM_CTA_CTE
               || ' v_COD_BCO_ORG:'
               || v_COD_BCO_ORG
               || ' v_COD_DVI: '
               || v_COD_DVI
               || 'FECHA '
               || TO_CHAR (v_FEC_INI, 'DD-MM-YYYY')
               || ' ';
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            RAISE;
         WHEN OTHERS
         THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            p_s_mensaje := err_code || '-' || err_msg;
            RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
      END;

      err_msg := 'Consultando saldos totales egreso';
      PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                          v_NOM_PCK,
                                          err_msg,
                                          v_NOM_SP);

      --Rescatamos saldos totales de egresos
      BEGIN
         SELECT   SUM (DETLL.IMP_TRN)
           INTO   v_SDL_DEB
           FROM   PABS_DT_DETLL_CRTLA DETLL
          WHERE       DETLL.FLG_DEB_CRE = PKG_PAB_CONSTANTES.V_FLG_EGR
                  AND DETLL.NUM_CTA_CTE = v_NUM_CTA_CTE
                  AND DETLL.COD_BCO_ORG = v_COD_BCO_ORG
                  AND DETLL.FEC_ING_CTL = v_FEC_INI
                  AND DETLL.COD_DVI = v_COD_DVI
                  AND DETLL.NUM_REF_ITN_CTA = v_NUM_REF_SWF_CTL;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            err_code := SQLCODE;
            err_msg := 'No se pudo obtener la suma del detalle Egreso';
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            RAISE;
         WHEN OTHERS
         THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            p_s_mensaje := err_code || '-' || err_msg;
            RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
      END;

      BEGIN
         err_msg := 'Consultando saldos totales ingreso';
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);

         SELECT   SUM (DETLL.IMP_TRN)
           INTO   v_SDL_CRE
           FROM   PABS_DT_DETLL_CRTLA DETLL
          WHERE       DETLL.FLG_DEB_CRE = PKG_PAB_CONSTANTES.V_FLG_ING
                  AND DETLL.NUM_CTA_CTE = v_NUM_CTA_CTE
                  AND DETLL.COD_BCO_ORG = v_COD_BCO_ORG
                  AND DETLL.FEC_ING_CTL = v_FEC_INI
                  AND DETLL.COD_DVI = v_COD_DVI
                  AND DETLL.NUM_REF_ITN_CTA = v_NUM_REF_SWF_CTL;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            err_code := SQLCODE;
            err_msg := 'No se pudo obtener la informacion detalle de Ingreso:';
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            RAISE;
         WHEN OTHERS
         THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            p_s_mensaje := err_code || '-' || err_msg;
            RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
      END;

      err_msg := 'Actualizando Saldos Cabecera Cartola';
      PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                          v_NOM_PCK,
                                          err_msg,
                                          v_NOM_SP);

      -- ACTUALIZAMOS LOS SALDOS INGRESOS EGRESOS TABLA CARTOLA
      UPDATE   PABS_DT_CBCRA_CRTLA
         SET   IMP_ING_CTL = NVL (v_SDL_CRE, 0),
               IMP_EGR_CTL = NVL (v_SDL_DEB, 0)
       WHERE       NUM_CTA_CTE = v_NUM_CTA_CTE                 --p_NUM_CTA_CTE
               AND COD_BCO_ORG = v_COD_BCO_ORG
               AND FEC_ING_CTL = v_FEC_INI
               AND COD_DVI = v_COD_DVI;


      IF v_COD_DVI = PKG_PAB_CONSTANTES.V_STR_DVI_CLP
      THEN
         err_msg := 'Actualizando Saldos MN PABS_DT_DETLL_SALDO_CAJA_MN_MX';
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);

         UPDATE   PABS_DT_DETLL_SALDO_CAJA_MN_MX
            SET   FEC_ING_CTL = v_FEC_INI,
                  IMP_INI_CTL = NVL (v_SDO_IN, 0),
                  IMP_ING_CTL = NVL (v_SDL_CRE, 0),
                  IMP_EGR_CTL = NVL (v_SDL_DEB, 0),
                  IMP_FNL_CTL = NVL (v_SDO_FNL, 0),
                  IMP_APR_AOS = NVL (v_SDO_FNL, 0),
                  NUM_FOL_CTL = NVL (v_NUM_CTL, 0),
                  IMP_TOT_FNL_DIA = NVL (v_SDO_FNL, 0),
                  IMP_RAL_AOS = NVL (v_SDO_FNL, 0)
          WHERE       COD_BCO_ORG = v_COD_BCO_ORG
                  AND NUM_CTA_CTE = v_NUM_CTA_CTE
                  AND                                          --v_NUM_CTA AND
                     COD_DVI = v_COD_DVI
                  AND FEC_ING_CTL <= v_FEC_INS_OPE;
      ELSE
         UPDATE   PABS_DT_DETLL_SALDO_CAJA_MN_MX
            SET   FEC_ING_CTL = v_FEC_INI,
                  IMP_INI_CTL = NVL (v_SDO_IN, 0),
                  IMP_ING_CTL = NVL (v_SDL_CRE, 0),
                  IMP_EGR_CTL = NVL (v_SDL_DEB, 0),
                  IMP_FNL_CTL = NVL (v_SDO_FNL, 0),
                  IMP_APR_AOS = NVL (v_SDO_FNL, 0),
                  NUM_FOL_CTL = NVL (v_NUM_CTL, 0)
          WHERE       SUBSTR (COD_BCO_ORG, 0, 8) = v_COD_BCO_ORG_SAL --v_COD_BCO_ORG
                  AND NUM_CTA_CTE = v_NUM_CTA_CTE
                  AND                                          --v_NUM_CTA AND
                     COD_DVI = v_COD_DVI
                  AND FEC_ING_CTL <= v_FEC_INS_OPE;
      END IF;

      IF v_COD_DVI <> PKG_PAB_CONSTANTES.V_STR_DVI_CLP
      THEN                            -- PROCESO DE ACTUALIZACION DE SALDOS MX
         err_msg := 'IF CODIGO DIVISA Y FECHAS';
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);

         PKG_PAB_CAJA.Sp_PAB_RNO_FEC_PYN (v_COD_BCO_ORG_COM,
                                          v_NUM_CTA_CTE,
                                          v_COD_DVI,
                                          v_FEC_INS_OPE,
                                          v_FEC_24HS,
                                          v_FEC_48HS,
                                          p_ERROR);

         err_msg :=
               'FECHAS'
            || v_FEC_INS_OPE
            || ' 24'
            || v_FEC_24HS
            || '48 '
            || v_FEC_48HS;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);

         v_FEC_48HS_VAL :=
            TO_CHAR (v_FEC_48HS, 'D', 'NLS_DATE_LANGUAGE=SPANISH');
         v_FEC_24HS_VAL :=
            TO_CHAR (v_FEC_24HS, 'D', 'NLS_DATE_LANGUAGE=SPANISH');

         IF ( (v_FEC_48HS_VAL = 7) OR (v_FEC_48HS_VAL = 1))
         THEN
            v_FEC_48HS := v_FEC_48HS + 2;

            err_msg :=
               'fecha nueva 48' || v_FEC_48HS || 'dia ' || v_FEC_48HS_VAL;
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
         END IF;

         IF (v_FEC_24HS_VAL = 7)
         THEN
            v_FEC_24HS := v_FEC_24HS + 2;

            err_msg :=
               'fecha nueva 24' || v_FEC_24HS || 'dia ' || v_FEC_24HS_VAL;
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
         END IF;


         /*?control de fechas */


         --Se rescata saldos intradia
         PKG_PAB_CAJA.SP_PAB_CNA_SLD_CJA (v_FEC_INS_OPE,
                                          v_NUM_CTA_CTE,
                                          v_COD_DVI,
                                          v_COD_BCO_ORG_SAL,
                                          v_IMP_ING_DIA,
                                          v_IMP_EGR_DIA,
                                          v_IMP_RAL_AOS,
                                          p_ERROR);

         UPDATE   PABS_DT_DETLL_SALDO_CAJA_MN_MX
            SET   IMP_APR_AOS = v_SDO_FNL,
                  IMP_ING_CPR = (NVL (v_IMP_ING_DIA, 0)), --INGRESOS COMPROMETIDOS
                  IMP_EGR_CPR = (NVL (v_IMP_EGR_DIA, 0)), -- EGRESOS COMPROMETIDOS.
                  IMP_RAL_AOS = v_SDO_FNL,
                  IMP_TOT_FNL_DIA =
                     (  NVL (v_SDO_FNL, 0)
                      + NVL (v_IMP_ING_DIA, 0)
                      - NVL (v_IMP_EGR_DIA, 0))         --  TOTAL COMPROMETIDO
          WHERE       SUBSTR (COD_BCO_ORG, 0, 8) = v_COD_BCO_ORG_SAL
                  AND NUM_CTA_CTE = v_NUM_CTA_CTE
                  AND COD_DVI = v_COD_DVI;

         BEGIN
            SELECT   IMP_TOT_FNL_DIA
              INTO   v_IMP_RAL_AOS_SAL
              FROM   PABS_DT_DETLL_SALDO_CAJA_MN_MX SALDO
             WHERE       SALDO.NUM_CTA_CTE = v_NUM_CTA_CTE
                     AND SALDO.COD_DVI = v_COD_DVI
                     AND SUBSTR (SALDO.COD_BCO_ORG, 0, 8) = v_COD_BCO_ORG_SAL; -- COMO ES CARTOLA.
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               err_code := SQLCODE;
               err_msg :=
                     'No se encuentra registro para cuenta '
                  || v_NUM_CTA_CTE
                  || ' - divisa '
                  || v_COD_DVI
                  || '- Banco '
                  || v_COD_BCO_ORG;
               PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                   v_NOM_PCK,
                                                   err_msg,
                                                   v_NOM_SP);
               p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            WHEN OTHERS
            THEN
               err_code := SQLCODE;
               err_msg := SUBSTR (SQLERRM, 1, 300);
               p_s_mensaje := err_code || '-' || err_msg;
               PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                   v_NOM_PCK,
                                                   err_msg,
                                                   v_NOM_SP);
               p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
               RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
         END;

         --Se rescata saldos 24 horas
         PKG_PAB_CAJA.SP_PAB_CNA_SLD_CJA (v_FEC_24HS,
                                          v_NUM_CTA_CTE,
                                          v_COD_DVI,
                                          v_COD_BCO_ORG_SAL,
                                          v_IMP_ING_24,
                                          v_IMP_EGR_24,
                                          v_IMP_TOT_24,
                                          p_ERROR);

         -- ACTUALIZAMOS SALDOS CUENTA.

         UPDATE   PABS_DT_DETLL_SALDO_CAJA_MN_MX
            SET   IMP_ING_24 = (NVL (v_IMP_ING_24, 0)),
                  IMP_EGR_24 = (NVL (v_IMP_EGR_24, 0)),
                  IMP_TOT_24 =
                     (  NVL (IMP_TOT_FNL_DIA, 0)
                      + NVL (v_IMP_ING_24, 0)
                      - NVL (v_IMP_EGR_24, 0))
          WHERE       SUBSTR (COD_BCO_ORG, 0, 8) = v_COD_BCO_ORG_SAL
                  AND NUM_CTA_CTE = v_NUM_CTA_CTE
                  AND COD_DVI = v_COD_DVI;


         --Se rescata saldos 48 horas.
         PKG_PAB_CAJA.SP_PAB_CNA_SLD_CJA (v_FEC_48HS,
                                          v_NUM_CTA_CTE,
                                          v_COD_DVI,
                                          v_COD_BCO_ORG_SAL,
                                          v_IMP_ING_48,
                                          v_IMP_EGR_48,
                                          v_IMP_TOT_48,
                                          p_ERROR);


         BEGIN
            SELECT   IMP_TOT_24
              INTO   v_IMP_TOT_24_SAL
              FROM   PABS_DT_DETLL_SALDO_CAJA_MN_MX SALDO
             WHERE       SALDO.NUM_CTA_CTE = v_NUM_CTA_CTE
                     AND SALDO.COD_DVI = v_COD_DVI
                     AND SUBSTR (SALDO.COD_BCO_ORG, 0, 8) = v_COD_BCO_ORG_SAL;
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               err_code := SQLCODE;
               err_msg :=
                     'No se encuentra registro para cuenta '
                  || v_NUM_CTA_CTE
                  || ' - divisa '
                  || v_COD_DVI
                  || '- Banco '
                  || v_COD_BCO_ORG;
               PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                   v_NOM_PCK,
                                                   err_msg,
                                                   v_NOM_SP);
               p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            WHEN OTHERS
            THEN
               err_code := SQLCODE;
               err_msg := SUBSTR (SQLERRM, 1, 300);
               p_s_mensaje := err_code || '-' || err_msg;
               PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                   v_NOM_PCK,
                                                   err_msg,
                                                   v_NOM_SP);
               p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
               RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
         END;

         UPDATE   PABS_DT_DETLL_SALDO_CAJA_MN_MX
            SET   IMP_ING_48 = (NVL (v_IMP_ING_48, 0)),
                  IMP_EGR_48 = (NVL (v_IMP_EGR_48, 0)),
                  IMP_TOT_48 =
                     (  NVL (v_IMP_TOT_24_SAL, 0)
                      + NVL (v_IMP_ING_48, 0)
                      - NVL (v_IMP_EGR_48, 0))
          WHERE       SUBSTR (COD_BCO_ORG, 0, 8) = v_COD_BCO_ORG_SAL
                  AND NUM_CTA_CTE = v_NUM_CTA_CTE
                  AND COD_DVI = v_COD_DVI;
      END IF;


      p_ERROR := PKG_PAB_CONSTANTES.v_OK;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END Sp_PAB_ACT_CRTLA_MX;

   --************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_INS_SLD_CRPSL
   -- Objetivo: Procedimiento que INSERTA registro saldo corresponsales en tabla saldo
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas:
   -- Fecha: 17/10/17
   -- Autor: Santander
   -- Input:
   -- p_FEC_ING_CTL  FECHA INGRESO CARTOLA
   -- p_COD_BCO_ORG  BANCO CORRESPONSAL
   -- p_NUM_CTA_CTE  NUMERO DE CUENTA CORRESPONSAL
   -- p_COD_DVI      MONEDA ASOCIADA A CUENTA CORRESPONSAL
   -- Output:
   -- p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE Sp_PAB_INS_SLD_CRPSL (p_COD_BCO_ORG   IN     CHAR,
                                   p_NUM_CTA_CTE   IN     VARCHAR2,
                                   p_COD_DVI       IN     CHAR,
                                   p_ERROR            OUT NUMBER)
   IS
      v_NOM_SP        VARCHAR2 (30) := 'Sp_PAB_ACT_CRPSL';
      v_COD_DVI       CHAR (3);
      v_FEC_PROC      DATE := TRUNC (SYSDATE);
      v_COD_BCO_ORG   CHAR (11);
   BEGIN
      v_COD_BCO_ORG := p_COD_BCO_ORG;

      v_COD_DVI := p_COD_DVI;

      --Consultamos saldos por cartola y banco (cabecera)


      INSERT INTO PABS_DT_DETLL_SALDO_CAJA_MN_MX (FEC_ING_CTL,
                                                  COD_BCO_ORG,
                                                  NUM_CTA_CTE,
                                                  COD_DVI,
                                                  NUM_FOL_CTL,
                                                  IMP_INI_CTL,
                                                  IMP_ING_CTL,
                                                  IMP_EGR_CTL,
                                                  IMP_FNL_CTL,
                                                  IMP_APR_AOS,
                                                  IMP_ING_DIA,
                                                  IMP_EGR_DIA,
                                                  IMP_ABN_BCT,
                                                  IMP_CGO_BCT,
                                                  IMP_RAL_AOS,
                                                  IMP_GRT_CBA,
                                                  IMP_ING_CBA,
                                                  IMP_EGR_CBA,
                                                  IMP_PSC_CBA,
                                                  IMP_ING_CPR,
                                                  IMP_EGR_CPR,
                                                  IMP_TOT_FNL_DIA,
                                                  IMP_ING_SNF,
                                                  IMP_EGR_SNF,
                                                  IMP_NET_SNF,
                                                  IMP_OER_503,
                                                  IMP_OER_103,
                                                  IMP_ING_24,
                                                  IMP_EGR_24,
                                                  IMP_TOT_24,
                                                  IMP_ING_48,
                                                  IMP_EGR_48,
                                                  IMP_TOT_48,
                                                  IMP_ING_OVR,
                                                  IMP_EGR_OVR,
                                                  IMP_TOT_OVR,
                                                  IMP_ING_103,
                                                  IMP_ING_200,
                                                  IMP_ING_202,
                                                  IMP_ING_205,
                                                  IMP_EGR_103,
                                                  IMP_EGR_200,
                                                  IMP_EGR_202,
                                                  IMP_EGR_205,
                                                  IMP_CGO_900,
                                                  IMP_ABN_910,
                                                  IMP_TOT_SWF)
        VALUES   (v_FEC_PROC,
                  v_COD_BCO_ORG,
                  p_NUM_CTA_CTE,
                  v_COD_DVI,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0);

      COMMIT;
      P_ERROR := PKG_PAB_CONSTANTES.V_OK;
   EXCEPTION
      WHEN DUP_VAL_ON_INDEX
      THEN
         err_code := SQLCODE;
         err_msg := '';
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END Sp_PAB_INS_SLD_CRPSL;


   --************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_ELI_SLD_CRPSL
   -- Objetivo: Procedimiento que ELIMINA registro saldo corresponsales en tabla saldo
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas:
   -- Fecha: 17/10/17
   -- Autor: Santander
   -- Input:
   -- p_FEC_ING_CTL  FECHA INGRESO CARTOLA
   -- p_COD_BCO_ORG  BANCO CORRESPONSAL
   -- p_NUM_CTA_CTE  NUMERO DE CUENTA CORRESPONSAL
   -- p_COD_DVI      MONEDA ASOCIADA A CUENTA CORRESPONSAL
   -- Output:
   -- p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE Sp_PAB_ELI_SLD_CRPSL (p_COD_BCO_ORG   IN     CHAR,
                                   p_NUM_CTA_CTE   IN     VARCHAR2,
                                   p_COD_DVI       IN     CHAR,
                                   p_ERROR            OUT NUMBER)
   IS
      v_NOM_SP        VARCHAR2 (30) := 'Sp_PAB_ACT_CRPSL';
      v_COD_DVI       CHAR (3);
      v_FEC_PROC      DATE := TRUNC (SYSDATE);
      v_COD_BCO_ORG   CHAR (11);
   BEGIN
      v_COD_BCO_ORG := p_COD_BCO_ORG;

      v_COD_DVI := p_COD_DVI;

      --Consultamos saldos por cartola y banco (cabecera)
      err_msg := v_COD_BCO_ORG || ' ' || p_NUM_CTA_CTE || ' ' || v_COD_DVI;
      PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                          v_NOM_PCK,
                                          err_msg,
                                          v_NOM_SP);

      DELETE FROM   PABS_DT_DETLL_SALDO_CAJA_MN_MX
            WHERE       COD_BCO_ORG = v_COD_BCO_ORG
                    AND NUM_CTA_CTE = p_NUM_CTA_CTE
                    AND COD_DVI = v_COD_DVI
                    AND FEC_ING_CTL <= v_FEC_PROC;

      COMMIT;
      P_ERROR := PKG_PAB_CONSTANTES.V_OK;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg := '';
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END Sp_PAB_ELI_SLD_CRPSL;

   --************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_RNO_FEC_PYN
   -- Objetivo: Procedimiento que retorna las fechas de proyeccion para MX
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas:
   -- Fecha: 19/03/18
   -- Autor: SANTANDER
   -- Input:
   -- p_FEC_ING_CTL  FECHA INGRESO CARTOLA
   -- p_COD_BCO_ORG  BANCO CORRESPONSAL
   -- p_NUM_CTA_CTE  NUMERO DE CUENTA CORRESPONSAL
   -- p_COD_DVI      MONEDA ASOCIADA A CUENTA CORRESPONSAL
   -- Output:
   -- p_FEC_DIA      RETORNO DE FECHA ACTUAL
   -- p_FEC_24H      RETORNO DE FECHA 24h
   -- p_FEC_48H      RETORNO DE FECHA 48h
   -- p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE Sp_PAB_RNO_FEC_PYN (p_COD_BCO_ORG   IN     CHAR,
                                 p_NUM_CTA_CTE   IN     VARCHAR2,
                                 p_COD_DVI       IN     CHAR,
                                 p_FEC_DIA          OUT DATE,
                                 p_FEC_24H          OUT DATE,
                                 p_FEC_48H          OUT DATE,
                                 p_ERROR            OUT NUMBER)
   IS
      v_NOM_SP            VARCHAR2 (30) := 'Sp_PAB_RNO_FEC_PYN';
      v_COD_DVI           CHAR (3);
      v_FEC_PROC          DATE;
      v_COD_BCO_ORG       CHAR (11);
      v_COD_BCO_ORG_CAR   CHAR (8);
      v_NUM_CTA_CTE       VARCHAR2 (30);
   BEGIN
      v_COD_BCO_ORG := p_COD_BCO_ORG;

      v_COD_DVI := p_COD_DVI;

      v_NUM_CTA_CTE := p_NUM_CTA_CTE;

      err_msg :=
            'BANCO'
         || v_COD_BCO_ORG
         || ' DIVISA'
         || v_COD_DVI
         || 'CUENTA CORRIENTE '
         || v_NUM_CTA_CTE;
      PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                          v_NOM_PCK,
                                          err_msg,
                                          v_NOM_SP);

      SELECT   TRUNC (FEC_ING_CTL)
        INTO   v_FEC_PROC
        FROM   PABS_DT_DETLL_SALDO_CAJA_MN_MX
       WHERE       TRIM (COD_BCO_ORG) = TRIM (v_COD_BCO_ORG)
               AND NUM_CTA_CTE = v_NUM_CTA_CTE
               AND COD_DVI = v_COD_DVI;


      IF v_FEC_PROC = TRUNC (SYSDATE)
      THEN
         p_FEC_DIA := TRUNC (v_FEC_PROC + 1);
         p_FEC_24H := TRUNC (v_FEC_PROC + 2);
         p_FEC_48H := TRUNC (v_FEC_PROC + 3);

         err_msg :=
               'FECHA'
            || p_FEC_DIA
            || ' FECHA 24'
            || p_FEC_24H
            || ' FECHA 48 '
            || p_FEC_48H;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
      ELSE
         IF v_FEC_PROC > TRUNC (SYSDATE)
         THEN
            /* SI LA FECHA DE LA CARTOLA IGUAL AL DIA, EL INTRADIA ES +1*/
            p_FEC_DIA := TRUNC (v_FEC_PROC + 1);
            p_FEC_24H := TRUNC (v_FEC_PROC + 2);
            p_FEC_48H := TRUNC (v_FEC_PROC + 3);

            err_msg :=
                  'FECHA'
               || p_FEC_DIA
               || ' FECHA 24'
               || p_FEC_24H
               || ' FECHA 48 '
               || p_FEC_48H;
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
         ELSE
            p_FEC_DIA := TRUNC (SYSDATE);
            p_FEC_24H := TRUNC (SYSDATE + 1);
            p_FEC_48H := TRUNC (SYSDATE + 2);

            err_msg :=
                  'FECHA'
               || p_FEC_DIA
               || ' FECHA 24'
               || p_FEC_24H
               || ' FECHA 48 '
               || p_FEC_48H;
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
         END IF;
      END IF;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         p_FEC_DIA := TRUNC (SYSDATE);
         p_FEC_24H := TRUNC (SYSDATE + 1);
         p_FEC_48H := TRUNC (SYSDATE + 2);

         err_msg :=
               'NO DATA, SE PROCESA FECHA FECHA'
            || p_FEC_DIA
            || ' FECHA 24'
            || p_FEC_24H
            || ' FECHA 48 '
            || p_FEC_48H;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END Sp_PAB_RNO_FEC_PYN;

   --******************************************************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_ACT_SALDO_MN_MX
   -- Objetivo: PROCEDIMIENTO QUE REALIZA LAS ACTUALIZACIONES DE LOS SALDOS PARA LA CARGA DE PROYECCIONES  DE INGRESO Y  EL EGRESO.
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas:PABS_DT_DETLL_MVNTO_CAJA ,PABS_DT_DETLL_SALDO_CAJA_MN_MX
   -- Fecha: 22/08/2018
   -- Autor: JBARBOZA
   -- Input:  N/A.
   -- Output:
   -- p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --********************************************************************************************************************************
   PROCEDURE sp_pab_act_saldo_mn_mx (p_error OUT NUMBER)
   IS
      v_nom_sp            VARCHAR2 (30) := 'SP_PAB_ACT_SALDO_MN_MX';
      v_fec_ins_ope       DATE := TRUNC (SYSDATE);
      v_fec_48hs          DATE := TRUNC (SYSDATE + 2);
      v_fec_24hs          DATE := TRUNC (SYSDATE + 1);
      v_fec_48hs_val      NUMBER;
      v_fec_24hs_val      NUMBER;
      v_imp_egr_24        NUMBER (15, 2);
      v_imp_ing_dia       NUMBER (15, 2);
      v_imp_egr_dia       NUMBER (15, 2);
      v_imp_ral_aos       NUMBER (15, 2);
      v_imp_ing_24        NUMBER (15, 2);
      v_cod_bco_bcch      CHAR (11) := pkg_pab_constantes.V_BIC_BCCH;
      v_imp_apr_aos_sal   NUMBER (15, 2);
      v_imp_ral_aos_sal   NUMBER (15, 2);
      v_imp_tot_24_sal    NUMBER (15, 2);
      v_imp_tot_48_sal    NUMBER (15, 2);
      v_imp_tot_24        NUMBER (15, 2);
      v_imp_ing_48        NUMBER (15, 2);
      v_imp_egr_48        NUMBER (15, 2);
      v_imp_tot_48        NUMBER (15, 2);


      CURSOR c_mvnto_caja
      IS
           SELECT   num_cta_cte,
                    cod_dvi,
                    CASE
                       WHEN (flg_ing_egr = PKG_PAB_CONSTANTES.V_FLG_ING)
                       THEN
                          cod_bco_org                                --INGRESO
                       ELSE
                          cod_bco_dtn
                    END
                       cod_bco_dtn_org,
                    fec_vta
             FROM   pabs_dt_detll_mvnto_caja
            WHERE   fec_vta >= v_fec_ins_ope
         GROUP BY   num_cta_cte,
                    cod_dvi,
                    CASE
                       WHEN (flg_ing_egr = PKG_PAB_CONSTANTES.V_FLG_ING)
                       THEN
                          cod_bco_org
                       ELSE
                          cod_bco_dtn
                    END,
                    fec_vta;
   BEGIN
      FOR r_mvnto_caja IN c_mvnto_caja
      LOOP
         IF (r_mvnto_caja.cod_dvi = pkg_pab_constantes.v_str_dvi_clp)
         THEN
            -- INICIO DE CALCULO PARA MONEDA NACIONAL
            pkg_pab_caja.sp_pab_cna_sld_cja (v_fec_ins_ope,
                                             r_mvnto_caja.num_cta_cte,
                                             r_mvnto_caja.cod_dvi,
                                             r_mvnto_caja.cod_bco_dtn_org,
                                             v_imp_ing_dia,
                                             v_imp_egr_dia,
                                             v_imp_ral_aos,
                                             p_error);


            BEGIN
               --Actualizamos los saldos de intradia
               UPDATE   pabs_dt_detll_saldo_caja_mn_mx
                  SET   imp_ing_cpr = NVL (v_imp_ing_dia, 0),
                        imp_egr_cpr = NVL (v_imp_egr_dia, 0)
                WHERE   cod_bco_org = v_cod_bco_bcch --r_mvnto_caja.cod_bco_dtn_org
                        --AND num_cta_cte = r_mvnto_caja.num_cta_cte
                        AND cod_dvi = r_mvnto_caja.cod_dvi;
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  err_code := SQLCODE;
                  err_msg :=
                     SUBSTR (SQLERRM, 1, 300)
                     || ' Error al actualizar los saldos de ingreso y egreso para los siguiente datos : codigo banco '
                     || r_mvnto_caja.cod_bco_dtn_org
                     || ' cuenta '
                     || r_mvnto_caja.num_cta_cte
                     || ' divisa '
                     || r_mvnto_caja.cod_dvi;
                  PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                      v_NOM_PCK,
                                                      err_msg,
                                                      v_NOM_SP);
                  p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
                  RAISE;
               WHEN OTHERS
               THEN
                  err_code := SQLCODE;
                  err_msg := SUBSTR (SQLERRM, 1, 300);
                  p_s_mensaje := err_code || '-' || err_msg;
                  PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                      v_NOM_PCK,
                                                      err_msg,
                                                      v_NOM_SP);
                  p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
                  RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
            END;

            --Calcula el saldo final de la caja mn con los datos actuales.
            pkg_pab_caja_mn.sp_pab_cal_sldo_final_dia_mn (p_error);
         ELSE                             --- Se trabaja moneda extranjera !!!
            -- PROCESO DE ACTUALIZACION.

            BEGIN
               --Obtemos saldos
               SELECT   imp_apr_aos
                 INTO   v_imp_apr_aos_sal
                 FROM   pabs_dt_detll_saldo_caja_mn_mx saldo
                WHERE   saldo.cod_dvi = r_mvnto_caja.cod_dvi
                        AND saldo.cod_bco_org = r_mvnto_caja.cod_bco_dtn_org; --v_cod_bco_bcch;
            --fecha menor del dia
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  err_code := SQLCODE;
                  err_msg :=
                        'No se encuentra registro para cuenta '
                     || r_mvnto_caja.num_cta_cte
                     || ' - divisa '
                     || r_mvnto_caja.cod_dvi
                     || '- Banco '
                     || r_mvnto_caja.cod_bco_dtn_org;
                  pkg_pab_tracking.sp_pab_ins_log_bd (err_code,
                                                      v_nom_pck,
                                                      err_msg,
                                                      v_nom_sp);
                  p_error := pkg_pab_constantes.v_error;
               WHEN OTHERS
               THEN
                  err_code := SQLCODE;
                  err_msg := SUBSTR (SQLERRM, 1, 300);
                  p_s_mensaje := err_code || '-' || err_msg;
                  pkg_pab_tracking.sp_pab_ins_log_bd (err_code,
                                                      v_nom_pck,
                                                      err_msg,
                                                      v_nom_sp);
                  p_error := pkg_pab_constantes.v_error;
                  RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
            END;


            --Se rescata saldos intradia
            pkg_pab_caja.sp_pab_cna_sld_cja (v_fec_ins_ope,
                                             r_mvnto_caja.num_cta_cte,
                                             r_mvnto_caja.cod_dvi,
                                             r_mvnto_caja.cod_bco_dtn_org,
                                             v_imp_ing_dia,
                                             v_imp_egr_dia,
                                             v_imp_ral_aos,
                                             p_error);

            BEGIN
               --Actualizan los saldos Intradia y el total final del dia
               UPDATE   pabs_dt_detll_saldo_caja_mn_mx
                  SET   imp_ing_cpr = (NVL (v_imp_ing_dia, 0)),
                        --ingresos comprometidos
                        imp_egr_cpr = (NVL (v_imp_egr_dia, 0)),
                        -- egresos comprometidos.
                        imp_tot_fnl_dia =
                           (  NVL (v_imp_apr_aos_sal, 0)
                            + NVL (v_imp_ing_dia, 0)
                            - NVL (v_imp_egr_dia, 0))   --  total comprometido
                WHERE   cod_bco_org = r_mvnto_caja.cod_bco_dtn_org --v_cod_bco_bcch
                        AND cod_dvi = r_mvnto_caja.cod_dvi;
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  err_code := SQLCODE;
                  err_msg :=
                     SUBSTR (SQLERRM, 1, 300)
                     || ' Error al actualizar los saldos de ingreso y egreso para los siguiente datos : codigo banco '
                     || r_mvnto_caja.cod_bco_dtn_org
                     || ' cuenta '
                     || r_mvnto_caja.num_cta_cte
                     || ' divisa '
                     || r_mvnto_caja.cod_dvi;
                  PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                      v_NOM_PCK,
                                                      err_msg,
                                                      v_NOM_SP);
                  p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
                  RAISE;
               WHEN OTHERS
               THEN
                  err_code := SQLCODE;
                  err_msg := SUBSTR (SQLERRM, 1, 300);
                  p_s_mensaje := err_code || '-' || err_msg;
                  PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                      v_NOM_PCK,
                                                      err_msg,
                                                      v_NOM_SP);
                  p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
                  RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
            END;

            BEGIN
               --Obtenemos el importe final del dia
               SELECT   imp_tot_fnl_dia
                 INTO   v_imp_ral_aos_sal
                 FROM   pabs_dt_detll_saldo_caja_mn_mx saldo
                WHERE       saldo.num_cta_cte = r_mvnto_caja.num_cta_cte
                        AND saldo.cod_dvi = r_mvnto_caja.cod_dvi
                        AND saldo.cod_bco_org = r_mvnto_caja.cod_bco_dtn_org
                        AND saldo.fec_ing_ctl <= v_fec_ins_ope;
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  err_code := SQLCODE;
                  err_msg :=
                        'No se encuentra registro para cuenta '
                     || r_mvnto_caja.num_cta_cte
                     || ' - divisa '
                     || r_mvnto_caja.cod_dvi
                     || '- Banco '
                     || r_mvnto_caja.cod_bco_dtn_org;
                  pkg_pab_tracking.sp_pab_ins_log_bd (err_code,
                                                      v_nom_pck,
                                                      err_msg,
                                                      v_nom_sp);
                  p_error := pkg_pab_constantes.v_error;
               WHEN OTHERS
               THEN
                  err_code := SQLCODE;
                  err_msg := SUBSTR (SQLERRM, 1, 300);
                  p_s_mensaje := err_code || '-' || err_msg;
                  pkg_pab_tracking.sp_pab_ins_log_bd (err_code,
                                                      v_nom_pck,
                                                      err_msg,
                                                      v_nom_sp);
                  p_error := pkg_pab_constantes.v_error;
                  RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
            END;


            --Obtenemos fecha dia,24,48 segun la fecha de la cartola
            pkg_pab_caja.sp_pab_rno_fec_pyn (r_mvnto_caja.cod_bco_dtn_org,
                                             r_mvnto_caja.num_cta_cte,
                                             r_mvnto_caja.cod_dvi,
                                             v_fec_ins_ope,
                                             v_fec_24hs,
                                             v_fec_48hs,
                                             p_error);

            -- controlamos si la fecha es sabado o dominfo para las 2 Hrs
            v_fec_48hs_val :=
               TO_CHAR (v_fec_48hs, 'D', 'NLS_DATE_LANGUAGE=SPANISH');
            v_fec_24hs_val :=
               TO_CHAR (v_fec_24hs, 'D', 'NLS_DATE_LANGUAGE=SPANISH');

            --Validamos que no sea (Sabado 7, Domingo 1)
            IF ( (v_fec_48hs_val = 7) OR (v_fec_48hs_val = 1))
            THEN
               --Lo cambiamos a Lunes
               v_fec_48hs := v_fec_48hs + 2;
            END IF;

            --Validamos que no sea Sabado
            IF (v_fec_24hs_val = 7)
            THEN
               --Lo cambiamos a Lunes 2
               v_fec_24hs := v_fec_24hs + 2;
            END IF;

            --Se rescata saldos 24 horas
            pkg_pab_caja.sp_pab_cna_sld_cja (v_fec_24hs,
                                             r_mvnto_caja.num_cta_cte,
                                             r_mvnto_caja.cod_dvi,
                                             r_mvnto_caja.cod_bco_dtn_org,
                                             v_imp_ing_24,
                                             v_imp_egr_24,
                                             v_imp_tot_24,
                                             p_error);


            BEGIN
               -- ACTUALIZAMOS SALDOS CUENTA 24 Horas
               UPDATE   pabs_dt_detll_saldo_caja_mn_mx
                  SET   imp_ing_24 = (NVL (v_imp_ing_24, 0)),
                        imp_egr_24 = (NVL (v_imp_egr_24, 0)),
                        imp_tot_24 =
                           (  NVL (imp_tot_fnl_dia, 0)
                            + NVL (v_imp_ing_24, 0)
                            - NVL (v_imp_egr_24, 0))
                WHERE       cod_bco_org = r_mvnto_caja.cod_bco_dtn_org
                        AND num_cta_cte = r_mvnto_caja.num_cta_cte
                        AND cod_dvi = r_mvnto_caja.cod_dvi
                        AND fec_ing_ctl <= v_fec_ins_ope;
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  err_code := SQLCODE;
                  err_msg :=
                     ' Error al actualizar los saldos de ingreso y egreso para los siguiente datos : codigo banco '
                     || r_mvnto_caja.cod_bco_dtn_org
                     || ' cuenta '
                     || r_mvnto_caja.num_cta_cte
                     || ' divisa '
                     || r_mvnto_caja.cod_dvi;
                  pkg_pab_tracking.sp_pab_ins_log_bd (err_code,
                                                      v_nom_pck,
                                                      err_msg,
                                                      v_nom_sp);
                  p_error := pkg_pab_constantes.v_error;
               WHEN OTHERS
               THEN
                  err_code := SQLCODE;
                  err_msg := SUBSTR (SQLERRM, 1, 300);
                  p_s_mensaje := err_code || '-' || err_msg;
                  pkg_pab_tracking.sp_pab_ins_log_bd (err_code,
                                                      v_nom_pck,
                                                      err_msg,
                                                      v_nom_sp);
                  p_error := pkg_pab_constantes.v_error;
                  RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
            END;

            --Se rescata saldos 48 horas.
            pkg_pab_caja.sp_pab_cna_sld_cja (v_fec_48hs,
                                             r_mvnto_caja.num_cta_cte,
                                             r_mvnto_caja.cod_dvi,
                                             r_mvnto_caja.cod_bco_dtn_org,
                                             v_imp_ing_48,
                                             v_imp_egr_48,
                                             v_imp_tot_48,
                                             p_error);


            BEGIN
               SELECT   imp_tot_24
                 INTO   v_imp_tot_24_sal
                 FROM   pabs_dt_detll_saldo_caja_mn_mx saldo
                WHERE       saldo.num_cta_cte = r_mvnto_caja.num_cta_cte
                        AND saldo.cod_dvi = r_mvnto_caja.cod_dvi
                        AND saldo.cod_bco_org = r_mvnto_caja.cod_bco_dtn_org;
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  err_code := SQLCODE;
                  err_msg :=
                        'No se encuentra registro para cuenta '
                     || r_mvnto_caja.num_cta_cte
                     || ' - divisa '
                     || r_mvnto_caja.cod_dvi
                     || '- Banco '
                     || r_mvnto_caja.cod_bco_dtn_org;
                  pkg_pab_tracking.sp_pab_ins_log_bd (err_code,
                                                      v_nom_pck,
                                                      err_msg,
                                                      v_nom_sp);
                  p_error := pkg_pab_constantes.v_error;
               WHEN OTHERS
               THEN
                  err_code := SQLCODE;
                  err_msg := SUBSTR (SQLERRM, 1, 300);
                  p_s_mensaje := err_code || '-' || err_msg;
                  pkg_pab_tracking.sp_pab_ins_log_bd (err_code,
                                                      v_nom_pck,
                                                      err_msg,
                                                      v_nom_sp);
                  p_error := pkg_pab_constantes.v_error;
                  RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
            END;


            BEGIN
               UPDATE   pabs_dt_detll_saldo_caja_mn_mx
                  SET   imp_ing_48 = (NVL (v_imp_ing_48, 0)),
                        imp_egr_48 = (NVL (v_imp_egr_48, 0)),
                        imp_tot_48 =
                           (  NVL (v_imp_tot_24_sal, 0)
                            + NVL (v_imp_ing_48, 0)
                            - NVL (v_imp_egr_48, 0))
                WHERE       cod_bco_org = r_mvnto_caja.cod_bco_dtn_org
                        AND num_cta_cte = r_mvnto_caja.num_cta_cte
                        AND cod_dvi = r_mvnto_caja.cod_dvi;
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  err_code := SQLCODE;
                  err_msg :=
                     ' Error al actualizar los saldos de ingreso y egreso para los siguiente datos : codigo banco '
                     || r_mvnto_caja.cod_bco_dtn_org
                     || ' cuenta '
                     || r_mvnto_caja.num_cta_cte
                     || ' divisa '
                     || r_mvnto_caja.cod_dvi;
                  pkg_pab_tracking.sp_pab_ins_log_bd (err_code,
                                                      v_nom_pck,
                                                      err_msg,
                                                      v_nom_sp);
                  p_error := pkg_pab_constantes.v_error;
               WHEN OTHERS
               THEN
                  err_code := SQLCODE;
                  err_msg := SUBSTR (SQLERRM, 1, 300);
                  p_s_mensaje := err_code || '-' || err_msg;
                  pkg_pab_tracking.sp_pab_ins_log_bd (err_code,
                                                      v_nom_pck,
                                                      err_msg,
                                                      v_nom_sp);
                  p_error := pkg_pab_constantes.v_error;
                  RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
            END;

            p_error := pkg_pab_constantes.v_ok;
         END IF;
      END LOOP;
   EXCEPTION
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         p_s_mensaje := err_code || '-' || err_msg;
         pkg_pab_tracking.sp_pab_ins_log_bd (err_code,
                                             v_nom_pck,
                                             err_msg,
                                             v_nom_sp);
         p_error := pkg_pab_constantes.v_error;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END sp_pab_act_saldo_mn_mx;

   /********************************************************************************************************
   FUNCION/PROCEDIMIENTO: SP_BUS_PRYCN_MNMX.
   OBJETIVO             : MUESTRA EL DETALLE DE LA PROYECCION CARGADA PARA MONEDA NACIONAL Y MONEDA
                          EXTRANJERA SEGUN AREA.
   SISTEMA              : DBO_PAB.
   BASE DE DATOS        : DBO_PAB.
   TABLAS USADAS        : PABS_DT_DETLL_MVNTO_CAJA.
                          PABS_DT_SISTM_ENTRD_SALID.
                          TCDTBAI.
   FECHA                : 13/02/19.
   AUTOR                : VAR.
   INPUT                : P_AREA    := AREA.
                          P_FLG_MRD := MONEDA.
                            0 := MN - 1 := MX.
   OUTPUT               : P_CURSOR  := DATA.
                          P_ERROR   := INDICADOR DE ERRORES.
   OBSERVACIONES
   FECHA       USUARIO    DETALLE
   13/02/19    VAR        CREACION.
   ********************************************************************************************************/
   PROCEDURE SP_BUS_PRYCN_MNMX (P_AREA      IN     CHAR,
                                P_FLG_MRD   IN     CHAR,
                                P_CURSOR       OUT SYS_REFCURSOR,
                                P_ERROR        OUT NUMBER)
   IS
      -------------------------------------DECLARACION DE VARIABLES Y CONSTANTES------------------------------
      V_NOM_SP         VARCHAR2 (30) := 'SP_BUS_PRYCN_MNMX';
      V_FLG_MRD        CHAR (1);
      v_FECHA_ACTUAL   DATE := TRUNC (SYSDATE);
      v_FECHA_AYER     DATE := TRUNC (SYSDATE - 1);
      V_AREA           PABS_DT_USRIO.COD_SIS_ENT%TYPE;
   --------------------------------------------------------------------------------------------------------
   BEGIN
      --RELACIONAMOS LOS PARAMETROS DE ENTRADA CON VARIABLES.
      V_AREA := P_AREA;
      V_FLG_MRD := P_FLG_MRD;

      --MONEDA NACIONAL.
      IF V_FLG_MRD = PKG_PAB_CONSTANTES.V_FLG_MRD_MN
      THEN
         --FILTRAMOS POR AREA : OPERACIONES FINANCIERAS
         IF V_AREA = PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SE_OOFF
         THEN
            OPEN P_CURSOR FOR
               SELECT   DECODE (MC.FLG_ING_EGR,
                                PKG_PAB_CONSTANTES.V_FLG_EGR,               --
                                                             MC.COD_BCO_DTN,
                                MC.COD_BCO_ORG)
                           BIC,
                        PKG_PAB_UTILITY.FN_PAB_OBT_DSC_BCO(DECODE (
                                                              FLG_ING_EGR,
                                                              PKG_PAB_CONSTANTES.V_FLG_EGR,
                                                              COD_BCO_DTN,
                                                              COD_BCO_ORG
                                                           ))
                           AS NOMBRE,
                        TO_CHAR (MC.FEC_VTA, 'DD-MM-YYYY') FECHA_VALUTA,
                        MC.COD_TPO_OPE_AOS CODIGO,
                        MC.FLG_ING_EGR FLG,
                        DECODE (MC.FLG_ING_EGR,
                                PKG_PAB_CONSTANTES.V_FLG_ING, MC.IMP_OPE,
                                PKG_PAB_CONSTANTES.V_FLG_EGR)
                           INGRESO,
                        DECODE (MC.FLG_ING_EGR,
                                PKG_PAB_CONSTANTES.V_FLG_EGR, MC.IMP_OPE,
                                PKG_PAB_CONSTANTES.V_FLG_EGR)
                           EGRESO,
                        SIS.DSC_SIS_ENT_SAL AREA,
                        MC.GLS_MVT REFERENCIA_AREA,
                        MC.NUM_FOL_OPE FOLIO_OPE,
                        MC.FEC_INS_OPE FECHA_OPE,
                        MC.NUM_FOL_NMN FOLIO_NOM
                 FROM   PABS_DT_DETLL_MVNTO_CAJA MC,
                        PABS_DT_SISTM_ENTRD_SALID SIS,
                        TCDTBAI BAI_ORG,
                        TCDTBAI BAI_DES
                WHERE       MC.COD_SIS_ENT = SIS.COD_SIS_ENT_SAL
                        AND MC.COD_DVI = PKG_PAB_CONSTANTES.V_STR_DVI_CLP
                        AND MC.COD_BCO_ORG = BAI_ORG.TGCDSWSA(+)
                        AND MC.COD_BCO_DTN = BAI_DES.TGCDSWSA(+)
                        AND MC.FEC_VTA >= V_FECHA_ACTUAL;
         --FILTRAMOS POR OTRAS AREAS.
         ELSE
            OPEN P_CURSOR FOR
               SELECT   DECODE (MC.FLG_ING_EGR,
                                PKG_PAB_CONSTANTES.V_FLG_ING, MC.COD_BCO_DTN,
                                MC.COD_BCO_ORG)
                           BIC,
                        PKG_PAB_UTILITY.FN_PAB_OBT_DSC_BCO(DECODE (
                                                              FLG_ING_EGR,
                                                              PKG_PAB_CONSTANTES.V_FLG_EGR,
                                                              COD_BCO_DTN,
                                                              COD_BCO_ORG
                                                           ))
                           AS NOMBRE,
                        TO_CHAR (MC.FEC_VTA, 'DD-MM-YYYY') FECHA_VALUTA,
                        MC.COD_TPO_OPE_AOS CODIGO,
                        MC.FLG_ING_EGR FLG,
                        DECODE (MC.FLG_ING_EGR,
                                PKG_PAB_CONSTANTES.V_FLG_ING, MC.IMP_OPE,
                                PKG_PAB_CONSTANTES.V_FLG_EGR)
                           INGRESO,
                        DECODE (MC.FLG_ING_EGR,
                                PKG_PAB_CONSTANTES.V_FLG_EGR, MC.IMP_OPE,
                                PKG_PAB_CONSTANTES.V_FLG_EGR)
                           EGRESO,
                        SIS.DSC_SIS_ENT_SAL AREA,
                        MC.GLS_MVT REFERENCIA_AREA,
                        MC.NUM_FOL_OPE FOLIO_OPE,
                        MC.FEC_INS_OPE FECHA_OPE,
                        MC.NUM_FOL_NMN FOLIO_NOM
                 FROM   PABS_DT_DETLL_MVNTO_CAJA MC,
                        PABS_DT_SISTM_ENTRD_SALID SIS,
                        TCDTBAI BAI_ORG,
                        TCDTBAI BAI_DES
                WHERE       MC.COD_SIS_ENT = SIS.COD_SIS_ENT_SAL
                        AND MC.COD_DVI = PKG_PAB_CONSTANTES.V_STR_DVI_CLP
                        AND SIS.COD_SIS_ENT_SAL = V_AREA
                        AND MC.COD_BCO_ORG = BAI_ORG.TGCDSWSA(+)
                        AND MC.COD_BCO_DTN = BAI_DES.TGCDSWSA(+)
                        AND MC.FEC_VTA >= V_FECHA_ACTUAL;
         END IF;
      --MONEDA EXTRANJERA.
      ELSE
         --FILTRAMOS POR AREA : OPERACIONES FINANCIERAS
         IF V_AREA = PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SE_OOFF
         THEN
            OPEN P_CURSOR FOR
               SELECT   MC.NUM_FOL_OPE NUM_OPE,
                        MC.NUM_FOL_NMN NUM_NMN,
                        CASE
                           WHEN MC.FLG_ING_EGR = PKG_PAB_CONSTANTES.V_FLG_ING
                           THEN
                              'Ingreso'
                           ELSE
                              'Egreso'
                        END
                           FLUJO,
                        CASE
                           WHEN MC.FLG_ING_EGR = PKG_PAB_CONSTANTES.V_FLG_ING
                           THEN
                              MC.COD_BCO_ORG
                           ELSE
                              MC.COD_BCO_DTN
                        END
                           BIC,
                        PKG_PAB_UTILITY.FN_PAB_OBT_DSC_BCO(DECODE (
                                                              FLG_ING_EGR,
                                                              PKG_PAB_CONSTANTES.V_FLG_EGR,
                                                              COD_BCO_DTN,
                                                              COD_BCO_ORG
                                                           ))
                           AS NOMBRE,
                        MC.NUM_CTA_CTE CUENTA,
                        MC.COD_TPO_OPE_AOS COD_OPE,
                        MC.COD_DVI COD_DVI,
                        MC.IMP_OPE MONTO,
                        MC.FEC_VTA FEC_VTA,
                        MC.GLS_SIS_ORG OP_ORG,
                        MC.GLS_MVT OBSERVACION
                 FROM   PABS_DT_DETLL_MVNTO_CAJA MC,
                        PABS_DT_SISTM_ENTRD_SALID SIS,
                        TCDTBAI BAI_ORG,
                        TCDTBAI BAI_DES
                WHERE       MC.COD_SIS_ENT = SIS.COD_SIS_ENT_SAL
                        AND MC.COD_DVI != PKG_PAB_CONSTANTES.V_STR_DVI_CLP
                        AND MC.COD_BCO_ORG = BAI_ORG.TGCDSWSA(+)
                        AND MC.COD_BCO_DTN = BAI_DES.TGCDSWSA(+)
                        AND MC.FEC_VTA >= V_FECHA_ACTUAL;
         --FILTRAMOS POR OTRAS AREAS.
         ELSE
            OPEN P_CURSOR FOR
               SELECT   MC.NUM_FOL_OPE NUM_OPE,
                        MC.NUM_FOL_NMN NUM_NMN,
                        CASE
                           WHEN MC.FLG_ING_EGR = PKG_PAB_CONSTANTES.V_FLG_ING
                           THEN
                              'Ingreso'
                           ELSE
                              'Egreso'
                        END
                           FLUJO,
                        CASE
                           WHEN MC.FLG_ING_EGR = PKG_PAB_CONSTANTES.V_FLG_ING
                           THEN
                              MC.COD_BCO_ORG
                           ELSE
                              MC.COD_BCO_DTN
                        END
                           BIC,
                        PKG_PAB_UTILITY.FN_PAB_OBT_DSC_BCO(DECODE (
                                                              FLG_ING_EGR,
                                                              PKG_PAB_CONSTANTES.V_FLG_EGR,
                                                              COD_BCO_DTN,
                                                              COD_BCO_ORG
                                                           ))
                           AS NOMBRE,
                        MC.NUM_CTA_CTE CUENTA,
                        MC.COD_TPO_OPE_AOS COD_OPE,
                        MC.COD_DVI COD_DVI,
                        MC.IMP_OPE MONTO,
                        MC.FEC_VTA FEC_VTA,
                        MC.GLS_SIS_ORG OP_ORG,
                        MC.GLS_MVT OBSERVACION
                 FROM   PABS_DT_DETLL_MVNTO_CAJA MC,
                        PABS_DT_SISTM_ENTRD_SALID SIS,
                        TCDTBAI BAI_ORG,
                        TCDTBAI BAI_DES
                WHERE       MC.COD_SIS_ENT = SIS.COD_SIS_ENT_SAL
                        AND MC.COD_DVI != PKG_PAB_CONSTANTES.V_STR_DVI_CLP
                        AND SIS.COD_SIS_ENT_SAL = V_AREA
                        AND MC.COD_BCO_ORG = BAI_ORG.TGCDSWSA(+)
                        AND MC.COD_BCO_DTN = BAI_DES.TGCDSWSA(+)
                        AND MC.FEC_VTA >= V_FECHA_ACTUAL;
         END IF;
      END IF;

      P_ERROR := PKG_PAB_CONSTANTES.V_OK;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         ERR_CODE := SQLCODE;
         ERR_MSG :=
            'No se han encontrado registos asociados a la busqueda por banco '
            || PKG_PAB_CONSTANTES.V_BIC_BCCH
            || ' - divisa  '
            || PKG_PAB_CONSTANTES.V_STR_DVI_CLP
            || '- fecha '
            || V_FECHA_AYER;
         PKG_PAB_TRACKING.SP_PAB_INS_LOG_BD (ERR_CODE,
                                             V_NOM_PCK,
                                             ERR_MSG,
                                             V_NOM_SP);
         P_ERROR := PKG_PAB_CONSTANTES.V_ERROR;
      WHEN OTHERS
      THEN
         ERR_CODE := SQLCODE;
         ERR_MSG := SUBSTR (SQLERRM, 1, 300);
         P_S_MENSAJE := ERR_CODE || '-' || ERR_MSG;
         PKG_PAB_TRACKING.SP_PAB_INS_LOG_BD (ERR_CODE,
                                             V_NOM_PCK,
                                             ERR_MSG,
                                             V_NOM_SP);
         P_ERROR := PKG_PAB_CONSTANTES.V_ERROR;
         RAISE_APPLICATION_ERROR (-20000, P_S_MENSAJE);
   END SP_BUS_PRYCN_MNMX;
END PKG_PAB_CAJA;
/


GRANT EXECUTE ON DBO_PAB.PKG_PAB_CAJA TO PUBLIC
/

GRANT EXECUTE ON DBO_PAB.PKG_PAB_CAJA TO R_APP_PAB
/

GRANT EXECUTE ON DBO_PAB.PKG_PAB_CAJA TO USR_PAB
/

