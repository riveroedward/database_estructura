CREATE OR REPLACE /* Formatted on 18-08-2020 17:45:48 (QP5 v5.115.810.9015) */
PACKAGE DBO_PAB.PKG_PAB_OPERACION_DEV
AS
   /**  Package el cual contrendra todas las funciones y procedimientos relacionados con las operaciones de devolucion
   -- @Santander */

   /******************************************************************************
      NAME:       PKG_PAB_OPERACION_DEV
      PURPOSE: Package que contiene lo procedimientos que se encargan de la insercion
              de una operacion de devolucion, abono y envio devolucion.

      REVISIONS:
      Ver        Date        Author           Description
      ---------  ----------  ---------------  ------------------------------------
      1.0        13-04-2017             1. Created this package.
   ******************************************************************************/
   -- ************************** INICIO VARIABLE GLOBALES ********************************
   v_NOM_PCK      VARCHAR2 (30) := 'PKG_PAB_OPERACION_DEV';
   ERR_CODE       NUMBER := 0;
   ERR_MSG        VARCHAR2 (300);
   v_DES          VARCHAR2 (300);
   v_DES_BIT      VARCHAR2 (100);
   v_BANCO        VARCHAR2 (9) := '970150005';
   p_s_mensaje    VARCHAR2 (400);
   TOPE_MSJ_BLQ   NUMBER := 299; --VALOR MAXIMO DE DETALLE DE MENSAJE PROTOCOLO POR BLOQUES

   TYPE T_NUM_FOL_NMN
   IS
      TABLE OF NUMBER
         INDEX BY PLS_INTEGER;

   v_dia_ini      DATE := TRUNC (SYSDATE) + INTERVAL '1' MINUTE;
   v_dia_fin DATE
         := TRUNC (SYSDATE) + INTERVAL '23' HOUR + INTERVAL '59' MINUTE ;
   v_cero         NUMBER (1) := 0;

   --   TOPE_MSJ_BLQ NUMBER := 299;  --VALOR MAXIMO DE DETALLE DE MENSAJE PROTOCOLO POR BLOQUES
   --   TYPE T_NUM_FOL_NMN  is table of NUMBER index by pls_integer;
   -- ************************** FIN VARIABLE GLOBALES ********************************

   -- ***********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_GEN_MT_DEV
   -- Objetivo: Procedimiento almacenado que busca detalle devolucion manual
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_DETLL_OPRCN y PABS_DT_OPRCN_INFCN_OPCON
   -- Fecha: 07/11/16
   -- Autor: Santander
   -- Input:
   -- p_FEC_ISR_OPE -> Fecha de insercion de operacion en el sistema, PK tabla detalle operaciones.
   -- p_NUM_FOL_OPE -> foliador del banco, PK tabla detalle operaciones.
   -- Output:
   -- p_CURSOR -> Cursor con las operaciones encontradas
   -- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   PROCEDURE SP_PAB_GEN_MT_DEV (p_FEC_ISR_OPE   IN     DATE,
                                p_NUM_FOL_OPE   IN     NUMBER,
                                p_CURSOR           OUT SYS_REFCURSOR,
                                p_ERROR            OUT NUMBER);



   /************************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_GEN_OPE_DEV
   -- Objetivo: Procedimiento almacenado Orquestador que llama al PL que inserta Operacion Para Devolver y luego al PL que actualiza la operacion original,
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas:
   -- Fecha: 24/03/17
   -- Autor: Santander--CAH
   -- Input:
   -- p_NUM_FOL_OPE        -->Foliador del banco, PK tabla detalle operaciones.
   -- p_FEC_ISR_OPE_ORG    -->Folio operacion de origen, PK tabla detalle operaciones.
   -- p_NUM_FOL_OPE_ORG    -->Fecha de insercion de operacion origina, PK tabla detalle operaciones.
   -- p_COD_DVI            -->Codigo de moneda
   -- p_IMP_OPE            -->Monto operacion
   -- p_COD_BCO_DTN        -->Codigo banco destino BIC
   -- p_NUM_REF_EXT        -->Codigo Referencia SWIFT externa
   -- p_OBS_OPC_SWF        -->Observacion opcional
   -- p_COD_USR            -->Codigo usuario
   -- Output:
   -- p_ERROR              -->indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE Sp_PAB_GEN_OPE_DEV (p_FEC_ISR_OPE_ORG   IN     DATE,
                                 p_NUM_FOL_OPE_ORG   IN     NUMBER,
                                 p_COD_DVI           IN     CHAR,
                                 p_IMP_OPE           IN     NUMBER,
                                 p_COD_BCO_DTN       IN     CHAR,
                                 p_NUM_REF_EXT       IN     CHAR,
                                 ----------Opcionales-------
                                 p_OBS_OPC_SWF       IN     VARCHAR2,
                                 --------------------------------
                                 p_COD_USR           IN     CHAR,
                                 p_ERROR                OUT NUMBER,
                                 p_NUM_FOL_OPE          OUT NUMBER,
                                 p_FEC_ISR_OPE          OUT DATE);

   --
   -- ***********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_BUS_OPE_ASO_SWF
   -- Objetivo: Procedimiento almacenado que retorna las operaciones origen posibles para asociar
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_DETLL_OPRCN_HTRCA, PABS_DT_OPRCN_OPCON_HTRCA
   -- Fecha: 07/11/16
   -- Autor: Santander
   -- Input:
   -- p_COD_BCO_DTN    -> codigo banco destino
   -- p_MTO_OPE        -> importe operacion
   -- p_FLG_EGR_ING    -> Tipo de operacion Ingreso = 1 o Egreso = 0
   -- p_FECHA_DES      -> Fecha desde
   -- p_FECHA_DES      -> Fecha hasta
   -- p_NUM_REF_SWF    -> Codigo Referencia SWIFT
   -- Output:
   -- p_CURSOR         -> Cursor con las operaciones posibles
   -- p_ERROR          -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   PROCEDURE SP_PAB_BUS_OPE_ASO_SWF (p_COD_BCO_DTN   IN     CHAR,
                                     p_MTO_OPE       IN     NUMBER,
                                     p_FLG_EGR_ING   IN     NUMBER,
                                     p_FECHA_DES     IN     DATE,
                                     p_FECHA_HAS     IN     DATE,
                                     p_NUM_REF_SWF   IN     CHAR,
                                     p_CURSOR           OUT SYS_REFCURSOR,
                                     p_ERROR            OUT NUMBER);

   --
   -- ***********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_ASO_OPE_ORI_SWF
   -- Objetivo: Procedimiento almacenado que asocia operacion origen y actualiza estado de la operacion
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_DETLL_OPRCN
   -- Fecha: 07/11/16
   -- Autor: Santander
   -- Input:
   -- p_NUM_FOL_OPE     -> foliador del banco, PK tabla detalle operaciones.
   -- p_FEC_ISR_OPE     -> Fecha de insercion de operacion en el sistema, PK tabla detalle operaciones.
   -- p_NUM_REF_SWF     -> Numero de Refencia SWIFT
   -- p_NUM_FOL_OPE_ORG -> Numero de folio de la operacion origen
   -- p_FEC_ISR_OPE_ORG -> Fecha de insercion de la operacion origen
   -- p_COD_USR         -> Codigo de Usuario que se registrara en bitacora
   -- Output:
   -- p_ERROR          -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   PROCEDURE SP_PAB_ASO_OPE_ORI_SWF (p_NUM_FOL_OPE       IN     NUMBER,
                                     p_FEC_ISR_OPE       IN     DATE,
                                     p_NUM_REF_EXT       IN     CHAR,
                                     p_NUM_FOL_OPE_ORG   IN     NUMBER,
                                     p_FEC_ISR_OPE_ORG   IN     DATE,
                                     p_COD_USR           IN     CHAR,
                                     p_ERROR                OUT NUMBER);

   --
   --
   -- ***********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_GES_ABO_MAN
   -- Objetivo: Procedimiento actualiza estado de la operacion devuelta y la operacion origen
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: N/A
   -- Fecha: 18/11/16
   -- Autor: Santander
   -- Input:
   -- p_NUM_FOL_OPE_ORI    -> Numero de Operacion Origen
   -- p_FEC_ISR_OPE_ORI    -> Fecha de insercion de operacion origen
   -- p_SIS_ENT_ORI        -> Nombre Canal de Entrada origen
   -- p_NUM_FOL_OPE_DEV    -> Numero de Operacion Devuelta
   -- p_FEC_ISR_OPE_DEV    -> Fecha de insercion de operacion devuelta
   -- p_NUM_CTA_BFC        -> Numero de cuenta
   -- p_TIP_ABO_MAN        -> Codigo de tipo Abono
   -- p_NUM_DEV_TRANS      -> Numero devuelto por WS de Vigencia Salida
   -- p_COD_USR            -> Codigo de Usuario que se registrara en bitacora
   -- Output:
   -- p_ERROR              -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   PROCEDURE SP_PAB_GES_ABO_MAN (p_NUM_FOL_OPE_ORI   IN     NUMBER,
                                 p_FEC_ISR_OPE_ORI   IN     DATE,
                                 p_SIS_ENT_ORI       IN     CHAR,
                                 p_NUM_FOL_OPE_DEV   IN     NUMBER,
                                 p_FEC_ISR_OPE_DEV   IN     DATE,
                                 p_NUM_CTA           IN     CHAR,
                                 p_TIP_ABO_MAN       IN     CHAR,
                                 p_NUM_DEV_TRANS     IN     VARCHAR2,
                                 p_COD_USR           IN     CHAR,
                                 p_ERROR                OUT NUMBER);

   --************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_BUS_SIS_SAL
   -- Objetivo: Procedimiento almacenado que Retorna Codigo de Sistema de Salida, segun Banco y Horario
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_BANCO_ALMOT,
   --          PABS_DT_SISTM_ENTRD_SALID,
   -- Fecha: 28/03/17
   -- Autor: Santander--CAH
   -- Input:
   -- p_COD_BCO_DTN    -->Codigo banco destino BIC
   -- p_COD_MT_SWF     -->Codigo MT SWF
   -- Output:
   -- p_COD_SIS_SAL    -->Codigo de sistema salida
   -- p_ERROR          -->indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/

   PROCEDURE Sp_PAB_BUS_SIS_SAL (p_COD_BCO_DTN   IN     CHAR,
                                 p_COD_MT_SWF    IN     NUMBER,
                                 p_COD_SIS_SAL      OUT CHAR,
                                 p_ERROR            OUT NUMBER);

   -- ***********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_CON_DEV_EGR
   -- Objetivo: Procedimiento que consulta y lista las operaciones Por Abonar o Por Asociar
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_DETLL_NOMNA, PABS_DT_DETLL_OPRCN
   -- Fecha: 27/10/16
   -- Autor: Santander
   -- Input:
   -- p_COD_EST_AOS -> Estado de la operacion Por Abonar y/o Por Asociar
   -- p_MNTO_DES -> Monto operacion desde
   -- p_MNTO_HAS -> Monto operacion hasta
   -- p_COD_DVI-> Codigo de moneda
   -- p_NUM_REF_SWF -> Numero Referencia SWIFT
   -- p_COD_BCO -> Codigo de banco
   -- Output:
   -- p_CURSOR -> Cursor con las operaciones encontradas
   -- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   PROCEDURE SP_PAB_CON_DEV_EGR (p_COD_EST_AOS   IN     NUMBER,
                                 p_MNTO_DES      IN     NUMBER,
                                 p_MNTO_HAS      IN     NUMBER,
                                 p_COD_DVI       IN     CHAR,
                                 p_NUM_REF_SWF   IN     CHAR,
                                 p_COD_BCO       IN     CHAR,
                                 p_CURSOR           OUT SYS_REFCURSOR,
                                 p_ERROR            OUT NUMBER);

   --
   -- ***********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_CMB_TIP_ABO
   -- Objetivo: Procedimiento que consulta y lista las tipos de abonos.
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: N/A
   -- Fecha: 07/11/16
   -- Autor: Santander
   -- Input:
   -- p_COD_MT_SWF -> Codigo motivo SWIFT
   -- Output:
   -- p_CURSOR -> Cursor con loa tipos de abonos
   -- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   PROCEDURE SP_PAB_CMB_TIP_ABO (p_COD_MT_SWF   IN     NUMBER,
                                 p_CURSOR          OUT SYS_REFCURSOR,
                                 p_ERROR           OUT NUMBER);

   --
   -- ***********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_BUS_CTA_ABO
   -- Objetivo: Procedimiento almacenado que busca numero relacionado a la cuenta del tipo abono
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_CENTA_CNTBL
   -- Fecha: 07/11/16
   -- Autor: Santander
   -- Input:
   -- p_COD_SIS_ENT_SAL ->  codigo sistema entrada salida
   -- p_COD_DVI ->  Tipo de operaci?n Ingreso o Egreso
   -- Output:
   -- p_CURSOR -> Cursor con registro encontrado
   -- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   PROCEDURE SP_PAB_BUS_CTA_ABO (p_COD_SIS_ENT_SAL   IN     CHAR,
                                 p_COD_DVI           IN     CHAR,
                                 p_CURSOR               OUT SYS_REFCURSOR,
                                 p_ERROR                OUT NUMBER);

   --

   -- ***********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_COMBO_EST_DEV_EGR
   -- Objetivo: Procedimiento que consulta estados Por Abonar o Por Asociar
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_ESTDO_ALMOT
   -- Fecha: 27/10/16

   -- Autor: Santander
   -- Input:
   -- N/A
   -- Output:
   -- p_CURSOR -> Cursor con los registros de los 2 estados
   -- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   PROCEDURE SP_PAB_COMBO_EST_DEV_EGR (p_CURSOR   OUT SYS_REFCURSOR,
                                       p_ERROR    OUT NUMBER);

   --
   /************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_ACT_EST_OPERACION_DEV
   -- Objetivo: Procedimiento que actualiza el estado de una operacion DEVUELTA Y DE ORIGEN
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_DETLL_OPRCN
   -- Fecha: 20/04/2017
   -- Autor: Santander- CAH
   -- Input:    p_NUM_FOL_OPE-->   Numero de Operacion
   --           p_FEC_ISR_OPE-->   Fecha de insercion de la operacion
   --           p_GLS_BTC_OPE-->   Glosa de bitacora
   --           p_NUM_REF_SWF-->   Ref Swift
   --           p_COD_EST_AOS->   Identificador de nuevo estado.
   --           p_COD_USR->       rut usuario
   -- Output:
   --        p_ERROR       -->     indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE Sp_PAB_ACT_EST_OPERACION_DEV (p_NUM_FOL_OPE   IN     NUMBER,
                                           p_FEC_ISR_OPE   IN     DATE,
                                           p_GLS_BTC_OPE   IN     VARCHAR2,
                                           p_NUM_REF_SWF   IN     CHAR,
                                           p_COD_EST_AOS   IN     CHAR,
                                           p_COD_USR       IN     VARCHAR2,
                                           p_ERROR            OUT NUMBER);



   /************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_GEN_OPE_DEV_298
   -- Objetivo: Procedimiento almacenado Orquestador que llama al PL que inserta Operacion Para Devolver y luego al PL que actualiza la operacion original,
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas:
   -- Fecha: 24/03/17
   -- Autor: Santander--CAH
   -- Input:
   -- p_NUM_FOL_OPE        -->Foliador del banco, PK tabla detalle operaciones.
   -- p_FEC_ISR_OPE_ORG    -->Folio operacion de origen, PK tabla detalle operaciones.
   -- p_NUM_FOL_OPE_ORG    -->Fecha de insercion de operacion origina, PK tabla detalle operaciones.
   -- p_COD_DVI            -->Codigo de moneda
   -- p_IMP_OPE            -->Monto operacion
   -- p_COD_BCO_DTN        -->Codigo banco destino BIC
   -- p_NUM_REF_EXT        -->Codigo Referencia SWIFT externa
   -- p_NUM_FOL_MT_298     -->Folio operacion MT298, PK tabla detalle operaciones.
   -- p_FEC_ISR_MT_298     -->Fecha de insercion de operacion MT298, PK tabla detalle operaciones.
   -- p_COD_USR            -->Codigo usuario
   -- Output:
   --        p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE Sp_PAB_GEN_OPE_DEV_298 (p_FEC_ISR_OPE_ORG   IN     DATE,
                                     p_NUM_FOL_OPE_ORG   IN     NUMBER,
                                     p_COD_DVI           IN     CHAR,
                                     p_IMP_OPE           IN     NUMBER,
                                     p_COD_BCO_DTN       IN     CHAR,
                                     p_NUM_REF_EXT       IN     CHAR,
                                     p_NUM_FOL_MT_298    IN     NUMBER,
                                     p_FEC_ISR_MT_298    IN     DATE,
                                     --------------------------------
                                     p_COD_USR           IN     CHAR,
                                     p_NUM_FOL_OPE          OUT NUMBER,
                                     p_ERROR                OUT NUMBER);


   -- ***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_GES_DEB_PROT
   -- Objetivo: Procedimiento actualiza estado de la operacion tras un cargo
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: N/A
   -- Fecha: 08/08/17
   -- Autor: Santander-GSD
   -- Input:
   --      p_NUM_FOL_OPE_ORI    -> Numero de Operacion Origen
   --      p_FEC_ISR_OPE_ORI    -> Fecha de insercion de operacion origen
   --      p_NUM_CTA            -> Numero de cuenta de cargo
   --      p_NUM_FOL_OPE        -> Numero de Operacion 202
   --      p_FEC_ISR_OPE        -> Fecha de insercion de operacion 202
   --      p_NUM_REF_SWF        -> Numero de referencia Swift202
   --      p_NUM_MOV            -> N�mero de moviimento Swift
   --      p_NUM_DEV_TRANS      -> Numero devuelto por WS de Vigencia Salida
   --      p_FEC_CGO            -> Fecha Cargo Operacion
   --    � p_HRA_CGO            -> Hora Cargo Operacion
   --      p_COD_USR            -> Codigo de Usuario que se registrara en bitacora
   -- Output:
   --      p_ERROR              -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   PROCEDURE Sp_PAB_GES_DEB_PROT (p_NUM_FOL_OPE_ORI   IN     NUMBER,
                                  p_FEC_ISR_OPE_ORI   IN     DATE,
                                  p_NUM_CTA           IN     CHAR,
                                  p_NUM_FOL_OPE       IN     NUMBER,
                                  p_FEC_ISR_OPE       IN     DATE,
                                  p_NUM_REF_SWF       IN     CHAR,
                                  p_NUM_MOV           IN     VARCHAR2,
                                  p_FEC_CGO           IN     DATE,
                                  p_HRA_CGO           IN     CHAR,
                                  p_COD_USR           IN     CHAR,
                                  p_ERROR                OUT NUMBER);

   -- ***********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_BUS_DEB_PAT
   -- Objetivo: Procedimiento que consulta y lista las operaciones Por Abonar o Por Asociar
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_DETLL_NOMNA, PABS_DT_DETLL_OPRCN
   -- Fecha: 27/10/16
   -- Autor: Santander
   -- Input:
   -- p_COD_EST_AOS -> Estado de la operacion Por Abonar y/o Por Asociar
   -- p_MNTO -> Monto operacion
   -- p_COD_DVI-> Codigo de moneda
   -- p_NUM_REF_SWF -> Numero Referencia SWIFT
   -- p_COD_BCO -> Codigo de banco
   -- p_FEC_ISR_OPE -> Fecha de insercion de la operacion
   -- Output:
   -- p_CURSOR -> Cursor con las operaciones encontradas
   -- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   PROCEDURE SP_PAB_BUS_DEB_PAT (p_COD_EST_AOS   IN     CHAR,
                                 p_MNTO          IN     NUMBER,        --CHAR,
                                 p_COD_DVI       IN     CHAR,
                                 p_NUM_REF_SWF   IN     CHAR,
                                 p_COD_BCO       IN     CHAR,
                                 p_FEC_ISR_OPE   IN     DATE,
                                 p_CURSOR           OUT SYS_REFCURSOR,
                                 p_ERROR            OUT NUMBER);

   -- ***********************************************************************************************
   -- Funcion/Procedimiento: FN_PAB_TRAN_GLOSA_DEV
   -- Objetivo: Funcion que transforma la glosa que le corresponde a este estado
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PKG_PAB_CONSTANTES
   -- Fecha: 21/06/16
   -- Autor: Santander
   -- Input:
   -- p_GLS_DEV->     TEXTO A VALIDAR.
   -- Output: N/A.
   -- Input/Output: N/A.
   -- Retorno: p_DSC_GLS-> TEXTO VALIDADO
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   FUNCTION FN_PAB_TRAN_GLOSA_DEV (p_GLS_DEV IN VARCHAR2)
      RETURN VARCHAR;
END PKG_PAB_OPERACION_DEV;
/


CREATE OR REPLACE /* Formatted on 18-08-2020 17:45:48 (QP5 v5.115.810.9015) */
PACKAGE BODY DBO_PAB.PKG_PAB_OPERACION_DEV
AS
   /**  Package el cual contrendra todas las funciones y procedimientos relacionados con las operaciones de devolucion
   -- @Santander */

   -- ***********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_GEN_MT_DEV
   -- Objetivo: Procedimiento almacenado que busca detalle devolucion manual
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_DETLL_OPRCN y PABS_DT_OPRCN_INFCN_OPCON
   -- Fecha: 07/11/16
   -- Autor: Santander
   -- Input:
   -- p_FEC_ISR_OPE -> Fecha de insercion de operacion en el sistema, PK tabla detalle operaciones.
   -- p_NUM_FOL_OPE -> foliador del banco, PK tabla detalle operaciones.
   -- Output:
   -- p_CURSOR -> Cursor con las operaciones encontradas
   -- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Fecha actualizacion: 22-03-2017
   -- Observaciones: Se agrega campo COD_BCO_ORG(BCO_ORIGEN) al select del procedimiento
   -- ***********************************************************************************************
   PROCEDURE SP_PAB_GEN_MT_DEV (p_FEC_ISR_OPE   IN     DATE,
                                p_NUM_FOL_OPE   IN     NUMBER,
                                p_CURSOR           OUT SYS_REFCURSOR,
                                p_ERROR            OUT NUMBER)
   IS
      v_NOM_SP   VARCHAR2 (30) := 'SP_PAB_GEN_MT_DEV';
   --
   BEGIN
      P_ERROR := PKG_PAB_CONSTANTES.V_OK;

      OPEN p_CURSOR FOR
           SELECT   OPD.FEC_ISR_OPE AS Fecha_Insercion,
                    OPD.NUM_FOL_OPE AS Nro_Operacion,
                    OPD.NOM_BFC AS Nombre_Beneficiario,
                    OPD.NUM_FOL_NMN AS Numero_nomina,
                    --OPD.NUM_FOL_OPE_ORG                                             AS NUM_FOL_OPE_ORG,
                    OPD.NUM_OPE_SIS_ENT AS NUM_OPE_SIS_ENT,
                    OPD.FLG_EGR_ING AS FLG_EGR_ING,
                    DECODE (OPD.FLG_EGR_ING,
                            PKG_PAB_CONSTANTES.v_FLG_EGR,
                            PKG_PAB_CONSTANTES.v_STR_FLG_EGR,
                            PKG_PAB_CONSTANTES.v_STR_FLG_ING)
                       AS DSC_EGR_ING,
                    OPD.COD_SIS_SAL AS COD_SIS_SAL,
                    PKG_PAB_UTILITY.FN_PAB_OBT_DSC_SIS_ENT_SAL (
                       OPD.COD_SIS_SAL
                    )
                       AS DSC_SAL_DET,
                    PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SE_OOFF AS COD_SIS_ENT,
                    PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SE_OOFF AS Canal_Origen,
                    OPD.COD_EST_AOS AS COD_EST_AOS,
                    PKG_PAB_UTILITY.FN_PAB_OBT_DSC_COD_EST (OPD.COD_EST_AOS)
                       AS Motivo,
                    OPD.COD_TPO_ING AS COD_TPO_ING,
                    PKG_PAB_UTILITY.FN_PAB_OBT_TIP_ING (OPD.COD_TPO_ING)
                       AS DSC_TPO_ING,
                    OPD.FEC_ENV_RCP_SWF AS FEC_ENV_RCP_SWF,
                    OPD.HOR_ENV_RCP_SWF AS HOR_ENV_RCP_SWF,
                    OPD.FEC_ENV_RCP_SWF || ' ' || OPD.HOR_ENV_RCP_SWF
                       AS FECHA_DEVOLUCION,
                    -- OPD.COD_MT_SWF                                                  AS MT_SWIFT,
                    PKG_PAB_CONSTANTES.V_COD_MT202 AS MT_SWIFT,
                    OPD.FLG_MRD_UTZ_SWF AS FLG_MRD_UTZ_SWF,
                    DECODE (OPD.FLG_MRD_UTZ_SWF,
                            PKG_PAB_CONSTANTES.v_FLG_MRD_MN,
                            PKG_PAB_CONSTANTES.v_STR_FLG_MRD_MN,
                            PKG_PAB_CONSTANTES.v_STR_FLG_MRD_MX)
                       AS DSC_MRD_UTZ_SWF,
                    --OPD.COD_TPO_OPE_AOS                                             AS COD_TPO_OPE_AOS,
                    PKG_PAB_CONSTANTES.V_STR_TPO_OPE_DEVFDOS AS COD_TPO_OPE_AOS,
                    --PKG_PAB_UTILITY.FN_PAB_OBT_DSC_TIP_OPE(OPD.COD_TPO_OPE_AOS)     AS Tipo_Operacion,  -- Falta codigo en tabla maestra
                    OPD.FEC_VTA AS Fecha_Valuta,
                    OPD.COD_DVI AS Moneda,
                    OPD.IMP_OPE AS Monto,
                    OPD.COD_BCO_ORG AS COD_BCO_DTN,              --BCO_DESTINO
                    OPD.COD_BCO_DTN AS COD_BCO_ORG,               --BCO_ORIGEN
                    --PKG_PAB_UTILITY.FN_PAB_OBT_DSC_BCO(OPD.COD_BCO_DTN)             AS Banco_Destino,  -- Falta codigo en tabla maestra
                    DECODE (
                       TRIM (OPI.NUM_DOC_BFC),
                       NULL,
                       TRIM (OPI.NUM_DOC_BFC)
                       || DECODE (OPI.NUM_DOC_BFC, NULL, NULL, OPI.COD_TPD_BFC),
                          TRIM (OPI.NUM_DOC_BFC)
                       || '-'
                       || DECODE (OPI.NUM_DOC_BFC, NULL, NULL, OPI.COD_TPD_BFC)
                    )
                       AS Rut_Beneficiario,
                    OPD.NUM_CTA_BFC AS Cta_Beneficiario,
                    OPD.COD_SUC AS Sucursal,
                    OPD.NUM_REF_SWF AS NUM_REF_EXT,             --NUM_REF_SWF,
                    OPD.NUM_REF_EXT AS NUM_REF_SWF,          --AS NUM_REF_EXT,
                    OPD.GLS_ADC_EST AS GLS_ADC_EST,
                    OPD.NUM_REF_CTB AS NUM_REF_CTB,
                    ----------------    PABS_DT_OPRCN_INFCN_OPCON  -------------------------------
                    OPI.OBS_OPC_SWF AS OBS_OPC_SWF,
                    OPI.COD_BCO_BFC AS COD_BCO_BFC,        -- BCO_BENEFICIARIO
                    OPI.COD_BCO_ITD AS COD_BCO_ITD,
                    PKG_PAB_UTILITY.FN_PAB_OBT_DSC_BCO (OPI.COD_BCO_ITD)
                       AS DSC_BCO_ITD,
                    OPI.NUM_DOC_BFC AS NUM_DOC_BFC,
                    OPI.COD_TPD_BFC AS COD_TPD_BFC,
                    OPI.NUM_CTA_DCV_BFC AS NUM_CTA_DCV_BFC,
                    OPI.GLS_DIR_BFC AS GLS_DIR_BFC,
                    OPI.NOM_CDD_BFC AS NOM_CDD_BFC,
                    OPI.COD_PAS_BFC AS COD_PAS_BFC,
                    PKG_PAB_UTILITY.Fn_PAB_COMBO_PAIS (OPI.COD_PAS_BFC)
                       AS DSC_PAS_BFC,
                    OPI.NOM_ODN AS Nombre_Ordenante,
                    DECODE (
                       TRIM (OPI.NUM_DOC_ODN),
                       NULL,
                       TRIM (OPI.NUM_DOC_ODN)
                       || DECODE (OPI.NUM_DOC_ODN, NULL, NULL, OPI.COD_TPD_ODN),
                          TRIM (OPI.NUM_DOC_ODN)
                       || '-'
                       || DECODE (OPI.NUM_DOC_ODN, NULL, NULL, OPI.COD_TPD_ODN)
                    )
                       AS Rut_Ordenante,
                    OPI.NUM_CTA_ODN AS Cta_Ordenante,
                    OPI.NUM_CTA_DCV_ODN AS NUM_CTA_DCV_ODN,
                    OPI.GLS_DIR_ODN AS GLS_DIR_ODN,
                    OPI.NOM_CDD_ODN AS NOM_CDD_ODN,
                    OPI.COD_PAS_ODN AS COD_PAS_ODN,
                    PKG_PAB_UTILITY.Fn_PAB_COMBO_PAIS (OPI.COD_PAS_ODN)
                       AS DSC_PAS_ODN,
                    OPI.NUM_CLV_NGC AS NUM_CLV_NGC,
                    OPI.NUM_AGT AS NUM_AGT,
                    OPI.COD_FND_CCLV AS COD_FND_CCLV,
                    OPI.COD_TPO_SDO AS COD_TPO_SDO,
                    PKG_PAB_UTILITY.Fn_PAB_COMBO_TSALDO (OPI.COD_TPO_SDO)
                       AS DSC_TPO_SDO,
                    OPI.COD_TPO_CMA AS COD_TPO_CMA,
                    PKG_PAB_UTILITY.Fn_PAB_COMBO_CAMARA (OPI.COD_TPO_CMA)
                       AS DSC_TPO_CMA,
                    OPI.COD_TPO_FND AS COD_TPO_FND,
                    PKG_PAB_UTILITY.FN_PAB_TRAN_COD_FONDO (OPI.COD_TPO_FND)
                       AS DSC_TPO_FND,
                    DECODE (OPI.FEC_TPO_OPE_CCLV, '01-01-3000', NULL)
                       AS FEC_TPO_OPE_CCLV,
                    OPI.NUM_CLV_IDF AS NUM_CLV_IDF,
                    OPI.GLS_EST_RCH AS GLS_EST_RCH,
                    OPI.GLS_MTV_VSD AS GLS_MTV_VSD
             FROM   PABS_DT_DETLL_OPRCN OPD, PABS_DT_OPRCN_INFCN_OPCON OPI
            WHERE       OPD.FEC_ISR_OPE = p_FEC_ISR_OPE
                    AND OPD.NUM_FOL_OPE = p_NUM_FOL_OPE
                    AND OPD.NUM_FOL_OPE = OPI.NUM_FOL_OPE(+)
                    AND OPD.FEC_ISR_OPE = OPI.FEC_ISR_OPE(+)
         ORDER BY   OPD.NUM_FOL_OPE ASC;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg :=
            'No se han encontrado registos asociados a la busqueda por fecha :'
            || p_FEC_ISR_OPE
            || ' -operacion '
            || p_NUM_FOL_OPE;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END SP_PAB_GEN_MT_DEV;

   /************************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_GEN_OPE_DEV
   -- Objetivo: Procedimiento almacenado Orquestador que llama al PL que inserta Operacion Para Devolver y luego al PL que actualiza la operacion original,
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas:
   -- Fecha: 24/03/17
   -- Autor: Santander--CAH
   -- Input:
   -- p_NUM_FOL_OPE        -->Foliador del banco, PK tabla detalle operaciones.
   -- p_FEC_ISR_OPE_ORG    -->Folio operacion de origen, PK tabla detalle operaciones.
   -- p_NUM_FOL_OPE_ORG    -->Fecha de insercion de operacion origina, PK tabla detalle operaciones.
   -- p_COD_DVI            -->Codigo de moneda
   -- p_IMP_OPE            -->Monto operacion
   -- p_COD_BCO_DTN        -->Codigo banco destino BIC
   -- p_NUM_REF_EXT        -->Codigo Referencia SWIFT externa
   -- p_OBS_OPC_SWF        -->Observacion opcional
   -- p_COD_USR            -->Codigo usuario
   -- Output:
   -- p_ERROR              -->indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE Sp_PAB_GEN_OPE_DEV (p_FEC_ISR_OPE_ORG   IN     DATE,
                                 p_NUM_FOL_OPE_ORG   IN     NUMBER,
                                 p_COD_DVI           IN     CHAR,
                                 p_IMP_OPE           IN     NUMBER,
                                 p_COD_BCO_DTN       IN     CHAR,
                                 p_NUM_REF_EXT       IN     CHAR,
                                 ----------Opcionales-------
                                 p_OBS_OPC_SWF       IN     VARCHAR2,
                                 --------------------------------
                                 p_COD_USR           IN     CHAR,
                                 p_ERROR                OUT NUMBER,
                                 p_NUM_FOL_OPE          OUT NUMBER,
                                 p_FEC_ISR_OPE          OUT DATE)
   IS                                                     --Variable de salida
      --Seteo de Variables
      v_NOM_SP                  VARCHAR2 (30) := 'SP_PAB_GEN_OPE_DEV';
      v_FLG_MRD_UTZ_SWF         NUMBER;
      v_GLS_BIT                 VARCHAR2 (100);
      v_COD_SIS_SAL             CHAR (10);
      v_FEC_ISR_OPE             DATE := SYSDATE;
      v_NUM_DOC_BANCO_DESTINO   CHAR (11);
      v_COD_TPD_BANCO_DESTINO   CHAR (1);
      V_CTA_BANCO_DESTINO       NUMBER (1) := 0;
      V_RUT_BANCO_DESTINO       CHAR (9);
      v_flujo                   VARCHAR2 (10);
      V_OBS_OPC_SWF             CHAR (200);
      V_GLS_ADC_EST             VARCHAR2 (210);
      V_NUM_FOL_OPE             NUMBER (12) := SPK_PAB_PAG_EAM.NEXTVAL; --Nuevo forma de obtener Folio.
      V_OBS_OPC_SWF_FCN         VARCHAR2 (210);
      --Variables de notificacion

      p_ADJUNTO                 NUMBER := NULL;
      p_ID                      VARCHAR2 (100) := NULL;
      p_MENSAJE_SWF             VARCHAR2 (800) := NULL;
      p_ASUNTO                  VARCHAR2 (100) := NULL;
      p_CORREO                  VARCHAR2 (100) := NULL;
      p_CANT                    NUMBER (2) := 0;
   BEGIN
      -- Se crea procedimiento que busca Canal de Salida
      PKG_PAB_OPERACION.Sp_PAB_BUS_CANAL_SALIDA (
         p_COD_BCO_DTN,
         PKG_PAB_CONSTANTES.V_COD_MT202,
         p_IMP_OPE,
         PKG_PAB_CONSTANTES.V_STR_TPO_OPE_DEVFDOS,
         p_COD_DVI,
         v_COD_SIS_SAL,
         p_ERROR
      );

      V_OBS_OPC_SWF := P_OBS_OPC_SWF;

      --Obtenemos el flujo
      v_flujo := SUBSTR (V_OBS_OPC_SWF, 0, 4);

      --Si viene de una devolucion del proceso automatico
      IF (v_flujo = 'ESB1')
      THEN
         --Limpio el campo de observacion
         V_GLS_ADC_EST := REPLACE (V_OBS_OPC_SWF, 'ESB1:', ''); --Motivo de la devolucion
         V_OBS_OPC_SWF := V_GLS_ADC_EST; --'Se devuelve la operacion:' || p_NUM_REF_EXT;

         BEGIN
            UPDATE   PABS_DT_DETLL_OPRCN
               SET   COD_SIS_SAL = PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SE_OOFF
             WHERE   NUM_FOL_OPE = p_NUM_FOL_OPE_ORG
                     AND FEC_ISR_OPE = p_FEC_ISR_OPE_ORG;
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               err_code := SQLCODE;
               err_msg :=
                  'No se pudo actualizar el canal de salida de la operacion:'
                  || p_NUM_FOL_OPE_ORG
                  || ' '
                  || p_FEC_ISR_OPE_ORG;
            WHEN OTHERS
            THEN
               err_code := SQLCODE;
               err_msg := SUBSTR (SQLERRM, 1, 300);
               PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                   v_NOM_PCK,
                                                   err_msg,
                                                   v_NOM_SP);
               p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
               p_s_mensaje := err_code || '-' || err_msg;
               RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
         END;
      END IF;

      --Las devoluciones son solo para pesos
      IF (p_COD_DVI = PKG_PAB_CONSTANTES.V_STR_DVI_CLP)
      THEN
         BEGIN
            --Obtenemos rut del banco
            SELECT   TCNIFENT
              INTO   V_RUT_BANCO_DESTINO
              FROM   tcdt040
             WHERE   TGCDSWSA = p_COD_BCO_DTN AND TCNIFENT(+) <> v_BANCO;

            --SEPARAMOS RUT
            PKG_PAB_UTILITY.Sp_PAB_DESCOMp_RUT (V_RUT_BANCO_DESTINO,
                                                v_NUM_DOC_BANCO_DESTINO,
                                                v_COD_TPD_BANCO_DESTINO);
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               err_code := SQLCODE;
               err_msg :=
                  'No se pudo obtener el rut del banco:' || p_COD_BCO_DTN;
               V_RUT_BANCO_DESTINO := NULL;
            WHEN OTHERS
            THEN
               err_code := SQLCODE;
               err_msg := SUBSTR (SQLERRM, 1, 300);
               PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                   v_NOM_PCK,
                                                   err_msg,
                                                   v_NOM_SP);
               p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
               p_s_mensaje := err_code || '-' || err_msg;
               RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
         END;
      END IF;

      --CALCULAMOS MERCADO
      PKG_PAB_UTILITY.Sp_PAB_CAL_MER (p_COD_DVI, v_FLG_MRD_UTZ_SWF);

      -- Para guardar nuemero de secuencia en el Folio de la operacion.
      p_NUM_FOL_OPE := V_NUM_FOL_OPE;
      p_FEC_ISR_OPE := v_FEC_ISR_OPE;


      --CAMBIO EN LAS DEVOLUCIONES PARA GLOSAS

      V_OBS_OPC_SWF_FCN :=
         PKG_PAB_OPERACION_DEV.FN_PAB_TRAN_GLOSA_DEV (V_OBS_OPC_SWF);

      --

      --Se llama al procedimiento que inserta la nueva operacion, para devolver la original (Operacion relacionada a la Original)
      PKG_PAB_OPERACION.SP_PAB_INS_OPE (
         p_FEC_ISR_OPE,
         p_NUM_FOL_OPE,
         NULL,
         p_NUM_FOL_OPE_ORG,
         NULL,
         PKG_PAB_CONSTANTES.V_FLG_EGR,
         v_COD_SIS_SAL,
         PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SE_OOFF,
         PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAT,
         PKG_PAB_CONSTANTES.V_COD_TIP_ING_DIR,
         PKG_PAB_CONSTANTES.V_COD_MT202,
         v_FLG_MRD_UTZ_SWF,
         PKG_PAB_CONSTANTES.V_STR_TPO_OPE_DEVFDOS,
         TRUNC (SYSDATE),
         p_COD_DVI,
         p_IMP_OPE,
         p_COD_BCO_DTN,
         PKG_PAB_CONSTANTES.V_BIC_BANCO_SANTANDER,
         NULL,
         NULL,
         NULL,
         p_NUM_REF_EXT,
         v_NUM_DOC_BANCO_DESTINO,
         v_COD_TPD_BANCO_DESTINO,
         NULL,
         V_CTA_BANCO_DESTINO,
         PKG_PAB_CONSTANTES.V_COD_SUC_ORG,
         V_GLS_ADC_EST,
         NULL,
         NULL,
         NULL,
         NULL,
         NULL,
         NULL,
         NULL,
         NULL,
         PKG_PAB_CONSTANTES.V_NUM_DOC_BANCO_SANTANDER,
         PKG_PAB_CONSTANTES.V_COD_TPD_BANCO_SANTANDER,
         PKG_PAB_CONSTANTES.V_CTA_BANCO_SANTANDER,
         NULL,
         NULL,
         NULL,
         NULL,
         NULL,
         NULL,
         NULL,
         NULL,
         NULL,
         NULL,
         NULL,
         NULL,
         V_OBS_OPC_SWF_FCN,
         NULL,
         NULL,
         p_COD_USR,
         p_ERROR
      );

      --Se llama al procedimiento que inserta bitacora. Recibe Datos de la operacion recien creada
      v_GLS_BIT :=
            'Creacion de operacion, para Devolver Operacion: '
         || p_NUM_FOL_OPE_ORG
         || ' REF:'
         || p_NUM_REF_EXT;

      PKG_PAB_TRACKING.Sp_PAB_INS_BIT_OPE (
         p_NUM_FOL_OPE,
         v_FEC_ISR_OPE,
         p_COD_USR,
         PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAT,
         PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAT,
         v_GLS_BIT,
         p_ERROR
      );


      IF p_COD_USR <> 'ESB'
      THEN
         --Insertamos la notificacion por la creacion del MT202 DEVFDOS para devolver
         PKG_PAB_NOTIFICACION.SP_PAB_DET_CRRO_TIBCO (NULL,
                                                     p_NUM_FOL_OPE,
                                                     v_FEC_ISR_OPE,
                                                     'NUEDV',
                                                     NULL,
                                                     p_CANT,
                                                     NULL,
                                                     NULL,
                                                     NULL,
                                                     p_ADJUNTO,
                                                     p_ID,
                                                     p_MENSAJE_SWF,
                                                     p_ASUNTO,
                                                     p_CORREO,
                                                     p_ERROR);
      END IF;

      v_GLS_BIT :=
         'Se genera operacion:' || V_NUM_FOL_OPE || ' para devolucion';

      --Se llama al Procedimiento que actualiza la operacion orginal e inserta bitacora
      PKG_PAB_OPERACION.Sp_PAB_ACT_EST_OPERACION_BTC (
         p_NUM_FOL_OPE_ORG,
         p_FEC_ISR_OPE_ORG,
         NULL,
         PKG_PAB_CONSTANTES.V_STR_EST_AOS_OPEPDV,
         p_COD_USR,
         v_GLS_BIT,
         p_ERROR
      );

      --Se actualiza informacion de la operacion Original donde se indica el motivo por el cual se devuelve
      PKG_PAB_OPERACION.Sp_PAB_ACT_DATOS_OPE (p_NUM_FOL_OPE_ORG,
                                              p_FEC_ISR_OPE_ORG,
                                              NULL,
                                              NULL,
                                              NULL,
                                              V_GLS_ADC_EST,
                                              NULL,
                                              NULL,
                                              p_COD_USR,
                                              p_ERROR);

      --Se inserta la notificacion de esta operacion de devolucion

      --Insertamos el registro quer llguo y se devolvio
      PKG_PAB_CAJA_MN.SP_BUS_CALCULA_AREA_MN (
         PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SE_OOFF,
         p_IMP_OPE,
         v_cero,
         p_ERROR
      );
   --COMMIT;

   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END Sp_PAB_GEN_OPE_DEV;

   -- ***********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_BUS_OPE_ASO_SWF
   -- Objetivo: Procedimiento almacenado que retorna las operaciones origen posibles para asociar
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_DETLL_OPRCN_HTRCA, PABS_DT_OPRCN_OPCON_HTRCA
   -- Fecha: 07/11/16
   -- Autor: Santander
   -- Input:
   -- p_COD_BCO_DTN    -> codigo banco destino
   -- p_MTO_OPE        -> importe operacion
   -- p_FLG_EGR_ING    -> Tipo de operacion Ingreso = 1 o Egreso = 0
   -- p_FECHA_DES      -> Fecha desde
   -- p_FECHA_DES      -> Fecha hasta
   -- p_NUM_REF_SWF    -> Codigo Referencia SWIFT
   -- Output:
   -- p_CURSOR         -> Cursor con las operaciones posibles
   -- p_ERROR          -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   PROCEDURE SP_PAB_BUS_OPE_ASO_SWF (p_COD_BCO_DTN   IN     CHAR,
                                     p_MTO_OPE       IN     NUMBER,
                                     p_FLG_EGR_ING   IN     NUMBER,
                                     p_FECHA_DES     IN     DATE,
                                     p_FECHA_HAS     IN     DATE,
                                     p_NUM_REF_SWF   IN     CHAR,
                                     p_CURSOR           OUT SYS_REFCURSOR,
                                     p_ERROR            OUT NUMBER)
   IS
      --
      v_NOM_SP        VARCHAR2 (30) := 'SP_PAB_BUS_OPE_ASO_SWF';
      v_IMP_DES       NUMBER;
      v_IMP_HAS       NUMBER;
      v_FECHA_DES     DATE := TRUNC (SYSDATE);
      v_FECHA_HAS     DATE := TRUNC (SYSDATE);

      v_NUM_REF_SWF   CHAR (16);
      v_COD_BCO_DTN   CHAR (11);
   --
   BEGIN
      P_ERROR := PKG_PAB_CONSTANTES.V_OK;

      IF (p_FECHA_DES IS NOT NULL)
      THEN
         v_FECHA_DES := p_FECHA_DES;
      END IF;

      IF (p_FECHA_HAS IS NOT NULL)
      THEN
         v_FECHA_HAS := p_FECHA_HAS;
      END IF;


      IF NVL (p_NUM_REF_SWF, '#') = '#'
      THEN
         v_NUM_REF_SWF := NULL;
      ELSE
         v_NUM_REF_SWF := p_NUM_REF_SWF;
      END IF;

      --Siempre data
      IF p_MTO_OPE = -1
      THEN
         v_IMP_DES := 0;
         v_IMP_HAS := 9999999999999;
      ELSE
         v_IMP_DES := p_MTO_OPE;
         v_IMP_HAS := p_MTO_OPE;
      END IF;

      OPEN p_CURSOR FOR
         SELECT   DETOP.COD_SIS_SAL AS CANAL_PAGO,
                  DETOP.COD_MT_SWF AS MT_SWIFT,
                  DETOP.COD_TPO_OPE_AOS AS TIPO_OPERACION,
                  EST.DSC_EST_AOS AS ESTADO,
                  DETOP.FEC_VTA AS FECHA_VALUTA,
                  DETOP.NUM_REF_SWF AS NUM_REF_SWIFT,
                  DETOP.COD_DVI AS MONEDA,
                  DETOP.COD_SIS_ENT AS CANAL_ORIGEN,
                  DETOP.COD_BCO_DTN AS BANCO_DESTINO,
                  DETOP.IMP_OPE AS MONTO,
                  DETOP.NUM_FOL_NMN AS NUM_NOMINA,
                  DETOP.NUM_FOL_OPE AS NUM_OPERACION,
                  TO_DATE (
                        DETOP.FEC_ENV_RCP_SWF
                     || ' '
                     || LPAD (DETOP.HOR_ENV_RCP_SWF, 4, 0),
                     'DD-MM-YYY HH24:MI:SS'
                  )
                     AS FECHA_ENVIO_SWF,
                  DETOP.FEC_ISR_OPE AS FECHA_INS_OPE,
                  'N' AS ORIGEN
           FROM         PABS_DT_DETLL_OPRCN DETOP
                     LEFT JOIN
                        PABS_DT_OPRCN_INFCN_OPCON OPEOP
                     ON DETOP.NUM_FOL_OPE = OPEOP.NUM_FOL_OPE
                        AND DETOP.FEC_ISR_OPE = OPEOP.FEC_ISR_OPE
                  LEFT JOIN
                     PABS_DT_ESTDO_ALMOT EST
                  ON EST.COD_EST_AOS = DETOP.COD_EST_AOS
          WHERE       DETOP.COD_BCO_DTN = p_COD_BCO_DTN
                  AND DETOP.IMP_OPE BETWEEN v_IMP_DES AND v_IMP_HAS
                  AND DETOP.FEC_VTA BETWEEN v_FECHA_DES AND v_FECHA_HAS
                  AND NVL (DETOP.NUM_REF_SWF, '#') =
                        NVL (v_NUM_REF_SWF, NVL (DETOP.NUM_REF_SWF, '#'))
                  AND DETOP.COD_EST_AOS IN
                           (PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAG,
                            PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAGCFI)
                  AND DETOP.FLG_EGR_ING = p_FLG_EGR_ING
         UNION ALL
         SELECT   DETOP.COD_SIS_SAL AS CANAL_PAGO,
                  DETOP.COD_MT_SWF AS MT_SWIFT,
                  DETOP.COD_TPO_OPE_AOS AS TIPO_OPERACION,
                  EST.DSC_EST_AOS AS ESTADO,
                  DETOP.FEC_VTA AS FECHA_VALUTA,
                  DETOP.NUM_REF_SWF AS NUM_REF_SWIFT,
                  DETOP.COD_DVI AS MONEDA,
                  DETOP.COD_SIS_ENT AS CANAL_ORIGEN,
                  DETOP.COD_BCO_DTN AS BANCO_DESTINO,
                  DETOP.IMP_OPE AS MONTO,
                  DETOP.NUM_FOL_NMN AS NUM_NOMINA,
                  DETOP.NUM_FOL_OPE AS NUM_OPERACION,
                  TO_DATE (
                        DETOP.FEC_ENV_RCP_SWF
                     || ' '
                     || LPAD (DETOP.HOR_ENV_RCP_SWF, 4, 0),
                     'DD-MM-YYY HH24:MI:SS'
                  )
                     AS FECHA_ENVIO_SWF,
                  DETOP.FEC_ISR_OPE AS FECHA_INS_OPE,
                  'N' AS ORIGEN
           FROM         PABS_DT_DETLL_OPRCN_HTRCA DETOP
                     LEFT JOIN
                        PABS_DT_OPRCN_OPCON_HTRCA OPEOP
                     ON DETOP.NUM_FOL_OPE = OPEOP.NUM_FOL_OPE
                        AND DETOP.FEC_ISR_OPE = OPEOP.FEC_ISR_OPE
                  LEFT JOIN
                     PABS_DT_ESTDO_ALMOT EST
                  ON EST.COD_EST_AOS = DETOP.COD_EST_AOS
          WHERE       DETOP.COD_BCO_DTN = p_COD_BCO_DTN
                  AND DETOP.IMP_OPE BETWEEN v_IMP_DES AND v_IMP_HAS
                  AND DETOP.FEC_VTA BETWEEN v_FECHA_DES AND v_FECHA_HAS
                  AND NVL (DETOP.NUM_REF_SWF, '#') =
                        NVL (v_NUM_REF_SWF, NVL (DETOP.NUM_REF_SWF, '#'))
                  AND DETOP.COD_EST_AOS IN
                           (PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAG,
                            PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAGCFI)
                  AND DETOP.FLG_EGR_ING = p_FLG_EGR_ING;
   --Procedimiento ejecutado de forma correcta
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg :=
            'No se han encontrado registos asociados a la busqueda por fecha :'
            || v_FECHA_DES
            || '-fecha hasta '
            || v_FECHA_HAS
            || ' -referencia '
            || v_NUM_REF_SWF
            || '- banco '
            || p_COD_BCO_DTN;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END SP_PAB_BUS_OPE_ASO_SWF;


   -- ***********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_ASO_OPE_ORI_SWF
   -- Objetivo: Procedimiento almacenado que asocia operacion origen y actualiza estado de la operacion
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_DETLL_OPRCN
   -- Fecha: 07/11/16
   -- Autor: Santander
   -- Input:
   -- p_NUM_FOL_OPE     -> foliador del banco, PK tabla detalle operaciones.
   -- p_FEC_ISR_OPE     -> Fecha de insercion de operacion en el sistema, PK tabla detalle operaciones.
   -- p_NUM_REF_SWF     -> Numero de Refencia SWIFT
   -- p_NUM_FOL_OPE_ORG -> Numero de folio de la operacion origen
   -- p_FEC_ISR_OPE_ORG -> Fecha de insercion de la operacion origen
   -- p_COD_USR         -> Codigo de Usuario que se registrara en bitacora
   -- Output:
   -- p_ERROR          -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   PROCEDURE SP_PAB_ASO_OPE_ORI_SWF (p_NUM_FOL_OPE       IN     NUMBER,
                                     p_FEC_ISR_OPE       IN     DATE,
                                     p_NUM_REF_EXT       IN     CHAR,
                                     p_NUM_FOL_OPE_ORG   IN     NUMBER,
                                     p_FEC_ISR_OPE_ORG   IN     DATE,
                                     p_COD_USR           IN     CHAR,
                                     p_ERROR                OUT NUMBER)
   IS
      v_NOM_SP        VARCHAR2 (30) := 'SP_PAB_ASO_OPE_ORI_SWF';
      V_COD_EST_OPE_INI NUMBER (5)
            := PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAS ;
      V_COD_EST_OPE_UDP NUMBER (5)
            := PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAB ;
      v_FEC_ISR_OPE   DATE := SYSDATE;
      v_DSC_GLB_BTC   VARCHAR2 (100) := 'Se asocia con operacion origen: ';
      ERR_COD_INEX EXCEPTION;
   BEGIN
      --Actualiza el estado de la Operacion
      BEGIN
         UPDATE   PABS_DT_DETLL_OPRCN op
            SET   COD_EST_AOS = V_COD_EST_OPE_UDP,
                  NUM_REF_EXT = p_NUM_REF_EXT,
                  NUM_FOL_OPE_ORG = p_NUM_FOL_OPE_ORG --folio operacion devolucion
                                                     ,
                  FEC_ISR_OPE_ORG = p_FEC_ISR_OPE_ORG --fecha operacion devolucion
                                                     ,
                  FEC_ACT_OPE = SYSDATE   --Fecha actualizacion para Dashborad
                                       ,
                  GLS_ADC_EST =
                     v_DSC_GLB_BTC || NVL (p_NUM_REF_EXT, p_NUM_FOL_OPE_ORG)
          WHERE   NUM_FOL_OPE = p_NUM_FOL_OPE AND FEC_ISR_OPE = p_FEC_ISR_OPE;

         COMMIT;
      EXCEPTION
         WHEN OTHERS
         THEN
            --err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            p_s_mensaje := err_code || '-' || err_msg;
            RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
      END;

      --Se llama al procedimiento de bitacora para registrar el usuario que modifica el estado de la operacion
      PKG_PAB_TRACKING.Sp_PAB_INS_BIT_OPE (
         p_NUM_FOL_OPE,
         p_FEC_ISR_OPE,
         p_COD_USR,
         V_COD_EST_OPE_INI,
         V_COD_EST_OPE_UDP,
         v_DSC_GLB_BTC || NVL (p_NUM_REF_EXT, p_NUM_FOL_OPE_ORG),
         p_ERROR
      );

      --Se llama al Procedimiento que actualiza la operacion orginal y inserta bitacora
      PKG_PAB_OPERACION.Sp_PAB_ACT_EST_OPERACION (
         p_NUM_FOL_OPE_ORG,
         p_FEC_ISR_OPE_ORG,
         NULL,
         PKG_PAB_CONSTANTES.V_STR_EST_AOS_OPEPDV,
         p_COD_USR,
         p_ERROR
      );
   EXCEPTION
      WHEN ERR_COD_INEX
      THEN
         err_msg := 'Codigo inexistente:' || V_COD_EST_OPE_UDP;
         P_ERROR := PKG_PAB_CONSTANTES.V_ERROR;
         PKG_PAB_TRACKING.SP_PAB_INS_LOG_BD (err_code,
                                             V_NOM_PCK,
                                             err_msg,
                                             V_NOM_SP);
      WHEN DUP_VAL_ON_INDEX
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         P_ERROR := PKG_PAB_CONSTANTES.V_ERROR;
         PKG_PAB_TRACKING.SP_PAB_INS_LOG_BD (err_code,
                                             V_NOM_PCK,
                                             err_msg,
                                             V_NOM_SP);
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END SP_PAB_ASO_OPE_ORI_SWF;


   --
   -- ***********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_GES_ABO_MAN
   -- Objetivo: Procedimiento actualiza estado de la operacion devuelta y la operacion origen
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: N/A
   -- Fecha: 18/11/16
   -- Autor: Santander
   -- Input:
   -- p_NUM_FOL_OPE_ORI    -> Numero de Operacion Origen
   -- p_FEC_ISR_OPE_ORI    -> Fecha de insercion de operacion origen
   -- p_SIS_ENT_ORI        -> Nombre Canal de Entrada origen
   -- p_NUM_FOL_OPE_DEV    -> Numero de Operacion Devuelta
   -- p_FEC_ISR_OPE_DEV    -> Fecha de insercion de operacion devuelta
   -- p_NUM_CTA            -> Numero de Cuenta
   -- p_TIP_ABO_MAN        -> Codigo de tipo Abono
   -- p_NUM_DEV_TRANS      -> Numero devuelto por WS de Vigencia Salida
   -- p_COD_USR            -> Codigo de Usuario que se registrara en bitacora
   -- Output:
   -- p_ERROR              -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   PROCEDURE SP_PAB_GES_ABO_MAN (p_NUM_FOL_OPE_ORI   IN     NUMBER,
                                 p_FEC_ISR_OPE_ORI   IN     DATE,
                                 p_SIS_ENT_ORI       IN     CHAR,
                                 p_NUM_FOL_OPE_DEV   IN     NUMBER,
                                 p_FEC_ISR_OPE_DEV   IN     DATE,
                                 p_NUM_CTA           IN     CHAR,
                                 p_TIP_ABO_MAN       IN     CHAR,
                                 p_NUM_DEV_TRANS     IN     VARCHAR2,
                                 p_COD_USR           IN     CHAR,
                                 p_ERROR                OUT NUMBER)
   IS
      v_NOM_SP            VARCHAR2 (30) := 'SP_PAB_GES_ABO_MAN';
      v_DSC_GLB_TPO_ABO   VARCHAR2 (100) := ' Tipo Abono: ' || p_TIP_ABO_MAN;
      v_TIP_ABO_MAN       CHAR (10);
      v_COD_EST_OPE_PAG NUMBER (5)
            := PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAG ;
      v_COD_EST_OPE_DEV NUMBER (5)
            := PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEDEV ;
      v_DSC_EST_OPE_DEV   CHAR (3) := PKG_PAB_CONSTANTES.V_STR_EST_AOS_OPEDEV;
      v_DSC_GLB_BTC_DEV VARCHAR2 (100)
            := 'Abono N: ' || p_NUM_DEV_TRANS || ' , Referencia Operacion: ' ;
      v_CAN_SAL           CHAR (10);

      v_COD_EST_OPE_PAB NUMBER (5)
            := PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAB ;
      v_COD_EST_OPE_ABN NUMBER (5)
            := PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEABN ;
      v_DSC_EST_OPE_ABN   CHAR (3) := PKG_PAB_CONSTANTES.V_STR_EST_AOS_OPEABN;

      v_DSC_GLB_BTC       VARCHAR2 (100);
      v_NUM_REF_SWF       VARCHAR2 (100);
      v_NUM_CTA_BFC       VARCHAR2 (100);           -- CAH para abono cta cte.
      v_REFER_MVTO        VARCHAR2 (100); -- Referencia contable TPN, anteriormente debe llamar al pl Sp_PAB_OBT_REF_MVTO

      V_IMP_OPE           NUMBER;

      ERR_COD_INEX EXCEPTION;
   BEGIN
      P_ERROR := PKG_PAB_CONSTANTES.V_OK;

      IF p_TIP_ABO_MAN = PKG_PAB_CONSTANTES.V_COD_TIP_ABO_PUE
      THEN                                            -- Abono Puente Contable
         v_CAN_SAL := p_SIS_ENT_ORI;
         v_DSC_GLB_BTC :=
            'Se abona en cuenta Puente Contable N: ' || p_NUM_DEV_TRANS;
      ELSIF p_TIP_ABO_MAN = PKG_PAB_CONSTANTES.V_COD_TIP_ABO_VXP OR p_TIP_ABO_MAN = PKG_PAB_CONSTANTES.V_COD_TIP_ABO_VAC
      THEN                     -- Abono Pendientes Por Pagar o Varios Acreedor
         v_CAN_SAL := PKG_PAB_CONSTANTES.V_COD_TIP_ABO_XDIS;
         v_DSC_GLB_BTC := 'Se da de alta la Vigente N: ' || p_NUM_DEV_TRANS;
      ELSE
         v_CAN_SAL := PKG_PAB_CONSTANTES.V_COD_TIP_ABO_CTA;
         v_DSC_GLB_BTC :=
               'Se abono en la Cuenta Corriente N: '
            || p_NUM_CTA
            || ' , Abono N: '
            || p_NUM_DEV_TRANS;

         --Se inserta registro de tabla de cargos y abonos
         PKG_PAB_UTILITY.SP_PAB_INS_MVNTO_CARGO_ABONO (
            p_NUM_DEV_TRANS,
            p_NUM_FOL_OPE_DEV,
            p_FEC_ISR_OPE_DEV,
            PKG_PAB_CONSTANTES.V_FLG_MOV_ABN,
            p_ERROR
         );

         -- Si abono cta. cte. guardamos la cuenta en NUM_REF_CTB y el movimiento Abono NUM_MVT_CTB
         BEGIN
            UPDATE   PABS_DT_DETLL_OPRCN ope
               SET   NUM_REF_CTB = LPAD (p_NUM_CTA, 12, 0), ---0000 + N_cuenta completando 12 caracteres v_NUM_CTA_BFC
                     NUM_MVT_CTB = p_NUM_DEV_TRANS,
                     NUM_CTA_BFC = p_NUM_CTA            --LPAD(p_NUM_CTA,12,0)
             WHERE   NUM_FOL_OPE = p_NUM_FOL_OPE_DEV
                     AND FEC_ISR_OPE = p_FEC_ISR_OPE_DEV;
         EXCEPTION
            WHEN OTHERS
            THEN
               err_code := SQLCODE;
               err_msg := SUBSTR (SQLERRM, 1, 300);
               PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                   v_NOM_PCK,
                                                   err_msg,
                                                   v_NOM_SP);
               p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
               p_s_mensaje := err_code || '-' || err_msg;
               RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
         END;
      END IF;

      -- Actualiza el canal de Operacion Devuelta
      BEGIN
         UPDATE   PABS_DT_DETLL_OPRCN
            SET   COD_SIS_SAL = v_CAN_SAL,
                  FEC_ACT_OPE = SYSDATE, --Fecha de actualizacion para Dashborad
                  GLS_ADC_EST = v_DSC_GLB_BTC
          WHERE   NUM_FOL_OPE = p_NUM_FOL_OPE_DEV
                  AND FEC_ISR_OPE = p_FEC_ISR_OPE_DEV;
      EXCEPTION
         WHEN OTHERS
         THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            p_s_mensaje := err_code || '-' || err_msg;
            RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
      END;

      --Obtenemos La Ref SWIFT para ingresar en Glosa en la Operacion de Origen
      BEGIN
         SELECT   NUM_REF_SWF, IMP_OPE
           INTO   V_NUM_REF_SWF, V_IMP_OPE
           FROM   PABS_DT_DETLL_OPRCN
          WHERE   NUM_FOL_OPE = p_NUM_FOL_OPE_DEV
                  AND FEC_ISR_OPE = p_FEC_ISR_OPE_DEV;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            err_code := SQLCODE;
            err_msg :=
               'No se han encontrado registos asociados a la busqueda por folio :'
               || p_NUM_FOL_OPE_DEV
               || '-fecha '
               || p_FEC_ISR_OPE_DEV;
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            RAISE;
         WHEN OTHERS
         THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            p_s_mensaje := err_code || '-' || err_msg;
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
      END;

      -- Actualiza Estado Operacion Origen
      PKG_PAB_OPERACION_DEV.SP_PAB_ACT_EST_OPERACION_DEV (
         p_NUM_FOL_OPE_ORI,
         p_FEC_ISR_OPE_ORI,
         v_DSC_GLB_BTC_DEV || v_NUM_REF_SWF,
         NULL,
         v_DSC_EST_OPE_DEV,
         p_COD_USR,
         p_ERROR
      );

      PKG_PAB_OPERACION_DEV.SP_PAB_ACT_EST_OPERACION_DEV (p_NUM_FOL_OPE_DEV,
                                                          p_FEC_ISR_OPE_DEV,
                                                          v_DSC_GLB_BTC,
                                                          NULL,
                                                          v_DSC_EST_OPE_ABN,
                                                          p_COD_USR,
                                                          p_ERROR);

      --
      --Calculamos el saldo pro area
      PKG_PAB_CAJA_MN.SP_BUS_CALCULA_AREA_MN (v_CAN_SAL,
                                              V_IMP_OPE,
                                              v_cero,
                                              p_ERROR);

      COMMIT;
   EXCEPTION
      WHEN ERR_COD_INEX
      THEN
         err_msg := 'Problemas con gestion de abono manual';
         P_ERROR := PKG_PAB_CONSTANTES.V_ERROR;
         PKG_PAB_TRACKING.SP_PAB_INS_LOG_BD (err_code,
                                             V_NOM_PCK,
                                             err_msg,
                                             V_NOM_SP);
      WHEN DUP_VAL_ON_INDEX
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         P_ERROR := PKG_PAB_CONSTANTES.V_ERROR;
         PKG_PAB_TRACKING.SP_PAB_INS_LOG_BD (err_code,
                                             V_NOM_PCK,
                                             err_msg,
                                             V_NOM_SP);
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END SP_PAB_GES_ABO_MAN;

   --************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_BUS_SIS_SAL
   -- Objetivo: Procedimiento almacenado que Retorna Codigo de Sistema de Salida, segun Banco y Horario
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_BANCO_ALMOT,
   --          PABS_DT_SISTM_ENTRD_SALID,
   -- Fecha: 28/03/17
   -- Autor: Santander--CAH
   -- Input:
   -- p_COD_BCO_DTN    -->Codigo banco destino BIC
   -- p_COD_MT_SWF     -->Codigo MT SWF
   -- Output:
   -- p_COD_SIS_SAL    -->Codigo de sistema salida
   -- p_ERROR          -->indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/

   PROCEDURE Sp_PAB_BUS_SIS_SAL (p_COD_BCO_DTN   IN     CHAR,
                                 p_COD_MT_SWF    IN     NUMBER,
                                 p_COD_SIS_SAL      OUT CHAR,
                                 p_ERROR            OUT NUMBER)
   IS
      v_NOM_SP        VARCHAR2 (30) := 'Sp_PAB_BUS_SIS_SAL';
      v_MNT_BCO_DTN   NUMBER;
      v_COD_SIS_SAL   CHAR (4);
      v_HOR_SIS       NUMBER (4);
      v_HOR_INI       NUMBER (4);
      v_HOR_CRR       NUMBER (4);
      v_COD_MT_SWF    NUMBER;
   BEGIN
      -- Variable a futuro
      v_COD_MT_SWF := p_COD_MT_SWF;
      v_COD_SIS_SAL := p_COD_SIS_SAL;

      --Se busca Monto Linea del Banco
      SELECT   M.IMP_MAX_BIL_CBA
        INTO   v_MNT_BCO_DTN
        FROM   PABS_DT_BANCO_ALMOT M
       WHERE   M.COD_BIC_BCO = p_COD_BCO_DTN;

      --Verificamos si tiene linea
      IF v_MNT_BCO_DTN > 0
      THEN
         v_COD_SIS_SAL := PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SAL_CMN;

         BEGIN
            --Busca los horarios de Combanc en tabla parametrica
            SELECT   HOR_CRR_SIS
              INTO   v_HOR_CRR
              FROM   PABS_DT_SISTM_ENTRD_SALID
             WHERE   COD_SIS_ENT_SAL = v_COD_SIS_SAL
                     AND FLG_VGN = PKG_PAB_CONSTANTES.V_REG_VIG;
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               err_code := SQLCODE;
               err_msg :=
                  'No se han encontrado registos asociados a la busqueda por canal de salida/entrada :'
                  || v_COD_SIS_SAL;
               PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                   v_NOM_PCK,
                                                   err_msg,
                                                   v_NOM_SP);
               p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            WHEN OTHERS
            THEN
               err_code := SQLCODE;
               err_msg := SUBSTR (SQLERRM, 1, 300);
               PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                   v_NOM_PCK,
                                                   err_msg,
                                                   v_NOM_SP);
               p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
               p_s_mensaje := err_code || '-' || err_msg;
               RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
         END;

         BEGIN
            --Obtenemos la hora del sistema
            SELECT   TO_NUMBER (
                        REPLACE (TO_CHAR (SYSDATE, 'HH24:MI'), ':', '')
                     )
              INTO   v_HOR_SIS
              FROM   DUAL;
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               err_code := SQLCODE;
               err_msg :=
                  'No se han encontrado registos asociados a la busqueda por hora :'
                  || v_HOR_SIS;
               PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                   v_NOM_PCK,
                                                   err_msg,
                                                   v_NOM_SP);
               p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            WHEN OTHERS
            THEN
               err_code := SQLCODE;
               err_msg := SUBSTR (SQLERRM, 1, 300);
               PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                   v_NOM_PCK,
                                                   err_msg,
                                                   v_NOM_SP);
               p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
               p_s_mensaje := err_code || '-' || err_msg;
               RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
         END;

         IF v_HOR_SIS < v_HOR_CRR
         THEN
            v_COD_SIS_SAL := PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SAL_CMN;
            p_COD_SIS_SAL := v_COD_SIS_SAL;
         ELSE
            v_COD_SIS_SAL := PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SAL_LBTR;
            p_COD_SIS_SAL := v_COD_SIS_SAL;
         END IF;

         p_ERROR := PKG_PAB_CONSTANTES.v_OK; --Procedimiento ejecutado de forma correcta
      ELSE
         p_COD_SIS_SAL := PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SAL_LBTR;
      END IF;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg :=
            'No se han encontrado registos asociados a la busqueda por banco :'
            || p_COD_BCO_DTN;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END Sp_PAB_BUS_SIS_SAL;

   -- ***********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_CON_DEV_EGR
   -- Objetivo: Procedimiento que consulta y lista las operaciones Por Abonar o Por Asociar
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_DETLL_NOMNA, PABS_DT_DETLL_OPRCN
   -- Fecha: 27/10/16
   -- Autor: Santander
   -- Input:
   -- p_COD_EST_AOS    -> Estado de la operacion Por Abonar y/o Por Asociar
   -- p_MNTO_DES       -> Monto operacion desde
   -- p_MNTO_HAS       -> Monto operacion hasta
   -- p_COD_DVI        -> Codigo de moneda
   -- p_NUM_REF_SWF    -> Numero Referencia SWIFT
   -- p_COD_BCO        -> Codigo de banco
   -- Output:
   -- p_CURSOR         -> Cursor con las operaciones encontradas
   -- p_ERROR          -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   PROCEDURE SP_PAB_CON_DEV_EGR (p_COD_EST_AOS   IN     NUMBER,
                                 p_MNTO_DES      IN     NUMBER,
                                 p_MNTO_HAS      IN     NUMBER,
                                 p_COD_DVI       IN     CHAR,
                                 p_NUM_REF_SWF   IN     CHAR,
                                 p_COD_BCO       IN     CHAR,
                                 p_CURSOR           OUT SYS_REFCURSOR,
                                 p_ERROR            OUT NUMBER)
   IS
      --Seteo de Variables
      V_NOM_SP        VARCHAR2 (30) := 'SP_PAB_CON_DEV_EGR';
      v_IMP_DES       NUMBER;
      v_IMP_HAS       NUMBER;
      v_COD_EST_ABO   NUMBER;
      v_COD_EST_ASO   NUMBER;
      v_NUM_REF_SWF   CHAR (16);
      v_COD_BCO       CHAR (11);
   BEGIN
      P_ERROR := PKG_PAB_CONSTANTES.V_OK;

      --Siempre data
      IF p_MNTO_DES = -1
      THEN
         v_IMP_DES := 0;
      ELSE
         v_IMP_DES := p_MNTO_DES;
      END IF;

      --Siempre data
      IF p_MNTO_HAS = -1
      THEN
         v_IMP_HAS := 9999999999999;
      ELSE
         v_IMP_HAS := p_MNTO_HAS;
      END IF;

      IF p_COD_EST_AOS = -1
      THEN
         v_COD_EST_ABO := PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAB;
         v_COD_EST_ASO := PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAS;
      ELSE
         v_COD_EST_ABO := p_COD_EST_AOS;
         v_COD_EST_ASO := p_COD_EST_AOS;
      END IF;

      IF NVL (p_NUM_REF_SWF, '#') = '#'
      THEN
         v_NUM_REF_SWF := NULL;
      ELSE
         v_NUM_REF_SWF := UPPER (p_NUM_REF_SWF);
      END IF;

      IF NVL (p_COD_BCO, '#') = '#'
      THEN
         v_COD_BCO := NULL;
      ELSE
         v_COD_BCO := p_COD_BCO;
      END IF;

      OPEN p_CURSOR FOR
           SELECT   OPD.FEC_ISR_OPE AS Fecha_Insercion,
                    OPD.NUM_FOL_OPE AS Nro_Operacion,
                    OPD.NOM_BFC AS Nombre_Beneficiario,
                    OPD.NUM_FOL_NMN AS Numero_nomina,
                    OPD.NUM_FOL_OPE_ORG AS NUM_FOL_OPE_ORG,
                    OPD.FEC_ISR_OPE_ORG AS FEC_ISR_OPE_ORG,
                    OPD.NUM_OPE_SIS_ENT AS NUM_OPE_SIS_ENT,
                    OPD.FLG_EGR_ING AS FLG_EGR_ING,
                    DECODE (OPD.FLG_EGR_ING,
                            PKG_PAB_CONSTANTES.v_FLG_EGR,
                            PKG_PAB_CONSTANTES.v_STR_FLG_EGR,
                            PKG_PAB_CONSTANTES.v_STR_FLG_ING)
                       AS DSC_EGR_ING,
                    OPD.COD_SIS_SAL AS COD_SIS_SAL,
                    PKG_PAB_UTILITY.FN_PAB_OBT_DSC_SIS_ENT_SAL (
                       OPD.COD_SIS_SAL
                    )
                       AS DSC_SAL_DET,
                    OPD.COD_SIS_ENT AS COD_SIS_ENT,
                    PKG_PAB_UTILITY.FN_PAB_OBT_DSC_SIS_ENT_SAL (
                       OPD.COD_SIS_ENT
                    )
                       AS Canal_Origen,
                    OPD.COD_EST_AOS AS COD_EST_AOS,
                    PKG_PAB_UTILITY.FN_PAB_OBT_DSC_COD_EST (OPD.COD_EST_AOS)
                       AS Motivo,
                    OPD.COD_TPO_ING AS COD_TPO_ING,
                    PKG_PAB_UTILITY.FN_PAB_OBT_TIP_ING (OPD.COD_TPO_ING)
                       AS DSC_TPO_ING,
                    OPD.FEC_ENV_RCP_SWF AS FEC_ENV_RCP_SWF,
                    OPD.HOR_ENV_RCP_SWF AS HOR_ENV_RCP_SWF,
                    OPD.FEC_ENV_RCP_SWF || ' ' || OPD.HOR_ENV_RCP_SWF
                       AS FECHA_DEVOLUCION,
                    OPD.COD_MT_SWF AS MT_SWIFT,
                    OPD.FLG_MRD_UTZ_SWF AS FLG_MRD_UTZ_SWF,
                    DECODE (OPD.FLG_MRD_UTZ_SWF,
                            PKG_PAB_CONSTANTES.v_FLG_MRD_MN,
                            PKG_PAB_CONSTANTES.v_STR_FLG_MRD_MN,
                            PKG_PAB_CONSTANTES.v_STR_FLG_MRD_MX)
                       AS DSC_MRD_UTZ_SWF,
                    OPD.COD_TPO_OPE_AOS AS COD_TPO_OPE_AOS,
                    --PKG_PAB_UTILITY.FN_PAB_OBT_DSC_TIP_OPE(OPD.COD_TPO_OPE_AOS)     AS Tipo_Operacion,  -- Falta codigo en tabla maestra
                    OPD.FEC_VTA AS Fecha_Valuta,
                    OPD.COD_DVI AS Moneda,
                    OPD.IMP_OPE AS Monto,
                    OPD.COD_BCO_ORG AS COD_BCO_DTN,
                    TI.DES_BCO AS DES_BCO,
                    --PKG_PAB_UTILITY.FN_PAB_OBT_DSC_BCO(OPD.COD_BCO_DTN)             AS Banco_Destino,  -- Falta codigo en tabla maestra
                    DECODE (
                       TRIM (OPI.NUM_DOC_BFC),
                       NULL,
                       TRIM (OPI.NUM_DOC_BFC)
                       || DECODE (OPI.NUM_DOC_BFC, NULL, NULL, OPI.COD_TPD_BFC),
                          TRIM (OPI.NUM_DOC_BFC)
                       || '-'
                       || DECODE (OPI.NUM_DOC_BFC, NULL, NULL, OPI.COD_TPD_BFC)
                    )
                       AS Rut_Beneficiario,
                    OPD.NUM_CTA_BFC AS Cta_Beneficiario,
                    OPD.COD_SUC AS Sucursal,
                    OPD.NUM_REF_SWF AS NUM_REF_SWF,
                    OPD.NUM_REF_EXT AS NUM_REF_EXT,
                    OPD.GLS_ADC_EST AS GLS_ADC_EST,
                    OPD.NUM_REF_CTB AS NUM_REF_CTB,
                    ----------------    PABS_DT_NOMNA_INFCN_OPCON   -------------------------------
                    OPI.OBS_OPC_SWF AS OBS_OPC_SWF,
                    OPI.COD_BCO_BFC AS COD_BCO_BFC,
                    OPI.COD_BCO_ITD AS COD_BCO_ITD,
                    PKG_PAB_UTILITY.FN_PAB_OBT_DSC_BCO (OPI.COD_BCO_ITD)
                       AS DSC_BCO_ITD,
                    OPI.NUM_DOC_BFC AS NUM_DOC_BFC,
                    OPI.COD_TPD_BFC AS COD_TPD_BFC,
                    OPI.NUM_CTA_DCV_BFC AS NUM_CTA_DCV_BFC,
                    OPI.GLS_DIR_BFC AS GLS_DIR_BFC,
                    OPI.NOM_CDD_BFC AS NOM_CDD_BFC,
                    OPI.COD_PAS_BFC AS COD_PAS_BFC,
                    PKG_PAB_UTILITY.Fn_PAB_COMBO_PAIS (OPI.COD_PAS_BFC)
                       AS DSC_PAS_BFC,
                    OPI.NOM_ODN AS Nombre_Ordenante,
                    DECODE (
                       TRIM (OPI.NUM_DOC_ODN),
                       NULL,
                       TRIM (OPI.NUM_DOC_ODN)
                       || DECODE (OPI.NUM_DOC_ODN, NULL, NULL, OPI.COD_TPD_ODN),
                          TRIM (OPI.NUM_DOC_ODN)
                       || '-'
                       || DECODE (OPI.NUM_DOC_ODN, NULL, NULL, OPI.COD_TPD_ODN)
                    )
                       AS Rut_Ordenante,
                    OPI.NUM_CTA_ODN AS Cta_Ordenante,
                    OPI.NUM_CTA_DCV_ODN AS NUM_CTA_DCV_ODN,
                    OPI.GLS_DIR_ODN AS GLS_DIR_ODN,
                    OPI.NOM_CDD_ODN AS NOM_CDD_ODN,
                    OPI.COD_PAS_ODN AS COD_PAS_ODN,
                    PKG_PAB_UTILITY.Fn_PAB_COMBO_PAIS (OPI.COD_PAS_ODN)
                       AS DSC_PAS_ODN,
                    OPI.NUM_CLV_NGC AS NUM_CLV_NGC,
                    OPI.NUM_AGT AS NUM_AGT,
                    OPI.COD_FND_CCLV AS COD_FND_CCLV,
                    OPI.COD_TPO_SDO AS COD_TPO_SDO,
                    PKG_PAB_UTILITY.Fn_PAB_COMBO_TSALDO (OPI.COD_TPO_SDO)
                       AS DSC_TPO_SDO,
                    OPI.COD_TPO_CMA AS COD_TPO_CMA,
                    PKG_PAB_UTILITY.Fn_PAB_COMBO_CAMARA (OPI.COD_TPO_CMA)
                       AS DSC_TPO_CMA,
                    OPI.COD_TPO_FND AS COD_TPO_FND,
                    PKG_PAB_UTILITY.FN_PAB_TRAN_COD_FONDO (OPI.COD_TPO_FND)
                       AS DSC_TPO_FND,
                    DECODE (OPI.FEC_TPO_OPE_CCLV, '01-01-3000', NULL)
                       AS FEC_TPO_OPE_CCLV,
                    OPI.NUM_CLV_IDF AS NUM_CLV_IDF,
                    OPI.GLS_EST_RCH AS GLS_EST_RCH,
                    OPI.GLS_MTV_VSD AS GLS_MTV_VSD
             FROM   PABS_DT_DETLL_OPRCN OPD,
                    PABS_DT_OPRCN_INFCN_OPCON OPI,
                    TCDTBAI TI
            WHERE       OPD.COD_BCO_ORG = TI.TGCDSWSA
                    AND OPD.COD_EST_AOS IN (v_COD_EST_ABO, v_COD_EST_ASO) --Filtramos por los 2 estados PAB y PAS
                    AND OPD.IMP_OPE BETWEEN v_IMP_DES AND v_IMP_HAS
                    AND OPD.COD_DVI = NVL (p_COD_DVI, OPD.COD_DVI)
                    AND UPPER (NVL (OPD.NUM_REF_SWF, '#')) =
                          NVL (v_NUM_REF_SWF, NVL (OPD.NUM_REF_SWF, '#'))
                    AND NVL (OPD.COD_BCO_ORG, '#') =
                          NVL (v_COD_BCO, NVL (OPD.COD_BCO_ORG, '#'))
                    AND OPD.FLG_EGR_ING = PKG_PAB_CONSTANTES.V_FLG_ING
                    AND OPD.COD_MT_SWF = PKG_PAB_CONSTANTES.V_COD_MT202
                    AND OPD.COD_TPO_OPE_AOS =
                          PKG_PAB_CONSTANTES.V_STR_TPO_OPE_DEVFDOS
                    AND OPD.NUM_FOL_OPE = OPI.NUM_FOL_OPE(+)
                    AND OPD.FEC_ISR_OPE = OPI.FEC_ISR_OPE(+)
         ORDER BY   OPD.NUM_FOL_OPE ASC;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         p_s_mensaje := err_code || '-' || err_msg;
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         RAISE_APPLICATION_ERROR (-20000, err_code || p_s_mensaje);
   END SP_PAB_CON_DEV_EGR;

   -- ***********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_CMB_TIP_ABO
   -- Objetivo: Procedimiento que consulta y lista las tipos de abonos.
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: N/A
   -- Fecha: 07/11/16
   -- Autor: Santander
   -- Input:
   -- p_COD_MT_SWF -> Codigo SWIFT
   -- Output:
   -- p_CURSOR     -> Cursor con loa tipos de abonos
   -- p_ERROR      -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   PROCEDURE SP_PAB_CMB_TIP_ABO (p_COD_MT_SWF   IN     NUMBER,
                                 p_CURSOR          OUT SYS_REFCURSOR,
                                 p_ERROR           OUT NUMBER)
   IS
      --Seteo de Variables
      V_NOM_SP            VARCHAR2 (30) := 'SP_PAB_CMB_TIP_ABO';
      v_DSC_SIS_ENT_SAL   VARCHAR2 (50);
      v_AUX_SIS_ENT_SAL   VARCHAR2 (50) := 'AUX';
   BEGIN
      P_ERROR := PKG_PAB_CONSTANTES.V_OK;

      IF p_COD_MT_SWF = PKG_PAB_CONSTANTES.V_COD_MT103
      THEN
         v_AUX_SIS_ENT_SAL := PKG_PAB_CONSTANTES.V_COD_TIP_ABO_CTA;
      END IF;

      OPEN p_CURSOR FOR
         SELECT   COD_SIS_ENT_SAL AS COD_TIP, DSC_SIS_ENT_SAL AS DSC_TIP
           FROM   PABS_DT_SISTM_ENTRD_SALID
          WHERE   (TRIM (COD_SIS_ENT_SAL) LIKE v_AUX_SIS_ENT_SAL
                   OR FLG_SIS_ENT_SAL = PKG_PAB_CONSTANTES.V_FLG_SIS_ABN);
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg :=
            'No se han encontrado registos asociados al canal de salida :'
            || v_AUX_SIS_ENT_SAL;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);

         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END SP_PAB_CMB_TIP_ABO;

   --
   -- ***********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_BUS_CTA_ABO
   -- Objetivo: Procedimiento almacenado que busca numero relacionado a la cuenta del tipo abono
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_CENTA_CNTBL
   -- Fecha: 07/11/16
   -- Autor: Santander
   -- Input:
   -- p_COD_SIS_ENT_SAL ->  codigo sistema entrada salida
   -- p_COD_DVI        ->  Tipo de operaci?n Ingreso o Egreso
   -- Output:
   -- p_CURSOR     -> Cursor con registro encontrado
   -- p_ERROR      -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   PROCEDURE SP_PAB_BUS_CTA_ABO (p_COD_SIS_ENT_SAL   IN     CHAR,
                                 p_COD_DVI           IN     CHAR,
                                 p_CURSOR               OUT SYS_REFCURSOR,
                                 p_ERROR                OUT NUMBER)
   IS
      v_NOM_SP             VARCHAR2 (30) := 'SP_PAB_BUS_CTA_ABO';
      v_FLAG_MRD_UTZ_SWF   NUMBER (1);
   BEGIN
      P_ERROR := PKG_PAB_CONSTANTES.V_OK;

      IF p_COD_DVI = PKG_PAB_CONSTANTES.V_STR_DVI_CLP
      THEN
         v_FLAG_MRD_UTZ_SWF := PKG_PAB_CONSTANTES.V_FLG_MRD_MN;
      ELSE
         v_FLAG_MRD_UTZ_SWF := PKG_PAB_CONSTANTES.V_FLG_MRD_MX;
      END IF;

      OPEN p_CURSOR FOR
         SELECT   COD_SIS_ENT_SAL AS COD_SIS_ENT_SAL,
                  FLAG_MRD_UTZ_SWF AS FLAG_MRD_UTZ_SWF,
                  NUM_CTA_CTB AS NUM_CTA_CTB,
                  NUM_CRT_CTA_CTB AS NUM_CRT_CTA_CTB,
                  NUM_CNP_CTA_CTB AS NUM_CNP_CTA_CTB,
                  COD_SUC_DTN_TPN AS COD_SUC_DTN_TPN,
                  GLS_IFM_MVT_CTB AS GLS_IFM_MVT_CTB,
                  GLS_COM_CTA_CTB AS GLS_COM_CTA_CTB
           FROM   PABS_DT_CENTA_CNTBL
          WHERE       COD_SIS_ENT_SAL = p_COD_SIS_ENT_SAL
                  AND FLAG_MRD_UTZ_SWF = v_FLAG_MRD_UTZ_SWF
                  AND FLG_VGN = PKG_PAB_CONSTANTES.V_REG_VIG;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg :=
               'No se han encontrado registos asociados al canal de salida :'
            || p_COD_SIS_ENT_SAL
            || ' -flag mercado: '
            || v_FLAG_MRD_UTZ_SWF;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END SP_PAB_BUS_CTA_ABO;

   -- ***********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_COMBO_EST_DEV_EGR
   -- Objetivo: Procedimiento que consulta estados Por Abonar o Por Asociar
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_ESTDO_ALMOT
   -- Fecha: 27/10/16
   -- Autor: Santander
   -- Input:
   -- N/A
   -- Output:
   -- p_CURSOR -> Cursor con los registros de los 2 estados
   -- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   PROCEDURE SP_PAB_COMBO_EST_DEV_EGR (p_CURSOR   OUT SYS_REFCURSOR,
                                       p_ERROR    OUT NUMBER)
   IS
      --Seteo de Variables
      V_NOM_SP   VARCHAR2 (30) := 'SP_PAB_COMBO_EST_DEV_EGR';
   BEGIN
      P_ERROR := PKG_PAB_CONSTANTES.V_OK;

      OPEN p_CURSOR FOR
           SELECT   COD_EST_AOS AS CODIGO, DSC_EST_AOS AS DESCRIPCION
             FROM   PABS_DT_ESTDO_ALMOT
            WHERE   COD_EST_AOS IN
                          (PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAB,
                           PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAS)
         ORDER BY   1 ASC;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg :=
               'No se han encontrado registos asociados a los estados :'
            || PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAB
            || ' y  '
            || PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAS;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);

         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END SP_PAB_COMBO_EST_DEV_EGR;

   /************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_ACT_EST_OPERACION_DEV
   -- Objetivo: Procedimiento que actualiza el estado de una operacion DEVUELTA Y DE ORIGEN
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_DETLL_OPRCN
   -- Fecha: 20/04/2017
   -- Autor: Santander- CAH
   -- Input:    p_NUM_FOL_OPE-->   Numero de Operacion
   --           p_FEC_ISR_OPE-->   Fecha de insercion de la operacion
   --           p_GLS_BTC_OPE-->   Glosa de bitacora
   --           p_NUM_REF_SWF-->   Ref Swift
   --           p_COD_EST_AOS->   Identificador de nuevo estado.
   --           p_COD_USR->       rut usuario
   -- Output:
   --        p_ERROR       -->     indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE Sp_PAB_ACT_EST_OPERACION_DEV (p_NUM_FOL_OPE   IN     NUMBER,
                                           p_FEC_ISR_OPE   IN     DATE,
                                           p_GLS_BTC_OPE   IN     VARCHAR2,
                                           p_NUM_REF_SWF   IN     CHAR,
                                           p_COD_EST_AOS   IN     CHAR,
                                           p_COD_USR       IN     VARCHAR2,
                                           p_ERROR            OUT NUMBER)
   IS
      v_NOM_SP             VARCHAR2 (30) := 'Sp_PAB_ACT_EST_OPERACION_DEV';
      v_COD_EST_AOS        NUMBER;
      v_COD_EST_AOS_ACT    NUMBER;
      v_RESULT             NUMBER;
      v_NUM_FOL_OPE        NUMBER;
      v_FEC_ISR_OPE        DATE;
      v_NUM_REF_SWF        CHAR (16);
      V_GLS_ADC_EST        VARCHAR2 (210) := NULL;
      --
      v_NUM_FOL_OPE_NOM    NUMBER;
      ERROR_REFSWF EXCEPTION;
      v_ES_MOV_HISTORICO   NUMBER := 0;
   BEGIN
      -- Revisa si este movimiento debe estar en las tablas historicas
      IF (TRUNC (SYSDATE) - TRUNC (p_FEC_ISR_OPE) >
             PKG_PAB_CONSTANTES.V_DIA_RES_HIS)
      THEN
         v_ES_MOV_HISTORICO := 1;
      END IF;

      --Si la busqueda es por numero de referencia Swift, buscamos el numero de operacion y la fecha.
      -- El numero de operacion si es igual a 0 significa nulo para tibco.
      IF (NVL (p_NUM_REF_SWF, '#') <> '#' AND NVL (p_NUM_FOL_OPE, 0) = 0)
      THEN
         v_NUM_REF_SWF := p_NUM_REF_SWF;

         BEGIN
            SELECT   NUM_FOL_OPE, FEC_ISR_OPE
              INTO   v_NUM_FOL_OPE, v_FEC_ISR_OPE
              FROM   PABS_DT_DETLL_OPRCN
             WHERE   NUM_REF_SWF = v_NUM_REF_SWF;

            IF (v_NUM_FOL_OPE IS NULL OR v_FEC_ISR_OPE IS NULL)
            THEN
               RAISE ERROR_REFSWF;
            END IF;
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               err_code := SQLCODE;
               err_msg :=
                  'No se han encontrado registos asociados a la referencia :'
                  || v_NUM_REF_SWF;
               PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                   v_NOM_PCK,
                                                   err_msg,
                                                   v_NOM_SP);
               p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
               RAISE;
            WHEN OTHERS
            THEN
               err_code := SQLCODE;
               err_msg := SUBSTR (SQLERRM, 1, 300);
               p_s_mensaje := err_code || '-' || err_msg;
               PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                   v_NOM_PCK,
                                                   err_msg,
                                                   v_NOM_SP);
               p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
               RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
         END;
      ELSE
         v_NUM_FOL_OPE := p_NUM_FOL_OPE;
         v_FEC_ISR_OPE := p_FEC_ISR_OPE;
      END IF;

      --Estados
      v_COD_EST_AOS :=
         PKG_PAB_UTILITY.FN_PAB_OBT_COD_DSC_EST (
            p_COD_EST_AOS,
            PKG_PAB_CONSTANTES.V_IND_OPE
         );

      --Obtener el estado actual
      PKG_PAB_OPERACION.SP_PAB_OBT_EST_ACT_OPE (v_NUM_FOL_OPE,
                                                v_FEC_ISR_OPE,
                                                v_COD_EST_AOS_ACT);

      --Actualizamos estado nuevo de la operacion.
      IF (v_ES_MOV_HISTORICO = 0)
      THEN
         UPDATE   PABS_DT_DETLL_OPRCN
            SET   COD_EST_AOS = v_COD_EST_AOS, FEC_ACT_OPE = SYSDATE --Fecha de actulizacion para dashboard
          WHERE   NUM_FOL_OPE = v_NUM_FOL_OPE AND FEC_ISR_OPE = v_FEC_ISR_OPE;
      ELSE
         UPDATE   PABS_DT_DETLL_OPRCN_HTRCA
            SET   COD_EST_AOS = v_COD_EST_AOS, FEC_ACT_OPE = SYSDATE --Fecha de actulizacion para dashboard
          WHERE   NUM_FOL_OPE = v_NUM_FOL_OPE AND FEC_ISR_OPE = v_FEC_ISR_OPE;
      END IF;


      --Llamamos al procedimiento de Bitacora
      v_DES_BIT := p_GLS_BTC_OPE;
      PKG_PAB_TRACKING.Sp_PAB_INS_BIT_OPE (v_NUM_FOL_OPE,
                                           v_FEC_ISR_OPE,
                                           p_COD_USR,
                                           v_COD_EST_AOS_ACT,
                                           v_COD_EST_AOS,
                                           v_DES_BIT,
                                           v_RESULT);

      -- Valida existencia la operacion en nomina,
      BEGIN
         SELECT   NUM_FOL_OPE
           INTO   v_NUM_FOL_OPE_NOM
           FROM   PABS_DT_DETLL_NOMNA
          WHERE   NUM_FOL_OPE = v_NUM_FOL_OPE AND FEC_ISR_OPE = v_FEC_ISR_OPE;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            err_msg :=
               'Operacion no se encuentra en las tablas de nomina:'
               || p_NUM_FOL_OPE;
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (NULL,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
         WHEN OTHERS
         THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            p_s_mensaje := err_code || '-' || err_msg;
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
      END;

      IF v_NUM_FOL_OPE_NOM > 0
      THEN
         --Actualizamos el estado de la operacion en la tabla Nomina.
         PKG_PAB_NOMINA.Sp_PAB_ACT_EST_OPE_NOMINA (v_NUM_FOL_OPE,
                                                   v_FEC_ISR_OPE,
                                                   v_COD_EST_AOS,
                                                   V_GLS_ADC_EST,
                                                   v_RESULT);
      END IF;

      --Guardamos
      COMMIT;
      p_ERROR := PKG_PAB_CONSTANTES.v_OK;
   EXCEPTION
      WHEN ERROR_REFSWF
      THEN
         err_msg :=
            'No se encontro la operacion con el codigo de referencia:'
            || p_NUM_REF_SWF;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (NULL,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         p_s_mensaje := err_code || '-' || err_msg;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END Sp_PAB_ACT_EST_OPERACION_DEV;



   /************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_GEN_OPE_DEV_298
   -- Objetivo: Procedimiento almacenado Orquestador que llama al PL que inserta Operacion Para Devolver y luego al PL que actualiza la operacion original,
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas:
   -- Fecha: 24/03/17
   -- Autor: Santander--CAH
   -- Input:
   -- p_NUM_FOL_OPE        -->Foliador del banco, PK tabla detalle operaciones.
   -- p_FEC_ISR_OPE_ORG    -->Folio operacion de origen, PK tabla detalle operaciones.
   -- p_NUM_FOL_OPE_ORG    -->Fecha de insercion de operacion origina, PK tabla detalle operaciones.
   -- p_COD_DVI            -->Codigo de moneda
   -- p_IMP_OPE            -->Monto operacion
   -- p_COD_BCO_DTN        -->Codigo banco destino BIC
   -- p_NUM_REF_EXT        -->Codigo Referencia SWIFT externa
   -- p_NUM_FOL_MT_298     -->Folio operacion MT298, PK tabla detalle operaciones.
   -- p_FEC_ISR_MT_298     -->Fecha de insercion de operacion MT298, PK tabla detalle operaciones.
   -- p_COD_USR            -->Codigo usuario
   -- Output:
   --        p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE Sp_PAB_GEN_OPE_DEV_298 (p_FEC_ISR_OPE_ORG   IN     DATE,
                                     p_NUM_FOL_OPE_ORG   IN     NUMBER,
                                     p_COD_DVI           IN     CHAR,
                                     p_IMP_OPE           IN     NUMBER,
                                     p_COD_BCO_DTN       IN     CHAR,
                                     p_NUM_REF_EXT       IN     CHAR,
                                     p_NUM_FOL_MT_298    IN     NUMBER,
                                     p_FEC_ISR_MT_298    IN     DATE,
                                     --------------------------------
                                     p_COD_USR           IN     CHAR,
                                     p_NUM_FOL_OPE          OUT NUMBER,
                                     p_ERROR                OUT NUMBER)
   IS
      --Seteo de Variables
      v_NOM_SP                  VARCHAR2 (30) := 'SP_PAB_GEN_OPE_DEV_298';
      v_FLG_MRD_UTZ_SWF         NUMBER;
      v_GLS_BIT                 VARCHAR2 (100);
      v_COD_SIS_SAL             CHAR (10);
      v_FEC_ISR_OPE             DATE := SYSDATE;
      v_NUM_DOC_BANCO_DESTINO   CHAR (11);
      v_COD_TPD_BANCO_DESTINO   CHAR (1);
      v_CTA_BANCO_DESTINO       NUMBER (1) := 0;
      v_RUT_BANCO_DESTINO       CHAR (9);
      v_MENSAJE_DEV             VARCHAR2 (200);
      v_MENSAJE_202             VARCHAR2 (200) := 'POR SOLICITUD MT298';
      v_COD_EST_AOS_OPE         NUMBER (3) := 0;
      v_STR_EST_AOS_OPE         CHAR (3);
      v_REF_ORI                 VARCHAR2 (200);
      v_REF_298                 VARCHAR2 (200);
      v_COD_BCO_ORG             VARCHAR2 (200);
      v_COD_MT_SWF              CHAR (9);
      v_IMP_OPE                 VARCHAR2 (100);
      v_COD_TPO_OPE_AOS         CHAR (9);
      v_COD_DVI                 CHAR (9);
      v_COD_SIS_SAL_FIN         VARCHAR2 (10);
      v_NUM_REF_CTB             VARCHAR2 (23);
      v_GLS_ADC_EST             VARCHAR2 (210);
      v_NUM_FOL_OPE             NUMBER := SPK_PAB_PAG_EAM.NEXTVAL;
      v_COD_SIS_ENT             CHAR (10);
      v_COD_EST_OPE_ENT         NUMBER (3) := 0;
      v_NUM_CTA_BFC             VARCHAR2 (30);
      v_NUM_CTA_BFC_202         VARCHAR2 (30);
      v_ES_MOV_HISTORICO        NUMBER := 0;

      --Variables de notificacion

      p_ADJUNTO                 NUMBER := NULL;
      p_ID                      VARCHAR2 (100) := NULL;
      p_MENSAJE_SWF             VARCHAR2 (800) := NULL;
      p_ASUNTO                  VARCHAR2 (100) := NULL;
      p_CORREO                  VARCHAR2 (100) := NULL;
      p_CANT                    NUMBER (2) := 0;
   BEGIN
      --CALCULAMOS MERCADO
      PKG_PAB_UTILITY.Sp_PAB_CAL_MER (p_COD_DVI, v_FLG_MRD_UTZ_SWF);

      --Obtencion de datos
      BEGIN
         -- Revisa si este movimiento debe estar en las tablas historicas
         IF (TRUNC (SYSDATE) - TRUNC (p_FEC_ISR_OPE_ORG) >
                PKG_PAB_CONSTANTES.V_DIA_RES_HIS)
         THEN
            v_ES_MOV_HISTORICO := 1;
         END IF;

         --Recopilo datos de Operacion original (como operacion actual)
         IF (v_ES_MOV_HISTORICO = 0)
         THEN
            SELECT   NUM_REF_SWF,
                     COD_BCO_ORG,
                     COD_MT_SWF,
                     IMP_OPE,
                     COD_TPO_OPE_AOS,
                     COD_DVI,
                     COD_SIS_SAL,
                     NUM_CTA_BFC
              INTO   v_REF_ORI,
                     v_COD_BCO_ORG,
                     v_COD_MT_SWF,
                     v_IMP_OPE,
                     v_COD_TPO_OPE_AOS,
                     v_COD_DVI,
                     v_COD_SIS_SAL,
                     v_NUM_CTA_BFC
              FROM   PABS_DT_DETLL_OPRCN
             WHERE   NUM_FOL_OPE = p_NUM_FOL_OPE_ORG
                     AND FEC_ISR_OPE = p_FEC_ISR_OPE_ORG;
         ELSE
            -- En este caso hay que ir a buscar el movimiento historico
            SELECT   NUM_REF_SWF,
                     COD_BCO_ORG,
                     COD_MT_SWF,
                     IMP_OPE,
                     COD_TPO_OPE_AOS,
                     COD_DVI,
                     COD_SIS_SAL,
                     NUM_CTA_BFC
              INTO   v_REF_ORI,
                     v_COD_BCO_ORG,
                     v_COD_MT_SWF,
                     v_IMP_OPE,
                     v_COD_TPO_OPE_AOS,
                     v_COD_DVI,
                     v_COD_SIS_SAL,
                     v_NUM_CTA_BFC
              FROM   PABS_DT_DETLL_OPRCN_HTRCA
             WHERE   NUM_FOL_OPE = p_NUM_FOL_OPE_ORG
                     AND FEC_ISR_OPE = p_FEC_ISR_OPE_ORG;
         END IF;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            err_msg :=
                  'Operacion no se encuentra en las tablas operacion:'
               || p_NUM_FOL_OPE_ORG
               || '_ '
               || TO_DATE (p_FEC_ISR_OPE_ORG, 'DD/MM/YYYY HH24:MI:SS');
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (NULL,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
         WHEN OTHERS
         THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            p_s_mensaje := err_code || '-' || err_msg;
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
      END;

      BEGIN
         -- Datos mensaje
         SELECT   NUM_REF_SWF
           INTO   v_REF_298
           FROM   PABS_DT_MNSJE_PRTCZ
          WHERE   NUM_FOL_MSJ_PRZ = p_NUM_FOL_MT_298
                  AND FEC_ISR_MSJ_PRZ = p_FEC_ISR_MT_298;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            err_msg :=
                  'Operacion no se encuentra en las tablas mensaje:'
               || p_NUM_FOL_MT_298
               || '_'
               || TO_DATE (p_FEC_ISR_MT_298, 'DD/MM/YYYY HH24:MI:SS');
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (NULL,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
         WHEN OTHERS
         THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            p_s_mensaje := err_code || '-' || err_msg;
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
      END;


      --INICIO OBTENCION DE RUT DEL BENEFICIARIO

      BEGIN
         --Obtenemos rut del banco
         SELECT   TCNIFENT
           INTO   V_RUT_BANCO_DESTINO
           FROM   tcdt040
          WHERE   TGCDSWSA = v_COD_BCO_ORG AND TCNIFENT(+) <> v_BANCO;

         --SEPARAMOS RUT
         PKG_PAB_UTILITY.Sp_PAB_DESCOMp_RUT (V_RUT_BANCO_DESTINO,
                                             v_NUM_DOC_BANCO_DESTINO,
                                             v_COD_TPD_BANCO_DESTINO);
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            err_code := SQLCODE;
            err_msg := 'No se pudo obtener el rut del banco:' || v_COD_BCO_ORG;
            V_RUT_BANCO_DESTINO := NULL;
         WHEN OTHERS
         THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            p_s_mensaje := err_code || '-' || err_msg;
            RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
      END;


      --INICIO OBTENCION DE RUT DEL BENEFICIARIO

      --Obtencion canal de sallida segun banco y mt
      PKG_PAB_OPERACION.Sp_PAB_BUS_CANAL_SALIDA (v_COD_BCO_ORG,
                                                 v_COD_MT_SWF,
                                                 v_IMP_OPE,
                                                 v_COD_TPO_OPE_AOS,
                                                 v_COD_DVI,
                                                 v_COD_SIS_SAL_FIN,
                                                 p_ERROR);

      -- Para Sistema de Salida Cta Cte de operacion original -> estado por debitar, si no por autorizar
      IF v_COD_SIS_SAL = PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SAL_CTACTE
      THEN
         v_COD_EST_AOS_OPE := PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPDEB;
         v_STR_EST_AOS_OPE := PKG_PAB_CONSTANTES.V_STR_EST_AOS_OPEPDEB;
         v_NUM_REF_CTB := LPAD (v_NUM_CTA_BFC, 12, 0);
         v_NUM_CTA_BFC_202 := v_NUM_CTA_BFC;
         v_COD_SIS_ENT := PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SAL_CTACTE;
         v_COD_EST_OPE_ENT := PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPDEB;
      ELSE
         v_COD_EST_AOS_OPE := PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAT;
         v_STR_EST_AOS_OPE := PKG_PAB_CONSTANTES.V_STR_EST_AOS_OPEPAT;
         v_NUM_REF_CTB := NULL;
         v_NUM_CTA_BFC_202 := PKG_PAB_CONSTANTES.V_CTA_BANCO_SANTANDER;
         --v_COD_SIS_ENT     := PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SE_OOFF;
         v_COD_EST_OPE_ENT := PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAT;

         IF v_COD_SIS_ENT IS NULL
         THEN
            v_COD_SIS_ENT := PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SE_OOFF;
         END IF;
      END IF;

      v_MENSAJE_DEV :=
            'DEVOLUCION DE PAGO RECIBIDO DE LA OPERACION '
         || p_NUM_FOL_OPE_ORG
         || ' CON REFERENCIA '
         || v_REF_ORI
         || ' Y FECHA DE CREACION '
         || TO_DATE (p_FEC_ISR_OPE_ORG, 'DD/MM/YYYY HH24:MI:SS');

      --Se llama al procedimiento que inserta la nueva operacion, para devolver la original (Operacion relacionada a la Original)
      --Se debe cargar los mismos datos con excepcion de Numero de operacion : FOLIO recibido y estado nuevo: Autorizada
      PKG_PAB_OPERACION.SP_PAB_INS_OPE (
         v_FEC_ISR_OPE,
         v_NUM_FOL_OPE,
         NULL,
         p_NUM_FOL_OPE_ORG,
         NULL,
         PKG_PAB_CONSTANTES.V_FLG_EGR,
         v_COD_SIS_SAL_FIN,
         v_COD_SIS_ENT,
         v_COD_EST_AOS_OPE,
         PKG_PAB_CONSTANTES.V_COD_TIP_ING_DIR,
         PKG_PAB_CONSTANTES.V_COD_MT202,
         v_FLG_MRD_UTZ_SWF,
         PKG_PAB_CONSTANTES.V_STR_TPO_OPE_DEVFDOS,
         TRUNC (SYSDATE),
         p_COD_DVI,
         p_IMP_OPE,
         v_COD_BCO_ORG,
         PKG_PAB_CONSTANTES.V_BIC_BANCO_SANTANDER,
         NULL,
         NULL,
         NULL,
         v_REF_ORI,
         v_NUM_DOC_BANCO_DESTINO,
         v_COD_TPD_BANCO_DESTINO,
         NULL,
         V_CTA_BANCO_DESTINO,
         PKG_PAB_CONSTANTES.V_COD_SUC_ORG,
         v_MENSAJE_DEV,
         v_NUM_REF_CTB,
         NULL,
         NULL,
         NULL,
         NULL,
         NULL,
         NULL,
         NULL,
         PKG_PAB_CONSTANTES.V_NUM_DOC_BANCO_SANTANDER,
         PKG_PAB_CONSTANTES.V_COD_TPD_BANCO_SANTANDER,
         v_NUM_CTA_BFC_202,
         NULL,
         NULL,
         NULL,
         NULL,
         NULL,
         NULL,
         NULL,
         NULL,
         NULL,
         NULL,
         NULL,
         NULL,
         v_MENSAJE_202,
         NULL,
         NULL,
         p_COD_USR,
         p_ERROR
      );


      --Se llama al procedimiento que inserta bitacora. Recibe Datos de la operacion recien creada
      v_GLS_BIT :=
         'Creacion de operacion, para Devolver Operacion: '
         || p_NUM_FOL_OPE_ORG;
      PKG_PAB_TRACKING.Sp_PAB_INS_BIT_OPE (v_NUM_FOL_OPE,
                                           v_FEC_ISR_OPE,
                                           p_COD_USR,
                                           v_COD_EST_OPE_ENT,
                                           v_COD_EST_AOS_OPE,
                                           v_GLS_BIT,
                                           p_ERROR);
      v_GLS_ADC_EST :=
         'La operacion pendiente de su devolucion a solicitud del MT298 Referencia: '
         || v_REF_298;


      BEGIN
         IF (v_ES_MOV_HISTORICO = 0)
         THEN
            --Actualizamos referencia ext del original (actual)
            UPDATE   PABS_DT_DETLL_OPRCN
               SET   GLS_ADC_EST = v_GLS_ADC_EST
             WHERE   NUM_FOL_OPE = p_NUM_FOL_OPE_ORG
                     AND FEC_ISR_OPE = p_FEC_ISR_OPE_ORG;
         ELSE
            --Actualizamos referencia ext del historico
            UPDATE   PABS_DT_DETLL_OPRCN_HTRCA
               SET   GLS_ADC_EST = v_GLS_ADC_EST
             WHERE   NUM_FOL_OPE = p_NUM_FOL_OPE_ORG
                     AND FEC_ISR_OPE = p_FEC_ISR_OPE_ORG;
         END IF;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            err_msg :=
                  'Operacion no se encuentra en las tablas de operacion:'
               || p_NUM_FOL_OPE_ORG
               || '_'
               || TO_DATE (p_FEC_ISR_OPE_ORG, 'DD/MM/YYYY HH24:MI:SS');
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (NULL,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
         WHEN OTHERS
         THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            p_s_mensaje := err_code || '-' || err_msg;
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
      END;

      --Se llama al Procedimiento que actualiza la operacion orginal y inserta bitacora
      PKG_PAB_OPERACION.Sp_PAB_ACT_EST_OPERACION (
         p_NUM_FOL_OPE_ORG,
         p_FEC_ISR_OPE_ORG,
         NULL,
         PKG_PAB_CONSTANTES.V_STR_EST_AOS_OPEPDV,
         p_COD_USR,
         p_ERROR
      );

      BEGIN
         --Actualizamos referencia ext del 298
         UPDATE   PABS_DT_MNSJE_PRTCZ
            SET   NUM_REF_SWF_RLD = v_REF_ORI,
                  GLS_EST_RCH =
                     CONCAT ('Mensaje asociado a operacion : ',
                             p_NUM_FOL_OPE_ORG),
                  COD_TPO_OPE = PKG_PAB_CONSTANTES.V_STR_TPO_OPE_DEVFDOS,
                  NUM_FOL_OPE_ORI = p_NUM_FOL_OPE_ORG
          WHERE   NUM_FOL_MSJ_PRZ = p_NUM_FOL_MT_298
                  AND FEC_ISR_MSJ_PRZ = p_FEC_ISR_MT_298;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            err_msg :=
               'Operacion no se encuentra en las tablas de nomina:'
               || v_NUM_FOL_OPE;
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (NULL,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
         WHEN OTHERS
         THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            p_s_mensaje := err_code || '-' || err_msg;
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
      END;

      --Se actualiza estado de MT298 a asociado
      PKG_PAB_PROTOCOLO.Sp_PAB_ACT_EST_MNSJE_PRTCZ (
         p_NUM_FOL_MT_298,
         p_FEC_ISR_MT_298,
         PKG_PAB_CONSTANTES.V_STR_EST_AOS_OPEASO,
         '',
         p_COD_USR,
         p_ERROR
      );

      --Insertamos la notificacion por la creacion del MT202 DEVFDOS para devolver
      PKG_PAB_NOTIFICACION.SP_PAB_DET_CRRO_TIBCO (NULL,
                                                  v_NUM_FOL_OPE,
                                                  v_FEC_ISR_OPE,
                                                  'NUEDV',
                                                  NULL,
                                                  p_CANT,
                                                  NULL,
                                                  NULL,
                                                  NULL,
                                                  p_ADJUNTO,
                                                  p_ID,
                                                  p_MENSAJE_SWF,
                                                  p_ASUNTO,
                                                  p_CORREO,
                                                  p_ERROR);



      COMMIT;

      p_NUM_FOL_OPE := v_NUM_FOL_OPE;
   EXCEPTION
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END Sp_PAB_GEN_OPE_DEV_298;

   -- ***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_GES_DEB_PROT
   -- Objetivo: Procedimiento actualiza estado de la operacion tras un cargo
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: N/A
   -- Fecha: 08/08/17
   -- Autor: Santander-GSD
   -- Input:
   --      p_NUM_FOL_OPE_ORI    -> Numero de Operacion Origen
   --      p_FEC_ISR_OPE_ORI    -> Fecha de insercion de operacion origen
   --      p_NUM_CTA            -> Numero de cuenta de cargo
   --      p_NUM_FOL_OPE        -> Numero de Operacion 202
   --      p_FEC_ISR_OPE        -> Fecha de insercion de operacion 202
   --      p_NUM_REF_SWF        -> Numero de referencia Swift202
   --      p_NUM_MOV            -> Numero de moviimento Swift
   --      p_FEC_CGO            -> Fecha Cargo Operacion
   --    ' p_HRA_CGO            -> Hora Cargo Operacion
   --      p_COD_USR            -> Codigo de Usuario que se registrara en bitacora
   -- Output:
   --      p_ERROR              -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   PROCEDURE Sp_PAB_GES_DEB_PROT (p_NUM_FOL_OPE_ORI   IN     NUMBER,
                                  p_FEC_ISR_OPE_ORI   IN     DATE,
                                  p_NUM_CTA           IN     CHAR,
                                  p_NUM_FOL_OPE       IN     NUMBER,
                                  p_FEC_ISR_OPE       IN     DATE,
                                  p_NUM_REF_SWF       IN     CHAR,
                                  p_NUM_MOV           IN     VARCHAR2,
                                  p_FEC_CGO           IN     DATE,
                                  p_HRA_CGO           IN     CHAR,
                                  p_COD_USR           IN     CHAR,
                                  p_ERROR                OUT NUMBER)
   IS
      v_NOM_SP            VARCHAR2 (30) := 'Sp_PAB_GES_DEB_PROT ';
      v_COD_EST_OPE_DEV NUMBER (5)
            := PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEDEV ;
      v_DSC_EST_OPE_DEV   CHAR (3) := PKG_PAB_CONSTANTES.V_STR_EST_AOS_OPEDEV;
      v_DSC_GLB_BTC       VARCHAR2 (100);
      v_NUM_CTA           VARCHAR2 (100) := p_NUM_CTA; --Numero de cuenta corriente de la Operacion Origen
      ERR_COD_INEX EXCEPTION;
   BEGIN
      v_DSC_GLB_BTC :=
            'Se realiza el cargo en la Cuenta Corriente N: '
         || v_NUM_CTA
         || ' , N: '
         || p_NUM_MOV;

      err_msg := 'Se invoca PL Sp_PAB_GES_DEB_PROT para debito';
      PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                          v_NOM_PCK,
                                          err_msg,
                                          v_NOM_SP);


      --Se inserta registro de tabla de cargos y abonos
      PKG_PAB_UTILITY.SP_PAB_INS_MVNTO_CARGO_ABONO (
         p_NUM_MOV,
         p_NUM_FOL_OPE,                                   --p_NUM_FOL_OPE_ORI,
         p_FEC_ISR_OPE,                                   --p_FEC_ISR_OPE_ORI,
         PKG_PAB_CONSTANTES.V_FLG_MOV_CGO,
         p_ERROR
      );

      PKG_PAB_OPERACION.Sp_PAB_ACT_EST_OPERACION (
         p_NUM_FOL_OPE,
         p_FEC_ISR_OPE,
         p_NUM_REF_SWF,
         PKG_PAB_CONSTANTES.V_STR_EST_AOS_OPEPAT,
         p_COD_USR,
         p_ERROR
      );

      --Actualizamos el campo informacion adicional
      BEGIN
         UPDATE   PABS_DT_DETLL_OPRCN
            SET   GLS_ADC_EST = v_DSC_GLB_BTC
          WHERE   NUM_FOL_OPE = p_NUM_FOL_OPE_ORI
                  AND FEC_ISR_OPE = p_FEC_ISR_OPE_ORI;
      EXCEPTION
         WHEN OTHERS
         THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            p_s_mensaje := err_code || '-' || err_msg;
            RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
      END;

      COMMIT;

      P_ERROR := PKG_PAB_CONSTANTES.V_OK;
   EXCEPTION
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END Sp_PAB_GES_DEB_PROT;


   -- ***********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_BUS_DEB_PAT
   -- Objetivo: Procedimiento que consulta y lista las operaciones Por Abonar o Por Asociar
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_DETLL_NOMNA, PABS_DT_DETLL_OPRCN
   -- Fecha: 08/08/17
   -- Autor: Santander GSD(Actualizacion)
   -- Input:
   -- p_COD_EST_AOS -> Estado de la operacion Por Abonar y/o Por Asociar
   -- p_MNTO -> Monto operacion
   -- p_COD_DVI-> Codigo de moneda
   -- p_NUM_REF_SWF -> Numero Referencia SWIFT
   -- p_COD_BCO -> Codigo de banco
   -- p_FEC_ISR_OPE -> Fecha de insercion de la operacion
   -- Output:
   -- p_CURSOR -> Cursor con las operaciones encontradas
   -- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   PROCEDURE SP_PAB_BUS_DEB_PAT (p_COD_EST_AOS   IN     CHAR,
                                 p_MNTO          IN     NUMBER,        --CHAR,
                                 p_COD_DVI       IN     CHAR,
                                 p_NUM_REF_SWF   IN     CHAR,
                                 p_COD_BCO       IN     CHAR,
                                 p_FEC_ISR_OPE   IN     DATE,
                                 p_CURSOR           OUT SYS_REFCURSOR,
                                 p_ERROR            OUT NUMBER)
   IS
      --Seteo de Variables
      V_NOM_SP        VARCHAR2 (30) := 'SP_PAB_BUS_DEB_PAT';
      v_IMP           NUMBER;
      v_COD_EST_DEB   NUMBER;
      v_COD_EST_PAT   NUMBER;
      v_COD_BCO       CHAR (11);
      v_NUM_REF_SWF   VARCHAR2 (16) := UPPER (p_NUM_REF_SWF);
      v_FEC_DESDE     DATE;
      v_FEC_HASTA     DATE;
   BEGIN
      IF (p_FEC_ISR_OPE IS NOT NULL)
      THEN
         v_FEC_DESDE := p_FEC_ISR_OPE + INTERVAL '1' MINUTE;
      ELSE
         v_FEC_DESDE := v_dia_ini;
      END IF;

      IF (p_FEC_ISR_OPE IS NOT NULL)
      THEN
         v_FEC_HASTA :=
            p_FEC_ISR_OPE + INTERVAL '23' HOUR + INTERVAL '59' MINUTE;
      ELSE
         v_FEC_HASTA := v_dia_FIN;
      END IF;

      --Validacion Monto
      IF p_MNTO = -1
      THEN
         v_IMP := NULL;
      ELSE
         v_IMP := p_MNTO;
      END IF;

      v_COD_BCO := p_COD_BCO;
      P_ERROR := PKG_PAB_CONSTANTES.V_OK;

      --v_IMP := p_MNTO;

      IF (p_COD_EST_AOS = -1)
      THEN
         v_COD_EST_DEB := PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPDEB;
         v_COD_EST_PAT := PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAT;
      ELSE
         --Estados
         v_COD_EST_DEB := p_COD_EST_AOS;
         v_COD_EST_PAT := p_COD_EST_AOS;
      END IF;

      OPEN p_CURSOR FOR
         SELECT   OPD.NUM_FOL_OPE AS FOL_OPE,
                  OPD.FEC_ISR_OPE AS FEC_ISR_OPE,
                  OPD.COD_EST_AOS AS ESTADO,
                  OPD.NUM_FOL_NMN AS NUM_NMN,
                  OPD.NUM_REF_EXT AS COD_REF_EXT,
                  OPD.IMP_OPE AS MONTO,
                  OPD.COD_DVI AS MONEDA,
                  OPD.COD_BCO_DTN AS BANCO_DEVOLUCION,
                  TI.DES_BCO AS NOMBRE_BANCO,
                  OPD.FEC_VTA AS FECHA_CREACION,
                  OPD.COD_MT_SWF AS MT_SWF,
                  DECODE (OPD.NUM_DOC_BFC, NULL, NULL, OPD.COD_TPD_BFC)
                     AS RUT_BFC,
                  OPORI.IMP_OPE AS MONTO_ORI,
                  OPORI.COD_DVI AS MONEDA_ORI,
                  OPORI.COD_MT_SWF AS MT_SWIFT_ORI,
                  OPORI.NUM_FOL_OPE AS FOL_OPE_ORI,
                  OPORI.FEC_ISR_OPE AS FEC_ISR_OPE_ORI,
                  OPORI.NOM_BFC AS NOMBRE_ORI,
                  OPORI.NUM_CTA_BFC AS CUENTA_ORI,
                  OPORI.NUM_REF_SWF AS REF_SWF_ORI,
                  OPORI.NUM_CTA_BFC AS CTA_BFC,
                  OPORI.NUM_REF_SWF AS NUM_REF_SWF,
                  OPORI.NOM_BFC AS NOMBRE_BFC,
                  CONCAT (TRIM (OPORI.NUM_DOC_BFC), TRIM (OPORI.COD_TPD_BFC))
                     AS RUT_ORI
           FROM   TCDTBAI TI,
                     PABS_DT_DETLL_OPRCN OPD
                  LEFT JOIN
                     PABS_DT_DETLL_OPRCN OPORI
                  ON OPD.NUM_FOL_OPE_ORG = OPORI.NUM_FOL_OPE
                     AND OPD.NUM_REF_EXT = OPORI.NUM_REF_SWF
          WHERE   OPD.COD_EST_AOS IN
                        (NVL (v_COD_EST_DEB, OPD.COD_EST_AOS),
                         NVL (v_COD_EST_PAT, OPD.COD_EST_AOS))
                  AND TI.TGCDSWSA = OPD.COD_BCO_DTN
                  AND OPD.IMP_OPE = NVL (v_IMP, OPD.IMP_OPE)
                  AND OPD.COD_DVI = NVL (p_COD_DVI, OPD.COD_DVI)
                  AND NVL (UPPER (OPD.NUM_REF_EXT), '#') LIKE
                        NVL (v_NUM_REF_SWF,
                             NVL (UPPER (OPD.NUM_REF_EXT), '#'))
                        || '%'
                  AND OPD.COD_BCO_DTN = NVL (v_COD_BCO, OPD.COD_BCO_DTN)
                  AND OPD.FLG_EGR_ING = PKG_PAB_CONSTANTES.V_FLG_EGR
                  AND OPD.COD_MT_SWF = PKG_PAB_CONSTANTES.V_COD_MT202
                  AND OPD.COD_TPO_OPE_AOS =
                        PKG_PAB_CONSTANTES.V_STR_TPO_OPE_DEVFDOS
                  AND OPD.FEC_ISR_OPE BETWEEN NVL (v_FEC_DESDE,
                                                   OPD.FEC_ISR_OPE)
                                          AND  NVL (v_FEC_HASTA,
                                                    OPD.FEC_ISR_OPE)
                  AND OPORI.IMP_OPE IS NOT NULL
         UNION ALL
         /*  Historico   */
         SELECT   OPD.NUM_FOL_OPE AS FOL_OPE,
                  OPD.FEC_ISR_OPE AS FEC_ISR_OPE,
                  OPD.COD_EST_AOS AS ESTADO,
                  OPD.NUM_FOL_NMN AS NUM_NMN,
                  OPD.NUM_REF_EXT AS COD_REF_EXT,
                  OPD.IMP_OPE AS MONTO,
                  OPD.COD_DVI AS MONEDA,
                  OPD.COD_BCO_DTN AS BANCO_DEVOLUCION,
                  TI.DES_BCO AS NOMBRE_BANCO,
                  OPD.FEC_VTA AS FECHA_CREACION,
                  OPD.COD_MT_SWF AS MT_SWF,
                  DECODE (OPD.NUM_DOC_BFC, NULL, NULL, OPD.COD_TPD_BFC)
                     AS RUT_BFC,
                  OPORI.IMP_OPE AS MONTO_ORI,
                  OPORI.COD_DVI AS MONEDA_ORI,
                  OPORI.COD_MT_SWF AS MT_SWIFT_ORI,
                  OPORI.NUM_FOL_OPE AS FOL_OPE_ORI,
                  OPORI.FEC_ISR_OPE AS FEC_ISR_OPE_ORI,
                  OPORI.NOM_BFC AS NOMBRE_ORI,
                  OPORI.NUM_CTA_BFC AS CUENTA_ORI,
                  OPORI.NUM_REF_SWF AS REF_SWF_ORI,
                  OPORI.NUM_CTA_BFC AS CTA_BFC,
                  OPORI.NUM_REF_SWF AS NUM_REF_SWF,
                  OPORI.NOM_BFC AS NOMBRE_BFC,
                  CONCAT (TRIM (OPORI.NUM_DOC_BFC), TRIM (OPORI.COD_TPD_BFC))
                     AS RUT_ORI
           FROM   TCDTBAI TI,
                     PABS_DT_DETLL_OPRCN OPD
                  LEFT JOIN
                     PABS_DT_DETLL_OPRCN_HTRCA OPORI
                  ON OPD.NUM_FOL_OPE_ORG = OPORI.NUM_FOL_OPE
                     AND OPD.NUM_REF_EXT = OPORI.NUM_REF_SWF
          WHERE   OPD.COD_EST_AOS IN
                        (NVL (v_COD_EST_DEB, OPD.COD_EST_AOS),
                         NVL (v_COD_EST_PAT, OPD.COD_EST_AOS))
                  AND TI.TGCDSWSA = OPD.COD_BCO_DTN
                  AND OPD.IMP_OPE = NVL (v_IMP, OPD.IMP_OPE)
                  AND OPD.COD_DVI = NVL (p_COD_DVI, OPD.COD_DVI)
                  AND NVL (UPPER (OPD.NUM_REF_EXT), '#') LIKE
                        NVL (v_NUM_REF_SWF,
                             NVL (UPPER (OPD.NUM_REF_EXT), '#'))
                        || '%'
                  AND OPD.COD_BCO_DTN = NVL (v_COD_BCO, OPD.COD_BCO_DTN)
                  AND OPD.FLG_EGR_ING = PKG_PAB_CONSTANTES.V_FLG_EGR
                  AND OPD.COD_MT_SWF = PKG_PAB_CONSTANTES.V_COD_MT202
                  AND OPD.COD_TPO_OPE_AOS =
                        PKG_PAB_CONSTANTES.V_STR_TPO_OPE_DEVFDOS
                  AND OPD.FEC_ISR_OPE BETWEEN NVL (v_FEC_DESDE,
                                                   OPD.FEC_ISR_OPE)
                                          AND  NVL (v_FEC_HASTA,
                                                    OPD.FEC_ISR_OPE)
                  AND OPORI.IMP_OPE IS NOT NULL
         ORDER BY   FOL_OPE ASC;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg :=
            'No se han encontrado registos asociados al banco :' || v_COD_BCO;
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         p_s_mensaje := err_code || '-' || err_msg;
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         RAISE_APPLICATION_ERROR (-20000, err_code || p_s_mensaje);
   END SP_PAB_BUS_DEB_PAT;

   -- ***********************************************************************************************
   -- Funcion/Procedimiento: FN_PAB_TRAN_GLOSA_DEV
   -- Objetivo: Funcion que transforma la glosa que le corresponde a este estado
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PKG_PAB_CONSTANTES
   -- Fecha: 21/06/16
   -- Autor: Santander
   -- Input:
   -- p_GLS_DEV->     TEXTO A VALIDAR.
   -- Output: N/A.
   -- Input/Output: N/A.
   -- Retorno: p_DSC_GLS-> TEXTO VALIDADO
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   FUNCTION FN_PAB_TRAN_GLOSA_DEV (p_GLS_DEV IN VARCHAR2)
      RETURN VARCHAR
   IS
      V_NOM_SP        VARCHAR2 (22) := 'FN_PAB_TRAN_GLOSA_DEV';
      p_DSC_GLS       VARCHAR2 (100) := '';
      p_DSC_GLS_FNC   VARCHAR2 (100) := '';
      p_LEVEL_FNC     NUMBER;
   BEGIN
      --

      BEGIN
             SELECT   LEVEL AS ID, REGEXP_SUBSTR (TRIM (p_GLS_DEV),
                                                  '[^@]+',
                                                  1,
                                                  LEVEL)
                                      AS data
               INTO   p_LEVEL_FNC, p_DSC_GLS_FNC
               FROM   DUAL
              WHERE   LEVEL = 2
         CONNECT BY   REGEXP_SUBSTR (TRIM (p_GLS_DEV),
                                     '[^@]+',
                                     1,
                                     LEVEL) IS NOT NULL;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            p_DSC_GLS := TRIM (p_GLS_DEV);
      END;

      --
      IF NVL (TRIM (p_DSC_GLS_FNC), '#') = '#'
      THEN
         p_DSC_GLS := TRIM (p_GLS_DEV);
      ELSE
         p_DSC_GLS :=
            SUBSTR (TRIM (p_DSC_GLS_FNC), 11, LENGTH (TRIM (p_DSC_GLS_FNC)));
      END IF;

      --
      RETURN p_DSC_GLS;
   --
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   --
   END FN_PAB_TRAN_GLOSA_DEV;
END PKG_PAB_OPERACION_DEV;
/


CREATE SYNONYM USR_PAB.PKG_PAB_OPERACION_DEV FOR DBO_PAB.PKG_PAB_OPERACION_DEV
/


GRANT EXECUTE ON DBO_PAB.PKG_PAB_OPERACION_DEV TO R_APP_PAB
/

GRANT EXECUTE ON DBO_PAB.PKG_PAB_OPERACION_DEV TO USR_PAB
/

