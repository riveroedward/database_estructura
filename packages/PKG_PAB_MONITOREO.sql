CREATE OR REPLACE /* Formatted on 18-08-2020 17:45:43 (QP5 v5.115.810.9015) */
PACKAGE DBO_PAB.PKG_PAB_MONITOREO
AS
   -- Package el cual contrendra todas las funciones y procedimientos relacionados con las nominas
   -- //@gcorrea
   /******************************************************************************
      NAME:       PKG_PAB_MONITOREO
      PURPOSE:

      REVISIONS:
      Ver        Date        Author           Description
      ---------  ----------  ---------------  ------------------------------------
      1.0        11-07-2016             1. Created this package.
   ******************************************************************************/
   V_NOM_PCK             VARCHAR2 (30) := 'PKG_PAB_MONITOREO';
   ERR_CODE              NUMBER := 0;
   ERR_MSG               VARCHAR2 (300);
   V_DES                 VARCHAR2 (300);
   V_DES_BIT             VARCHAR2 (100);
   V_BANCO               VARCHAR2 (9) := '970150005';

   TYPE T_NUM_FOL_NMN
   IS
      TABLE OF NUMBER
         INDEX BY PLS_INTEGER;

   p_s_mensaje           VARCHAR2 (400); --BORRA CUANDO SE PASE A PCKS ORIGINAL
   v_dia_ini             DATE := TRUNC (SYSDATE) + INTERVAL '1' MINUTE;
   v_dia_fin DATE
         := TRUNC (SYSDATE) + INTERVAL '23' HOUR + INTERVAL '59' MINUTE ;
   v_dia_hoy             DATE := TRUNC (SYSDATE);

   --Variables para  orden de estados
   v_EST_VAL             NUMBER := 1;
   v_EST_DUP             NUMBER := 2;
   v_EST_INV             NUMBER := 3;
   v_EST_ELI             NUMBER := 4;
   v_EST_CUR             NUMBER := 5;
   v_EST_REC             NUMBER := 6;
   v_EST_AUT             NUMBER := 7;

   --Variables para  orden de estados Ingreso
   v_EST_EGR_OPE_PVI     NUMBER := 1;
   v_EST_EGR_OPEINGAR    NUMBER := 2;
   v_EST_EGR_OPEPDEB     NUMBER := 3;
   v_EST_EGR_OPEPAT      NUMBER := 4;
   v_EST_EGR_OPEAUT      NUMBER := 5;
   v_EST_EGR_OPELIB      NUMBER := 6;
   v_EST_EGR_OPEENV      NUMBER := 7;
   v_EST_EGR_OPEPAG      NUMBER := 8;
   v_EST_EGR_OPEPAGCFI   NUMBER := 9;
   v_EST_EGR_OPEREC      NUMBER := 10;
   v_EST_EGR_OPEPDV      NUMBER := 11;
   v_EST_EGR_OPEDEV      NUMBER := 12;
   v_EST_EGR_OPEELI      NUMBER := 13;

   --    --Variables para  orden de estados
   V_EST_REG             NUMBER := 1;
   v_EST_PAS             NUMBER := 2;
   v_EST_PAB             NUMBER := 3;
   v_EST_ABN             NUMBER := 4;
   v_EST_PDV             NUMBER := 5;
   v_EST_DEV             NUMBER := 6;

   V_IND_NOM             NUMBER := 0;
   V_IND_OPE             NUMBER := 1;

   --***********************************************************************************************
   -- Funcion/Procedimiento: FN_PAB_BUS_ULT_REG_EST
   -- Objetivo: Consulta ultimo registro por estado de nomina
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01
   -- Tablas Usadas: PABS_DT_ESTDO_ALMOT, PABS_DT_CBCRA_NOMNA,PABS_DT_SISTM_ENTRD_SALID
   -- Fecha: 25/10/2016
   -- Autor: gcorrea
   -- Input:
   -- P_COD_EST_AOS -> CODIGO ESTADO
   -- P_COD_SIS_ENT -> CODIGO SISTEMA
   -- P_IND_NOM_OPE -> INDICA SI ES NOMINA 0 OPERACION 1
   -- N/A
   -- Output:
   -- N/A.
   -- Input/Output:
   -- N/A.
   -- Retorno:
   -- V_ULTIMO_REGISTRO -> valor de retorno que muestra la ultima fecha y horario por estado de nomina
   -- Observaciones: <Fecha y Detalle de ?ltimos cambios>
   --***********************************************************************************************
   FUNCTION FN_PAB_BUS_ULT_REG_EST (P_COD_EST_AOS   IN NUMBER,
                                    P_COD_SIS_ENT   IN CHAR,
                                    P_IND_NOM_OPE      NUMBER)
      RETURN VARCHAR2;

   -- ***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_CANT_NMN_EST
   -- Objetivo: Procedimiento almacenado que cuenta por el tipo de estado cada una de las nominas
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_DETLL_NOMNA
   -- Fecha: 24/10/16
   -- Autor: gcorrea
   -- Input:
   -- P_COD_SIS_ENT -> codigo de sistema entrada
   -- P_COD_EST_AOS -> codigo estado
   -- Output:
   -- V_CANT_NMN -> CANTIDAD DE NOMINAS POR ESTADO
   -- p_ERROR-> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ?ltimos cambios>
   -- ***********************************************************************************************
   PROCEDURE Sp_PAB_CANT_NMN_EST (P_COD_EST_AOS   IN     NUMBER,
                                  P_COD_SIS_ENT   IN     CHAR,
                                  p_CURSOR           OUT SYS_REFCURSOR,
                                  P_ERROR            OUT NUMBER);

   -- ***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_EST_NMN_DSH
   -- Objetivo: Procedimiento almacenado que obtine los estados de las nominas para el dashboard segun el rol
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_DETLL_NOMNA
   -- Fecha: 21/06/16
   -- Autor: gcorrea
   -- Input:
   -- p_COD_TROL -> tipo de rol
   -- p_COD_SIS_ENT -> canal entrada salida
   -- Output:
   -- p_ERROR-> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- p_CURSOR-> cursor de salida de resultado
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ?ltimos cambios>
   -- ***********************************************************************************************
   PROCEDURE Sp_PAB_EST_NMN_DSH (p_COD_TROL      IN     NUMBER,
                                 p_COD_SIS_ENT   IN     CHAR,
                                 p_CURSOR           OUT SYS_REFCURSOR,
                                 p_ERROR            OUT NUMBER);

   -- ***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_EST_OPE_DSH
   -- Objetivo: Procedimiento almacenado que obtine los estados de las operaciones para el dashboard segun el rol
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_DETLL_NOMNA
   -- Fecha: 21/06/16
   -- Autor: gcorrea
   -- Input:
   -- p_COD_TROL -> Tipo de rol
   -- p_COD_SIS_ENT -> Codigo de sistema entrada
   -- Output:
   -- p_CURSOR-> Cursor de salida
   -- p_ERROR-> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de Ultimos cambios>
   -- ***********************************************************************************************
   PROCEDURE Sp_PAB_EST_OPE_DSH (p_COD_TROL      IN     NUMBER,
                                 p_COD_SIS_ENT   IN     CHAR,
                                 p_CURSOR           OUT SYS_REFCURSOR,
                                 p_ERROR            OUT NUMBER);

   -- ***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_CANT_OPE_EST
   -- Objetivo: Procedimiento almacenado que cuenta por el tipo de estado cada una de las operaciones
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_DETLL_OPRCN, PABS_DT_ESTDO_ALMOT, PABS_DT_SISTM_ENTRD_SALID
   -- Fecha: 24/10/16
   -- Autor: gcorrea
   -- Input:
   -- P_COD_EST_AOS -> codigo de estado de operacion
   -- P_COD_SIS_ENT -> codigo de sistema entrada
   -- Output:
   -- p_CURSOR -> cursor de resultado
   -- p_ERROR-> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ?ltimos cambios>
   -- ***********************************************************************************************
   PROCEDURE Sp_PAB_CANT_OPE_EST (P_COD_EST_AOS   IN     NUMBER,
                                  P_COD_SIS_ENT   IN     CHAR,
                                  p_CURSOR           OUT SYS_REFCURSOR,
                                  P_ERROR            OUT NUMBER);

   --***********************************************************************************************
   -- Funcion/Procedimiento: FN_PAB_BUS_HOR_INICIO
   -- Objetivo: Consulta el horario del corte horario - INICIO
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01
   -- Tablas Usadas: PABS_DT_SISTM_ENTRD_SALID
   -- Fecha: 09/11/2016
   -- Autor: gcorrea
   -- Input:
   -- P_COD_SIS_SAL -> CODIGO SISTEMA ENTRADA SALIDA (AREA)
   -- N/A
   -- Output:
   -- N/A.
   -- Input/Output:
   -- N/A.
   -- Retorno:
   -- V_HORA_INICIO -> valor de retorno que muestra el horario de inicio segun el area
   -- Observaciones: <Fecha y Detalle de ?ltimos cambios>
   --***********************************************************************************************
   FUNCTION FN_PAB_BUS_HOR_INICIO (P_COD_SIS_SAL IN CHAR)
      RETURN VARCHAR2;

   --***********************************************************************************************
   -- Funcion/Procedimiento: FN_PAB_BUS_HOR_CIERRE
   -- Objetivo: Consulta el horario del corte horario - CIERRE
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01
   -- Tablas Usadas: PABS_DT_SISTM_ENTRD_SALID
   -- Fecha: 09/11/2016
   -- Autor: gcorrea
   -- Input:
   -- P_COD_SIS_SAL -> CODIGO SISTEMA ENTRADA SALIDA (AREA)
   -- N/A
   -- Output:
   -- N/A.
   -- Input/Output:
   -- N/A.
   -- Retorno:
   -- V_HORA_CIERRE -> valor de retorno que muestra el horario de CIERRE segun el area
   -- Observaciones: <Fecha y Detalle de ?ltimos cambios>
   --***********************************************************************************************
   FUNCTION FN_PAB_BUS_HOR_CIERRE (P_COD_SIS_SAL IN CHAR)
      RETURN VARCHAR2;

   -- ***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_BUS_CRTE_HOR
   -- Objetivo: Procedimiento almacenado que obtiene el horario de inicio y cierre por area
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_SISTM_ENTRD_SALID
   -- Fecha: 09/11/16
   -- Autor: gcorrea
   -- Input:
   -- P_COD_SIS_ENT -> codigo de sistema entrada
   -- Output:
   -- p_CURSOR -> Cursor de salida
   -- p_ERROR-> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ?ltimos cambios>
   -- ***********************************************************************************************
   PROCEDURE Sp_PAB_BUS_CRTE_HOR (P_COD_SIS_ENT   IN     CHAR,
                                  p_CURSOR           OUT SYS_REFCURSOR,
                                  P_ERROR            OUT NUMBER);

   -- ***********************************************************************************************
   -- Funcion/Procedimiento: FN_PAB_RISG_COLR
   -- Objetivo: Funcion que retorna el color segun horario en el que se encuentra.
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_DETLL_NOMNA
   -- Fecha: 21/06/16
   -- Autor: gcorrea
   -- Input:
   -- p_COD_SIS_ENT -> Codigo de sistema entrada
   -- P_COD_EST     -> Codigo estado nomina
   -- P_IND_NOM_OPE -> INDICA SI ES NOMINA 0 OPERACION 1
   -- Output:
   -- p_ERROR-> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: V_COLOR -> color segun el horario
   -- Observaciones: <Fecha y Detalle de ?ltimos cambios>
   -- ***********************************************************************************************
   FUNCTION FN_PAB_RISG_COLR (P_COD_SIS_ENT   IN CHAR,
                              P_COD_EST       IN NUMBER,
                              P_IND_NOM_OPE   IN NUMBER)
      RETURN VARCHAR2;

   --***********************************************************************************************
   -- Funcion/Procedimiento: FN_PAB_BUS_ULT_REG_OPE_EST
   -- Objetivo: Consulta ultimo registro por estado de operaciones
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01
   -- Tablas Usadas: PABS_DT_ESTDO_ALMOT, PABS_DT_DETLL_OPRCN,PABS_DT_SISTM_ENTRD_SALID
   -- Fecha: 25/10/2016
   -- Autor: gcorrea
   -- Input:
   -- P_COD_EST_AOS -> CODIGO ESTADO NOMINA
   -- N/A
   -- Output:
   -- N/A.
   -- Input/Output:
   -- N/A.
   -- Retorno:
   -- V_ULTIMO_REGISTRO -> valor de retorno que muestra la ultima fecha y horario por estado de nomina
   -- Observaciones: <Fecha y Detalle de ?ltimos cambios>
   --***********************************************************************************************
   --FUNCTION FN_PAB_BUS_ULT_REG_OPE_EST ( P_COD_EST_AOS IN NUMBER ) RETURN VARCHAR2;

   --***********************************************************************************************
   -- Funcion/Procedimiento: FN_PAB_BUS_ULT_REG_OPE_EST_EGR
   -- Objetivo: Consulta ultimo registro por estado de operaciones
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01
   -- Tablas Usadas: PABS_DT_ESTDO_ALMOT, PABS_DT_DETLL_OPRCN,PABS_DT_SISTM_ENTRD_SALID
   -- Fecha: 27-12-2017
   -- Autor: gcorrea
   -- Input:
   -- p_COD_SIS_ENT -> Codigo de sistema entrada
   -- P_COD_EST_AOS -> CODIGO ESTADO NOMINA
   -- P_FLG_EGR_ING -> Flag Ingreso o egreso de la operacion
   -- N/A
   -- Output:
   -- N/A.
   -- Input/Output:
   -- N/A.
   -- Retorno:
   -- V_ULTIMO_REGISTRO -> valor de retorno que muestra la ultima fecha y horario por estado de nomina
   -- Observaciones: <Fecha y Detalle de ?ltimos cambios>
   --***********************************************************************************************
   FUNCTION FN_PAB_BUS_ULT_REG_OPE_EST (P_COD_SIS_ENT   IN CHAR,
                                        P_COD_EST_AOS   IN NUMBER,
                                        P_FLG_EGR_ING   IN NUMBER)
      RETURN VARCHAR2;

   /************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_BUS_HIT_OFF
   -- Objetivo: Procedimiento almacenado que retorna lista de hitos para OOFF
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_SISTM_ENTRD_SALID
   --
   -- Fecha: 04/07/17
   -- Autor: gcorrea CAH
   -- Input: p_COD_SIS_ENT      Area de trabajo, OOFF, CANJE, Etc.
   -- Output:
   --        p_CURSOR : SYS_REFCURSOR, resultados de consulta.
   --        p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE Sp_PAB_BUS_HIT_TBJ (p_COD_SIS_ENT   IN     CHAR,
                                 p_CURSOR           OUT SYS_REFCURSOR,
                                 p_ERROR            OUT NUMBER);

   -- ***************************************************************************************************************
   -- FUNCION/PROCEDIMIENTO: SP_PAB_MNI_EGR_ENT
   -- OBJETIVO: SP MUESTRA ACTUALIZACI�N EN PANTALLA MONITOREO PARA EGRESOS EN MONEDA NACIONAL Y EXTRANJERA (ENTRADA).
   -- SISTEMA: DBO_PAB.
   -- BASE DE DATOS: DBO_PAB.
   -- TABLAS USADAS: PABS_DT_SISTM_ENTRD_SALID
   -- FECHA: 09-08-2017
   -- AUTOR: GCORREA- GSD
   -- INPUT:
   -- P_FLAG_DVI -> INDICADOR DE DIVISA  (0 MN, 1 MX)
   -- OUTPUT:
   -- P_CURSOR -> CURSOR DE RESULTADO
   -- P_ERROR-> INDICADOR DE ERRORES: 0 SIN ERRORES 1 EXISTEN ERRORES.
   -- OBSERVACIONES: <FECHA Y DETALLE DE ULTIMOS CAMBIOS>
   -- *****************************************************************************************************************
   PROCEDURE Sp_PAB_MNI_EGR_ENT (p_FLAG_DVI   IN     NUMBER,
                                 p_CURSOR        OUT SYS_REFCURSOR,
                                 p_ERROR         OUT NUMBER);

   -- *****************************************************************************************************************
   -- FUNCION/PROCEDIMIENTO: SP_PAB_MNI_EGR_SAL
   -- OBJETIVO: SP MUESTRA ACTUALIZACI�N EN PANTALLA MONITOREO PARA EGRESOS EN MONEDA NACIONAL Y EXTRANJERA (SALIDA).
   -- SISTEMA: DBO_PAB.
   -- BASE DE DATOS: DBO_PAB.
   -- TABLAS USADAS: PABS_DT_SISTM_ENTRD_SALID
   -- FECHA: 21-07-2017
   -- AUTOR: GCORREA- CAH
   -- INPUT:
   -- P_FLAG_DVI -> INDICADOR DE DIVISA
   -- OUTPUT:
   -- P_CURSOR -> CURSOR DE RESULTADO
   -- P_ERROR -> INDICADOR DE ERRORES: 0 SIN ERRORES 1 EXISTEN ERRORES.
   -- OBSERVACIONES: <FECHA Y DETALLE DE ULTIMOS CAMBIOS>
   -- *****************************************************************************************************************
   PROCEDURE Sp_PAB_MNI_EGR_SAL (p_FLAG_DVI   IN     NUMBER,
                                 p_CURSOR        OUT SYS_REFCURSOR,
                                 P_ERROR         OUT NUMBER);

   -- ****************************************************************************************************************
   -- FUNCION/PROCEDIMIENTO: SP_PAB_MNI_ING_SAL
   -- OBJETIVO: SP MUESTRA ACTUALIZACI�N EN PANTALLA MONITOREO INGRESO DE MONEDA NACIONAL (ENTRADA Y SALIDA)
   -- SISTEMA: DBO_PAB
   -- BASE DE DATOS: DBO_PAB
   -- TABLAS USADAS: PABS_DT_SISTM_ENTRD_SALID
   -- FECHA: 21-07-2017
   -- AUTOR: GCORREA
   -- INPUT:
   -- P_FLAG_ENT_SAL -> INDICADOR DE FLUJO
   -- OUTPUT:
   -- P_CURSOR -> CURSOR DE RESULTADO
   -- P_ERROR-> INDICADOR DE ERRORES 0 SIN ERRORES 1 EXISTEN ERRORES.
   -- OBSERVACIONES: <FECHA Y DETALLE DE ULTIMOS CAMBIOS>
   -- ***************************************************************************************************************
   PROCEDURE Sp_PAB_MNI_ING_SAL (p_FLAG_ENT_SAL   IN     NUMBER,
                                 p_CURSOR            OUT SYS_REFCURSOR,
                                 P_ERROR             OUT NUMBER);

   --***************************************************************************************************************
   -- FUNCION/PROCEDIMIENTO: FN_PAB_BUS_CAN_OPE_LIB
   -- OBJETIVO: RETORNA CANTIDAD DE OPERACIONES POR SER LIBERADAS
   -- SISTEMA: DBO_PAB
   -- BASE DE DATOS: DBO_PAB
   -- TABLAS USADAS: PABS_DT_DETLL_OPRCN
   -- FECHA:21/07/2017
   -- AUTOR: GCORREA
   -- INPUT:
   -- P_COD_SIS_ENT_SAL -> CODIGO SISTEMA ENTRADA SALIDA (CANAL)
   -- P_COD_DVI -> INDICADOS DE MONEDA NACIONAL O EXTRANJERA (0 NACIONAL, 1 EXTRANJERA)
   -- OBSERVACIONES: <FECHA Y DETALLE>
   --***************************************************************************************************************
   FUNCTION FN_PAB_BUS_CAN_OPE_LIB (P_COD_SIS_ENT_SAL   IN CHAR,
                                    P_FLG_SIS_ENT_SAL      NUMBER,
                                    P_COD_DVI              CHAR)
      RETURN NUMBER;

   --***********************************************************************************************
   -- FUNCION/PROCEDIMIENTO: FN_PAB_BUS_CAN_OPE_PAG
   -- OBJETIVO: RETORNA CANTIDAD DE OPERACIONES PAGADAS
   -- SISTEMA: DBO_PAB
   -- BASE DE DATOS: DBO_PAB
   -- TABLAS USADAS: PABS_DT_DETLL_CARGO_ABONO_CAJA
   -- FECHA:21/07/2017
   -- AUTOR: GCORREA
   -- INPUT:
   -- P_COD_SIS_ENT_SAL -> C�DIGO CANAL
   -- P_FLG_SIS_ENT_SAL -> FLAG CANAL DE SALIDA O ENTRADA
   -- P_COD_DVI -> DIVISA
   -- OBSERVACIONES: <FECHA Y DETALLE DE ?LTIMOS CAMBIOS>
   --***********************************************************************************************
   FUNCTION FN_PAB_BUS_CAN_OPE_PAG (P_COD_SIS_ENT_SAL   IN CHAR,
                                    P_FLG_SIS_ENT_SAL      NUMBER,
                                    P_COD_DVI              NUMBER)
      RETURN NUMBER;

   --*****************************************************************************************************
   -- FUNCION/PROCEDIMIENTO: FN_PAB_BUS_CAN_OPE_TOT
   -- OBJETIVO: FN RETORNA CANTIDAD TOTAL DE OPERACIONES POR LIBERAR + NO PAGADAS + PAGADAS + ENVIADAS
   -- SISTEMA: PAB
   -- BASE DE DATOS: DBO_PAB
   -- TABLAS USADAS: PABS_DT_DETLL_OPRCN - PABS_DT_DETLL_CARGO_ABONO_CAJA
   -- FECHA:21/07/2017
   -- AUTOR: GCORREA-CAH
   -- INPUT:
   -- P_COD_SIS_ENT_SAL -> C�DIGO CANAL
   -- P_FLG_SIS_ENT_SAL -> FLAG CANAL
   -- P_COD_DVI -> DIVISA
   -- OBSERVACIONES: <FECHA Y DETALLE>
   --*****************************************************************************************************
   FUNCTION FN_PAB_BUS_CAN_OPE_TOT (P_COD_SIS_ENT_SAL   IN CHAR,
                                    P_FLG_SIS_ENT_SAL      NUMBER,
                                    P_COD_DVI              NUMBER)
      RETURN NUMBER;

   --*****************************************************************************************************
   -- FUNCION/PROCEDIMIENTO: FN_PAB_BUS_CAN_OPE_NPAG
   -- OBJETIVO: RETORNA CANTIDAD DE OPERACIONES NO PAGADAS
   -- SISTEMA: DBO_PAB
   -- BASE DE DATOS: DBO_PAB
   -- TABLAS USADAS: PABS_DT_DETLL_OPRCN
   -- FECHA:21/07/2017
   -- AUTOR: GCORREA
   -- INPUT:
   -- P_COD_SIS_ENT_SAL -> C�DIGO CANAL
   -- P_FLG_SIS_ENT_SAL -> FLAG DE CANAL DE SALIDA O ENTRADA
   -- P_COD_DVI -> DIVISA
   -- OBSERVACIONES: <FECHA Y DETALLE >
   --****************************************************************************************************
   FUNCTION FN_PAB_BUS_CAN_OPE_NPAG (P_COD_SIS_ENT_SAL   IN CHAR,
                                     P_FLG_SIS_ENT_SAL      NUMBER,
                                     P_COD_DVI              NUMBER)
      RETURN NUMBER;

   --***********************************************************************************************
   -- FUNCION/PROCEDIMIENTO: FN_PAB_BUS_CANT_OPE_ABO
   -- OBJETIVO: RETORNA CANTIDA DE OPERACIONES ABONADAS
   -- SISTEMA: DBO_PAB
   -- BASE DE DATOS: DBO_PAB
   -- TABLAS USADAS: PABS_DT_DETLL_OPRCN - PABS_DT_DETLL_CARGO_ABONO_CAJA
   -- FECHA:21/07/2017
   -- AUTOR: GCORREA
   -- INPUT:
   -- P_COD_SIS_ENT_SAL -> C�DIGO CANAL
   -- P_FLG_SIS_ENT_SAL -> FLAG DE CANAL DE SALIDA O ENTRADA
   -- OBSERVACIONES: <FECHA Y DETALLE>
   --***********************************************************************************************
   FUNCTION FN_PAB_BUS_CANT_OPE_ABO (P_COD_SIS_ENT_SAL   IN CHAR,
                                     p_FLAG_ENT_SAL      IN NUMBER)
      RETURN NUMBER;

   -- --***********************************************************************************************
   ---- FUNCION/PROCEDIMIENTO: FN_PAB_BUS_CANT_OPE_NAB
   ---- OBJETIVO: RETORNA CANTIDAD DE OPERACIONES POR SER LIBERADAS
   ---- SISTEMA: DBO_PAB
   ---- BASE DE DATOS: DBO_PAB
   ---- TABLAS USADAS: PABS_DT_DETLL_OPRCN
   ---- FECHA:21/07/2017
   ---- AUTOR: GCORREA
   ---- INPUT:
   ---- P_COD_SIS_ENT_SAL -> CANAL
   ---- P_FLG_SIS_ENT_SAL -> INIDCADO DE CANAL DE SALIDA O ENTRADA
   ---- OBSERVACIONES: <FECHA Y DETALLE>
   ----***********************************************************************************************
   FUNCTION FN_PAB_BUS_CANT_OPE_NAB (P_COD_SIS_ENT_SAL   IN CHAR,
                                     p_FLAG_ENT_SAL      IN NUMBER)
      RETURN NUMBER;

   --***********************************************************************************************
   -- FUNCION/PROCEDIMIENTO: FN_PAB_BUS_CAN_OPE_TOT_SAL
   -- OBJETIVO: RETORNA CANTIDAD TOTAL DE OPERACIONES LIBERAR + NO PAGADAS + PAGADAS + ENVIADAS
   -- SISTEMA: DBO_PAB
   -- BASE DE DATOS: DBO_PAB
   -- TABLAS USADAS: PABS_DT_DETLL_OPRCN, PABS_DT_DETLL_CARGO_ABONO_CAJA
   -- FECHA:21/07/2017
   -- AUTOR: GCORREA-CAH
   -- INPUT:
   -- P_COD_SIS_ENT_SAL -> C�DIGO CANAL
   -- P_FLG_SIS_ENT_SAL -> FLAGIDCADO DE CANAL DE SALIDA O ENTRADA
   -- OBSERVACIONES: <FECHA Y DETALLE>
   --***********************************************************************************************
   FUNCTION FN_PAB_BUS_CAN_OPE_TOT_SAL (P_COD_SIS_ENT_SAL   IN CHAR,
                                        P_FLG_SIS_ENT_SAL      NUMBER,
                                        P_COD_DVI              NUMBER)
      RETURN NUMBER;

   -- ***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_EST_OPE_DSH_EGR
   -- Objetivo: Procedimiento almacenado que obtine los estados de las operaciones para el dashboard segun el rol
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_DETLL_NOMNA
   -- Fecha: 21/06/16
   -- Autor: gcorrea
   -- Input:
   -- p_COD_TROL -> Tipo de rol
   -- p_COD_SIS_ENT -> Codigo de sistema entrada
   -- Output:
   -- p_CURSOR-> Cursor de salida
   -- p_ERROR-> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de Ultimos cambios>
   -- ***********************************************************************************************
   PROCEDURE Sp_PAB_EST_OPE_DSH_EGR (p_COD_TROL      IN     NUMBER,
                                     p_COD_SIS_ENT   IN     CHAR,
                                     p_CURSOR           OUT SYS_REFCURSOR,
                                     p_ERROR            OUT NUMBER);

   -- ***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_EST_OPE_DSH_ING
   -- Objetivo: Procedimiento almacenado que obtine los estados de las operaciones para el dashboard segun el rol
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_DETLL_NOMNA
   -- Fecha: 21/06/16
   -- Autor: gcorrea
   -- Input:
   -- p_COD_TROL -> Tipo de rol
   -- p_COD_SIS_ENT -> Codigo de sistema entrada
   -- Output:
   -- p_CURSOR-> Cursor de salida
   -- p_ERROR-> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de Ultimos cambios>
   -- ***********************************************************************************************
   PROCEDURE Sp_PAB_EST_OPE_DSH_ING (p_COD_TROL      IN     NUMBER,
                                     p_COD_SIS_ENT   IN     CHAR,
                                     p_CURSOR           OUT SYS_REFCURSOR,
                                     p_ERROR            OUT NUMBER);

   -- ****************************************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_EST_NMN_DSH_EGR
   -- Objetivo: Procedimiento almacenado que obtiene los estados para operaciones en respecto a n�minas
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_DETLL_NOMNA
   -- Fecha: 06-11-2017
   -- Autor: gcorrea- CAH
   -- Input:
   -- p_COD_TROL -> Tipo de rol
   -- p_COD_SIS_ENT -> Codigo de sistema entrada
   -- Output:
   -- p_CURSOR-> Cursor de salida
   -- p_ERROR-> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de Ultimos cambios>
   -- ******************************************************************************************************************************
   PROCEDURE Sp_PAB_EST_NMN_DSH_EGR (p_COD_TROL      IN     NUMBER,
                                     p_COD_SIS_ENT   IN     CHAR,
                                     p_CURSOR           OUT SYS_REFCURSOR,
                                     p_ERROR            OUT NUMBER);

   -- ***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_CANT_OPE_EST
   -- Objetivo: Procedimiento almacenado que cuenta por el tipo de estado cada una de las operaciones
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_DETLL_OPRCN, PABS_DT_ESTDO_ALMOT, PABS_DT_SISTM_ENTRD_SALID
   -- Fecha: 24/10/16
   -- Autor: gcorrea
   -- Input:
   -- P_COD_EST_AOS -> codigo de estado de operacion
   -- P_COD_SIS_ENT -> codigo de sistema entrada
   -- Output:
   -- p_CURSOR -> cursor de resultado
   -- p_ERROR-> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ?ltimos cambios>
   -- ***********************************************************************************************
   PROCEDURE Sp_PAB_CANT_OPE_EST_EGR (P_COD_EST_AOS   IN     NUMBER,
                                      P_COD_SIS_ENT   IN     CHAR,
                                      p_CURSOR           OUT SYS_REFCURSOR,
                                      P_ERROR            OUT NUMBER);

   -- ***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_CANT_OPE_EST
   -- Objetivo: Procedimiento almacenado que cuenta por el tipo de estado cada una de las operaciones
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_DETLL_OPRCN, PABS_DT_ESTDO_ALMOT, PABS_DT_SISTM_ENTRD_SALID
   -- Fecha: 24/10/16
   -- Autor: gcorrea
   -- Input:
   -- P_COD_EST_AOS -> codigo de estado de operacion
   -- P_COD_SIS_ENT -> codigo de sistema entrada
   -- Output:
   -- p_CURSOR -> cursor de resultado
   -- p_ERROR-> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ?ltimos cambios>
   -- ***********************************************************************************************
   PROCEDURE Sp_PAB_CANT_OPE_EST_ING (P_COD_EST_AOS   IN     NUMBER,
                                      P_COD_SIS_ENT   IN     CHAR,
                                      p_CURSOR           OUT SYS_REFCURSOR,
                                      P_ERROR            OUT NUMBER);

   -- ***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_CANT_OPE_EST
   -- Objetivo: Procedimiento almacenado que cuenta por el tipo de estado cada una de las operaciones
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_DETLL_OPRCN, PABS_DT_ESTDO_ALMOT, PABS_DT_SISTM_ENTRD_SALID
   -- Fecha: 06/11/2017
   -- Autor: gcorrea- CAH
   -- Input:
   -- P_COD_EST_AOS -> codigo de estado de operacion
   -- P_COD_SIS_ENT -> codigo de sistema entrada
   -- Output:
   -- p_CURSOR -> cursor de resultado
   -- p_ERROR-> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ?ltimos cambios>
   -- ***********************************************************************************************
   PROCEDURE Sp_PAB_CANT_NMN_EST_EGR (P_COD_EST_AOS   IN     NUMBER,
                                      P_COD_SIS_ENT   IN     CHAR,
                                      p_CURSOR           OUT SYS_REFCURSOR,
                                      P_ERROR            OUT NUMBER);

   -- --***********************************************************************************************
   ---- FUNCION/PROCEDIMIENTO: Sp_PAB_MNX_ING_SAL
   ---- OBJETIVO: SP MUESTRA ACTUALIZACI�N EN PANTALLA MONITOREO PARA INGRESO EN MONEDA EXTRANJERA
   ---- SISTEMA: DBO_PAB
   ---- BASE DE DATOS: DBO_PAB
   ---- TABLAS USADAS: PABS_DT_SISTM_ENTRD_SALID
   ---- FECHA:
   ---- AUTOR: NEORIS
   ---- INPUT:
   ---- p_FLAG_ENT_SAL -> FLAG ENTRADA 0 SALIDA 1
   ---- OUTPUT:
   ---- p_CURSOR -> RETORNO DATOS
   ---- P_ERROR  -> MANEJO DE ERRORES.
   ---- OBSERVACIONES: <FECHA Y DETALLE DE ?LTIMOS CAMP_FLG_SIS_ENT_SALBIOS>
   ----***********************************************************************************************
   PROCEDURE Sp_PAB_MNX_ING_SAL (p_FLAG_ENT_SAL   IN     NUMBER,
                                 p_CURSOR            OUT SYS_REFCURSOR,
                                 P_ERROR             OUT NUMBER);

   -- --**********************************************************************************************
   ---- FUNCION/PROCEDIMIENTO: FN_PAB_BUS_CANT_OPE_ABONO
   ---- OBJETIVO: FN MUESTRA LOS MT910 PARA MONEDA EXTRANJERA DEL D�A.
   ---- SISTEMA: DBO_PAB
   ---- BASE DE DATOS: DBO_PAB
   ---- TABLAS USADAS: PABS_DT_DETLL_CARGO_ABONO_CAJA
   ---- FECHA:
   ---- AUTOR: NEORIS
   ---- INPUT:
   ---- P_COD_SIS_ENT_SAL -> C�DIGO CANAL
   ---- P_FLG_SIS_ENT_SAL -> FLAG ENTRADA O SALIDA
   ---- OUTPUT:
   ---- N/A
   ---- OBSERVACIONES: <FECHA Y DETALLE>
   ---- VAR 22.10.2018 SE MODIFICA FUNCI�N.
   ----***********************************************************************************************
   FUNCTION FN_PAB_BUS_CANT_OPE_ABONO (P_COD_SIS_ENT_SAL   IN CHAR,
                                       P_FLG_SIS_ENT_SAL   IN CHAR)
      RETURN NUMBER;
END PKG_PAB_MONITOREO;
/


CREATE OR REPLACE /* Formatted on 18-08-2020 17:45:43 (QP5 v5.115.810.9015) */
PACKAGE BODY DBO_PAB.PKG_PAB_MONITOREO
AS
   --***********************************************************************************************
   -- Funcion/Procedimiento: FN_PAB_BUS_ULT_REG_EST
   -- Objetivo: Consulta ultimo registro por estado de nomina
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01
   -- Tablas Usadas: PABS_DT_ESTDO_ALMOT, PABS_DT_CBCRA_NOMNA,PABS_DT_SISTM_ENTRD_SALID
   -- Fecha: 25/10/2016
   -- Autor: gcorrea
   -- Input:
   -- P_COD_EST_AOS -> CODIGO ESTADO NOMINA
   -- P_COD_SIS_ENT -> CODIGO SISTEMA NOMINA
   -- P_IND_NOM_OPE -> INDICA SI ES NOMINA 0 OPERACION 1
   -- Output:
   -- N/A.
   -- Input/Output:
   -- N/A.
   -- Retorno:
   -- V_ULTIMO_REGISTRO -> valor de retorno que muestra la ultima fecha y horario por estado de nomina
   -- Observaciones: <Fecha y Detalle de ?ltimos cambios>
   --***********************************************************************************************
   FUNCTION FN_PAB_BUS_ULT_REG_EST (P_COD_EST_AOS   IN NUMBER,
                                    P_COD_SIS_ENT   IN CHAR,
                                    P_IND_NOM_OPE      NUMBER)
      RETURN VARCHAR2
   IS
      V_NOM_SP            VARCHAR2 (30) := 'FN_PAB_BUS_ULT_REG_EST';
      V_ULTIMO_REGISTRO   VARCHAR2 (8);
      V_COD_SIS_ENT       CHAR (10);
   BEGIN
      --Tecnologia debe ver el ultimo registro de todo
      IF (P_COD_SIS_ENT <> PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SE_TECNO)
      THEN
         V_COD_SIS_ENT := P_COD_SIS_ENT;
      END IF;

      --Operaci�n
      IF (P_IND_NOM_OPE = V_IND_OPE)
      THEN
         --Si es una autorizaci�n de nomina
         IF P_COD_EST_AOS = PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEAUT
         THEN
            --Obtenemos la hora del ultimo registro seleccionado.
            SELECT   TO_CHAR (MAX (DETNOM.FEC_ACT_NMN), 'HH24:MI:SS')
              INTO   V_ULTIMO_REGISTRO
              FROM   PABS_DT_DETLL_NOMNA DETNOM,
                     PABS_DT_CBCRA_NOMNA CABNOM,
                     PABS_DT_ESTDO_ALMOT EST
             WHERE   CABNOM.COD_SIS_ENT =
                        NVL (V_COD_SIS_ENT, CABNOM.COD_SIS_ENT)
                     AND CABNOM.NUM_FOL_NMN = DETNOM.NUM_FOL_NMN
                     AND DETNOM.COD_EST_AOS IN
                              (PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEAUT,
                               PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPVI,
                               PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPELIB,
                               PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEENV,
                               PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAG,
                               PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAGCFI,
                               PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEREC,
                               PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPDV,
                               PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEDEV)
                     AND DETNOM.COD_EST_AOS = EST.COD_EST_AOS
                     AND DETNOM.FEC_ISR_OPE BETWEEN v_dia_ini AND v_dia_fin; --Solo las nominas del dia
         ELSE
            --Obtenemos la hora del ultimo registro seleccionado.
            SELECT   TO_CHAR (MAX (DETNOM.FEC_ACT_NMN), 'HH24:MI:SS')
              INTO   V_ULTIMO_REGISTRO
              FROM   PABS_DT_DETLL_NOMNA DETNOM,
                     PABS_DT_CBCRA_NOMNA CABNOM,
                     PABS_DT_ESTDO_ALMOT EST
             WHERE   CABNOM.COD_SIS_ENT =
                        NVL (V_COD_SIS_ENT, CABNOM.COD_SIS_ENT)
                     AND CABNOM.NUM_FOL_NMN = DETNOM.NUM_FOL_NMN
                     AND DETNOM.COD_EST_AOS = P_COD_EST_AOS
                     AND DETNOM.COD_EST_AOS = EST.COD_EST_AOS
                     AND DETNOM.FEC_ISR_OPE BETWEEN v_dia_ini AND v_dia_fin; --Solo las nominas del dia
         END IF;
      ELSE
         --Nomina
         --Obtenemos la hora del ultimo registro seleccionado.
         SELECT   TO_CHAR (MAX (DETNOM.FEC_ACT_NMN), 'HH24:MI:SS')
           INTO   V_ULTIMO_REGISTRO
           FROM   PABS_DT_DETLL_NOMNA DETNOM,
                  PABS_DT_CBCRA_NOMNA CABNOM,
                  PABS_DT_ESTDO_ALMOT EST
          WHERE   CABNOM.COD_EST_AOS = P_COD_EST_AOS
                  AND CABNOM.COD_SIS_ENT =
                        NVL (V_COD_SIS_ENT, CABNOM.COD_SIS_ENT)
                  AND CABNOM.NUM_FOL_NMN = DETNOM.NUM_FOL_NMN
                  AND DETNOM.COD_EST_AOS = EST.COD_EST_AOS
                  AND DETNOM.FEC_ISR_OPE BETWEEN v_dia_ini AND v_dia_fin; --Solo las nominas del dia
      END IF;

      RETURN V_ULTIMO_REGISTRO;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END FN_PAB_BUS_ULT_REG_EST;

   -- ***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_CANT_NMN_EST
   -- Objetivo: Procedimiento almacenado que cuenta por el tipo de estado cada una de las nominas
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_DETLL_NOMNA
   -- Fecha: 24/10/16
   -- Autor: gcorrea
   -- Input:
   -- P_COD_SIS_ENT -> codigo de sistema entrada
   -- P_COD_EST_AOS -> codigo estado
   -- Output:
   -- p_ERROR-> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- p_CURSOR-> cursor resultado
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ?ltimos cambios>
   -- ***********************************************************************************************
   PROCEDURE Sp_PAB_CANT_NMN_EST (P_COD_EST_AOS   IN     NUMBER,
                                  P_COD_SIS_ENT   IN     CHAR,
                                  p_CURSOR           OUT SYS_REFCURSOR,
                                  P_ERROR            OUT NUMBER)
   IS
      v_NOM_SP        VARCHAR2 (30) := 'Sp_PAB_CANT_NMN_EST';
      V_COD_SIS_ENT   CHAR (10);
   BEGIN
      --Tecnologia ve las nominas de todas las areas
      IF (P_COD_SIS_ENT <> PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SE_TECNO)
      THEN
         V_COD_SIS_ENT := P_COD_SIS_ENT;
      END IF;

      OPEN p_CURSOR FOR
           SELECT   COUNT (CABNOM.NUM_FOL_NMN) AS CANT_NMN,
                    EST.COD_EST_AOS AS COD_EST_AOS,
                    EST.DSC_EST_AOS AS GLS_EST,
                    FN_PAB_BUS_ULT_REG_EST (P_COD_EST_AOS,
                                            P_COD_SIS_ENT,
                                            V_IND_NOM)
                       AS ULT_REG,
                    FN_PAB_RISG_COLR (P_COD_SIS_ENT, P_COD_EST_AOS, V_IND_NOM)
                       AS COLOR
             FROM   PABS_DT_CBCRA_NOMNA CABNOM,
                    PABS_DT_ESTDO_ALMOT EST,
                    PABS_DT_SISTM_ENTRD_SALID CAN
            WHERE       CABNOM.COD_EST_AOS = EST.COD_EST_AOS
                    AND CAN.COD_SIS_ENT_SAL = CABNOM.COD_SIS_ENT
                    AND CABNOM.COD_EST_AOS = P_COD_EST_AOS
                    AND CABNOM.COD_SIS_ENT =
                          NVL (V_COD_SIS_ENT, CABNOM.COD_SIS_ENT)
                    AND CABNOM.FEC_ISR_NMN BETWEEN v_dia_ini AND v_dia_fin --Solo las nominas del dia
         GROUP BY   EST.COD_EST_AOS, EST.DSC_EST_AOS;

      p_ERROR := PKG_PAB_CONSTANTES.v_OK;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END Sp_PAB_CANT_NMN_EST;

   -- ***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_EST_NMN_DSH
   -- Objetivo: Procedimiento almacenado que obtine los estados de las nominas para el dashboard segun el rol
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_DETLL_NOMNA
   -- Fecha: 21/06/16
   -- Autor: gcorrea
   -- Input:
   -- p_COD_TROL -> tipo de rol
   -- p_COD_SIS_ENT -> canal entrada salida
   -- Output:
   -- p_ERROR-> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- p_CURSOR-> cursor de salida de resultado
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ?ltimos cambios>
   -- ***********************************************************************************************
   PROCEDURE Sp_PAB_EST_NMN_DSH (p_COD_TROL      IN     NUMBER,
                                 p_COD_SIS_ENT   IN     CHAR,
                                 p_CURSOR           OUT SYS_REFCURSOR,
                                 p_ERROR            OUT NUMBER)
   IS
      v_NOM_SP        VARCHAR2 (30) := 'Sp_PAB_EST_NMN_DSH';
      v_COD_SIS_ENT   CHAR (10);
   BEGIN
      p_ERROR := PKG_PAB_CONSTANTES.v_OK;

      --Variable a futuro
      v_COD_SIS_ENT := p_COD_SIS_ENT;

      CASE
         --Tecnologia
         WHEN v_COD_SIS_ENT = PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SE_TECNO
         THEN
            --Administrador
            IF (p_COD_TROL = PKG_PAB_CONSTANTES.V_COD_TIP_ROL_ADM)
            THEN
               --OR p_COD_TROL = 0

               OPEN p_CURSOR FOR
                    SELECT   EST.COD_EST_AOS,
                             DECODE (COD_EST_AOS,
                                     PKG_PAB_CONSTANTES.V_COD_EST_AOS_NMNVAL,
                                     'POR CURSAR',
                                     EST.DSC_EST_AOS)
                                AS DSC_EST_AOS,
                             DECODE (COD_EST_AOS,
                                     PKG_PAB_CONSTANTES.V_COD_EST_AOS_NMNCUR,
                                     v_EST_CUR,
                                     PKG_PAB_CONSTANTES.V_COD_EST_AOS_NMNINV,
                                     v_EST_INV,
                                     PKG_PAB_CONSTANTES.V_COD_EST_AOS_NMNVAL,
                                     v_EST_VAL,
                                     PKG_PAB_CONSTANTES.V_COD_EST_AOS_NMNDUP,
                                     v_EST_DUP,
                                     PKG_PAB_CONSTANTES.V_COD_EST_AOS_NMNAUT,
                                     v_EST_AUT,
                                     PKG_PAB_CONSTANTES.V_COD_EST_AOS_NMNREC,
                                     v_EST_REC,
                                     PKG_PAB_CONSTANTES.V_COD_EST_AOS_NMNELI,
                                     v_EST_ELI) --Campo Utilizado para ordenar la salida
                      FROM   PABS_DT_ESTDO_ALMOT EST
                     WHERE   COD_EST_AOS IN
                                   (PKG_PAB_CONSTANTES.V_COD_EST_AOS_NMNINV, --Validas
                                    PKG_PAB_CONSTANTES.V_COD_EST_AOS_NMNDUP, --Duplicada
                                    PKG_PAB_CONSTANTES.V_COD_EST_AOS_NMNVAL, --Invalidas
                                    PKG_PAB_CONSTANTES.V_COD_EST_AOS_NMNREC, --Rechazada
                                    PKG_PAB_CONSTANTES.V_COD_EST_AOS_NMNELI, --Eliminadas
                                    PKG_PAB_CONSTANTES.V_COD_EST_AOS_NMNCUR, --Cursada
                                    PKG_PAB_CONSTANTES.V_COD_EST_AOS_NMNAUT --Autorizada
                                                                           )
                             AND FLG_VGN = PKG_PAB_CONSTANTES.V_REG_VIG
                  ORDER BY   3 ASC;
            END IF;
         --Mesa
         WHEN v_COD_SIS_ENT = PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SE_MESA
         THEN
            NULL;                            --La mesa no ve estado de nominas
         --Todas las otras areas
         ELSE
            --Supervisor
            IF (p_COD_TROL = PKG_PAB_CONSTANTES.V_COD_TIP_ROL_SUP)
            THEN
               OPEN p_CURSOR FOR
                    SELECT   EST.COD_EST_AOS,
                             DECODE (COD_EST_AOS,
                                     PKG_PAB_CONSTANTES.V_COD_EST_AOS_NMNCUR,
                                     'POR AUTORIZAR',
                                     EST.DSC_EST_AOS)
                                AS DSC_EST_AOS,
                             DECODE (COD_EST_AOS,
                                     PKG_PAB_CONSTANTES.V_COD_EST_AOS_NMNCUR,
                                     v_EST_CUR,
                                     PKG_PAB_CONSTANTES.V_COD_EST_AOS_NMNAUT,
                                     v_EST_AUT,
                                     PKG_PAB_CONSTANTES.V_COD_EST_AOS_NMNREC,
                                     v_EST_REC)
                      FROM   PABS_DT_ESTDO_ALMOT EST
                     WHERE   COD_EST_AOS IN
                                   (PKG_PAB_CONSTANTES.V_COD_EST_AOS_NMNCUR, --Por Autorizar
                                    PKG_PAB_CONSTANTES.V_COD_EST_AOS_NMNAUT, --Autorizadas
                                    PKG_PAB_CONSTANTES.V_COD_EST_AOS_NMNREC --Rechazadas
                                                                           )
                             AND FLG_VGN = PKG_PAB_CONSTANTES.V_REG_VIG
                  ORDER BY   3 ASC;
            --Analista
            ELSIF (p_COD_TROL = PKG_PAB_CONSTANTES.V_COD_TIP_ROL_ANA)
            THEN
               OPEN p_CURSOR FOR
                    SELECT   EST.COD_EST_AOS,
                             DECODE (COD_EST_AOS,
                                     PKG_PAB_CONSTANTES.V_COD_EST_AOS_NMNVAL,
                                     'POR CURSAR',
                                     EST.DSC_EST_AOS)
                                AS DSC_EST_AOS,
                             DECODE (COD_EST_AOS,
                                     PKG_PAB_CONSTANTES.V_COD_EST_AOS_NMNCUR,
                                     v_EST_CUR,
                                     PKG_PAB_CONSTANTES.V_COD_EST_AOS_NMNINV,
                                     v_EST_INV,
                                     PKG_PAB_CONSTANTES.V_COD_EST_AOS_NMNVAL,
                                     v_EST_VAL,
                                     PKG_PAB_CONSTANTES.V_COD_EST_AOS_NMNDUP,
                                     v_EST_DUP,
                                     PKG_PAB_CONSTANTES.V_COD_EST_AOS_NMNAUT,
                                     v_EST_AUT,
                                     PKG_PAB_CONSTANTES.V_COD_EST_AOS_NMNREC,
                                     v_EST_REC,
                                     PKG_PAB_CONSTANTES.V_COD_EST_AOS_NMNELI,
                                     v_EST_ELI) --Campo Utilizado para ordenar la salida
                      FROM   PABS_DT_ESTDO_ALMOT EST
                     WHERE   COD_EST_AOS IN
                                   (PKG_PAB_CONSTANTES.V_COD_EST_AOS_NMNINV, --Validas
                                    PKG_PAB_CONSTANTES.V_COD_EST_AOS_NMNDUP, --Duplicada
                                    PKG_PAB_CONSTANTES.V_COD_EST_AOS_NMNVAL, --Invalidas
                                    PKG_PAB_CONSTANTES.V_COD_EST_AOS_NMNREC, --Rechazada
                                    PKG_PAB_CONSTANTES.V_COD_EST_AOS_NMNELI, --Eliminadas
                                    PKG_PAB_CONSTANTES.V_COD_EST_AOS_NMNCUR, --Cursada
                                    PKG_PAB_CONSTANTES.V_COD_EST_AOS_NMNAUT --Autorizada
                                                                           )
                             AND FLG_VGN = PKG_PAB_CONSTANTES.V_REG_VIG
                  ORDER BY   3 ASC;
            END IF;
      END CASE;
   --Procedimiento ejecutado de forma correcta
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END Sp_PAB_EST_NMN_DSH;

   -- ***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_EST_OPE_DSH
   -- Objetivo: Procedimiento almacenado que obtine los estados de las operaciones para el dashboard segun el rol
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_DETLL_NOMNA
   -- Fecha: 21/06/16
   -- Autor: gcorrea
   -- Input:
   -- p_COD_TROL -> Tipo de rol
   -- p_COD_SIS_ENT -> Codigo de sistema entrada
   -- Output:
   -- p_CURSOR-> Cursor de salida
   -- p_ERROR-> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de Ultimos cambios>
   -- ***********************************************************************************************
   PROCEDURE Sp_PAB_EST_OPE_DSH (p_COD_TROL      IN     NUMBER,
                                 p_COD_SIS_ENT   IN     CHAR,
                                 p_CURSOR           OUT SYS_REFCURSOR,
                                 p_ERROR            OUT NUMBER)
   IS
      v_NOM_SP        VARCHAR2 (30) := 'Sp_PAB_EST_OPE_DSH';
      v_COD_SIS_ENT   VARCHAR2 (10);
   BEGIN
      NULL;
   --        p_ERROR := PKG_PAB_CONSTANTES.v_OK;
   --
   --        --Variable a futuro
   --        v_COD_SIS_ENT := p_COD_SIS_ENT;
   --
   --        IF p_COD_TROL = 1  THEN
   --       OPEN p_CURSOR FOR
   --              SELECT COD_EST_AOS AS COD_EST_AOS
   --                           , DECODE(DSC_EST_AOS ,'AUTORIZADA','POR LIBERAR', DSC_EST_AOS) AS DSC_EST_AOS
   --              FROM PABS_DT_ESTDO_ALMOT
   --              WHERE COD_EST_AOS IN (  PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEDUP
   --                                            --,PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEELI    --4     * ELIMINADA
   --                                            ,PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEREC    --6     * RECHAZADA
   --                                            ,PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPELIB    --10    * LIBERADA
   --                                            ,PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAG    --12    * PAGADA
   --                                            ,PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEENV    --13    * ENVIADA
   --                                            ,PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEABN    --15    * ABONADA/ RECIBIDA
   --                                            ,PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEDEV    --18    * DEVUELTA
   --                                            ,PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEAUT    --7     * AUTORIZADA / POR LIBERAR
   --                                            )  --13
   --              AND FLG_VGN = PKG_PAB_CONSTANTES.V_REG_VIG;

   --
   --        ELSIF p_COD_TROL = 2 THEN
   --            OPEN p_CURSOR FOR
   --                SELECT COD_EST_AOS AS COD_EST_AOS
   --                           , DECODE(DSC_EST_AOS ,'AUTORIZADA','POR LIBERAR', DSC_EST_AOS ) AS DSC_EST_AOS
   --              FROM PABS_DT_ESTDO_ALMOT
   --              WHERE COD_EST_AOS IN (  PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEDUP
   --                                            ,PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEELI    --4     * ELIMINADA
   --                                            ,PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEREC    --6     * RECHAZADA
   --                                            ,PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPELIB    --10    * LIBERADA --
   --                                            , PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAG   --12    * PAGADA    --
   --                                            ,PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEENV    --13    * ENVIADA --
   --                                            ,PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEABN    --15    * ABONADA/ RECIBIDA
   --                                            ,PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEDEV    --18    * DEVUELTA
   --                                            ,PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEAUT    --7     * AUTORIZADA / POR LIBERAR
   --                                            )
   --              AND FLG_VGN = PKG_PAB_CONSTANTES.V_REG_VIG;
   --        ELSE
   --            OPEN p_CURSOR FOR
   --                SELECT DECODE(DSC_EST_AOS ,'AUTORIZADA','POR LIBERAR' , DSC_EST_AOS) AS DSC_EST_AOS,
   --                                COD_EST_AOS AS COD_EST_AOS,
   --                                ROWNUM AS ORDEN
   --              FROM PABS_DT_ESTDO_ALMOT
   --              WHERE COD_EST_AOS IN (  PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEINV     --2
   --                                            ,PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEVAL    --3 cambiar a Por Cursar
   --                                            ,PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEELI    --4
   --                                            ,PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPECUR    --5 Cursadas
   --                                            ,PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEREC    --6
   --                                            ,PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEAUT    --7
   --                                            ,PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEDUP)    --17
   --                                           -- ,PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAB)--16 POR ABONAR
   --                    AND FLG_VGN = PKG_PAB_CONSTANTES.V_REG_VIG
   --                    UNION
   --                    SELECT DECODE(DSC_EST_AOS ,'AUTORIZADA','POR LIBERAR' , DSC_EST_AOS) AS DSC_EST_AOS,
   --                                COD_EST_AOS AS COD_EST_AOS,
   --                                (ROWNUM + 8) AS ORDEN
   --                         --COD_EST_AOS AS COD_EST_AOS
   --                         --  , DECODE(DSC_EST_AOS ,'AUTORIZADA','POR LIBERAR' , DSC_EST_AOS) AS DSC_EST_AOS
   --                    FROM PABS_DT_ESTDO_ALMOT
   --                    WHERE COD_EST_AOS IN (   PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPVI    --8
   --                                            ,PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPELIB    --10
   --                                            ,PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAG    --12
   --                                            ,PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEENV    --13
   --                                            ,PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEABN    --15
   --                                            ,PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAB    --16
   --                                            ,PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEDEV    --18
   --                                            ,PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAS    --19 Por Asociar
   --                                            ,PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPDV    --23 Por Devolver
   --                                            ,PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPDEB   --11 Pendiente por debitar
   --                                            ,PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAT -- Por autorizar
   --                                            ,PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEREG  --Registrada
   --                                            )
   --              AND FLG_VGN = PKG_PAB_CONSTANTES.V_REG_VIG
   --                    order by 3;
   --
   --        END IF;
   --                                                                                                                                                                                                                                                                                                                                                                                                             --Procedimiento ejecutado de forma correcta
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END Sp_PAB_EST_OPE_DSH;

   -- ***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_CANT_OPE_EST
   -- Objetivo: Procedimiento almacenado que cuenta por el tipo de estado cada una de las operaciones
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_DETLL_OPRCN, PABS_DT_ESTDO_ALMOT, PABS_DT_SISTM_ENTRD_SALID
   -- Fecha: 24/10/16
   -- Autor: gcorrea
   -- Input:
   -- P_COD_EST_AOS -> codigo de estado de operacion
   -- P_COD_SIS_ENT -> codigo de sistema entrada
   -- Output:
   -- p_CURSOR -> cursor de resultado
   -- p_ERROR-> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ?ltimos cambios>
   -- ***********************************************************************************************
   PROCEDURE Sp_PAB_CANT_OPE_EST (P_COD_EST_AOS   IN     NUMBER,
                                  P_COD_SIS_ENT   IN     CHAR,
                                  p_CURSOR           OUT SYS_REFCURSOR,
                                  P_ERROR            OUT NUMBER)
   IS
      v_NOM_SP        VARCHAR2 (30) := 'Sp_PAB_CANT_OPE_EST';
      v_COD_SIS_ENT   CHAR (10);
   BEGIN
      --        IF(P_COD_SIS_ENT = PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SE_OOFF )THEN
      --            v_COD_SIS_ENT := NULL;
      --        ELSE
      --            v_COD_SIS_ENT := P_COD_SIS_ENT;
      --        END IF;
      --
      --
      --        --Estado Nomina
      --        IF P_COD_EST_AOS = PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEVAL   OR P_COD_EST_AOS= PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEINV  OR
      --         P_COD_EST_AOS= PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEDUP OR P_COD_EST_AOS= PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEELI
      --         OR P_COD_EST_AOS= PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPECUR OR P_COD_EST_AOS= PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEREC THEN --Estados 2, 3 y 17
      --
      --            BEGIN
      --
      --                OPEN p_CURSOR FOR
      --          SELECT 7344 AS CANT_OPE         --COUNT (DETNMN.COD_EST_AOS) AS CANT_OPE
      --                    , DETNMN.COD_EST_AOS
      --            , EST.DSC_EST_AOS  AS DSC_EST
      --            , FN_PAB_BUS_ULT_REG_OPE_EST(P_COD_EST_AOS) AS ULT_REG
      --            , FN_PAB_RISG_COLR(P_COD_SIS_ENT,P_COD_EST_AOS) AS COLOR
      --                FROM PABS_DT_DETLL_NOMNA DETNMN,
      --                    PABS_DT_CBCRA_NOMNA CABNOM,
      --                    PABS_DT_ESTDO_ALMOT EST,
      --                    PABS_DT_SISTM_ENTRD_SALID CAN
      --                WHERE DETNMN.COD_EST_AOS = EST.COD_EST_AOS
      --                    AND DETNMN.NUM_FOL_NMN = CABNOM.NUM_FOL_NMN
      --                    AND CAN.COD_SIS_ENT_SAL = CABNOM.COD_SIS_ENT
      --                    AND DETNMN.COD_EST_AOS= P_COD_EST_AOS
      --                    AND DETNMN.FEC_ISR_OPE BETWEEN v_dia_ini and v_dia_fin --Solo las nominas del dia
      --                   AND CABNOM.COD_SIS_ENT = P_COD_SIS_ENT
      --                GROUP BY EST.DSC_EST_AOS, DETNMN.COD_EST_AOS;
      --
      --            END;
      --
      --         --Estado Operacion
      --         ELSE
      --             BEGIN
      --                   OPEN p_CURSOR FOR
      --                         SELECT 7344 AS CANT_OPE --COUNT (DETOPE.COD_EST_AOS) AS CANT_OPE
      --                            , DETOPE.COD_EST_AOS
      --                            , EST.DSC_EST_AOS  AS DSC_EST
      --                            , FN_PAB_BUS_ULT_REG_OPE_EST(P_COD_EST_AOS) AS ULT_REG
      --                            , FN_PAB_RISG_COLR(P_COD_SIS_ENT,P_COD_EST_AOS) AS COLOR
      --                    FROM PABS_DT_DETLL_OPRCN DETOPE,
      --                            PABS_DT_ESTDO_ALMOT EST,
      --                            PABS_DT_SISTM_ENTRD_SALID ENT
      --                    WHERE DETOPE.COD_EST_AOS = EST.COD_EST_AOS
      --                    AND DETOPE.COD_SIS_ENT = ENT.COD_SIS_ENT_SAL
      --                    AND DETOPE.COD_EST_AOS = P_COD_EST_AOS
      --                    AND (DETOPE.COD_SIS_ENT = NVL(v_COD_SIS_ENT,DETOPE.COD_SIS_ENT)
      --                         OR DETOPE.COD_SIS_SAL = NVL(v_COD_SIS_ENT,DETOPE.COD_SIS_ENT))
      --                    AND DETOPE.FEC_ISR_OPE BETWEEN v_dia_ini and v_dia_fin --Solo las nominas del dia
      --                    GROUP BY EST.DSC_EST_AOS, DETOPE.COD_EST_AOS;
      --
      --             END;
      --
      --        END IF;

      --
      p_ERROR := PKG_PAB_CONSTANTES.v_OK;
   --
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END Sp_PAB_CANT_OPE_EST;

   --***********************************************************************************************
   -- Funcion/Procedimiento: FN_PAB_BUS_HOR_INICIO
   -- Objetivo: Consulta el horario del corte horario - INICIO
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01
   -- Tablas Usadas: PABS_DT_SISTM_ENTRD_SALID
   -- Fecha: 09/11/2016
   -- Autor: gcorrea
   -- Input:
   -- P_COD_SIS_SAL -> CODIGO SISTEMA ENTRADA SALIDA (AREA)
   -- N/A
   -- Output:
   -- N/A.
   -- Input/Output:
   -- N/A.
   -- Retorno:
   -- V_HORA_INICIO -> valor de retorno que muestra el horario de inicio segun el area
   -- Observaciones: <Fecha y Detalle de ?ltimos cambios>
   --***********************************************************************************************
   FUNCTION FN_PAB_BUS_HOR_INICIO (P_COD_SIS_SAL IN CHAR)
      RETURN VARCHAR2
   IS
      V_NOM_SP        VARCHAR2 (30) := 'FN_PAB_BUS_HOR_INICIO';
      V_HORA_INICIO   VARCHAR2 (5);
   BEGIN
      --
      SELECT      SUBSTR (LPAD (HOR_ICO_SIS, 4, '0'), 1, 2)
               || ':'
               || SUBSTR (LPAD (HOR_ICO_SIS, 4, '0'), 3, 2)
                  AS INICIO
        INTO   V_HORA_INICIO
        FROM   PABS_DT_SISTM_ENTRD_SALID ENS
       WHERE   ENS.COD_SIS_ENT_SAL = P_COD_SIS_SAL;

      --
      RETURN V_HORA_INICIO;
   --
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END FN_PAB_BUS_HOR_INICIO;

   --***********************************************************************************************
   -- Funcion/Procedimiento: FN_PAB_BUS_HOR_CIERRE
   -- Objetivo: Consulta el horario del corte horario - CIERRE
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01
   -- Tablas Usadas: PABS_DT_SISTM_ENTRD_SALID
   -- Fecha: 09/11/2016
   -- Autor: gcorrea
   -- Input:
   -- P_COD_SIS_SAL -> CODIGO SISTEMA ENTRADA SALIDA (AREA)
   -- N/A
   -- Output:
   -- N/A.
   -- Input/Output:
   -- N/A.
   -- Retorno:
   -- V_HORA_CIERRE -> valor de retorno que muestra el horario de CIERRE segun el area
   -- Observaciones: <Fecha y Detalle de ?ltimos cambios>
   --***********************************************************************************************
   FUNCTION FN_PAB_BUS_HOR_CIERRE (P_COD_SIS_SAL IN CHAR)
      RETURN VARCHAR2
   IS
      V_NOM_SP        VARCHAR2 (30) := 'FN_PAB_BUS_HOR_CIERRE';
      V_HORA_CIERRE   VARCHAR2 (5);
   BEGIN
      --
      SELECT      SUBSTR (LPAD (HOR_CRR_SIS, 4, '0'), 1, 2)
               || ':'
               || SUBSTR (LPAD (HOR_CRR_SIS, 4, '0'), 3, 2)
                  AS CIERRE
        INTO   V_HORA_CIERRE
        FROM   PABS_DT_SISTM_ENTRD_SALID ENS
       WHERE   ENS.COD_SIS_ENT_SAL = P_COD_SIS_SAL;

      --
      RETURN V_HORA_CIERRE;
   --
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END FN_PAB_BUS_HOR_CIERRE;

   -- ***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_BUS_CRTE_HOR
   -- Objetivo: Procedimiento almacenado que obtiene el horario de inicio y cierre por area
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_SISTM_ENTRD_SALID
   -- Fecha: 09/11/16
   -- Autor: gcorrea
   -- Input:
   -- P_COD_SIS_ENT -> codigo de sistema entrada
   -- Output:
   -- p_CURSOR -> Cursor de salida
   -- p_ERROR-> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ?ltimos cambios>
   -- ***********************************************************************************************
   PROCEDURE Sp_PAB_BUS_CRTE_HOR (P_COD_SIS_ENT   IN     CHAR,
                                  p_CURSOR           OUT SYS_REFCURSOR,
                                  P_ERROR            OUT NUMBER)
   IS
      v_NOM_SP   VARCHAR2 (30) := 'Sp_PAB_BUS_CRTE_HOR';
   BEGIN
      OPEN p_CURSOR FOR
         SELECT   FN_PAB_BUS_HOR_INICIO (P_COD_SIS_ENT) AS INICIO,
                  FN_PAB_BUS_HOR_CIERRE (P_COD_SIS_ENT) AS CIERRE,
                  ENS.DSC_SIS_ENT_SAL
           FROM   PABS_DT_SISTM_ENTRD_SALID ENS
          WHERE   ENS.COD_SIS_ENT_SAL = P_COD_SIS_ENT;

      p_ERROR := PKG_PAB_CONSTANTES.v_OK;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END Sp_PAB_BUS_CRTE_HOR;

   -- ***********************************************************************************************
   -- Funcion/Procedimiento: FN_PAB_RISG_COLR
   -- Objetivo: Funcion que retorna el color segun horario en el que se encuentra.
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_DETLL_NOMNA
   -- Fecha: 21/06/16
   -- Autor: gcorrea
   -- Input:
   -- p_COD_SIS_ENT -> Codigo de sistema entrada
   -- P_COD_EST     -> Codigo estado nomina
   -- P_IND_NOM_OPE -> INDICA SI ES NOMINA 0 OPERACION 1
   -- Output:
   -- p_ERROR-> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: V_COLOR -> color segun el horario
   -- Observaciones: <Fecha y Detalle de ?ltimos cambios>
   -- ***********************************************************************************************
   FUNCTION FN_PAB_RISG_COLR (P_COD_SIS_ENT   IN CHAR,
                              P_COD_EST       IN NUMBER,
                              P_IND_NOM_OPE   IN NUMBER)
      RETURN VARCHAR2
   IS
      --
      v_NOM_SP          VARCHAR2 (30) := 'FN_PAB_RISG_COLR';
      V_TIME_BD         NUMBER := 0;
      V_HOR_CIERRE      NUMBER := 0;
      V_PRIMER_TRAMO    NUMBER := 0;
      V_SEGUNDO_TRAMO   NUMBER := 0;
      V_COLOR           VARCHAR2 (20);
      V_CANTIDAD        NUMBER := 0;
      V_COD_SIS_ENT     CHAR (10);
   BEGIN
      BEGIN
         SELECT   TO_NUMBER (TO_CHAR (SYSDATE, 'HH24MI'))
           INTO   V_TIME_BD
           FROM   DUAL;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            err_code := 'Problema al convertir fecha en numero';
            err_msg := SUBSTR (SQLERRM, 1, 300);
            RAISE;
         WHEN OTHERS
         THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
            p_s_mensaje := err_code || '-' || err_msg;
            RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
      END;

      BEGIN
         SELECT   ENT.HOR_CRR_SIS
           INTO   V_HOR_CIERRE
           FROM   PABS_DT_SISTM_ENTRD_SALID ENT
          WHERE   COD_SIS_ENT_SAL = P_COD_SIS_ENT;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            err_code :=
               'Problema al obtener horario de cierre de:' || P_COD_SIS_ENT;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            RAISE;
         WHEN OTHERS
         THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
            p_s_mensaje := err_code || '-' || err_msg;
            RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
      END;

      V_PRIMER_TRAMO := V_HOR_CIERRE - 200;                          --2 Horas
      V_SEGUNDO_TRAMO := V_HOR_CIERRE - 400;                         --4 Horas

      --Alerta Error
      IF V_TIME_BD >= V_PRIMER_TRAMO
      THEN
         --Se debe indicar ciertos estados
         IF P_COD_EST IN
                  (PKG_PAB_CONSTANTES.V_COD_EST_AOS_NMNVAL,
                   PKG_PAB_CONSTANTES.V_COD_EST_AOS_NMNCUR,
                   PKG_PAB_CONSTANTES.V_COD_EST_AOS_NMNINV,
                   PKG_PAB_CONSTANTES.V_COD_EST_AOS_NMNREC,
                   PKG_PAB_CONSTANTES.V_COD_EST_AOS_NMNDUP,
                   PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEVAL,
                   PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEDUP,
                   PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEINV,
                   PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPECUR,
                   PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEREC,
                   PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPVI,
                   PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEINGAR,
                   PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAT,
                   PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEAUT,
                   PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPELIB,
                   PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEENV,
                   PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPDV,
                   PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEREG,
                   PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAS,
                   PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAB)
         THEN
            V_COLOR := 'bg-light-red';                                 -- Rojo
         ELSE
            --Son estados que no se tienen que alertar
            V_COLOR := 'bg-green';                                    -- Verde
         END IF;
      --Alerta Warning
      ELSIF V_TIME_BD < V_PRIMER_TRAMO AND V_TIME_BD >= V_SEGUNDO_TRAMO
      THEN
         --Se debe indicar ciertos estados
         IF P_COD_EST IN
                  (PKG_PAB_CONSTANTES.V_COD_EST_AOS_NMNVAL,
                   PKG_PAB_CONSTANTES.V_COD_EST_AOS_NMNCUR,
                   PKG_PAB_CONSTANTES.V_COD_EST_AOS_NMNINV,
                   PKG_PAB_CONSTANTES.V_COD_EST_AOS_NMNREC,
                   PKG_PAB_CONSTANTES.V_COD_EST_AOS_NMNDUP,
                   PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEVAL,
                   PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEDUP,
                   PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEINV,
                   PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPECUR,
                   PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEREC,
                   PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPVI,
                   PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEINGAR,
                   PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAT,
                   PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEAUT,
                   PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPELIB,
                   PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEENV,
                   PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPDV,
                   PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEREG,
                   PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAS,
                   PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAB)
         THEN
            V_COLOR := 'bg-orange';                                  --Naranjo
         ELSE
            --Son estados que no se tienen que alertar
            V_COLOR := 'bg-green';                                    -- Verde
         END IF;
      --Sin problemas
      ELSE
         V_COLOR := 'bg-green';                                        --verde
      END IF;

      --SI es una nomina y es autorizacion
      IF (V_IND_NOM = P_IND_NOM_OPE
          AND P_COD_EST = PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEAUT)
      THEN
         V_COLOR := 'bg-green';                                        --verde
      END IF;

      --Si es un ingreso a garantia o autorizacion se debe verificar si existen operaciones del d�a que no han sido
      IF (P_IND_NOM_OPE = V_IND_OPE
          AND P_COD_EST = PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEINGAR)
      THEN
         BEGIN
            --Verificamos si existen operaciones del d�a de garantia
            SELECT   COUNT (1)
              INTO   V_CANTIDAD
              FROM   PABS_DT_DETLL_OPRCN DETOPE
             WHERE       DETOPE.FLG_EGR_ING = PKG_PAB_CONSTANTES.V_FLG_EGR --Egreso
                     AND DETOPE.COD_EST_AOS = P_COD_EST
                     AND DETOPE.FEC_VTA = v_dia_hoy         --Operaci�n de hoy
                     AND DETOPE.COD_SIS_ENT =
                           PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SE_OOFF;

            --Si no existen operaciones se envia siempre verde
            IF (V_CANTIDAD = 0)
            THEN
               V_COLOR := 'bg-green';                                  --verde
            END IF;
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               err_code :=
                  'Problema al buscar cantidad operaciones de garantia de egreso a fecha:'
                  || v_dia_hoy
                  || ' Estado:'
                  || P_COD_EST;
               err_msg := SUBSTR (SQLERRM, 1, 300);
               RAISE;
            WHEN OTHERS
            THEN
               err_code := SQLCODE;
               err_msg := SUBSTR (SQLERRM, 1, 300);
               PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                   v_NOM_PCK,
                                                   err_msg,
                                                   v_NOM_SP);
               p_s_mensaje := err_code || '-' || err_msg;
               RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
         END;
      END IF;

      --Si es un autorizacion de una operacion se debe validar que no existan con valuta del d�a
      IF (V_IND_OPE = P_IND_NOM_OPE
          AND P_COD_EST = PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEAUT)
      THEN
         BEGIN
            --Tecnologia debe ver el ultimo registro de todo
            IF (P_COD_SIS_ENT NOT IN
                      (PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SE_TECNO,
                       PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SE_OOFF))
            THEN
               V_COD_SIS_ENT := P_COD_SIS_ENT;                     --Filtramos
            END IF;

            --Verificamos si existen operaciones a pagar con valuta del d�a
            SELECT   COUNT (1)
              INTO   V_CANTIDAD
              FROM   PABS_DT_DETLL_OPRCN DETOPE
             WHERE   DETOPE.FLG_EGR_ING = PKG_PAB_CONSTANTES.V_FLG_EGR --Egreso
                     AND DETOPE.COD_EST_AOS =
                           PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEAUT -- Autorizada
                     AND DETOPE.FEC_VTA = v_dia_hoy         --Operaci�n de hoy
                     AND DETOPE.COD_SIS_ENT =
                           NVL (V_COD_SIS_ENT, DETOPE.COD_SIS_ENT);

            --Si no existen operaciones se envia siempre verde
            IF (V_CANTIDAD = 0)
            THEN
               V_COLOR := 'bg-green';                                  --verde
            END IF;
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               err_code := 'Problema al buscar cantidad operaciones de egreso';
               err_msg := SUBSTR (SQLERRM, 1, 300);
               RAISE;
            WHEN OTHERS
            THEN
               err_code := SQLCODE;
               err_msg := SUBSTR (SQLERRM, 1, 300);
               PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                   v_NOM_PCK,
                                                   err_msg,
                                                   v_NOM_SP);
               p_s_mensaje := err_code || '-' || err_msg;
               RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
         END;
      END IF;

      RETURN V_COLOR;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         --Grabamos Error
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END FN_PAB_RISG_COLR;

   --***********************************************************************************************
   -- Funcion/Procedimiento: FN_PAB_BUS_ULT_REG_OPE_EST
   -- Objetivo: Consulta ultimo registro por estado de operaciones
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01
   -- Tablas Usadas: PABS_DT_ESTDO_ALMOT, PABS_DT_DETLL_OPRCN,PABS_DT_SISTM_ENTRD_SALID
   -- Fecha: 25/10/2016
   -- Autor: gcorrea
   -- Input:
   -- P_COD_EST_AOS -> CODIGO ESTADO NOMINA
   -- N/A
   -- Output:
   -- N/A.
   -- Input/Output:
   -- N/A.
   -- Retorno:
   -- V_ULTIMO_REGISTRO -> valor de retorno que muestra la ultima fecha y horario por estado de nomina
   -- Observaciones: <Fecha y Detalle de ?ltimos cambios>
   --***********************************************************************************************
   -- FUNCTION FN_PAB_BUS_ULT_REG_OPE_EST ( P_COD_EST_AOS IN NUMBER  ) RETURN VARCHAR2 IS
   --
   --    V_NOM_SP        VARCHAR2(30) := 'FN_PAB_BUS_ULT_REG_OPE_EST';
   --    V_ULTIMO_REGISTRO     VARCHAR2(8) ;
   --    V_EST_PVI NUMBER;
   --
   --    BEGIN
   --
   ----            IF P_COD_EST_AOS = PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEAUT THEN
   ----                V_EST_PVI := PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPVI;
   ----            ELSE
   ----                V_EST_PVI := 0;
   ----            END IF;
   ----
   ----
   ----            SELECT TO_CHAR(MAX(DETNMN.FEC_ACT_NMN),'HH24:MI:SS')
   ----            INTO V_ULTIMO_REGISTRO
   ----            FROM PABS_DT_DETLL_NOMNA DETNMN,
   ----            PABS_DT_ESTDO_ALMOT EST
   ----            WHERE DETNMN.COD_EST_AOS  = EST.COD_EST_AOS
   ----            AND DETNMN.COD_EST_AOS = P_COD_EST_AOS-- IN(P_COD_EST_AOS, V_EST_PVI)
   ----            AND DETNMN.FEC_ISR_OPE BETWEEN v_dia_ini and v_dia_fin; --Solo las nominas del dia

   ----
   --
   --        RETURN V_ULTIMO_REGISTRO;
   --
   --    EXCEPTION
   --        WHEN NO_DATA_FOUND
   --        THEN
   --         err_code := SQLCODE;
   --         err_msg := SUBSTR (SQLERRM, 1, 300);
   --         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
   --                                             v_NOM_PCK,
   --                                             err_msg,
   --                                             v_NOM_SP);
   --        WHEN OTHERS
   --        THEN
   --         err_code := SQLCODE;
   --         err_msg := SUBSTR (SQLERRM, 1, 300);
   --         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
   --                                             v_NOM_PCK,
   --                                             err_msg,
   --                                             v_NOM_SP);
   --         p_s_mensaje := err_code || '-' || err_msg;
   --         RAISE_APPLICATION_ERROR(-20000, p_s_mensaje);
   --
   --    END FN_PAB_BUS_ULT_REG_OPE_EST;

   --***********************************************************************************************
   -- Funcion/Procedimiento: FN_PAB_BUS_ULT_REG_OPE_EST
   -- Objetivo: Consulta ultimo registro por estado de operaciones
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01
   -- Tablas Usadas: PABS_DT_ESTDO_ALMOT, PABS_DT_DETLL_OPRCN,PABS_DT_SISTM_ENTRD_SALID
   -- Fecha: 27-12-2017
   -- Autor: gcorrea
   -- Input:
   -- p_COD_SIS_ENT -> Codigo de sistema entrada
   -- P_COD_EST_AOS -> CODIGO ESTADO NOMINA
   -- P_FLG_EGR_ING -> Flag Ingreso o egreso de la operacion
   -- N/A
   -- Output:
   -- N/A.
   -- Input/Output:
   -- N/A.
   -- Retorno:
   -- V_ULTIMO_REGISTRO -> valor de retorno que muestra la ultima fecha y horario por estado de nomina
   -- Observaciones: <Fecha y Detalle de ?ltimos cambios>
   --***********************************************************************************************
   FUNCTION FN_PAB_BUS_ULT_REG_OPE_EST (P_COD_SIS_ENT   IN CHAR,
                                        P_COD_EST_AOS   IN NUMBER,
                                        P_FLG_EGR_ING   IN NUMBER)
      RETURN VARCHAR2
   IS
      V_NOM_SP            VARCHAR2 (30) := 'FN_PAB_BUS_ULT_REG_OPE_EST';
      V_ULTIMO_REGISTRO   VARCHAR2 (12);
      V_COD_SIS_ENT       CHAR (10) := NULL;
   BEGIN
      --Tecnologia debe ver el ultimo registro de todo
      IF (P_COD_SIS_ENT NOT IN
                (PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SE_TECNO,
                 PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SE_OOFF))
      THEN
         V_COD_SIS_ENT := P_COD_SIS_ENT;                           --Filtramos
      END IF;

      SELECT   TO_CHAR (MAX (DETOPE.FEC_ACT_OPE), 'HH24:MI:SS')
        INTO   V_ULTIMO_REGISTRO
        FROM   PABS_DT_DETLL_OPRCN DETOPE
       WHERE   DETOPE.COD_SIS_ENT = NVL (V_COD_SIS_ENT, DETOPE.COD_SIS_ENT) --Filtramos por perfil
               AND DETOPE.FLG_EGR_ING = P_FLG_EGR_ING
               AND DETOPE.COD_EST_AOS = P_COD_EST_AOS
               AND DETOPE.FEC_VTA >= v_dia_hoy; --Todas las operaciones que tienen valuta mayor o igual a hoy

      RETURN V_ULTIMO_REGISTRO;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg :=
               'No se encuentra registro:'
            || v_dia_hoy
            || ' V_COD_SIS_ENT:'
            || V_COD_SIS_ENT;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END FN_PAB_BUS_ULT_REG_OPE_EST;

   /************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_BUS_HIT_TBJ
   -- Objetivo: Procedimiento almacenado que retorna lista de hitos para OOFF
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_SISTM_ENTRD_SALID
   --
   -- Fecha: 04/07/17
   -- Autor: gcorrea CAH
   -- Input: p_COD_SIS_ENT      Area de trabajo, OOFF, CANJE, Etc.
   -- Output:
   --        p_CURSOR : SYS_REFCURSOR, resultados de consulta.
   --        p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE Sp_PAB_BUS_HIT_TBJ (p_COD_SIS_ENT   IN     CHAR,
                                 p_CURSOR           OUT SYS_REFCURSOR,
                                 p_ERROR            OUT NUMBER)
   IS
      v_NOM_SP            VARCHAR2 (30) := 'Sp_PAB_BUS_HIT_TBJ';
      v_FLG_SIS_ENT_SAL   NUMBER (1) := 5;
   BEGIN
      OPEN p_CURSOR FOR
         SELECT   TO_CHAR (TO_DATE (LPAD (DET_HIT.HOR_INI, 4, 0), 'HH24:MI'),
                           'HH24:MI')
                     AS HORA,
                  TO_CHAR (TO_DATE (LPAD (DET_HIT.HOR_FIN, 4, 0), 'HH24:MI'),
                           'HH24:MI')
                  || ' - '
                  || HIT.DSC_HIT_SIS
                     AS GLOSA
           FROM   PABS_DT_HITO_TRBJO_SISTM DET_HIT, PABS_DT_HITO_TRBJO HIT
          WHERE   HIT.COD_HIT_SIS = DET_HIT.COD_HIT_SIS
                  AND DET_HIT.COD_SIS_ENT_SAL = p_COD_SIS_ENT;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         p_s_mensaje := err_code || '-' || err_msg;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END Sp_PAB_BUS_HIT_TBJ;

   -- ********************************************************************************************************************
   -- FUNCION/PROCEDIMIENTO: SP_PAB_MNI_EGR_ENT
   -- OBJETIVO: SP MUESTRA ACTUALIZACI�N EN PANTALLA MONITOREO PARA EGRESOS EN MONEDA NACIONAL Y EXTRANJERA (ENTRADA).
   -- SISTEMA: DBO_PAB.
   -- BASE DE DATOS: DBO_PAB.
   -- TABLAS USADAS: PABS_DT_SISTM_ENTRD_SALID
   -- FECHA: 09-08-2017
   -- AUTOR: GCORREA- GSD
   -- INPUT:
   -- P_FLAG_DVI -> INDICADOR DE DIVISA  (0 MN, 1 MX)
   -- OUTPUT:
   -- P_CURSOR -> CURSOR DE RESULTADO
   -- P_ERROR -> INDICADOR DE ERRORES: 0 SIN ERRORES 1 EXISTEN ERRORES.
   -- OBSERVACIONES: <FECHA Y DETALLE DE ULTIMOS CAMBIOS>
   -- ********************************************************************************************************************
   PROCEDURE SP_PAB_MNI_EGR_ENT (P_FLAG_DVI   IN     NUMBER,
                                 P_CURSOR        OUT SYS_REFCURSOR,
                                 P_ERROR         OUT NUMBER)
   IS
      V_NOM_SP            VARCHAR2 (30) := 'SP_PAB_MNI_EGR_ENT';
      V_COD_EST_AOS_LIB   NUMBER := PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEAUT; --OPERACIONES AUTORIZADAS PARA LIBERAR
      V_COD_EST_AOS_PAG   NUMBER := PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAG; --OPERACIONES PAGADAS.
      V_FLG_SIS_ENT       NUMBER := PKG_PAB_CONSTANTES.V_FLG_SIS_ENT; --ENTRADA
   BEGIN
      BEGIN
         IF P_FLAG_DVI = PKG_PAB_CONSTANTES.V_FLG_MRD_MN
         THEN
            --PARA MONEDA NACIONAL
            OPEN P_CURSOR FOR
                 SELECT   DISTINCT
                          CAN.DSC_SIS_ENT_SAL AS CANAL,
                          FN_PAB_BUS_CAN_OPE_TOT (CAN.COD_SIS_ENT_SAL,
                                                  v_FLG_SIS_ENT,
                                                  P_FLAG_DVI)
                             AS TOTAL,
                          FN_PAB_BUS_CAN_OPE_LIB (CAN.COD_SIS_ENT_SAL,
                                                  v_FLG_SIS_ENT,
                                                  P_FLAG_DVI)
                             AS POR_LIBERAR,
                          FN_PAB_BUS_CAN_OPE_PAG (CAN.COD_SIS_ENT_SAL,
                                                  v_FLG_SIS_ENT,
                                                  P_FLAG_DVI)
                             AS PAGADA,
                          FN_PAB_BUS_CAN_OPE_NPAG (CAN.COD_SIS_ENT_SAL,
                                                   V_FLG_SIS_ENT,
                                                   P_FLAG_DVI)
                             AS NO_PAGADA
                   FROM   PABS_DT_SISTM_ENTRD_SALID CAN
                  WHERE   (CAN.FLG_SIS_ENT_SAL IN
                                 (PKG_PAB_CONSTANTES.V_FLG_SIS_ENT,
                                  PKG_PAB_CONSTANTES.V_FLG_SIS_900_910)
                           OR CAN.COD_SIS_ENT_SAL IN
                                   (PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SAL_CTACTE,
                                    PKG_PAB_CONSTANTES.V_COD_TIP_BOLSA))
                          AND CAN.COD_SIS_ENT_SAL !=
                                PKG_PAB_CONSTANTES.V_COD_TIP_LEAFAC
               ORDER BY   CAN.DSC_SIS_ENT_SAL;

            P_ERROR := PKG_PAB_CONSTANTES.V_OK;
         ELSE
            --PARA MONEDA EXTRANJERA
            OPEN P_CURSOR FOR
                 SELECT   CAN.DSC_SIS_ENT_SAL AS CANAL,
                          FN_PAB_BUS_CAN_OPE_TOT (CAN.COD_SIS_ENT_SAL,
                                                  v_FLG_SIS_ENT,
                                                  P_FLAG_DVI)
                             AS TOTAL,
                          FN_PAB_BUS_CAN_OPE_LIB (CAN.COD_SIS_ENT_SAL,
                                                  v_FLG_SIS_ENT,
                                                  P_FLAG_DVI)
                             AS POR_LIBERAR,
                          FN_PAB_BUS_CAN_OPE_PAG (CAN.COD_SIS_ENT_SAL,
                                                  v_FLG_SIS_ENT,
                                                  P_FLAG_DVI)
                             AS PAGADA,
                          FN_PAB_BUS_CAN_OPE_NPAG (CAN.COD_SIS_ENT_SAL,
                                                   V_FLG_SIS_ENT,
                                                   P_FLAG_DVI)
                             AS NO_PAGADA
                   FROM   PABS_DT_SISTM_ENTRD_SALID CAN
                  WHERE   (CAN.FLG_SIS_ENT_SAL IN
                                 (PKG_PAB_CONSTANTES.V_FLG_SIS_ENT)
                           OR CAN.COD_SIS_ENT_SAL =
                                PKG_PAB_CONSTANTES.V_COD_TIP_BOLSA)
                          AND CAN.COD_SIS_ENT_SAL NOT IN
                                   (PKG_PAB_CONSTANTES.V_COD_TIP_CUSTODIA,
                                    PKG_PAB_CONSTANTES.V_COD_TIP_LBTRCOMB,
                                    PKG_PAB_CONSTANTES.V_COD_TIP_OB,
                                    PKG_PAB_CONSTANTES.V_COD_TIP_LEAFAC,
                                    PKG_PAB_CONSTANTES.V_COD_TIP_CAJA)
               ORDER BY   CAN.DSC_SIS_ENT_SAL;

            P_ERROR := PKG_PAB_CONSTANTES.V_OK;
         END IF;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            ERR_CODE := SQLCODE;
            ERR_MSG := SUBSTR (SQLERRM, 1, 300);
            PKG_PAB_TRACKING.SP_PAB_INS_LOG_BD (ERR_CODE,
                                                V_NOM_PCK,
                                                ERR_MSG,
                                                V_NOM_SP);
            P_ERROR := PKG_PAB_CONSTANTES.V_ERROR;
         WHEN OTHERS
         THEN
            ERR_CODE := SQLCODE;
            ERR_MSG := SUBSTR (SQLERRM, 1, 300);
            PKG_PAB_TRACKING.SP_PAB_INS_LOG_BD (ERR_CODE,
                                                V_NOM_PCK,
                                                ERR_MSG,
                                                V_NOM_SP);
            P_ERROR := PKG_PAB_CONSTANTES.V_ERROR;
            P_S_MENSAJE := ERR_CODE || '-' || ERR_MSG;
            RAISE_APPLICATION_ERROR (-20000, P_S_MENSAJE);
      END;
   END SP_PAB_MNI_EGR_ENT;

   -- *****************************************************************************************************************
   -- FUNCION/PROCEDIMIENTO: SP_PAB_MNI_EGR_SAL
   -- OBJETIVO: SP MUESTRA ACTUALIZACI�N EN PANTALLA MONITOREO PARA EGRESOS EN MONEDA NACIONAL Y EXTRANJERA (SALIDA).
   -- SISTEMA: DBO_PAB.
   -- BASE DE DATOS: DBO_PAB.
   -- TABLAS USADAS: PABS_DT_SISTM_ENTRD_SALID
   -- FECHA: 21-07-2017
   -- AUTOR: GCORREA- CAH
   -- INPUT:
   -- P_FLAG_DVI -> INDICADOR DE DIVISA
   -- OUTPUT:
   -- P_CURSOR -> CURSOR DE RESULTADO
   -- P_ERROR-> INDICADOR DE ERRORES: 0 SIN ERRORES 1 EXISTEN ERRORES.
   -- OBSERVACIONES: <FECHA Y DETALLE DE ULTIMOS CAMBIOS>
   -- *****************************************************************************************************************
   PROCEDURE Sp_PAB_MNI_EGR_SAL (p_FLAG_DVI   IN     NUMBER,
                                 p_CURSOR        OUT SYS_REFCURSOR,
                                 P_ERROR         OUT NUMBER)
   IS
      v_NOM_SP            VARCHAR2 (30) := 'Sp_PAB_MNI_EGR_SAL';
      v_COD_EST_AOS_LIB   NUMBER := PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEAUT; --Operaciones autorizadas para liberar
      v_COD_EST_AOS_PAG   NUMBER := PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAG; --Operaciones Pagadas.
      v_FLG_SIS_SAL       NUMBER := PKG_PAB_CONSTANTES.V_FLG_SIS_SAL;
   BEGIN
      BEGIN
         IF p_FLAG_DVI = PKG_PAB_CONSTANTES.V_FLG_MRD_MN
         THEN
            OPEN p_CURSOR FOR
                 --MN
                 SELECT   DISTINCT
                          CAN.DSC_SIS_ENT_SAL AS CANAL,
                          FN_PAB_BUS_CAN_OPE_TOT_SAL (CAN.COD_SIS_ENT_SAL,
                                                      v_FLG_SIS_SAL,
                                                      p_FLAG_DVI)
                             AS TOTAL,        --Modificar segun estado mostrar
                          FN_PAB_BUS_CAN_OPE_LIB (CAN.COD_SIS_ENT_SAL,
                                                  v_FLG_SIS_SAL,
                                                  P_FLAG_DVI)
                             AS POR_LIBERAR,
                          FN_PAB_BUS_CAN_OPE_PAG (CAN.COD_SIS_ENT_SAL,
                                                  v_FLG_SIS_SAL,
                                                  P_FLAG_DVI)
                             AS PAGADA,      --Modificar segun estadoa mostrar
                          FN_PAB_BUS_CAN_OPE_NPAG (CAN.COD_SIS_ENT_SAL,
                                                   v_FLG_SIS_SAL,
                                                   P_FLAG_DVI)
                             AS NO_PAGADA    --Modificar segun estadoa mostrar
                   FROM   PABS_DT_SISTM_ENTRD_SALID CAN
                  WHERE   CAN.FLG_SIS_ENT_SAL =
                             PKG_PAB_CONSTANTES.V_FLG_SIS_SAL
                          AND CAN.COD_SIS_ENT_SAL !=
                                PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SAL_SWF
               ORDER BY   CAN.DSC_SIS_ENT_SAL;
         ELSE
            OPEN p_CURSOR FOR
                 --MX
                 SELECT   DISTINCT
                          CAN.DSC_SIS_ENT_SAL AS CANAL,
                          FN_PAB_BUS_CAN_OPE_TOT_SAL (CAN.COD_SIS_ENT_SAL,
                                                      v_FLG_SIS_SAL,
                                                      p_FLAG_DVI)
                             AS TOTAL,        --Modificar segun estado mostrar
                          FN_PAB_BUS_CAN_OPE_LIB (CAN.COD_SIS_ENT_SAL,
                                                  v_FLG_SIS_SAL,
                                                  P_FLAG_DVI)
                             AS POR_LIBERAR,
                          FN_PAB_BUS_CAN_OPE_PAG (CAN.COD_SIS_ENT_SAL,
                                                  v_FLG_SIS_SAL,
                                                  P_FLAG_DVI)
                             AS PAGADA,      --Modificar segun estadoa mostrar
                          FN_PAB_BUS_CAN_OPE_NPAG (CAN.COD_SIS_ENT_SAL,
                                                   v_FLG_SIS_SAL,
                                                   P_FLAG_DVI)
                             AS NO_PAGADA    --Modificar segun estadoa mostrar
                   FROM   PABS_DT_SISTM_ENTRD_SALID CAN
                  WHERE   CAN.FLG_SIS_ENT_SAL =
                             PKG_PAB_CONSTANTES.V_FLG_SIS_SAL
                          AND CAN.COD_SIS_ENT_SAL !=
                                PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SAL_CMN
               ORDER BY   CAN.DSC_SIS_ENT_SAL;
         END IF;

         P_ERROR := PKG_PAB_CONSTANTES.V_OK;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            ERR_CODE := SQLCODE;
            ERR_MSG := SUBSTR (SQLERRM, 1, 300);
            PKG_PAB_TRACKING.SP_PAB_INS_LOG_BD (ERR_CODE,
                                                V_NOM_PCK,
                                                ERR_MSG,
                                                V_NOM_SP);
            P_ERROR := PKG_PAB_CONSTANTES.V_ERROR;
         WHEN OTHERS
         THEN
            ERR_CODE := SQLCODE;
            ERR_MSG := SUBSTR (SQLERRM, 1, 300);
            PKG_PAB_TRACKING.SP_PAB_INS_LOG_BD (ERR_CODE,
                                                V_NOM_PCK,
                                                ERR_MSG,
                                                V_NOM_SP);
            P_ERROR := PKG_PAB_CONSTANTES.V_ERROR;
            P_S_MENSAJE := ERR_CODE || '-' || ERR_MSG;
            RAISE_APPLICATION_ERROR (-20000, P_S_MENSAJE);
      END;
   END SP_PAB_MNI_EGR_SAL;

   -- **************************************************************************************************
   -- FUNCION/PROCEDIMIENTO: SP_PAB_MNI_ING_SAL
   -- OBJETIVO: SP MUESTRA ACTUALIZACI�N EN PANTALLA MONITOREO INGRESO DE MONEDA NACIONAL (ENTRADA Y SALIDA)
   -- SISTEMA: DBO_PAB
   -- BASE DE DATOS: DBO_PAB
   -- TABLAS USADAS: PABS_DT_SISTM_ENTRD_SALID
   -- FECHA: 21-07-2017
   -- AUTOR: GCORREA
   -- INPUT:
   -- P_FLAG_ENT_SAL -> INDICADOR DE FLUJO
   -- OUTPUT:
   -- P_CURSOR -> CURSOR DE RESULTADO
   -- P_ERROR-> INDICADOR DE ERRORES 0 SIN ERRORES 1 EXISTEN ERRORES.
   -- OBSERVACIONES: <FECHA Y DETALLE DE ULTIMOS CAMBIOS>
   -- **************************************************************************************************
   PROCEDURE Sp_PAB_MNI_ING_SAL (p_FLAG_ENT_SAL   IN     NUMBER,
                                 p_CURSOR            OUT SYS_REFCURSOR,
                                 P_ERROR             OUT NUMBER)
   IS
      v_NOM_SP            VARCHAR2 (30) := 'Sp_PAB_MNI_ING_SAL';
      v_COD_EST_AOS_LIB   NUMBER := PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEAUT; --Operaciones autorizadas para liberar
      v_COD_EST_AOS_PAG   NUMBER := PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAG; --Operaciones Pagadas.
      v_FLG_SIS_ENT       NUMBER := PKG_PAB_CONSTANTES.V_FLG_SIS_ENT;
      v_FLG_SIS_SAL       NUMBER := PKG_PAB_CONSTANTES.V_FLG_SIS_SAL;
   --PANTALLA MONITOREO - INGRESO MONEDA NACIONAL:
   --CANALES ENTRADA := 0
   --CANALES SALIDA  := 1
   BEGIN
      BEGIN
         --INGRESO MONEDA NACIONAL SALIDA
         IF p_FLAG_ENT_SAL = PKG_PAB_CONSTANTES.V_FLG_SIS_SAL
         THEN
            OPEN p_CURSOR FOR
                 --SALIDA - INGRESO MONEDA NACIONAL
                 SELECT   DISTINCT
                          CAN.DSC_SIS_ENT_SAL AS CANAL,
                          FN_PAB_BUS_CANT_OPE_NAB (CAN.COD_SIS_ENT_SAL,
                                                   p_FLAG_ENT_SAL)
                             AS POR_ABONAR,
                          FN_PAB_BUS_CANT_OPE_ABO (CAN.COD_SIS_ENT_SAL,
                                                   p_FLAG_ENT_SAL)
                             AS ABONADAS
                   FROM   PABS_DT_SISTM_ENTRD_SALID CAN
                  WHERE   (CAN.FLG_SIS_ENT_SAL IN
                                 (PKG_PAB_CONSTANTES.V_FLG_SIS_ENT,
                                  PKG_PAB_CONSTANTES.V_NUM_MT_AVISOS_PRO,
                                  PKG_PAB_CONSTANTES.V_FLG_SIS_900_910,
                                  PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAT)
                           OR CAN.COD_SIS_ENT_SAL IN
                                   (PKG_PAB_CONSTANTES.V_COD_TIP_BOLSA,
                                    PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SAL_CTACTE))
                          AND CAN.COD_SIS_ENT_SAL NOT IN
                                   (PKG_PAB_CONSTANTES.V_COD_TIP_OPHIPO,
                                    PKG_PAB_CONSTANTES.V_COD_TIP_CUSTODIA,
                                    PKG_PAB_CONSTANTES.V_COD_TIP_CORRBOL,
                                    PKG_PAB_CONSTANTES.V_COD_TIP_GTB,
                                    PKG_PAB_CONSTANTES.V_COD_TIP_LEAFAC,
                                    PKG_PAB_CONSTANTES.V_COD_TIP_CAJA,
                                    PKG_PAB_CONSTANTES.V_COD_TIP_CORRSEG,
                                    PKG_PAB_CONSTANTES.V_COD_TIP_CARGXDISTR,
                                    PKG_PAB_CONSTANTES.V_COD_TIP_OB,
                                    PKG_PAB_CONSTANTES.V_COD_TIP_BANPRI)
               ORDER BY   CAN.DSC_SIS_ENT_SAL;

            P_ERROR := PKG_PAB_CONSTANTES.V_OK;
         ELSE
            OPEN P_CURSOR FOR
               --ENTRADA - INGRESO MONEDA NACIONAL
               SELECT   DISTINCT
                        CAN.DSC_SIS_ENT_SAL AS CANAL,
                        FN_PAB_BUS_CANT_OPE_NAB (CAN.COD_SIS_ENT_SAL,
                                                 P_FLAG_ENT_SAL)
                           AS POR_ABONAR,
                        FN_PAB_BUS_CANT_OPE_ABO (CAN.COD_SIS_ENT_SAL,
                                                 P_FLAG_ENT_SAL)
                           AS ABONADAS
                 FROM   PABS_DT_SISTM_ENTRD_SALID CAN
                WHERE   CAN.COD_SIS_ENT_SAL IN
                              (PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SAL_LBTR,
                               PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SAL_CMN);

            P_ERROR := PKG_PAB_CONSTANTES.V_OK;
         END IF;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            ERR_CODE := SQLCODE;
            ERR_MSG := SUBSTR (SQLERRM, 1, 300);
            PKG_PAB_TRACKING.SP_PAB_INS_LOG_BD (ERR_CODE,
                                                V_NOM_PCK,
                                                ERR_MSG,
                                                V_NOM_SP);
            P_ERROR := PKG_PAB_CONSTANTES.V_ERROR;
         WHEN OTHERS
         THEN
            ERR_CODE := SQLCODE;
            ERR_MSG := SUBSTR (SQLERRM, 1, 300);
            PKG_PAB_TRACKING.SP_PAB_INS_LOG_BD (ERR_CODE,
                                                V_NOM_PCK,
                                                ERR_MSG,
                                                V_NOM_SP);
            P_ERROR := PKG_PAB_CONSTANTES.V_ERROR;
            P_S_MENSAJE := ERR_CODE || '-' || ERR_MSG;
            RAISE_APPLICATION_ERROR (-20000, P_S_MENSAJE);
      END;
   END Sp_PAB_MNI_ING_SAL;

   --***********************************************************************************************
   -- FUNCION/PROCEDIMIENTO: FN_PAB_BUS_CAN_OPE_LIB
   -- OBJETIVO: RETORNA CANTIDAD DE OPERACIONES POR SER LIBERADAS
   -- SISTEMA: DBO_PAB
   -- BASE DE DATOS: DBO_PAB
   -- TABLAS USADAS: PABS_DT_DETLL_OPRCN
   -- FECHA:21/07/2017
   -- AUTOR: GCORREA
   -- INPUT:
   -- P_COD_SIS_ENT_SAL -> CODIGO SISTEMA ENTRADA SALIDA (CANAL)
   -- P_COD_DVI -> INDICADOS DE MONEDA NACIONAL O EXTRANJERA (0 NACIONAL, 1 EXTRANJERA)
   -- OBSERVACIONES: <FECHA Y DETALLE>
   --***********************************************************************************************
   FUNCTION FN_PAB_BUS_CAN_OPE_LIB (P_COD_SIS_ENT_SAL   IN CHAR,
                                    P_FLG_SIS_ENT_SAL      NUMBER,
                                    P_COD_DVI              CHAR)
      RETURN NUMBER
   IS
      V_NOM_SP        VARCHAR2 (30) := 'FN_PAB_BUS_CAN_OPE_LIB';
      V_CANT_LIB      NUMBER (10);
      V_COD_SIS_ENT   CHAR (10);
      V_COD_SIS_SAL   CHAR (10);
   BEGIN
      --P_FLG_SIS_ENT_SAL: 0 ENTRADA / 1 SALIDA (PANTALLA MONITOREO)
      IF P_FLG_SIS_ENT_SAL = PKG_PAB_CONSTANTES.V_FLG_SIS_ENT
      THEN
         V_COD_SIS_ENT := P_COD_SIS_ENT_SAL;
         V_COD_SIS_SAL := NULL;
      ELSE
         V_COD_SIS_ENT := NULL;
         V_COD_SIS_SAL := P_COD_SIS_ENT_SAL;
      END IF;

      --MONEDA NACIONAL
      IF P_COD_DVI = PKG_PAB_CONSTANTES.V_FLG_MRD_MN
      THEN
         SELECT   COUNT (DETOPE.COD_EST_AOS)
           INTO   V_CANT_LIB
           FROM   PABS_DT_DETLL_OPRCN DETOPE
          WHERE   DETOPE.COD_SIS_ENT =
                     NVL (V_COD_SIS_ENT, DETOPE.COD_SIS_ENT)
                  AND DETOPE.COD_SIS_SAL =
                        NVL (V_COD_SIS_SAL, DETOPE.COD_SIS_SAL)
                  AND DETOPE.COD_DVI = PKG_PAB_CONSTANTES.V_STR_DVI_CLP
                  AND (TO_DATE (DETOPE.FEC_ISR_OPE, 'DD_MM_YYYY') =
                          TO_DATE (SYSDATE, 'DD_MM_YYYY')
                       OR TO_DATE (DETOPE.FEC_VTA, 'DD_MM_YYYY') =
                            TO_DATE (SYSDATE, 'DD_MM_YYYY'))
                  AND DETOPE.COD_EST_AOS =
                        PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEAUT; --AUTORIZADA(7)
      --AND DETOPE.COD_SIS_ENT != 'TECNO';
      ELSE                                                 --MONEDA EXTRANJERA
         SELECT   COUNT (DETOPE.COD_EST_AOS)
           INTO   V_CANT_LIB
           FROM   PABS_DT_DETLL_OPRCN DETOPE
          WHERE   DETOPE.COD_SIS_ENT =
                     NVL (V_COD_SIS_ENT, DETOPE.COD_SIS_ENT)
                  AND DETOPE.COD_SIS_SAL =
                        NVL (V_COD_SIS_SAL, DETOPE.COD_SIS_SAL)
                  AND DETOPE.COD_DVI != PKG_PAB_CONSTANTES.V_STR_DVI_CLP
                  AND (TO_DATE (DETOPE.FEC_ISR_OPE, 'DD_MM_YYYY') =
                          TO_DATE (SYSDATE, 'DD_MM_YYYY')
                       OR TO_DATE (DETOPE.FEC_VTA, 'DD_MM_YYYY') =
                            TO_DATE (SYSDATE, 'DD_MM_YYYY'))
                  AND DETOPE.COD_EST_AOS IN
                           (PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEAUT); --AUTORIZADA(7)
      END IF;

      RETURN V_CANT_LIB;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         ERR_CODE := SQLCODE;
         ERR_MSG := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.SP_PAB_INS_LOG_BD (ERR_CODE,
                                             V_NOM_PCK,
                                             ERR_MSG,
                                             V_NOM_SP);
      WHEN OTHERS
      THEN
         ERR_CODE := SQLCODE;
         ERR_MSG := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.SP_PAB_INS_LOG_BD (ERR_CODE,
                                             V_NOM_PCK,
                                             ERR_MSG,
                                             V_NOM_SP);
         P_S_MENSAJE := ERR_CODE || '-' || ERR_MSG;
         RAISE_APPLICATION_ERROR (-20000, P_S_MENSAJE);
   END FN_PAB_BUS_CAN_OPE_LIB;

   --***********************************************************************************************
   -- FUNCION/PROCEDIMIENTO: FN_PAB_BUS_CAN_OPE_PAG
   -- OBJETIVO: RETORNA CANTIDAD DE OPERACIONES PAGADAS
   -- SISTEMA: DBO_PAB
   -- BASE DE DATOS: DBO_PAB
   -- TABLAS USADAS: PABS_DT_DETLL_CARGO_ABONO_CAJA
   -- FECHA:21/07/2017
   -- AUTOR: GCORREA
   -- INPUT:
   -- P_COD_SIS_ENT_SAL -> C�DIGO CANAL
   -- P_FLG_SIS_ENT_SAL -> FLAG CANAL DE SALIDA O ENTRADA
   -- P_COD_DVI -> DIVISA
   -- OBSERVACIONES: <FECHA Y DETALLE DE ?LTIMOS CAMBIOS>
   --***********************************************************************************************
   FUNCTION FN_PAB_BUS_CAN_OPE_PAG (P_COD_SIS_ENT_SAL   IN CHAR,
                                    P_FLG_SIS_ENT_SAL      NUMBER,
                                    P_COD_DVI              NUMBER)
      RETURN NUMBER
   IS
      V_NOM_SP        VARCHAR2 (30) := 'FN_PAB_BUS_CAN_OPE_PAG';
      V_CANT_PAG      NUMBER (10);
      V_COD_SIS_ENT   CHAR (10);
      V_COD_SIS_SAL   CHAR (10);
   BEGIN
      --P_FLG_SIS_ENT_SAL: 0 ENTRADA / 1 SALIDA (PANTALLA MONITOREO)
      IF P_FLG_SIS_ENT_SAL = PKG_PAB_CONSTANTES.V_FLG_SIS_ENT
      THEN
         V_COD_SIS_ENT := P_COD_SIS_ENT_SAL;
         V_COD_SIS_SAL := NULL;
      ELSE
         V_COD_SIS_ENT := NULL;
         V_COD_SIS_SAL := P_COD_SIS_ENT_SAL;
      END IF;

      --MONEDA NACIONAL
      IF P_COD_DVI = PKG_PAB_CONSTANTES.V_FLG_MRD_MN
      THEN
         --TIPO BOLSA (CANAL ENTRADA VAC�O)
         IF P_COD_SIS_ENT_SAL = PKG_PAB_CONSTANTES.V_COD_TIP_BOLSA
         THEN
            SELECT   COUNT (CAJA.NUM_FOL_OPE)
              INTO   V_CANT_PAG
              FROM   PABS_DT_DETLL_CARGO_ABONO_CAJA CAJA
             WHERE       CAJA.COD_MT_SWF = PKG_PAB_CONSTANTES.V_COD_MT900
                     AND CAJA.COD_SIS_ENT IS NULL
                     AND CAJA.COD_DVI = PKG_PAB_CONSTANTES.V_STR_DVI_CLP
                     AND (TO_CHAR (CAJA.FEC_ING_OPE, 'DD_MM_YYYY') =
                             TO_CHAR (SYSDATE, 'DD_MM_YYYY')
                          OR TO_CHAR (CAJA.FEC_VTA, 'DD_MM_YYYY') =
                               TO_CHAR (SYSDATE, 'DD_MM_YYYY'));
         ELSE                                                    --OPERACIONES
            SELECT   SUM (CANTIDAD)
              INTO   V_CANT_PAG
              FROM   (SELECT   COUNT (DETOPE.COD_EST_AOS) CANTIDAD
                        FROM   PABS_DT_DETLL_OPRCN DETOPE
                       WHERE   DETOPE.COD_SIS_ENT =
                                  NVL (V_COD_SIS_ENT, DETOPE.COD_SIS_ENT)
                               AND DETOPE.COD_SIS_SAL =
                                     NVL (V_COD_SIS_SAL, DETOPE.COD_SIS_SAL)
                               AND DETOPE.COD_DVI =
                                     PKG_PAB_CONSTANTES.V_STR_DVI_CLP
                               AND (TO_DATE (DETOPE.FEC_ISR_OPE,
                                             'DD_MM_YYYY') =
                                       TO_DATE (SYSDATE, 'DD_MM_YYYY')
                                    OR TO_DATE (DETOPE.FEC_VTA, 'DD_MM_YYYY') =
                                         TO_DATE (SYSDATE, 'DD_MM_YYYY'))
                               AND DETOPE.COD_EST_AOS IN
                                        (PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAG, --PAGADA(12)
                                         PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAGCON, --PAGADA CONTINGENCIA(21)
                                         PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEDEV, --DEVUELTA(18)
                                         PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPDV) --POR DEVOLVER(23)
                      --AND DETOPE.COD_SIS_SAL != 'TECNO'
                      UNION                                              --900
                      SELECT   COUNT (CAJA.NUM_FOL_OPE) CANTIDAD
                        FROM   PABS_DT_DETLL_CARGO_ABONO_CAJA CAJA
                       WHERE   CAJA.COD_MT_SWF =
                                  PKG_PAB_CONSTANTES.V_COD_MT900
                               AND NVL (CAJA.COD_SIS_ENT, '#') =
                                     NVL (V_COD_SIS_ENT,
                                          NVL (CAJA.COD_SIS_ENT, '#'))
                               AND NVL (CAJA.COD_SIS_SAL, '#') =
                                     NVL (V_COD_SIS_SAL,
                                          NVL (CAJA.COD_SIS_SAL, '#'))
                               AND CAJA.COD_DVI =
                                     PKG_PAB_CONSTANTES.V_STR_DVI_CLP
                               AND (TO_CHAR (CAJA.FEC_ING_OPE, 'DD_MM_YYYY') =
                                       TO_CHAR (SYSDATE, 'DD_MM_YYYY')
                                    OR TO_CHAR (CAJA.FEC_VTA, 'DD_MM_YYYY') =
                                         TO_CHAR (SYSDATE, 'DD_MM_YYYY')));
         END IF;
      ELSE                                                 --MONEDA EXTRANJERA
         SELECT   SUM (CANTIDAD)
           INTO   V_CANT_PAG
           FROM   (SELECT   COUNT (DETOPE.COD_EST_AOS) CANTIDAD
                     FROM   PABS_DT_DETLL_OPRCN DETOPE
                    WHERE   DETOPE.COD_SIS_ENT =
                               NVL (V_COD_SIS_ENT, DETOPE.COD_SIS_ENT)
                            AND DETOPE.COD_SIS_SAL =
                                  NVL (V_COD_SIS_SAL, DETOPE.COD_SIS_SAL)
                            AND DETOPE.COD_DVI !=
                                  PKG_PAB_CONSTANTES.V_STR_DVI_CLP
                            AND (TO_DATE (DETOPE.FEC_ISR_OPE, 'DD_MM_YYYY') =
                                    TO_DATE (SYSDATE, 'DD_MM_YYYY')
                                 OR TO_DATE (DETOPE.FEC_VTA, 'DD_MM_YYYY') =
                                      TO_DATE (SYSDATE, 'DD_MM_YYYY'))
                            AND DETOPE.COD_EST_AOS IN
                                     (PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAG, --PAGADA(12)
                                      PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPELIB) --LIBERADA(10)
                   --AND DETOPE.COD_SIS_ENT != 'TECNO'
                   UNION                                                 --900
                   SELECT   COUNT (CAJA.NUM_FOL_OPE) CANTIDAD
                     FROM   PABS_DT_DETLL_CARGO_ABONO_CAJA CAJA
                    WHERE   CAJA.COD_MT_SWF = PKG_PAB_CONSTANTES.V_COD_MT900
                            AND NVL (CAJA.COD_SIS_ENT, '#') =
                                  NVL (V_COD_SIS_ENT,
                                       NVL (CAJA.COD_SIS_ENT, '#'))
                            AND NVL (CAJA.COD_SIS_SAL, '#') =
                                  NVL (V_COD_SIS_SAL,
                                       NVL (CAJA.COD_SIS_SAL, '#'))
                            AND CAJA.COD_DVI !=
                                  PKG_PAB_CONSTANTES.V_STR_DVI_CLP
                            AND (TO_CHAR (CAJA.FEC_ING_OPE, 'DD_MM_YYYY') =
                                    TO_CHAR (SYSDATE, 'DD_MM_YYYY')
                                 OR TO_CHAR (CAJA.FEC_VTA, 'DD_MM_YYYY') =
                                      TO_CHAR (SYSDATE, 'DD_MM_YYYY')));
      END IF;

      RETURN V_CANT_PAG;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         ERR_CODE := SQLCODE;
         ERR_MSG := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.SP_PAB_INS_LOG_BD (ERR_CODE,
                                             V_NOM_PCK,
                                             ERR_MSG,
                                             V_NOM_SP);
      WHEN OTHERS
      THEN
         ERR_CODE := SQLCODE;
         ERR_MSG := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.SP_PAB_INS_LOG_BD (ERR_CODE,
                                             V_NOM_PCK,
                                             ERR_MSG,
                                             V_NOM_SP);
         P_S_MENSAJE := ERR_CODE || '-' || ERR_MSG;
         RAISE_APPLICATION_ERROR (-20000, P_S_MENSAJE);
   END FN_PAB_BUS_CAN_OPE_PAG;

   --*****************************************************************************************************
   -- FUNCION/PROCEDIMIENTO: FN_PAB_BUS_CAN_OPE_TOT
   -- OBJETIVO: FN RETORNA CANTIDAD TOTAL DE OPERACIONES POR LIBERAR + NO PAGADAS + PAGADAS + ENVIADAS
   -- SISTEMA: PAB
   -- BASE DE DATOS: DBO_PAB
   -- TABLAS USADAS: PABS_DT_DETLL_OPRCN - PABS_DT_DETLL_CARGO_ABONO_CAJA
   -- FECHA:21/07/2017
   -- AUTOR: GCORREA-CAH
   -- INPUT:
   -- P_COD_SIS_ENT_SAL -> C�DIGO CANAL
   -- P_FLG_SIS_ENT_SAL -> FLAG CANAL
   -- P_COD_DVI -> DIVISA
   -- OBSERVACIONES: <FECHA Y DETALLE>
   --*****************************************************************************************************
   FUNCTION FN_PAB_BUS_CAN_OPE_TOT (P_COD_SIS_ENT_SAL   IN CHAR,
                                    P_FLG_SIS_ENT_SAL      NUMBER,
                                    P_COD_DVI              NUMBER)
      RETURN NUMBER
   IS
      V_NOM_SP        VARCHAR2 (30) := 'FN_PAB_BUS_CAN_OPE_TOT';
      V_CANT          NUMBER (10);
      V_COD_SIS_ENT   CHAR (10);
      V_COD_SIS_SAL   CHAR (10);
   BEGIN
      --P_FLG_SIS_ENT_SAL: 0 ENTRADA / 1 SALIDA (PANTALLA MONITOREO)
      IF P_FLG_SIS_ENT_SAL = PKG_PAB_CONSTANTES.V_FLG_SIS_ENT
      THEN
         V_COD_SIS_ENT := P_COD_SIS_ENT_SAL;
         V_COD_SIS_SAL := NULL;
      ELSE
         V_COD_SIS_ENT := NULL;
         V_COD_SIS_SAL := P_COD_SIS_ENT_SAL;
      END IF;

      --MONEDA NACIONAL
      IF P_COD_DVI = PKG_PAB_CONSTANTES.V_FLG_MRD_MN
      THEN
         --TIPO BOLSA (CANAL ENTRADA VAC�O)
         IF P_COD_SIS_ENT_SAL = PKG_PAB_CONSTANTES.V_COD_TIP_BOLSA
         THEN
            SELECT   COUNT (CAJA.NUM_FOL_OPE)
              INTO   V_CANT
              FROM   PABS_DT_DETLL_CARGO_ABONO_CAJA CAJA
             WHERE       CAJA.COD_MT_SWF = PKG_PAB_CONSTANTES.V_COD_MT900
                     AND CAJA.COD_SIS_ENT IS NULL
                     AND CAJA.COD_DVI = PKG_PAB_CONSTANTES.V_STR_DVI_CLP
                     AND (TO_CHAR (CAJA.FEC_ING_OPE, 'DD_MM_YYYY') =
                             TO_CHAR (SYSDATE, 'DD_MM_YYYY')
                          OR TO_CHAR (CAJA.FEC_VTA, 'DD_MM_YYYY') =
                               TO_CHAR (SYSDATE, 'DD_MM_YYYY'));
         ELSE                                                    --OPERACIONES
            SELECT   SUM (CANTIDAD)
              INTO   V_CANT
              FROM   (SELECT   COUNT (DETOPE.COD_EST_AOS) CANTIDAD
                        FROM   PABS_DT_DETLL_OPRCN DETOPE
                       WHERE   DETOPE.COD_SIS_ENT =
                                  NVL (V_COD_SIS_ENT, DETOPE.COD_SIS_ENT)
                               AND DETOPE.COD_SIS_SAL =
                                     NVL (V_COD_SIS_SAL, DETOPE.COD_SIS_SAL)
                               AND DETOPE.COD_DVI =
                                     PKG_PAB_CONSTANTES.V_STR_DVI_CLP
                               AND DETOPE.FLG_EGR_ING =
                                     PKG_PAB_CONSTANTES.V_FLG_MRD_MN
                               AND (TO_DATE (DETOPE.FEC_ISR_OPE,
                                             'DD_MM_YYYY') =
                                       TO_DATE (SYSDATE, 'DD_MM_YYYY')
                                    OR TO_DATE (DETOPE.FEC_VTA, 'DD_MM_YYYY') =
                                         TO_DATE (SYSDATE, 'DD_MM_YYYY'))
                               AND DETOPE.COD_EST_AOS IN
                                        (PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEAUT, --AUTORIZADA(7)
                                         PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAG, --PAGADA(12)
                                         PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAGCON, --PAGADA CONTINGENCIA(21)
                                         PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEDEV, --DEVUELTA(18)
                                         PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPDV, --POR DEVOLVER(23)
                                         PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPELIB, --LIBERADA(10)
                                         PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEENV, --ENVIADA(13)
                                         PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEREC, --RECHAZADA(6)
                                         PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEELI) --ELIMINADA(4)
                      UNION                                              --900
                      SELECT   COUNT (CAJA.NUM_FOL_OPE) CANTIDAD
                        FROM   PABS_DT_DETLL_CARGO_ABONO_CAJA CAJA
                       WHERE   CAJA.COD_MT_SWF = 900
                               AND NVL (CAJA.COD_SIS_ENT, '#') =
                                     NVL (V_COD_SIS_ENT,
                                          NVL (CAJA.COD_SIS_ENT, '#'))
                               AND NVL (CAJA.COD_SIS_SAL, '#') =
                                     NVL (V_COD_SIS_SAL,
                                          NVL (CAJA.COD_SIS_SAL, '#'))
                               AND CAJA.COD_DVI = 'CLP'
                               AND (TO_CHAR (CAJA.FEC_ING_OPE, 'DD_MM_YYYY') =
                                       TO_CHAR (SYSDATE, 'DD_MM_YYYY')
                                    OR TO_CHAR (CAJA.FEC_VTA, 'DD_MM_YYYY') =
                                         TO_CHAR (SYSDATE, 'DD_MM_YYYY')));
         END IF;
      ELSE                                                 --MONEDA EXTRANJERA
         --TIPO BOLSA (CANAL ENTRADA VAC�O)
         IF P_COD_SIS_ENT_SAL = PKG_PAB_CONSTANTES.V_COD_TIP_BOLSA
         THEN
            SELECT   COUNT (CAJA.NUM_FOL_OPE)
              INTO   V_CANT
              FROM   PABS_DT_DETLL_CARGO_ABONO_CAJA CAJA
             WHERE       CAJA.COD_MT_SWF = PKG_PAB_CONSTANTES.V_COD_MT900
                     AND CAJA.COD_SIS_ENT IS NULL
                     AND CAJA.COD_DVI != PKG_PAB_CONSTANTES.V_STR_DVI_CLP
                     AND (TO_CHAR (CAJA.FEC_ING_OPE, 'DD_MM_YYYY') =
                             TO_CHAR (SYSDATE, 'DD_MM_YYYY')
                          OR TO_CHAR (CAJA.FEC_VTA, 'DD_MM_YYYY') =
                               TO_CHAR (SYSDATE, 'DD_MM_YYYY'));
         ELSE                                                    --OPERACIONES
            SELECT   SUM (CANTIDAD)
              INTO   V_CANT
              FROM   (SELECT   COUNT (DETOPE.COD_EST_AOS) CANTIDAD
                        FROM   PABS_DT_DETLL_OPRCN DETOPE
                       WHERE   DETOPE.COD_SIS_ENT =
                                  NVL (V_COD_SIS_ENT, DETOPE.COD_SIS_ENT)
                               AND DETOPE.COD_SIS_SAL =
                                     NVL (V_COD_SIS_SAL, DETOPE.COD_SIS_SAL)
                               AND DETOPE.COD_DVI !=
                                     PKG_PAB_CONSTANTES.V_STR_DVI_CLP
                               AND DETOPE.FLG_EGR_ING =
                                     PKG_PAB_CONSTANTES.V_FLG_MRD_MN
                               AND (TO_DATE (DETOPE.FEC_ISR_OPE,
                                             'DD_MM_YYYY') =
                                       TO_DATE (SYSDATE, 'DD_MM_YYYY')
                                    OR TO_DATE (DETOPE.FEC_VTA, 'DD_MM_YYYY') =
                                         TO_DATE (SYSDATE, 'DD_MM_YYYY'))
                               AND DETOPE.COD_SIS_ENT = P_COD_SIS_ENT_SAL
                               AND DETOPE.COD_EST_AOS IN
                                        (PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEAUT, --AUTORIZADA(7)
                                         PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAG, --PAGADA(12)
                                         PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPELIB, --LIBERADA(10)
                                         PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEELI) --ELIMINADA(4)
                      UNION                                              --900
                      SELECT   COUNT (CAJA.NUM_FOL_OPE) CANTIDAD
                        FROM   PABS_DT_DETLL_CARGO_ABONO_CAJA CAJA
                       WHERE   CAJA.COD_MT_SWF =
                                  PKG_PAB_CONSTANTES.V_COD_MT900
                               AND NVL (CAJA.COD_SIS_ENT, '#') =
                                     NVL (V_COD_SIS_ENT,
                                          NVL (CAJA.COD_SIS_ENT, '#'))
                               AND NVL (CAJA.COD_SIS_SAL, '#') =
                                     NVL (V_COD_SIS_SAL,
                                          NVL (CAJA.COD_SIS_SAL, '#'))
                               AND CAJA.COD_DVI !=
                                     PKG_PAB_CONSTANTES.V_STR_DVI_CLP
                               AND (TO_CHAR (CAJA.FEC_ING_OPE, 'DD_MM_YYYY') =
                                       TO_CHAR (SYSDATE, 'DD_MM_YYYY')
                                    OR TO_CHAR (CAJA.FEC_VTA, 'DD_MM_YYYY') =
                                         TO_CHAR (SYSDATE, 'DD_MM_YYYY')));
         END IF;
      END IF;

      RETURN V_CANT;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         ERR_CODE := SQLCODE;
         ERR_MSG := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.SP_PAB_INS_LOG_BD (ERR_CODE,
                                             V_NOM_PCK,
                                             ERR_MSG,
                                             V_NOM_SP);
      WHEN OTHERS
      THEN
         ERR_CODE := SQLCODE;
         ERR_MSG := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.SP_PAB_INS_LOG_BD (ERR_CODE,
                                             V_NOM_PCK,
                                             ERR_MSG,
                                             V_NOM_SP);
         P_S_MENSAJE := ERR_CODE || '-' || ERR_MSG;
         RAISE_APPLICATION_ERROR (-20000, P_S_MENSAJE);
   END FN_PAB_BUS_CAN_OPE_TOT;

   --***********************************************************************************************
   -- FUNCION/PROCEDIMIENTO: FN_PAB_BUS_CAN_OPE_NPAG
   -- OBJETIVO: RETORNA CANTIDAD DE OPERACIONES NO PAGADAS
   -- SISTEMA: DBO_PAB
   -- BASE DE DATOS: DBO_PAB
   -- TABLAS USADAS: PABS_DT_DETLL_OPRCN
   -- FECHA:21/07/2017
   -- AUTOR: GCORREA
   -- INPUT:
   -- P_COD_SIS_ENT_SAL -> C�DIGO CANAL
   -- P_FLG_SIS_ENT_SAL -> FLAG DE CANAL DE SALIDA O ENTRADA
   -- P_COD_DVI -> DIVISA
   -- OBSERVACIONES: <FECHA Y DETALLE >
   --***********************************************************************************************
   FUNCTION FN_PAB_BUS_CAN_OPE_NPAG (P_COD_SIS_ENT_SAL   IN CHAR,
                                     P_FLG_SIS_ENT_SAL      NUMBER,
                                     P_COD_DVI              NUMBER)
      RETURN NUMBER
   IS
      V_NOM_SP        VARCHAR2 (30) := 'FN_PAB_BUS_CAN_OPE_NPAG';
      V_CANT          NUMBER (10);
      V_COD_SIS_ENT   CHAR (10);
      V_COD_SIS_SAL   CHAR (10);
      P_ESTADO_LIB    NUMBER (2);
   BEGIN
      --P_FLG_SIS_ENT_SAL: 0 ENTRADA / 1 SALIDA (PANTALLA MONITOREO)
      IF P_FLG_SIS_ENT_SAL = PKG_PAB_CONSTANTES.V_FLG_SIS_ENT
      THEN
         V_COD_SIS_ENT := P_COD_SIS_ENT_SAL;
         V_COD_SIS_SAL := NULL;
      --P_ESTADO_LIB  := PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEREC;
      ELSE
         V_COD_SIS_ENT := NULL;
         V_COD_SIS_SAL := P_COD_SIS_ENT_SAL;
      --P_ESTADO_LIB  := PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEAUT;
      END IF;

      --MONEDA NACIONAL
      IF P_COD_DVI = PKG_PAB_CONSTANTES.V_FLG_MRD_MN
      THEN
         SELECT   COUNT (DETOPE.COD_EST_AOS)
           INTO   V_CANT
           FROM   PABS_DT_DETLL_OPRCN DETOPE
          WHERE   DETOPE.COD_SIS_ENT =
                     NVL (V_COD_SIS_ENT, DETOPE.COD_SIS_ENT)
                  AND DETOPE.COD_SIS_SAL =
                        NVL (V_COD_SIS_SAL, DETOPE.COD_SIS_SAL)
                  AND DETOPE.FLG_EGR_ING = PKG_PAB_CONSTANTES.V_FLG_EGR
                  AND DETOPE.COD_DVI = PKG_PAB_CONSTANTES.V_STR_DVI_CLP
                  AND (TO_DATE (DETOPE.FEC_ISR_OPE, 'DD_MM_YYYY') =
                          TO_DATE (SYSDATE, 'DD_MM_YYYY')
                       OR TO_DATE (DETOPE.FEC_VTA, 'DD_MM_YYYY') =
                            TO_DATE (SYSDATE, 'DD_MM_YYYY'))
                  AND DETOPE.COD_EST_AOS IN
                           (PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPELIB, --LIBERADA(10)
                            PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEENV, --ENVIADA(13)
                            PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEREC, --RECHAZADA(6)
                            PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEELI); --ELIMINADA(4)
      --AND DETOPE.COD_SIS_ENT != 'TECNO';
      ELSE                                                 --MONEDA EXTRANJERA
         SELECT   COUNT (DETOPE.COD_EST_AOS)
           INTO   V_CANT
           FROM   PABS_DT_DETLL_OPRCN DETOPE
          WHERE   DETOPE.COD_SIS_ENT =
                     NVL (V_COD_SIS_ENT, DETOPE.COD_SIS_ENT)
                  AND DETOPE.COD_SIS_SAL =
                        NVL (V_COD_SIS_SAL, DETOPE.COD_SIS_SAL)
                  AND DETOPE.FLG_EGR_ING = PKG_PAB_CONSTANTES.V_FLG_EGR
                  AND DETOPE.COD_DVI != PKG_PAB_CONSTANTES.V_STR_DVI_CLP
                  AND (TO_DATE (DETOPE.FEC_ISR_OPE, 'DD_MM_YYYY') =
                          TO_DATE (SYSDATE, 'DD_MM_YYYY')
                       OR TO_DATE (DETOPE.FEC_VTA, 'DD_MM_YYYY') =
                            TO_DATE (SYSDATE, 'DD_MM_YYYY'))
                  AND DETOPE.COD_EST_AOS =
                        PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEELI; --ELIMINADA(4)
      --AND DETOPE.COD_SIS_ENT != 'TECNO';
      END IF;

      RETURN V_CANT;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         ERR_CODE := SQLCODE;
         ERR_MSG := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.SP_PAB_INS_LOG_BD (ERR_CODE,
                                             V_NOM_PCK,
                                             ERR_MSG,
                                             V_NOM_SP);
      WHEN OTHERS
      THEN
         ERR_CODE := SQLCODE;
         ERR_MSG := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.SP_PAB_INS_LOG_BD (ERR_CODE,
                                             V_NOM_PCK,
                                             ERR_MSG,
                                             V_NOM_SP);
         P_S_MENSAJE := ERR_CODE || '-' || ERR_MSG;
         RAISE_APPLICATION_ERROR (-20000, P_S_MENSAJE);
   END FN_PAB_BUS_CAN_OPE_NPAG;

   --***********************************************************************************************
   -- FUNCION/PROCEDIMIENTO: FN_PAB_BUS_CANT_OPE_ABO
   -- OBJETIVO: RETORNA CANTIDA DE OPERACIONES ABONADAS
   -- SISTEMA: DBO_PAB
   -- BASE DE DATOS: DBO_PAB
   -- TABLAS USADAS: PABS_DT_DETLL_OPRCN - PABS_DT_DETLL_CARGO_ABONO_CAJA
   -- FECHA:21/07/2017
   -- AUTOR: GCORREA
   -- INPUT:
   -- P_COD_SIS_ENT_SAL -> C�DIGO CANAL
   -- P_FLG_SIS_ENT_SAL -> FLAG DE CANAL DE SALIDA O ENTRADA
   -- OBSERVACIONES: <FECHA Y DETALLE>
   --***********************************************************************************************
   FUNCTION FN_PAB_BUS_CANT_OPE_ABO (P_COD_SIS_ENT_SAL   IN CHAR,
                                     p_FLAG_ENT_SAL      IN NUMBER)
      RETURN NUMBER
   IS
      V_NOM_SP   VARCHAR2 (30) := 'FN_PAB_BUS_CANT_OPE_ABO';
      V_CANT     NUMBER (10);
   BEGIN
      --INGRESO MONEDA NACIONAL ENTRADA. FLAG 0
      IF p_FLAG_ENT_SAL = PKG_PAB_CONSTANTES.V_FLG_EGR
      THEN
         SELECT   SUM (CANTIDAD)
           INTO   V_CANT
           FROM   (SELECT   COUNT (DETOPE.COD_EST_AOS) CANTIDAD
                     FROM   PABS_DT_DETLL_OPRCN DETOPE
                    WHERE   DETOPE.COD_SIS_ENT = P_COD_SIS_ENT_SAL
                            AND DETOPE.FLG_EGR_ING =
                                  PKG_PAB_CONSTANTES.V_FLG_ING
                            AND (TO_DATE (DETOPE.FEC_ISR_OPE, 'DD_MM_YYYY') =
                                    TO_DATE (SYSDATE, 'DD_MM_YYYY')
                                 OR TO_DATE (DETOPE.FEC_VTA, 'DD_MM_YYYY') =
                                      TO_DATE (SYSDATE, 'DD_MM_YYYY'))
                            AND DETOPE.COD_EST_AOS IN
                                     (PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEABN, --ABONADA(15)
                                      PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEDEV) --DEVUELTA(23)
                   --AND DETOPE.COD_SIS_ENT != PKG_PAB_CONSTANTES.V_COD_TIP_TECNO
                   UNION
                   SELECT   COUNT (CAJA.NUM_FOL_OPE) CANTIDAD
                     FROM   PABS_DT_DETLL_CARGO_ABONO_CAJA CAJA
                    WHERE   CAJA.COD_SIS_ENT = P_COD_SIS_ENT_SAL
                            AND CAJA.COD_DVI =
                                  PKG_PAB_CONSTANTES.V_STR_DVI_CLP
                            AND CAJA.COD_MT_SWF =
                                  PKG_PAB_CONSTANTES.V_COD_MT910
                            AND (TO_CHAR (CAJA.FEC_ING_OPE, 'DD_MM_YYYY') =
                                    TO_CHAR (SYSDATE, 'DD_MM_YYYY')
                                 OR TO_CHAR (CAJA.FEC_VTA, 'DD_MM_YYYY') =
                                      TO_CHAR (SYSDATE, 'DD_MM_YYYY')));
      ELSE                            --INGRESO MONEDA NACIONAL SALIDA. FLAG 1
         IF P_COD_SIS_ENT_SAL != PKG_PAB_CONSTANTES.V_COD_TIP_BOLSA
         THEN
            SELECT   SUM (CANTIDAD)
              INTO   V_CANT
              FROM   (SELECT   COUNT (DETOPE.COD_EST_AOS) CANTIDAD
                        FROM   PABS_DT_DETLL_OPRCN DETOPE
                       WHERE   DETOPE.COD_SIS_SAL = P_COD_SIS_ENT_SAL
                               AND DETOPE.FLG_EGR_ING =
                                     PKG_PAB_CONSTANTES.V_FLG_ING
                               AND (TO_DATE (DETOPE.FEC_ISR_OPE,
                                             'DD_MM_YYYY') =
                                       TO_DATE (SYSDATE, 'DD_MM_YYYY')
                                    OR TO_DATE (DETOPE.FEC_VTA, 'DD_MM_YYYY') =
                                         TO_DATE (SYSDATE, 'DD_MM_YYYY'))
                               AND DETOPE.COD_EST_AOS IN
                                        (PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEABN, --ABONADA(15)
                                         PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEDEV) --DEVUELTA(23)
                      --AND DETOPE.COD_SIS_ENT != PKG_PAB_CONSTANTES.V_COD_TIP_TECNO
                      UNION ALL
                      SELECT   COUNT (CAJA.NUM_FOL_OPE) CANTIDAD
                        FROM   PABS_DT_DETLL_CARGO_ABONO_CAJA CAJA
                       WHERE   CAJA.COD_SIS_SAL = P_COD_SIS_ENT_SAL
                               AND CAJA.COD_DVI =
                                     PKG_PAB_CONSTANTES.V_STR_DVI_CLP
                               AND CAJA.COD_MT_SWF =
                                     PKG_PAB_CONSTANTES.V_COD_MT910
                               AND (TO_CHAR (CAJA.FEC_ING_OPE, 'DD_MM_YYYY') =
                                       TO_CHAR (SYSDATE, 'DD_MM_YYYY')
                                    OR TO_CHAR (CAJA.FEC_VTA, 'DD_MM_YYYY') =
                                         TO_CHAR (SYSDATE, 'DD_MM_YYYY')));
         ELSE
            SELECT   SUM (CANTIDAD)
              INTO   V_CANT
              FROM   (SELECT   COUNT (DETOPE.COD_EST_AOS) CANTIDAD
                        FROM   PABS_DT_DETLL_OPRCN DETOPE
                       WHERE   DETOPE.COD_SIS_SAL IS NULL
                               AND DETOPE.FLG_EGR_ING =
                                     PKG_PAB_CONSTANTES.V_FLG_ING
                               AND (TO_DATE (DETOPE.FEC_ISR_OPE,
                                             'DD_MM_YYYY') =
                                       TO_DATE (SYSDATE, 'DD_MM_YYYY')
                                    OR TO_DATE (DETOPE.FEC_VTA, 'DD_MM_YYYY') =
                                         TO_DATE (SYSDATE, 'DD_MM_YYYY'))
                               AND DETOPE.COD_EST_AOS =
                                     PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEABN
                      --AND DETOPE.COD_SIS_ENT != PKG_PAB_CONSTANTES.V_COD_TIP_TECNO
                      UNION
                      SELECT   COUNT (CAJA.NUM_FOL_OPE) CANTIDAD
                        FROM   PABS_DT_DETLL_CARGO_ABONO_CAJA CAJA
                       WHERE   CAJA.COD_SIS_SAL IS NULL
                               AND CAJA.COD_DVI =
                                     PKG_PAB_CONSTANTES.V_STR_DVI_CLP
                               AND CAJA.COD_MT_SWF =
                                     PKG_PAB_CONSTANTES.V_COD_MT910
                               AND (TO_CHAR (CAJA.FEC_ING_OPE, 'DD_MM_YYYY') =
                                       TO_CHAR (SYSDATE, 'DD_MM_YYYY')
                                    OR TO_CHAR (CAJA.FEC_VTA, 'DD_MM_YYYY') =
                                         TO_CHAR (SYSDATE, 'DD_MM_YYYY')));
         END IF;
      END IF;

      RETURN V_CANT;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         ERR_CODE := SQLCODE;
         ERR_MSG := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.SP_PAB_INS_LOG_BD (ERR_CODE,
                                             V_NOM_PCK,
                                             ERR_MSG,
                                             V_NOM_SP);
      WHEN OTHERS
      THEN
         ERR_CODE := SQLCODE;
         ERR_MSG := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.SP_PAB_INS_LOG_BD (ERR_CODE,
                                             V_NOM_PCK,
                                             ERR_MSG,
                                             V_NOM_SP);
         P_S_MENSAJE := ERR_CODE || '-' || ERR_MSG;
         RAISE_APPLICATION_ERROR (-20000, P_S_MENSAJE);
   END FN_PAB_BUS_CANT_OPE_ABO;

   -- --***********************************************************************************************
   ---- FUNCION/PROCEDIMIENTO: FN_PAB_BUS_CANT_OPE_NAB
   ---- OBJETIVO: RETORNA CANTIDAD DE OPERACIONES POR SER LIBERADAS
   ---- SISTEMA: DBO_PAB
   ---- BASE DE DATOS: DBO_PAB
   ---- TABLAS USADAS: PABS_DT_DETLL_OPRCN
   ---- FECHA:21/07/2017
   ---- AUTOR: GCORREA
   ---- INPUT:
   ---- P_COD_SIS_ENT_SAL -> CANAL
   ---- P_FLG_SIS_ENT_SAL -> INIDCADO DE CANAL DE SALIDA O ENTRADA
   ---- OBSERVACIONES: <FECHA Y DETALLE>
   ----***********************************************************************************************
   FUNCTION FN_PAB_BUS_CANT_OPE_NAB (P_COD_SIS_ENT_SAL   IN CHAR,
                                     p_FLAG_ENT_SAL      IN NUMBER)
      RETURN NUMBER
   IS
      V_NOM_SP   VARCHAR2 (30) := 'FN_PAB_BUS_CANT_OPE_NAB';
      V_CANT     NUMBER (10);
   BEGIN
      --ENTRADA INGRESO MONEDA NACIONAL
      IF P_FLAG_ENT_SAL = PKG_PAB_CONSTANTES.V_FLG_EGR
      THEN
         SELECT   COUNT (DETOPE.COD_EST_AOS)
           INTO   V_CANT
           FROM   PABS_DT_DETLL_OPRCN DETOPE
          WHERE   DETOPE.COD_SIS_ENT = P_COD_SIS_ENT_SAL
                  AND DETOPE.COD_MT_SWF IN
                           (PKG_PAB_CONSTANTES.V_COD_MT103,
                            PKG_PAB_CONSTANTES.V_COD_MT200,
                            PKG_PAB_CONSTANTES.V_COD_MT202,
                            PKG_PAB_CONSTANTES.V_COD_MT205)
                  AND (TO_DATE (DETOPE.FEC_ISR_OPE, 'DD_MM_YYYY') =
                          TO_DATE (SYSDATE, 'DD_MM_YYYY')
                       OR TO_DATE (DETOPE.FEC_VTA, 'DD_MM_YYYY') =
                            TO_DATE (SYSDATE, 'DD_MM_YYYY'))
                  AND DETOPE.COD_EST_AOS IN
                           (PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEING, --INGRESADA(1)
                            PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAB, --POR ABONAR(16)
                            PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAS, --POR ASOCIAR(19)
                            PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPDV); --POR DEVOLVER(23)
      --AND DETOPE.COD_SIS_ENT != 'TECNO' ;
      ELSE                                    --SALIDA INGRESO MONEDA NACIONAL
         IF P_COD_SIS_ENT_SAL != PKG_PAB_CONSTANTES.V_COD_TIP_BOLSA
         THEN
            SELECT   COUNT (DETOPE.COD_EST_AOS)
              INTO   V_CANT
              FROM   PABS_DT_DETLL_OPRCN DETOPE
             WHERE   DETOPE.COD_SIS_SAL = P_COD_SIS_ENT_SAL
                     AND DETOPE.FLG_EGR_ING = PKG_PAB_CONSTANTES.V_FLG_ING
                     AND DETOPE.COD_MT_SWF IN
                              (PKG_PAB_CONSTANTES.V_COD_MT103,
                               PKG_PAB_CONSTANTES.V_COD_MT200,
                               PKG_PAB_CONSTANTES.V_COD_MT202,
                               PKG_PAB_CONSTANTES.V_COD_MT205)
                     AND (TO_DATE (DETOPE.FEC_ISR_OPE, 'DD_MM_YYYY') =
                             TO_DATE (SYSDATE, 'DD_MM_YYYY')
                          OR TO_DATE (DETOPE.FEC_VTA, 'DD_MM_YYYY') =
                               TO_DATE (SYSDATE, 'DD_MM_YYYY'))
                     AND DETOPE.COD_EST_AOS IN
                              (PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEING, --INGRESADA(1)
                               PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAB, --POR ABONAR(16)
                               PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAS, --POR ASOCIAR(19)
                               PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPDV); --POR DEVOLVER(23)
         --AND DETOPE.COD_SIS_ENT != 'TECNO';
         ELSE
            SELECT   COUNT (DETOPE.COD_EST_AOS)
              INTO   V_CANT
              FROM   PABS_DT_DETLL_OPRCN DETOPE
             WHERE   DETOPE.FLG_EGR_ING = PKG_PAB_CONSTANTES.V_FLG_ING
                     AND DETOPE.COD_SIS_SAL IS NULL
                     AND DETOPE.COD_MT_SWF IN
                              (PKG_PAB_CONSTANTES.V_COD_MT103,
                               PKG_PAB_CONSTANTES.V_COD_MT200,
                               PKG_PAB_CONSTANTES.V_COD_MT202,
                               PKG_PAB_CONSTANTES.V_COD_MT205)
                     AND (TO_DATE (DETOPE.FEC_ISR_OPE, 'DD_MM_YYYY') =
                             TO_DATE (SYSDATE, 'DD_MM_YYYY')
                          OR TO_DATE (DETOPE.FEC_VTA, 'DD_MM_YYYY') =
                               TO_DATE (SYSDATE, 'DD_MM_YYYY'))
                     AND DETOPE.COD_EST_AOS <>
                           PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEABN;
         --AND DETOPE.COD_SIS_ENT != 'TECNO';
         END IF;
      END IF;

      RETURN V_CANT;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END FN_PAB_BUS_CANT_OPE_NAB;

   --***********************************************************************************************
   -- FUNCION/PROCEDIMIENTO: FN_PAB_BUS_CAN_OPE_TOT_SAL
   -- OBJETIVO: RETORNA CANTIDAD TOTAL DE OPERACIONES LIBERAR + NO PAGADAS + PAGADAS + ENVIADAS
   -- SISTEMA: DBO_PAB
   -- BASE DE DATOS: DBO_PAB
   -- TABLAS USADAS: PABS_DT_DETLL_OPRCN, PABS_DT_DETLL_CARGO_ABONO_CAJA
   -- FECHA:21/07/2017
   -- AUTOR: GCORREA-CAH
   -- INPUT:
   -- P_COD_SIS_ENT_SAL -> C�DIGO CANAL
   -- P_FLG_SIS_ENT_SAL -> FLAGIDCADO DE CANAL DE SALIDA O ENTRADA
   -- OBSERVACIONES: <FECHA Y DETALLE>
   --***********************************************************************************************
   FUNCTION FN_PAB_BUS_CAN_OPE_TOT_SAL (P_COD_SIS_ENT_SAL   IN CHAR,
                                        P_FLG_SIS_ENT_SAL      NUMBER,
                                        P_COD_DVI              NUMBER)
      RETURN NUMBER
   IS
      V_NOM_SP        VARCHAR2 (30) := 'FN_PAB_BUS_CAN_OPE_TOT_SAL';
      V_CANT          NUMBER (10);
      P_COD_SIS_ENT   CHAR (10);
      P_COD_SIS_SAL   CHAR (10);
      P_ESTADO_LIB    NUMBER (2);
   BEGIN
      IF P_FLG_SIS_ENT_SAL = 0
      THEN
         P_COD_SIS_ENT := P_COD_SIS_ENT_SAL;
         P_COD_SIS_SAL := NULL;
      ELSE
         P_COD_SIS_ENT := NULL;
         P_COD_SIS_SAL := P_COD_SIS_ENT_SAL;
      END IF;

      V_CANT := 0;

      IF P_COD_DVI = '0'
      THEN
         SELECT   SUM (CANTIDAD)
           INTO   V_CANT
           FROM   (SELECT   COUNT (DETOPE.COD_EST_AOS) CANTIDAD
                     FROM   PABS_DT_DETLL_OPRCN DETOPE
                    WHERE   DETOPE.COD_SIS_ENT =
                               NVL (P_COD_SIS_ENT, DETOPE.COD_SIS_ENT)
                            AND DETOPE.COD_SIS_SAL =
                                  NVL (P_COD_SIS_SAL, DETOPE.COD_SIS_SAL)
                            AND DETOPE.COD_DVI =
                                  PKG_PAB_CONSTANTES.V_STR_DVI_CLP
                            AND (TO_DATE (DETOPE.FEC_ISR_OPE, 'DD_MM_YYYY') =
                                    TO_DATE (SYSDATE, 'DD_MM_YYYY')
                                 OR TO_DATE (DETOPE.FEC_VTA, 'DD_MM_YYYY') =
                                      TO_DATE (SYSDATE, 'DD_MM_YYYY'))
                            AND DETOPE.COD_EST_AOS IN
                                     (PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEAUT, --AUTORIZADA(7)
                                      PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAG, --PAGADA(12)
                                      PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAGCON, --PAGADA CONTINGENCIA(21)
                                      PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEDEV, --DEVUELTA(18)
                                      PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPDV, --POR DEVOLVER(23)
                                      PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPELIB, --LIBERADA(10)
                                      PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEENV, --ENVIADA(13)
                                      PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEREC, --RECHAZADA(6)
                                      PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEELI) --ELIMINADA(4)
                   --AND DETOPE.COD_SIS_ENT != 'TECNO'
                   UNION
                   SELECT   COUNT (CAJA.NUM_FOL_OPE) CANTIDAD
                     FROM   PABS_DT_DETLL_CARGO_ABONO_CAJA CAJA
                    WHERE   NVL (CAJA.COD_SIS_ENT, '#') =
                               NVL (P_COD_SIS_ENT,
                                    NVL (CAJA.COD_SIS_ENT, '#'))
                            AND NVL (CAJA.COD_SIS_SAL, '#') =
                                  NVL (P_COD_SIS_SAL,
                                       NVL (CAJA.COD_SIS_SAL, '#'))
                            AND CAJA.COD_DVI = 'CLP'
                            AND CAJA.COD_MT_SWF = 900
                            AND (TO_CHAR (CAJA.FEC_ING_OPE, 'DD_MM_YYYY') =
                                    TO_CHAR (SYSDATE, 'DD_MM_YYYY')
                                 OR TO_CHAR (CAJA.FEC_VTA, 'DD_MM_YYYY') =
                                      TO_CHAR (SYSDATE, 'DD_MM_YYYY')));
      ELSE
         SELECT   SUM (CANTIDAD)
           INTO   V_CANT
           FROM   (SELECT   COUNT (DETOPE.COD_EST_AOS) CANTIDAD
                     FROM   PABS_DT_DETLL_OPRCN DETOPE
                    WHERE   DETOPE.COD_SIS_ENT =
                               NVL (P_COD_SIS_ENT, DETOPE.COD_SIS_ENT)
                            AND DETOPE.COD_SIS_SAL =
                                  NVL (P_COD_SIS_SAL, DETOPE.COD_SIS_SAL)
                            AND DETOPE.COD_DVI !=
                                  PKG_PAB_CONSTANTES.V_STR_DVI_CLP
                            AND (TO_DATE (DETOPE.FEC_ISR_OPE, 'DD_MM_YYYY') =
                                    TO_DATE (SYSDATE, 'DD_MM_YYYY')
                                 OR TO_DATE (DETOPE.FEC_VTA, 'DD_MM_YYYY') =
                                      TO_DATE (SYSDATE, 'DD_MM_YYYY'))
                            AND DETOPE.COD_EST_AOS IN
                                     (PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEAUT, --AUTORIZADA(7)
                                      PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAG, --PAGADA(12)
                                      PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPELIB, --LIBERADA(10)
                                      PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEELI) --ELIMINADA(4)
                   --AND DETOPE.COD_SIS_ENT != 'TECNO'
                   UNION
                   SELECT   COUNT (CAJA.NUM_FOL_OPE) CANTIDAD
                     FROM   PABS_DT_DETLL_CARGO_ABONO_CAJA CAJA
                    WHERE   NVL (CAJA.COD_SIS_ENT, '#') =
                               NVL (P_COD_SIS_ENT,
                                    NVL (CAJA.COD_SIS_ENT, '#'))
                            AND NVL (CAJA.COD_SIS_SAL, '#') =
                                  NVL (P_COD_SIS_SAL,
                                       NVL (CAJA.COD_SIS_SAL, '#'))
                            AND CAJA.COD_DVI != 'CLP'
                            AND CAJA.COD_MT_SWF = 900
                            AND (TO_CHAR (CAJA.FEC_ING_OPE, 'DD_MM_YYYY') =
                                    TO_CHAR (SYSDATE, 'DD_MM_YYYY')
                                 OR TO_CHAR (CAJA.FEC_VTA, 'DD_MM_YYYY') =
                                      TO_CHAR (SYSDATE, 'DD_MM_YYYY')));
      END IF;

      RETURN V_CANT;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END FN_PAB_BUS_CAN_OPE_TOT_SAL;

   -- ***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_EST_OPE_DSH_EGR
   -- Objetivo: Procedimiento almacenado que obtine los estados de las operaciones para el dashboard segun el rol
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_DETLL_NOMNA
   -- Fecha: 21/06/16
   -- Autor: gcorrea
   -- Input:
   -- p_COD_TROL -> Tipo de rol
   -- p_COD_SIS_ENT -> Codigo de sistema entrada
   -- Output:
   -- p_CURSOR-> Cursor de salida
   -- p_ERROR-> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de Ultimos cambios>
   -- ***********************************************************************************************
   PROCEDURE Sp_PAB_EST_OPE_DSH_EGR (p_COD_TROL      IN     NUMBER,
                                     p_COD_SIS_ENT   IN     CHAR,
                                     p_CURSOR           OUT SYS_REFCURSOR,
                                     p_ERROR            OUT NUMBER)
   IS
      v_NOM_SP        VARCHAR2 (30) := 'Sp_PAB_EST_OPE_DSH_EGR';
      v_COD_SIS_ENT   CHAR (10);

      --Glosa de Estados
      v_EST_AUTOR     VARCHAR2 (30) := 'Autorizada     ';
      v_EST_PRLIB     VARCHAR2 (30) := 'Por liberar';
      v_EST_GRNTA     VARCHAR2 (30) := 'Ingreso Garantia';
      v_EST_INGTA     VARCHAR2 (30) := 'Ing. Garantia';
      v_EST_PAUTO     VARCHAR2 (30) := 'Por Autorizar';
      v_EST_CONTG     VARCHAR2 (30) := 'Pagada Contingencia';
      v_EST_PCNTG     VARCHAR2 (30) := 'Pagada Cont.';
      v_EST_RECHA     VARCHAR2 (30) := 'Rechazada      ';
      v_EST_RESWF     VARCHAR2 (30) := 'Rechazo SWF';
   BEGIN
      p_ERROR := PKG_PAB_CONSTANTES.v_OK;

      --Variable a futuro
      v_COD_SIS_ENT := p_COD_SIS_ENT;

      CASE
         --Tecnologia / OOFF
         WHEN (v_COD_SIS_ENT = PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SE_TECNO AND p_COD_TROL = PKG_PAB_CONSTANTES.V_COD_TIP_ROL_ADM) OR v_COD_SIS_ENT = PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SE_OOFF
         THEN
            OPEN p_CURSOR FOR
                 SELECT   DECODE (DSC_EST_AOS,
                                  v_EST_AUTOR, v_EST_PRLIB,
                                  v_EST_GRNTA, v_EST_INGTA,
                                  v_EST_CONTG, v_EST_PCNTG,
                                  v_EST_RECHA, v_EST_RESWF,
                                  DSC_EST_AOS)
                             AS DSC_EST_AOS,
                          COD_EST_AOS AS COD_EST_AOS,
                          DECODE (COD_EST_AOS,
                                  PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPVI,
                                  v_EST_EGR_OPE_PVI,
                                  PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEINGAR,
                                  v_EST_EGR_OPEINGAR,
                                  PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPDEB,
                                  v_EST_EGR_OPEPDEB,
                                  PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAT,
                                  v_EST_EGR_OPEPAT,
                                  PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEAUT,
                                  v_EST_EGR_OPEAUT,
                                  PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPELIB,
                                  v_EST_EGR_OPELIB,
                                  PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEENV,
                                  v_EST_EGR_OPEENV,
                                  PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAG,
                                  v_EST_EGR_OPEPAG,
                                  PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAGCON,
                                  v_EST_EGR_OPEPAGCFI,
                                  PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEREC,
                                  v_EST_EGR_OPEREC,
                                  PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPDV,
                                  v_EST_EGR_OPEPDV,
                                  PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEDEV,
                                  v_EST_EGR_OPEDEV,
                                  PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEELI,
                                  v_EST_EGR_OPEELI)
                   FROM   PABS_DT_ESTDO_ALMOT
                  WHERE   COD_EST_AOS IN
                                (PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPVI, --Por Visar
                                 PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEINGAR, --Ingreso Garantia
                                 PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPDEB, --Por Debitar
                                 PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAT, --Por Autorizar
                                 PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEAUT, --Autorizada
                                 PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPELIB, --Liberada
                                 PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEENV, --Enviada
                                 PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAG, --Pagada
                                 PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAGCFI, --Contingencia
                                 PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEREC, --Rechazada
                                 PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPDV, --Por Devolver
                                 PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEDEV, --Devuelta
                                 PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEELI --Eliminada
                                                                        )
                          AND FLG_VGN = PKG_PAB_CONSTANTES.V_REG_VIG
               ORDER BY   3 ASC;
         --Mesa
         WHEN v_COD_SIS_ENT = PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SE_MESA
         THEN
            OPEN p_CURSOR FOR
               SELECT   DSC_EST_AOS AS DSC_EST_AOS,
                        COD_EST_AOS AS COD_EST_AOS
                 FROM   PABS_DT_ESTDO_ALMOT
                WHERE   COD_EST_AOS IN
                              (PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPVI --Por Visar
                                                                      )
                        AND FLG_VGN = PKG_PAB_CONSTANTES.V_REG_VIG;
         --Todas las otras areas
         ELSE
            --Supervisor
            IF (p_COD_TROL = PKG_PAB_CONSTANTES.V_COD_TIP_ROL_SUP)
            THEN
               OPEN p_CURSOR FOR
                    SELECT   DECODE (DSC_EST_AOS,
                                     v_EST_AUTOR, v_EST_PRLIB,
                                     v_EST_GRNTA, v_EST_INGTA,
                                     v_EST_CONTG, v_EST_PCNTG,
                                     v_EST_RECHA, v_EST_RESWF,
                                     DSC_EST_AOS)
                                AS DSC_EST_AOS,
                             COD_EST_AOS AS COD_EST_AOS,
                             DECODE (
                                COD_EST_AOS,
                                PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPVI,
                                v_EST_EGR_OPE_PVI,
                                PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEAUT,
                                v_EST_EGR_OPEAUT,
                                PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPELIB,
                                v_EST_EGR_OPELIB,
                                PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEENV,
                                v_EST_EGR_OPEENV,
                                PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAG,
                                v_EST_EGR_OPEPAG,
                                PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAGCON,
                                v_EST_EGR_OPEPAGCFI,
                                PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEREC,
                                v_EST_EGR_OPEREC,
                                PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPDV,
                                v_EST_EGR_OPEPDV,
                                PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEDEV,
                                v_EST_EGR_OPEDEV
                             )
                      FROM   PABS_DT_ESTDO_ALMOT
                     WHERE   COD_EST_AOS IN
                                   (PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPVI, --Por Visar
                                    PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEAUT, --Autorizada
                                    PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPELIB, --Liberada
                                    PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEENV, --Enviada
                                    PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAG, --Pagada
                                    PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAGCFI, --Contingencia
                                    PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEREC, --Rechazada
                                    PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPDV, --Por Devolver
                                    PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEDEV --Devuelta
                                                                           )
                             AND FLG_VGN = PKG_PAB_CONSTANTES.V_REG_VIG
                  ORDER BY   3 ASC;
            --Analista
            ELSIF (p_COD_TROL = PKG_PAB_CONSTANTES.V_COD_TIP_ROL_ANA)
            THEN
               OPEN p_CURSOR FOR
                    SELECT   DECODE (DSC_EST_AOS,
                                     v_EST_AUTOR, v_EST_PRLIB,
                                     v_EST_GRNTA, v_EST_INGTA,
                                     v_EST_CONTG, v_EST_PCNTG,
                                     v_EST_RECHA, v_EST_RESWF,
                                     DSC_EST_AOS)
                                AS DSC_EST_AOS,
                             COD_EST_AOS AS COD_EST_AOS,
                             DECODE (
                                COD_EST_AOS,
                                PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPVI,
                                v_EST_EGR_OPE_PVI,
                                PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEAUT,
                                v_EST_EGR_OPEAUT,
                                PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPELIB,
                                v_EST_EGR_OPELIB,
                                PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEENV,
                                v_EST_EGR_OPEENV,
                                PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAG,
                                v_EST_EGR_OPEPAG,
                                PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAGCON,
                                v_EST_EGR_OPEPAGCFI,
                                PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEREC,
                                v_EST_EGR_OPEREC,
                                PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPDV,
                                v_EST_EGR_OPEPDV,
                                PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEDEV,
                                v_EST_EGR_OPEDEV
                             )
                      FROM   PABS_DT_ESTDO_ALMOT
                     WHERE   COD_EST_AOS IN
                                   (PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPVI, --Por Visar
                                    PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEAUT, --Autorizada
                                    PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPELIB, --Liberada
                                    PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEENV, --Enviada
                                    PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAG, --Pagada
                                    PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAGCFI, --Contingencia
                                    PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEREC, --Rechazada
                                    PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPDV, --Por Devolver
                                    PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEDEV --Devuelta
                                                                           )
                             AND FLG_VGN = PKG_PAB_CONSTANTES.V_REG_VIG
                  ORDER BY   3 ASC;
            END IF;
      END CASE;
   --        CASE
   --           WHEN p_COD_SIS_ENT  = PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SE_OOFF  THEN-- Operaciones Financieras
   --
   --                IF p_COD_TROL = 1  THEN --Supervisor
   --                     OPEN p_CURSOR FOR
   --                       SELECT COD_EST_AOS AS COD_EST_AOS,
   --                       DECODE(DSC_EST_AOS ,v_EST_AUTOR, v_EST_PRLIB, v_EST_GRNTA,v_EST_INGTA,v_EST_CONTG, v_EST_PCNTG,v_EST_RECHA, v_EST_RESWF, DSC_EST_AOS) AS DSC_EST_AOS,
   --                       DECODE(COD_EST_AOS,PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEREC, v_EST_REC,
   --                                          PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEINGAR, v_EST_GAR,
   --                                          PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEAUT, v_EST_AUT,
   --                                          PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPVI, v_EST_PVI,
   --                                          PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPDEB, v_EST_DEB,
   --                                          PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAT, v_EST_PAT,
   --                                          PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPELIB, v_EST_LIB,
   --                                          PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAG, v_EST_PAG,
   --                                          PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAGCON, v_EST_PAGCON,
   --                                          PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEENV, v_EST_ENV,
   --                                          PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEDEV, v_EST_DEV)
   --                       FROM PABS_DT_ESTDO_ALMOT
   --                       WHERE COD_EST_AOS IN (PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEREC     --6     * RECHAZADA
   --                                            ,PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEINGAR  --20    * Garantias
   --                                            ,PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEAUT    --7     * AUTORIZADA / POR LIBERAR
   --                                            ,PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAT
   --                                            ,PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPDEB
   --                                            ,PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPVI    --8     * Por Visar
   --                                            ,PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPELIB    --10    * LIBERADA --
   --                                            ,PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAG    --12    * PAGADA   --
   --                                            ,PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAGCON --21    * Pagada x Contingencia
   --                                            ,PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEENV    --13    * ENVIADA --
   --                                            --,PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPDV    --23    * Por Devolver
   --                                            ,PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEDEV)    --18    * DEVUELTA
   --                            AND FLG_VGN = PKG_PAB_CONSTANTES.V_REG_VIG
   --                            ORDER BY 3 ASC;

   --
   --                ELSIF p_COD_TROL = 2 THEN -- Analista
   --                    OPEN p_CURSOR FOR
   --                         SELECT COD_EST_AOS AS COD_EST_AOS,
   --                         DECODE(DSC_EST_AOS ,v_EST_AUTOR, v_EST_PRLIB, v_EST_GRNTA,v_EST_INGTA,v_EST_CONTG, v_EST_PCNTG,v_EST_RECHA, v_EST_RESWF, DSC_EST_AOS) AS DSC_EST_AOS,
   --                         DECODE(COD_EST_AOS,PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEREC, v_EST_REC,
   --                                          PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEINGAR, v_EST_GAR,
   --                                          PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEAUT, v_EST_AUT,
   --                                          PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAT, v_EST_PAT,
   --                                          PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPDEB, v_EST_DEB,
   --                                          PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPVI, v_EST_PVI,
   --                                          PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPELIB, v_EST_LIB,
   --                                          PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAG, v_EST_PAG,
   --                                          PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAGCON, v_EST_PAGCON,
   --                                          PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEENV, v_EST_ENV,
   --                                          PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEDEV, v_EST_DEV)
   --                         FROM PABS_DT_ESTDO_ALMOT
   --                         WHERE COD_EST_AOS IN (PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEREC    --6     * RECHAZADA
   --                                              ,PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEINGAR --20   *Garantias
   --                                              ,PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEAUT    --7     * AUTORIZADA / POR LIBERAR
   --                                              ,PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAT
   --                                              ,PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPDEB
   --                                              ,PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPVI    --8
   --                                              ,PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPELIB    --10    * LIBERADA --
   --                                              ,PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAG    --12    * PAGADA    --
   --                                              ,PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAGCON --21    * Pagada x Contingencia
   --                                              ,PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEENV    --13    * ENVIADA --
   --                                              ,PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEDEV)    --18    * DEVUELTA
   --                                AND FLG_VGN = PKG_PAB_CONSTANTES.V_REG_VIG
   --                                ORDER BY 3 ASC;
   --
   --                ELSE
   --                    OPEN p_CURSOR FOR
   --                            SELECT DECODE(DSC_EST_AOS ,v_EST_AUTOR, v_EST_PRLIB, v_EST_GRNTA,v_EST_INGTA,v_EST_CONTG, v_EST_PCNTG,v_EST_RECHA, v_EST_RESWF, DSC_EST_AOS) AS DSC_EST_AOS ,
   --                                COD_EST_AOS AS COD_EST_AOS,
   --                                DECODE(COD_EST_AOS,PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEREC, v_EST_REC,
   --                                          PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEINGAR, v_EST_GAR,
   --                                          PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEAUT, v_EST_AUT,
   --                                          PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAT, v_EST_PAT,
   --                                          PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPDEB, v_EST_DEB,
   --                                          PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPVI, v_EST_PVI,
   --                                          PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPELIB, v_EST_LIB,
   --                                          PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAG, v_EST_PAG,
   --                                          PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAGCON, v_EST_PAGCON,
   --                                          PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEENV, v_EST_ENV,
   --                                          PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEDEV, v_EST_DEV)
   --                                  FROM PABS_DT_ESTDO_ALMOT
   --                                WHERE COD_EST_AOS IN (   PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPVI    --8
   --                                                        ,PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPELIB    --10
   --                                                        ,PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPDEB
   --                                                        ,PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAG    --12
   --                                                        ,PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEENV    --13
   --                                                        ,PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEINGAR --20   *Garantias
   --                                                        ,PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEDEV    --18
   --                                                        ,PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAS    --19 Por Asociar
   --                                                        ,PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPDV    --23 Por Devolver
   --                                                        ,PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPDEB   --11 Pendiente por debitar
   --                                                        ,PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAT) -- Por autorizar
   --                                AND FLG_VGN = PKG_PAB_CONSTANTES.V_REG_VIG
   --                                ORDER BY 3 ASC;
   --
   --                END IF;
   --
   --           WHEN p_COD_SIS_ENT  = 'TECNO' THEN-- TODO
   --
   --                    OPEN p_CURSOR FOR
   --                         SELECT COD_EST_AOS AS COD_EST_AOS,
   --                         DECODE(DSC_EST_AOS ,v_EST_AUTOR, v_EST_PRLIB, v_EST_GRNTA,v_EST_INGTA,v_EST_CONTG, v_EST_PCNTG,v_EST_RECHA, v_EST_RESWF, DSC_EST_AOS) AS DSC_EST_AOS,
   --                         DECODE(COD_EST_AOS,PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEREC, v_EST_REC,
   --                                          PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEINGAR, v_EST_GAR,
   --                                          PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEAUT, v_EST_AUT,
   --                                          PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAT, v_EST_PAT,
   --                                          PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPDEB, v_EST_DEB,
   --                                          PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPVI, v_EST_PVI,
   --                                          PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPELIB, v_EST_LIB,
   --                                          PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAG, v_EST_PAG,
   --                                          PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAGCON, v_EST_PAGCON,
   --                                          PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEENV, v_EST_ENV,
   --                                          PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEDEV, v_EST_DEV)
   --                         FROM PABS_DT_ESTDO_ALMOT
   --                         WHERE COD_EST_AOS IN (PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEREC    --6     * RECHAZADA
   --                                              ,PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEINGAR --20   *Garantias
   --                                              ,PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEAUT    --7     * AUTORIZADA / POR LIBERAR
   --                                              ,PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAT
   --                                              ,PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPDEB
   --                                              ,PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPVI    --8
   --                                              ,PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPELIB    --10    * LIBERADA --
   --                                              ,PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAG    --12    * PAGADA    --
   --                                              ,PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAGCON --21    * Pagada x Contingencia
   --                                              ,PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEENV    --13    * ENVIADA --
   --                                              ,PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEDEV)    --18    * DEVUELTA
   --                                AND FLG_VGN = PKG_PAB_CONSTANTES.V_REG_VIG
   --                                ORDER BY 3 ASC;
   --

   --             WHEN p_COD_SIS_ENT  = 'MESA' -- Mesa solo visar
   --             THEN
   --                OPEN p_CURSOR FOR
   --                    SELECT COD_EST_AOS AS COD_EST_AOS,
   --                    DECODE(DSC_EST_AOS , v_EST_AUTOR, v_EST_PRLIB, v_EST_GRNTA ,v_EST_PAUTO, DSC_EST_AOS) AS DSC_EST_AOS
   --                    FROM PABS_DT_ESTDO_ALMOT
   --                    WHERE COD_EST_AOS = PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPVI
   --                    AND FLG_VGN = PKG_PAB_CONSTANTES.V_REG_VIG;
   --
   --
   --             ELSE -- OTRAS �REAS
   --
   --                IF p_COD_TROL = 1  THEN --1 Supervisor
   --                             OPEN p_CURSOR FOR
   --                                 SELECT COD_EST_AOS AS COD_EST_AOS,
   --                                 DECODE(DSC_EST_AOS ,v_EST_AUTOR, v_EST_PRLIB, v_EST_GRNTA,v_EST_INGTA,v_EST_CONTG, v_EST_PCNTG,v_EST_RECHA, v_EST_RESWF, DSC_EST_AOS) AS DSC_EST_AOS,
   --                                 DECODE(COD_EST_AOS,PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEREC, v_EST_REC,
   --                                          PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEAUT, v_EST_AUT,
   --                                          PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPVI, v_EST_PVI,
   --                                          PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPELIB, v_EST_LIB,
   --                                          PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAG, v_EST_PAG,
   --                                          PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAGCON, v_EST_PAGCON,
   --                                          PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEENV, v_EST_ENV)
   --                                 FROM PABS_DT_ESTDO_ALMOT
   --                                 WHERE COD_EST_AOS IN (PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEREC    --6     Rechazada
   --                                                      ,PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEAUT    --7     Autorizada
   --                                                      ,PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPVI    --8     Por visar
   --                                                      ,PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPELIB    --10    Liberada
   --                                                      ,PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAG    --12    Pagada
   --                                                      ,PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAGCON --21    * Pagada x Contingencia
   --                                                      ,PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEENV)    --13    Enviada
   --                                    AND FLG_VGN = PKG_PAB_CONSTANTES.V_REG_VIG
   --                                    ORDER BY 3 ASC;
   --                ELSE -- 2 Analista
   --                            OPEN p_CURSOR FOR
   --                                    SELECT COD_EST_AOS AS COD_EST_AOS,
   --                                    DECODE(DSC_EST_AOS ,v_EST_AUTOR, v_EST_PRLIB, v_EST_GRNTA,v_EST_INGTA,v_EST_CONTG, v_EST_PCNTG,v_EST_RECHA, v_EST_RESWF, DSC_EST_AOS) AS DSC_EST_AOS,
   --                                    DECODE(COD_EST_AOS,PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEREC, v_EST_REC,
   --                                                      PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEAUT, v_EST_AUT,
   --                                                      PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPVI, v_EST_PVI,
   --                                                      PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPELIB, v_EST_LIB,
   --                                                      PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAG, v_EST_PAG,
   --                                                      PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAGCON, v_EST_PAGCON,
   --                                                      PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEENV, v_EST_ENV,
   --                                                      PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEDEV, v_EST_DEV)
   --                                    FROM PABS_DT_ESTDO_ALMOT
   --                                    WHERE COD_EST_AOS IN (PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEREC    --6     * RECHAZADA
   --                                                         ,PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPVI    --8     Por visar
   --                                                         ,PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEAUT    --7     * AUTORIZADA / POR LIBERAR
   --                                                         ,PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPELIB    --10    * LIBERADA --
   --                                                         ,PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAG    --12    * PAGADA    --
   --                                                         ,PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAGCON --21    * Pagada x Contingencia
   --                                                         ,PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEENV    --13    * ENVIADA --
   --                                                         ,PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEDEV)   --18    * DEVUELTA
   --                                          AND FLG_VGN = PKG_PAB_CONSTANTES.V_REG_VIG
   --                                          ORDER BY 3 ASC;
   --                END IF;
   --
   --
   --        END CASE;

   --Procedimiento ejecutado de forma correcta
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END Sp_PAB_EST_OPE_DSH_EGR;

   -- ***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_EST_OPE_DSH_ING
   -- Objetivo: Procedimiento almacenado que obtine los estados de las operaciones para el dashboard segun el rol
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_DETLL_NOMNA
   -- Fecha: 21/06/16
   -- Autor: gcorrea
   -- Input:
   -- p_COD_TROL -> Tipo de rol
   -- p_COD_SIS_ENT -> Codigo de sistema entrada
   -- Output:
   -- p_CURSOR-> Cursor de salida
   -- p_ERROR-> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de Ultimos cambios>
   -- ***********************************************************************************************
   PROCEDURE Sp_PAB_EST_OPE_DSH_ING (p_COD_TROL      IN     NUMBER,
                                     p_COD_SIS_ENT   IN     CHAR,
                                     p_CURSOR           OUT SYS_REFCURSOR,
                                     p_ERROR            OUT NUMBER)
   IS
      v_NOM_SP        VARCHAR2 (30) := 'Sp_PAB_EST_OPE_DSH_ING';
      v_COD_SIS_ENT   CHAR (10);
   BEGIN
      p_ERROR := PKG_PAB_CONSTANTES.v_OK;

      --Variable a futuro
      v_COD_SIS_ENT := p_COD_SIS_ENT;

      CASE
         --Tecnologia / OOFF
         WHEN (v_COD_SIS_ENT = PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SE_TECNO AND p_COD_TROL = PKG_PAB_CONSTANTES.V_COD_TIP_ROL_ADM) OR v_COD_SIS_ENT = PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SE_OOFF
         THEN
            OPEN p_CURSOR FOR
                 SELECT   COD_EST_AOS AS COD_EST_AOS,
                          DECODE (DSC_EST_AOS,
                                  'AUTORIZADA', 'POR LIBERAR',
                                  DSC_EST_AOS)
                             AS DSC_EST_AOS,
                          DECODE (COD_EST_AOS,
                                  PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEREG,
                                  V_EST_REG,
                                  PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAS,
                                  v_EST_PAS,
                                  PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAB,
                                  v_EST_PAB,
                                  PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEABN,
                                  v_EST_ABN,
                                  PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPDV,
                                  v_EST_PDV,
                                  PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEDEV,
                                  v_EST_DEV)
                   FROM   PABS_DT_ESTDO_ALMOT
                  WHERE   COD_EST_AOS IN
                                (PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEREG, --Recibida
                                 PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAS, --Por Asociar
                                 PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAB, --Por Abonar
                                 PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEABN, --Abonada
                                 PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPDV, --Por Devolver
                                 PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEDEV --Devuelta
                                                                        )
                          AND FLG_VGN = PKG_PAB_CONSTANTES.V_REG_VIG
               ORDER BY   3 ASC;
         --Mesa
         WHEN v_COD_SIS_ENT = PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SE_MESA
         THEN
            NULL;                            --La mesa no ve estado de nominas
         --Todas las otras areas
         ELSE
            --Supervisor
            IF (p_COD_TROL = PKG_PAB_CONSTANTES.V_COD_TIP_ROL_SUP)
            THEN
               OPEN p_CURSOR FOR
                    SELECT   COD_EST_AOS AS COD_EST_AOS,
                             DSC_EST_AOS AS DSC_EST_AOS,
                             DECODE (COD_EST_AOS,
                                     PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEABN,
                                     v_EST_ABN,
                                     PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPDV,
                                     v_EST_PDV,
                                     PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEDEV,
                                     v_EST_DEV)
                      FROM   PABS_DT_ESTDO_ALMOT
                     WHERE   COD_EST_AOS IN
                                   (PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEABN, --Abonada
                                    PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPDV, --Por Devolver
                                    PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEDEV --Devuelta
                                                                           )
                             AND FLG_VGN = PKG_PAB_CONSTANTES.V_REG_VIG
                  ORDER BY   3 ASC;
            --Analista
            ELSIF (p_COD_TROL = PKG_PAB_CONSTANTES.V_COD_TIP_ROL_ANA)
            THEN
               OPEN p_CURSOR FOR
                    SELECT   COD_EST_AOS AS COD_EST_AOS,
                             DSC_EST_AOS AS DSC_EST_AOS,
                             DECODE (COD_EST_AOS,
                                     PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEABN,
                                     v_EST_ABN,
                                     PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPDV,
                                     v_EST_PDV,
                                     PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEDEV,
                                     v_EST_DEV)
                      FROM   PABS_DT_ESTDO_ALMOT
                     WHERE   COD_EST_AOS IN
                                   (PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEABN, --Abonada
                                    PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPDV, --Por Devolver
                                    PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEDEV --Devuelta
                                                                           )
                             AND FLG_VGN = PKG_PAB_CONSTANTES.V_REG_VIG
                  ORDER BY   3 ASC;
            END IF;
      END CASE;
   --Procedimiento ejecutado de forma correcta
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END Sp_PAB_EST_OPE_DSH_ING;

   -- ****************************************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_EST_NMN_DSH_EGR
   -- Objetivo: Procedimiento almacenado que obtiene los estados para operaciones en respecto a n�minas
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_DETLL_NOMNA
   -- Fecha: 06-11-2017
   -- Autor: gcorrea- CAH
   -- Input:
   -- p_COD_TROL -> Tipo de rol
   -- p_COD_SIS_ENT -> Codigo de sistema entrada
   -- Output:
   -- p_CURSOR-> Cursor de salida
   -- p_ERROR-> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de Ultimos cambios>
   -- ******************************************************************************************************************************
   PROCEDURE Sp_PAB_EST_NMN_DSH_EGR (p_COD_TROL      IN     NUMBER,
                                     p_COD_SIS_ENT   IN     CHAR,
                                     p_CURSOR           OUT SYS_REFCURSOR,
                                     p_ERROR            OUT NUMBER)
   IS
      v_NOM_SP        VARCHAR2 (30) := 'Sp_PAB_EST_NMN_DSH_EGR';
      v_COD_SIS_ENT   CHAR (10);
   BEGIN
      p_ERROR := PKG_PAB_CONSTANTES.v_OK;

      --Variable a futuro
      v_COD_SIS_ENT := p_COD_SIS_ENT;

      CASE
         --Tecnologia
         WHEN v_COD_SIS_ENT = PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SE_TECNO
         THEN
            --Administrador (0 lo esta enviando el Front cuando no es Supervisor o Analista)
            IF (p_COD_TROL = PKG_PAB_CONSTANTES.V_COD_TIP_ROL_ADM)
            THEN
               -- OR p_COD_TROL = 0

               OPEN p_CURSOR FOR
                    SELECT   COD_EST_AOS AS COD_EST_AOS,
                             DECODE (COD_EST_AOS,
                                     PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEVAL,
                                     'POR CURSAR',
                                     EST.DSC_EST_AOS)
                                AS DSC_EST_AOS,
                             DECODE (COD_EST_AOS,
                                     PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPECUR,
                                     v_EST_CUR,
                                     PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEINV,
                                     v_EST_INV,
                                     PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEVAL,
                                     v_EST_VAL,
                                     PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEDUP,
                                     v_EST_DUP,
                                     PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEAUT,
                                     v_EST_AUT,
                                     PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEREC,
                                     v_EST_REC,
                                     PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEELI,
                                     v_EST_ELI)
                      FROM   PABS_DT_ESTDO_ALMOT EST
                     WHERE   COD_EST_AOS IN
                                   (PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEVAL, --Valida
                                    PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEDUP, --Duplicada
                                    PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEINV, --Invalida
                                    PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEREC, --Rechazada
                                    PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEELI, --Eliminada
                                    PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPECUR, --Cursada
                                    PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEAUT --Autorizada
                                                                           )
                             AND FLG_VGN = PKG_PAB_CONSTANTES.V_REG_VIG
                  ORDER BY   3 ASC;
            END IF;
         --Mesa
         WHEN v_COD_SIS_ENT = PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SE_MESA
         THEN
            NULL;                            --La mesa no ve estado de nominas
         --Todas las otras areas
         ELSE
            --Supervisor
            IF (p_COD_TROL = PKG_PAB_CONSTANTES.V_COD_TIP_ROL_SUP)
            THEN
               OPEN p_CURSOR FOR
                    SELECT   COD_EST_AOS AS COD_EST_AOS,
                             DECODE (COD_EST_AOS,
                                     PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPECUR,
                                     'POR AUTORIZAR',
                                     DSC_EST_AOS)
                                AS DSC_EST_AOS,
                             DECODE (COD_EST_AOS,
                                     PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEREC,
                                     v_EST_REC,
                                     PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEAUT,
                                     v_EST_AUT,
                                     PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPECUR,
                                     v_EST_CUR)
                      FROM   PABS_DT_ESTDO_ALMOT
                     WHERE   COD_EST_AOS IN
                                   (PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEREC, --Rechazada
                                    PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEAUT, --Autorizada
                                    PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPECUR --Cursada
                                                                           )
                             AND FLG_VGN = PKG_PAB_CONSTANTES.V_REG_VIG
                  ORDER BY   3;
            --Analista
            ELSIF (p_COD_TROL = PKG_PAB_CONSTANTES.V_COD_TIP_ROL_ANA)
            THEN
               OPEN p_CURSOR FOR
                    SELECT   COD_EST_AOS AS COD_EST_AOS,
                             DECODE (COD_EST_AOS,
                                     PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEVAL,
                                     'POR CURSAR',
                                     EST.DSC_EST_AOS)
                                AS DSC_EST_AOS,
                             DECODE (COD_EST_AOS,
                                     PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPECUR,
                                     v_EST_CUR,
                                     PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEINV,
                                     v_EST_INV,
                                     PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEVAL,
                                     v_EST_VAL,
                                     PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEDUP,
                                     v_EST_DUP,
                                     PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEAUT,
                                     v_EST_AUT,
                                     PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEREC,
                                     v_EST_REC,
                                     PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEELI,
                                     v_EST_ELI)
                      FROM   PABS_DT_ESTDO_ALMOT EST
                     WHERE   COD_EST_AOS IN
                                   (PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEINV, --Invalida
                                    PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEVAL, --Valida
                                    PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEELI, --Eliminada
                                    PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEAUT, --Autorizada
                                    PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPECUR, --Cursada
                                    PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEREC, --Rechazada
                                    PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEDUP --Duplicada
                                                                           )
                             AND FLG_VGN = PKG_PAB_CONSTANTES.V_REG_VIG
                  ORDER BY   3 ASC;
            END IF;
      END CASE;
   --Procedimiento ejecutado de forma correcta
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END Sp_PAB_EST_NMN_DSH_EGR;

   -- ***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_CANT_OPE_EST_EGR
   -- Objetivo: Procedimiento almacenado que cuenta por el tipo de estado cada una de las operaciones
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_DETLL_OPRCN, PABS_DT_ESTDO_ALMOT, PABS_DT_SISTM_ENTRD_SALID
   -- Fecha: 24/10/16
   -- Autor: gcorrea
   -- Input:
   -- P_COD_EST_AOS -> codigo de estado de operacion
   -- P_COD_SIS_ENT -> codigo de sistema entrada
   -- Output:
   -- p_CURSOR -> cursor de resultado
   -- p_ERROR-> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ?ltimos cambios>
   -- ***********************************************************************************************
   PROCEDURE Sp_PAB_CANT_OPE_EST_EGR (P_COD_EST_AOS   IN     NUMBER,
                                      P_COD_SIS_ENT   IN     CHAR,
                                      p_CURSOR           OUT SYS_REFCURSOR,
                                      P_ERROR            OUT NUMBER)
   IS
      v_NOM_SP        VARCHAR2 (30) := 'Sp_PAB_CANT_OPE_EST_EGR';
      v_COD_SIS_ENT   CHAR (10);
   BEGIN
      --Tecnologia y OOFF debe ver el ultimo registro de todas las areas
      IF (P_COD_SIS_ENT NOT IN
                (PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SE_TECNO,
                 PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SE_OOFF))
      THEN
         V_COD_SIS_ENT := P_COD_SIS_ENT;                           --Filtramos
      END IF;

      --Si el estado es por visar la mesa debe ver la de todos
      IF (P_COD_SIS_ENT = PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SE_MESA
          AND P_COD_EST_AOS = PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPVI)
      THEN
         V_COD_SIS_ENT := NULL;
      END IF;

      OPEN p_CURSOR FOR
           SELECT   COUNT (DETOPE.COD_EST_AOS) AS CANT_OPE,
                    DETOPE.COD_EST_AOS AS COD_EST_AOS,
                    EST.DSC_EST_AOS AS DSC_EST,
                    FN_PAB_BUS_ULT_REG_OPE_EST (P_COD_SIS_ENT,
                                                P_COD_EST_AOS,
                                                PKG_PAB_CONSTANTES.V_FLG_EGR)
                       AS ULT_REG,
                    FN_PAB_RISG_COLR (P_COD_SIS_ENT, P_COD_EST_AOS, V_IND_OPE)
                       AS COLOR
             FROM   PABS_DT_DETLL_OPRCN DETOPE, PABS_DT_ESTDO_ALMOT EST
            WHERE   DETOPE.FLG_EGR_ING = PKG_PAB_CONSTANTES.V_FLG_EGR --Egreso
                    AND DETOPE.COD_SIS_ENT =
                          NVL (v_COD_SIS_ENT, DETOPE.COD_SIS_ENT) --P_COD_SIS_ENT
                    AND DETOPE.COD_EST_AOS = EST.COD_EST_AOS
                    AND DETOPE.COD_EST_AOS = P_COD_EST_AOS
                    AND DETOPE.FEC_VTA >= v_dia_hoy --Todas las operaciones que tienen valuta mayor o igual a hoy
         GROUP BY   EST.DSC_EST_AOS, DETOPE.COD_EST_AOS;

      p_ERROR := PKG_PAB_CONSTANTES.v_OK;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END Sp_PAB_CANT_OPE_EST_EGR;

   -- ***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_CANT_OPE_EST_ING
   -- Objetivo: Procedimiento almacenado que cuenta por el tipo de estado cada una de las operaciones
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_DETLL_OPRCN, PABS_DT_ESTDO_ALMOT, PABS_DT_SISTM_ENTRD_SALID
   -- Fecha: 24/10/16
   -- Autor: gcorrea
   -- Input:
   -- P_COD_EST_AOS -> codigo de estado de operacion
   -- P_COD_SIS_ENT -> codigo de sistema entrada
   -- Output:
   -- p_CURSOR -> cursor de resultado
   -- p_ERROR-> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ?ltimos cambios>
   -- ***********************************************************************************************
   PROCEDURE Sp_PAB_CANT_OPE_EST_ING (P_COD_EST_AOS   IN     NUMBER,
                                      P_COD_SIS_ENT   IN     CHAR,
                                      p_CURSOR           OUT SYS_REFCURSOR,
                                      P_ERROR            OUT NUMBER)
   IS
      v_NOM_SP        VARCHAR2 (30) := 'Sp_PAB_CANT_OPE_EST_ING';
      v_COD_SIS_ENT   CHAR (10);
   BEGIN
      --Tecnologia y OOFF debe ver el ultimo registro de todas las areas
      IF (P_COD_SIS_ENT NOT IN
                (PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SE_TECNO,
                 PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SE_OOFF))
      THEN
         V_COD_SIS_ENT := P_COD_SIS_ENT;                           --Filtramos
      END IF;

      --Estado Operacion
      OPEN p_CURSOR FOR
           SELECT   COUNT (DETOPE.COD_EST_AOS) AS CANT_OPE,
                    DETOPE.COD_EST_AOS AS COD_EST_AOS,
                    EST.DSC_EST_AOS AS DSC_EST,
                    FN_PAB_BUS_ULT_REG_OPE_EST (P_COD_SIS_ENT,
                                                P_COD_EST_AOS,
                                                PKG_PAB_CONSTANTES.V_FLG_ING)
                       AS ULT_REG,
                    FN_PAB_RISG_COLR (P_COD_SIS_ENT, P_COD_EST_AOS, V_IND_OPE)
                       AS COLOR
             FROM   PABS_DT_DETLL_OPRCN DETOPE, PABS_DT_ESTDO_ALMOT EST
            WHERE   DETOPE.FLG_EGR_ING = PKG_PAB_CONSTANTES.V_FLG_ING --Ingreso
                    AND DETOPE.COD_EST_AOS = P_COD_EST_AOS
                    AND NVL (DETOPE.COD_SIS_SAL, '#') =
                          NVL (V_COD_SIS_ENT, NVL (DETOPE.COD_SIS_SAL, '#'))
                    AND DETOPE.COD_EST_AOS = EST.COD_EST_AOS
                    AND DETOPE.FEC_VTA >= v_dia_hoy --Todas las operaciones que tienen valuta mayor o igual a hoy
         GROUP BY   EST.DSC_EST_AOS, DETOPE.COD_EST_AOS;

      p_ERROR := PKG_PAB_CONSTANTES.v_OK;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END Sp_PAB_CANT_OPE_EST_ING;

   -- ***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_CANT_NMN_EST_EGR
   -- Objetivo: Procedimiento almacenado que cuenta por el tipo de estado cada una de las operaciones
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_DETLL_OPRCN, PABS_DT_ESTDO_ALMOT, PABS_DT_SISTM_ENTRD_SALID
   -- Fecha: 06/11/2017
   -- Autor: gcorrea- CAH
   -- Input:
   -- P_COD_EST_AOS -> codigo de estado de operacion
   -- P_COD_SIS_ENT -> codigo de sistema entrada
   -- Output:
   -- p_CURSOR -> cursor de resultado
   -- p_ERROR-> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ?ltimos cambios>
   -- ***********************************************************************************************
   PROCEDURE Sp_PAB_CANT_NMN_EST_EGR (P_COD_EST_AOS   IN     NUMBER,
                                      P_COD_SIS_ENT   IN     CHAR,
                                      p_CURSOR           OUT SYS_REFCURSOR,
                                      P_ERROR            OUT NUMBER)
   IS
      v_NOM_SP        VARCHAR2 (30) := 'Sp_PAB_CANT_NMN_EST_EGR';
      V_COD_SIS_ENT   CHAR (10);
   BEGIN
      --Tecnologia ve todas las operaciones de nominas de todas las areas
      IF (P_COD_SIS_ENT <> PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SE_TECNO)
      THEN
         V_COD_SIS_ENT := P_COD_SIS_ENT;
      END IF;

      --Si el estado es autorizada (Se debe sumar mas estados)
      IF P_COD_EST_AOS = PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEAUT
      THEN
         OPEN p_CURSOR FOR
            SELECT   COUNT (DETNMN.COD_EST_AOS) AS CANT_OPE,
                     PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEAUT AS COD_EST_AOS,
                     'AUTORIZADA' AS DSC_EST,
                     FN_PAB_BUS_ULT_REG_EST (P_COD_EST_AOS,
                                             P_COD_SIS_ENT,
                                             V_IND_OPE)
                        AS ULT_REG,
                     FN_PAB_RISG_COLR (
                        P_COD_SIS_ENT,
                        PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEAUT,
                        V_IND_NOM
                     )
                        AS COLOR
              FROM   PABS_DT_DETLL_NOMNA DETNMN, PABS_DT_CBCRA_NOMNA CABNOM
             WHERE   DETNMN.NUM_FOL_NMN = CABNOM.NUM_FOL_NMN
                     AND DETNMN.COD_EST_AOS IN
                              (PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEAUT,
                               PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPVI,
                               PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPELIB,
                               PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEENV,
                               PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAG,
                               PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAGCFI,
                               PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEREC,
                               PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPDV,
                               PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEDEV)
                     AND DETNMN.FEC_ISR_OPE BETWEEN v_dia_ini AND v_dia_fin --Solo las nominas del dia
                     AND CABNOM.COD_SIS_ENT =
                           NVL (V_COD_SIS_ENT, CABNOM.COD_SIS_ENT)
                     AND CABNOM.COD_EST_AOS =
                           PKG_PAB_CONSTANTES.V_COD_EST_AOS_NMNAUT
            HAVING   COUNT (DETNMN.COD_EST_AOS) > 0;
      ELSE
         OPEN p_CURSOR FOR
              SELECT   COUNT (DETNMN.COD_EST_AOS) AS CANT_OPE,
                       EST.COD_EST_AOS AS COD_EST_AOS,
                       EST.DSC_EST_AOS AS DSC_EST,
                       FN_PAB_BUS_ULT_REG_EST (P_COD_EST_AOS,
                                               P_COD_SIS_ENT,
                                               V_IND_OPE)
                          AS ULT_REG,
                       FN_PAB_RISG_COLR (P_COD_SIS_ENT,
                                         P_COD_EST_AOS,
                                         V_IND_NOM)
                          AS COLOR
                FROM   PABS_DT_CBCRA_NOMNA CABNOM,
                       PABS_DT_DETLL_NOMNA DETNMN,
                       PABS_DT_ESTDO_ALMOT EST
               WHERE       DETNMN.COD_EST_AOS = P_COD_EST_AOS
                       AND DETNMN.FEC_ISR_OPE BETWEEN v_dia_ini AND v_dia_fin --Solo las nominas del dia
                       AND DETNMN.NUM_REF_SWF IS NULL --Para que no cuente las rechazadas por swift
                       AND DETNMN.COD_SIS_SAL IS NULL --Para que no cuente las rechazadas por swift
                       AND DETNMN.COD_EST_AOS = EST.COD_EST_AOS
                       AND DETNMN.NUM_FOL_NMN = CABNOM.NUM_FOL_NMN
                       AND CABNOM.COD_SIS_ENT =
                             NVL (V_COD_SIS_ENT, CABNOM.COD_SIS_ENT)
            GROUP BY   EST.COD_EST_AOS, EST.DSC_EST_AOS
              HAVING   COUNT (DETNMN.COD_EST_AOS) > 0;
      END IF;

      p_ERROR := PKG_PAB_CONSTANTES.v_OK;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END Sp_PAB_CANT_NMN_EST_EGR;

   -- --***********************************************************************************************
   ---- FUNCION/PROCEDIMIENTO: Sp_PAB_MNX_ING_SAL
   ---- OBJETIVO: SP MUESTRA ACTUALIZACI�N EN PANTALLA MONITOREO PARA INGRESO EN MONEDA EXTRANJERA
   ---- SISTEMA: DBO_PAB
   ---- BASE DE DATOS: DBO_PAB
   ---- TABLAS USADAS: PABS_DT_SISTM_ENTRD_SALID
   ---- FECHA:
   ---- AUTOR: NEORIS
   ---- INPUT:
   ---- p_FLAG_ENT_SAL -> FLAG ENTRADA 0 SALIDA 1
   ---- OUTPUT:
   ---- p_CURSOR -> RETORNO DATOS
   ---- P_ERROR  -> MANEJO DE ERRORES.
   ---- OBSERVACIONES: <FECHA Y DETALLE DE ?LTIMOS CAMP_FLG_SIS_ENT_SALBIOS>
   ----***********************************************************************************************
   PROCEDURE Sp_PAB_MNX_ING_SAL (p_FLAG_ENT_SAL   IN     NUMBER,
                                 p_CURSOR            OUT SYS_REFCURSOR,
                                 P_ERROR             OUT NUMBER)
   IS
      v_NOM_SP   VARCHAR2 (30) := 'Sp_PAB_MNX_ING_SAL';
   BEGIN
      BEGIN
         IF P_FLAG_ENT_SAL = PKG_PAB_CONSTANTES.V_FLG_SIS_ENT
         THEN
            --INGRESO MONEDA EXTRANJERA - ENTRADA FLAG 0
            OPEN P_CURSOR FOR
               SELECT   DISTINCT
                        CAN.DSC_SIS_ENT_SAL AS CANAL,
                        0 AS POR_ABONAR,
                        FN_PAB_BUS_CANT_OPE_ABONO (CAN.COD_SIS_ENT_SAL,
                                                   P_FLAG_ENT_SAL)
                           AS ABONADAS
                 FROM   PABS_DT_SISTM_ENTRD_SALID CAN
                WHERE   CAN.COD_SIS_ENT_SAL IN
                              (PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SAL_LBTR,
                               PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SAL_SWF);

            P_ERROR := PKG_PAB_CONSTANTES.V_OK;
         --INGRESO MONEDA EXTRANJERA - SALIDA FLAG 1
         ELSE
            OPEN P_CURSOR FOR
               SELECT   DISTINCT
                        CAN.DSC_SIS_ENT_SAL AS CANAL,
                        0 AS POR_ABONAR,
                        FN_PAB_BUS_CANT_OPE_ABONO (CAN.COD_SIS_ENT_SAL,
                                                   P_FLAG_ENT_SAL)
                           AS ABONADAS
                 FROM   PABS_DT_SISTM_ENTRD_SALID CAN
                WHERE   CAN.COD_SIS_ENT_SAL IN
                              (PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SE_OOFF,
                               PKG_PAB_CONSTANTES.V_COD_TIP_BOLSA);

            P_ERROR := PKG_PAB_CONSTANTES.V_OK;
         END IF;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            ERR_CODE := SQLCODE;
            ERR_MSG := SUBSTR (SQLERRM, 1, 300);
            PKG_PAB_TRACKING.SP_PAB_INS_LOG_BD (ERR_CODE,
                                                V_NOM_PCK,
                                                ERR_MSG,
                                                V_NOM_SP);
            P_ERROR := PKG_PAB_CONSTANTES.V_ERROR;
         WHEN OTHERS
         THEN
            ERR_CODE := SQLCODE;
            ERR_MSG := SUBSTR (SQLERRM, 1, 300);
            PKG_PAB_TRACKING.SP_PAB_INS_LOG_BD (ERR_CODE,
                                                V_NOM_PCK,
                                                ERR_MSG,
                                                V_NOM_SP);
            P_ERROR := PKG_PAB_CONSTANTES.V_ERROR;
            P_S_MENSAJE := ERR_CODE || '-' || ERR_MSG;
            RAISE_APPLICATION_ERROR (-20000, P_S_MENSAJE);
      END;
   END Sp_PAB_MNX_ING_SAL;

   ----**********************************************************************************************
   ---- FUNCION/PROCEDIMIENTO: FN_PAB_BUS_CANT_OPE_ABONO
   ---- OBJETIVO: FN MUESTRA LOS MT910 PARA MONEDA EXTRANJERA DEL D�A.
   ---- SISTEMA: DBO_PAB
   ---- BASE DE DATOS: DBO_PAB
   ---- TABLAS USADAS: PABS_DT_DETLL_CARGO_ABONO_CAJA
   ---- FECHA:
   ---- AUTOR: NEORIS
   ---- INPUT:
   ---- P_COD_SIS_ENT_SAL -> C�DIGO CANAL
   ---- P_FLG_SIS_ENT_SAL -> FLAG ENTRADA O SALIDA
   ---- OUTPUT:
   ---- N/A
   ---- OBSERVACIONES: <FECHA Y DETALLE>
   ---- VAR 22.10.2018 SE MODIFICA FUNCI�N.
   ----***********************************************************************************************
   FUNCTION FN_PAB_BUS_CANT_OPE_ABONO (P_COD_SIS_ENT_SAL   IN CHAR,
                                       P_FLG_SIS_ENT_SAL   IN CHAR)
      RETURN NUMBER
   IS
      V_NOM_SP          VARCHAR2 (30) := 'FN_PAB_BUS_CANT_OPE_ABONO';
      P_COD_SIS_ENT     VARCHAR2 (30);
      P_COD_SIS_SAL     VARCHAR2 (30);
      TOTAL             NUMBER (10);
      V_CANT            NUMBER (10);
      V_CANT2           NUMBER (10);
      V_CANT_910_SWF    NUMBER (10);
      V_CANT_910_LBTR   NUMBER (10);
   BEGIN
      --LIMPIAMOS VARIABLES.
      V_CANT_910_LBTR := PKG_PAB_CONSTANTES.V_CANT_0;
      V_CANT_910_SWF := PKG_PAB_CONSTANTES.V_CANT_0;
      V_CANT := PKG_PAB_CONSTANTES.V_CANT_0;
      V_CANT2 := PKG_PAB_CONSTANTES.V_CANT_0;

      --SI EL FLAG ES 0, BUSCAR� LA ENTRADA DE INGRESO EN MONEDA EXTRAJERA.
      IF P_FLG_SIS_ENT_SAL = PKG_PAB_CONSTANTES.V_FLG_SIS_ENT
      THEN
         --CONSULTA SI ES LBTR.
         IF P_COD_SIS_ENT_SAL = PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SAL_LBTR
         THEN
            SELECT   COUNT (AC.NUM_FOL_OPE)
              INTO   V_CANT_910_LBTR
              FROM   PABS_DT_DETLL_CARGO_ABONO_CAJA AC
             WHERE   AC.COD_SIS_ENT =
                        PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SAL_LBTR
                     AND AC.COD_DVI != PKG_PAB_CONSTANTES.V_STR_DVI_CLP
                     AND AC.COD_MT_SWF = PKG_PAB_CONSTANTES.V_COD_MT910
                     AND TO_CHAR (AC.FEC_ING_OPE, 'DD-MM-YYYY') =
                           TO_CHAR (SYSDATE, 'DD-MM-YYYY')
                     AND (AC.COD_SIS_SAL IS NULL
                          OR AC.COD_SIS_SAL =
                               PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SE_OOFF);
         --CONSULTA SI ES SWIFT.
         ELSIF P_COD_SIS_ENT_SAL = PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SAL_SWF
         THEN
            SELECT   COUNT (AC.NUM_FOL_OPE)
              INTO   V_CANT_910_SWF
              FROM   PABS_DT_DETLL_CARGO_ABONO_CAJA AC
             WHERE   AC.COD_SIS_ENT !=
                        PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SAL_LBTR
                     AND AC.COD_DVI != PKG_PAB_CONSTANTES.V_STR_DVI_CLP
                     AND AC.COD_MT_SWF = PKG_PAB_CONSTANTES.V_COD_MT910
                     AND TO_CHAR (AC.FEC_ING_OPE, 'DD-MM-YYYY') =
                           TO_CHAR (SYSDATE, 'DD-MM-YYYY');
         END IF;

         RETURN V_CANT_910_LBTR + V_CANT_910_SWF;
      --SI EL FLAG ES 1, BUSCAR� LA SALIDA DE INGRESO EN MONEDA EXTRANJERA.
      ELSE
         --CONSULTA SI ES OPERACIONES FINANCIERAS.
         IF P_COD_SIS_ENT_SAL = PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SE_OOFF
         THEN
            SELECT   COUNT (CA.NUM_FOL_OPE) TOTAL
              INTO   V_CANT
              FROM   PABS_DT_DETLL_CARGO_ABONO_CAJA CA
             WHERE   CA.COD_MT_SWF = PKG_PAB_CONSTANTES.V_COD_MT910
                     AND (CA.COD_SIS_ENT =
                             PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SE_OOFF
                          OR CA.COD_SIS_SAL =
                               PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SE_OOFF)
                     AND CA.COD_DVI != PKG_PAB_CONSTANTES.V_STR_DVI_CLP
                     AND (TO_CHAR (CA.FEC_ING_OPE, 'DD_MM_YYYY') =
                             TO_CHAR (SYSDATE, 'DD_MM_YYYY')
                          OR TO_CHAR (CA.FEC_VTA, 'DD_MM_YYYY') =
                               TO_CHAR (SYSDATE, 'DD_MM_YYYY'));
         --SI ES CON OPERACI�N 'SIN-IDEN'.
         ELSE
            SELECT   COUNT (CA.NUM_FOL_OPE) TOTAL
              INTO   V_CANT2
              FROM   PABS_DT_DETLL_CARGO_ABONO_CAJA CA
             WHERE   CA.COD_TPO_OPE_AOS =
                        PKG_PAB_CONSTANTES.V_STR_TPO_OPE_SIDEN
                     AND CA.COD_MT_SWF = PKG_PAB_CONSTANTES.V_COD_MT910
                     AND CA.COD_DVI != PKG_PAB_CONSTANTES.V_STR_DVI_CLP
                     AND (TO_CHAR (CA.FEC_ING_OPE, 'DD_MM_YYYY') =
                             TO_CHAR (SYSDATE, 'DD_MM_YYYY')
                          OR TO_CHAR (CA.FEC_VTA, 'DD_MM_YYYY') =
                               TO_CHAR (SYSDATE, 'DD_MM_YYYY'));
         END IF;

         RETURN V_CANT + V_CANT2;
      END IF;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         ERR_CODE := SQLCODE;
         ERR_MSG := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.SP_PAB_INS_LOG_BD (ERR_CODE,
                                             V_NOM_PCK,
                                             ERR_MSG,
                                             V_NOM_SP);
      WHEN OTHERS
      THEN
         ERR_CODE := SQLCODE;
         ERR_MSG := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.SP_PAB_INS_LOG_BD (ERR_CODE,
                                             V_NOM_PCK,
                                             ERR_MSG,
                                             V_NOM_SP);
         P_S_MENSAJE := ERR_CODE || '-' || ERR_MSG;
         RAISE_APPLICATION_ERROR (-20000, P_S_MENSAJE);
   END FN_PAB_BUS_CANT_OPE_ABONO;
END PKG_PAB_MONITOREO;
/


CREATE SYNONYM USR_PAB.PKG_PAB_MONITOREO FOR DBO_PAB.PKG_PAB_MONITOREO
/


GRANT EXECUTE ON DBO_PAB.PKG_PAB_MONITOREO TO R_APP_PAB
/

GRANT EXECUTE ON DBO_PAB.PKG_PAB_MONITOREO TO USR_PAB
/

