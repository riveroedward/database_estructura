CREATE OR REPLACE /* Formatted on 18-08-2020 17:45:38 (QP5 v5.115.810.9015) */
PACKAGE DBO_PAB.PKG_PAB_CONTINGENCIA
IS
   /**************************VARIABLE GLOBALES************************************/
   v_NOM_PCK     VARCHAR2 (30) := 'PKG_PAB_CONTINGENCIA';
   ERR_CODE      NUMBER := 0;
   ERR_MSG       VARCHAR2 (300);
   v_DES         VARCHAR2 (300);
   p_s_mensaje   VARCHAR2 (400);

   /**************************FIN VARIABLE GLOBALES********************************/
   --***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_ACT_EST_OPE_CON
   -- Objetivo: Procedimiento que actualiza operaciones por contingencia
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: SP_EST_OPE_AOS
   -- Fecha: 25/07/17
   -- Autor: Santander
   -- Input:
   -- p_NUM_FOL_OPE    -> Numero folio operacion
   -- p_FEC_ISR_OPE    -> fecha de insercion de operacion
   -- p_COD_EST_AOS    -> Codigo estado operacion
   -- p_GLS_ADC_EST    -> glosa adicional estado
   -- p_COD_USR        -> usuario de accion
   -- Output:
   -- p_ERROR         -> codigo de error de ejecucion de PL (para log)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de utimos cambios>
   --***********************************************************************************************
   PROCEDURE Sp_PAB_ACT_EST_OPE_CON (p_NUM_FOL_OPE   IN     NUMBER,
                                     p_FEC_ISR_OPE   IN     DATE,
                                     p_COD_EST_AOS   IN     CHAR,
                                     p_GLS_ADC_EST   IN     VARCHAR2,
                                     p_COD_USR       IN     VARCHAR2,
                                     p_ERROR            OUT NUMBER);

   --************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_BUS_OPE_ING
   -- Objetivo: Procedimiento que busca una operaci?n de ingreso por n?mero de referencia swift, para comprobar si existe antes de insertar una nueva operaci?n.
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_DETLL_OPRCN
   -- Fecha: 02/06/16
   -- Autor: Santander
   -- Input:    p_NUM_REF_SWF->   foliador del banco
   -- Output:
   --        p_FLG_EXT : flag indicador de salida de programa.
   --        p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/

   PROCEDURE Sp_PAB_BUS_OPE_ING (p_NUM_REF_SWF   IN     CHAR,
                                 p_FLG_EXT          OUT NUMBER,
                                 p_ERROR            OUT NUMBER);

   --***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_BUS_OPE_CONT_COMBANC
   -- Objetivo: Procedimiento que busca operaciones para enviar por contingencia combanc
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_DETLL_OPRCN
   -- Fecha: 25/07/17
   -- Autor: Santander
   -- Input:
   -- p_TIPO_OP        -> tipo de operacion
   -- p_MT_SWIFT       -> numero de mensaje
   -- p_NOM_BFC        -> nombre beneficiario
   -- p_BCO_DTN        -> banco destino
   -- p_MONTO_DESDE    -> monto desde
   -- p_MONTO_HASTA    -> monto hasta
   -- p_NUM_NOM        -> Numero de nomina
   -- Output:
   -- p_CURSOR        -> cursor que retorna operaciones disponibles.
   -- p_ERROR         -> codigo de error de ejecucion de PL (para log)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de utimos cambios>
   --***********************************************************************************************
   PROCEDURE Sp_PAB_BUS_OPE_CONT_COMBANC (
      p_TIPO_OP       IN     CHAR,
      p_MT_SWIFT      IN     NUMBER,
      p_NOM_BFC       IN     VARCHAR2,
      p_BCO_DTN       IN     CHAR,
      p_MONTO_DESDE   IN     NUMBER,
      p_MONTO_HASTA   IN     NUMBER,
      p_NUM_NOM       IN     NUMBER,
      p_CURSOR           OUT SYS_REFCURSOR,
      p_ERROR            OUT NUMBER
   );

   --************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_BUS_ARCH_GEN
   -- Objetivo: Retorna los archivos generados y por descargar de las contingencias combanc
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas:
   -- Fecha: 13/12/2016
   -- Autor: Santander
   -- Input:
   -- p_FEC_ARC_CGE    -> Fecha Proceso.
   -- p_FLG_ARC_CGE    -> Flag de descarga/descarga de archivo
   -- Output:
   -- p_CURSOR         -> Cursor con los archivos generados.
   -- p_ERROR          -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores
   -- Retorno: N/A.
   -- Observaciones: 14/09/2016, Creacion Procedimiento.
   --***********************************************************************************************
   PROCEDURE Sp_PAB_BUS_ARCH_GEN (p_FEC_ARC_CGE   IN     DATE,
                                  p_FLG_ARC_CGE   IN     NUMBER,
                                  p_CURSOR           OUT SYS_REFCURSOR,
                                  p_ERROR            OUT NUMBER);

   --************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_ACT_ARCH_GEN
   -- Objetivo: actualiza los archivos generados y por descargar de las contingencias combanc
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas:
   -- Fecha: 13/12/2016
   -- Autor: Santander
   -- Input:
   -- p_NUM_CRV_ARC_CGE     -> numero identificador de archivo
   -- p_COD_USR_DGA_CGE     -> codigo usuario que descarga
   -- p_FEC_DGA_ARC_CGE     -> fecha de ultima descarga
   -- Output:
   -- p_ERROR          -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores
   -- Retorno: N/A.
   -- Observaciones: 14/09/2016, Creacion Procedimiento.
   --***********************************************************************************************
   PROCEDURE Sp_PAB_ACT_ARCH_GEN (p_NUM_CRV_ARC_CGE   IN     NUMBER,
                                  p_COD_USR_DGA_CGE   IN     VARCHAR2,
                                  p_FEC_DGA_ARC_CGE   IN     DATE,
                                  p_ERROR                OUT NUMBER);

   --************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_RES_CONT_SINACOFI
   -- Objetivo: procedimiento que genera el resumen de operacione generadas por sinacofi.
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas:
   -- Fecha: 13/12/2016
   -- Autor: Santander
   -- Input:
   -- p_FEC_VTA     -> fecha valuta
   -- Output:
   -- p_CURSOR         -> cursor con resumen de operaciones
   -- p_ERROR          -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores
   -- Retorno: N/A.
   -- Observaciones: 14/09/2016, Creacion Procedimiento.
   --***********************************************************************************************
   PROCEDURE Sp_PAB_RES_CONT_SINACOFI (p_FEC_VTA   IN     DATE,
                                       p_CURSOR       OUT SYS_REFCURSOR,
                                       p_ERROR        OUT NUMBER);

   --************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_DET_CONT_SINACOFI
   -- Objetivo: procedimiento que genera el detalle de operacione generadas por sinacofi.
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas:
   -- Fecha: 13/12/2016
   -- Autor: Santander
   -- Input:
   -- p_COD_BCO_DTN  -> BIC BANCO DESTINO
   -- p_FEC_VTA     -> fecha valuta
   -- Output:
   -- p_CURSOR         -> cursor con resumen de operaciones
   -- p_ERROR          -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores
   -- Retorno: N/A.
   -- Observaciones: 14/09/2016, Creacion Procedimiento.
   --***********************************************************************************************
   PROCEDURE Sp_PAB_DET_CONT_SINACOFI (p_COD_BCO_DTN   IN     CHAR,
                                       p_FEC_VTA       IN     DATE,
                                       p_CURSOR           OUT SYS_REFCURSOR,
                                       p_ERROR            OUT NUMBER);

   --************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_BUS_OPE_CONT_LBTR
   -- Objetivo: Busca operaciones para generar contingencia SINACOF.
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas:
   -- Fecha: 13/12/2016
   -- Autor: Santander
   -- Input:
   -- p_TIPO_OP        -> tipo de operacion
   -- p_MT_SWIFT       -> numero de mensaje
   -- p_NOM_BFC        -> nombre beneficiario
   -- p_BCO_DTN        -> banco destino
   -- p_MONTO_DESDE    -> monto desde
   -- p_MONTO_HASTA    -> monto hasta
   -- p_NUM_FOL_MNA    -> Numero de nomina
   -- p_COD_SIS_ENT    -> codigo sistema de entrada
   -- p_COD_SIS_SAL    -> codigo sistema salida
   -- p_NUM_FOL_OPR    -> numero de folio
   -- p_NOM_BFC        -> nombre beneficiario
   -- p_NUM_DCO_ORD    -> numero de documento ordenante
   -- Output:
   -- p_CURSOR         -> cursor con resumen de operaciones
   -- p_ERROR          -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores
   -- Retorno: N/A.
   -- Observaciones: 14/09/2016, Creacion Procedimiento.
   --***********************************************************************************************
   PROCEDURE Sp_PAB_BUS_OPE_CONT_LBTR (p_TIPO_OP       IN     CHAR,
                                       p_MT_SWIFT      IN     NUMBER,
                                       p_BCO_DTN       IN     CHAR,
                                       p_MONTO_DESDE   IN     NUMBER,
                                       p_MONTO_HASTA   IN     NUMBER,
                                       p_NUM_FOL_MNA   IN     NUMBER,
                                       p_COD_SIS_ENT   IN     VARCHAR2,
                                       p_COD_SIS_SAL   IN     VARCHAR2,
                                       p_NUM_FOL_OPR   IN     NUMBER,
                                       p_NOM_BFC       IN     VARCHAR2,
                                       p_NUM_DCO_ORD   IN     VARCHAR2,
                                       p_CURSOR           OUT SYS_REFCURSOR,
                                       p_ERROR            OUT NUMBER);


   --************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_ING_ARCH_CGE
   -- Objetivo: Inserta acrchivo generados y por descargar de las contingencias combanc
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas:
   -- Fecha: 13/11/2017
   -- Autor: Santander
   -- Input:
   -- p_ARC_CGE_ECD -> nombre beneficiario
   -- Output:
   -- p_ERROR       -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores
   -- Retorno: N/A.
   -- Observaciones:
   --***********************************************************************************************
   PROCEDURE Sp_PAB_ING_ARCH_CGE (p_ARC_CGE_ECD IN CLOB, p_ERROR OUT NUMBER);

   --************************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_BUS_VVV_CTG_PLZ
   -- Objetivo: Procedimiento que busca las operaciones con fecha valuta hoy, de los canales de entrada de OFFICE BANKING y CAJA
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_DETLL_OPRCN
   -- Fecha: 02/06/16
   -- Autor: Santander
   -- Input:    p_FECHA_DESDE->   Fecha desde para fecha valuta
   --           p_FECHA_HASTA->   Fecha hasta para fecha valuta
   -- Output:
   --           p_CURSOR: Cursor con tabla de resultado de operaciones
   --           p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE SP_PAB_BUS_VVV_CTG_PLZ (p_FECHA_DESDE       DATE,
                                     p_FECHA_HASTA       DATE,
                                     p_CURSOR        OUT SYS_REFCURSOR,
                                     p_ERROR         OUT NUMBER);
END PKG_PAB_CONTINGENCIA;
/


CREATE OR REPLACE /* Formatted on 18-08-2020 17:45:38 (QP5 v5.115.810.9015) */
PACKAGE BODY DBO_PAB.PKG_PAB_CONTINGENCIA
IS
   --***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_ACT_EST_OPE_CON
   -- Objetivo: Procedimiento que actualiza operaciones por contingencia
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: SP_EST_OPE_AOS
   -- Fecha: 25/07/17
   -- Autor: Santander
   -- Input:
   -- p_NUM_FOL_OPE    -> Numero folio operacion
   -- p_FEC_ISR_OPE    -> fecha de insercion de operacion
   -- p_COD_EST_AOS    -> Codigo estado operacion
   -- p_GLS_ADC_EST    -> glosa adicional estado
   -- p_COD_USR        -> usuario de accion
   -- Output:
   -- p_ERROR         -> codigo de error de ejecucion de PL (para log)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de utimos cambios>
   --***********************************************************************************************
   PROCEDURE Sp_PAB_ACT_EST_OPE_CON (p_NUM_FOL_OPE   IN     NUMBER,
                                     p_FEC_ISR_OPE   IN     DATE,
                                     p_COD_EST_AOS   IN     CHAR,
                                     p_GLS_ADC_EST   IN     VARCHAR2,
                                     p_COD_USR       IN     VARCHAR2,
                                     p_ERROR            OUT NUMBER)
   IS
      v_NOM_SP            VARCHAR2 (30) := 'Sp_PAB_ACT_EST_OPE_CON';
      v_COD_EST_AOS       NUMBER;
      v_COD_EST_AOS_ACT   NUMBER;
      v_RESULT            NUMBER;
      v_NUM_FOL_OPE       NUMBER;
      v_FEC_ISR_OPE       DATE;
      V_GLS_ADC_EST       VARCHAR2 (210) := NULL;
      v_DES_BIT           VARCHAR2 (300);
   BEGIN
      V_GLS_ADC_EST := p_GLS_ADC_EST;

      --Actualiza estado de la operacion y glosa en sp de package operacion
      PKG_PAB_OPERACION.SP_PAB_ACT_EST_GLS_OPE (
         p_NUM_FOL_OPE,
         p_FEC_ISR_OPE,
         p_COD_EST_AOS,
         PKG_PAB_CONSTANTES.V_GLS_EST_CON,
         p_COD_USR,
         p_ERROR
      );

      --Guardamos
      COMMIT;

      p_ERROR := PKG_PAB_CONSTANTES.v_OK;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg :=
               'No se encuentran registros para operacio '
            || p_NUM_FOL_OPE
            || '- fecha '
            || p_FEC_ISR_OPE
            || '- estado'
            || p_COD_EST_AOS;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         p_s_mensaje := err_code || '-' || err_msg;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END Sp_PAB_ACT_EST_OPE_CON;

   --************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_BUS_OPE_ING
   -- Objetivo: Procedimiento que busca una operaci?n de ingreso por n?mero de referencia swift, para comprobar si existe antes de insertar una nueva operaci?n.
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_DETLL_OPRCN
   -- Fecha: 02/06/16
   -- Autor: Santander
   -- Input:    p_NUM_FOL_NMN->   foliador del banco
   --           p_FEC_ISR_OPE->   Fecha de insercion de la operacion
   --           p_COD_EST_AOS->   Identificador de nuevo estado.
   --           p_COD_USR->       rut usuario
   -- Output:
   --        p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE Sp_PAB_BUS_OPE_ING (p_NUM_REF_SWF   IN     CHAR,
                                 p_FLG_EXT          OUT NUMBER,
                                 p_ERROR            OUT NUMBER)
   IS
      v_NOM_SP        VARCHAR2 (30) := 'Sp_PAB_BUS_OPE_ING';
      v_NUM_FOL_OPE   NUMBER;
      v_FEC_ISR_OPE   DATE;
      v_NUM_REF_SWF   CHAR (16);
   BEGIN
      p_ERROR := 0;

      --Si la busqueda es por numero de referencia Swift, buscamos el numero de operacion y la fecha.
      IF (NVL (p_NUM_REF_SWF, '#') <> '#')
      THEN
         v_NUM_REF_SWF := p_NUM_REF_SWF;

         BEGIN
            SELECT   NUM_FOL_OPE, FEC_ISR_OPE
              INTO   v_NUM_FOL_OPE, v_FEC_ISR_OPE
              FROM   PABS_DT_DETLL_OPRCN
             WHERE   NUM_REF_SWF = v_NUM_REF_SWF AND FLG_EGR_ING = 1
                     AND FEC_ISR_OPE BETWEEN TO_DATE (
                                                TO_CHAR (SYSDATE,
                                                         'yyyy-mm-dd')
                                                || ' 00:00:00',
                                                'yyyy-mm-dd hh24:mi:ss'
                                             )
                                         AND  TO_DATE (
                                                 TO_CHAR (SYSDATE,
                                                          'yyyy-mm-dd')
                                                 || ' 23:59:59',
                                                 'yyyy-mm-dd hh24:mi:ss'
                                              );
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               p_FLG_EXT := 0;
            WHEN OTHERS
            THEN
               err_code := SQLCODE;
               err_msg := SUBSTR (SQLERRM, 1, 300);
               p_s_mensaje := err_code || '-' || err_msg;
               --PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);
               p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
               RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
         END;

         IF (v_NUM_FOL_OPE IS NOT NULL AND v_FEC_ISR_OPE IS NOT NULL)
         THEN
            p_FLG_EXT := 1;
         END IF;
      END IF;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         p_s_mensaje := err_code || '-' || err_msg;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END Sp_PAB_BUS_OPE_ING;

   --***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_BUS_OPE_CONT_COMBANC
   -- Objetivo: Procedimiento que busca operaciones para enviar por contingencia combanc
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_DETLL_OPRCN
   -- Fecha: 25/07/17
   -- Autor: Santander
   -- Input:
   -- p_TIPO_OP        -> tipo de operacion
   -- p_MT_SWIFT       -> numero de mensaje
   -- p_NOM_BFC        -> nombre beneficiario
   -- p_BCO_DTN        -> banco destino
   -- p_MONTO_DESDE    -> monto desde
   -- p_MONTO_HASTA    -> monto hasta
   -- p_NUM_NOM        -> Numero de nomina
   -- Output:
   -- p_CURSOR        -> cursor que retorna operaciones disponibles.
   -- p_ERROR         -> codigo de error de ejecucion de PL (para log)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de utimos cambios>
   --***********************************************************************************************
   PROCEDURE Sp_PAB_BUS_OPE_CONT_COMBANC (
      p_TIPO_OP       IN     CHAR,
      p_MT_SWIFT      IN     NUMBER,
      p_NOM_BFC       IN     VARCHAR2,
      p_BCO_DTN       IN     CHAR,
      p_MONTO_DESDE   IN     NUMBER,
      p_MONTO_HASTA   IN     NUMBER,
      p_NUM_NOM       IN     NUMBER,
      p_CURSOR           OUT SYS_REFCURSOR,
      p_ERROR            OUT NUMBER
   )
   IS
      --Seteo de Variables
      v_NOM_SP        VARCHAR2 (30) := 'Sp_PAB_BUS_OPE_LIB';

      v_TIPO_OP       CHAR (8);
      v_COD_ENT       CHAR (10);
      v_COD_SAL       CHAR (10);
      v_BCO_DTN       CHAR (11);
      v_NUM_RUT       CHAR (11);
      v_IMp_DSD       NUMBER;
      v_IMp_HST       NUMBER;
      v_NUM_NOM       NUMBER;
      v_NUM_OPE       NUMBER;
      v_Dv_RUT        VARCHAR2 (1);
      v_MT_SWIFT      NUMBER;
      v_COD_TIP_SEG   NUMBER;
   BEGIN
      --Seteamos las variableas
      v_TIPO_OP := p_TIPO_OP;                                   --Siempre data
      v_BCO_DTN := p_BCO_DTN;                                   --Siempre data


      --Siempre data
      IF p_MT_SWIFT = -1
      THEN
         v_MT_SWIFT := NULL;
      ELSE
         v_MT_SWIFT := p_MT_SWIFT;
      END IF;

      --Siempre data
      IF p_MONTO_DESDE = -1
      THEN
         v_IMp_DSD := 0;
      ELSE
         v_IMp_DSD := p_MONTO_DESDE;
      END IF;

      --Siempre data
      IF p_MONTO_HASTA IS NULL OR p_MONTO_HASTA = 0
      THEN
         v_IMp_HST := 9999999999999;
      ELSE
         v_IMp_HST := p_MONTO_HASTA;
      END IF;

      --Puede ser nulo
      IF p_NUM_NOM = -1
      THEN
         v_NUM_NOM := NULL;
      ELSE
         v_NUM_NOM := p_NUM_NOM;
      END IF;


      p_ERROR := 0;


      BEGIN
         OPEN p_CURSOR FOR
            SELECT   DET.NUM_FOL_OPE,
                     DET.FEC_ISR_OPE,
                     DET.COD_SIS_ENT,
                     SIS.DSC_SIS_ENT_SAL,
                     DET.COD_MT_SWF,
                     DET.COD_TPO_OPE_AOS,
                     DET.IMP_OPE,
                     DET.COD_DVI,
                     DET.NOM_BFC,
                     DET.COD_BCO_DTN,
                     TI.DES_BCO,
                     DET.FEC_VTA,
                     PKG_PAB_UTILITY.FN_PAB_TRAN_COD_EST_GLOSA (
                        DET.COD_EST_AOS,
                        PKG_PAB_CONSTANTES.V_IND_OPE
                     )
                        AS COD_EST_AOS
              FROM   PABS_DT_DETLL_OPRCN DET,
                     PABS_DT_SISTM_ENTRD_SALID SIS,
                     TCDTBAI TI
             WHERE   DET.COD_BCO_DTN = TI.TGCDSWSA
                     AND DET.COD_SIS_SAL =
                           PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SAL_CMN
                     AND DET.COD_SIS_SAL = SIS.COD_SIS_ENT_SAL
                     AND DET.COD_EST_AOS IN
                              (PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEAUT) --,PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEENV )
                     AND NVL (DET.COD_TPO_OPE_AOS, '#') =
                           NVL (v_TIPO_OP, NVL (DET.COD_TPO_OPE_AOS, '#')) -- TIPO OPERACION ALTOS MONTOS
                     AND NVL (DET.COD_BCO_DTN, '#') =
                           NVL (v_BCO_DTN, NVL (DET.COD_BCO_DTN, '#')) -- BANCO DESTINO
                     AND NVL (DET.COD_MT_SWF, 0) =
                           NVL (v_MT_SWIFT, NVL (DET.COD_MT_SWF, 0)) -- MT SWIFT
                  --AND DET.IMp_OPE BETWEEN v_IMp_DSD AND v_IMp_HST; -- MONTOS
                     AND NVL (DET.NUM_FOL_NMN, 0) =
                           NVL (v_NUM_NOM, NVL (DET.NUM_FOL_NMN, 0));
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            err_code := SQLCODE;
            err_msg :=
                  'No se encuentran registros para operacion '
               || p_TIPO_OP
               || '- tipo de mensaje '
               || p_MT_SWIFT
               || '- nombre beneficiario '
               || p_NOM_BFC
               || '-banco destino '
               || p_BCO_DTN
               || '- monto desde'
               || p_MONTO_DESDE
               || '- monto hasta'
               || p_MONTO_HASTA
               || '- numero de nomina'
               || p_NUM_NOM;



            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         WHEN OTHERS
         THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            p_s_mensaje := err_code || '-' || err_msg;
            --PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
      END;
   EXCEPTION
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         p_s_mensaje := err_code || '-' || err_msg;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END Sp_PAB_BUS_OPE_CONT_COMBANC;

   --************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_BUS_ARCH_GEN
   -- Objetivo: Retorna los archivos generados y por descargar de las contingencias combanc
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas:
   -- Fecha: 13/12/2016
   -- Autor: Santander
   -- Input:
   -- p_FEC_ARC_CGE    -> Fecha Proceso.
   -- p_FLG_ARC_CGE    -> Flag de descarga/descarga de archivo
   -- Output:
   -- p_CURSOR         -> Cursor con los archivos generados.
   -- p_ERROR          -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores
   -- Retorno: N/A.
   -- Observaciones: 14/09/2016, Creacion Procedimiento.
   --***********************************************************************************************
   PROCEDURE Sp_PAB_BUS_ARCH_GEN (p_FEC_ARC_CGE   IN     DATE,
                                  p_FLG_ARC_CGE   IN     NUMBER,
                                  p_CURSOR           OUT SYS_REFCURSOR,
                                  p_ERROR            OUT NUMBER)
   IS
      --Seteo de Variables
      v_NOM_SP            VARCHAR2 (30) := 'Sp_PAB_BUS_ARCH_GEN';
      v_NUM_CRV_ARC_CGE   NUMBER := 0;
   BEGIN
      p_ERROR := 0;


      BEGIN
         OPEN p_CURSOR FOR
            SELECT   NUM_CRV_ARC_CGE,
                     FEC_ARC_CGE,
                     NOM_ARC_CGE,
                     ARC_CGE_ECD AS TXT_BASE64
              FROM   PABS_DT_ARCHV_CNTGC_COBAC
             WHERE       NUM_CRV_ARC_CGE > v_NUM_CRV_ARC_CGE
                     AND TRUNC (FEC_ARC_CGE) = TRUNC (p_FEC_ARC_CGE) --VALIDAR FECHA
                     AND FLG_ARC_CGE = p_FLG_ARC_CGE;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            err_code := SQLCODE;
            err_msg :=
                  'No se encuentran registros para archivo nombre '
               || p_FEC_ARC_CGE
               || '- flag de operacion '
               || p_FLG_ARC_CGE;
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         WHEN OTHERS
         THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            p_s_mensaje := err_code || '-' || err_msg;
            --PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
      END;
   EXCEPTION
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         p_s_mensaje := err_code || '-' || err_msg;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END Sp_PAB_BUS_ARCH_GEN;


   --************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_ACT_ARCH_GEN
   -- Objetivo: actualiza los archivos generados y por descargar de las contingencias combanc
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas:
   -- Fecha: 13/12/2016
   -- Autor: Santander
   -- Input:
   -- p_NUM_CRV_ARC_CGE     -> numero identificador de archivo
   -- p_COD_USR_DGA_CGE     -> codigo usuario que descarga
   -- p_FEC_DGA_ARC_CGE     -> fecha de ultima descarga
   -- Output:
   -- p_ERROR          -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores
   -- Retorno: N/A.
   -- Observaciones: 14/09/2016, Creacion Procedimiento.
   --***********************************************************************************************
   PROCEDURE Sp_PAB_ACT_ARCH_GEN (p_NUM_CRV_ARC_CGE   IN     NUMBER,
                                  p_COD_USR_DGA_CGE   IN     VARCHAR2,
                                  p_FEC_DGA_ARC_CGE   IN     DATE,
                                  p_ERROR                OUT NUMBER)
   IS
      --Seteo de Variables
      v_NOM_SP   VARCHAR2 (30) := 'Sp_PAB_ACT_ARCH_GEN';
   BEGIN
      p_ERROR := 0;


      BEGIN
         UPDATE   PABS_DT_ARCHV_CNTGC_COBAC
            SET   FLG_ARC_CGE = 1,
                  COD_USR_DGA_CGE = p_COD_USR_DGA_CGE,
                  FEC_DGA_ARC_CGE = p_FEC_DGA_ARC_CGE
          WHERE   NUM_CRV_ARC_CGE = p_NUM_CRV_ARC_CGE;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         WHEN OTHERS
         THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            p_s_mensaje := err_code || '-' || err_msg;
            --PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
      END;
   EXCEPTION
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         p_s_mensaje := err_code || '-' || err_msg;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END Sp_PAB_ACT_ARCH_GEN;

   --************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_RES_CONT_SINACOFI
   -- Objetivo: procedimiento que genera el resumen de operacione generadas por sinacofi.
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas:
   -- Fecha: 13/12/2016
   -- Autor: Santander
   -- Input:
   -- p_FEC_VTA     -> fecha valuta
   -- Output:
   -- p_CURSOR         -> cursor con resumen de operaciones
   -- p_ERROR          -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores
   -- Retorno: N/A.
   -- Observaciones: 14/09/2016, Creacion Procedimiento.
   --***********************************************************************************************
   PROCEDURE Sp_PAB_RES_CONT_SINACOFI (p_FEC_VTA   IN     DATE,
                                       p_CURSOR       OUT SYS_REFCURSOR,
                                       p_ERROR        OUT NUMBER)
   IS
      --Seteo de Variables
      v_NOM_SP   VARCHAR2 (30) := 'Sp_PAB_RES_CONT_SINACOFI';
   BEGIN
      p_ERROR := 0;


      BEGIN
         OPEN p_CURSOR FOR
              SELECT   OPE.COD_BCO_DTN AS BIC_BCO,
                       TCDT.NBBAN AS NOM_BCO,
                       SUM (OPE.IMP_OPE) AS TOTAL
                FROM   PABS_DT_DETLL_OPRCN OPE, TCDT040 TCDT
               WHERE   TCDT.TGCDSWSA = OPE.COD_BCO_DTN
                       AND OPE.FEC_VTA = p_FEC_VTA
                       AND OPE.COD_EST_AOS =
                             PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAGCFI --> ESTADO 21
            GROUP BY   OPE.COD_BCO_DTN, TCDT.NBBAN;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            err_code := SQLCODE;
            err_msg := 'No se encuentran registros para fecha ' || p_FEC_VTA;
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         WHEN OTHERS
         THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            p_s_mensaje := err_code || '-' || err_msg;
            --PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
      END;
   EXCEPTION
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         p_s_mensaje := err_code || '-' || err_msg;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END Sp_PAB_RES_CONT_SINACOFI;


   --************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_DET_CONT_SINACOFI
   -- Objetivo: procedimiento que genera el detalle de operacione generadas por sinacofi.
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas:
   -- Fecha: 13/12/2016
   -- Autor: Santander
   -- Input:
   -- p_COD_BCO_DTN  -> BIC BANCO DESTINO
   -- p_FEC_VTA     -> fecha valuta
   -- Output:
   -- p_CURSOR         -> cursor con resumen de operaciones
   -- p_ERROR          -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores
   -- Retorno: N/A.
   -- Observaciones: 14/09/2016, Creacion Procedimiento.
   --***********************************************************************************************
   PROCEDURE Sp_PAB_DET_CONT_SINACOFI (p_COD_BCO_DTN   IN     CHAR,
                                       p_FEC_VTA       IN     DATE,
                                       p_CURSOR           OUT SYS_REFCURSOR,
                                       p_ERROR            OUT NUMBER)
   IS
      --Seteo de Variables
      v_NOM_SP        VARCHAR2 (30) := 'Sp_PAB_DET_CONT_SINACOFI';
      v_COD_BCO_DTN   CHAR (11);
   BEGIN
      v_COD_BCO_DTN := p_COD_BCO_DTN;
      p_ERROR := 0;


      BEGIN
         OPEN p_CURSOR FOR
            SELECT   OPE.NUM_FOL_OPE,
                     OPE.FEC_ISR_OPE,
                     OPE.FEC_VTA,
                     DET.NUM_DOC_ODN,
                     DET.NOM_ODN,
                     DET.NUM_CTA_ODN,
                     DET.NUM_DOC_BFC,
                     OPE.NOM_BFC,
                     DET.NUM_CTA_ODN,
                     OPE.IMP_OPE
              FROM   PABS_DT_DETLL_OPRCN OPE, PABS_DT_OPRCN_INFCN_OPCON DET
             WHERE   OPE.COD_EST_AOS =
                        PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAGCFI --> ESTADO 21
                     AND OPE.FEC_VTA = p_FEC_VTA
                     AND OPE.NUM_FOL_OPE = DET.NUM_FOL_OPE
                     AND OPE.FEC_ISR_OPE = DET.FEC_ISR_OPE
                     AND OPE.COD_BCO_DTN = v_COD_BCO_DTN;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            err_code := SQLCODE;
            err_msg :=
                  'No se encuentran registros para banco '
               || p_COD_BCO_DTN
               || '- fecha'
               || p_FEC_VTA;
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         WHEN OTHERS
         THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            p_s_mensaje := err_code || '-' || err_msg;
            --PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
      END;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         p_s_mensaje := err_code || '-' || err_msg;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END Sp_PAB_DET_CONT_SINACOFI;

   --************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_BUS_OPE_CONT_LBTR
   -- Objetivo: Busca operaciones para generar contingencia SINACOF.
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas:
   -- Fecha: 13/12/2016
   -- Autor: Santander
   -- Input:
   -- p_TIPO_OP        -> tipo de operacion
   -- p_MT_SWIFT       -> numero de mensaje
   -- p_NOM_BFC        -> nombre beneficiario
   -- p_BCO_DTN        -> banco destino
   -- p_MONTO_DESDE    -> monto desde
   -- p_MONTO_HASTA    -> monto hasta
   -- p_NUM_FOL_MNA    -> Numero de nomina
   -- p_COD_SIS_ENT    -> codigo sistema de entrada
   -- p_COD_SIS_SAL    -> codigo sistema salida
   -- p_NUM_FOL_OPR    -> numero de folio
   -- p_NOM_BFC        -> nombre beneficiario
   -- p_NUM_DCO_ORD    -> numero de documento ordenante
   -- Output:
   -- p_CURSOR         -> cursor con resumen de operaciones
   -- p_ERROR          -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores
   -- Retorno: N/A.
   -- Observaciones: 14/09/2016, Creacion Procedimiento.
   --***********************************************************************************************
   PROCEDURE Sp_PAB_BUS_OPE_CONT_LBTR (p_TIPO_OP       IN     CHAR,
                                       p_MT_SWIFT      IN     NUMBER,
                                       p_BCO_DTN       IN     CHAR,
                                       p_MONTO_DESDE   IN     NUMBER,
                                       p_MONTO_HASTA   IN     NUMBER,
                                       p_NUM_FOL_MNA   IN     NUMBER,
                                       p_COD_SIS_ENT   IN     VARCHAR2,
                                       p_COD_SIS_SAL   IN     VARCHAR2,
                                       p_NUM_FOL_OPR   IN     NUMBER,
                                       p_NOM_BFC       IN     VARCHAR2,
                                       p_NUM_DCO_ORD   IN     VARCHAR2,
                                       p_CURSOR           OUT SYS_REFCURSOR,
                                       p_ERROR            OUT NUMBER)
   IS
      --Seteo de Variables
      v_NOM_SP        VARCHAR2 (30) := 'Sp_PAB_BUS_OPE_CONT_LBTR';
      v_MT_SWIFT      NUMBER (3);
      v_TIPO_OP       CHAR (8);
      v_COD_ENT       CHAR (10);
      v_COD_SAL       CHAR (10);
      v_BCO_DTN       CHAR (11);
      v_NUM_RUT       CHAR (11);
      v_IMp_DSD       NUMBER;
      v_IMp_HST       NUMBER;
      v_NUM_NOM       NUMBER;
      v_NUM_OPE       NUMBER;
      v_NUM_DCO_ORD   VARCHAR2 (10);
      v_NOM_BFC       VARCHAR2 (100);
      v_Dv_RUT        VARCHAR2 (1);
      v_COD_TIP_SEG   NUMBER;
   BEGIN
      --Seteamos las variableas
      v_TIPO_OP := p_TIPO_OP;
      v_BCO_DTN := p_BCO_DTN;
      v_MT_SWIFT := p_MT_SWIFT;
      v_COD_ENT := p_COD_SIS_ENT;
      v_COD_SAL := p_COD_SIS_SAL;

      --Siempre data
      IF p_MT_SWIFT = -1
      THEN
         v_MT_SWIFT := NULL;
      ELSE
         v_MT_SWIFT := p_MT_SWIFT;
      END IF;

      --Siempre data
      IF p_MONTO_DESDE = -1
      THEN
         v_IMp_DSD := 0;
      ELSE
         v_IMp_DSD := p_MONTO_DESDE;
      END IF;

      --Siempre data
      IF p_MONTO_HASTA IS NULL OR p_MONTO_HASTA = 0
      THEN
         v_IMp_HST := 9999999999999;
      ELSE
         v_IMp_HST := p_MONTO_HASTA;
      END IF;

      --Puede ser nulo
      IF p_NUM_FOL_MNA = -1
      THEN
         v_NUM_NOM := NULL;
      ELSE
         v_NUM_NOM := p_NUM_FOL_MNA;
      END IF;

      IF p_NUM_FOL_OPR = -1
      THEN
         v_NUM_OPE := NULL;
      ELSE
         v_NUM_OPE := p_NUM_FOL_OPR;
      END IF;

      IF p_NUM_DCO_ORD = -1
      THEN
         v_NUM_DCO_ORD := NULL;
      ELSE
         v_NUM_DCO_ORD := p_NUM_DCO_ORD;
      END IF;

      IF p_NOM_BFC = ''
      THEN
         v_NOM_BFC := NULL;
      ELSE
         v_NOM_BFC := TRIM (p_NOM_BFC);
      END IF;

      --Siempre data

      --  PABS_DT_BNFCR_ORDNT

      p_ERROR := 0;


      BEGIN
         OPEN p_CURSOR FOR
            SELECT   DET.NUM_FOL_OPE,
                     DET.FEC_ISR_OPE,
                     DET.COD_SIS_ENT,
                     SIS.DSC_SIS_ENT_SAL,
                     DET.COD_MT_SWF,
                     DET.COD_TPO_OPE_AOS,
                     DET.IMP_OPE,
                     DET.COD_DVI,
                     DET.NOM_BFC,
                     DET.COD_BCO_DTN,
                     TI.DES_BCO,
                     DET.FEC_VTA
              FROM   PABS_DT_DETLL_OPRCN DET,
                     PABS_DT_SISTM_ENTRD_SALID SIS,
                     TCDTBAI TI,
                     PABS_DT_OPRCN_INFCN_OPCON DETOPC
             WHERE   DET.COD_BCO_DTN = TI.TGCDSWSA(+)
                     AND DET.COD_SIS_SAL =
                           PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SAL_LBTR
                     AND DET.COD_SIS_SAL = SIS.COD_SIS_ENT_SAL
                     AND DET.COD_EST_AOS IN
                              (PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEAUT,
                               PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPELIB)
                     AND NVL (DET.COD_TPO_OPE_AOS, '#') =
                           NVL (v_TIPO_OP, NVL (DET.COD_TPO_OPE_AOS, '#')) -- TIPO OPERACION ALTOS MONTOS
                     AND NVL (DET.COD_BCO_DTN, '#') =
                           NVL (v_BCO_DTN, NVL (DET.COD_BCO_DTN, '#')) -- BANCO DESTINO
                     AND NVL (DET.COD_MT_SWF, 0) =
                           NVL (v_MT_SWIFT, NVL (DET.COD_MT_SWF, 0)) -- MT SWIFT
                     AND DET.IMp_OPE BETWEEN v_IMp_DSD AND v_IMp_HST -- MONTOS
                     AND NVL (DET.NUM_FOL_NMN, 0) =
                           NVL (v_NUM_NOM, NVL (DET.NUM_FOL_NMN, 0))
                     AND DET.COD_SIS_ENT = NVL (v_COD_ENT, DET.COD_SIS_ENT)
                     AND DET.COD_SIS_SAL = NVL (v_COD_SAL, DET.COD_SIS_SAL)
                     AND DET.NUM_FOL_OPE LIKE
                           NVL (v_NUM_OPE, DET.NUM_FOL_OPE) || '%'
                     AND UPPER (DET.NOM_BFC) LIKE
                           '%' || NVL (UPPER (p_NOM_BFC), DET.NOM_BFC) || '%'
                     AND TRIM (DETOPC.NUM_DOC_ODN)
                        || UPPER (DETOPC.COD_TPD_ODN) =
                           NVL (
                              v_NUM_DCO_ORD,
                              TRIM (DETOPC.NUM_DOC_ODN) || DETOPC.COD_TPD_ODN
                           )
                     AND DET.NUM_FOL_OPE = DETOPC.NUM_FOL_OPE(+);
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            err_code := SQLCODE;
            err_msg :=
                  'No se encuentran registros para tipo operacion '
               || p_TIPO_OP
               || '- mensaje swift'
               || p_MT_SWIFT;
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         WHEN OTHERS
         THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            p_s_mensaje := err_code || '-' || err_msg;
            --PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
      END;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg :=
               'No se encuentran registros para tipo operacion '
            || p_TIPO_OP
            || '- mensaje swift'
            || p_MT_SWIFT;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         p_s_mensaje := err_code || '-' || err_msg;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END Sp_PAB_BUS_OPE_CONT_LBTR;



   --************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_ING_ARCH_CGE
   -- Objetivo: Inserta acrchivo generados y por descargar de las contingencias combanc
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas:
   -- Fecha: 13/11/2017
   -- Autor: Santander
   -- Input:
   -- p_ARC_CGE_ECD -> nombre beneficiario
   -- Output:
   -- p_ERROR       -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores
   -- Retorno: N/A.
   -- Observaciones:
   --***********************************************************************************************
   PROCEDURE Sp_PAB_ING_ARCH_CGE (p_ARC_CGE_ECD IN CLOB, p_ERROR OUT NUMBER)
   IS
      --Seteo de Variables
      v_NOM_SP     VARCHAR2 (30) := 'Sp_PAB_ING_ARCH_CGE';
      v_NOM_ARCH   VARCHAR2 (30) := TO_CHAR (SYSDATE, 'YYYYMMDDHHMMSS');
   BEGIN
      INSERT INTO PABS_DT_ARCHV_CNTGC_COBAC (NUM_CRV_ARC_CGE,
                                             FEC_ARC_CGE,
                                             NOM_ARC_CGE,
                                             FLG_ARC_CGE,
                                             COD_USR_CRC_CGE,
                                             COD_USR_DGA_CGE,
                                             FEC_DGA_ARC_CGE,
                                             ARC_CGE_ECD)
        VALUES   (SPK_PAB_COD_ARC_CONT_COMBANC.NEXTVAL,
                  SYSDATE,
                  'AIPAC' || v_NOM_ARCH,
                  0,
                  NULL,
                  NULL,
                  NULL,
                  p_ARC_CGE_ECD);

      p_ERROR := PKG_PAB_CONSTANTES.v_OK;
   EXCEPTION
      WHEN DUP_VAL_ON_INDEX
      THEN
         err_code := SQLCODE;
         err_msg :=
            SUBSTR (SQLERRM, 1, 300)
            || ' No se pudo generar la inserción del registro ';
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         p_s_mensaje := err_code || '-' || err_msg;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END Sp_PAB_ING_ARCH_CGE;

   --************************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_BUS_VVV_CTG_PLZ
   -- Objetivo: Procedimiento que busca los vales vista virtuales con fecha valuta hoy, de los canales de entrada de OFFICE BANKING y CAJA
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_DETLL_OPRCN
   -- Fecha: 02/06/16
   -- Autor: Santander
   -- Input:    p_NUM_FOL_NMN->   foliador del banco
   --           p_FEC_ISR_OPE->   Fecha de insercion de la operacion
   --           p_COD_EST_AOS->   Identificador de nuevo estado.
   --           p_COD_USR->       rut usuario
   -- Output:
   --        p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE SP_PAB_BUS_VVV_CTG_PLZ (p_FECHA_DESDE       DATE,
                                     p_FECHA_HASTA       DATE,
                                     p_CURSOR        OUT SYS_REFCURSOR,
                                     p_ERROR         OUT NUMBER)
   IS
      --Seteo de Variables
      v_NOM_SP        VARCHAR2 (30) := 'SP_PAB_BUS_VVV_CTG_PLZ';
      v_FECHA_DESDE   DATE := TRUNC (SYSDATE);
      v_FECHA_HASTA   DATE := TRUNC (SYSDATE);
   BEGIN
      p_ERROR := 0;

      IF p_FECHA_DESDE IS NOT NULL
      THEN
         v_FECHA_DESDE := p_FECHA_DESDE;
      END IF;

      IF p_FECHA_HASTA IS NOT NULL
      THEN
         v_FECHA_HASTA := p_FECHA_HASTA;
      END IF;

      BEGIN
         OPEN p_CURSOR FOR
            SELECT   OPR.NUM_OPE_SIS_ENT
              FROM   PABS_DT_DETLL_OPRCN OPR
             WHERE   OPR.FEC_VTA BETWEEN TRUNC (v_FECHA_DESDE)
                                     AND  TRUNC (v_FECHA_HASTA)
                     AND COD_SIS_ENT IN
                              (DBO_PAB.PKG_PAB_CONSTANTES.V_COD_TIP_OB,
                               DBO_PAB.PKG_PAB_CONSTANTES.V_COD_TIP_CAJA)
            UNION ALL
            SELECT   OPR_ITR.NUM_OPE_SIS_ENT_ITR
              FROM   PABS_DT_DETLL_OPRCN_INTER OPR_ITR
             WHERE   TRUNC (OPR_ITR.FEC_ACT_OPE_ITR) BETWEEN TRUNC (
                                                                v_FECHA_DESDE
                                                             )
                                                         AND  TRUNC(v_FECHA_HASTA)
                     AND OPR_ITR.COD_SIS_ENT_ITR IN
                              (DBO_PAB.PKG_PAB_CONSTANTES.V_COD_TIP_OB,
                               DBO_PAB.PKG_PAB_CONSTANTES.V_COD_TIP_CAJA);
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            err_code := SQLCODE;
            err_msg :=
               'No se encuentran registros de vales vista virtuales para hoy';
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         WHEN OTHERS
         THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            p_s_mensaje := err_code || '-' || err_msg;
            --PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
      END;
   EXCEPTION
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         p_s_mensaje := err_code || '-' || err_msg;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END SP_PAB_BUS_VVV_CTG_PLZ;
END PKG_PAB_CONTINGENCIA;
/


GRANT EXECUTE ON DBO_PAB.PKG_PAB_CONTINGENCIA TO R_APP_PAB
/

GRANT EXECUTE ON DBO_PAB.PKG_PAB_CONTINGENCIA TO USR_PAB
/

