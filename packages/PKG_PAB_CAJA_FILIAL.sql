CREATE OR REPLACE /* Formatted on 18-08-2020 17:45:28 (QP5 v5.115.810.9015) */
PACKAGE DBO_PAB.PKG_PAB_CAJA_FILIAL
IS
   --
   v_NOM_PCK               VARCHAR2 (30) := 'PKG_PAB_CAJA_FILIAL';
   ERR_CODE                NUMBER := 0;
   ERR_MSG                 VARCHAR2 (300);
   v_DES                   VARCHAR2 (300);
   v_DES_BIT               VARCHAR2 (100);
   v_BANCO                 VARCHAR2 (9) := '970150005';
   p_s_mensaje             VARCHAR2 (400);
   TOPE_MSJ_BLQ            NUMBER := 300;
   V_FECHA_CUR             DATE := TRUNC (SYSDATE);
   v_dia_ini               DATE := TRUNC (SYSDATE) + INTERVAL '1' MINUTE;
   v_dia_fin DATE
         := TRUNC (SYSDATE) + INTERVAL '23' HOUR + INTERVAL '59' MINUTE ;

   --
   CURSOR C_FILIALES_PROYECCION (
      P_NUM_CTA_CTE                 VARCHAR2,
      P_NUM_DOC_FIL                 CHAR
   )
   IS
        SELECT   NUM_DOC_FIL, NUM_CTA_CTE
          FROM   PABS_DT_MVNTO_FLIAL
         WHERE       FEC_VTA_FIL >= V_FECHA_CUR
                 AND NUM_DOC_FIL = NVL (P_NUM_DOC_FIL, NUM_DOC_FIL)
                 AND NUM_CTA_CTE = NVL (P_NUM_CTA_CTE, NUM_CTA_CTE)
      GROUP BY   NUM_DOC_FIL, NUM_CTA_CTE;

   -------------------------------
   R_FILIALES_PROYECCION   C_FILIALES_PROYECCION%ROWTYPE;

   ----------------------------

   /************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_INS_PYN_MNT_FIL
   -- Objetivo: Procedimiento que inserta movimientos de filiales de WS TIBCO al cargar n�mina
   -- de proyeccci�n de caja filiales
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_MVNTO_FLIAL
   -- Fecha: 20/09/17
   -- Autor: Santander
   -- Input:
   -- p_NUM_RUT_FIL  -> Rut Filial
   -- p_NUM_FOL_NMN  -> Folio N�mina
   -- p_COD_FIL_AOS  -> C�digo Filial
   -- p_NUM_CTA_CTE  -> N�mero de Cuenta Corriente
   -- p_COD_DVI      -> Moneda
   -- p_FEC_VTA_FIL  -> Fecha Valuta
   -- p_FLAG_FLJ_FIL -> Flag indicador Egreso o ingreso
   -- p_IMP_MONTO    -> Monto
   -- p_COD_CNP      -> C�digo Concepto
   -- p_GLS_CNP      -> Glosa  Concepto
   -- p_OBS_CNP      -> Observaci�n Concepto
   -- p_COD_USR      -> Usuario Tibco
   -- p_COD_USR_SIS  -> Usuario Sistema
   -- p_COD_SIS_ENT  -> Canal
   -- Output:
   -- p_NUM_FOL_OPE  -> Folio Operaci�n
   -- p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   -- GSD - OK
   PROCEDURE Sp_PAB_INS_PYN_MNT_FIL (p_NUM_FOL_NMN    IN     NUMBER,
                                     p_NUM_RUT_FIL    IN     VARCHAR,
                                     p_COD_FIL_AOS    IN     VARCHAR,
                                     p_NUM_CTA_CTE    IN     VARCHAR,
                                     p_COD_DVI        IN     VARCHAR,
                                     p_FEC_VTA_FIL    IN     DATE,
                                     p_FLAG_FLJ_FIL   IN     VARCHAR,
                                     p_IMP_MONTO      IN     NUMBER,
                                     p_COD_CNP        IN     VARCHAR,
                                     p_GLS_CNP        IN     VARCHAR,
                                     p_OBS_CNP        IN     VARCHAR,
                                     p_COD_USR        IN     VARCHAR,
                                     p_COD_USR_SIS    IN     VARCHAR,
                                     p_COD_SIS_ENT    IN     VARCHAR,
                                     p_NUM_FOL_OPE       OUT NUMBER,
                                     p_ERROR             OUT NUMBER);

   --**********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_PYN_RSM_CJA_FIL
   -- Objetivo: Retorna el resumen de proyecciones de caja filiales del d�a
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_ENCAB_CRTLA_FLIAL, PABS_DT_CAJA_FLIAL
   -- Fecha: 27/09/17
   -- Autor: Santander
   -- Input:
   -- N/A
   -- Output:
   -- p_CURSOR ->  Lista los filiales caja flilales
   -- p_ERROR  ->  Indicada si el procedimiento
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de utimos cambios>
   --***********************************************************************************************
   --
   PROCEDURE SP_PAB_PYN_RSM_CJA_FIL (p_CURSOR   OUT SYS_REFCURSOR,
                                     p_ERROR    OUT NUMBER);



   --**********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_PYN_DELL_CJA_FIL
   -- Objetivo: Retorna el detalle de proyecciones de caja filiales
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_ENCAB_CRTLA_FLIAL, PABS_DT_CAJA_FLIAL
   -- Fecha: 27/09/17
   -- Autor: Santander
   -- Input:
   -- p_N_CTA    -> N�nmero cuenta
   -- p_NUM_DOC  -> Numero de documento de filial
   -- Output:
   -- p_CURSOR ->  Lista los filiales caja flilales
   -- p_ERROR  ->  Indicada si el procedimiento
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de utimos cambios>
   --***********************************************************************************************
   PROCEDURE SP_PAB_PYN_DELL_CJA_FIL (p_N_CTA         IN     NUMBER,
                                      p_NUM_DOC       IN     CHAR,
                                      P_CABECERA         OUT SYS_REFCURSOR,
                                      P_MOVIMIENTOS      OUT SYS_REFCURSOR,
                                      p_ERROR            OUT NUMBER);


   --**********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_PYN_ELI_CJA_FIL
   -- Objetivo: Elimina registro de proyecciones de caja filiales seg�n n�mero de operaci�n y su respectiva n�mina
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_ENCAB_CRTLA_FLIAL, PABS_DT_CAJA_FLIAL
   -- Fecha: 27/09/17
   -- Autor: Santander
   -- Input:
   -- p_NUM_FOL_NMN  -> N�nmero Folio
   -- p_NUM_FOL_OPE  -> N�mero Operaci�n
   -- p_FEC_ING_MVT  -> Fecha Ingreso Movimiento
   -- Output:
   -- p_CURSOR ->  Lista los filiales caja flilales
   -- p_ERROR  ->  Indicada si el procedimiento
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de utimos cambios>
   --***********************************************************************************************
   PROCEDURE SP_PAB_PYN_ELI_CJA_FIL (p_NUM_FOL_NMN   IN     NUMBER,
                                     p_NUM_FOL_OPE   IN     NUMBER,
                                     p_FEC_ING_MVT   IN     DATE,
                                     p_ERROR            OUT NUMBER);


   --**********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_PYN_CTL_CJA_FIL
   -- Objetivo: Retorna cartola de proyecciones de caja filiales del d�a
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_ENCAB_CRTLA_FLIAL, PABS_DT_CAJA_FLIAL
   -- Fecha: 27/09/17
   -- Autor: Santander
   -- Input:
   -- N/A
   -- Output:
   -- p_CURSOR ->  Lista los filiales caja flilales
   -- p_ERROR  ->  Indicada si el procedimiento
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de utimos cambios>
   --***********************************************************************************************
   PROCEDURE SP_PAB_PYN_CTL_CJA_FIL (p_CURSOR   OUT SYS_REFCURSOR,
                                     p_ERROR    OUT NUMBER);


   --**********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_PYN_DELL_CTL_FIL
   -- Objetivo: Retorna el detalle de proyecciones de caja filiales
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_ENCAB_CRTLA_FLIAL, PABS_DT_CAJA_FLIAL
   -- Fecha: 27/09/17
   -- Autor: Santander
   -- Input:
   -- p_N_CTA        -> N�nmero cuenta
   -- p_NUM_EPS_FIL  -> N�mero empresa filial
   -- Output:
   -- p_CURSOR ->  Lista los filiales caja flilales
   -- p_ERROR  ->  Indicada si el procedimiento
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de utimos cambios>
   --***********************************************************************************************
   PROCEDURE SP_PAB_PYN_DELL_CTL_FIL (p_N_CTA         IN     VARCHAR2,
                                      p_NUM_DOC_FIL   IN     CHAR,
                                      p_CABECERA         OUT SYS_REFCURSOR,
                                      p_DETALLE          OUT SYS_REFCURSOR,
                                      p_ERROR            OUT NUMBER);


   --**********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_CNA_CJA_FIL
   -- Objetivo: Retorna datos de las empresas filiales de Santander.
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_CAJA_FLIAL
   -- Fecha: 04/10/17
   -- Autor: Santander
   -- Input:
   -- N/A
   -- Output:
   -- p_CURSOR ->  Lista los filiales caja flilales
   -- p_ERROR  ->  Indicada si el procedimiento
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de utimos cambios>
   --***********************************************************************************************POK
   PROCEDURE SP_PAB_CNA_CJA_FIL (p_CURSOR   OUT SYS_REFCURSOR,
                                 p_ERROR    OUT NUMBER);



   --************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_INS_CBCRA_CRTLA_FLIAL
   -- Objetivo: Procedimiento que inserta cabecera de Cartola Filiales desde WS Tibco desde la MW157 y 110.
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_ENCAB_CRTLA_FLIAL, PABS_DT_CAJA_FLIAL
   -- Fecha: 17/10/17
   -- Autor: Santander
   -- Input:
   -- p_NUM_DOC_FIL  -> Rut
   -- p_NUM_CTA_CTE  -> Cuenta
   -- p_COD_DVI      -> Divisa
   -- p_FEC_CTL_CTA  -> Fecha Cartola
   -- p_GLS_TPO_CTL  -> Tipo Cartola
   -- p_FEC_INI_CTL  -> Fecha de inicio
   -- p_FEC_FNL_CTL  -> Fecha Final
   -- p_NUM_CTL_FIL  -> N�mero Cartola
   -- p_NUM_MVT_CTL  -> N�mero Movimiento
   -- p_IMP_INI_CTL  -> Saldo Incial
   -- p_IMP_TOT_ING  -> Saldo Total Ingreso
   -- p_IMP_TOT_EGR  -> Saldo Total Egreso
   -- p_IMP_FNL_CTL  -> Saldo Final
   -- p_IMP_RTN_CTL  -> Saldo Retenci�n
   -- p_IMP_LCD_CTL  -> Saldo L�nea
   -- p_IMP_LCD_USO  -> Saldo L�nea Uso
   -- p_IMP_LCD_DIP  -> Saldo L�nea Disponible
   -- p_IMP_TOT_DIP  -> Saldo Disponible
   -- p_ERROR
   -- Output:
   -- p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE Sp_PAB_INS_CBCRA_CRTLA_FLIAL (p_NUM_DOC_FIL   IN     VARCHAR2,
                                           p_NUM_CTA_CTE   IN     VARCHAR2,
                                           p_COD_DVI       IN     VARCHAR2,
                                           p_FEC_CTL_CTA   IN     DATE,
                                           p_GLS_TPO_CTL   IN     VARCHAR2,
                                           p_FEC_INI_CTL   IN     DATE,
                                           p_FEC_FNL_CTL   IN     DATE,
                                           p_NUM_CTL_FIL   IN     NUMBER,
                                           p_NUM_MVT_CTL   IN     NUMBER,
                                           p_IMP_INI_CTL   IN     NUMBER,
                                           p_IMP_TOT_ING   IN     NUMBER,
                                           p_IMP_TOT_EGR   IN     NUMBER,
                                           p_IMP_FNL_CTL   IN     NUMBER,
                                           p_IMP_RTN_CTL   IN     NUMBER,
                                           p_IMP_LCD_CTL   IN     NUMBER,
                                           p_IMP_LCD_USO   IN     NUMBER,
                                           p_IMP_LCD_DIP   IN     NUMBER,
                                           p_IMP_TOT_DIP   IN     NUMBER,
                                           p_ERROR            OUT NUMBER);


   --************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_INS_DETLL_CRTLA_FLIAL
   -- Objetivo: Procedimiento que inserta el detalle de cartola.
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_DETLL_CRTLA_FLIAL, PABS_DT_CAJA_FLIAL
   -- Fecha: 23/10/17
   -- Autor: Santander
   -- Input:
   -- P_NUM_CTL_FIL        -> N�mero de cartola
   -- p_FEC_CTL_CTA        -> Fecha de Cartola
   -- p_NUM_CTA_CTE        -> N�mero de cuenta
   -- p_NUM_MVT_FIL        -> N�mero de Movimiento
   -- p_NUM_DOC_FIL        -> Rut Filial
   -- p_CANT_MOV           -> Cantidad de Movimientos
   -- p_XML_MOV            -> XML con detalle de los movimientos
   -- Output:
   --        p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE Sp_PAB_INS_DETLL_CRTLA_FLIAL (P_NUM_CTL_FIL   IN     NUMBER,
                                           p_NUM_CTA_CTE   IN     VARCHAR2,
                                           p_FEC_CTL_CTA   IN     DATE,
                                           p_NUM_DOC_FIL   IN     CHAR,
                                           p_NUM_MVT_FIL   IN     NUMBER,
                                           p_CANT_MOV      IN     NUMBER,
                                           p_XML_MOV       IN     CLOB,
                                           p_ERROR            OUT NUMBER);


   --**********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_ELI_DETLL_CRTLA
   -- Objetivo: Elimina detalles de cartolas previo la inserci�n de la carga enviada por TIBCO al comienzo del d�a
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_DETLL_CRTLA_FLIAL
   -- Fecha: 18/10/2017
   -- Autor: Santander
   -- Input:
   -- p_NUM_RUT_FIL
   -- p_NUM_CTA_CTE
   -- Output:
   -- p_ERROR  ->  Indicada si el procedimiento
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de utimos cambios>
   --***********************************************************************************************
   -- GSD
   PROCEDURE SP_PAB_ELI_DETLL_CRTLA (p_NUM_RUT_FIL   IN     VARCHAR,
                                     p_NUM_CTA_CTE   IN     VARCHAR,
                                     p_ERROR            OUT NUMBER);

   --************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_ACT_CRTLA_FLIAL
   -- Objetivo: Procedimiento que actualiza los montos de egreso e ingreso seg�n la tabla de movimientos de proyecci�n filiales.
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_MVNTO_FLIAL, PABS_DT_ENCAB_CRTLA_FLIAL
   -- Fecha: 17/10/17
   -- Autor: Santander
   -- Input:
   -- p_NUM_CTA_CTE -> N�mero de cuenta corriente
   -- Output:
   -- p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   -- Pendiente de An�lisis con Claudio F.
   PROCEDURE Sp_PAB_ACT_CRTLA_FLIAL (p_NUM_CTL_FIL   IN     NUMBER,
                                     p_NUM_CTA_CTE   IN     VARCHAR2,
                                     p_FEC_CTL_CTA   IN     DATE,
                                     p_NUM_DOC_FIL   IN     CHAR,
                                     p_ERROR            OUT NUMBER);



   --**********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_ACT_CAB_PYN_FIL
   -- Objetivo: Actualiza los saldos de la cabecera de filiales cuando se realiza una carga de nomina
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_ENCAB_CRTLA_FLIAL
   -- Fecha: 27/09/17
   -- Autor: Santander
   -- Input:
   -- N/A
   -- Output:
   -- p_NUM_RUT_FIL -> N�mero Rut Filiales
   -- p_NUM_CTA_CTE -> N�mero Cta Corriente
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de utimos cambios>
   --***********************************************************************************************
   PROCEDURE SP_PAB_ACT_CAB_PYN_FIL (p_NUM_RUT_FIL   IN     VARCHAR,
                                     p_NUM_CTA_CTE   IN     VARCHAR,
                                     p_NUM_FOL_NMN   IN     NUMBER,
                                     p_ERROR            OUT NUMBER);
END PKG_PAB_CAJA_FILIAL;
/


CREATE OR REPLACE /* Formatted on 18-08-2020 17:45:28 (QP5 v5.115.810.9015) */
PACKAGE BODY DBO_PAB.PKG_PAB_CAJA_FILIAL
IS

/************************************************************************************************
-- Funcion/Procedimiento: Sp_PAB_INS_PYN_MNT_FIL
-- Objetivo: Procedimiento que inserta movimientos de filiales de WS TIBCO al cargar n�mina
-- de proyeccci�n de caja filiales 
-- Sistema: DBO_PAB.
-- Base de Datos: DBO_PAB.
-- Tablas Usadas: PABS_DT_MVNTO_FLIAL
-- Fecha: 20/09/17
-- Autor: Santander
-- Input:
-- p_NUM_RUT_FIL  -> Rut Filial
-- p_NUM_FOL_NMN  -> Folio N�mina 
-- p_COD_FIL_AOS  -> C�digo Filial
-- p_NUM_CTA_CTE  -> N�mero de Cuenta Corriente
-- p_COD_DVI      -> Moneda
-- p_FEC_VTA_FIL  -> Fecha Valuta
-- p_FLAG_FLJ_FIL -> Flag indicador Egreso o ingreso
-- p_IMP_MONTO    -> Monto
-- p_COD_CNP      -> C�digo Concepto 
-- p_GLS_CNP      -> Glosa  Concepto
-- p_OBS_CNP      -> Observaci�n Concepto 
-- p_COD_USR      -> Usuario Tibco
-- p_COD_USR_SIS  -> Usuario Sistema
-- p_COD_SIS_ENT  -> Canal
-- Output:
-- p_NUM_FOL_OPE  -> Folio Operaci�n 
-- p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
-- Input/Output: N/A
-- Retorno: N/A.
-- Observaciones: <Fecha y Detalle de ultimos cambios>
--***********************************************************************************************/
-- GSD Revisar con Front
PROCEDURE Sp_PAB_INS_PYN_MNT_FIL  (
                                p_NUM_FOL_NMN  IN NUMBER,
                                p_NUM_RUT_FIL  IN VARCHAR,
                                p_COD_FIL_AOS  IN VARCHAR,
                                p_NUM_CTA_CTE  IN VARCHAR,
                                p_COD_DVI      IN VARCHAR,
                                p_FEC_VTA_FIL  IN DATE,
                                p_FLAG_FLJ_FIL IN VARCHAR,
                                p_IMP_MONTO    IN NUMBER,
                                p_COD_CNP      IN VARCHAR,
                                p_GLS_CNP      IN VARCHAR,
                                p_OBS_CNP      IN VARCHAR,
                                p_COD_USR      IN VARCHAR,
                                p_COD_USR_SIS  IN VARCHAR,
                                p_COD_SIS_ENT  IN VARCHAR,
                                p_NUM_FOL_OPE  OUT NUMBER,
                                p_ERROR        OUT NUMBER)
    IS
     --Seteo de Variables
        v_NOM_SP      VARCHAR2 (30) := 'Sp_PAB_INS_PYN_MNT_FIL';
        v_IMP_ING_FIL NUMBER(15,2)  := 0;
        v_IMP_EGR_FIL NUMBER(15,2)  := 0;
        v_GLS_MVT_CTL VARCHAR2 (60) := 'Carga Masiva Proyecci�n Filiales';
        v_NUM_RUT     CHAR(11);
        v_Dv_RUT      VARCHAR2(1);
        v_NUM_FOL_OPE NUMBER (12)  := SPK_PAB_OPE_CJA_AM.NEXTVAL;
        v_FEC_ING_MVT TIMESTAMP;
        v_FECHA_HOY      DATE;
        v_FECHA_24       DATE;
        v_FECHA_48       DATE;
        err_valuta     exception;

    BEGIN

        --Asignamos monto seg�n flujo
        IF p_FLAG_FLJ_FIL = PKG_PAB_CONSTANTES.V_STR_FLG_EGR THEN
            v_IMP_EGR_FIL := p_IMP_MONTO;
        ELSE
            v_IMP_ING_FIL := p_IMP_MONTO;
        END IF;

        --Asignamos la fecha de hoy a la carga de ma n�mina
        v_FEC_ING_MVT := SYSTIMESTAMP;

        --Obtenemos la fecha de valuta a 48 Horas para saber cual es la maxima fecha que se puede ingresar.
        v_FECHA_HOY := TRUNC(SYSDATE);
        v_FECHA_24  := v_FECHA_HOY + 1;
        PKG_PAB_UTILITY.SP_PAB_DEV_DIA_HABIL_SIG(PKG_PAB_CONSTANTES.v_STR_CAL_CL,v_FECHA_24);
        v_FECHA_48 := v_FECHA_24 + 1;
        PKG_PAB_UTILITY.SP_PAB_DEV_DIA_HABIL_SIG(PKG_PAB_CONSTANTES.v_STR_CAL_CL,v_FECHA_48);

        --Validamos la fecha de valuta ingresada
        IF(p_FEC_VTA_FIL > v_FECHA_48) THEN

            err_code := SQLCODE;
            err_msg := 'La fecha ingresada:' || p_FEC_VTA_FIL || ' es mayor a 48 horas:' || v_FECHA_48 ;
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);

        ELSE

            BEGIN

                --Separamos Rut
                PKG_PAB_UTILITY.Sp_PAB_DESCOMp_RUT(p_NUM_RUT_FIL,v_NUM_RUT,v_Dv_RUT);


                --Inserta n�mina de proyecci�n de cajas filiales
                INSERT INTO PABS_DT_MVNTO_FLIAL(
                    NUM_FOL_NMN,
                    NUM_FOL_OPE,
                    NUM_DOC_FIL,
                    COD_TPO_FIL,
                    NUM_CTA_CTE,
                    FEC_ING_MVT,
                    COD_CNP_FIL,
                    NUM_EPS_FIL,
                    FEC_VTA_FIL,
                    IMP_ING_FIL,
                    IMP_EGR_FIL,
                    GLS_MVT_CTL,
                    GLS_SUC_TRN_CTL,
                    COD_DVI
                    )
                    VALUES (
                    p_NUM_FOL_NMN,
                    v_NUM_FOL_OPE,
                    v_NUM_RUT,
                    v_Dv_RUT,
                    p_NUM_CTA_CTE,
                    v_FEC_ING_MVT,
                    p_COD_CNP,
                    p_COD_FIL_AOS,
                    p_FEC_VTA_FIL,
                    v_IMP_ING_FIL,
                    v_IMP_EGR_FIL,
                    v_GLS_MVT_CTL,
                    p_OBS_CNP,
                    p_COD_DVI
                    );


            EXCEPTION
                WHEN DUP_VAL_ON_INDEX THEN
                    err_code := SQLCODE;
                    err_msg := SUBSTR(SQLERRM, 1, 300) || ' No se pudo generar la inserci�n del registro ' ;

                WHEN OTHERS THEN
                    err_code := SQLCODE;
                    err_msg := SUBSTR(SQLERRM, 1, 300);
                    p_s_mensaje := err_code || '-' || err_msg;
                    PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);
                    p_ERROR:= PKG_PAB_CONSTANTES.v_ERROR;
                    RAISE_APPLICATION_ERROR(-20000,p_s_mensaje);
            END;

        END IF;

        p_ERROR:= PKG_PAB_CONSTANTES.v_OK;
        p_NUM_FOL_OPE := v_NUM_FOL_OPE;

    EXCEPTION
        WHEN DUP_VAL_ON_INDEX THEN
                err_code := SQLCODE;
                err_msg := SUBSTR(SQLERRM, 1, 300);
                PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);
                p_ERROR:= PKG_PAB_CONSTANTES.v_ERROR;

        WHEN OTHERS THEN
            err_code := SQLCODE;
            err_msg := SUBSTR(SQLERRM, 1, 300);
            p_s_mensaje := err_code || '-' || err_msg;
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);
            p_ERROR:= PKG_PAB_CONSTANTES.v_ERROR;
            RAISE_APPLICATION_ERROR(-20000,p_s_mensaje);


    END Sp_PAB_INS_PYN_MNT_FIL;

--**********************************************************************************************
-- Funcion/Procedimiento: SP_PAB_PYN_RSM_CJA_FIL
-- Objetivo: Retorna el resumen de proyecciones de caja filiales del d�a
-- Sistema: PAB
-- Base de Datos: DGBMSEGDB01_NGALTM
-- Tablas Usadas: PABS_DT_ENCAB_CRTLA_FLIAL, PABS_DT_CAJA_FLIAL
-- Fecha: 27/09/17
-- Autor: Santander
-- Input:
-- N/A
-- Output:
-- p_CURSOR ->  Lista los filiales caja flilales
-- p_ERROR  ->  Indicada si el procedimiento
-- Input/Output: N/A
-- Retorno: N/A.
-- Observaciones: <Fecha y Detalle de utimos cambios>
--***********************************************************************************************
-- GSD - OK ACCESS FULL FEC_CTL_CTA
  PROCEDURE SP_PAB_PYN_RSM_CJA_FIL (
                                p_CURSOR            OUT SYS_REFCURSOR,
                                p_ERROR             OUT NUMBER)
   IS
    v_NOM_SP                    VARCHAR2 (30) := 'SP_PAB_PYN_RSM_CJA_FIL';
    v_FECHA_DIA                 DATE := TRUNC(SYSDATE);



    BEGIN

        OPEN p_CURSOR FOR
            SELECT
                CJAFL.GLS_EPS_FIL || ' ' || CJAFL.GLS_CTA_CTE NOM_FIAL ,
                CJAFL.NUM_EPS_FIL AS COD_EMP,
                BAI.TGCDSWSA AS BANCO,
                BAI.DES_BCO AS DES_BCO,
                CJAFL.NUM_CTA_CTE AS N_CTA,
                CRTFL.NUM_LCD_CTA AS N_LCRED,
                CJAFL.COD_DVI     AS MONEDA,
                CRTFL.IMP_TOT_DIA AS M_INTRADIA,
                CRTFL.IMP_TOT_24  AS M_SALDO24,
                CRTFL.IMP_TOT_48  AS M_SALDO48,
                CJAFL.NUM_EPS_FIL AS N_EPS_FIL,
                CJAFL.NUM_DOC_FIL AS NUM_DOC
            FROM
                PABS_DT_CAJA_FLIAL CJAFL,
                PABS_DT_ENCAB_CRTLA_FLIAL CRTFL,
                TCDTBAI BAI
            WHERE
                CJAFL.NUM_DOC_FIL = CRTFL.NUM_DOC_FIL AND
                CRTFL.NUM_CTA_CTE = CJAFL.NUM_CTA_CTE AND
                --CRTFL.COD_TPO_FIL = CJAFL.COD_TPO_FIL AND
                CJAFL.COD_BIC_BCO = BAI.TGCDSWSA AND
                --Verificamos que tenga a lo menos un movimientos para mostrar la proyeccci�n
                CJAFL.NUM_CTA_CTE IN (
                    SELECT DISTINCT MVTO.NUM_CTA_CTE
                    FROM PABS_DT_MVNTO_FLIAL MVTO
                    WHERE MVTO.NUM_CTA_CTE = NUM_CTA_CTE AND
                    MVTO.FEC_VTA_FIL >= v_FECHA_DIA AND
                    MVTO.FEC_ING_MVT BETWEEN v_dia_ini AND v_dia_fin)
                    ;-- <= v_FECHA_DIA);


            p_ERROR :=  PKG_PAB_CONSTANTES.v_OK;
                                                                                                                                                                                                                                                                                                                                                                                                                                  --Procedimiento ejecutado de forma correcta
    EXCEPTION
    WHEN NO_DATA_FOUND
        THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300) || ' No se encontr� datos ';
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
    WHEN OTHERS
        THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR(-20000, p_s_mensaje);

    END SP_PAB_PYN_RSM_CJA_FIL;

--**********************************************************************************************
-- Funcion/Procedimiento: SP_PAB_PYN_DELL_CJA_FIL
-- Objetivo: Retorna el detalle de proyecciones de caja filiales
-- Sistema: PAB
-- Base de Datos: DGBMSEGDB01_NGALTM
-- Tablas Usadas: PABS_DT_ENCAB_CRTLA_FLIAL, PABS_DT_CAJA_FLIAL
-- Fecha: 27/09/17
-- Autor: Santander
-- Input:
-- p_N_CTA    -> N�nmero cuenta
-- p_NUM_DOC  -> Numero de documento de filial
-- Output:
-- p_CURSOR ->  Lista los filiales caja flilales
-- p_ERROR  ->  Indicada si el procedimiento
-- Input/Output: N/A
-- Retorno: N/A.
-- Observaciones: <Fecha y Detalle de utimos cambios>
--***********************************************************************************************
  PROCEDURE SP_PAB_PYN_DELL_CJA_FIL (
                                p_N_CTA       IN NUMBER,
                                p_NUM_DOC     IN CHAR,
                                P_CABECERA          OUT SYS_REFCURSOR,
                                P_MOVIMIENTOS       OUT SYS_REFCURSOR,
                                p_ERROR             OUT NUMBER)
    IS
    v_NOM_SP        VARCHAR2 (30) := 'SP_PAB_PYN_DELL_CJA_FIL';
    v_hoy           date:= TRUNC(SYSDATE);

    BEGIN

        BEGIN
            OPEN P_CABECERA FOR
                SELECT TRIM(CJAFL.NUM_DOC_FIL) || '-' || CJAFL.COD_TPO_FIL AS RUT_FIAL,
                       CJAFL.GLS_EPS_FIL || ' ' || CJAFL.GLS_CTA_CTE AS NOM_FIAL,
                       CJAFL.NUM_CTA_CTE AS N_CTA,
                       CJAFL.GLS_CTA_CTE AS D_CTA,
                       BAI.DES_BCO       AS BANCO,
                       CJAFL.NUM_EPS_FIL AS COD_EMP,
                       CRTFL.IMP_TOT_DIA AS M_INTRA,
                       CRTFL.IMP_TOT_24  AS M_SDO24,
                       CRTFL.IMP_TOT_48  AS M_SDO48,
                       CJAFL.COD_DVI     AS MONEDA,
                       CRTFL.IMP_INI_CTL AS SDO_INI,
                       CRTFL.IMP_TOT_ING AS SDO_ING,
                       CRTFL.IMP_TOT_EGR AS SDO_EGR,
                       CRTFL.IMP_FNL_CTL AS SDO_CRR,
                       CRTFL.IMP_RTN_CTL AS SDO_RET,
                       CRTFL.IMP_TOT_DIP AS SDO_DIS,
                       CRTFL.IMP_LCD_CTL AS LCR_CTL,
                       CRTFL.IMP_LCD_USO AS LCR_USO,
                       CRTFL.IMP_LCD_DIP AS LCR_DIP
                FROM PABS_DT_CAJA_FLIAL CJAFL,
                     PABS_DT_ENCAB_CRTLA_FLIAL CRTFL,
                     TCDTBAI BAI
                WHERE CJAFL.NUM_DOC_FIL = CRTFL.NUM_DOC_FIL AND
                      CJAFL.NUM_CTA_CTE = CRTFL.NUM_CTA_CTE AND
                      CRTFL.NUM_CTA_CTE = p_N_CTA           AND
                      --CRTFL.COD_TPO_FIL = CJAFL.COD_TPO_FIL AND
                      CJAFL.COD_BIC_BCO = BAI.TGCDSWSA;


        EXCEPTION
            WHEN NO_DATA_FOUND
                THEN
                 err_code := SQLCODE;
                 err_msg := SUBSTR (SQLERRM, 1, 300) || 'No se puedo encontrar datos para la cuenta: ' || p_N_CTA ;
                 RAISE;
            WHEN OTHERS
                THEN
                 err_code := SQLCODE;
                 err_msg := SUBSTR (SQLERRM, 1, 300);
                 PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                     v_NOM_PCK,
                                                     err_msg,
                                                     v_NOM_SP);
                 p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
                 p_s_mensaje := err_code || '-' || err_msg;
                 RAISE_APPLICATION_ERROR(-20000, p_s_mensaje);
        END;

        BEGIN

            OPEN P_MOVIMIENTOS FOR
                SELECT
                       MVNTO.NUM_FOL_NMN AS N_NMN,
                       MVNTO.NUM_FOL_OPE AS N_OPE,
                       MVNTO.GLS_SUC_TRN_CTL AS OBSERVACION,
                       MVNTO.IMP_ING_FIL AS M_ING,
                       MVNTO.IMP_EGR_FIL AS M_EGRE,
                       MVNTO.FEC_ING_MVT AS FEC_ING,
                       MVNTO.FEC_VTA_FIL AS FEC_VAL,
                       CNPTO.DSC_CNP_FIL AS DSC_MVNTO
                FROM PABS_DT_MVNTO_FLIAL MVNTO,
                     PABS_DT_CNPTO_FLIAL CNPTO
                WHERE
                      MVNTO.NUM_DOC_FIL = p_NUM_DOC         AND
                      MVNTO.COD_CNP_FIL = CNPTO.COD_CNP_FIL AND  --
                      MVNTO.NUM_CTA_CTE = p_N_CTA AND
                      MVNTO.FEC_VTA_FIL >= v_hoy
                      AND MVNTO.FEC_ING_MVT BETWEEN v_dia_ini AND v_dia_fin;--<= v_hoy;

        EXCEPTION
                WHEN NO_DATA_FOUND
                    THEN
                     err_code := SQLCODE;
                     err_msg := SUBSTR (SQLERRM, 1, 300) || ' No se puedo encontrar datos para la cuenta: ' || p_N_CTA ;
                     RAISE;
                WHEN OTHERS
                    THEN
                     err_code := SQLCODE;
                     err_msg := SUBSTR (SQLERRM, 1, 300);
                     PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                         v_NOM_PCK,
                                                         err_msg,
                                                         v_NOM_SP);
                     p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
                     p_s_mensaje := err_code || '-' || err_msg;
                     RAISE_APPLICATION_ERROR(-20000, p_s_mensaje);

        END;

        p_ERROR := PKG_PAB_CONSTANTES.v_OK;
                                                                                                                                                                                                                                                                                                                                                                                                                               --Procedimiento ejecutado de forma correcta
    EXCEPTION
    WHEN NO_DATA_FOUND
        THEN
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
    WHEN OTHERS
        THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR(-20000, p_s_mensaje);

    END SP_PAB_PYN_DELL_CJA_FIL;



--**********************************************************************************************
-- Funcion/Procedimiento: SP_PAB_PYN_ELI_CJA_FIL
-- Objetivo: Elimina registro de proyecciones de caja filiales seg�n n�mero de operaci�n y su respectiva n�mina
-- Sistema: PAB
-- Base de Datos: DGBMSEGDB01_NGALTM
-- Tablas Usadas: PABS_DT_MVNTO_FLIAL
-- Fecha: 27/09/17
-- Autor: Santander
-- Input:
-- p_NUM_FOL_NMN  -> N�nmero Folio
-- p_NUM_FOL_OPE  -> N�mero Operaci�n
-- p_FEC_ING_MVT  -> Fecha Ingreso Movimiento
-- Output:
-- p_ERROR  ->  Indicada si el procedimiento
-- Input/Output: N/A
-- Retorno: N/A.
-- Observaciones: <Fecha y Detalle de utimos cambios>
--***********************************************************************************************
-- GSD
  PROCEDURE SP_PAB_PYN_ELI_CJA_FIL (
                                p_NUM_FOL_NMN          IN NUMBER,
                                p_NUM_FOL_OPE          IN NUMBER,
                                p_FEC_ING_MVT          IN DATE,
                                p_ERROR                OUT NUMBER)
   IS
    v_NOM_SP                    VARCHAR2 (30) := 'SP_PAB_PYN_ELI_CJA_FIL';
    v_NUM_RUT_FIL               VARCHAR2 (11);
    v_NUM_CTA_CTE               VARCHAR2 (25);
    v_hoy                       date:= TRUNC(SYSDATE);

    BEGIN

        BEGIN

            --
            SELECT NUM_DOC_FIL,NUM_CTA_CTE
            INTO v_NUM_RUT_FIL,v_NUM_CTA_CTE
            FROM PABS_DT_MVNTO_FLIAL MVNTO
            WHERE MVNTO.NUM_FOL_NMN = p_NUM_FOL_NMN
              AND MVNTO.NUM_FOL_OPE = p_NUM_FOL_OPE
              AND MVNTO.FEC_ING_MVT BETWEEN v_dia_ini AND v_dia_fin ;--<= v_hoy;

        EXCEPTION
            WHEN NO_DATA_FOUND
                THEN
                 err_code := SQLCODE;
                 err_msg := 'No se pudo eliminar la operaci�n:' || p_NUM_FOL_OPE || ' de la Nomina:' || p_NUM_FOL_NMN;
                 PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                     v_NOM_PCK,
                                                     err_msg,
                                                     v_NOM_SP);
                 p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            WHEN OTHERS
                THEN
                 err_code := SQLCODE;
                 err_msg := SUBSTR (SQLERRM, 1, 300);
                 PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                     v_NOM_PCK,
                                                     err_msg,
                                                     v_NOM_SP);
                 p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
                 p_s_mensaje := err_code || '-' || err_msg;
                 RAISE_APPLICATION_ERROR(-20000, p_s_mensaje);
        END;

        --Eliminamos el movimiento
        DELETE FROM PABS_DT_MVNTO_FLIAL MVNTO
        WHERE MVNTO.NUM_FOL_NMN = p_NUM_FOL_NMN AND
              MVNTO.NUM_FOL_OPE = p_NUM_FOL_OPE
              AND MVNTO.FEC_ING_MVT BETWEEN v_dia_ini AND v_dia_fin;--<= v_hoy;

         --Debemos calcular los montos
         SP_PAB_ACT_CAB_PYN_FIL(v_NUM_RUT_FIL,v_NUM_CTA_CTE,NULL,p_ERROR);

         p_ERROR := PKG_PAB_CONSTANTES.v_OK;

    EXCEPTION
    WHEN NO_DATA_FOUND
        THEN
         err_code := SQLCODE;
         err_msg := 'No se pudo eliminar la operaci�n:' || p_NUM_FOL_OPE || ' de la Nomina:' || p_NUM_FOL_NMN;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
    WHEN OTHERS
        THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR(-20000, p_s_mensaje);

    END SP_PAB_PYN_ELI_CJA_FIL;



--**********************************************************************************************
-- Funcion/Procedimiento: SP_PAB_PYN_CTL_CJA_FIL
-- Objetivo: Retorna cuentas corrientes de las filiales que gestiona el banco
-- Base de Datos: DGBMSEGDB01_NGALTM
-- Tablas Usadas: PABS_DT_ENCAB_CRTLA_FLIAL, PABS_DT_CAJA_FLIAL
-- Fecha: 27/09/17
-- Autor: Santander
-- Input:
-- N/A
-- Output:
-- p_CURSOR ->  Lista los filiales caja flilales
-- p_ERROR  ->  Indicada si el procedimiento
-- Input/Output: N/A
-- Retorno: N/A.
-- Observaciones: <Fecha y Detalle de utimos cambios>
--***********************************************************************************************
-- GSD - Pendiente Modelo de datos
  PROCEDURE SP_PAB_PYN_CTL_CJA_FIL (
                                p_CURSOR            OUT SYS_REFCURSOR,
                                p_ERROR             OUT NUMBER)
    IS
        v_NOM_SP            VARCHAR2 (30) := 'SP_PAB_PYN_CTL_CJA_FIL';
        v_fecha_hoy         DATE := TRUNC(SYSDATE);


    BEGIN

        OPEN p_CURSOR FOR

          SELECT
            CJAFL.GLS_EPS_FIL || ' ' || CJAFL.GLS_CTA_CTE  AS NOM_FIAL,
            CRTFL.NUM_CTA_CTE AS N_CTA,
            CRTFL.NUM_LCD_CTA AS N_LCRED,
            CJAFL.COD_DVI AS MONEDA,
            CRTFL.IMP_INI_CTL AS M_SDOINI,
            CRTFL.IMP_TOT_ING AS M_SDOING,
            CRTFL.IMP_TOT_EGR AS M_SDOEGR,
            CRTFL.IMP_FNL_CTL AS M_SDOCRR,
            CRTFL.IMP_RTN_CTL AS M_SDORET,
            CRTFL.IMP_LCD_DIP AS M_SDODIS,
            CRTFL.IMP_LCD_CTL AS LCR_CTL,
            CRTFL.IMP_LCD_USO AS LCR_USO,
            CRTFL.IMP_LCD_DIP AS LCR_DIP,
            CJAFL.NUM_DOC_FIL AS NUM_DOC_FIL
          FROM  PABS_DT_CAJA_FLIAL CJAFL,
                PABS_DT_ENCAB_CRTLA_FLIAL CRTFL
          WHERE
            CRTFL.NUM_DOC_FIL = CJAFL.NUM_DOC_FIL AND
            CRTFL.NUM_CTA_CTE = CJAFL.NUM_CTA_CTE;

        p_ERROR := PKG_PAB_CONSTANTES.v_OK;
    --                                                                                                                                                                                                                                                                                                                                                                                                                                --Procedimiento ejecutado de forma correcta
    EXCEPTION
    WHEN NO_DATA_FOUND
        THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300) || ' No se pudo encontrar datos en PABS_DT_CAJA_FLIAL ' ;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
    WHEN OTHERS
        THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR(-20000, p_s_mensaje);
    --
    END SP_PAB_PYN_CTL_CJA_FIL;

--**********************************************************************************************
-- Funcion/Procedimiento: SP_PAB_PYN_DELL_CTL_FIL
-- Objetivo: Retorna el detalle de proyecciones de caja filiales
-- Sistema: PAB
-- Base de Datos: DGBMSEGDB01_NGALTM
-- Tablas Usadas: PABS_DT_ENCAB_CRTLA_FLIAL, PABS_DT_CAJA_FLIAL, PABS_DT_DETLL_CRTLA_FLIAL
-- Fecha: 27/09/17
-- Autor: Santander
-- Input:
-- p_N_CTA        -> N�nmero cuenta
-- p_NUM_DOC_FIL  -> N�mero empresa filial
-- Output:
-- p_CURSOR ->  Retorna el encabezado cartola
-- p_CURSOR ->  Retorna el detalle de cartola
-- p_ERROR  ->  Indicada si el procedimiento
-- Input/Output: N/A
-- Retorno: N/A.
-- Observaciones: <Fecha y Detalle de utimos cambios>
--***********************************************************************************************
-- GSD - Pendiente Modelo Saldo / N Operaci�n
  PROCEDURE SP_PAB_PYN_DELL_CTL_FIL (
                                    p_N_CTA       IN VARCHAR2,
                                    p_NUM_DOC_FIL IN CHAR,
                                    p_CABECERA    OUT SYS_REFCURSOR,
                                    p_DETALLE     OUT SYS_REFCURSOR,
                                    p_ERROR       OUT NUMBER)
   IS
    v_NOM_SP            VARCHAR2 (30) := 'SP_PAB_PYN_DELL_CTL_FIL';
    v_MOV_FIL_MYR       CHAR(1):= '0';


    BEGIN

        BEGIN

            OPEN p_CABECERA FOR
                SELECT  CRTFL.NUM_DOC_FIL RUT_FIAL,--|| '-' || CRTFL.COD_TPO_FIL AS RUT_FIAL,
                    CJAFL.GLS_EPS_FIL || ' ' || CJAFL.GLS_CTA_CTE NOM_FIAL,
                    CRTFL.NUM_CTA_CTE AS N_CTA,
                    CJAFL.COD_DVI     AS MONEDA,
                    CJAFL.NUM_EPS_FIL AS COD_EMP,
                    CRTFL.NUM_SUC_ORG AS N_SUC,
                    CRTFL.FEC_INI_CTL AS FEC_INI,
                    CRTFL.FEC_FNL_CTL AS FEC_FIN,
                    CRTFL.FEC_CTL_CTA AS FEC_GEN,
                    CRTFL.NUM_MVT_CTL AS NUN_MOV,
                    CRTFL.GLS_TPO_CTL AS TIP_CTL,
                    CRTFL.NUM_CTL_FIL AS NUM_CTL,
                    CRTFL.IMP_INI_CTL AS SDO_INI,
                    CRTFL.IMP_TOT_ING AS SDO_ING,
                    CRTFL.IMP_TOT_EGR AS SDO_EGR,
                    CRTFL.IMP_FNL_CTL AS SDO_CRR,
                    CRTFL.IMP_RTN_CTL AS SDO_RET,
                    CRTFL.IMP_TOT_DIP AS SDO_DIS,
                    CRTFL.IMP_LCD_CTL AS LCR_CTL,
                    CRTFL.IMP_LCD_USO AS LCR_USO,
                    CRTFL.IMP_LCD_DIP AS LCR_DIP
                FROM
                        PABS_DT_CAJA_FLIAL CJAFL,
                        PABS_DT_ENCAB_CRTLA_FLIAL CRTFL
                WHERE CJAFL.NUM_DOC_FIL = p_NUM_DOC_FIL      AND
                      CJAFL.NUM_CTA_CTE = p_N_CTA            AND
                      CJAFL.NUM_DOC_FIL = CRTFL.NUM_DOC_FIL  AND
                      CJAFL.NUM_CTA_CTE = CRTFL.NUM_CTA_CTE;

        EXCEPTION
            WHEN NO_DATA_FOUND
                THEN
                 err_code := SQLCODE;
                 err_msg := SUBSTR (SQLERRM, 1, 300) || ' No se pudo encontrar datos para la cuenta: ' || p_N_CTA;
                 PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                     v_NOM_PCK,
                                                     err_msg,
                                                     v_NOM_SP);
                 p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            WHEN OTHERS
                THEN
                 err_code := SQLCODE;
                 err_msg := SUBSTR (SQLERRM, 1, 300);
                 PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                     v_NOM_PCK,
                                                     err_msg,
                                                     v_NOM_SP);
                 p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
                 p_s_mensaje := err_code || '-' || err_msg;
                 RAISE_APPLICATION_ERROR(-20000, p_s_mensaje);
        END;

        BEGIN

            OPEN p_DETALLE FOR

            SELECT
                DETLL.NUM_MVT_FIL     AS N_MVTO,
                DETLL.IMP_TRN_FIL     AS MONTO,
                DETLL.FEC_CTL_CTA     AS FEC_ING,
                DETLL.FEC_CTL_CTA     AS FEC_VAL,
                --DETLL.IMP_TRN_FIL     AS M_SDO,
                DETLL.IMP_NVO_SDO     AS M_SDO,
                DETLL.GLS_TRN_CTL     AS DSC_MVNTO,
                DETLL.GLS_SUC_TRN_CTL AS DSC_SUC
            FROM PABS_DT_DETLL_CRTLA_FLIAL DETLL
            WHERE
                DETLL.NUM_CTA_CTE = p_N_CTA AND
                DETLL.NUM_DOC_FIL = p_NUM_DOC_FIL
                AND DETLL.NUM_MVT_FIL >v_MOV_FIL_MYR;

        END;

        p_ERROR := PKG_PAB_CONSTANTES.v_OK;
                                                                                                                                                                                                                                                                                                                                                                                                                                  --Procedimiento ejecutado de forma correcta
    EXCEPTION
        WHEN NO_DATA_FOUND
            THEN
             err_code := SQLCODE;
             err_msg := SUBSTR (SQLERRM, 1, 300) || ' No se pudo encontrar datos para la cuenta: ' || p_N_CTA;
             PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                 v_NOM_PCK,
                                                 err_msg,
                                                 v_NOM_SP);
             p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;

        WHEN OTHERS
            THEN
             err_code := SQLCODE;
             err_msg := SUBSTR (SQLERRM, 1, 300);
             PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                 v_NOM_PCK,
                                                 err_msg,
                                                 v_NOM_SP);
             p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
             p_s_mensaje := err_code || '-' || err_msg;
             RAISE_APPLICATION_ERROR(-20000, p_s_mensaje);
    --
    END SP_PAB_PYN_DELL_CTL_FIL;

--************************************************************************************************
-- Funcion/Procedimiento: Sp_PAB_INS_CBCRA_CRTLA_FLIAL
-- Objetivo: Procedimiento que inserta cabecera de Cartola Filiales desde WS Tibco desde la MW157 y 110.
-- Sistema: DBO_PAB.
-- Base de Datos: DBO_PAB.
-- Tablas Usadas: PABS_DT_ENCAB_CRTLA_FLIAL, PABS_DT_CAJA_FLIAL
-- Fecha: 17/10/17
-- Autor: Santander
-- Input:
-- p_NUM_DOC_FIL  -> Rut
-- p_NUM_CTA_CTE  -> Cuenta
-- p_COD_DVI      -> Divisa
-- p_FEC_CTL_CTA  -> Fecha Cartola
-- p_GLS_TPO_CTL  -> Tipo Cartola
-- p_FEC_INI_CTL  -> Fecha de inicio
-- p_FEC_FNL_CTL  -> Fecha Final
-- p_NUM_CTL_FIL  -> N�mero Cartola
-- p_NUM_MVT_CTL  -> N�mero Movimiento
-- p_IMP_INI_CTL  -> Saldo Incial
-- p_IMP_TOT_ING  -> Saldo Total Ingreso
-- p_IMP_TOT_EGR  -> Saldo Total Egreso
-- p_IMP_FNL_CTL  -> Saldo Final
-- p_IMP_RTN_CTL  -> Saldo Retenci�n
-- p_IMP_LCD_CTL  -> Saldo L�nea
-- p_IMP_LCD_USO  -> Saldo L�nea Uso
-- p_IMP_LCD_DIP  -> Saldo L�nea Disponible
-- p_IMP_TOT_DIP  -> Saldo Total Disponible
-- p_ERROR
-- Output:
-- p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
-- Input/Output: N/A
-- Retorno: N/A.
-- Observaciones: <Fecha y Detalle de ultimos cambios>
--***********************************************************************************************/
--
PROCEDURE Sp_PAB_INS_CBCRA_CRTLA_FLIAL (
                                    p_NUM_DOC_FIL    IN VARCHAR2,
                                    p_NUM_CTA_CTE   IN VARCHAR2,
                                    p_COD_DVI        IN VARCHAR2,
                                    p_FEC_CTL_CTA    IN DATE,
                                    p_GLS_TPO_CTL    IN VARCHAR2,
                                    p_FEC_INI_CTL    IN DATE,
                                    p_FEC_FNL_CTL    IN DATE,
                                    p_NUM_CTL_FIL    IN NUMBER,
                                    p_NUM_MVT_CTL    IN NUMBER,
                                    p_IMP_INI_CTL    IN NUMBER,
                                    p_IMP_TOT_ING    IN NUMBER,
                                    p_IMP_TOT_EGR    IN NUMBER,
                                    p_IMP_FNL_CTL    IN NUMBER,
                                    p_IMP_RTN_CTL    IN NUMBER,
                                    p_IMP_LCD_CTL    IN NUMBER,
                                    p_IMP_LCD_USO    IN NUMBER,
                                    p_IMP_LCD_DIP    IN NUMBER,
                                    p_IMP_TOT_DIP   IN NUMBER,
                                    p_ERROR          OUT NUMBER)
    IS

    v_NOM_SP            VARCHAR2(30)    :=  'Sp_PAB_INS_CBCRA_CRTLA_FLIAL';
    v_EXISTE            NUMBER:= 0;
    v_NUM_EPS_FIL       CHAR(4);
    v_NUM_SUC_ORI       CHAR(4);
    v_NUM_RUT           CHAR(11);
    v_Dv_RUT            CHAR(1);
    v_NUM_LCD_CTA       CHAR(12);
    v_MOV_FIL_MYR       CHAR(1):= '0';

    BEGIN

        --Separamos Rut
        PKG_PAB_UTILITY.Sp_PAB_DESCOMp_RUT(p_NUM_DOC_FIL,v_NUM_RUT,v_Dv_RUT);

        BEGIN
                SELECT CJA.NUM_EPS_FIL, CJA.NUM_SUC_ORI, NUM_LCD_CTA
                INTO v_NUM_EPS_FIL ,v_NUM_SUC_ORI, v_NUM_LCD_CTA
                FROM PABS_DT_CAJA_FLIAL  CJA
                WHERE CJA.NUM_CTA_CTE = p_NUM_CTA_CTE
                  AND CJA.NUM_DOC_FIL = v_NUM_RUT;

            EXCEPTION
            WHEN NO_DATA_FOUND
                THEN
                 err_code := SQLCODE ;
                 err_msg := SUBSTR (SQLERRM, 1, 300) || 'No se encontr� registro para NUM_CTA_CTE: ' || p_NUM_CTA_CTE || ' NUM_DOC_FIL:' || v_NUM_RUT ;

            WHEN OTHERS
                THEN
                 err_code := SQLCODE ;
                 err_msg := SUBSTR (SQLERRM, 1, 300) || 'NUM_CTA_CTE: ' || p_NUM_CTA_CTE || 'NUM_DOC_FIL: ' || v_NUM_RUT ;
                 PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                     v_NOM_PCK,
                                                     err_msg,
                                                     v_NOM_SP);
                 p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
                 p_s_mensaje := err_code || '-' || err_msg;
                 RAISE_APPLICATION_ERROR(-20000, p_s_mensaje);
        END;


        BEGIN
                SELECT COUNT(1) INTO v_EXISTE
                FROM PABS_DT_ENCAB_CRTLA_FLIAL  CBCRA
                WHERE CBCRA.NUM_CTA_CTE = p_NUM_CTA_CTE
                 AND CBCRA.NUM_DOC_FIL = v_NUM_RUT;

            EXCEPTION
            WHEN NO_DATA_FOUND
                THEN
                 err_code := SQLCODE ;
                 err_msg := SUBSTR (SQLERRM, 1, 300) || 'No se encontr� registro para NUM_CTA_CTE: ' || p_NUM_CTA_CTE || ' NUM_DOC_FIL:' || v_NUM_RUT  ;
            WHEN OTHERS
                THEN
                 err_code := SQLCODE ;
                 err_msg := SUBSTR (SQLERRM, 1, 300) || 'NUM_CTA_CTE: ' || p_NUM_CTA_CTE || ' NUM_DOC_FIL:' || v_NUM_RUT   ;
                 PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                     v_NOM_PCK,
                                                     err_msg,
                                                     v_NOM_SP);
                 p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
                 p_s_mensaje := err_code || '-' || err_msg;
                 RAISE_APPLICATION_ERROR(-20000, p_s_mensaje);
        END;


        IF v_EXISTE <> 0 THEN

            --Actualizamos valores entregados desde Middleware CCCKC157
            UPDATE PABS_DT_ENCAB_CRTLA_FLIAL
                SET
                        FEC_CTL_CTA   =    p_FEC_CTL_CTA,
                        NUM_SUC_ORG   =    v_NUM_SUC_ORI,
                        GLS_TPO_CTL   =    p_GLS_TPO_CTL,
                        FEC_INI_CTL   =    p_FEC_INI_CTL,
                        FEC_FNL_CTL   =    p_FEC_FNL_CTL,
                        NUM_CTL_FIL   =    p_NUM_CTL_FIL,
                        NUM_MVT_CTL   =    p_NUM_MVT_CTL,
                        IMP_INI_CTL   =    p_IMP_INI_CTL,
                        IMP_TOT_ING   =    p_IMP_TOT_ING,
                        IMP_TOT_EGR   =    p_IMP_TOT_EGR,
                        IMP_FNL_CTL   =    p_IMP_FNL_CTL,
                        IMP_RTN_CTL   =    p_IMP_RTN_CTL,
                        IMP_LCD_CTL   =    p_IMP_LCD_CTL,
                        IMP_LCD_USO   =    p_IMP_LCD_USO,
                        IMP_LCD_DIP   =    p_IMP_LCD_DIP,
                        IMP_TOT_DIP =      p_IMP_TOT_DIP

                WHERE   NUM_DOC_FIL = v_NUM_RUT AND
                        NUM_CTA_CTE = p_NUM_CTA_CTE;

        ELSE
            INSERT INTO PABS_DT_ENCAB_CRTLA_FLIAL (
                        NUM_DOC_FIL,
                        --COD_TPO_FIL,
                        NUM_CTA_CTE,
                        FEC_CTL_CTA,
                        GLS_TPO_CTL,
                        FEC_INI_CTL,
                        FEC_FNL_CTL,
                        NUM_CTL_FIL,
                        NUM_MVT_CTL,
                        NUM_LCD_CTA,
                        IMP_INI_CTL,
                        IMP_TOT_ING,
                        IMP_TOT_EGR,
                        IMP_FNL_CTL,
                        IMP_RTN_CTL,
                        IMP_LCD_CTL,
                        IMP_LCD_USO,
                        IMP_LCD_DIP,
                        IMP_TOT_DIP,
                        IMP_ING_DIA,
                        IMP_EGR_DIA,
                        IMP_TOT_DIA,
                        IMP_ING_24 ,
                        IMP_EGR_24 ,
                        IMP_TOT_24 ,
                        IMP_ING_48 ,
                        IMP_EGR_48 ,
                        IMP_TOT_48 ,
                        NUM_SUC_ORG )
                       VALUES (
                        v_NUM_RUT,
                        --v_Dv_RUT,
                        p_NUM_CTA_CTE,
                        p_FEC_CTL_CTA,
                        p_GLS_TPO_CTL,
                        p_FEC_INI_CTL,
                        p_FEC_FNL_CTL,
                        p_NUM_CTL_FIL,
                        p_NUM_MVT_CTL,
                        NVL(v_NUM_LCD_CTA,'0'),
                        p_IMP_INI_CTL,
                        p_IMP_TOT_ING,
                        p_IMP_TOT_EGR,
                        p_IMP_FNL_CTL,
                        p_IMP_RTN_CTL,
                        p_IMP_LCD_CTL,
                        p_IMP_LCD_USO,
                        p_IMP_LCD_DIP,
                        p_IMP_TOT_DIP,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        '0');
        END IF;


        DELETE FROM PABS_DT_DETLL_CRTLA_FLIAL
        WHERE NUM_DOC_FIL = v_NUM_RUT AND
              NUM_CTA_CTE = p_NUM_CTA_CTE
              AND NUM_MVT_FIL > v_MOV_FIL_MYR;

        p_ERROR:= PKG_PAB_CONSTANTES.v_ok;

    EXCEPTION
        WHEN NO_DATA_FOUND THEN
                PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);
                p_ERROR:= PKG_PAB_CONSTANTES.v_ERROR;
        WHEN OTHERS THEN
                err_code := SQLCODE;
                err_msg := SUBSTR(SQLERRM, 1, 300) || 'g: ' || p_NUM_CTA_CTE;
                p_s_mensaje := err_code || '-' || err_msg;
                PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);
                p_ERROR:= PKG_PAB_CONSTANTES.v_ERROR;
                RAISE_APPLICATION_ERROR(-20000,p_s_mensaje);
    END Sp_PAB_INS_CBCRA_CRTLA_FLIAL;


--**********************************************************************************************
-- Funcion/Procedimiento: SP_PAB_CNA_CJA_FIL
-- Objetivo: Retorna datos de las empresas filiales de Santander.
-- Base de Datos: DGBMSEGDB01_NGALTM
-- Tablas Usadas: PABS_DT_CAJA_FLIAL
-- Fecha: 04/10/17
-- Autor: Santander
-- Input:
-- N/A
-- Output:
-- p_CURSOR ->  Lista los filiales caja flilales
-- p_ERROR  ->  Indicada si el procedimiento
-- Input/Output: N/A
-- Retorno: N/A.
-- Observaciones: <Fecha y Detalle de utimos cambios>
--***********************************************************************************************
  PROCEDURE SP_PAB_CNA_CJA_FIL (
                                p_CURSOR            OUT SYS_REFCURSOR,
                                p_ERROR             OUT NUMBER)
   IS
    v_NOM_SP                    VARCHAR2 (30) := 'SP_PAB_CNA_CJA_FIL';


    BEGIN

        OPEN p_CURSOR FOR
            SELECT
                CJAFL.COD_BIC_BCO,
                CJAFL.COD_DVI,
                CJAFL.COD_TPO_FIL,
                CJAFL.GLS_CTA_CTE,
                CJAFL.GLS_EPS_FIL,
                --CJAFL.NOM_BCO_CTA,
                CJAFL.NUM_CTA_CTE,
                CJAFL.NUM_DOC_FIL,
                CJAFL.NUM_EPS_FIL,
                CJAFL.NUM_LCD_CTA
            FROM  PABS_DT_CAJA_FLIAL CJAFL;

            p_ERROR := PKG_PAB_CONSTANTES.v_OK;
    --                                                                                                                                                                                                                                                                                                                                                                                                                                --Procedimiento ejecutado de forma correcta
    EXCEPTION
    WHEN NO_DATA_FOUND
        THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300) || ' No se pudo encontrar datos en PABS_DT_CAJA_FLIAL' ;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
    WHEN OTHERS
        THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR(-20000, p_s_mensaje);
    --
    END SP_PAB_CNA_CJA_FIL;



--************************************************************************************************
-- Funcion/Procedimiento: Sp_PAB_INS_DETLL_CRTLA_FLIAL
-- Objetivo: Procedimiento que inserta el detalle de cartola.
-- Sistema: DBO_PAB.
-- Base de Datos: DBO_PAB.
-- Tablas Usadas: PABS_DT_DETLL_CRTLA_FLIAL, PABS_DT_CAJA_FLIAL
-- Fecha: 23/10/17
-- Autor: Santander
-- Input:
-- P_NUM_CTL_FIL        -> N�mero de cartola
-- p_FEC_CTL_CTA        -> Fecha de Cartola
-- p_NUM_CTA_CTE        -> N�mero de cuenta
-- p_NUM_MVT_FIL        -> N�mero de Movimiento
-- p_NUM_DOC_FIL        -> Rut Filial
-- p_CANT_MOV           -> Cantidad de Movimientos
-- p_XML_MOV            -> XML con detalle de los movimientos
-- Output:
--        p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
-- Input/Output: N/A
-- Retorno: N/A.
-- Observaciones: <Fecha y Detalle de ultimos cambios>
--***********************************************************************************************/
PROCEDURE Sp_PAB_INS_DETLL_CRTLA_FLIAL (
                                        P_NUM_CTL_FIL           IN NUMBER,
                                        p_NUM_CTA_CTE           IN VARCHAR2,
                                        p_FEC_CTL_CTA           IN DATE,
                                        p_NUM_DOC_FIL           IN CHAR,
                                        p_NUM_MVT_FIL           IN NUMBER,
                                        p_CANT_MOV               IN NUMBER,
                                        p_XML_MOV               IN CLOB,
                                        p_ERROR                  OUT NUMBER)
    IS

    v_NOM_SP            VARCHAR2(30)    :=  'Sp_PAB_INS_DETLL_CRTLA_FLIAL';
    v_EXISTE            NUMBER          :=  NULL;
    v_COD_TPO_FIL       CHAR;
    v_NUM_CTA_CTE       VARCHAR2(30);
    v_FEC_CTL_CTA       VARCHAR2(50);
    v_NUM_EPS_FIL       CHAR(4);
    v_NUM_MVT_FIL       CHAR(12);
    v_IMP_TRN_FIL       VARCHAR2(30);--NUMBER;
    v_GLS_TRN_CTL       VARCHAR2(60);
    v_GLS_SUC_TRN_CTL   VARCHAR2(60);
    v_NUM_RUT           CHAR(11);
    v_Dv_RUT            CHAR(1);
    v_IMP_NVO_SDO       VARCHAR2(30);--NUMBER;
    v_FEC_MVT           VARCHAR2(50);--DATE;
    v_FEC_MVT_CTL       DATE;



    BEGIN
        BEGIN
            SELECT COD_TPO_FIL,NUM_EPS_FIL
            INTO v_COD_TPO_FIL,v_NUM_EPS_FIL
            FROM DBO_PAB.PABS_DT_CAJA_FLIAL
            WHERE NUM_CTA_CTE = p_NUM_CTA_CTE;


            --Separamos Rut
            PKG_PAB_UTILITY.Sp_PAB_DESCOMp_RUT(p_NUM_DOC_FIL,v_NUM_RUT,v_Dv_RUT);

            EXCEPTION
                  WHEN NO_DATA_FOUND THEN
                            err_code := SQLCODE;
                            err_msg := '1v_NUM_DOC_FIL: ' || p_NUM_DOC_FIL || '/' || v_NUM_RUT || ' p_NUM_CTA_CTE: ' || p_NUM_CTA_CTE;
                            p_s_mensaje := err_code || '-' || err_msg;
                            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);
                            p_ERROR:= PKG_PAB_CONSTANTES.v_ERROR;
                    WHEN OTHERS THEN
                            err_code := SQLCODE;
                            err_msg := SUBSTR (SQLERRM, 1, 300) || 'NUM_CTA_CTE: ' || p_NUM_CTA_CTE;
                            p_s_mensaje := err_code || '-' || err_msg;
                            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);
                            p_ERROR:= PKG_PAB_CONSTANTES.v_ERROR;

        END;


        FOR i IN 1..p_CANT_MOV LOOP
            BEGIN
                SELECT EXTRACT(XMLTYPE(p_XML_MOV), '//movimiento[' || TO_CHAR(i) || ']/numerMov/text()').getStringVal()         INTO v_NUM_MVT_FIL FROM DUAL;
                SELECT EXTRACT(XMLTYPE(p_XML_MOV), '//movimiento[' || TO_CHAR(i) || ']/monto/text()').getStringVal()            INTO v_IMP_TRN_FIL FROM DUAL;
                SELECT EXTRACT(XMLTYPE(p_XML_MOV), '//movimiento[' || TO_CHAR(i) || ']/fecha/text()').getStringVal()            INTO v_FEC_CTL_CTA FROM DUAL;
                SELECT EXTRACT(XMLTYPE(p_XML_MOV), '//movimiento[' || TO_CHAR(i) || ']/descripcion/text()').getStringVal()      INTO v_GLS_TRN_CTL FROM DUAL;
                SELECT EXTRACT(XMLTYPE(p_XML_MOV), '//movimiento[' || TO_CHAR(i) || ']/nomSucMovimiento/text()').getStringVal() INTO v_GLS_SUC_TRN_CTL FROM DUAL;
                SELECT EXTRACT(XMLTYPE(p_XML_MOV), '//movimiento[' || TO_CHAR(i) || ']/nvoSaldo/text()').getStringVal()         INTO v_IMP_NVO_SDO FROM DUAL;
                SELECT EXTRACT(XMLTYPE(p_XML_MOV), '//movimiento[' || TO_CHAR(i) || ']/fechaConta/text()').getStringVal()       INTO v_FEC_MVT     FROM DUAL;


                EXCEPTION
                    WHEN OTHERS THEN
                        err_code := SQLCODE;
                        err_msg := SUBSTR(SQLERRM, 1, 300);
                        p_s_mensaje := err_code || '-' || err_msg;
                        PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);
                        p_ERROR:= PKG_PAB_CONSTANTES.v_ERROR;
                        RAISE_APPLICATION_ERROR(-20000,p_s_mensaje);
            END;

            v_FEC_MVT_CTL := TO_DATE(v_FEC_MVT,'YYYY/MM/DD');

            BEGIN
                INSERT INTO PABS_DT_DETLL_CRTLA_FLIAL (
                    NUM_DOC_FIL,
                    NUM_CTA_CTE,
                    FEC_CTL_CTA,
                    NUM_MVT_FIL,
                    IMP_TRN_FIL,
                    GLS_TRN_CTL,
                    GLS_SUC_TRN_CTL,
                    NUM_CTL_FIL,
                    FEC_MVT_CTL,
                    IMP_NVO_SDO )
                    VALUES (
                    v_NUM_RUT,
                    p_NUM_CTA_CTE,
                    SYSDATE,
                    v_NUM_MVT_FIL,
                    v_IMP_TRN_FIL,
                    v_GLS_TRN_CTL,
                    v_GLS_SUC_TRN_CTL,
                    P_NUM_CTL_FIL,
                    v_FEC_MVT_CTL,
                    v_IMP_NVO_SDO );

                EXCEPTION
                    WHEN DUP_VAL_ON_INDEX THEN
                        err_code := SQLCODE;
                        err_msg := SUBSTR(SQLERRM, 1, 300) || p_NUM_CTA_CTE ||  '-> ' || v_NUM_RUT;
                        p_s_mensaje := err_code || '-' || err_msg;
                        PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);
                        p_ERROR:= PKG_PAB_CONSTANTES.v_ERROR;
                        RAISE_APPLICATION_ERROR(-20000,p_s_mensaje);

                    WHEN OTHERS THEN
                        err_code := SQLCODE;
                        err_msg := SUBSTR(SQLERRM, 1, 300);
                        p_s_mensaje := err_code || '-' || err_msg;
                        PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);
                        p_ERROR:= PKG_PAB_CONSTANTES.v_ERROR;
                        RAISE_APPLICATION_ERROR(-20000,p_s_mensaje);
            END;

        END LOOP;

        p_ERROR:= PKG_PAB_CONSTANTES.v_ok;

    EXCEPTION
        WHEN NO_DATA_FOUND THEN
                PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);
                p_ERROR:= PKG_PAB_CONSTANTES.v_ERROR;

        WHEN OTHERS THEN
                err_code := SQLCODE;
                err_msg := SUBSTR(SQLERRM, 1, 300);
                p_s_mensaje := err_code || '-' || err_msg;
                PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD( err_code, v_NOM_PCK ,err_msg , v_NOM_SP);
                p_ERROR:= PKG_PAB_CONSTANTES.v_ERROR;
                RAISE_APPLICATION_ERROR(-20000,p_s_mensaje);
END Sp_PAB_INS_DETLL_CRTLA_FLIAL;




--**********************************************************************************************
-- Funcion/Procedimiento: SP_PAB_ELI_DETLL_CRTLA
-- Objetivo: Elimina detalles de cartolas previo la inserci�n de la carga enviada por TIBCO al comienzo del d�a
-- Sistema: PAB
-- Base de Datos: DGBMSEGDB01_NGALTM
-- Tablas Usadas: PABS_DT_DETLL_CRTLA_FLIAL
-- Fecha: 18/10/2017
-- Autor: Santander
-- Input:
-- p_NUM_RUT_FIL
-- p_NUM_CTA_CTE
-- Output:
-- p_ERROR  ->  Indicada si el procedimiento
-- Input/Output: N/A
-- Retorno: N/A.
-- Observaciones: <Fecha y Detalle de utimos cambios>
--***********************************************************************************************
-- GSD
  PROCEDURE SP_PAB_ELI_DETLL_CRTLA (
                                    p_NUM_RUT_FIL  IN VARCHAR,
                                    p_NUM_CTA_CTE  IN VARCHAR,
                                    p_ERROR        OUT NUMBER)
    IS

        v_NOM_SP             VARCHAR2 (30) := 'SP_PAB_ELI_DETLL_CRTLA';

    BEGIN


        DELETE FROM PABS_DT_DETLL_CRTLA_FLIAL DETLL
        WHERE TRUNC(DETLL.FEC_CTL_CTA)    = TRUNC(SYSDATE);
              --AND TO_DATE(MVNTO.FEC_ING_MVT,'DD/MM/YYYY HH24:MI:SS')  = TO_DATE(p_FEC_ING_MVT,'DD/MM/YYYY HH24:MI:SS'); --GSD

        p_ERROR := PKG_PAB_CONSTANTES.v_OK;

    EXCEPTION
        WHEN NO_DATA_FOUND
            THEN
             err_code := SQLCODE;
             err_msg := 'No se encuentra registro para eliminar con rut ' || p_NUM_RUT_FIL || ' y cuenta ' || p_NUM_CTA_CTE;
             PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                 v_NOM_PCK,
                                                 err_msg,
                                                 v_NOM_SP);
             p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
        WHEN OTHERS
            THEN
             err_code := SQLCODE;
             err_msg := SUBSTR (SQLERRM, 1, 300);
             PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                 v_NOM_PCK,
                                                 err_msg,
                                                 v_NOM_SP);
             p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
             p_s_mensaje := err_code || '-' || err_msg;
             RAISE_APPLICATION_ERROR(-20000, p_s_mensaje);

  END SP_PAB_ELI_DETLL_CRTLA;


--************************************************************************************************
-- Funcion/Procedimiento: Sp_PAB_ACT_CRTLA_FLIAL
-- Objetivo: Procedimiento que actualiza los montos de egreso e ingreso seg�n la tabla de movimientos de proyecci�n filiales.
-- Sistema: DBO_PAB.
-- Base de Datos: DBO_PAB.
-- Tablas Usadas: PABS_DT_MVNTO_FLIAL, PABS_DT_ENCAB_CRTLA_FLIAL
-- Fecha: 17/10/17
-- Autor: Santander
-- Input:
-- p_NUM_CTA_CTE -> N�mero de cuenta corriente
-- Output:
-- p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
-- Input/Output: N/A
-- Retorno: N/A.
-- Observaciones: <Fecha y Detalle de ultimos cambios>
--***********************************************************************************************/
-- Pendiente se debe solicitar cambio de tipo de campo p_NUM_EPS_FIL
PROCEDURE Sp_PAB_ACT_CRTLA_FLIAL (    p_NUM_CTL_FIL   IN NUMBER,
                                    p_NUM_CTA_CTE   IN VARCHAR2,
                                    p_FEC_CTL_CTA   IN DATE,
                                    p_NUM_DOC_FIL   IN CHAR,
                                    p_ERROR           OUT NUMBER)
    IS
    v_NOM_SP            VARCHAR2 (30) := 'Sp_PAB_ACT_CRTLA_FLIAL';
    v_IMP_EGR_FIL       NUMBER;
    v_IMP_ING_FIL       NUMBER;
    v_NUM_RUT           CHAR(11);
    v_Dv_RUT            CHAR(1);

    BEGIN

        p_ERROR := PKG_PAB_CONSTANTES.v_OK;

         --Debemos volver a calcular los montoS
         SP_PAB_ACT_CAB_PYN_FIL(NULL,NULL,0,p_ERROR);


    EXCEPTION
    WHEN NO_DATA_FOUND
        THEN
         err_code := SQLCODE;
         err_msg := 'No se encuentra registro para actualizar con cuenta filial ' || p_NUM_CTL_FIL || ' - cuenta corriente ' || p_NUM_CTA_CTE || '- fecha ' ||p_FEC_CTL_CTA|| '- rut ' || p_NUM_DOC_FIL;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
    WHEN OTHERS
        THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR(-20000, p_s_mensaje);

    END Sp_PAB_ACT_CRTLA_FLIAL;


--**********************************************************************************************
-- Funcion/Procedimiento: SP_PAB_ACT_CAB_PYN_FIL
-- Objetivo: Actualiza los saldos de la cabecera de filiales cuando se realiza una carga de nomina
-- Base de Datos: DGBMSEGDB01_NGALTM
-- Tablas Usadas: PABS_DT_ENCAB_CRTLA_FLIAL
-- Fecha: 27/09/17
-- Autor: Santander
-- Input:
-- N/A
-- Output:
-- p_NUM_RUT_FIL -> N�mero Rut Filiales
-- p_NUM_CTA_CTE -> N�mero Cta Corriente
-- Input/Output: N/A
-- Retorno: N/A.
-- Observaciones: <Fecha y Detalle de utimos cambios>
--***********************************************************************************************
  PROCEDURE SP_PAB_ACT_CAB_PYN_FIL (
                                    p_NUM_RUT_FIL  IN VARCHAR,
                                    p_NUM_CTA_CTE  IN VARCHAR,
                                    p_NUM_FOL_NMN  IN NUMBER,
                                    p_ERROR        OUT NUMBER)

 IS
    v_NOM_SP         VARCHAR2 (30) := 'SP_PAB_ACT_CAB_PYN_FIL';
    v_IMP_EGR_FIL    NUMBER;
    v_IMP_ING_FIL    NUMBER;
    ------------------------
    v_IMP_ING_DIA    NUMBER;
    v_IMP_EGR_DIA    NUMBER;
    v_IMP_TOT_DIA    NUMBER;
    v_IMP_ING_24     NUMBER;
    v_IMP_EGR_24     NUMBER;
    v_IMP_TOT_24     NUMBER;
    v_IMP_TOT_48     NUMBER;
    v_IMP_EGR_48     NUMBER;
    v_IMP_ING_48     NUMBER;
    v_IMP_FNL_CTL    NUMBER;
    -------------------------
    v_FECHA_HOY      DATE;
    v_FECHA_24       DATE;
    v_FECHA_48       DATE;
    v_COD_PAIS       char(2) := 'SN'; --Santiago
    v_Dv_RUT         char(1);

    P_NUM_CTA_CTE_CUR VARCHAR2(25);
    P_NUM_DOC_FIL     CHAR(11);

    BEGIN

        -- (Viene del proceso de carga)
        IF(p_NUM_FOL_NMN IS NOT NULL) THEN

            --Separamos Rut
            P_NUM_CTA_CTE_CUR := NULL;
            P_NUM_DOC_FIL     := NULL;
        ELSE
            --Viene de la eliminaci�n de uno
            P_NUM_CTA_CTE_CUR := p_NUM_CTA_CTE;
            P_NUM_DOC_FIL     := p_NUM_RUT_FIL;
        END IF;

        --Calculos que se realizan solo una vez
        /*****************************************************************************************/
        v_FECHA_HOY  := TRUNC(SYSDATE); -- Obtenemos la fecha de hoy

        v_FECHA_24  := v_FECHA_HOY + 1; -- Obtenemos la fecha 24

        --Obtenemos la fecha a 24 Horas
        PKG_PAB_UTILITY.SP_PAB_DEV_DIA_HABIL_SIG(v_COD_PAIS,v_FECHA_24);

        v_FECHA_48 := v_FECHA_24 + 1; -- Obtenemos la fecha 48

        --Obtenemos la fecha a 24 Horas
        PKG_PAB_UTILITY.SP_PAB_DEV_DIA_HABIL_SIG(v_COD_PAIS,v_FECHA_48);
        /********************************Calculo 24 Horas*****************************************/

        BEGIN

            OPEN C_FILIALES_PROYECCION(P_NUM_CTA_CTE_CUR,P_NUM_DOC_FIL);

        EXCEPTION
            WHEN OTHERS THEN
                err_code := SQLCODE;
                err_msg := 'No se pudo abrir el cursor C_FILIALES_PROYECCION';
                PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                     v_NOM_PCK,
                                                     err_msg,
                                                     v_NOM_SP);
                p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
                RAISE;
        END;

        BEGIN
            FETCH C_FILIALES_PROYECCION INTO R_FILIALES_PROYECCION;
        EXCEPTION
            WHEN OTHERS THEN

                err_code := SQLCODE;
                err_msg := 'No se pudo asignar el cursor R_FILIALES_PROYECCION';
                PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                     v_NOM_PCK,
                                                     err_msg,
                                                     v_NOM_SP);
                p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
                RAISE;
        END;

        WHILE C_FILIALES_PROYECCION%FOUND
        LOOP

            --Recorremos las filiales que debemos actualizar las operaciones.
            P_NUM_DOC_FIL     := R_FILIALES_PROYECCION.NUM_DOC_FIL;
            P_NUM_CTA_CTE_CUR := R_FILIALES_PROYECCION.NUM_CTA_CTE;

            BEGIN

                --Obtenemos el valor final de la cartola cargada
                SELECT IMP_FNL_CTL
                  INTO V_IMP_FNL_CTL
                FROM PABS_DT_ENCAB_CRTLA_FLIAL
                WHERE NUM_DOC_FIL = P_NUM_DOC_FIL
                  AND NUM_CTA_CTE = P_NUM_CTA_CTE_CUR;

            EXCEPTION
                WHEN NO_DATA_FOUND THEN
                     err_code := SQLCODE;
                     err_msg := 'No se pueden obtener los saldos de filiales RUT:' || P_NUM_DOC_FIL ||  '<- CUENTA:' || P_NUM_CTA_CTE_CUR || '<-';
                     RAISE;
                WHEN OTHERS THEN
                     err_code := SQLCODE;
                     err_msg := SUBSTR (SQLERRM, 1, 300);
                     PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                         v_NOM_PCK,
                                                         err_msg,
                                                         v_NOM_SP);
                     p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
                     p_s_mensaje := err_code || '-' || err_msg;

                    --Cerramos cursor
                    IF (C_FILIALES_PROYECCION %ISOPEN) THEN
                        CLOSE C_FILIALES_PROYECCION;
                    END IF;

                    RAISE_APPLICATION_ERROR(-20000, p_s_mensaje);
            END;

            /********************************Calculo Intradia*****************************************/

            --Obtenemos la suma de los movimientos cargados en la nomina (Fecha Hoy)
            BEGIN

                --
                SELECT NVL(SUM(IMP_ING_FIL),0),NVL(SUM(IMP_EGR_FIL),0)
                INTO v_IMP_ING_FIL,v_IMP_EGR_FIL
                FROM PABS_DT_MVNTO_FLIAL
                WHERE NUM_DOC_FIL = P_NUM_DOC_FIL
                AND NUM_CTA_CTE   = P_NUM_CTA_CTE_CUR
                AND FEC_VTA_FIL = v_FECHA_HOY
                AND FEC_ING_MVT BETWEEN v_dia_ini AND v_dia_fin;--<= v_FECHA_HOY;

            EXCEPTION
                WHEN NO_DATA_FOUND THEN
                     err_code := SQLCODE;
                     err_msg := 'No se pudo obtener los saldos de la cuenta ' || p_NUM_CTA_CTE || ' Rut:' || P_NUM_DOC_FIL;
                     PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                         v_NOM_PCK,
                                                         err_msg,
                                                         v_NOM_SP);
                     p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
                     RAISE;
                WHEN OTHERS THEN
                     err_code := SQLCODE;
                     err_msg := SUBSTR (SQLERRM, 1, 300);
                     PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                         v_NOM_PCK,
                                                         err_msg,
                                                         v_NOM_SP);
                     p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
                     p_s_mensaje := err_code || '-' || err_msg;
                    --Cerramos cursor
                    IF (C_FILIALES_PROYECCION %ISOPEN) THEN
                        CLOSE C_FILIALES_PROYECCION;
                    END IF;
                     RAISE_APPLICATION_ERROR(-20000, p_s_mensaje);
            END;

            -- Calculamos los datos Intradia -- (Actual + Nuevo)
            V_IMP_ING_DIA := v_IMP_ING_FIL;
            V_IMP_EGR_DIA := v_IMP_EGR_FIL;
            V_IMP_TOT_DIA := V_IMP_FNL_CTL + V_IMP_ING_DIA - V_IMP_EGR_DIA;

            /**********************************************Calculo 24 Horas**********************/

             --Obtenemos la suma de los movimientos cargados en la nomina (Fecha 24 Horas)
            BEGIN

                --
                SELECT NVL(SUM(IMP_ING_FIL),0),NVL(SUM(IMP_EGR_FIL),0)
                INTO v_IMP_ING_FIL,v_IMP_EGR_FIL
                FROM PABS_DT_MVNTO_FLIAL
                WHERE NUM_DOC_FIL = P_NUM_DOC_FIL
                AND NUM_CTA_CTE   = P_NUM_CTA_CTE_CUR
                AND FEC_VTA_FIL = v_FECHA_24
                AND FEC_ING_MVT BETWEEN v_dia_ini AND v_dia_fin;-- <= v_FECHA_HOY;

            EXCEPTION
                WHEN NO_DATA_FOUND THEN
                     err_code := SQLCODE;
                     err_msg := 'No se pudo obtener los saldos de la cuenta ' || p_NUM_CTA_CTE || ' Rut:' || P_NUM_DOC_FIL;
                     PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                         v_NOM_PCK,
                                                         err_msg,
                                                         v_NOM_SP);
                     p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
                     RAISE;
                WHEN OTHERS THEN
                     err_code := SQLCODE;
                     err_msg := SUBSTR (SQLERRM, 1, 300);
                     PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                         v_NOM_PCK,
                                                         err_msg,
                                                         v_NOM_SP);
                     p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
                     p_s_mensaje := err_code || '-' || err_msg;
                     --Cerramos cursor
                     IF (C_FILIALES_PROYECCION %ISOPEN) THEN
                         CLOSE C_FILIALES_PROYECCION;
                     END IF;
                     RAISE_APPLICATION_ERROR(-20000, p_s_mensaje);
            END;

            -- Calculamos los datos 24 Horas -- (Actual + Nuevo)
            v_IMP_ING_24 := v_IMP_ING_FIL;
            v_IMP_EGR_24 := v_IMP_EGR_FIL;
            v_IMP_TOT_24 := V_IMP_TOT_DIA + v_IMP_ING_24 - v_IMP_EGR_24;

            /********************************Calculo 48 Horas*****************************************/

            --Obtenemos la suma de los movimientos cargados en la nomina (Fecha 24 Horas)
            BEGIN

                --
                SELECT NVL(SUM(IMP_ING_FIL),0),NVL(SUM(IMP_EGR_FIL),0)
                INTO v_IMP_ING_FIL,v_IMP_EGR_FIL
                FROM PABS_DT_MVNTO_FLIAL
                WHERE NUM_DOC_FIL = P_NUM_DOC_FIL
                AND NUM_CTA_CTE   = P_NUM_CTA_CTE_CUR
                AND FEC_VTA_FIL = v_FECHA_48
                AND FEC_ING_MVT BETWEEN v_dia_ini AND v_dia_fin; --<= v_FECHA_HOY;

            EXCEPTION
                WHEN NO_DATA_FOUND THEN
                     err_code := SQLCODE;
                     err_msg := 'No se pudo obtener los saldos de la cuenta ' || p_NUM_CTA_CTE || ' Rut:' || P_NUM_DOC_FIL;
                     PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                         v_NOM_PCK,
                                                         err_msg,
                                                         v_NOM_SP);
                     p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
                     RAISE;
                WHEN OTHERS THEN
                     err_code := SQLCODE;
                     err_msg := SUBSTR (SQLERRM, 1, 300);
                     PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                         v_NOM_PCK,
                                                         err_msg,
                                                         v_NOM_SP);
                     p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
                     p_s_mensaje := err_code || '-' || err_msg;
                     --Cerramos cursor
                     IF (C_FILIALES_PROYECCION %ISOPEN) THEN
                         CLOSE C_FILIALES_PROYECCION;
                     END IF;
                     RAISE_APPLICATION_ERROR(-20000, p_s_mensaje);
            END;

            -- Calculamos los datos 24 Horas -- (Actual + Nuevo)
            v_IMP_ING_48 := v_IMP_ING_FIL;
            v_IMP_EGR_48 := v_IMP_EGR_FIL;
            v_IMP_TOT_48 := v_IMP_TOT_24 + v_IMP_ING_48 - v_IMP_EGR_48;

            /************************************************************/

            --Actualizamos los saldos siempre y cuando algun valor se adistinto de vacio
            UPDATE PABS_DT_ENCAB_CRTLA_FLIAL
            SET IMP_ING_DIA = NVL(V_IMP_ING_DIA,IMP_ING_DIA),
                IMP_EGR_DIA = NVL(V_IMP_EGR_DIA,IMP_EGR_DIA),
                IMP_TOT_DIA = NVL(V_IMP_TOT_DIA,IMP_TOT_DIA),
                IMP_ING_24  = NVL(v_IMP_ING_24,IMP_ING_24),
                IMP_EGR_24  = NVL(v_IMP_EGR_24,IMP_EGR_24),
                IMP_TOT_24  = NVL(v_IMP_TOT_24,IMP_TOT_24),
                IMP_ING_48  = NVL(v_IMP_ING_48,IMP_ING_48),
                IMP_EGR_48  = NVL(v_IMP_EGR_48,IMP_EGR_48),
                IMP_TOT_48  = NVL(v_IMP_TOT_48,IMP_TOT_48)
            WHERE NUM_DOC_FIL = P_NUM_DOC_FIL
              AND NUM_CTA_CTE   = P_NUM_CTA_CTE_CUR;

            FETCH C_FILIALES_PROYECCION INTO R_FILIALES_PROYECCION;

        END LOOP;

        CLOSE C_FILIALES_PROYECCION;

        p_ERROR := PKG_PAB_CONSTANTES.v_OK;

    EXCEPTION
        WHEN NO_DATA_FOUND
            THEN
             PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                 v_NOM_PCK,
                                                 err_msg,
                                                 v_NOM_SP);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;

            --Cerramos cursor
            IF (C_FILIALES_PROYECCION %ISOPEN) THEN
                CLOSE C_FILIALES_PROYECCION;
            END IF;

        WHEN OTHERS
            THEN
             err_code := SQLCODE;
             err_msg := SUBSTR (SQLERRM, 1, 300);
             PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                 v_NOM_PCK,
                                                 err_msg,
                                                 v_NOM_SP);
             p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
             p_s_mensaje := err_code || '-' || err_msg;

              --Cerramos cursor
             IF (C_FILIALES_PROYECCION %ISOPEN) THEN
                CLOSE C_FILIALES_PROYECCION;
             END IF;

             RAISE_APPLICATION_ERROR(-20000, p_s_mensaje);

    END SP_PAB_ACT_CAB_PYN_FIL;

END PKG_PAB_CAJA_FILIAL;
/


GRANT EXECUTE ON DBO_PAB.PKG_PAB_CAJA_FILIAL TO PUBLIC
/

GRANT EXECUTE ON DBO_PAB.PKG_PAB_CAJA_FILIAL TO R_APP_PAB
/

GRANT EXECUTE ON DBO_PAB.PKG_PAB_CAJA_FILIAL TO USR_PAB
/

