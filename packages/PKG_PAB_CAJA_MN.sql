CREATE OR REPLACE /* Formatted on 18-08-2020 17:45:30 (QP5 v5.115.810.9015) */
PACKAGE DBO_PAB.PKG_PAB_CAJA_MN
IS
   --
   v_NOM_PCK        VARCHAR2 (30) := 'PKG_PAB_CAJA_MN';
   ERR_CODE         NUMBER := 0;
   ERR_MSG          VARCHAR2 (300);
   v_DES            VARCHAR2 (300);
   v_DES_BIT        VARCHAR2 (100);
   p_s_mensaje      VARCHAR2 (400);
   v_FECHA_AYER     DATE := TRUNC (SYSDATE - 1);
   v_FECHA_ACTUAL   DATE := TRUNC (SYSDATE);
   v_COD_PAIS       CHAR (2) := 'SN';
   v_cero           NUMBER := 0;

   --Canal de salida, para estado Pagado por contingencia
   v_EST_PAG_CNT    CHAR (10) := 'PCNT';

   v_dia_ini        DATE := TRUNC (SYSDATE) + INTERVAL '1' MINUTE;
   v_dia_fin DATE
         := TRUNC (SYSDATE) + INTERVAL '23' HOUR + INTERVAL '59' MINUTE ;


   /************************************************************************************************
   -- Funcion/Procedimiento: SP_BUS_PSCON_BCCH_MN
   -- Objetivo: Procedimiento almacenado que Muestra la posici�n del Banco Central, saldos
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_DETLL_SALDO_CAJA_MN_MX
   --
   -- Fecha: 22-11-2017
   -- Autor: Santander
   -- Input: p_
   --
   -- Output:
   --        p_CURSOR : SYS_REFCURSOR, resultados de consulta.
   --        p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE SP_BUS_PSCON_BCCH_MN (p_CURSOR   OUT SYS_REFCURSOR,
                                   p_ERROR    OUT NUMBER);

   /************************************************************************************************
   -- Funcion/Procedimiento: SP_BUS_DETLL_BCCH_MN
   -- Objetivo: Procedimiento almacenado que Muestra el detalle de la posici�n del Banco Central
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_DETLL_SALDO_CAJA_MN_MX
   --
   -- Fecha: 22-11-2017
   -- Autor: Santander
   -- Input: p_
   --
   -- Output:
   --        p_CURSOR : SYS_REFCURSOR, resultados de consulta.
   --        p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE SP_BUS_DETLL_BCCH_MN (p_CURSOR    OUT SYS_REFCURSOR,
                                   p_CURSOR2   OUT SYS_REFCURSOR,
                                   p_ERROR     OUT NUMBER);

   /************************************************************************************************
   -- Funcion/Procedimiento: SP_BUS_PSCON_COMBAC_MN
   -- Objetivo: Procedimiento almacenado que Muestra la posici�n del Comban
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_DETLL_SALDO_CAJA_MN_MX
   --
   -- Fecha: 22-11-2017
   -- Autor: Santander
   -- Input: p_
   --
   -- Output:
   --        p_CURSOR : SYS_REFCURSOR, resultados de consulta.
   --        p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE SP_BUS_PSCON_COMBAC_MN (p_CURSOR   OUT SYS_REFCURSOR,
                                     p_ERROR    OUT NUMBER);


   /************************************************************************************************
   -- Funcion/Procedimiento: SP_BUS_DETLL_COMBANC_MN
   -- Objetivo: Procedimiento almacenado que Muestra el detalle de la posici�n del Combanc
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_DETLL_SALDO_CAJA_MN_MX
   --
   -- Fecha: 22-11-2017
   -- Autor: Santander
   -- Input: p_
   --
   -- Output:
   --        p_CURSOR : SYS_REFCURSOR, resultados de consulta.
   --        p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE SP_BUS_DETLL_COMBANC_MN (p_CURSOR    OUT SYS_REFCURSOR,
                                      p_CURSOR2   OUT SYS_REFCURSOR,
                                      p_ERROR     OUT NUMBER);

   /************************************************************************************************
   -- Funcion/Procedimiento: SP_BUS_PSCON_SINACOFI_MN
   -- Objetivo: Procedimiento almacenado que Muestra la posici�n del Sinacofi
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_DETLL_SALDO_CAJA_MN_MX
   --
   -- Fecha: 22-11-2017
   -- Autor: Santander
   -- Input: p_
   --
   -- Output:
   --        p_CURSOR : SYS_REFCURSOR, resultados de consulta.
   --        p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE SP_BUS_PSCON_SINACOFI_MN (p_CURSOR   OUT SYS_REFCURSOR,
                                       p_ERROR    OUT NUMBER);

   /************************************************************************************************
   -- Funcion/Procedimiento: SP_BUS_DETLL_SINACOFI_MN
   -- Objetivo: Procedimiento almacenado que Muestra el detalle de la posici�n del Combanc
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_DETLL_SALDO_CAJA_MN_MX,
   --
   -- Fecha: 22-11-2017
   -- Autor: Santander
   -- Input: p_
   --
   -- Output:
   --        p_CURSOR : SYS_REFCURSOR, resultados de consulta.
   --        p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE SP_BUS_DETLL_SINACOFI_MN (p_CURSOR    OUT SYS_REFCURSOR,
                                       p_CURSOR2   OUT SYS_REFCURSOR,
                                       p_ERROR     OUT NUMBER);


   /************************************************************************************************
   -- Funcion/Procedimiento: SP_BUS_PRYCN_INTRADIA_MN
   -- Objetivo: Procedimiento almacenado que Muestra los saldos de Intradia
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_DETLL_SALDO_CAJA_MN_MX
   --
   -- Fecha: 22-11-2017
   -- Autor: Santander
   -- Input: p_CURSOR
   --
   -- Output:
   --        p_CURSOR : SYS_REFCURSOR, resultados de consulta.
   --        p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE SP_BUS_PRYCN_INTRADIA_MN (p_CURSOR   OUT SYS_REFCURSOR,
                                       p_ERROR    OUT NUMBER);



   /************************************************************************************************
   -- Funcion/Procedimiento: SP_BUS_DETLL_INTRADIA_MN
   -- Objetivo: Procedimiento almacenado que Muestra el detalle de saldos Intradia
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_DETLL_SALDO_CAJA_MN_MX
   --
   -- Fecha: 22-11-2017
   -- Autor: Santander
   -- Input: p_
   --
   -- Output:
   --        p_CURSOR : SYS_REFCURSOR, resultados de consulta.
   --        p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE SP_BUS_DETLL_INTRADIA_MN (p_CURSOR    OUT SYS_REFCURSOR,
                                       p_CURSOR2   OUT SYS_REFCURSOR,
                                       p_ERROR     OUT NUMBER);

   --*******************************************************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_ELI_PRYCN_MN
   -- Objetivo: Procedimiento que elimina registro de proyecciones de Caja Nacional seg�n n�mero de operaci�n y su respectiva n�mina
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_MVNTO_FLIAL
   -- Fecha: 21-11-2017
   -- Autor: Santander- CAH
   -- Input:
   -- p_NUM_FOL_NMN  -> N�nmero Folio
   -- p_NUM_FOL_OPE  -> N�mero Operaci�n
   -- p_FEC_INS_OPE  -> Fecha Ingreso Movimiento
   -- Output:
   -- p_ERROR  ->  Indicada si el procedimiento
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de utimos cambios>
   --********************************************************************************************************************************
   PROCEDURE SP_PAB_ELI_PRYCN_MN (p_NUM_FOL_NMN   IN     NUMBER,
                                  p_NUM_FOL_OPE   IN     NUMBER,
                                  p_FEC_INS_OPE   IN     DATE,
                                  p_ERROR            OUT NUMBER);

   /***************************************************************************************************************
   -- Funcion/Procedimiento: P_PAB_CAL_SLDO_FINAL_DIA_MN
   -- Objetivo: Procedimiento almacenado que actualiza saldo del campo Total final D�a de Proyecci�n
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_DETLL_SALDO_CAJA_MN_MX
   --
   -- Fecha:22-11-2017
   -- Autor: Santander - CAH
   -- Input:
   -- Output:
   --    p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --**************************************************************************************************************/
   PROCEDURE SP_PAB_CAL_SLDO_FINAL_DIA_MN (p_ERROR OUT NUMBER);

   /************************************************************************************************
   -- Funcion/Procedimiento: SP_BUS_GRNTA_COMDER_MN
   -- Objetivo: Procedimiento almacenado que Muestra la posici�n del Banco Central, saldos
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_BIBLI_CAMPO_OBLTO_MT
   --
   -- Fecha: 16/06/17
   -- Autor: Santander
   -- Input: p_
   --
   -- Output:
   --        p_CURSOR : SYS_REFCURSOR, resultados de consulta.
   --        p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE SP_BUS_GRNTA_COMDER_MN (p_CURSOR   OUT SYS_REFCURSOR,
                                     p_ERROR    OUT NUMBER);


   /************************************************************************************************
  -- Funcion/Procedimiento: SP_BUS_CRTLA_BBCH_MN
  -- Objetivo: Procedimiento almacenado que Muestra la cartola del Banco Central, saldos
  -- Sistema: DBO_PAB.
  -- Base de Datos: DBO_PAB.
  -- Tablas Usadas: PABS_DT_BIBLI_CAMPO_OBLTO_MT
  --
  -- Fecha: 16/06/17
  -- Autor: Santander
  -- Input: p_
  --
  -- Output:
  --        p_CURSOR : SYS_REFCURSOR, resultados de consulta.
  --        p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
  -- Input/Output: N/A
  -- Retorno: N/A.
  -- Observaciones: <Fecha y Detalle de ultimos cambios>
  --***********************************************************************************************/
   PROCEDURE SP_BUS_CRTLA_BCCH_MN (p_CURSOR   OUT SYS_REFCURSOR,
                                   p_ERROR    OUT NUMBER);

   /************************************************************************************************
   -- Funcion/Procedimiento: SP_DETLL_CTCTE_BCCH_MN
   -- Objetivo: Procedimiento almacenado que Muestra detalle de Cartola Banco Central de Chile
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_BIBLI_CAMPO_OBLTO_MT
   --
   -- Fecha: 16/06/17
   -- Autor: Santander
   -- Input: p_
   --
   -- Output:
   --        p_CURSOR : SYS_REFCURSOR, resultados de consult cabecera.
   --        p_CURSOR2 : SYS_REFCURSOR, resultados de consulta detalle.
   --        p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE SP_DETLL_CTCTE_BCCH_MN (p_CURSOR    OUT SYS_REFCURSOR,
                                     p_CURSOR2   OUT SYS_REFCURSOR,
                                     p_ERROR     OUT NUMBER);

   /************************************************************************************************
   -- Funcion/Procedimiento: SP_BUS_SALDO_OPRCN_MN
   -- Objetivo: Procedimiento almacenado que Muestra la posici�n del Banco Central, saldos
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_BIBLI_CAMPO_OBLTO_MT
   --
   -- Fecha: 16/06/17
   -- Autor: Santander
   -- Input: p_
   --
   -- Output:
   --        p_CURSOR : SYS_REFCURSOR, resultados de consulta.
   --        p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE SP_BUS_SALDO_OPRCN_MN (p_CURSOR   OUT SYS_REFCURSOR,
                                    p_ERROR    OUT NUMBER);


   /************************************************************************************************
   -- Funcion/Procedimiento: SP_BUS_SALDO_AREA_MN
   -- Objetivo: Procedimiento almacenado que Muestra la posici�n del Banco Central, saldos
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_BIBLI_CAMPO_OBLTO_MT
   --
   -- Fecha: 16/06/17
   -- Autor: Santander
   -- Input: p_
   --
   -- Output:
   --        p_CURSOR : SYS_REFCURSOR, resultados de consulta.
   --        p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE SP_BUS_SALDO_AREA_MN (p_CURSOR   OUT SYS_REFCURSOR,
                                   p_ERROR    OUT NUMBER);

   /************************************************************************************************
   -- Funcion/Procedimiento: SP_GES_CAJA_EGRESO_MN
   -- Objetivo: Procedimiento almacenado que actualiza saldos de Combanc o LBTR.
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_DETLL_OPRCN
   --
   -- Fecha: 16/06/17
   -- Autor: Santander - CAH
   -- Input:
   --           p_NUM_REF_SWF->   Numero referencia Swift
   --           p_COD_SIS_SAL->   Sistema salida LBTR o Combanc
   -- Output:
   --        p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE SP_GES_CAJA_EGRESO_MN (p_NUM_REF_SWF   IN     CHAR,
                                    p_ERROR            OUT NUMBER);

   /************************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_CALCULA_SALDO_BILATERAL
   -- Objetivo: Procedimiento almacenado que actualiza saldos bilaterales para LBTR y Combanc
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_SALDO_BILAT
   --
   -- Fecha:14-11-2017
   -- Autor: Gonzalo Correa
   -- Input_
   --  p_MONTO     -->Monto de la operaci�n
   --  p_BANCO_ORG_DET     -->Banco del movimiento
   --  p_FLAG_EGR_ING      -->Flag de ingreso o egreso
   --  p_CANAL_ENT_SAL     -->Canal del movimiento (LBTR o COMBANC)
   -- Output:
   --  p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE SP_PAB_CALCULA_SALDO_BILATERAL (p_MONTO           IN     NUMBER,
                                             p_BANCO_ORG_DET   IN     CHAR,
                                             p_FLAG_EGR_ING    IN     NUMBER,
                                             p_CANAL_ENT_SAL   IN     CHAR,
                                             p_ERROR              OUT NUMBER);

   /************************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_CALCULA_SALDO_CAJA
   -- Objetivo: Procedimiento almacenado que actualiza saldos de caja
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_DETLL_SALDO_CAJA_MN_MX
   --
   -- Fecha:14-11-2017
   -- Autor: Gonzalo Correa
   -- Input: P_IMP_ING_DIA    --> Monto Ingreso Altos Montos
   --        P_IMP_EGR_DIA    --> Montos Egreso Altos Montos
   --        P_IMP_ABN_BCT    --> Abonos Directos al central
   --        P_IMP_CGO_BCT    --> Cargos directos al central
   -- Output:
   --    p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE SP_PAB_CAL_SLDO_POS_LBTR (P_IMP_ING_DIA   IN     NUMBER,
                                       P_IMP_EGR_DIA   IN     NUMBER,
                                       P_IMP_ABN_BCT   IN     NUMBER,
                                       P_IMP_CGO_BCT   IN     NUMBER,
                                       p_ERROR            OUT NUMBER);

   /************************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_CAL_SLDO_POS_CMN
   -- Objetivo: Procedimiento almacenado que actualiza saldos de caja
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_DETLL_SALDO_CAJA_MN_MX
   --
   -- Fecha:14-11-2017
   -- Autor: Gonzalo Correa
   -- Input: P_IMP_ING_CBA    -->Monto ingreso combanc
   --        P_IMP_EGR_CBA    -->Monto egreso combanc
   -- Output:
   --    p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE SP_PAB_CAL_SLDO_POS_CMN (P_IMP_ING_CBA   IN     NUMBER,
                                      P_IMP_EGR_CBA   IN     NUMBER,
                                      P_ERROR            OUT NUMBER);

   /************************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_CAL_SLDO_MNT_SWF
   -- Objetivo: Procedimiento almacenado que actualiza saldos de caja
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_DETLL_SALDO_CAJA_MN_MX
   --
   -- Fecha:14-11-2017
   -- Autor: Gonzalo Correa
   -- Input: P_IMP_103    -->Monto 103
   --        P_IMP_202    -->Monto 202
   --        P_IMP_205    -->Monto 205
   --        P_IMP_900    -->Monto 900
   --        P_IMP_910    -->Monto 910
   -- Output:
   --    p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE SP_PAB_CAL_SLDO_MNT_SWF (P_IMP_103        IN     NUMBER,
                                      P_IMP_202        IN     NUMBER,
                                      P_IMP_205        IN     NUMBER,
                                      P_IMP_900        IN     NUMBER,
                                      P_IMP_910        IN     NUMBER,
                                      p_FLAG_EGR_ING   IN     NUMBER,
                                      p_ERROR             OUT NUMBER);

   /************************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_CAL_SLDO_POS_SINACOFI
   -- Objetivo: Procedimiento almacenado que actualiza saldos, de operaciones recibidas o enviadas
               por Sinacofi
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_DETLL_SALDO_CAJA_MN_MX
   --
   -- Fecha:17-11-2017
   -- Autor: Santander - CAH
   -- Input: P_IMP_ING_SNF    --> Monto Ingreso Sinacofi
   --        P_IMP_EGR_SNF    --> Montos Egreso Sinacofi
   -- Output:
   --    p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE SP_PAB_CAL_SLDO_POS_SINACOFI (P_IMP_ING_SNF   IN     NUMBER,
                                           P_IMP_EGR_SNF   IN     NUMBER,
                                           p_ERROR            OUT NUMBER);

   -- ***************************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_GES_CNTGC_BCCH_IDA
   -- Objetivo: Procedimiento gestor que llama a pl actuliza operaci�n por contingencia y actuliza saldo para caja MN
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: N/A
   -- Fecha: 21-11-2017
   -- Autor: Santander- CAH
   -- Input:
   -- p_REG_OPER -> parametro tipo registro que contiene el numero y fecha operacion
   -- p_COD_EST_AOS-> Estado de la operaci�n
   -- p_COD_USR -> rut usuario
   -- p_DSC_GLB_BTC-> Glosa para bitacora de la operaci�n.
   -- Output:
   -- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ****************************************************************************************************************
   PROCEDURE Sp_PAB_GES_CNTGC_BCCH_IDA (p_REG_OPER      IN     REG_OPER,
                                        p_COD_EST_AOS   IN     CHAR,
                                        p_COD_USR       IN     VARCHAR2,
                                        p_DSC_GLB_BTC   IN     VARCHAR2,
                                        p_ERROR            OUT NUMBER);


   /************************************************************************************************
   -- Funcion/Procedimiento: SP_BUS_CALCULA_AREA_MN
   -- Objetivo: Procedimiento calcula los saldos por areas
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_SALDO_AREA
   --
   -- Fecha: 16/06/17
   -- Autor: GCORREA
   -- Input:
   --
   -- Output:
   --        p_CURSOR : SYS_REFCURSOR, resultados de consulta.
   --        p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE SP_BUS_CALCULA_AREA_MN (P_COD_SIS_ENT_SAL   IN     CHAR,
                                     P_IMP_ING_ARA       IN     NUMBER,
                                     P_IMP_EGR_ARA       IN     NUMBER,
                                     p_ERROR                OUT NUMBER);

   /********************************************************************************************************
   FUNCION/PROCEDIMIENTO: SP_BUS_ACT_AREA_MN.
   OBJETIVO             : ACTUALIZA LOS SALDOS POR �REA CUANDO EXISTE UN CAMBIO DE CANAL (RESTA),
                          SOLO MT 900/910.
   SISTEMA              : DBO_PAB.
   BASE DE DATOS        : DBO_PAB.
   TABLAS USADAS        : PABS_DT_SALDO_AREA.
   FECHA                : 03/09/2018.
   AUTOR                : VARAYA.
   INPUT                : P_COD_SIS_ENT_SAL := C�DIGO ENTRADA/SALIDA.
                          P_IMP_ING_ARA     := IMPUESTO INGRESO MT910.
                          P_IMP_EGR_ARA     := IMPUESTO EGRESO MT900.
   OUTPUT               : P_ERROR           := INDICADOR DE ERRORES:
                                               0 := SIN ERRORES.
                                               1 := CON ERRORES.
   OBSERVACIONES
   FECHA       USUARIO    DETALLE
   03-09-2018  VARAYA     CREACI�N SP.
   ********************************************************************************************************/
   PROCEDURE SP_BUS_ACT_AREA_MN (P_COD_SIS_ENT_SAL   IN     CHAR,
                                 P_IMP_ING_ARA       IN     NUMBER,
                                 P_IMP_EGR_ARA       IN     NUMBER,
                                 p_ERROR                OUT NUMBER);

   /************************************************************************************************
   -- Funcion/Procedimiento: SP_REI_SALDO_MN
   -- Objetivo: Procedimiento que reinicia los saldos de mn
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_DETLL_SALDO_CAJA_MN_MX
   --
   -- Fecha: 24-11-2017
   -- Autor: Santander - CAH
   -- Input:
   --
   -- Output:
   --        p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE SP_REI_SLDO_MN (p_ERROR OUT NUMBER);
END PKG_PAB_CAJA_MN;
/


CREATE OR REPLACE /* Formatted on 18-08-2020 17:45:30 (QP5 v5.115.810.9015) */
PACKAGE BODY DBO_PAB.PKG_PAB_CAJA_MN
IS
   /************************************************************************************************
   -- Funcion/Procedimiento: SP_BUS_PSCON_BCCH_MN
   -- Objetivo: Procedimiento almacenado que Muestra la posici�n del Banco Central, saldos
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_DETLL_SALDO_CAJA_MN_MX
   --
   -- Fecha: 22-11-2017
   -- Autor: Santander
   -- Input: N/A
   --
   -- Output:
   --        p_CURSOR : SYS_REFCURSOR, resultados de consulta.
   --        p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE SP_BUS_PSCON_BCCH_MN (p_CURSOR   OUT SYS_REFCURSOR,
                                   p_ERROR    OUT NUMBER)
   IS
      v_NOM_SP   VARCHAR2 (30) := 'SP_BUS_PSCON_BCCH_MN';
   BEGIN
      --Obtenemos la fecha habil de ayer
      PKG_PAB_UTILITY.SP_PAB_DEV_DIA_HABIL_ANT (v_COD_PAIS, v_FECHA_AYER);

      ---Posici�n banco central
      OPEN p_CURSOR FOR
         SELECT   SL.IMP_FNL_CTL AS SALDO_APERTURA,
                  SL.IMP_ING_DIA AS INGRESO_SPAV,
                  SL.IMP_EGR_DIA AS EGRESO_SPAV,
                  SL.IMP_ABN_BCT AS ABONO,
                  SL.IMP_CGO_BCT AS CARGO,
                  SL.IMP_RAL_AOS AS SALDO_FINAL
           FROM   PABS_DT_DETLL_SALDO_CAJA_MN_MX SL
          WHERE       SL.COD_BCO_ORG = PKG_PAB_CONSTANTES.V_BIC_BCCH
                  AND SL.FEC_ING_CTL = v_FECHA_AYER
                  AND SL.COD_DVI = PKG_PAB_CONSTANTES.V_STR_DVI_CLP;

      p_ERROR := PKG_PAB_CONSTANTES.v_OK;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg :=
            'No se han encontrado registos asociados a la busqueda por banco :'
            || PKG_PAB_CONSTANTES.V_BIC_BCCH
            || '- fecha '
            || v_FECHA_AYER
            || '- divisa '
            || PKG_PAB_CONSTANTES.V_STR_DVI_CLP;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         p_s_mensaje := err_code || '-' || err_msg;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END SP_BUS_PSCON_BCCH_MN;

   /************************************************************************************************
   -- Funcion/Procedimiento: SP_BUS_DETLL_BCCH_MN
   -- Objetivo: Procedimiento almacenado que Muestra el detalle de la posici�n del Banco Central
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_DETLL_SALDO_CAJA_MN_MX
   --
   -- Fecha: 22-11-2017
   -- Autor: Santander
   -- Input: p_
   --
   -- Output:
   --        p_CURSOR : SYS_REFCURSOR, resultados de consulta.
   --        p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE SP_BUS_DETLL_BCCH_MN (p_CURSOR    OUT SYS_REFCURSOR,
                                   p_CURSOR2   OUT SYS_REFCURSOR,
                                   p_ERROR     OUT NUMBER)
   IS
      v_NOM_SP   VARCHAR2 (30) := 'SP_BUS_DETLL_BCCH_MN';
   BEGIN
      --Obtenemos la fecha habil del d�a anterior
      PKG_PAB_UTILITY.SP_PAB_DEV_DIA_HABIL_ANT (v_COD_PAIS, v_FECHA_AYER);

      BEGIN
         --Totales.
         OPEN p_CURSOR FOR
            SELECT   IMP_ING_DIA + IMP_ABN_BCT AS INGRESO,
                     IMP_EGR_DIA + IMP_CGO_BCT AS EGRESO,
                     (IMP_ING_DIA + IMP_ABN_BCT)
                     - (IMP_EGR_DIA + IMP_CGO_BCT)
                        AS POSICION
              FROM   PABS_DT_DETLL_SALDO_CAJA_MN_MX
             WHERE       COD_BCO_ORG = PKG_PAB_CONSTANTES.V_BIC_BCCH
                     AND FEC_ING_CTL = v_FECHA_AYER
                     AND COD_DVI = PKG_PAB_CONSTANTES.V_STR_DVI_CLP;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            err_code := SQLCODE;
            err_msg :=
               'No se han encontrado registos asociados a la busqueda por banco :'
               || PKG_PAB_CONSTANTES.V_BIC_BCCH
               || '- fecha '
               || v_FECHA_AYER
               || '- divisa '
               || PKG_PAB_CONSTANTES.V_STR_DVI_CLP;
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         WHEN OTHERS
         THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            p_s_mensaje := err_code || '-' || err_msg;
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
      END;


      --Detalle BCCH (grilla)
      OPEN p_CURSOR2 FOR
         SELECT   COD_BIC_BCO AS BIC,
                  TI.DES_BCO AS NOMBRE_BANCO,
                  IMP_PAG_REC AS INGRESO,
                  IMP_PAG_ENV AS EGRESO,
                  IMP_POS_BIL AS POSICION                    -- Ingreso-Egreso
           FROM   PABS_DT_SALDO_BILAT SB, TCDTBAI TI
          WHERE       TI.TGCDSWSA(+) = SB.COD_BIC_BCO
                  AND SB.FEC_ING_CTL = v_FECHA_AYER
                  AND COD_SIS_SAL = PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SAL_LBTR;



      p_ERROR := PKG_PAB_CONSTANTES.v_OK;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg :=
            'No se han encontrado registos asociados a la busqueda por fecha '
            || v_FECHA_AYER
            || '- canal de salida '
            || PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SAL_LBTR;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         p_s_mensaje := err_code || '-' || err_msg;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END SP_BUS_DETLL_BCCH_MN;

   /************************************************************************************************
   -- Funcion/Procedimiento: SP_BUS_PSCON_COMBAC_MN
   -- Objetivo: Procedimiento almacenado que Muestra la posici�n del Comban
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_DETLL_SALDO_CAJA_MN_MX
   --
   -- Fecha: 22-11-2017
   -- Autor: Santander
   -- Input: p_
   --
   -- Output:
   --        p_CURSOR : SYS_REFCURSOR, resultados de consulta.
   --        p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE SP_BUS_PSCON_COMBAC_MN (p_CURSOR   OUT SYS_REFCURSOR,
                                     p_ERROR    OUT NUMBER)
   IS
      v_NOM_SP       VARCHAR2 (30) := 'SP_BUS_PSCON_COMBAC_MN';
      v_FECHA_AYER   DATE := TRUNC (SYSDATE - 1);
   BEGIN
      --D�a Habil
      PKG_PAB_UTILITY.SP_PAB_DEV_DIA_HABIL_ANT (v_COD_PAIS, v_FECHA_AYER);

      OPEN p_CURSOR FOR
         SELECT   SL.IMP_GRT_CBA AS GARANTIA,
                  SL.IMP_ING_CBA AS INGRESOS,
                  SL.IMP_EGR_CBA AS EGRESOS,
                  SL.IMP_PSC_CBA AS SALDO_FINAL
           FROM   PABS_DT_DETLL_SALDO_CAJA_MN_MX SL
          WHERE       SL.COD_BCO_ORG = PKG_PAB_CONSTANTES.V_BIC_BCCH
                  AND SL.FEC_ING_CTL = v_FECHA_AYER
                  AND SL.COD_DVI = PKG_PAB_CONSTANTES.V_STR_DVI_CLP;

      p_ERROR := PKG_PAB_CONSTANTES.v_OK;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg :=
            'No se han encontrado registos asociados a la busqueda por fecha '
            || v_FECHA_AYER
            || '- canal de salida '
            || PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SAL_LBTR;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         p_s_mensaje := err_code || '-' || err_msg;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END SP_BUS_PSCON_COMBAC_MN;

   /************************************************************************************************
   -- Funcion/Procedimiento: SP_BUS_DETLL_COMBANC_MN
   -- Objetivo: Procedimiento almacenado que Muestra el detalle de la posici�n del Combanc
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_DETLL_SALDO_CAJA_MN_MX
   --
   -- Fecha: 22-11-2017
   -- Autor: Santander
   -- Input: p_
   --
   -- Output:
   --        p_CURSOR : SYS_REFCURSOR, resultados de consulta.
   --        p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE SP_BUS_DETLL_COMBANC_MN (p_CURSOR    OUT SYS_REFCURSOR,
                                      p_CURSOR2   OUT SYS_REFCURSOR,
                                      p_ERROR     OUT NUMBER)
   IS
      v_NOM_SP       VARCHAR2 (30) := 'SP_BUS_DETLL_COMBANC_MN';
      v_FECHA_AYER   DATE := TRUNC (SYSDATE - 1);
      v_PORCENTAJE   NUMBER := 11.5;
      v_100          NUMBER := 100;
      v_LIMITE_DEB   NUMBER;
   BEGIN
      --D�a Habil
      PKG_PAB_UTILITY.SP_PAB_DEV_DIA_HABIL_ANT (v_COD_PAIS, v_FECHA_AYER);

      BEGIN                 -- Calculamos porcentaje del mayor limite recibido
         SELECT   ( (MAX (IMP_LMT_DBT)) * v_PORCENTAJE) / v_100
           INTO   v_LIMITE_DEB
           FROM   PABS_DT_BANCO_ALMOT BA, TCDTBAI TI
          WHERE       TI.TGCDSWSA = BA.COD_BIC_BCO
                  AND (BA.IMP_LMT_CDT > v_cero OR BA.IMP_LMT_DBT > v_cero)
                  AND BA.FLG_VGN = PKG_PAB_CONSTANTES.V_REG_VIG
                  AND BA.COD_BIC_BCO NOT IN
                           (SELECT   COD_BIC_BCO
                              FROM   PABS_DT_SALDO_BILAT
                             WHERE   COD_SIS_SAL =
                                        PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SAL_CMN
                                     AND FEC_ING_CTL = v_FECHA_AYER);
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            err_code := SQLCODE;
            err_msg :=
               'No se han encontrado registos asociados a la busqueda por fecha '
               || v_FECHA_AYER;
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         WHEN OTHERS
         THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            p_s_mensaje := err_code || '-' || err_msg;
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
      END;


      ---Totales combanc
      BEGIN
         OPEN p_CURSOR FOR
            SELECT   SUM (SB.IMP_PAG_REC) AS INGRESO,
                     SUM (SB.IMP_PAG_ENV) AS EGRESO,
                     SUM (SB.IMP_PAG_REC) - SUM (SB.IMP_PAG_ENV) AS POSICION,
                     v_cero AS LIMITE_CRE,
                     --SUM(SB.IMP_CRE_REC)    AS LIMITE_DEB, --Se debe calcular
                     v_LIMITE_DEB AS LIMITE_DEB,            --Se debe calcular
                     SUM (SB.IMP_CRE_ENV)
                     + (SUM (SB.IMP_PAG_REC) - SUM (SB.IMP_PAG_ENV))
                        AS DISPONIBLE_ENV,                        --LIMCRE+POS
                     v_cero AS DISPONIBLE_REC
              --SUM(SB.IMP_CRE_REC) - (SUM(SB.IMP_PAG_REC) - SUM(SB.IMP_PAG_ENV))    AS DISPONIBLE_REC  --LIMCEB-POS
              FROM   PABS_DT_SALDO_BILAT SB
             WHERE   SB.COD_SIS_SAL =
                        PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SAL_CMN
                     AND SB.COD_DVI = PKG_PAB_CONSTANTES.V_STR_DVI_CLP
                     AND SB.FEC_ING_CTL <= v_FECHA_AYER;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            err_code := SQLCODE;
            err_msg :=
               'No se han encontrado registos asociados a la busqueda por sistema de salida '
               || PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SAL_CMN
               || ' - divisa  '
               || PKG_PAB_CONSTANTES.V_STR_DVI_CLP
               || '- fecha '
               || v_FECHA_AYER;
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         WHEN OTHERS
         THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            p_s_mensaje := err_code || '-' || err_msg;
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
      END;

      --Detalle combanc.
      OPEN p_CURSOR2 FOR
         SELECT   TI.DES_BCO AS NOMBRE_BANCO,
                  COD_BIC_BCO AS BIC,
                  IMP_PAG_REC AS INGRESO,
                  IMP_PAG_ENV AS EGRESO,
                  IMP_POS_BIL AS POSICION,                   -- Ingreso-Egreso
                  IMP_CRE_ENV AS LIMITE_CRE,
                  IMP_CRE_REC AS LIMITE_DEB,
                  IMP_DIS_ENV AS DISPONIBLE_ENV,
                  IMP_DIS_REC AS DISPONIBLE_REC
           FROM   PABS_DT_SALDO_BILAT SB, TCDTBAI TI
          WHERE   TI.TGCDSWSA = SB.COD_BIC_BCO
                  AND SB.COD_SIS_SAL =
                        PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SAL_CMN
                  AND SB.FEC_ING_CTL <= v_FECHA_AYER
         UNION ALL
         SELECT   TI.DES_BCO AS NOMBRE_BANCO,
                  BA.COD_BIC_BCO AS BIC,
                  v_cero AS INGRESO,
                  v_cero AS EGRESO,
                  v_cero AS POSICION,                        -- Ingreso-Egreso
                  IMP_LMT_CDT AS LIMITE_CRE,
                  IMP_LMT_DBT AS LIMITE_DEB,
                  IMP_LMT_CDT AS DISPONIBLE_ENV,
                  IMP_LMT_DBT AS DISPONIBLE_REC
           FROM   PABS_DT_BANCO_ALMOT BA, TCDTBAI TI
          WHERE       TI.TGCDSWSA = BA.COD_BIC_BCO
                  AND (BA.IMP_LMT_CDT > v_cero OR BA.IMP_LMT_DBT > v_cero)
                  AND BA.FLG_VGN = PKG_PAB_CONSTANTES.V_REG_VIG --El banco debe estar vigente
                  AND BA.COD_SIS_SAL =
                        PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SAL_CMN
                  AND BA.COD_BIC_BCO NOT IN
                           (SELECT   COD_BIC_BCO
                              FROM   PABS_DT_SALDO_BILAT
                             WHERE   COD_SIS_SAL =
                                        PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SAL_CMN
                                     AND FEC_ING_CTL <= v_FECHA_AYER);

      p_ERROR := PKG_PAB_CONSTANTES.v_OK;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg :=
            'No se han encontrado registos asociados a la busqueda por sistema de salida '
            || PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SAL_CMN
            || '- fecha '
            || v_FECHA_AYER;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         p_s_mensaje := err_code || '-' || err_msg;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END SP_BUS_DETLL_COMBANC_MN;

   /************************************************************************************************
   -- Funcion/Procedimiento: SP_BUS_PSCON_SINACOFI_MN
   -- Objetivo: Procedimiento almacenado que Muestra la posici�n del Sinacofi
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_DETLL_SALDO_CAJA_MN_MX
   --
   -- Fecha: 22-11-2017
   -- Autor: Santander
   -- Input: p_
   --
   -- Output:
   --        p_CURSOR : SYS_REFCURSOR, resultados de consulta.
   --        p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE SP_BUS_PSCON_SINACOFI_MN (p_CURSOR   OUT SYS_REFCURSOR,
                                       p_ERROR    OUT NUMBER)
   IS
      v_NOM_SP         VARCHAR2 (30) := 'SP_BUS_PSCON_SINACOFI_MN';
      v_FECHA_ACTUAL   DATE := TRUNC (SYSDATE);
   BEGIN
      --Obtenemos la fecha habil del d�a anterior
      PKG_PAB_UTILITY.SP_PAB_DEV_DIA_HABIL_ANT (v_COD_PAIS, v_FECHA_AYER);

      OPEN p_CURSOR FOR
         SELECT   SL.IMP_ING_SNF AS INGRESO,
                  SL.IMP_EGR_SNF AS EGRESO,
                  SL.IMP_NET_SNF AS NETO
           FROM   PABS_DT_DETLL_SALDO_CAJA_MN_MX SL
          WHERE       SL.COD_DVI = PKG_PAB_CONSTANTES.V_STR_DVI_CLP
                  AND SL.COD_BCO_ORG = PKG_PAB_CONSTANTES.V_BIC_BCCH
                  AND SL.FEC_ING_CTL = v_FECHA_AYER;


      p_ERROR := PKG_PAB_CONSTANTES.v_OK;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg :=
            'No se han encontrado registos asociados a la busqueda por banco '
            || PKG_PAB_CONSTANTES.V_BIC_BCCH
            || ' - divisa  '
            || PKG_PAB_CONSTANTES.V_STR_DVI_CLP
            || '- fecha '
            || v_FECHA_AYER;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         p_s_mensaje := err_code || '-' || err_msg;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END SP_BUS_PSCON_SINACOFI_MN;

   /************************************************************************************************
   -- Funcion/Procedimiento: SP_BUS_DETLL_SINACOFI_MN
   -- Objetivo: Procedimiento almacenado que Muestra el detalle de la posici�n del Combanc
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_DETLL_SALDO_CAJA_MN_MX,
   --
   -- Fecha: 22-11-2017
   -- Autor: Santander
   -- Input: p_
   --
   -- Output:
   --        p_CURSOR : SYS_REFCURSOR, resultados de consulta.
   --        p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE SP_BUS_DETLL_SINACOFI_MN (p_CURSOR    OUT SYS_REFCURSOR,
                                       p_CURSOR2   OUT SYS_REFCURSOR,
                                       p_ERROR     OUT NUMBER)
   IS
      v_NOM_SP         VARCHAR2 (30) := 'SP_BUS_DETLL_SINACOFI_MN';
      v_FECHA_ACTUAL   DATE := TRUNC (SYSDATE);
   BEGIN
      --Obtenemos la fecha habil del d�a anterior
      PKG_PAB_UTILITY.SP_PAB_DEV_DIA_HABIL_ANT (v_COD_PAIS, v_FECHA_AYER);

      BEGIN
         --Totales.
         OPEN p_CURSOR FOR
            SELECT   SL.IMP_ING_SNF AS INGRESO,
                     SL.IMP_EGR_SNF AS EGRESO,
                     (SL.IMP_ING_SNF) - (SL.IMP_EGR_SNF) AS POSICION -- Ingreso-Egreso
              FROM   PABS_DT_DETLL_SALDO_CAJA_MN_MX SL
             WHERE       SL.COD_BCO_ORG = PKG_PAB_CONSTANTES.V_BIC_BCCH
                     AND SL.COD_DVI = PKG_PAB_CONSTANTES.V_STR_DVI_CLP
                     AND SL.FEC_ING_CTL = v_FECHA_AYER;
      END;

      BEGIN
         --
         OPEN p_CURSOR2 FOR
            SELECT   COD_BIC_BCO AS BIC,
                     TI.DES_BCO AS NOMBRE_BANCO,
                     IMP_PAG_REC AS INGRESO,
                     IMP_PAG_ENV AS EGRESO,
                     IMP_POS_BIL AS POSICION                   -- Ingreso-Egre
              FROM   PABS_DT_SALDO_BILAT SB, TCDTBAI TI
             WHERE       TI.TGCDSWSA(+) = SB.COD_BIC_BCO --temporal el trim mientras se solicita modificacion bd
                     AND SB.FEC_ING_CTL = v_FECHA_AYER
                     AND SB.COD_SIS_SAL = v_EST_PAG_CNT; -- C�digo Pagado por contingencia
      END;

      p_ERROR := PKG_PAB_CONSTANTES.v_OK;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg :=
            'No se han encontrado registos asociados a la busqueda por banco '
            || PKG_PAB_CONSTANTES.V_BIC_BCCH
            || ' - divisa  '
            || PKG_PAB_CONSTANTES.V_STR_DVI_CLP
            || '- fecha '
            || v_FECHA_AYER;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         p_s_mensaje := err_code || '-' || err_msg;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END SP_BUS_DETLL_SINACOFI_MN;

   /************************************************************************************************
   -- Funcion/Procedimiento: SP_BUS_PRYCN_INTRADIA_MN
   -- Objetivo: Procedimiento almacenado que Muestra los saldos de Intradia
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_DETLL_SALDO_CAJA_MN_MX
   --
   -- Fecha: 22-11-2017
   -- Autor: Santander
   -- Input: p_CURSOR
   --
   -- Output:
   --        p_CURSOR : SYS_REFCURSOR, resultados de consulta.
   --        p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE SP_BUS_PRYCN_INTRADIA_MN (p_CURSOR   OUT SYS_REFCURSOR,
                                       p_ERROR    OUT NUMBER)
   IS
      v_NOM_SP         VARCHAR2 (30) := 'SP_BUS_PSCON_BBCH_MN';
      v_FECHA_ACTUAL   DATE := TRUNC (SYSDATE);
   BEGIN
      --Obtenemos la fecha habil de ayer
      PKG_PAB_UTILITY.SP_PAB_DEV_DIA_HABIL_ANT (v_COD_PAIS, v_FECHA_AYER);

      OPEN p_CURSOR FOR
         SELECT   SL.IMP_ING_CPR AS INGRESO,
                  SL.IMP_EGR_CPR AS EGRESO,
                  SL.IMP_PSC_CBA AS SALDO_COMBANC,
                  SL.IMP_TOT_FNL_DIA AS SALDO_FINAL
           FROM   PABS_DT_DETLL_SALDO_CAJA_MN_MX SL
          WHERE       SL.COD_BCO_ORG = PKG_PAB_CONSTANTES.V_BIC_BCCH
                  AND SL.FEC_ING_CTL = v_FECHA_AYER
                  AND SL.COD_DVI = PKG_PAB_CONSTANTES.V_STR_DVI_CLP;


      p_ERROR := PKG_PAB_CONSTANTES.v_OK;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg :=
            'No se han encontrado registos asociados a la busqueda por banco '
            || PKG_PAB_CONSTANTES.V_BIC_BCCH
            || ' - divisa  '
            || PKG_PAB_CONSTANTES.V_STR_DVI_CLP
            || '- fecha '
            || v_FECHA_AYER;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         p_s_mensaje := err_code || '-' || err_msg;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END SP_BUS_PRYCN_INTRADIA_MN;

   /************************************************************************************************
   -- Funcion/Procedimiento: SP_BUS_DETLL_INTRADIA_MN
   -- Objetivo: Procedimiento almacenado que Muestra el detalle de saldos Intradia
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_DETLL_SALDO_CAJA_MN_MX
   --
   -- Fecha: 22-11-2017
   -- Autor: Santander
   -- Input: p_
   --
   -- Output:
   --        p_CURSOR : SYS_REFCURSOR, resultados de consulta.
   --        p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE SP_BUS_DETLL_INTRADIA_MN (p_CURSOR    OUT SYS_REFCURSOR,
                                       p_CURSOR2   OUT SYS_REFCURSOR,
                                       p_ERROR     OUT NUMBER)
   IS
      v_NOM_SP         VARCHAR2 (30) := 'SP_BUS_DETLL_INTRADIA_MN';
      v_FECHA_ACTUAL   DATE := TRUNC (SYSDATE);
   BEGIN
      --Obtenemos la fecha habil del d�a anterior
      PKG_PAB_UTILITY.SP_PAB_DEV_DIA_HABIL_ANT (v_COD_PAIS, v_FECHA_AYER);

      BEGIN
         -- Encabezado
         OPEN p_CURSOR FOR
            SELECT   SL.IMP_ING_CPR AS INGRESO,
                     SL.IMP_EGR_CPR AS EGRESO,
                     (SL.IMP_ING_CPR) - (SL.IMP_EGR_CPR) AS POSICION -- Ingreso-Egreso
              FROM   PABS_DT_DETLL_SALDO_CAJA_MN_MX SL
             WHERE       SL.COD_BCO_ORG = PKG_PAB_CONSTANTES.V_BIC_BCCH
                     AND SL.FEC_ING_CTL = v_FECHA_AYER
                     AND SL.COD_DVI = PKG_PAB_CONSTANTES.V_STR_DVI_CLP;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            err_code := SQLCODE;
            err_msg :=
               'No se han encontrado registos asociados a la busqueda por banco '
               || PKG_PAB_CONSTANTES.V_BIC_BCCH
               || ' - divisa  '
               || PKG_PAB_CONSTANTES.V_STR_DVI_CLP
               || '- fecha '
               || v_FECHA_AYER;
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         WHEN OTHERS
         THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            p_s_mensaje := err_code || '-' || err_msg;
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
      END;

      --Detalle (grilla pantalla)
      OPEN p_CURSOR2 FOR
         SELECT   DECODE (MC.FLG_ING_EGR,
                          PKG_PAB_CONSTANTES.V_FLG_ING, MC.COD_BCO_ORG,
                          MC.COD_BCO_DTN)
                     AS BIC,                              --0 EGRESO 1 INGRESO
                  DECODE (MC.FLG_ING_EGR,
                          PKG_PAB_CONSTANTES.V_FLG_ING, BAI_ORG.DES_BCO,
                          BAI_DES.DES_BCO)
                     AS NOMBRE,
                  TO_CHAR (MC.FEC_VTA, 'dd-mm-yyyy') AS FECHA_VALUTA,
                  MC.COD_TPO_OPE_AOS AS CODIGO,
                  MC.FLG_ING_EGR AS FLG,                   --0 EGRES 1 INGRESO
                  DECODE (MC.FLG_ING_EGR, 1, MC.IMP_OPE, '0') AS INGRESO,
                  DECODE (MC.FLG_ING_EGR, 0, MC.IMP_OPE, '0') AS EGRESO,
                  --MC.COD_SIS_ENT                          AS AREA,
                  SIS.DSC_SIS_ENT_SAL AS AREA,
                  MC.GLS_MVT AS REFERENCIA_AREA,
                  MC.NUM_FOL_OPE AS FOLIO_OPE,
                  MC.FEC_INS_OPE AS FECHA_OPE,
                  MC.NUM_FOL_NMN AS FOLIO_NOM
           FROM   PABS_DT_DETLL_MVNTO_CAJA MC,
                  PABS_DT_SISTM_ENTRD_SALID SIS,
                  TCDTBAI BAI_ORG,
                  TCDTBAI BAI_DES
          WHERE       MC.COD_SIS_ENT = SIS.COD_SIS_ENT_SAL
                  AND MC.COD_DVI = PKG_PAB_CONSTANTES.V_STR_DVI_CLP
                  AND MC.COD_BCO_ORG = BAI_ORG.TGCDSWSA(+)
                  AND MC.COD_BCO_DTN = BAI_DES.TGCDSWSA(+)
                  AND MC.FEC_VTA >= v_FECHA_ACTUAL;

      p_ERROR := PKG_PAB_CONSTANTES.v_OK;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg :=
            'No se han encontrado registos asociados a la busqueda por banco '
            || PKG_PAB_CONSTANTES.V_BIC_BCCH
            || ' - divisa  '
            || PKG_PAB_CONSTANTES.V_STR_DVI_CLP
            || '- fecha '
            || v_FECHA_AYER;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         p_s_mensaje := err_code || '-' || err_msg;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END SP_BUS_DETLL_INTRADIA_MN;

   --*******************************************************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_ELI_PRYCN_MN
   -- Objetivo: Procedimiento que elimina registro de proyecciones de Caja Nacional seg�n n�mero de operaci�n y su
   --           respectiva n�mina, y luego
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_DETLL_MVNTO_CAJA
   -- Fecha: 21-11-2017
   -- Autor: Santander- CAH
   -- Input:
   -- p_NUM_FOL_NMN  -> N�nmero Folio
   -- p_NUM_FOL_OPE  -> N�mero Operaci�n
   -- p_FEC_INS_OPE  -> Fecha Ingreso Movimiento
   -- Output:
   -- p_ERROR  ->  Indicada si el procedimiento
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de utimos cambios>
   --********************************************************************************************************************************
   PROCEDURE SP_PAB_ELI_PRYCN_MN (p_NUM_FOL_NMN   IN     NUMBER,
                                  p_NUM_FOL_OPE   IN     NUMBER,
                                  p_FEC_INS_OPE   IN     DATE,
                                  p_ERROR            OUT NUMBER)
   IS
      v_NOM_SP            VARCHAR2 (30) := 'SP_PAB_ELI_PRYCN_MN';
      v_INGRESO           NUMBER;
      v_EGRESO            NUMBER;
      V_IMP_ING_CPR       NUMBER;
      V_IMP_EGR_CPR       NUMBER;
      V_IMP_TOT_FNL_DIA   NUMBER;
      V_IMP_PSC_CBA       NUMBER;
      V_IMP_APR_AOS       NUMBER;
   BEGIN
      --Eliminamos el registro de la tabla
      DELETE FROM   PABS_DT_DETLL_MVNTO_CAJA DC
            WHERE       DC.NUM_FOL_OPE = p_NUM_FOL_OPE
                    AND DC.FEC_INS_OPE = p_FEC_INS_OPE
                    AND DC.NUM_FOL_NMN = p_NUM_FOL_NMN;

      COMMIT;

      --Obtenemos la fecha habil de ayer
      PKG_PAB_UTILITY.SP_PAB_DEV_DIA_HABIL_ANT (v_COD_PAIS, v_FECHA_AYER);

      BEGIN
         --Buscamos los montos actuales para actualizar los saldo MN
         SELECT   SUM (DECODE (FLG_ING_EGR, 1, IMP_OPE, '0')),
                  SUM (DECODE (FLG_ING_EGR, 0, IMP_OPE, '0'))
           INTO   v_INGRESO, v_EGRESO
           FROM   PABS_DT_DETLL_MVNTO_CAJA DC
          WHERE   COD_DVI = PKG_PAB_CONSTANTES.V_STR_DVI_CLP
                  AND DC.FEC_VTA >= v_FECHA_ACTUAL;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            err_code := SQLCODE;
            err_msg := 'No hay registros en la tabla';
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         WHEN OTHERS
         THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            p_s_mensaje := err_code || '-' || err_msg;
            RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
      END;

      p_ERROR := PKG_PAB_CONSTANTES.v_OK;

      BEGIN
         --Obtenemos el monto de posici�n Combanc
         SELECT   IMP_PSC_CBA, IMP_APR_AOS
           INTO   V_IMP_PSC_CBA, V_IMP_APR_AOS
           FROM   PABS_DT_DETLL_SALDO_CAJA_MN_MX SN
          WHERE       COD_BCO_ORG = PKG_PAB_CONSTANTES.V_BIC_BCCH
                  AND FEC_ING_CTL = v_FECHA_AYER
                  AND COD_DVI = PKG_PAB_CONSTANTES.V_STR_DVI_CLP;

         --Realizamos los calculos
         V_IMP_ING_CPR := NVL (v_INGRESO, 0);
         V_IMP_EGR_CPR := NVL (v_EGRESO, 0);
         V_IMP_TOT_FNL_DIA :=
              NVL (V_IMP_APR_AOS, 0)
            + NVL (V_IMP_ING_CPR, 0)
            - NVL (V_IMP_EGR_CPR, 0)
            + NVL (V_IMP_PSC_CBA, 0);

         --Final D�a   = SALDO APERTURA ALTOS MONTOS + INGRESOS COMPROMETIDOS - EGRESOS COMPROMETIDOS + POSICION COMBANC

         --Actualizamos los saldos nuevos, dado la eliminaci�n
         UPDATE   PABS_DT_DETLL_SALDO_CAJA_MN_MX
            SET   IMP_ING_CPR = V_IMP_ING_CPR,
                  IMP_EGR_CPR = V_IMP_EGR_CPR,
                  IMP_TOT_FNL_DIA = V_IMP_TOT_FNL_DIA
          WHERE       COD_BCO_ORG = PKG_PAB_CONSTANTES.V_BIC_BCCH
                  AND FEC_ING_CTL = v_FECHA_AYER
                  AND COD_DVI = PKG_PAB_CONSTANTES.V_STR_DVI_CLP;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            err_code := SQLCODE;
            err_msg :=
               'No se encontro informacion para el BIC : '
               || PKG_PAB_CONSTANTES.V_BIC_BCCH;
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         WHEN OTHERS
         THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            p_s_mensaje := err_code || '-' || err_msg;
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
      END;

      p_ERROR := PKG_PAB_CONSTANTES.v_OK;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg :=
               'No se pudo eliminar la operaci�n:'
            || p_NUM_FOL_OPE
            || ' de la Nomina:'
            || p_NUM_FOL_NMN;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END SP_PAB_ELI_PRYCN_MN;

   /***************************************************************************************************************
   -- Funcion/Procedimiento: P_PAB_CAL_SLDO_FINAL_DIA_MN
   -- Objetivo: Procedimiento almacenado que actualiza saldo del campo Total final D�a de Proyecci�n
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_DETLL_SALDO_CAJA_MN_MX
   --
   -- Fecha:22-11-2017
   -- Autor: Santander - CAH
   -- Input:
   -- Output:
   --    p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --**************************************************************************************************************/
   PROCEDURE SP_PAB_CAL_SLDO_FINAL_DIA_MN (p_ERROR OUT NUMBER)
   IS
      v_NOM_SP            VARCHAR2 (30) := 'P_PAB_CAL_SLDO_FINAL_DIA_MN';

      V_IMP_ING_CPR       NUMBER;
      V_IMP_EGR_CPR       NUMBER;
      V_IMP_TOT_FNL_DIA   NUMBER;
      V_IMP_PSC_CBA       NUMBER;
      V_IMP_APR_AOS       NUMBER;
   BEGIN
      --Obtenemos la fecha habil de ayer
      PKG_PAB_UTILITY.SP_PAB_DEV_DIA_HABIL_ANT (v_COD_PAIS, v_FECHA_AYER);

      --Obtenemos el monto de posici�n Combanc
      SELECT   IMP_ING_CPR,
               IMP_EGR_CPR,
               IMP_PSC_CBA,
               IMP_APR_AOS
        INTO   V_IMP_ING_CPR,
               V_IMP_EGR_CPR,
               V_IMP_PSC_CBA,
               V_IMP_APR_AOS
        FROM   PABS_DT_DETLL_SALDO_CAJA_MN_MX SN
       WHERE       COD_BCO_ORG = PKG_PAB_CONSTANTES.V_BIC_BCCH
               AND FEC_ING_CTL = v_FECHA_AYER
               AND COD_DVI = PKG_PAB_CONSTANTES.V_STR_DVI_CLP;

      --Realizamos el calculo
      V_IMP_TOT_FNL_DIA :=
         V_IMP_APR_AOS + V_IMP_ING_CPR - V_IMP_EGR_CPR + V_IMP_PSC_CBA;

      --Total D�a  = SALDO APERTURA ALTOS MONTOS + INGRESOS COMPROMETIDOS - EGRESOS COMPROMETIDOS + POSICION COMBANC


      UPDATE   PABS_DT_DETLL_SALDO_CAJA_MN_MX
         SET   IMP_TOT_FNL_DIA = V_IMP_TOT_FNL_DIA
       WHERE       COD_BCO_ORG = PKG_PAB_CONSTANTES.V_BIC_BCCH
               AND FEC_ING_CTL = v_FECHA_AYER
               AND COD_DVI = PKG_PAB_CONSTANTES.V_STR_DVI_CLP;

      p_ERROR := PKG_PAB_CONSTANTES.v_OK;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg :=
            'No se encontro informacion para el BIC : '
            || PKG_PAB_CONSTANTES.V_BIC_BCCH;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         p_s_mensaje := err_code || '-' || err_msg;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END SP_PAB_CAL_SLDO_FINAL_DIA_MN;

   /************************************************************************************************
   -- Funcion/Procedimiento: SP_BUS_GRNTA_COMDER_MN
   -- Objetivo: Procedimiento almacenado que muestra los saldos por garantia Comder 503 recibido
                y 103 enviado.
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_DETLL_SALDO_CAJA_MN_MX
   --
   -- Fecha: 22-11-2017
   -- Autor: Santander
   -- Input: p_
   --
   -- Output:
   --        p_CURSOR : SYS_REFCURSOR, resultados de consulta.
   --        p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE SP_BUS_GRNTA_COMDER_MN (p_CURSOR   OUT SYS_REFCURSOR,
                                     p_ERROR    OUT NUMBER)
   IS
      v_NOM_SP   VARCHAR2 (30) := 'SP_BUS_GRNTA_COMDER_MN';
   ---v_FECHA_ACTUAL  DATE := TRUNC(SYSDATE);


   BEGIN
      --Obtenemos la fecha habil de ayer
      PKG_PAB_UTILITY.SP_PAB_DEV_DIA_HABIL_ANT (v_COD_PAIS, v_FECHA_AYER);

      OPEN p_CURSOR FOR
         SELECT   SL.IMP_OER_503 AS RECIBIDO, SL.IMP_OER_103 AS ENVIADO
           FROM   PABS_DT_DETLL_SALDO_CAJA_MN_MX SL
          WHERE       SL.COD_DVI = PKG_PAB_CONSTANTES.V_STR_DVI_CLP
                  AND SL.COD_BCO_ORG = PKG_PAB_CONSTANTES.V_BIC_BCCH
                  AND SL.FEC_ING_CTL = v_FECHA_AYER;


      p_ERROR := PKG_PAB_CONSTANTES.v_OK;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg :=
            'No se han encontrado registos asociados a la busqueda por banco '
            || PKG_PAB_CONSTANTES.V_BIC_BCCH
            || ' - divisa  '
            || PKG_PAB_CONSTANTES.V_STR_DVI_CLP
            || '- fecha '
            || v_FECHA_AYER;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         p_s_mensaje := err_code || '-' || err_msg;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END SP_BUS_GRNTA_COMDER_MN;

   /************************************************************************************************
   -- Funcion/Procedimiento: SP_BUS_CRTLA_BCCH_MN
   -- Objetivo: Procedimiento almacenado que muestra los saldos de la cartola del Banco central.
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_DETLL_SALDO_CAJA_MN_MX
   --
   -- Fecha: 22-11-2017
   -- Autor: Santander
   -- Input: p_
   --
   -- Output:
   --        p_CURSOR : SYS_REFCURSOR, resultados de consulta.
   --        p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE SP_BUS_CRTLA_BCCH_MN (p_CURSOR   OUT SYS_REFCURSOR,
                                   p_ERROR    OUT NUMBER)
   IS
      v_NOM_SP         VARCHAR2 (30) := 'SP_BUS_CRTLA_BCCH_MN';
      v_FECHA_ACTUAL   DATE := TRUNC (SYSDATE);
   BEGIN
      --Obtenemos la fecha habil de ayer
      PKG_PAB_UTILITY.SP_PAB_DEV_DIA_HABIL_ANT (v_COD_PAIS, v_FECHA_AYER);

      OPEN p_CURSOR FOR
         SELECT   SL.IMP_INI_CTL AS SALDO_INICIAL,
                  SL.IMP_FNL_CTL AS SALDO_FINAL
           FROM   PABS_DT_DETLL_SALDO_CAJA_MN_MX SL
          WHERE       SL.COD_DVI = PKG_PAB_CONSTANTES.V_STR_DVI_CLP
                  AND SL.COD_BCO_ORG = PKG_PAB_CONSTANTES.V_BIC_BCCH
                  AND SL.FEC_ING_CTL = v_FECHA_AYER;


      p_ERROR := PKG_PAB_CONSTANTES.v_OK;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg :=
            'No se han encontrado registos asociados a la busqueda por banco '
            || PKG_PAB_CONSTANTES.V_BIC_BCCH
            || ' - divisa  '
            || PKG_PAB_CONSTANTES.V_STR_DVI_CLP
            || '- fecha '
            || v_FECHA_AYER;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         p_s_mensaje := err_code || '-' || err_msg;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END SP_BUS_CRTLA_BCCH_MN;

   /************************************************************************************************
   -- Funcion/Procedimiento: SP_DETLL_CTCTE_BCCH_MN
   -- Objetivo: Procedimiento almacenado que Muestra detalle de Cartola Banco Central de Chile
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_CBCRA_CRTLA
   --                PABS_DT_DETLL_CRTLA
   --
   -- Fecha: 22-11-2017
   -- Autor: Santander
   -- Input: p_
   --
   -- Output:
   --        p_CURSOR : SYS_REFCURSOR, resultados de consult cabecera.
   --        p_CURSOR2 : SYS_REFCURSOR, resultados de consulta detalle.
   --        p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE SP_DETLL_CTCTE_BCCH_MN (p_CURSOR    OUT SYS_REFCURSOR,
                                     p_CURSOR2   OUT SYS_REFCURSOR,
                                     p_ERROR     OUT NUMBER)
   IS
      v_NOM_SP          VARCHAR2 (30) := 'SP_DETLL_CTCTE_BCCH_MN';
      V_FOLIO_CARTOLA   NUMBER (12);
   BEGIN
      --Obtenemos la fecha habil de ayer
      PKG_PAB_UTILITY.SP_PAB_DEV_DIA_HABIL_ANT (v_COD_PAIS, v_FECHA_AYER);

      BEGIN
         --Cursor para cabecera de pagina
         OPEN p_CURSOR FOR
            SELECT   CB.COD_BCO_ORG AS BIC,
                     TI.DES_BCO AS NOMBRE_BCO,
                     CB.NUM_CTA_CTE AS CUENTA,
                     CB.COD_DVI AS MONEDA,
                     TO_CHAR (CB.FEC_ING_CTL, 'DD-MM-YYYY') AS FECHA,
                        SUBSTR (LPAD (CB.HOR_ING_CTL, 4, 0), 1, 2)
                     || ':'
                     || SUBSTR (LPAD (CB.HOR_ING_CTL, 4, 0), 3, 2)
                        AS HORA,
                     CB.IMP_INI_CTL AS SALDO_INICIAL,
                     CB.IMP_ING_CTL AS INGRESO,
                     CB.IMP_EGR_CTL AS EGRESO,
                     CB.IMP_FNL_CTL AS SALDO_FINAL
              FROM   PABS_DT_CBCRA_CRTLA CB, TCDTBAI TI
             WHERE       TI.TGCDSWSA = CB.COD_BCO_ORG
                     --AND CB.FEC_ING_CTL = v_FECHA_AYER
                     AND CB.COD_BCO_ORG = PKG_PAB_CONSTANTES.V_BIC_BCCH
                     AND CB.COD_DVI = PKG_PAB_CONSTANTES.V_STR_DVI_CLP;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            err_code := SQLCODE;
            err_msg :=
               'No se han encontrado registos asociados a la busqueda por banco '
               || PKG_PAB_CONSTANTES.V_BIC_BCCH
               || ' - divisa  '
               || PKG_PAB_CONSTANTES.V_STR_DVI_CLP
               || '- fecha '
               || v_FECHA_AYER;
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         WHEN OTHERS
         THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            p_s_mensaje := err_code || '-' || err_msg;
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
      END;

      BEGIN
         --Cursor para detalle de Caja moneda nacional-->Resumen-->Cartola de cuenta corriente corresponsal
         OPEN p_CURSOR2 FOR
            SELECT   DC.NUM_REF_ITN_CTA AS REFER,
                     TO_CHAR (DC.FEC_TRN_OPE, 'DD-MM-YYYY') AS FECHA,
                     TO_CHAR (DC.FEC_TRN_OPE, 'HH24:MI') AS HORA,
                     DECODE (DC.FLG_DEB_CRE, 0, DC.IMP_TRN, '0') AS MONTO_ING,
                     DECODE (DC.FLG_DEB_CRE, 1, DC.IMP_TRN, '0') AS MONTO_EGR,
                     DECODE (FLG_DEB_CRE,
                             0, SUBSTR (GLS_TRN, 16, 8),
                             SUBSTR (GLS_TRN, 5, 8))
                        AS GLOSA
              FROM   PABS_DT_CBCRA_CRTLA CB, PABS_DT_DETLL_CRTLA DC
             WHERE       CB.COD_BCO_ORG = PKG_PAB_CONSTANTES.V_BIC_BCCH
                     AND CB.COD_DVI = PKG_PAB_CONSTANTES.V_STR_DVI_CLP
                     AND CB.FEC_ING_CTL = DC.FEC_ING_CTL
                     AND CB.NUM_CTA_CTE = DC.NUM_CTA_CTE
                     AND CB.COD_BCO_ORG = DC.COD_BCO_ORG
                     AND DC.FEC_TRN_OPE >= v_FECHA_AYER;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            err_code := SQLCODE;
            err_msg :=
               'No se han encontrado registos asociados a la busqueda por banco '
               || PKG_PAB_CONSTANTES.V_BIC_BCCH
               || ' - divisa  '
               || PKG_PAB_CONSTANTES.V_STR_DVI_CLP
               || '- fecha '
               || v_FECHA_AYER;
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         WHEN OTHERS
         THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            p_s_mensaje := err_code || '-' || err_msg;
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
      END;

      p_ERROR := PKG_PAB_CONSTANTES.v_OK;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         p_s_mensaje := err_code || '-' || err_msg;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END SP_DETLL_CTCTE_BCCH_MN;

   /************************************************************************************************
   -- Funcion/Procedimiento: SP_BUS_SALDO_OPRCN_MN
   -- Objetivo: Procedimiento almacenado que muestras los saldos por Operaci�n Swift MT
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_DETLL_SALDO_CAJA_MN_MX
   --
   -- Fecha: 22-11-2017
   -- Autor: Santander
   -- Input: p_
   --
   -- Output:
   --        p_CURSOR : SYS_REFCURSOR, resultados de consulta.
   --        p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE SP_BUS_SALDO_OPRCN_MN (p_CURSOR   OUT SYS_REFCURSOR,
                                    p_ERROR    OUT NUMBER)
   IS
      v_NOM_SP         VARCHAR2 (30) := 'SP_BUS_SALDO_OPRCN_MN';
      v_FECHA_ACTUAL   DATE := TRUNC (SYSDATE);
   BEGIN
      --Obtenemos la fecha habil de ayer
      PKG_PAB_UTILITY.SP_PAB_DEV_DIA_HABIL_ANT (v_COD_PAIS, v_FECHA_AYER);

      OPEN p_CURSOR FOR
         SELECT   SL.IMP_ING_103 AS ING_103,
                  SL.IMP_EGR_103 AS EGR_103,
                  SL.IMP_ING_200 AS ING_200,
                  SL.IMP_EGR_200 AS EGR_200,
                  SL.IMP_ING_202 AS ING_202,
                  SL.IMP_EGR_202 AS EGR_202,
                  SL.IMP_ING_205 AS ING_205,
                  SL.IMP_EGR_205 AS EGR_205,
                  SL.IMP_CGO_900 AS EGR_900,
                  SL.IMP_ABN_910 AS ING_910
           FROM   PABS_DT_DETLL_SALDO_CAJA_MN_MX SL
          WHERE       SL.COD_BCO_ORG = PKG_PAB_CONSTANTES.V_BIC_BCCH
                  AND SL.FEC_ING_CTL = v_FECHA_AYER
                  AND SL.COD_DVI = PKG_PAB_CONSTANTES.V_STR_DVI_CLP;

      p_ERROR := PKG_PAB_CONSTANTES.v_OK;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg :=
            'No se han encontrado registos asociados a la busqueda por banco '
            || PKG_PAB_CONSTANTES.V_BIC_BCCH
            || ' - divisa  '
            || PKG_PAB_CONSTANTES.V_STR_DVI_CLP
            || '- fecha '
            || v_FECHA_AYER;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         p_s_mensaje := err_code || '-' || err_msg;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END SP_BUS_SALDO_OPRCN_MN;

   /************************************************************************************************
   -- Funcion/Procedimiento: SP_BUS_SALDO_AREA_MN
   -- Objetivo: Procedimiento almacenado que muestra los saldos por �rea
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_SALDO_AREA
   --               PABS_DT_SISTM_ENTRD_SALID
   -- Fecha: 22-11-2017
   -- Autor: Santander
   -- Input: p_
   --
   -- Output:
   --        p_CURSOR : SYS_REFCURSOR, resultados de consulta.
   --        p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE SP_BUS_SALDO_AREA_MN (p_CURSOR   OUT SYS_REFCURSOR,
                                   p_ERROR    OUT NUMBER)
   IS
      v_NOM_SP         VARCHAR2 (30) := 'SP_BUS_SALDO_AREA_MN';
      v_FECHA_ACTUAL   DATE := TRUNC (SYSDATE);
   BEGIN
      OPEN p_CURSOR FOR
         SELECT   SI.DSC_SIS_ENT_SAL AS AREA,
                  SA.IMP_ING_ARA AS INGRESOS,
                  SA.IMP_EGR_ARA AS EGRSOS
           FROM   PABS_DT_SALDO_AREA SA, PABS_DT_SISTM_ENTRD_SALID SI
          WHERE   SA.COD_SIS_ENT_SAL = SI.COD_SIS_ENT_SAL;

      p_ERROR := PKG_PAB_CONSTANTES.v_OK;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg :=
            'No se han encontrado registos asociados a la busqueda sistema de salida/entrada';
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         p_s_mensaje := err_code || '-' || err_msg;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END SP_BUS_SALDO_AREA_MN;

   /************************************************************************************************
   -- Funcion/Procedimiento: SP_GES_CAJA_EGRESO_MN
   -- Objetivo: Procedimiento almacenado que actualiza saldos de Combanc o LBTR.
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_DETLL_OPRCN
   --
   -- Fecha: 16/06/17
   -- Autor: Santander - CAH
   -- Input:
   --           p_NUM_REF_SWF->   Numero referencia Swift
   --           p_COD_SIS_SAL->   Sistema salida LBTR o Combanc
   -- Output:
   --        p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE SP_GES_CAJA_EGRESO_MN (p_NUM_REF_SWF   IN     CHAR,
                                    p_ERROR            OUT NUMBER)
   IS
      v_NOM_SP            VARCHAR2 (30) := 'SP_GES_CAJA_EGRESO_MN';
      v_NUM_REF_SWF       CHAR (16);
      v_NUM_FOL_OPE       NUMBER;
      v_FEC_ISR_OPE       DATE;
      v_IMP_OPE           NUMBER (15, 2);
      v_COD_DVI           CHAR (3);
      v_COD_SIS_SAL       CHAR (10);
      v_COD_SIS_ENT       CHAR (10);
      v_COD_BCO_DTN       CHAR (11);
      v_COD_MT_SWF        NUMBER (3);
      v_COD_TPO_OPE_AOS   CHAR (8);
      v_IMP_OER_103       NUMBER (15, 2);
      V_COD_EST_AOS       NUMBER (5);
   BEGIN
      BEGIN
         --Obtenemos la informacion de la operaci�n origen
         SELECT   OPE.NUM_FOL_OPE,
                  OPE.FEC_ISR_OPE,
                  OPE.IMP_OPE,
                  OPE.COD_SIS_SAL,
                  OPE.COD_DVI,
                  OPE.COD_BCO_DTN,
                  OPE.COD_MT_SWF,
                  OPE.COD_TPO_OPE_AOS,
                  OPE.COD_EST_AOS,
                  OPE.COD_SIS_ENT
           INTO   v_NUM_FOL_OPE,
                  v_FEC_ISR_OPE,
                  v_IMP_OPE,
                  v_COD_SIS_SAL,
                  v_COD_DVI,
                  v_COD_BCO_DTN,
                  v_COD_MT_SWF,
                  v_COD_TPO_OPE_AOS,
                  V_COD_EST_AOS,
                  v_COD_SIS_ENT
           FROM   PABS_DT_DETLL_OPRCN OPE
          WHERE   OPE.NUM_REF_SWF = p_NUM_REF_SWF
                  AND OPE.FLG_EGR_ING = PKG_PAB_CONSTANTES.V_FLG_EGR; --Solo pagos enviados
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            err_code := SQLCODE;
            err_msg :=
               'No se pudo obtener la informaci�n de la operaci�n origen:'
               || p_NUM_REF_SWF;
            RAISE;
         WHEN OTHERS
         THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            p_s_mensaje := err_code || '-' || err_msg;
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
      END;

      --Calculamos e insertamos el salso bilateral para combanc y lbtr
      SP_PAB_CALCULA_SALDO_BILATERAL (v_IMP_OPE,
                                      v_COD_BCO_DTN,
                                      PKG_PAB_CONSTANTES.V_FLG_EGR,
                                      v_COD_SIS_SAL,
                                      p_ERROR);

      --Actualizamos los saldos monitor swift
      CASE
         WHEN v_COD_MT_SWF = PKG_PAB_CONSTANTES.V_COD_MT103
         THEN
            SP_PAB_CAL_SLDO_MNT_SWF (v_IMP_OPE,
                                     v_cero,
                                     v_cero,
                                     v_cero,
                                     v_cero,
                                     PKG_PAB_CONSTANTES.V_FLG_EGR,
                                     p_ERROR);
         WHEN v_COD_MT_SWF = PKG_PAB_CONSTANTES.V_COD_MT202
         THEN
            SP_PAB_CAL_SLDO_MNT_SWF (v_cero,
                                     v_IMP_OPE,
                                     v_cero,
                                     v_cero,
                                     v_cero,
                                     PKG_PAB_CONSTANTES.V_FLG_EGR,
                                     p_ERROR);
         WHEN v_COD_MT_SWF = PKG_PAB_CONSTANTES.V_COD_MT205
         THEN
            SP_PAB_CAL_SLDO_MNT_SWF (v_cero,
                                     v_cero,
                                     v_IMP_OPE,
                                     v_cero,
                                     v_cero,
                                     PKG_PAB_CONSTANTES.V_FLG_EGR,
                                     p_ERROR);
      END CASE;

      --Calculamos Saldos Bilaterales
      IF (v_COD_SIS_SAL = PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SAL_LBTR)
      THEN
         --Actualizamos posicion banco central
         SP_PAB_CAL_SLDO_POS_LBTR (v_cero,
                                   v_IMP_OPE,
                                   v_cero,
                                   v_cero,
                                   p_ERROR);
      ELSIF (v_COD_SIS_SAL = PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SAL_CMN)
      THEN
         --Actualizamos posicion banco combanc
         SP_PAB_CAL_SLDO_POS_CMN (v_cero, v_IMP_OPE, p_ERROR);
      END IF;

      --Obtenemos la fecha habil de ayer
      PKG_PAB_UTILITY.SP_PAB_DEV_DIA_HABIL_ANT (v_COD_PAIS, v_FECHA_AYER);

      --Actualizamos si es un mt de garantia Combanc
      IF v_COD_MT_SWF = PKG_PAB_CONSTANTES.V_COD_MT202
         AND v_COD_TPO_OPE_AOS = PKG_PAB_CONSTANTES.V_STR_TPO_OPE_CDLE
      THEN
         UPDATE   PABS_DT_DETLL_SALDO_CAJA_MN_MX SL
            SET   IMP_GRT_CBA = v_IMP_OPE
          WHERE       COD_BCO_ORG = PKG_PAB_CONSTANTES.V_BIC_BCCH
                  AND COD_DVI = PKG_PAB_CONSTANTES.V_STR_DVI_CLP
                  AND SL.FEC_ING_CTL = v_FECHA_AYER;
      END IF;

      --Actualizamos si es un mt de garantia Comder 103
      IF v_COD_MT_SWF = PKG_PAB_CONSTANTES.V_COD_MT103
         AND (v_COD_TPO_OPE_AOS = PKG_PAB_CONSTANTES.V_COMDERM
              OR v_COD_TPO_OPE_AOS = PKG_PAB_CONSTANTES.V_COMDERG)
      THEN
         --Buscamos saldo actual de un 103 comder
         BEGIN
            SELECT   SN.IMP_OER_103
              INTO   v_IMP_OER_103
              FROM   PABS_DT_DETLL_SALDO_CAJA_MN_MX SN
             WHERE   SN.COD_BCO_ORG = PKG_PAB_CONSTANTES.V_BIC_BCCH; --'CDERCLR0';
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               err_code := SQLCODE;
               err_msg :=
                     'No existe registro para el banco: '
                  || PKG_PAB_CONSTANTES.V_BIC_BCCH
                  || ' del d�a';
               PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                   v_NOM_PCK,
                                                   err_msg,
                                                   v_NOM_SP);
               p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
               RAISE;
            WHEN OTHERS
            THEN
               err_code := SQLCODE;
               err_msg := SUBSTR (SQLERRM, 1, 300);
               p_s_mensaje := err_code || '-' || err_msg;
               PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                   v_NOM_PCK,
                                                   err_msg,
                                                   v_NOM_SP);
               p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
               RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
         END;

         UPDATE   PABS_DT_DETLL_SALDO_CAJA_MN_MX SC
            SET   SC.IMP_OER_103 = v_IMP_OPE + v_IMP_OER_103
          WHERE       COD_BCO_ORG = PKG_PAB_CONSTANTES.V_BIC_BCCH -- CDERCLR0 comder, COMDERM  tipo ope
                  AND COD_DVI = PKG_PAB_CONSTANTES.V_STR_DVI_CLP
                  AND SC.FEC_ING_CTL = v_FECHA_AYER;
      END IF;

      --Calculamos el saldo por area
      SP_BUS_CALCULA_AREA_MN (v_COD_SIS_ENT,
                              v_cero,
                              v_IMP_OPE,
                              p_ERROR);
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         p_s_mensaje := err_code || '-' || err_msg;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END SP_GES_CAJA_EGRESO_MN;

   /************************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_CALCULA_SALDO_BILATERAL
   -- Objetivo: Procedimiento almacenado que actualiza saldos bilaterales para LBTR y Combanc
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_BANCO_ALMOT
   --                PABS_DT_SALDO_BILAT
   --
   -- Fecha:14-11-2017
   -- Autor: Santander- CAH
   -- Input_
   -- p_MONTO     -->Monto de la operaci�n
   -- p_BANCO_ORG_DET     -->Banco del movimiento
   -- p_FLAG_EGR_ING      -->Flag de ingreso o egreso
   -- p_CANAL_ENT_SAL     -->Canal del movimiento (LBTR o COMBANC)
   -- Output:
   --        p_CURSOR : SYS_REFCURSOR, resultados de consulta.
   --        p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE SP_PAB_CALCULA_SALDO_BILATERAL (p_MONTO           IN     NUMBER,
                                             p_BANCO_ORG_DET   IN     CHAR,
                                             p_FLAG_EGR_ING    IN     NUMBER,
                                             p_CANAL_ENT_SAL   IN     CHAR,
                                             p_ERROR              OUT NUMBER)
   IS
      v_NOM_SP           VARCHAR2 (30) := 'SP_PAB_CALCULA_SALDO_BILATERAL';
      v_BANCO            NUMBER;

      v_MONTO_ENVIADO    NUMBER := 0;
      v_MONTO_RECIBIDO   NUMBER := 0;
      v_IMP_PAG_ENV      NUMBER := 0;
      v_IMP_PAG_REC      NUMBER := 0;

      v_IMP_POS_BIL      NUMBER := 0;
      v_IMP_DIS_ENV      NUMBER := 0;
      v_IMP_DIS_REC      NUMBER := 0;

      --Valores se combanc
      v_IMP_CRE_REC      NUMBER := 0;
      v_IMP_CRE_ENV      NUMBER := 0;

      V_BIC_BCCH_A       CHAR (11) := 'BCECCLR0   ';
      V_BIC_BCCH_B       CHAR (11) := 'BCECCLRM   ';
      v_BANCO_ORG_DET    CHAR (11);
   BEGIN
      --GCP
      err_msg := 'Inicio SP_PAB_CALCULA_SALDO_BILATERAL';
      PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                          v_NOM_PCK,
                                          err_msg,
                                          v_NOM_SP);


      --Obtenemos la fecha habil de ayer
      PKG_PAB_UTILITY.SP_PAB_DEV_DIA_HABIL_ANT (v_COD_PAIS, v_FECHA_AYER);

      -- Egreso --
      IF p_FLAG_EGR_ING = PKG_PAB_CONSTANTES.V_FLG_EGR
      THEN                                                   -- 0 Pago enviado
         v_IMP_PAG_ENV := p_MONTO;
         v_IMP_POS_BIL := -p_MONTO;
      ELSE
         -- Ingreso --
         v_IMP_PAG_REC := p_MONTO;
         v_IMP_POS_BIL := p_MONTO;
      END IF;

      --En caso de utilizar
      IF (p_BANCO_ORG_DET = V_BIC_BCCH_A OR p_BANCO_ORG_DET = V_BIC_BCCH_B)
      THEN
         v_BANCO_ORG_DET := PKG_PAB_CONSTANTES.V_BIC_BCCH;
      ELSE
         v_BANCO_ORG_DET := p_BANCO_ORG_DET;
      END IF;

      IF (p_CANAL_ENT_SAL = PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SAL_CMN)
      THEN
         --Buscamos los limites de cr�ditos entre bancos si es combanc
         BEGIN
            SELECT   BN.IMP_LMT_CDT, BN.IMP_LMT_DBT
              INTO   v_IMP_CRE_REC, v_IMP_CRE_ENV
              FROM   PABS_DT_BANCO_ALMOT BN
             WHERE   BN.COD_BIC_BCO = v_BANCO_ORG_DET;
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               err_code := SQLCODE;
               err_msg :=
                  'No Existe limites de credito para el banco: '
                  || p_BANCO_ORG_DET;
               RAISE;
            WHEN OTHERS
            THEN
               err_code := SQLCODE;
               err_msg := SUBSTR (SQLERRM, 1, 300);
               p_s_mensaje := err_code || '-' || err_msg;
               PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                   v_NOM_PCK,
                                                   err_msg,
                                                   v_NOM_SP);
               p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
               RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
         END;

         --Calculamos los disponibles
         v_IMP_DIS_ENV := v_IMP_CRE_REC + v_IMP_POS_BIL;
         v_IMP_DIS_REC := v_IMP_CRE_ENV - v_IMP_POS_BIL;
      ELSE
         v_IMP_CRE_REC := v_cero;
         v_IMP_CRE_ENV := v_cero;
      END IF;

      --Insertamos de acuerdo a los datos de entrada
      INSERT INTO PABS_DT_SALDO_BILAT (FEC_ING_CTL,
                                       COD_BIC_BCO,
                                       COD_DVI,
                                       COD_SIS_SAL,
                                       IMP_PAG_REC,
                                       IMP_PAG_ENV,
                                       IMP_POS_BIL,
                                       IMP_CRE_ENV,
                                       IMP_CRE_REC,
                                       IMP_DIS_ENV,
                                       IMP_DIS_REC)
        VALUES   (v_FECHA_AYER,
                  v_BANCO_ORG_DET,
                  PKG_PAB_CONSTANTES.V_STR_DVI_CLP,
                  p_CANAL_ENT_SAL,
                  v_IMP_PAG_REC,
                  v_IMP_PAG_ENV,
                  v_IMP_POS_BIL,
                  v_IMP_CRE_ENV,
                  v_IMP_CRE_REC,
                  v_IMP_DIS_ENV,
                  v_IMP_DIS_REC);

      p_ERROR := PKG_PAB_CONSTANTES.v_OK;
   EXCEPTION
      WHEN DUP_VAL_ON_INDEX
      THEN
         -- Si el registro ya existia se buscan los datos para calcular y actualizar
         BEGIN
            SELECT   IMP_PAG_ENV,
                     IMP_PAG_REC,
                     IMP_POS_BIL,
                     IMP_DIS_ENV,
                     IMP_DIS_REC,
                     IMP_CRE_REC,
                     IMP_CRE_ENV
              INTO   v_IMP_PAG_ENV,
                     v_IMP_PAG_REC,
                     v_IMP_POS_BIL,
                     v_IMP_DIS_ENV,
                     v_IMP_DIS_REC,
                     v_IMP_CRE_REC,
                     v_IMP_CRE_ENV
              FROM   PABS_DT_SALDO_BILAT SB
             WHERE       COD_BIC_BCO = v_BANCO_ORG_DET       --p_BANCO_ORG_DET
                     AND COD_SIS_SAL = p_CANAL_ENT_SAL
                     AND FEC_ING_CTL = v_FECHA_AYER;
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               err_code := SQLCODE;
               err_msg :=
                     'No Existe registro para el banco: '
                  || p_BANCO_ORG_DET
                  || ' del d�a';
               PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                   v_NOM_PCK,
                                                   err_msg,
                                                   v_NOM_SP);
               p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
               RAISE;
            WHEN OTHERS
            THEN
               err_code := SQLCODE;
               err_msg := SUBSTR (SQLERRM, 1, 300);
               p_s_mensaje := err_code || '-' || err_msg;
               PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                   v_NOM_PCK,
                                                   err_msg,
                                                   v_NOM_SP);
               p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
               RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
         END;

         -- Egreso
         IF p_FLAG_EGR_ING = PKG_PAB_CONSTANTES.V_FLG_EGR
         THEN                                                -- 0 Pago enviado
            v_IMP_PAG_ENV := v_IMP_PAG_ENV + p_MONTO;
            v_IMP_POS_BIL := v_IMP_PAG_REC - v_IMP_PAG_ENV;
         ELSE
            --Ingreso

            v_IMP_PAG_REC := v_IMP_PAG_REC + p_MONTO;
            v_IMP_POS_BIL := v_IMP_PAG_REC - v_IMP_PAG_ENV;
         END IF;


         IF (p_CANAL_ENT_SAL = PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SAL_CMN)
         THEN
            --Cuando ingresamos datos de Combanc
            v_IMP_DIS_ENV := v_IMP_CRE_REC + v_IMP_POS_BIL;
            v_IMP_DIS_REC := v_IMP_CRE_ENV - v_IMP_POS_BIL;
         ELSE
            --Si no es combanc dejamos valores en cero
            v_IMP_DIS_ENV := v_cero;
            v_IMP_DIS_REC := v_cero;
         END IF;

         --Actualizamos saldos
         UPDATE   PABS_DT_SALDO_BILAT SB
            SET   IMP_PAG_ENV = v_IMP_PAG_ENV,
                  IMP_PAG_REC = v_IMP_PAG_REC,
                  IMP_POS_BIL = v_IMP_POS_BIL,
                  IMP_DIS_ENV = v_IMP_DIS_ENV,
                  IMP_DIS_REC = v_IMP_DIS_REC
          WHERE       COD_BIC_BCO = v_BANCO_ORG_DET          --p_BANCO_ORG_DET
                  AND COD_SIS_SAL = p_CANAL_ENT_SAL
                  AND SB.FEC_ING_CTL = v_FECHA_AYER;

         p_ERROR := PKG_PAB_CONSTANTES.v_OK;
      WHEN NO_DATA_FOUND
      THEN
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         p_s_mensaje := err_code || '-' || err_msg;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END SP_PAB_CALCULA_SALDO_BILATERAL;

   /************************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_CALCULA_SALDO_CAJA
   -- Objetivo: Procedimiento almacenado que actualiza saldos de caja
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_DETLL_SALDO_CAJA_MN_MX
   --
   -- Fecha:14-11-2017
   -- Autor: Gonzalo Correa
   -- Input: P_IMP_ING_DIA    --> Monto Ingreso Altos Montos
   --        P_IMP_EGR_DIA    --> Montos Egreso Altos Montos
   --        P_IMP_ABN_BCT    --> Abonos Directos al central
   --        P_IMP_CGO_BCT    --> Cargos directos al central
   -- Output:
   --    p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE SP_PAB_CAL_SLDO_POS_LBTR (P_IMP_ING_DIA   IN     NUMBER,
                                       P_IMP_EGR_DIA   IN     NUMBER,
                                       P_IMP_ABN_BCT   IN     NUMBER,
                                       P_IMP_CGO_BCT   IN     NUMBER,
                                       p_ERROR            OUT NUMBER)
   IS
      v_NOM_SP        VARCHAR2 (30) := 'SP_PAB_CAL_SLDO_POS_LBTR';

      V_IMP_ING_DIA   NUMBER;
      V_IMP_EGR_DIA   NUMBER;
      V_IMP_ABN_BCT   NUMBER;
      V_IMP_CGO_BCT   NUMBER;
      V_IMP_RAL_AOS   NUMBER;
      V_IMP_FNL_CTL   NUMBER;
   BEGIN
      --Obtenemos la fecha habil de ayer
      PKG_PAB_UTILITY.SP_PAB_DEV_DIA_HABIL_ANT (v_COD_PAIS, v_FECHA_AYER);

      SELECT   IMP_FNL_CTL,
               IMP_ING_DIA,
               IMP_EGR_DIA,
               IMP_ABN_BCT,
               IMP_CGO_BCT
        INTO   V_IMP_FNL_CTL,
               V_IMP_ING_DIA,
               V_IMP_EGR_DIA,
               V_IMP_ABN_BCT,
               V_IMP_CGO_BCT
        FROM   PABS_DT_DETLL_SALDO_CAJA_MN_MX
       WHERE       COD_BCO_ORG = PKG_PAB_CONSTANTES.V_BIC_BCCH
               AND FEC_ING_CTL = v_FECHA_AYER
               AND COD_DVI = PKG_PAB_CONSTANTES.V_STR_DVI_CLP;

      --Realizamos los calculos
      V_IMP_ING_DIA := V_IMP_ING_DIA + P_IMP_ING_DIA;
      V_IMP_EGR_DIA := V_IMP_EGR_DIA + p_IMP_EGR_DIA;
      V_IMP_ABN_BCT := V_IMP_ABN_BCT + P_IMP_ABN_BCT;
      V_IMP_CGO_BCT := V_IMP_CGO_BCT + P_IMP_CGO_BCT;
      V_IMP_RAL_AOS :=
           V_IMP_FNL_CTL
         + V_IMP_ING_DIA
         - V_IMP_EGR_DIA
         + V_IMP_ABN_BCT
         - V_IMP_CGO_BCT;

      --GCP Eliminar
      err_msg :=
            'Calculos: V_IMP_ING_DIA'
         || V_IMP_ING_DIA
         || ' V_IMP_EGR_DIA:'
         || V_IMP_EGR_DIA
         || ' V_IMP_ABN_BCT:'
         || V_IMP_ABN_BCT
         || ' V_IMP_CGO_BCT:'
         || V_IMP_CGO_BCT
         || ' V_IMP_RAL_AOS:'
         || V_IMP_RAL_AOS;
      PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                          v_NOM_PCK,
                                          err_msg,
                                          v_NOM_SP);

      UPDATE   PABS_DT_DETLL_SALDO_CAJA_MN_MX
         SET   IMP_ING_DIA = V_IMP_ING_DIA,
               IMP_EGR_DIA = V_IMP_EGR_DIA,
               IMP_ABN_BCT = V_IMP_ABN_BCT,
               IMP_CGO_BCT = V_IMP_CGO_BCT,
               IMP_RAL_AOS = V_IMP_RAL_AOS
       WHERE       COD_BCO_ORG = PKG_PAB_CONSTANTES.V_BIC_BCCH
               AND FEC_ING_CTL = v_FECHA_AYER
               AND COD_DVI = PKG_PAB_CONSTANTES.V_STR_DVI_CLP;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg :=
            'No se encontro informacion para el BIC : '
            || PKG_PAB_CONSTANTES.V_BIC_BCCH;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         p_s_mensaje := err_code || '-' || err_msg;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END;


   /************************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_CAL_SLDO_POS_CMN
   -- Objetivo: Procedimiento almacenado que actualiza saldos de caja
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_DETLL_SALDO_CAJA_MN_MX
   --
   -- Fecha:14-11-2017
   -- Autor: Gonzalo Correa
   -- Input: P_IMP_ING_CBA    -->Monto ingreso combanc
   --        P_IMP_EGR_CBA    -->Monto egreso combanc
   -- Output:
   --    p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE SP_PAB_CAL_SLDO_POS_CMN (P_IMP_ING_CBA   IN     NUMBER,
                                      P_IMP_EGR_CBA   IN     NUMBER,
                                      P_ERROR            OUT NUMBER)
   IS
      v_NOM_SP            VARCHAR2 (30) := 'SP_PAB_CAL_SLDO_POS_CMN';

      V_IMP_ING_CBA       NUMBER;
      V_IMP_EGR_CBA       NUMBER;
      V_IMP_PSC_CBA       NUMBER;
      V_IMP_ING_CPR       NUMBER;
      V_IMP_EGR_CPR       NUMBER;
      V_IMP_FNL_CTL       NUMBER;
      V_IMP_TOT_FNL_DIA   NUMBER;
   BEGIN
      --Obtenemos la fecha habil de ayer
      PKG_PAB_UTILITY.SP_PAB_DEV_DIA_HABIL_ANT (v_COD_PAIS, v_FECHA_AYER);

      SELECT   IMP_ING_CBA,
               IMP_EGR_CBA,
               IMP_PSC_CBA,
               IMP_ING_CPR,
               IMP_EGR_CPR,
               IMP_FNL_CTL
        INTO   V_IMP_ING_CBA,
               V_IMP_EGR_CBA,
               V_IMP_PSC_CBA,
               V_IMP_ING_CPR,
               V_IMP_EGR_CPR,
               V_IMP_FNL_CTL
        FROM   PABS_DT_DETLL_SALDO_CAJA_MN_MX
       WHERE       COD_BCO_ORG = PKG_PAB_CONSTANTES.V_BIC_BCCH
               AND FEC_ING_CTL = v_FECHA_AYER
               AND COD_DVI = PKG_PAB_CONSTANTES.V_STR_DVI_CLP;

      --Realizamos los calculos
      V_IMP_ING_CBA := V_IMP_ING_CBA + P_IMP_ING_CBA;
      V_IMP_EGR_CBA := V_IMP_EGR_CBA + P_IMP_EGR_CBA;
      V_IMP_PSC_CBA := V_IMP_ING_CBA - V_IMP_EGR_CBA;
      V_IMP_TOT_FNL_DIA :=
         V_IMP_FNL_CTL + V_IMP_ING_CPR - V_IMP_EGR_CPR + V_IMP_PSC_CBA;

      UPDATE   PABS_DT_DETLL_SALDO_CAJA_MN_MX
         SET   IMP_ING_CBA = V_IMP_ING_CBA,
               IMP_EGR_CBA = V_IMP_EGR_CBA,
               IMP_PSC_CBA = V_IMP_PSC_CBA,
               IMP_TOT_FNL_DIA = V_IMP_TOT_FNL_DIA
       WHERE       COD_BCO_ORG = PKG_PAB_CONSTANTES.V_BIC_BCCH
               AND FEC_ING_CTL = v_FECHA_AYER
               AND COD_DVI = PKG_PAB_CONSTANTES.V_STR_DVI_CLP;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg :=
            'No se encontro informacion para el BIC : '
            || PKG_PAB_CONSTANTES.V_BIC_BCCH;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         p_s_mensaje := err_code || '-' || err_msg;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END;

   /************************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_CAL_SLDO_MNT_SWF
   -- Objetivo: Procedimiento almacenado que actualiza saldos de caja
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_DETLL_SALDO_CAJA_MN_MX
   --
   -- Fecha:14-11-2017
   -- Autor: Gonzalo Correa
   -- Input: P_IMP_103    -->Monto 103
   --        P_IMP_202    -->Monto 202
   --        P_IMP_205    -->Monto 205
   --        P_IMP_900    -->Monto 900
   --        P_IMP_910    -->Monto 910
   -- Output:
   --    p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE SP_PAB_CAL_SLDO_MNT_SWF (P_IMP_103        IN     NUMBER,
                                      P_IMP_202        IN     NUMBER,
                                      P_IMP_205        IN     NUMBER,
                                      P_IMP_900        IN     NUMBER,
                                      P_IMP_910        IN     NUMBER,
                                      p_FLAG_EGR_ING   IN     NUMBER,
                                      P_ERROR             OUT NUMBER)
   IS
      v_NOM_SP        VARCHAR2 (30) := 'SP_PAB_CAL_SLDO_MNT_SWF';

      V_IMP_103       NUMBER;
      V_IMP_202       NUMBER;
      V_IMP_205       NUMBER;
      V_IMP_ING_103   NUMBER;
      V_IMP_ING_202   NUMBER;
      V_IMP_ING_205   NUMBER;
      V_IMP_EGR_103   NUMBER;
      V_IMP_EGR_202   NUMBER;
      V_IMP_EGR_205   NUMBER;
      V_IMP_CGO_900   NUMBER;
      V_IMP_ABN_910   NUMBER;
   BEGIN
      --Obtenemos la fecha habil de ayer
      PKG_PAB_UTILITY.SP_PAB_DEV_DIA_HABIL_ANT (v_COD_PAIS, v_FECHA_AYER);

      SELECT   IMP_ING_103,
               IMP_ING_202,
               IMP_ING_205,
               IMP_EGR_103,
               IMP_EGR_202,
               IMP_EGR_205,
               IMP_CGO_900,
               IMP_ABN_910
        INTO   V_IMP_ING_103,
               V_IMP_ING_202,
               V_IMP_ING_205,
               V_IMP_EGR_103,
               V_IMP_EGR_202,
               V_IMP_EGR_205,
               V_IMP_CGO_900,
               V_IMP_ABN_910
        FROM   PABS_DT_DETLL_SALDO_CAJA_MN_MX
       WHERE       COD_BCO_ORG = PKG_PAB_CONSTANTES.V_BIC_BCCH
               AND FEC_ING_CTL = v_FECHA_AYER
               AND COD_DVI = PKG_PAB_CONSTANTES.V_STR_DVI_CLP;

      --Realizamos los calculos
      IF (p_FLAG_EGR_ING = PKG_PAB_CONSTANTES.V_FLG_EGR)
      THEN
         --Egreso
         V_IMP_EGR_103 := V_IMP_EGR_103 + P_IMP_103;
         V_IMP_EGR_202 := V_IMP_EGR_202 + P_IMP_202;
         V_IMP_EGR_205 := V_IMP_EGR_205 + P_IMP_205;
      ELSE
         --Ingreso
         V_IMP_ING_103 := V_IMP_ING_103 + P_IMP_103;
         V_IMP_ING_202 := V_IMP_ING_202 + P_IMP_202;
         V_IMP_ING_205 := V_IMP_ING_205 + P_IMP_205;
      END IF;

      V_IMP_CGO_900 := V_IMP_CGO_900 + P_IMP_900;
      V_IMP_ABN_910 := V_IMP_ABN_910 + P_IMP_910;

      UPDATE   PABS_DT_DETLL_SALDO_CAJA_MN_MX
         SET   IMP_EGR_103 = V_IMP_EGR_103,
               IMP_EGR_202 = V_IMP_EGR_202,
               IMP_EGR_205 = V_IMP_EGR_205,
               IMP_ING_103 = V_IMP_ING_103,
               IMP_ING_202 = V_IMP_ING_202,
               IMP_ING_205 = V_IMP_ING_205,
               IMP_CGO_900 = V_IMP_CGO_900,
               IMP_ABN_910 = V_IMP_ABN_910
       WHERE       COD_BCO_ORG = PKG_PAB_CONSTANTES.V_BIC_BCCH
               AND FEC_ING_CTL = v_FECHA_AYER
               AND COD_DVI = PKG_PAB_CONSTANTES.V_STR_DVI_CLP;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg :=
            'No se encontro informacion para el BIC : '
            || PKG_PAB_CONSTANTES.V_BIC_BCCH;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         p_s_mensaje := err_code || '-' || err_msg;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END;

   /************************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_CAL_SLDO_POS_SINACOFI
   -- Objetivo: Procedimiento almacenado que actualiza saldos, de operaciones recibidas o enviadas
               por Sinacofi
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_DETLL_SALDO_CAJA_MN_MX
   --
   -- Fecha:17-11-2017
   -- Autor: Santander - CAH
   -- Input: P_IMP_ING_SNF    --> Monto Ingreso Sinacofi
   --        P_IMP_EGR_SNF    --> Montos Egreso Sinacofi
   -- Output:
   --    p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE SP_PAB_CAL_SLDO_POS_SINACOFI (P_IMP_ING_SNF   IN     NUMBER,
                                           P_IMP_EGR_SNF   IN     NUMBER,
                                           p_ERROR            OUT NUMBER)
   IS
      v_NOM_SP        VARCHAR2 (30) := 'SP_PAB_CAL_SLDO_POS_SINACOFI';

      v_IMP_ING_SNF   NUMBER;
      v_IMP_EGR_SNF   NUMBER;
      v_IMP_NET_SNF   NUMBER;
   BEGIN
      --Obtenemos la fecha habil de ayer
      PKG_PAB_UTILITY.SP_PAB_DEV_DIA_HABIL_ANT (v_COD_PAIS, v_FECHA_AYER);

      --Se busca los saldos de sinacofi
      SELECT   SN.IMP_ING_SNF, SN.IMP_EGR_SNF, SN.IMP_NET_SNF
        INTO   v_IMP_ING_SNF, v_IMP_EGR_SNF, v_IMP_NET_SNF
        FROM   PABS_DT_DETLL_SALDO_CAJA_MN_MX SN
       WHERE       COD_BCO_ORG = PKG_PAB_CONSTANTES.V_BIC_BCCH
               AND FEC_ING_CTL = v_FECHA_AYER
               AND COD_DVI = PKG_PAB_CONSTANTES.V_STR_DVI_CLP;

      --Realizamos los calculos
      v_IMP_ING_SNF := v_IMP_ING_SNF + P_IMP_ING_SNF;
      v_IMP_EGR_SNF := v_IMP_EGR_SNF + P_IMP_EGR_SNF;
      v_IMP_NET_SNF := v_IMP_ING_SNF - v_IMP_EGR_SNF;

      --Actualizamos los saldos sinacofi
      UPDATE   PABS_DT_DETLL_SALDO_CAJA_MN_MX
         SET   IMP_ING_SNF = v_IMP_ING_SNF,
               IMP_EGR_SNF = v_IMP_EGR_SNF,
               IMP_NET_SNF = v_IMP_NET_SNF
       WHERE       COD_BCO_ORG = PKG_PAB_CONSTANTES.V_BIC_BCCH
               AND FEC_ING_CTL = v_FECHA_AYER
               AND COD_DVI = PKG_PAB_CONSTANTES.V_STR_DVI_CLP;

      p_ERROR := PKG_PAB_CONSTANTES.v_OK;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg :=
            'No se encontro informacion para el BIC : '
            || PKG_PAB_CONSTANTES.V_BIC_BCCH;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         p_s_mensaje := err_code || '-' || err_msg;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END SP_PAB_CAL_SLDO_POS_SINACOFI;

   -- *************************************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_GES_CNTGC_BCCH_IDA
   -- Objetivo: Procedimiento gestor que llama a pl actuliza operaci�n pagada por contingencia y actuliza saldo para caja MN
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: N/A
   -- Fecha: 21-11-2017
   -- Autor: Santander- CAH
   -- Input:
   -- p_REG_OPER -> parametro tipo registro que contiene el numero y fecha operacion
   -- p_COD_EST_AOS-> Estado de la operaci�n
   -- p_COD_USR -> rut usuario
   -- p_DSC_GLB_BTC-> Glosa para bitacora de la operaci�n.
   -- Output:
   -- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************************************
   PROCEDURE Sp_PAB_GES_CNTGC_BCCH_IDA (p_REG_OPER      IN     REG_OPER,
                                        p_COD_EST_AOS   IN     CHAR,
                                        p_COD_USR       IN     VARCHAR2,
                                        p_DSC_GLB_BTC   IN     VARCHAR2,
                                        p_ERROR            OUT NUMBER)
   IS
      --
      v_NOM_SP        VARCHAR2 (31) := 'Sp_PAB_GES_CNTGC_BCCH_IDA';

      v_FEC_ISR_OPE   DATE;
      v_EST_NMN       NUMBER;
      v_DSC_GLB_BTC   VARCHAR2 (100) := p_DSC_GLB_BTC;
      v_IMP_OPE       NUMBER;
      v_IMP_TOTAL     NUMBER := 0;
      v_COD_BCO_DTN   CHAR (11);
      v_COD_MT_SWF    NUMBER (3);
   BEGIN
      p_ERROR := PKG_PAB_CONSTANTES.v_OK;

      IF p_REG_OPER.COUNT > 0
      THEN
         FOR i IN p_REG_OPER.FIRST .. p_REG_OPER.LAST
         LOOP
            -- Actualiza Estado Operacion Origen
            PKG_PAB_OPERACION.Sp_PAB_ACT_EST_OPERACION_BTC (
               p_REG_OPER (i).NUM_FOL_OPE,
               p_REG_OPER (i).FEC_ISR_OPE,
               NULL,
               p_COD_EST_AOS,
               p_COD_USR,
               v_DSC_GLB_BTC,
               p_ERROR
            );


            --Buscamos datos de la operaci�n, monto para actulizar saldo
            BEGIN
               SELECT   IMP_OPE, COD_BCO_DTN, COD_MT_SWF
                 INTO   v_IMP_OPE, v_COD_BCO_DTN, v_COD_MT_SWF
                 FROM   PABS_DT_DETLL_OPRCN OP
                WHERE   NUM_FOL_OPE = p_REG_OPER (i).NUM_FOL_OPE
                        AND FEC_ISR_OPE = p_REG_OPER (i).FEC_ISR_OPE;
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  err_msg :=
                     'No se pudo obtener informacion de la operacion:'
                     || p_REG_OPER (i).NUM_FOL_OPE;
                  PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (NULL,
                                                      v_NOM_PCK,
                                                      err_msg,
                                                      v_NOM_SP);
                  RAISE;
               WHEN OTHERS
               THEN
                  err_code := SQLCODE;
                  err_msg := SUBSTR (SQLERRM, 1, 300);
                  p_s_mensaje := err_code || '-' || err_msg;
                  PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                      v_NOM_PCK,
                                                      err_msg,
                                                      v_NOM_SP);
                  p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
                  RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
            END;


            v_IMP_TOTAL := v_IMP_OPE + v_IMP_TOTAL;

            --Calculamos e insertamos el saldo bilateral
            SP_PAB_CALCULA_SALDO_BILATERAL (v_IMP_OPE,
                                            v_COD_BCO_DTN,
                                            PKG_PAB_CONSTANTES.V_FLG_EGR,
                                            v_EST_PAG_CNT,
                                            p_ERROR);
         --

         --                --Actualizamos los saldos monitor swift
         --                CASE
         --                    WHEN v_COD_MT_SWF  = PKG_PAB_CONSTANTES.V_COD_MT103 THEN
         --                        SP_PAB_CAL_SLDO_MNT_SWF(v_IMP_OPE,v_cero,v_cero,v_cero,v_cero,PKG_PAB_CONSTANTES.V_FLG_EGR,p_ERROR);
         --                    WHEN v_COD_MT_SWF  = PKG_PAB_CONSTANTES.V_COD_MT202 THEN
         --                        SP_PAB_CAL_SLDO_MNT_SWF(v_cero,v_IMP_OPE,v_cero,v_cero,v_cero,PKG_PAB_CONSTANTES.V_FLG_EGR,p_ERROR);
         --                    WHEN v_COD_MT_SWF  = PKG_PAB_CONSTANTES.V_COD_MT205 THEN
         --                        SP_PAB_CAL_SLDO_MNT_SWF(v_cero,v_cero,v_IMP_OPE,v_cero,v_cero,PKG_PAB_CONSTANTES.V_FLG_EGR,p_ERROR);
         --                END CASE;

         --Calculamos el saldo por area
         --                SP_BUS_CALCULA_AREA_MN(v_COD_SIS_ENT,v_cero,v_IMP_OPE,p_ERROR);

         END LOOP;

         p_ERROR := PKG_PAB_CONSTANTES.v_OK;
      END IF;

      --Actualizamos los saldos de acuerdo a los pagos por Contingencia
      SP_PAB_CAL_SLDO_POS_SINACOFI (v_cero, v_IMP_TOTAL, p_ERROR);


      COMMIT;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         --commit;

         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END Sp_PAB_GES_CNTGC_BCCH_IDA;

   /************************************************************************************************
   -- Funcion/Procedimiento: SP_BUS_CALCULA_AREA_MN
   -- Objetivo: Procedimiento calcula los saldos por areas
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_SALDO_AREA
   --
   -- Fecha: 16/06/17
   -- Autor: GCORREA
   -- Input:
   --
   -- Output:
   --        p_CURSOR : SYS_REFCURSOR, resultados de consulta.
   --        p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE SP_BUS_CALCULA_AREA_MN (P_COD_SIS_ENT_SAL   IN     CHAR,
                                     P_IMP_ING_ARA       IN     NUMBER,
                                     P_IMP_EGR_ARA       IN     NUMBER,
                                     p_ERROR                OUT NUMBER)
   IS
      v_NOM_SP        VARCHAR2 (30) := 'SP_BUS_CALCULA_AREA_MN';

      V_IMP_ING_ARA   NUMBER;
      V_IMP_EGR_ARA   NUMBER;
   BEGIN
      p_ERROR := PKG_PAB_CONSTANTES.v_OK;

      --Se busca los saldos actuales
      SELECT   IMP_ING_ARA, IMP_EGR_ARA
        INTO   V_IMP_ING_ARA, V_IMP_EGR_ARA
        FROM   PABS_DT_SALDO_AREA SA
       WHERE   SA.COD_SIS_ENT_SAL = P_COD_SIS_ENT_SAL;

      --Realizamos los calculos
      V_IMP_ING_ARA := V_IMP_ING_ARA + P_IMP_ING_ARA;
      V_IMP_EGR_ARA := V_IMP_EGR_ARA + P_IMP_EGR_ARA;

      --Actualizamos los saldos por area
      UPDATE   PABS_DT_SALDO_AREA
         SET   IMP_ING_ARA = V_IMP_ING_ARA, IMP_EGR_ARA = V_IMP_EGR_ARA
       WHERE   COD_SIS_ENT_SAL = P_COD_SIS_ENT_SAL;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         --Debemos insertar el registro
         INSERT INTO PABS_DT_SALDO_AREA (
                                            COD_SIS_ENT_SAL,
                                            IMP_ING_ARA,
                                            IMP_EGR_ARA
                    )
           VALUES   (P_COD_SIS_ENT_SAL, P_IMP_ING_ARA, P_IMP_EGR_ARA);
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         p_s_mensaje := err_code || '-' || err_msg;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END SP_BUS_CALCULA_AREA_MN;

   /********************************************************************************************************
   FUNCION/PROCEDIMIENTO: SP_BUS_ACT_AREA_MN.
   OBJETIVO             : ACTUALIZA LOS SALDOS POR �REA CUANDO EXISTE UN CAMBIO DE CANAL (RESTA),
                          SOLO MT 900/910.
   SISTEMA              : DBO_PAB.
   BASE DE DATOS        : DBO_PAB.
   TABLAS USADAS        : PABS_DT_SALDO_AREA.
   FECHA                : 03/09/2018.
   AUTOR                : VARAYA.
   INPUT                : P_COD_SIS_ENT_SAL := C�DIGO ENTRADA/SALIDA.
                          P_IMP_ING_ARA     := IMPUESTO INGRESO MT910.
                          P_IMP_EGR_ARA     := IMPUESTO EGRESO MT900.
   OUTPUT               : P_ERROR           := INDICADOR DE ERRORES:
                                               0 := SIN ERRORES.
                                               1 := CON ERRORES.
   OBSERVACIONES
   FECHA       USUARIO    DETALLE
   03-09-2018  VARAYA     CREACI�N SP.
   ********************************************************************************************************/
   PROCEDURE SP_BUS_ACT_AREA_MN (P_COD_SIS_ENT_SAL   IN     CHAR,
                                 P_IMP_ING_ARA       IN     NUMBER,
                                 P_IMP_EGR_ARA       IN     NUMBER,
                                 p_ERROR                OUT NUMBER)
   IS
      ---------------------------------------DECLARACI�N DE VARIABLES Y CONSTANTES----------------------------
      V_NOM_SP        VARCHAR2 (30) := 'SP_BUS_ACT_AREA_MN';
      V_IMP_ING_ARA   PABS_DT_SALDO_AREA.IMP_ING_ARA%TYPE;
      V_IMP_EGR_ARA   PABS_DT_SALDO_AREA.IMP_EGR_ARA%TYPE;
   --------------------------------------------------------------------------------------------------------
   BEGIN
      P_ERROR := PKG_PAB_CONSTANTES.v_OK;

      --BUSCA LOS SALDOS ACTUALES
      BEGIN
         SELECT   IMP_ING_ARA, IMP_EGR_ARA
           INTO   V_IMP_ING_ARA, V_IMP_EGR_ARA
           FROM   PABS_DT_SALDO_AREA SA
          WHERE   SA.COD_SIS_ENT_SAL = P_COD_SIS_ENT_SAL;
      END;

      --REALIZAMOS LOS CALCULOS.
      V_IMP_ING_ARA := V_IMP_ING_ARA - P_IMP_ING_ARA;
      V_IMP_EGR_ARA := V_IMP_EGR_ARA - P_IMP_EGR_ARA;

      --ACTUALIZAMOS LOS SALDOS POR AREA.
      UPDATE   PABS_DT_SALDO_AREA
         SET   IMP_ING_ARA = V_IMP_ING_ARA, IMP_EGR_ARA = V_IMP_EGR_ARA
       WHERE   COD_SIS_ENT_SAL = P_COD_SIS_ENT_SAL;
   --SI NO EXISTE EL REGISTRO PARA ACTUALIZAR DEBEMOS INSERTARLO EN LA TABLA.
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         INSERT INTO PABS_DT_SALDO_AREA (
                                            COD_SIS_ENT_SAL,
                                            IMP_ING_ARA,
                                            IMP_EGR_ARA
                    )
           VALUES   (P_COD_SIS_ENT_SAL, P_IMP_ING_ARA, P_IMP_EGR_ARA);
      WHEN OTHERS
      THEN
         ERR_CODE := SQLCODE;
         ERR_MSG := SUBSTR (SQLERRM, 1, 300);
         P_S_MENSAJE := ERR_CODE || '-' || ERR_MSG;
         PKG_PAB_TRACKING.SP_PAB_INS_LOG_BD (ERR_CODE,
                                             V_NOM_PCK,
                                             ERR_MSG,
                                             V_NOM_SP);
         P_ERROR := PKG_PAB_CONSTANTES.V_ERROR;
         RAISE_APPLICATION_ERROR (-20000, P_S_MENSAJE);
   END SP_BUS_ACT_AREA_MN;

   /************************************************************************************************
   -- Funcion/Procedimiento: SP_REI_SALDO_MN
   -- Objetivo: Procedimiento que reinicia los saldos de mn
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_DETLL_SALDO_CAJA_MN_MX
   --
   -- Fecha: 24-11-2017
   -- Autor: Santander - CAH
   -- Input:
   --
   -- Output:
   --        p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE SP_REI_SLDO_MN (p_ERROR OUT NUMBER)
   IS
      v_NOM_SP   VARCHAR2 (30) := 'SP_REI_SLDO_MN';
   BEGIN
      --Obtenemos la fecha habil de ayer
      PKG_PAB_UTILITY.SP_PAB_DEV_DIA_HABIL_ANT (v_COD_PAIS, v_FECHA_AYER);

      err_msg :=
            'Se van a reiniciar los saldos de cartola MN '
         || PKG_PAB_CONSTANTES.V_BIC_BCCH
         || ' '
         || PKG_PAB_CONSTANTES.V_STR_DVI_CLP
         || ' '
         || v_FECHA_AYER;
      PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                          v_NOM_PCK,
                                          err_msg,
                                          v_NOM_SP);

      --Actualizamos los saldos MN, Cuando se carga la cartola 950 BCCH
      UPDATE   PABS_DT_DETLL_SALDO_CAJA_MN_MX
         SET   NUM_FOL_CTL = v_cero,
               IMP_INI_CTL = v_cero,
               IMP_ING_CTL = v_cero,
               IMP_EGR_CTL = v_cero,
               IMP_FNL_CTL = v_cero,
               IMP_APR_AOS = v_cero,
               IMP_ING_DIA = v_cero,
               IMP_EGR_DIA = v_cero,
               IMP_ABN_BCT = v_cero,
               IMP_CGO_BCT = v_cero,
               IMP_RAL_AOS = v_cero,
               IMP_GRT_CBA = v_cero,
               IMP_ING_CBA = v_cero,
               IMP_EGR_CBA = v_cero,
               IMP_PSC_CBA = v_cero,
               IMP_ING_CPR = v_cero,
               IMP_EGR_CPR = v_cero,
               IMP_TOT_FNL_DIA = v_cero,
               IMP_ING_SNF = v_cero,
               IMP_EGR_SNF = v_cero,
               IMP_NET_SNF = v_cero,
               IMP_OER_503 = v_cero,
               IMP_OER_103 = v_cero,
               IMP_ING_24 = v_cero,
               IMP_EGR_24 = v_cero,
               IMP_TOT_24 = v_cero,
               IMP_ING_48 = v_cero,
               IMP_EGR_48 = v_cero,
               IMP_TOT_48 = v_cero,
               IMP_ING_OVR = v_cero,
               IMP_EGR_OVR = v_cero,
               IMP_TOT_OVR = v_cero,
               IMP_ING_103 = v_cero,
               IMP_ING_200 = v_cero,
               IMP_ING_202 = v_cero,
               IMP_ING_205 = v_cero,
               IMP_EGR_103 = v_cero,
               IMP_EGR_200 = v_cero,
               IMP_EGR_202 = v_cero,
               IMP_EGR_205 = v_cero,
               IMP_CGO_900 = v_cero,
               IMP_ABN_910 = v_cero,
               IMP_TOT_SWF = v_cero
       WHERE   COD_BCO_ORG = PKG_PAB_CONSTANTES.V_BIC_BCCH
               AND COD_DVI = PKG_PAB_CONSTANTES.V_STR_DVI_CLP;

      p_ERROR := PKG_PAB_CONSTANTES.v_OK;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg :=
            'No se han encontrado registos asociados a la busqueda por banco '
            || PKG_PAB_CONSTANTES.V_BIC_BCCH
            || ' - divisa  '
            || PKG_PAB_CONSTANTES.V_STR_DVI_CLP;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         p_s_mensaje := err_code || '-' || err_msg;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END SP_REI_SLDO_MN;
END PKG_PAB_CAJA_MN;
/


GRANT EXECUTE ON DBO_PAB.PKG_PAB_CAJA_MN TO R_APP_PAB
/

GRANT EXECUTE ON DBO_PAB.PKG_PAB_CAJA_MN TO USR_PAB
/

