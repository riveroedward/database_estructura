CREATE OR REPLACE /* Formatted on 18-08-2020 17:45:42 (QP5 v5.115.810.9015) */
PACKAGE DBO_PAB.PKG_PAB_MANTENEDOR
IS
   --
   /**************************VARIABLE GLOBALES************************************/
   v_NOM_PCK       VARCHAR2 (30) := 'PKG_PAB_MANTENEDOR';
   ERR_CODE        NUMBER := 0;
   ERR_MSG         VARCHAR2 (300);
   v_DES           VARCHAR2 (300);
   v_DES_BIT       VARCHAR2 (100);
   v_BANCO         VARCHAR2 (9) := '970150005';
   p_s_mensaje     VARCHAR2 (400);
   V_FLG_VGN_NO    NUMBER (1);
   V_FLG_VGN_SI    NUMBER (1);
   V_FLG_VGN_ELI   NUMBER (1);

   TYPE T_NUM_FOL_NMN
   IS
      TABLE OF NUMBER
         INDEX BY PLS_INTEGER;

   /**************************FIN VARIABLE GLOBALES********************************/

   --
   -- ***********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_CON_BCO_ALMOT
   -- Objetivo: Procedimiento actualiza estado de la operaci?n abonada exitosamente.
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: N/A
   -- Fecha: 18/11/16
   -- Autor: Santander
   -- Input:
   -- p_COD_PAIS -> foliador del banco, PK tabla detalle operaciones.
   -- p_COD_BIC -> Fecha de insercion de operacion en el sistema, PK tabla detalle operaciones.
   -- p_COD_RUT -> Numero de Refencia SWIFT
   -- p_NOMBRE -> Codigo de Usuario que se registrara en bitacora
   -- p_FLG_VGN -> Indica si el banco esta habilitado o no
   -- Output:
   -- p_CURSOR -> Resultado de consulta
   -- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************

   PROCEDURE SP_PAB_CON_BCO_ALMOT (p_COD_PAIS   IN     CHAR,
                                   p_COD_BIC    IN     CHAR,
                                   p_COD_RUT    IN     CHAR,
                                   p_NOMBRE     IN     CHAR,
                                   p_FLG_VGN    IN     CHAR,
                                   p_CURSOR        OUT SYS_REFCURSOR,
                                   p_ERROR         OUT NUMBER);


   --
   -- ***********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_INS_BCO_ALMOT
   -- Objetivo: Procedimiento actualiza estado de la operaci?n abonada exitosamente.
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: N/A
   -- Fecha: 18/11/16
   -- Autor: Santander
   -- Input:
   -- COD_BIC -> codigo bis del banco
   -- p_IMP_LMT_CDT  -> Importe Limite Credito
   -- p_IMP_LMT_DBT  -> Importe Limite Debito
   -- p_CANALPAGO    -> Canla de pago
   -- p_USUARIO      -> Corresponde al usuario
   -- Output:
   -- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.

   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   PROCEDURE SP_PAB_INS_BCO_ALMOT (p_COD_BIC       IN     CHAR,
                                   p_IMP_LMT_CDT   IN     NUMBER,
                                   p_IMP_LMT_DBT   IN     NUMBER,
                                   p_CANALPAGO     IN     CHAR,
                                   p_USUARIO       IN     CHAR,
                                   p_CURSOR           OUT SYS_REFCURSOR,
                                   p_ERROR            OUT NUMBER);

   -- ***********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_ACT_BCO_ALMOT
   -- Objetivo: Procedimiento actualiza estado de la operaci?n abonada exitosamente.
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: N/A
   -- Fecha: 18/11/16
   -- Autor: Santander
   -- Input:
   --    p_COD_BIC -> codigo bis del banco
   --    p_IMP_LMT_CDT  -> Importe Limite Credito
   --    p_IMP_LMT_DBT  -> Importe Limite Debito
   --    p_HABILITADO -> indica si esta habilitado
   --    p_CANALPAGO -> corresponde al canal de pago
   --    p_USUARIO -> corresponde al codigo de usuario
   -- Output:
   -- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.

   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   PROCEDURE SP_PAB_ACT_BCO_ALMOT (p_COD_BIC       IN     CHAR,
                                   p_IMP_LMT_CDT   IN     NUMBER,
                                   p_IMP_LMT_DBT   IN     NUMBER,
                                   p_HABILITADO    IN     CHAR,
                                   p_CANALPAGO     IN     CHAR,
                                   p_USUARIO       IN     CHAR,
                                   p_ERROR            OUT NUMBER);


   -- ***********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_CON_BCO_COR_ALMOT
   -- Objetivo: Procedimiento que consultar bancos de altos montos
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas:  TCDTBAI , TCDT040, TGEN_0112
   -- Fecha: 01/12/2016
   -- Autor: Santander
   -- Input:
   --    p_COD_PAIS -> corresponde al codigo del pais
   --    p_COD_BIC -> corresponde al codigo bic del banco
   --    p_COD_RUT -> corresponde al rut del banco
   --    p_NOMBRE -> corresponde al nombre del banco
   -- Output:
   -- p_CURSOR -> Cursor con los registros de los elementos
   -- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- **************************************************************************
   PROCEDURE SP_PAB_CON_BCO_COR_ALMOT (p_COD_PAIS   IN     CHAR,
                                       p_COD_BIC    IN     CHAR,
                                       p_COD_RUT    IN     CHAR,
                                       p_NOMBRE     IN     CHAR,
                                       p_CURSOR        OUT SYS_REFCURSOR,
                                       p_ERROR         OUT NUMBER);

   --
   -- ***********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_ELI_BCO_ALMOT
   -- Objetivo: Procedimiento actualiza estado de la operaci?n abonada exitosamente.
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: N/A
   -- Fecha: 18/11/16
   -- Autor: Santander
   -- Input:
   --    p_COD_BIC -> codigo bis del banco
   --    p_HABILITADO -> indica si esta habilitado
   --    p_USUARIO -> corresponde al usuario que realiza el cambio
   --    p_USUARIO -> corresponde al codigo de usuario
   -- Output:
   -- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.

   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   PROCEDURE SP_PAB_ELI_BCO_ALMOT (p_COD_BIC      IN     CHAR,
                                   p_HABILITADO   IN     NUMBER,
                                   p_USUARIO      IN     CHAR,
                                   p_ERROR           OUT NUMBER);

   --
   --
   -- ***********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_ACT_MON_ALMOT
   -- Objetivo: Procedimiento actualiza monto de la moneda.
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: N/A
   -- Fecha: 18/11/16
   -- Autor: Santander
   -- Input:
   --    p_COD_DVI -> tipo de moneda
   --    p_IMP_MAX_SIN_ATZ -> importe maximo
   --    p_USR_MOD -> corresponde al codigo de usuario
   -- Output:
   -- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   PROCEDURE SP_PAB_ACT_MON_ALMOT (p_COD_DVI           IN     CHAR,
                                   p_IMP_MAX_SIN_ATZ   IN     NUMBER,
                                   p_USR_MOD           IN     CHAR,
                                   p_ERROR                OUT NUMBER);

   -- ***********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_ELI_MON_ALMOT
   -- Objetivo: Procedimiento que Elimina bancos de altos montos
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas:  PABS_DT_MONDA_ALMOT
   -- Fecha: 01/12/2016
   -- Autor: Santander
   -- Input:
   -- p_COD_DVI
   -- p_USR_MOD
   -- N/A
   -- Output:
   -- p_CURSOR -> Cursor con los registros de los 15 elementos
   -- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   PROCEDURE SP_PAB_ELI_MON_ALMOT (p_COD_DVI   IN     CHAR,
                                   p_USR_MOD   IN     CHAR,
                                   p_ERROR        OUT NUMBER);

   --
   -- ***********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_INS_MON_ALMOT
   -- Objetivo: Procedimiento que Elimina bancos de altos montos
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas:  PABS_DT_BANCO_ALMOT
   -- Fecha: 01/12/2016
   -- Autor: Santander
   -- Input:
   --    p_COD_DVI : Codigo de moneda
   --    p_IMP_MAX_SIN_ATZ : valor de importe
   --    p_FLG_VGN : flag de vigencia
   --    p_USR_MOD . usuario de modificacion
   -- N/A
   -- Output:
   -- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   PROCEDURE SP_PAB_INS_MON_ALMOT (p_COD_DVI           IN     CHAR,
                                   p_IMP_MAX_SIN_ATZ   IN     NUMBER,
                                   p_FLG_VGN           IN     CHAR,
                                   p_USR_MOD           IN     CHAR,
                                   p_ERROR                OUT NUMBER);

   --
   -- ***********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_CON_MON_ALMOT
   -- Objetivo: Procedimiento que Cosulta las monedas de altos montos
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas:  PABS_DT_MONDA_ALMOT
   -- Fecha: 01/12/2016
   -- Autor: Santander
   -- Input:
   --    p_COD_DVI : Codigo de monedan
   -- N/A
   -- Output:
   -- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   PROCEDURE SP_PAB_CON_MON_ALMOT (p_COD_DVI   IN     CHAR,
                                   p_CURSOR       OUT SYS_REFCURSOR,
                                   p_ERROR        OUT NUMBER);

   --
   -- ***********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_CON_TPO_OPE_ALMOT
   -- Objetivo: Procedimiento que consulta tipo de operacion CONSTITUCION GARANTIA COMBANC
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas:  PABS_DT_TIPO_OPRCN_ALMOT
   -- Fecha: 17/02/2017
   -- Autor: Santander
   -- Input:
   -- Output:
   -- p_CURSOR -> Cursor con los registros de los elementos
   -- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   PROCEDURE SP_PAB_CON_TPO_OPE_ALMOT (p_CURSOR   OUT SYS_REFCURSOR,
                                       p_ERROR    OUT NUMBER);

   --
   -- ***********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_ACT_TPO_OPE_ALMOT
   -- Objetivo: Procedimiento que Actualiza tipo de operacion CONSTITUCION GARANTIA COMBANC
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas:  NN
   -- Fecha: 17/02/2017
   -- Autor: Santander
   -- Input:
   --    p_MONTO_OPE -> corresponde al Monto
   --  p_COD_USR -> Codigo de Usuario
   -- Output:
   -- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   PROCEDURE SP_PAB_ACT_TPO_OPE_ALMOT (p_MONTO_OPE   IN     NUMBER,
                                       p_COD_USR     IN     CHAR,
                                       p_ERROR          OUT NUMBER);

   --
   -- ***********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_CON_CTA_CNTBL_ALMOT
   -- Objetivo: Procedimiento que consulta los datos de las cuentas
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_CENTA_CNTBL, PABS_DT_SISTM_ENTRD_SALID
   -- Fecha: 12/DIC/2016
   -- Autor: Santander
   -- Input:
   -- p_COD_SIS_ENT_SAL -> corresponde al codigo del sistema
   -- p_FLAG_MRD_UTZ_SW -> Flag de vigencia
   -- Output:
   -- p_CURSOR
   -- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   PROCEDURE SP_PAB_CON_CTA_CNTBL_ALMOT (
      p_COD_SIS_ENT_SAL   IN     CHAR,
      p_FLAG_MRD_UTZ_SW   IN     NUMBER,
      p_CURSOR               OUT SYS_REFCURSOR,
      p_ERROR                OUT NUMBER
   );

   --
   -- ***********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_INS_CTA_CNTBL_ALMOT
   -- Objetivo: Procedimiento inserta los datos del corresponsal
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_CENTA_CNTBL
   -- Fecha: 12/DIC/2016
   -- Autor: Santander
   -- Input:
   --    p_COD_SIS_ENT_SAL -> corresponde al codigo del sistema
   --    p_FLAG_UTZ_SWF -> Flag de vigencia
   --    p_NUM_CTA_CTB -> corresponde al numero de cuenta
   --    p_NUM_CTA_CTB -> corresponde al numero de cuenta
   --    p_NUM_CRT_CTA_CTB -> corresponde al numero corto de cuenta
   --    p_NUM_CNP_CTA_CTB -> numero de concepto cuenta
   --    p_COD_SUC_DTN_TPN -> corresponde al codigo de la sucursal
   --    p_GLS_IFM_MVT_CTB -> corresponde a la glosa
   --    p_COD_USR_CRC -> Corresponde al numero codigo de usuario
   -- Output:
   -- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   PROCEDURE SP_PAB_INS_CTA_CNTBL_ALMOT (p_COD_SIS_ENT_SAL   IN     CHAR,
                                         p_FLAG_UTZ_SWF      IN     NUMBER,
                                         p_NUM_CTA_CTB       IN     CHAR,
                                         p_NUM_CRT_CTA_CTB   IN     CHAR,
                                         p_NUM_CNP_CTA_CTB   IN     CHAR,
                                         p_COD_SUC_DTN_TPN   IN     CHAR,
                                         p_GLS_IFM_MVT_CTB   IN     CHAR,
                                         p_GLS_COM_CTA_CTB   IN     CHAR,
                                         p_COD_USR_CRC       IN     NUMBER,
                                         p_ERROR                OUT NUMBER);


   --
   --
   -- ***********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_CON_CRPSL_ALMOT
   -- Objetivo: Procedimiento almacenado que muestra los bancos corresponsales de Altos Montos.
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM, TCDTBAI TBAI, TGEN_0112 PAIS
   -- Tablas Usadas: PABS_DT_CENTA_BANCO_CRPSL
   -- Fecha: 12/DIC/2016
   -- Autor: Santander
   -- Input:
   --    p_COD_BIC -> Corresponde al codigo bic del banco
   --    p_COD_PAIS -> Corresponde al codigo del pais
   --    p_COD_MON -> corresponde al codigo del tipo de moneda
   --    p_COD_CTA -> Corresponde al codigo de cuenta
   -- Output:
   -- p_CURSOR
   -- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   PROCEDURE SP_PAB_CON_CRPSL_ALMOT (p_COD_BIC    IN     CHAR,
                                     p_COD_PAIS   IN     CHAR,
                                     p_COD_MON    IN     CHAR,
                                     p_COD_CTA    IN     CHAR,
                                     p_CURSOR        OUT SYS_REFCURSOR,
                                     p_ERROR         OUT NUMBER);

   -- ***********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_INS_CRPSL_ALMOT
   -- Objetivo: Procedimiento inserta los datos del corresponsal
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_CENTA_BANCO_CRPSL
   -- Fecha: 12/DIC/2016
   -- Autor: Santander
   -- Input:
   --    p_NUM_CTA_COR -> Corresponde al numero de cuenta
   --    p_COD_BIC_BCO -> Corresponde al codigo bic del banco
   --    p_COD_DVI -> Codigo de moneda
   --    p_IMP_TOT_LCD -> monto linea de credito
   --    p_COD_USR_CRC     -> Codigo del usuario
   -- Output:
   -- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   PROCEDURE SP_PAB_INS_CRPSL_ALMOT (p_NUM_CTA_COR       IN     VARCHAR2,
                                     p_COD_BIC_BCO       IN     CHAR,
                                     p_COD_DVI           IN     CHAR,
                                     p_IMP_TOT_LCD       IN     NUMBER,
                                     p_COD_USR_CRC       IN     CHAR,
                                     p_COD_MT_SWF        IN     NUMBER,
                                     p_NUM_PDI           IN     NUMBER,
                                     p_POR_INT_LNA_CDT   IN     NUMBER,
                                     p_FEC_VGN_LNA       IN     DATE,
                                     p_FLG_CHQ           IN     NUMBER,
                                     p_FLG_TRF_LNA       IN     NUMBER,
                                     p_FLG_OVR_LNA       IN     NUMBER,
                                     p_IMP_LMT_SBF_LNA   IN     NUMBER,
                                     p_HOR_CRR_COR       IN     VARCHAR2,
                                     p_IMP_MAX_SBF       IN     NUMBER,
                                     p_ERROR                OUT NUMBER);

   --
   -- ***********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_ACT_CRPSL_ALMOT
   -- Objetivo: Procedimiento actualiza los datos del corresponsal
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_CENTA_BANCO_CRPSL
   -- Fecha: 12/DIC/2016
   -- Autor: Santander
   -- Input:
   -- p_NUM_CTA_COR -> Corresponde al numero de cuenta
   -- p_COD_BIC_BCO -> Corresponde al codigo bic del banco
   -- p_COD_DVI     -> Codigo de moneda
   -- p_IMP_TOT_LCD -> monto linea de credito
   -- p_IMP_SDO_CTA_COR -> monto cuenta corriente
   -- p_FLG_VGN -> Indica el estado de vigencia 1 vigente // 0 no vigente
   -- p_NUM_LIN_CRD
   -- p_COD_USR -> Codigo del usuario
   -- Output:
   -- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   PROCEDURE SP_PAB_ACT_CRPSL_ALMOT (p_NUM_CTA_COR       IN     VARCHAR2,
                                     p_COD_BIC_BCO       IN     CHAR,
                                     p_COD_DVI           IN     CHAR,
                                     p_IMP_LMT_SBF_LNA   IN     NUMBER,
                                     p_IMP_MAX_SBF       IN     NUMBER,
                                     p_HOR_CRR_COR       IN     CHAR,
                                     p_COD_USR           IN     CHAR,
                                     p_IMP_TOT_LCD       IN     NUMBER,
                                     p_COD_MT_SWF        IN     NUMBER,
                                     p_NUM_PDI           IN     NUMBER,
                                     p_POR_INT_LNA_CDT   IN     NUMBER,
                                     p_FEC_VGN_LNA       IN     DATE,
                                     p_FLG_CHQ           IN     NUMBER,
                                     p_FLG_TRF_LNA       IN     NUMBER,
                                     p_FLG_OVR_LNA       IN     NUMBER,
                                     p_ERROR                OUT NUMBER);

   --
   -- ***********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_ELI_CRPSL_ALMOT
   -- Objetivo: Procedimiento elimina de forma logica un corresponsal
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_CENTA_BANCO_CRPSL
   -- Fecha: 12/DIC/2016
   -- Autor: Santander
   -- Input:
   -- p_NUM_CTA_COR -> Corresponde al numero de cuenta
   -- p_COD_BIC_BCO -> Corresponde al codigo bic del banco
   -- p_COD_USR -> Codigo del usuario
   -- Output:
   -- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   PROCEDURE SP_PAB_ELI_CRPSL_ALMOT (p_NUM_CTA_COR   IN     VARCHAR2,
                                     p_COD_BIC_BCO   IN     CHAR,
                                     p_COD_DVI       IN     CHAR,
                                     p_ERROR            OUT NUMBER);

   --
   -- ***********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_CON_CRPSL_COR_ALMOT
   -- Objetivo: Procedimiento consulta los corresponsales que no estan en las tabls de altosmontos
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_CENTA_BANCO_CRPSL,  TCDTBAI, TGEN_0112, TCDT040
   -- Fecha: 12/DIC/2016
   -- Autor: Santander
   -- Input:
   -- p_RUT_BCO -> Corresponde al numero de rut del banco
   -- p_NOM_BCO -> Corresponde al nombre del banco
   -- p_COD_BIC -> Corresponde al codigo bic del banco
   -- p_COD_PAIS -> Codigo del pais
   -- Output:
   -- p_CURSOR
   -- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   PROCEDURE SP_PAB_CON_CRPSL_COR_ALMOT (p_COD_BIC    IN     CHAR,
                                         p_COD_PAIS   IN     CHAR,
                                         p_RUT_BCO    IN     CHAR,
                                         p_NOM_BCO    IN     CHAR,
                                         p_CURSOR        OUT SYS_REFCURSOR,
                                         p_ERROR         OUT NUMBER);

   --
   --**********************************************************************************************
   -- Funcion/Procedimiento: SP_ PAB_OBT_SIS_ENTSAL
   -- Objetivo: Procedimiento que entrega lista de canales segun el tipo ingresado
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_SISTM_ENTRD_SALID
   -- Fecha: 30/11/16
   -- Autor: Santander
   -- Input:
   -- p_COD_SIS_ENT_SAL -> Codigo de canal
   -- Output:
   -- p_CURSOR ->  Lista de horarios de inicio o cierre de canales
   -- p_ERROR -> Indicador de errores (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de utimos cambios>
   --***********************************************************************************************
   PROCEDURE SP_PAB_OBT_SIS_ENTSAL (p_COD_SIS_ENT_SAL   IN     CHAR,
                                    p_CURSOR               OUT SYS_REFCURSOR,
                                    p_ERROR                OUT NUMBER);

   --**********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_ACT_SIS_ENTSAL
   -- Objetivo: Procedimiento que entrega detale de canal seleccionado
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_SISTM_ENTRD_SALID
   -- Fecha: 30/11/16
   -- Autor: Santander
   -- Input:
   -- p_COD_SIS_ENT_SAL -> Codigo de canal
   -- p_COD_USR_ULT_MOD
   -- p_HOR_ICO_SIS
   -- p_HOR_CRR_SIS
   -- p_FLG_VGN
   -- Output:
   -- p_ERROR -> Indicador de errores (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de utimos cambios>
   --***********************************************************************************************
   PROCEDURE SP_PAB_ACT_SIS_ENTSAL (p_COD_SIS_ENT_SAL   IN     CHAR,
                                    p_COD_USR_ULT_MOD   IN     CHAR,
                                    p_HOR_ICO_SIS       IN     NUMBER,
                                    p_HOR_CRR_SIS       IN     NUMBER,
                                    p_FLG_VGN           IN     NUMBER,
                                    p_ERROR                OUT NUMBER);

   --

   --***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_COMBO_BAN_ALMTO_MAN
   -- Objetivo: PROCEDIMIENTO ALMACENADO QUE CONSULTA Y ENTREGA TODOS LOS BANCOS DEL SISTEMA (HABILITADOS/DESHABILITADOS)
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_BANCO_ALMOT
   -- Fecha: 21/06/16
   -- Autor: Santander
   -- Input:
   -- N/A
   -- Output:
   -- p_CURSOR -> SYS_REFCURSOR, resultados de consulta con los bancos
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************
   PROCEDURE Sp_PAB_COMBO_BAN_ALMTO_MAN (p_CURSOR OUT SYS_REFCURSOR);

   --***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_COMBO_BAN_NUE_ALMTO
   -- Objetivo: PROCEDIMIENTO ALMACENADO QUE CONSULTA Y ENTREGA TODOS LOS BANCOS DEL SISTEMA (HABILITADOS/DESHABILITADOS)
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_BANCO_ALMOT
   -- Fecha: 21/06/16
   -- Autor: Santander
   -- Input:
   -- N/A
   -- Output:
   -- p_CURSOR -> SYS_REFCURSOR, resultados de consulta con los bancos
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************
   PROCEDURE Sp_PAB_COMBO_BAN_NUE_ALMTO (p_CURSOR OUT SYS_REFCURSOR);

   -- ***********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_COMBO_TPO_MT_CRTLA
   -- Objetivo: Procedimiento que retorne tipo de mt para cartola 940 � 950
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_TIPO_MT_SWIFT
   -- Fecha: 05/09/2017
   -- Autor: Santander- CAH
   -- Input:
   -- N/A
   -- Output:
   -- p_CURSOR -> Cursor con los registros de los 2 estados
   -- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   PROCEDURE SP_PAB_COMBO_TPO_MT_CRTLA (p_CURSOR   OUT SYS_REFCURSOR,
                                        p_ERROR    OUT NUMBER);
END PKG_PAB_MANTENEDOR;
/


CREATE OR REPLACE /* Formatted on 18-08-2020 17:45:42 (QP5 v5.115.810.9015) */
PACKAGE BODY DBO_PAB.PKG_PAB_MANTENEDOR
IS
   --
   -- ***********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_CON_BCO_ALMOT
   -- Objetivo: Procedimiento que consulta los bancos de altos montos
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_BANCO_ALMOT, PABS_DT_SISTM_ENTRD_SALID
   -- Fecha: 18/11/16
   -- Autor: Santander
   -- Input:
   -- p_COD_PAIS -> foliador del banco, PK tabla detalle operaciones.
   -- p_COD_BIC -> Fecha de insercion de operacion en el sistema, PK tabla detalle operaciones.
   -- p_COD_RUT -> Numero de Refencia SWIFT
   -- p_NOMBRE -> Codigo de Usuario que se registrara en bitacora
   -- p_FLG_VGN -> Indica si el banco esta habilitado o no
   -- Output:
   -- p_CURSOR -> Resultado de consulta
   -- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************

   PROCEDURE SP_PAB_CON_BCO_ALMOT (p_COD_PAIS   IN     CHAR,
                                   p_COD_BIC    IN     CHAR,
                                   p_COD_RUT    IN     CHAR,
                                   p_NOMBRE     IN     CHAR,
                                   p_FLG_VGN    IN     CHAR,
                                   p_CURSOR        OUT SYS_REFCURSOR,
                                   p_ERROR         OUT NUMBER)
   IS
      --Seteo de Variables
      V_NOM_SP       VARCHAR2 (30) := 'SP_PAB_CON_BCO_ALMOT';
      v_COD_BIC      CHAR (11);
      V_FLG_VGN_SI   NUMBER := PKG_PAB_CONSTANTES.V_FLG_VGN_SI;
      V_FLG_VGN_NO   NUMBER := PKG_PAB_CONSTANTES.V_FLG_VGN_NO;
      v_NOMBRE       VARCHAR2 (40);
      v_COD_RUT      VARCHAR2 (15);
   BEGIN
      P_ERROR := PKG_PAB_CONSTANTES.V_OK;

      BEGIN
         --
         v_COD_BIC := p_COD_BIC;
         v_NOMBRE := UPPER (p_NOMBRE);
         v_COD_RUT := UPPER (p_COD_RUT);

         --Dejamps el valor enviado por el combo en caso de tener una preferencia
         IF (p_FLG_VGN IS NOT NULL)
         THEN
            V_FLG_VGN_SI := p_FLG_VGN;
            V_FLG_VGN_NO := p_FLG_VGN;
         END IF;

         IF (p_COD_RUT IS NULL)
         THEN
            OPEN p_CURSOR FOR
                 SELECT   BCOALM.COD_BIC_BCO AS COD_BIC,
                          TBAI.cod_pais AS CODIGOPAIS,
                          PAIS.TCMPAIS AS NOMBREPAIS,
                          DECODE (TBAI.FLG_COR, 'S', 'S', 'N') AS CORRESPONSAL,
                          ENTSAL.COD_SIS_ENT_SAL AS CANAL_SALIDA,
                          bcoalm.imp_max_bil_cba AS MONTO_BILATERAL,
                          BCOALM.FLG_VGN AS VIGENTE,
                          T40.STBANCO AS STBANCO,
                          TBAI.DES_BCO AS NOMBRE_BCO,
                          T40.FHALTA AS FHALTA,
                          T40.FHBAJA AS FHBAJA,
                          T40.TCNIFENT AS RUT_BCO,
                          TBAI.des_bco AS DES_BCO,
                          ENTSAL.DSC_SIS_ENT_SAL AS DSC_CANAL_SALIDA,
                          bcoalm.IMP_LMT_CDT AS LMT_CDT,
                          bcoalm.IMP_LMT_DBT AS LMT_DBT
                   FROM   PABS_DT_BANCO_ALMOT bcoalm,
                          PABS_DT_SISTM_ENTRD_SALID entsal,
                          TCDTBAI TBAI,
                          TCDT040 T40,
                          TGEN_0112 pais
                  WHERE       BCOALM.COD_BIC_BCO = TBAI.TGCDSWSA
                          AND T40.TGCDSWSA(+) = TBAI.TGCDSWSA
                          AND PAIS.CODEPAIS = TBAI.cod_pais
                          AND ENTSAL.COD_SIS_ENT_SAL = bcoalm.cod_sis_sal
                          AND T40.TCNIFENT(+) <> v_BANCO
                          --------------------------------------
                          AND PAIS.TCCPAIS4 = NVL (p_COD_PAIS, PAIS.TCCPAIS4)
                          AND bcoalm.COD_BIC_BCO =
                                NVL (v_COD_BIC, bcoalm.COD_BIC_BCO)
                          AND UPPER (TBAI.des_bco) LIKE
                                NVL (v_NOMBRE, UPPER (TBAI.des_bco)) || '%'
                          AND BCOALM.FLG_VGN IN (V_FLG_VGN_SI, V_FLG_VGN_NO)
               ORDER BY   BCOALM.COD_BIC_BCO;
         ELSE
            OPEN p_CURSOR FOR
                 SELECT   BCOALM.COD_BIC_BCO AS COD_BIC,
                          TBAI.cod_pais AS CODIGOPAIS,
                          PAIS.TCMPAIS AS NOMBREPAIS,
                          DECODE (TBAI.FLG_COR, 'S', 'S', 'N') AS CORRESPONSAL,
                          entsal.COD_SIS_ENT_SAL AS CANAL_SALIDA,
                          bcoalm.imp_max_bil_cba AS MONTO_BILATERAL,
                          BCOALM.FLG_VGN AS VIGENTE,
                          T40.STBANCO AS STBANCO,
                          TBAI.DES_BCO AS NOMBRE_BCO,
                          T40.FHALTA AS FHALTA,
                          T40.FHBAJA AS FHBAJA,
                          T40.TCNIFENT AS RUT_BCO,
                          TBAI.des_bco AS DES_BCO,
                          ENTSAL.DSC_SIS_ENT_SAL AS DSC_CANAL_SALIDA,
                          bcoalm.IMP_LMT_CDT AS LMT_CDT,
                          bcoalm.IMP_LMT_DBT AS LMT_DBT
                   FROM   PABS_DT_BANCO_ALMOT bcoalm,
                          PABS_DT_SISTM_ENTRD_SALID entsal,
                          TCDTBAI TBAI,
                          TCDT040 T40,
                          TGEN_0112 pais
                  WHERE       BCOALM.COD_BIC_BCO = TBAI.TGCDSWSA
                          AND T40.TGCDSWSA(+) = TBAI.TGCDSWSA
                          AND PAIS.CODEPAIS = TBAI.cod_pais
                          AND ENTSAL.COD_SIS_ENT_SAL = bcoalm.cod_sis_sal
                          --------------------------------------
                          AND PAIS.TCCPAIS4 = NVL (p_COD_PAIS, PAIS.TCCPAIS4)
                          AND T40.TCNIFENT(+) <> v_BANCO
                          AND bcoalm.COD_BIC_BCO =
                                NVL (v_COD_BIC, bcoalm.COD_BIC_BCO)
                          AND T40.TCNIFENT = v_COD_RUT
                          AND UPPER (TBAI.des_bco) LIKE
                                NVL (v_NOMBRE, UPPER (TBAI.des_bco)) || '%'
                          AND BCOALM.FLG_VGN IN (V_FLG_VGN_SI, V_FLG_VGN_NO)
               ORDER BY   BCOALM.COD_BIC_BCO;
         END IF;
      EXCEPTION
         WHEN DUP_VAL_ON_INDEX
         THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            P_ERROR := PKG_PAB_CONSTANTES.V_ERROR;
            PKG_PAB_TRACKING.SP_PAB_INS_LOG_BD (err_code,
                                                V_NOM_PCK,
                                                err_msg,
                                                V_NOM_SP);
         WHEN OTHERS
         THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            p_s_mensaje := err_code || '-' || err_msg;
            RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
      END;
   END SP_PAB_CON_BCO_ALMOT;

   -- ***********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_INS_BCO_ALMOT
   -- Objetivo: Procedimiento actualiza estado de la operaci?n abonada exitosamente.
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: N/A
   -- Fecha: 18/11/16
   -- Autor: Santander
   -- Input:
   -- COD_BIC -> codigo bis del banco
   -- p_IMP_LMT_CDT  -> Importe Limite Credito
   -- p_IMP_LMT_DBT  -> Importe Limite Debito
   -- p_CANALPAGO    -> Canla de pago
   -- p_USUARIO      -> Corresponde al usuario
   -- Output:
   -- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.

   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   PROCEDURE SP_PAB_INS_BCO_ALMOT (p_COD_BIC       IN     CHAR,
                                   p_IMP_LMT_CDT   IN     NUMBER,
                                   p_IMP_LMT_DBT   IN     NUMBER,
                                   p_CANALPAGO     IN     CHAR,
                                   p_USUARIO       IN     CHAR,
                                   p_CURSOR           OUT SYS_REFCURSOR,
                                   p_ERROR            OUT NUMBER)
   IS
      --Seteo de Variables
      V_NOM_SP    VARCHAR2 (30) := 'SP_PAB_INS_BCO_ALMOT';
      V_EXISTE    NUMERIC;
      V_COD_BIC   CHAR (11) := p_COD_BIC;
   BEGIN
      P_ERROR := PKG_PAB_CONSTANTES.V_OK;

      BEGIN
         --Insertamos si da error de insert es por que ya existia y se debe actualizar
         BEGIN
            INSERT INTO PABS_DT_BANCO_ALMOT (COD_BIC_BCO,
                                             COD_SIS_SAL,
                                             IMP_MAX_BIL_CBA,
                                             FLG_VGN,
                                             COD_USR_CRC, -- temporalmente comentado a espera de cambio en el modelo
                                             FEC_CRC,
                                             FLG_CTB_MN,
                                             FLG_CTB_MX,
                                             IMP_LMT_CDT,
                                             IMP_LMT_DBT)
              VALUES   (p_COD_BIC,
                        p_CANALPAGO,
                        0,
                        PKG_PAB_CONSTANTES.V_FLG_VGN_SI,
                        p_USUARIO, -- temporalmente comentado a espera de cambio en el modelo
                        SYSDATE,
                        0,
                        0,
                        p_IMP_LMT_CDT,
                        p_IMP_LMT_DBT);
         EXCEPTION
            WHEN DUP_VAL_ON_INDEX
            THEN
               --Si el banco ya existia y esta desabilitado se vuelve a habilitar
               UPDATE   PABS_DT_BANCO_ALMOT
                  SET   FLG_VGN = PKG_PAB_CONSTANTES.V_FLG_VGN_SI,
                        COD_USR_ULT_MOD = p_USUARIO,
                        FEC_ULT_MOD = SYSDATE,
                        COD_SIS_SAL = p_CANALPAGO,
                        IMP_LMT_CDT = p_IMP_LMT_CDT,
                        IMP_LMT_DBT = p_IMP_LMT_DBT
                WHERE   COD_BIC_BCO = V_COD_BIC
                        AND FLG_VGN = PKG_PAB_CONSTANTES.V_FLG_VGN_ELI;
            WHEN OTHERS
            THEN
               err_code := SQLCODE;
               err_msg := SUBSTR (SQLERRM, 1, 300);
               PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                   v_NOM_PCK,
                                                   err_msg,
                                                   v_NOM_SP);
               p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
               p_s_mensaje := err_code || '-' || err_msg;
               RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
         END;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            COMMIT;
         WHEN OTHERS
         THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
      END;
   END SP_PAB_INS_BCO_ALMOT;

   -- ***********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_ACT_BCO_ALMOT
   -- Objetivo: Procedimiento actualiza estado de la operaci?n abonada exitosamente.
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: N/A
   -- Fecha: 18/11/16
   -- Autor: Santander
   -- Input:
   --    p_COD_BIC -> codigo bis del banco
   --    p_IMP_LMT_CDT  -> Importe Limite Credito
   --    p_IMP_LMT_DBT  -> Importe Limite Debito
   --    p_HABILITADO -> indica si esta habilitado
   --    p_CANALPAGO -> corresponde al canal de pago
   --    p_USUARIO -> corresponde al codigo de usuario
   -- Output:
   -- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.

   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   PROCEDURE SP_PAB_ACT_BCO_ALMOT (p_COD_BIC       IN     CHAR,
                                   p_IMP_LMT_CDT   IN     NUMBER,
                                   p_IMP_LMT_DBT   IN     NUMBER,
                                   p_HABILITADO    IN     CHAR,
                                   p_CANALPAGO     IN     CHAR,
                                   p_USUARIO       IN     CHAR,
                                   p_ERROR            OUT NUMBER)
   IS
      --Seteo de Variables
      V_NOM_SP    VARCHAR2 (30) := 'SP_ACT_BCO_ALMOT';
      v_COD_BIC   CHAR (11);
   BEGIN
      P_ERROR := PKG_PAB_CONSTANTES.V_OK;

      BEGIN
         v_COD_BIC := p_COD_BIC;

         UPDATE   PABS_DT_BANCO_ALMOT
            SET   COD_SIS_SAL = p_CANALPAGO,
                  IMP_LMT_CDT = p_IMP_LMT_CDT,
                  IMP_LMT_DBT = p_IMP_LMT_DBT,
                  FLG_VGN = NVL (p_HABILITADO, FLG_VGN),
                  COD_USR_ULT_MOD = p_USUARIO,
                  FEC_ULT_MOD = SYSDATE
          WHERE   COD_BIC_BCO = v_COD_BIC;

         COMMIT;
      EXCEPTION
         WHEN DUP_VAL_ON_INDEX
         THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300) || p_USUARIO; --Pendiente por definici�n g.
            P_ERROR := PKG_PAB_CONSTANTES.V_ERROR;
            PKG_PAB_TRACKING.SP_PAB_INS_LOG_BD (err_code,
                                                V_NOM_PCK,
                                                err_msg,
                                                V_NOM_SP);
         WHEN OTHERS
         THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            p_s_mensaje := err_code || '-' || err_msg;
            RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
      END;
   END SP_PAB_ACT_BCO_ALMOT;


   -- ***********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_CON_BCO_COR_ALMOT
   -- Objetivo: Procedimiento que consultar bancos de altos montos
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas:  TCDTBAI , TCDT040, TGEN_0112
   -- Fecha: 01/12/2016
   -- Autor: Santander
   -- Input:
   --    p_COD_PAIS -> corresponde al codigo del pais
   --    p_COD_BIC -> corresponde al codigo bic del banco
   --    p_COD_RUT -> corresponde al rut del banco
   --    p_NOMBRE -> corresponde al nombre del banco
   -- Output:
   -- p_CURSOR -> Cursor con los registros de los elementos
   -- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>--
   -- **************************************************************************
   PROCEDURE SP_PAB_CON_BCO_COR_ALMOT (p_COD_PAIS   IN     CHAR,
                                       p_COD_BIC    IN     CHAR,
                                       p_COD_RUT    IN     CHAR,
                                       p_NOMBRE     IN     CHAR,
                                       p_CURSOR        OUT SYS_REFCURSOR,
                                       p_ERROR         OUT NUMBER)
   IS
      --Seteo de Variables
      V_NOM_SP    VARCHAR2 (30) := 'SP_PAB_CON_BCO_COR_ALMOT';
      v_COD_BIC   CHAR (11);
      v_NOMBRE    VARCHAR2 (40);
      v_COD_RUT   VARCHAR2 (15);
   BEGIN
      P_ERROR := PKG_PAB_CONSTANTES.V_OK;

      BEGIN
         --Seteo
         v_COD_BIC := p_COD_BIC;
         v_NOMBRE := UPPER (p_NOMBRE);
         v_COD_RUT := UPPER (p_COD_RUT);

         IF (v_COD_RUT IS NULL)
         THEN
            OPEN p_CURSOR FOR
               SELECT   TBAI.TGCDSWSA AS COD_BIC,
                        T40.NBBAN AS DES_BCO,
                        TBAI.des_bco AS NOMBRE_BCO,
                        TBAI.COD_PAIS AS CODIGOPAIS,
                        PAIS.TCMPAIS AS NOMBREPAIS,
                        T40.TCNIFENT AS RUT_BCO,
                        TBAI.des_cd AS NOMBRECIUDAD,
                        TBAI.DSC_DCM_1 AS DIRECCION,
                        TBAI.FLG_COR AS CORRESPONSAL,
                        T40.TGCDSWSA AS TGCDSWSA,
                        T40.STBANCO AS STBANCO
                 FROM   TCDTBAI TBAI, TCDT040 T40, TGEN_0112 pais
                WHERE       TBAI.TGCDSWSA = T40.TGCDSWSA(+)
                        AND PAIS.CODEPAIS = TBAI.COD_PAIS
                        AND PAIS.TCCPAIS4 = NVL (p_COD_PAIS, PAIS.TCCPAIS4)
                        AND TBAI.TGCDSWSA = NVL (v_COD_BIC, TBAI.TGCDSWSA)
                        AND UPPER (TBAI.des_bco) LIKE
                              NVL (v_NOMBRE, UPPER (TBAI.des_bco)) || '%'
                        AND T40.TCNIFENT(+) <> v_BANCO
                        AND TBAI.TGCDSWSA NOT IN
                                 (SELECT   bcoalm.COD_BIC_BCO
                                    FROM   PABS_DT_BANCO_ALMOT bcoalm
                                   WHERE   bcoalm.FLG_VGN IN
                                                 (PKG_PAB_CONSTANTES.V_FLG_VGN_SI,
                                                  PKG_PAB_CONSTANTES.V_FLG_VGN_NO));
         ELSE
            OPEN p_CURSOR FOR
               SELECT   TBAI.TGCDSWSA AS COD_BIC,
                        T40.NBBAN AS DES_BCO,
                        TBAI.des_bco AS NOMBRE_BCO,
                        TBAI.COD_PAIS AS CODIGOPAIS,
                        PAIS.TCMPAIS AS NOMBREPAIS,
                        T40.TCNIFENT AS RUT_BCO,
                        TBAI.des_cd AS NOMBRECIUDAD,
                        TBAI.DSC_DCM_1 AS DIRECCION,
                        TBAI.FLG_COR AS CORRESPONSAL,
                        T40.TGCDSWSA AS TGCDSWSA,
                        T40.STBANCO AS STBANCO
                 FROM   TCDTBAI TBAI, TCDT040 T40, TGEN_0112 pais
                WHERE       TBAI.TGCDSWSA = T40.TGCDSWSA(+)
                        AND T40.TCNIFENT = v_COD_RUT
                        AND PAIS.CODEPAIS = TBAI.COD_PAIS
                        AND PAIS.TCCPAIS4 = NVL (p_COD_PAIS, PAIS.TCCPAIS4)
                        AND TBAI.TGCDSWSA = NVL (v_COD_BIC, TBAI.TGCDSWSA)
                        AND UPPER (TBAI.des_bco) LIKE
                              NVL (v_NOMBRE, UPPER (TBAI.des_bco)) || '%'
                        AND T40.TCNIFENT(+) <> v_BANCO
                        AND TBAI.TGCDSWSA NOT IN
                                 (SELECT   bcoalm.COD_BIC_BCO
                                    FROM   PABS_DT_BANCO_ALMOT bcoalm
                                   WHERE   bcoalm.FLG_VGN IN
                                                 (PKG_PAB_CONSTANTES.V_FLG_VGN_SI,
                                                  PKG_PAB_CONSTANTES.V_FLG_VGN_NO));
         END IF;
      EXCEPTION
         WHEN DUP_VAL_ON_INDEX
         THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300) || p_COD_RUT; --Pendiente por definici�n g.
            P_ERROR := PKG_PAB_CONSTANTES.V_ERROR;
            PKG_PAB_TRACKING.SP_PAB_INS_LOG_BD (err_code,
                                                V_NOM_PCK,
                                                err_msg,
                                                V_NOM_SP);
         WHEN OTHERS
         THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            p_s_mensaje := err_code || '-' || err_msg;
            RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
      END;
   END SP_PAB_CON_BCO_COR_ALMOT;

   --
   -- ***********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_ELI_BCO_ALMOT
   -- Objetivo: Procedimiento que Elimina bancos de altos montos
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas:  PABS_DT_BANCO_ALMOT
   -- Fecha: 01/12/2016
   -- Autor: Santander
   -- Input:
   -- p_COD_BIC    -> Bic banco
   -- p_HABILITADO  -> Estado
   -- p_USUARIO   -> Usuario
   -- Output:
   -- p_CURSOR -> Cursor con los registros de los 15 elementos
   -- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>--
   -- ***********************************************************************************************
   PROCEDURE SP_PAB_ELI_BCO_ALMOT (p_COD_BIC      IN     CHAR,
                                   p_HABILITADO   IN     NUMBER,
                                   p_USUARIO      IN     CHAR,
                                   p_ERROR           OUT NUMBER)
   IS
      --Seteo de Variables
      V_NOM_SP    VARCHAR2 (30) := 'SP_PAB_ELI_BCO_ALMOT';
      V_COD_BIC   CHAR (11);
   BEGIN
      BEGIN
         P_ERROR := PKG_PAB_CONSTANTES.V_OK;
         V_COD_BIC := p_COD_BIC;

         --Dejamos banco como deshabilitado
         UPDATE   PABS_DT_BANCO_ALMOT
            SET   FLG_VGN = PKG_PAB_CONSTANTES.V_FLG_VGN_ELI               --2
                                                            ,
                  COD_USR_ULT_MOD = p_USUARIO,
                  FEC_ULT_MOD = SYSDATE
          WHERE   COD_BIC_BCO = V_COD_BIC;

         COMMIT;
      EXCEPTION
         WHEN DUP_VAL_ON_INDEX
         THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300) || p_USUARIO || p_HABILITADO; --Pendiente por definici�n g.
            P_ERROR := PKG_PAB_CONSTANTES.V_ERROR;
            PKG_PAB_TRACKING.SP_PAB_INS_LOG_BD (err_code,
                                                V_NOM_PCK,
                                                err_msg,
                                                V_NOM_SP);
         WHEN OTHERS
         THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            p_s_mensaje := err_code || '-' || err_msg;
            RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
      END;
   END SP_PAB_ELI_BCO_ALMOT;

   --
   -- ***********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_ACT_MON_ALMOT
   -- Objetivo: Procedimiento actualiza monto de la moneda
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_MONDA_ALMOT
   -- Fecha: 18/11/16
   -- Autor: Santander
   -- Input:
   --    p_COD_DVI -> tipo de moneda
   --    p_IMP_MAX_SIN_ATZ -> importe maximo
   --    p_USR_MOD -> corresponde al codigo de usuario
   -- Output:
   -- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>--
   -- ***********************************************************************************************
   PROCEDURE SP_PAB_ACT_MON_ALMOT (p_COD_DVI           IN     CHAR,
                                   p_IMP_MAX_SIN_ATZ   IN     NUMBER,
                                   p_USR_MOD           IN     CHAR,
                                   p_ERROR                OUT NUMBER)
   IS
      --Seteo de Variables
      V_NOM_SP    VARCHAR2 (30) := 'SP_PAB_ACT_MON_ALMOT';
      V_COD_DVI   CHAR (3);
   BEGIN
      P_ERROR := PKG_PAB_CONSTANTES.V_OK;

      BEGIN
         V_COD_DVI := p_COD_DVI;

         UPDATE   PABS_DT_MONDA_ALMOT
            SET   IMP_MAX_SIN_ATZ = p_IMP_MAX_SIN_ATZ,
                  COD_USR_ULT_MOD = p_USR_MOD,
                  FEC_ULT_MOD = SYSDATE
          WHERE   COD_DVI = V_COD_DVI;

         p_ERROR := PKG_PAB_CONSTANTES.v_OK;

         COMMIT;
      EXCEPTION
         WHEN DUP_VAL_ON_INDEX
         THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            P_ERROR := PKG_PAB_CONSTANTES.V_ERROR;
            PKG_PAB_TRACKING.SP_PAB_INS_LOG_BD (err_code,
                                                V_NOM_PCK,
                                                err_msg,
                                                V_NOM_SP);
         WHEN OTHERS
         THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            p_s_mensaje := err_code || '-' || err_msg;
            RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
      END;
   END SP_PAB_ACT_MON_ALMOT;

   -- ***********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_ELI_MON_ALMOT
   -- Objetivo: Procedimiento que Elimina bancos de altos montos
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas:  PABS_DT_MONDA_ALMOT
   -- Fecha: 01/12/2016
   -- Autor: Santander
   -- Input:
   -- p_COD_DVI  ->
   -- p_USR_MOD  ->
   -- N/A
   -- Output:
   -- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   PROCEDURE SP_PAB_ELI_MON_ALMOT (p_COD_DVI   IN     CHAR,
                                   p_USR_MOD   IN     CHAR,
                                   p_ERROR        OUT NUMBER)
   IS
      --Seteo de Variables
      V_NOM_SP         VARCHAR2 (30) := 'SP_PAB_ELI_MON_ALMOT';
      v_COD_DVI        CHAR (3);
      v_valorDefecto   NUMBER (1) := 0;
   BEGIN
      P_ERROR := PKG_PAB_CONSTANTES.V_OK;

      BEGIN
         v_COD_DVI := p_COD_DVI;

         UPDATE   PABS_DT_MONDA_ALMOT
            SET   IMP_MAX_SIN_ATZ = v_valorDefecto,
                  COD_USR_ULT_MOD = p_USR_MOD,
                  FEC_ULT_MOD = SYSDATE
          WHERE   COD_DVI = v_COD_DVI;

         COMMIT;
      EXCEPTION
         WHEN DUP_VAL_ON_INDEX
         THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            P_ERROR := PKG_PAB_CONSTANTES.V_ERROR;
            PKG_PAB_TRACKING.SP_PAB_INS_LOG_BD (err_code,
                                                V_NOM_PCK,
                                                err_msg,
                                                V_NOM_SP);
         WHEN OTHERS
         THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            p_s_mensaje := err_code || '-' || err_msg;
            RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
      END;
   END SP_PAB_ELI_MON_ALMOT;

   --
   -- ***********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_INS_MON_ALMOT
   -- Objetivo: Procedimiento que Elimina bancos de altos montos
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas:  PABS_DT_MONDA_ALMOT
   -- Fecha: 01/12/2016
   -- Autor: Santander
   -- Input:
   --    p_COD_DVI : Codigo de moneda
   --    p_IMP_MAX_SIN_ATZ : valor de importe
   --    p_FLG_VGN : flag de vigencia
   --    p_USR_MOD . usuario de modificacion
   -- N/A
   -- Output:
   -- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   PROCEDURE SP_PAB_INS_MON_ALMOT (p_COD_DVI           IN     CHAR,
                                   p_IMP_MAX_SIN_ATZ   IN     NUMBER,
                                   p_FLG_VGN           IN     CHAR,
                                   p_USR_MOD           IN     CHAR,
                                   p_ERROR                OUT NUMBER)
   IS
      --Seteo de Variables
      V_NOM_SP    VARCHAR2 (30) := 'SP_PAB_INS_MON_ALMOT';
      V_USR_MOD   CHAR (8) := 'PAB';
   BEGIN
      P_ERROR := PKG_PAB_CONSTANTES.V_OK;

      BEGIN
         INSERT INTO PABS_DT_MONDA_ALMOT (COD_DVI,
                                          IMP_MAX_SIN_ATZ,
                                          FLG_VGN,
                                          COD_USR_CRC,
                                          FEC_CRC)
           VALUES   (p_COD_DVI,
                     p_IMP_MAX_SIN_ATZ,
                     p_FLG_VGN,
                     V_USR_MOD,
                     SYSDATE);

         COMMIT;
      EXCEPTION
         WHEN DUP_VAL_ON_INDEX
         THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300) || p_USR_MOD; --Pendiente por definici�n g.
            P_ERROR := PKG_PAB_CONSTANTES.V_ERROR_KEY_DUPLICATE;
            PKG_PAB_TRACKING.SP_PAB_INS_LOG_BD (err_code,
                                                V_NOM_PCK,
                                                err_msg,
                                                V_NOM_SP);
         WHEN OTHERS
         THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            p_s_mensaje := err_code || '-' || err_msg;
            RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
      END;
   END SP_PAB_INS_MON_ALMOT;

   --
   -- ***********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_CON_MON_ALMOT
   -- Objetivo: Procedimiento que Cosulta las monedas de altos montos
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas:  PABS_DT_MONDA_ALMOT
   -- Fecha: 01/12/2016
   -- Autor: Santander
   -- Input:
   --    p_COD_DVI : Codigo de monedan
   -- N/A
   -- Output:
   -- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   PROCEDURE SP_PAB_CON_MON_ALMOT (p_COD_DVI   IN     CHAR,
                                   p_CURSOR       OUT SYS_REFCURSOR,
                                   p_ERROR        OUT NUMBER)
   IS
      --Seteo de Variables
      V_NOM_SP    VARCHAR2 (30) := 'SP_PAB_CON_MON_ALMOT';
      v_COD_DVI   CHAR (3);
   BEGIN
      P_ERROR := PKG_PAB_CONSTANTES.V_OK;

      v_COD_DVI := p_COD_DVI;

      OPEN p_CURSOR FOR
         SELECT   MONDA.COD_DVI AS COD_DVI,
                  MONDA.IMP_MAX_SIN_ATZ AS MONTO_MAX
           FROM   PABS_DT_MONDA_ALMOT MONDA
          WHERE   MONDA.COD_DVI = NVL (v_COD_DVI, MONDA.COD_DVI)
                  AND MONDA.FLG_VGN IN (PKG_PAB_CONSTANTES.V_FLG_VGN_SI);

      P_ERROR := PKG_PAB_CONSTANTES.V_OK;
   EXCEPTION
      WHEN DUP_VAL_ON_INDEX
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         P_ERROR := PKG_PAB_CONSTANTES.V_ERROR;
         PKG_PAB_TRACKING.SP_PAB_INS_LOG_BD (err_code,
                                             V_NOM_PCK,
                                             err_msg,
                                             V_NOM_SP);
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END SP_PAB_CON_MON_ALMOT;


   --
   -- ***********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_CON_TPO_OPE_ALMOT
   -- Objetivo: Procedimiento que consulta tipo de operacion CONSTITUCION GARANTIA COMBANC
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas:  PABS_DT_TIPO_OPRCN_ALMOT
   -- Fecha: 17/02/2017
   -- Autor: Santander
   -- Input:
   -- Output:
   -- p_CURSOR -> Cursor con los registros de los elementos
   -- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   PROCEDURE SP_PAB_CON_TPO_OPE_ALMOT (p_CURSOR   OUT SYS_REFCURSOR,
                                       p_ERROR    OUT NUMBER)
   IS
      --Seteo de Variables
      V_NOM_SP     VARCHAR2 (30) := 'SP_PAB_CON_TPO_OPE_ALMOT';
      v_TIPO_OPE   CHAR;
   BEGIN
      P_ERROR := PKG_PAB_CONSTANTES.V_OK;

      OPEN p_CURSOR FOR
         SELECT   COD_TPO_OPE_AOS AS COD_TPO_OPE,
                  DSC_TPO_OPE_AOS AS DSC_TPO_OPE,
                  IMP_GRT_BCS AS MTO_IMP
           FROM   PABS_DT_TIPO_OPRCN_ALMOT
          WHERE   COD_TPO_OPE_AOS = PKG_PAB_CONSTANTES.V_STR_TPO_OPE_CDLE;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg :=
            'No se han encontrado registos al tipo de operacion :'
            || PKG_PAB_CONSTANTES.V_STR_TPO_OPE_CDLE;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;

         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END SP_PAB_CON_TPO_OPE_ALMOT;


   --
   -- ***********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_ACT_TPO_OPE_ALMOT
   -- Objetivo: Procedimiento que Actualiza tipo de operacion CONSTITUCION GARANTIA COMBANC
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas:  NN
   -- Fecha: 17/02/2017
   -- Autor: Santander
   -- Input:
   --  p_MONTO_OPE -> corresponde al Monto
   --  p_COD_USR -> Codigo de Usuario
   -- Output:
   -- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   PROCEDURE SP_PAB_ACT_TPO_OPE_ALMOT (p_MONTO_OPE   IN     NUMBER,
                                       p_COD_USR     IN     CHAR,
                                       p_ERROR          OUT NUMBER)
   IS
      --Seteo de Variables
      V_NOM_SP      VARCHAR2 (30) := 'SP_PAB_ACT_TPO_OPE_ALMOT';
      v_MONTO_OPE   NUMBER;
      v_COD_USR     CHAR (8);
   BEGIN
      P_ERROR := PKG_PAB_CONSTANTES.V_OK;
      v_MONTO_OPE := p_MONTO_OPE;
      v_COD_USR := 0;

      UPDATE   PABS_DT_TIPO_OPRCN_ALMOT
         SET   IMP_GRT_BCS = v_MONTO_OPE,
               COD_USR_ULT_MOD = v_COD_USR,
               FEC_ULT_MOD = SYSDATE
       WHERE   COD_TPO_OPE_AOS = PKG_PAB_CONSTANTES.V_STR_TPO_OPE_CDLE;

      COMMIT;
   EXCEPTION
      WHEN DUP_VAL_ON_INDEX
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300) || p_COD_USR; --Pendiente por definici�n g.
         P_ERROR := PKG_PAB_CONSTANTES.V_ERROR;
         PKG_PAB_TRACKING.SP_PAB_INS_LOG_BD (err_code,
                                             V_NOM_PCK,
                                             err_msg,
                                             V_NOM_SP);
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END SP_PAB_ACT_TPO_OPE_ALMOT;

   --
   -- ***********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_CON_CTA_CNTBL_ALMOT
   -- Objetivo: Procedimiento que consulta los datos de las cuentas
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_CENTA_CNTBL, PABS_DT_SISTM_ENTRD_SALID
   -- Fecha: 12/DIC/2016
   -- Autor: Santander
   -- Input:
   -- p_COD_SIS_ENT_SAL -> corresponde al codigo del sistema
   -- p_FLAG_MRD_UTZ_SW -> Flag de vigencia
   -- Output:
   -- p_CURSOR
   -- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   PROCEDURE SP_PAB_CON_CTA_CNTBL_ALMOT (
      p_COD_SIS_ENT_SAL   IN     CHAR,
      p_FLAG_MRD_UTZ_SW   IN     NUMBER,
      p_CURSOR               OUT SYS_REFCURSOR,
      p_ERROR                OUT NUMBER
   )
   IS
      --Seteo de Variables
      V_NOM_SP            VARCHAR2 (30) := 'SP_PAB_CON_CTA_CNTBL_ALMOT';
      v_COD_SIS_ENT_SAL   CHAR (10);
      v_FLAG_MRD_UTZ_SW   NUMBER (1);
   BEGIN
      BEGIN
         P_ERROR := PKG_PAB_CONSTANTES.V_OK;

         v_COD_SIS_ENT_SAL := p_COD_SIS_ENT_SAL;

         IF (p_FLAG_MRD_UTZ_SW = -1)
         THEN
            v_FLAG_MRD_UTZ_SW := NULL;
         ELSE
            v_FLAG_MRD_UTZ_SW := p_FLAG_MRD_UTZ_SW;
         END IF;

         OPEN p_CURSOR FOR
              SELECT   SIS.DSC_SIS_ENT_SAL AS DSC_SIS_ENT_SAL,
                       DECODE (CTCN.FLAG_MRD_UTZ_SWF,
                               PKG_PAB_CONSTANTES.V_FLG_MRD_MN,
                               PKG_PAB_CONSTANTES.V_STR_FLG_MRD_MN,
                               PKG_PAB_CONSTANTES.V_FLG_MRD_MX,
                               PKG_PAB_CONSTANTES.V_STR_FLG_MRD_MX)
                          AS MERCADO,
                       CTCN.NUM_CTA_CTB AS NUM_CTA_CTB,
                       CTCN.NUM_CRT_CTA_CTB AS NUM_CRT_CTA_CTB,
                       CTCN.NUM_CNP_CTA_CTB AS NUM_CNP_CTA_CTB,
                       CTCN.COD_SUC_DTN_TPN AS COD_SUC_DTN_TPN,
                       CTCN.GLS_IFM_MVT_CTB AS GLS_IFM_MVT,
                       CTCN.GLS_COM_CTA_CTB AS GLS_COM_CTA,
                       CTCN.FLG_VGN AS FLG_VGN,
                       CTCN.COD_USR_CRC AS USR_CRC,
                       CTCN.FEC_CRC AS FEC_CRC,
                       CTCN.COD_USR_ULT_MOD AS USR_MOD,
                       CTCN.FEC_ULT_MOD AS FEC_MOD
                FROM   PABS_DT_CENTA_CNTBL CTCN, PABS_DT_SISTM_ENTRD_SALID SIS
               WHERE   CTCN.COD_SIS_ENT_SAL =
                          NVL (v_COD_SIS_ENT_SAL, CTCN.COD_SIS_ENT_SAL)
                       AND CTCN.COD_SIS_ENT_SAL = SIS.COD_SIS_ENT_SAL
                       AND CTCN.FLAG_MRD_UTZ_SWF =
                             NVL (v_FLAG_MRD_UTZ_SW, CTCN.FLAG_MRD_UTZ_SWF)
            ORDER BY   SIS.DSC_SIS_ENT_SAL;
      EXCEPTION
         WHEN DUP_VAL_ON_INDEX
         THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            P_ERROR := PKG_PAB_CONSTANTES.V_ERROR;
            PKG_PAB_TRACKING.SP_PAB_INS_LOG_BD (err_code,
                                                V_NOM_PCK,
                                                err_msg,
                                                V_NOM_SP);
         WHEN OTHERS
         THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            p_s_mensaje := err_code || '-' || err_msg;
            RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
      END;
   END SP_PAB_CON_CTA_CNTBL_ALMOT;

   -- ***********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_INS_CTA_CNTBL_ALMOT
   -- Objetivo: Procedimiento inserta los datos del corresponsal
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_CENTA_CNTBL
   -- Fecha: 12/DIC/2016
   -- Autor: Santander
   -- Input:
   --    p_COD_SIS_ENT_SAL -> corresponde al codigo del sistema
   --    p_FLAG_UTZ_SWF -> Flag de vigencia
   --    p_NUM_CTA_CTB -> corresponde al numero de cuenta
   --    p_NUM_CTA_CTB -> corresponde al numero de cuenta
   --    p_NUM_CRT_CTA_CTB -> corresponde al numero corto de cuenta
   --    p_NUM_CNP_CTA_CTB -> numero de concepto cuenta
   --    p_COD_SUC_DTN_TPN -> corresponde al codigo de la sucursal
   --    p_GLS_IFM_MVT_CTB -> corresponde a la glosa
   --    p_COD_USR_CRC -> Corresponde al numero codigo de usuario
   -- Output:
   -- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   PROCEDURE SP_PAB_INS_CTA_CNTBL_ALMOT (p_COD_SIS_ENT_SAL   IN     CHAR,
                                         p_FLAG_UTZ_SWF      IN     NUMBER,
                                         p_NUM_CTA_CTB       IN     CHAR,
                                         p_NUM_CRT_CTA_CTB   IN     CHAR,
                                         p_NUM_CNP_CTA_CTB   IN     CHAR,
                                         p_COD_SUC_DTN_TPN   IN     CHAR,
                                         p_GLS_IFM_MVT_CTB   IN     CHAR,
                                         p_GLS_COM_CTA_CTB   IN     CHAR,
                                         p_COD_USR_CRC       IN     NUMBER,
                                         p_ERROR                OUT NUMBER)
   IS
      --Seteo de Variables
      V_COD_USR_CRC       NUMBER;
      V_NOM_SP            VARCHAR2 (30) := 'SP_PAB_INS_CTA_CNTBL_ALMOT';
      V_FLG_VGN_SI        NUMERIC := PKG_PAB_CONSTANTES.V_FLG_VGN_SI;
      V_COD_SIS_ENT_SAL   CHAR (10);
   BEGIN
      V_COD_USR_CRC := p_COD_USR_CRC;
      P_ERROR := PKG_PAB_CONSTANTES.V_OK;
      V_COD_SIS_ENT_SAL := p_COD_SIS_ENT_SAL;

      INSERT INTO PABS_DT_CENTA_CNTBL (COD_SIS_ENT_SAL,
                                       FLAG_MRD_UTZ_SWF,
                                       NUM_CTA_CTB,
                                       NUM_CRT_CTA_CTB,
                                       NUM_CNP_CTA_CTB,
                                       COD_SUC_DTN_TPN,
                                       GLS_IFM_MVT_CTB,
                                       GLS_COM_CTA_CTB,
                                       FLG_VGN,
                                       COD_USR_CRC,
                                       FEC_CRC)
        VALUES   (V_COD_SIS_ENT_SAL,
                  p_FLAG_UTZ_SWF,
                  p_NUM_CTA_CTB,
                  p_NUM_CRT_CTA_CTB,
                  p_NUM_CNP_CTA_CTB,
                  p_COD_SUC_DTN_TPN,
                  p_GLS_IFM_MVT_CTB,
                  p_GLS_COM_CTA_CTB,
                  V_FLG_VGN_SI,
                  V_COD_USR_CRC,
                  SYSDATE);

      COMMIT;
   EXCEPTION
      WHEN DUP_VAL_ON_INDEX
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         P_ERROR := PKG_PAB_CONSTANTES.V_ERROR_KEY_DUPLICATE; /* Retorna valor 2, en caso llave duplicada.*/
         PKG_PAB_TRACKING.SP_PAB_INS_LOG_BD (err_code,
                                             V_NOM_PCK,
                                             err_msg,
                                             V_NOM_SP);
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END SP_PAB_INS_CTA_CNTBL_ALMOT;


   -- ***********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_CON_CRPSL_ALMOT
   -- Objetivo: Procedimiento almacenado que muestra los bancos corresponsales de Altos Montos.
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM, TCDTBAI TBAI, TGEN_0112 PAIS
   -- Tablas Usadas: PABS_DT_CENTA_BANCO_CRPSL
   -- Fecha: 12/DIC/2016
   -- Autor: Santander
   -- Input:
   --    p_COD_BIC -> Corresponde al codigo bic del banco
   --    p_COD_PAIS -> Corresponde al codigo del pais
   --    p_COD_MON -> corresponde al codigo del tipo de moneda
   --    p_COD_CTA -> Corresponde al codigo de cuenta
   -- Output:
   -- p_CURSOR
   -- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   PROCEDURE SP_PAB_CON_CRPSL_ALMOT (p_COD_BIC    IN     CHAR,
                                     p_COD_PAIS   IN     CHAR,
                                     p_COD_MON    IN     CHAR,
                                     p_COD_CTA    IN     CHAR,
                                     p_CURSOR        OUT SYS_REFCURSOR,
                                     p_ERROR         OUT NUMBER)
   IS
      --Seteo de Variables
      V_NOM_SP     VARCHAR2 (30) := 'SP_PAB_CON_CRPSL_ALMOT';
      v_COD_BIC    CHAR (11);
      v_COD_MON    CHAR (3);
      v_COD_PAIS   VARCHAR2 (2);
      v_COD_CTA    VARCHAR2 (30);
   BEGIN
      BEGIN
         P_ERROR := PKG_PAB_CONSTANTES.V_OK;
         v_COD_BIC := p_COD_BIC;
         v_COD_MON := p_COD_MON;
         v_COD_PAIS := p_COD_PAIS;
         v_COD_CTA := p_COD_CTA;

         OPEN p_CURSOR FOR
            SELECT   CRPSL.NUM_CTA_COR AS NUM_CTA_COR,
                     CRPSL.COD_BIC_BCO AS COD_BIC_BCO,
                     TBAI.DES_BCO AS NOM_BCO,
                     PAIS.TCMPAIS AS PAIS,
                     TBAI.DES_CD AS CUIDAD                           -- CUIDAD
                                          ,
                     CRPSL.COD_DVI AS MONEDA,
                     CRPSL.IMP_TOT_LCD AS TOTAL_LINEA,
                     CRPSL.FLG_OVR_LNA AS OVERNIGHT,
                     CRPSL.IMP_SDO_CTA_COR AS SALDO_CUENTA,
                     CRPSL.FLG_VGN AS FLG_VGN,
                     DECODE (CRPSL.NUM_PDI,
                             1,
                             'D�aria',
                             2,
                             'Semanal',
                             3,
                             'Mensual')
                        AS PERIOCIDAD,
                     CRPSL.COD_MT_SWF AS MT_CARTOLA,
                     CRPSL.POR_INT_LNA_CDT AS INTERES,
                     CRPSL.IMP_LMT_SBF_LNA AS LIMITE,
                     CRPSL.HOR_CRR_COR AS HORA_CIERRE,
                     CRPSL.FEC_VGN_LNA AS FECHA_VIGENCIA,
                     CRPSL.FLG_CHQ AS CHEQUE,
                     CRPSL.FLG_TRF_LNA AS AUTOTRANSFER,
                     CRPSL.IMP_MAX_SBF AS DISPONIBLE
              FROM   PABS_DT_CENTA_BANCO_CRPSL CRPSL,
                     TCDTBAI TBAI,
                     TGEN_0112 PAIS
             WHERE   TBAI.TGCDSWSA = CRPSL.COD_BIC_BCO
                     AND PAIS.CODEPAIS = TBAI.COD_PAIS
                     AND CRPSL.COD_BIC_BCO =
                           NVL (v_COD_BIC, CRPSL.COD_BIC_BCO)
                     AND PAIS.TCCPAIS4 = NVL (v_COD_PAIS, PAIS.TCCPAIS4)
                     AND CRPSL.COD_DVI = NVL (v_COD_MON, CRPSL.COD_DVI)
                     AND CRPSL.NUM_CTA_COR =
                           NVL (v_COD_CTA, CRPSL.NUM_CTA_COR)
                     AND CRPSL.FLG_VGN = PKG_PAB_CONSTANTES.V_FLG_VGN_SI;
      EXCEPTION
         WHEN DUP_VAL_ON_INDEX
         THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            P_ERROR := PKG_PAB_CONSTANTES.V_ERROR;
            PKG_PAB_TRACKING.SP_PAB_INS_LOG_BD (err_code,
                                                V_NOM_PCK,
                                                err_msg,
                                                V_NOM_SP);
         WHEN OTHERS
         THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            p_s_mensaje := err_code || '-' || err_msg;
            RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
      END;
   END SP_PAB_CON_CRPSL_ALMOT;


   --
   -- ***********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_INS_CRPSL_ALMOT
   -- Objetivo: Procedimiento inserta los datos del corresponsal
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_CENTA_BANCO_CRPSL
   -- Fecha: 12/DIC/2016
   -- Autor: Santander
   -- Input:
   --      p_NUM_CTA_COR   -> Corresponde al numero de cuenta
   --      p_COD_BIC_BCO   -> Corresponde al codigo bic del banco
   --      p_COD_DVI       -> Codigo de moneda
   --      p_IMP_TOT_LCD   -> monto linea de credito
   --      p_COD_USR_CRC   -> Codigo del usuario
   --      p_COD_MT_SWF   ->Codigo mt de cartola
   --      p_NUM_PDI      -> Periodicidad
   --      p_POR_INT_LNA_CDT   -> Interes de la linea
   --      p_FEC_VGN_LNA   -> Fecha de vigencia linea
   --      p_FLG_CHQ       -> Flag de cheque
   --      p_FLG_TRF_LNA   -> Flag transfer
   --      p_FLG_OVR_LNA   -> Flag OverNight
   --      p_IMP_LMT_SBF_LNA -> Limite linea SBIF
   --      p_HOR_CRR_COR   -> Hora de cierre del corresponsal
   --      p_IMP_MAX_SBF   ->Monto disponible
   -- Output:
   -- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   PROCEDURE SP_PAB_INS_CRPSL_ALMOT (p_NUM_CTA_COR       IN     VARCHAR2,
                                     p_COD_BIC_BCO       IN     CHAR,
                                     p_COD_DVI           IN     CHAR,
                                     p_IMP_TOT_LCD       IN     NUMBER,
                                     p_COD_USR_CRC       IN     CHAR,
                                     p_COD_MT_SWF        IN     NUMBER,
                                     p_NUM_PDI           IN     NUMBER,
                                     p_POR_INT_LNA_CDT   IN     NUMBER,
                                     p_FEC_VGN_LNA       IN     DATE,
                                     p_FLG_CHQ           IN     NUMBER,
                                     p_FLG_TRF_LNA       IN     NUMBER,
                                     p_FLG_OVR_LNA       IN     NUMBER,
                                     p_IMP_LMT_SBF_LNA   IN     NUMBER,
                                     p_HOR_CRR_COR       IN     VARCHAR2,
                                     p_IMP_MAX_SBF       IN     NUMBER,
                                     p_ERROR                OUT NUMBER)
   IS
      --Seteo de Variables
      V_NOM_SP        VARCHAR2 (30) := 'SP_PAB_INS_CRPSL_ALMOT';
      V_FLG_VGT_SI    NUMBER := PKG_PAB_CONSTANTES.V_FLG_VGN_SI;
      V_COD_USR_CRC   VARCHAR2 (8) := SUBSTR (p_COD_USR_CRC, 1, 8);
      v_COD_BIC       CHAR (11);
      v_FLG_CJA       NUMBER (1) := 1;

      v_NUM_CTA_COR   VARCHAR2 (30);
   BEGIN
      P_ERROR := PKG_PAB_CONSTANTES.V_OK;
      v_COD_BIC := p_COD_BIC_BCO;

      v_NUM_CTA_COR := TRIM (LEADING 0 FROM p_NUM_CTA_COR);

      INSERT INTO PABS_DT_CENTA_BANCO_CRPSL (NUM_CTA_COR,
                                             COD_BIC_BCO,
                                             COD_DVI,
                                             IMP_TOT_LCD,
                                             IMP_SDO_CTA_COR,
                                             FLG_VGN,
                                             COD_USR_CRC,
                                             FEC_CRC,
                                             COD_MT_SWF,
                                             NUM_PDI,
                                             POR_INT_LNA_CDT,
                                             FLG_CJA,
                                             FEC_APR_CTA,
                                             FEC_VGN_LNA,
                                             FLG_CHQ,
                                             FLG_TRF_LNA,
                                             FLG_OVR_LNA,
                                             IMP_LMT_SBF_LNA,
                                             HOR_CRR_COR,
                                             IMP_MAX_SBF)
        VALUES   (v_NUM_CTA_COR,
                  p_COD_BIC_BCO,
                  p_COD_DVI,
                  p_IMP_TOT_LCD,
                  0,
                  1,                                               --1 VIGENTE
                  p_COD_USR_CRC,
                  SYSDATE,
                  p_COD_MT_SWF,                        --PEDIR PUEDE SER VACIO
                  p_NUM_PDI,                           --PEDIR PUEDE SER VACIO
                  p_POR_INT_LNA_CDT,                                   --PEDIR
                  1,                                                -- AGREGAR
                  SYSDATE,
                  p_FEC_VGN_LNA,                                       --PEDIR
                  p_FLG_CHQ,                                           --PEDIR
                  p_FLG_TRF_LNA,                                       --PEDIR
                  p_FLG_OVR_LNA,                                       --PEDIR
                  p_IMP_LMT_SBF_LNA,                                   --PEDIR
                  p_HOR_CRR_COR,                                       --PEDIR
                  p_IMP_MAX_SBF);

      COMMIT;
      P_ERROR := PKG_PAB_CONSTANTES.V_OK;

      --INGRESAMOS EL REGISTRO EN LA TABLA SALDO

      PKG_PAB_CAJA.SP_PAB_INS_SLD_CRPSL (p_COD_BIC_BCO,
                                         v_NUM_CTA_COR,
                                         p_COD_DVI,
                                         P_ERROR);
   EXCEPTION
      WHEN DUP_VAL_ON_INDEX
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         P_ERROR := PKG_PAB_CONSTANTES.V_ERROR;
         PKG_PAB_TRACKING.SP_PAB_INS_LOG_BD (err_code,
                                             V_NOM_PCK,
                                             err_msg,
                                             V_NOM_SP);
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END SP_PAB_INS_CRPSL_ALMOT;



   -- ***********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_ACT_CRPSL_ALMOT
   -- Objetivo: Procedimiento actualiza los datos de la cuenta del corresponsal
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_CENTA_BANCO_CRPSL
   -- Fecha: 12/DIC/2016
   -- Autor: Santander
   -- Input:
   --      p_NUM_CTA_COR       -> Corresponde al numero de cuenta
   --      p_COD_BIC_BCO       -> Corresponde al codigo bic del banco
   --      p_COD_DVI           -> Codigo de moneda
   --      p_IMP_LMT_SBF_LNA   -> Limite linea SBIF
   --      p_IMP_MAX_SBF       ->Monto disponible
   --      p_HOR_CRR_COR       -> Hora de cierre del corresponsa
   --      p_COD_USR           -> Codigo del usuario
   -- Output:
   --       p_ERROR            -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   PROCEDURE SP_PAB_ACT_CRPSL_ALMOT (p_NUM_CTA_COR       IN     VARCHAR2,
                                     p_COD_BIC_BCO       IN     CHAR,
                                     p_COD_DVI           IN     CHAR,
                                     p_IMP_LMT_SBF_LNA   IN     NUMBER,
                                     p_IMP_MAX_SBF       IN     NUMBER,
                                     p_HOR_CRR_COR       IN     CHAR,
                                     p_COD_USR           IN     CHAR,
                                     p_IMP_TOT_LCD       IN     NUMBER,
                                     p_COD_MT_SWF        IN     NUMBER,
                                     p_NUM_PDI           IN     NUMBER,
                                     p_POR_INT_LNA_CDT   IN     NUMBER,
                                     p_FEC_VGN_LNA       IN     DATE,
                                     p_FLG_CHQ           IN     NUMBER,
                                     p_FLG_TRF_LNA       IN     NUMBER,
                                     p_FLG_OVR_LNA       IN     NUMBER,
                                     p_ERROR                OUT NUMBER)
   IS
      --Seteo de Variables
      V_NOM_SP            VARCHAR2 (30) := 'SP_PAB_ACT_CRPSL_ALMOT';
      v_NUM_CTA_COR       VARCHAR2 (30);
      V_COD_USR           CHAR (11);
      v_COD_BIC_BCO       CHAR (11);
      v_COD_DVI           CHAR (3);
      v_IMP_TOT_LCD       NUMERIC (17, 2);
      v_IMP_SDO_CTA_COR   NUMERIC (17, 2);
   BEGIN
      UPDATE   PABS_DT_CENTA_BANCO_CRPSL CRPSL
         SET   CRPSL.IMP_LMT_SBF_LNA = p_IMP_LMT_SBF_LNA,
               CRPSL.IMP_MAX_SBF = p_IMP_MAX_SBF,
               CRPSL.HOR_CRR_COR = p_HOR_CRR_COR,
               CRPSL.COD_USR_ULT_MOD = p_COD_USR,
               CRPSL.FEC_ULT_MOD = SYSDATE,
               CRPSL.NUM_CTA_COR = p_NUM_CTA_COR,
               CRPSL.COD_DVI = p_COD_DVI,
               CRPSL.IMP_TOT_LCD = p_IMP_TOT_LCD,
               CRPSL.COD_MT_SWF = p_COD_MT_SWF,
               CRPSL.NUM_PDI = p_NUM_PDI,
               CRPSL.POR_INT_LNA_CDT = p_POR_INT_LNA_CDT,
               CRPSL.FEC_VGN_LNA = p_FEC_VGN_LNA,
               CRPSL.FLG_CHQ = p_FLG_CHQ,
               CRPSL.FLG_TRF_LNA = p_FLG_TRF_LNA,
               CRPSL.FLG_OVR_LNA = p_FLG_OVR_LNA
       WHERE       CRPSL.NUM_CTA_COR = p_NUM_CTA_COR
               AND CRPSL.COD_BIC_BCO = p_COD_BIC_BCO
               AND CRPSL.COD_DVI = p_COD_DVI;

      COMMIT;

      P_ERROR := PKG_PAB_CONSTANTES.V_OK;

      err_msg := 'Actualizaci�n OK';
      PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                          v_NOM_PCK,
                                          err_msg,
                                          v_NOM_SP);
   EXCEPTION
      WHEN DUP_VAL_ON_INDEX
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         P_ERROR := 2;
         PKG_PAB_TRACKING.SP_PAB_INS_LOG_BD (err_code,
                                             V_NOM_PCK,
                                             err_msg,
                                             V_NOM_SP);
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END SP_PAB_ACT_CRPSL_ALMOT;

   --
   -- ***********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_ELI_CRPSL_ALMOT
   -- Objetivo: Procedimiento elimina de forma logica un corresponsal
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_CENTA_BANCO_CRPSL
   -- Fecha: 12/DIC/2016
   -- Autor: Santander
   -- Input:
   --   p_NUM_CTA_COR -> Corresponde al numero de cuenta
   --   p_COD_BIC_BCO -> Corresponde al codigo bic del banco
   --   p_COD_DVI      -> Codigo de moneda
   -- Output:
   --  p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   PROCEDURE SP_PAB_ELI_CRPSL_ALMOT (p_NUM_CTA_COR   IN     VARCHAR2,
                                     p_COD_BIC_BCO   IN     CHAR,
                                     p_COD_DVI       IN     CHAR,
                                     p_ERROR            OUT NUMBER)
   IS
      --Seteo de Variables
      V_NOM_SP   VARCHAR2 (30) := 'SP_PAB_ELI_CRPSL_ALMOT';
   BEGIN
      DELETE   PABS_DT_CENTA_BANCO_CRPSL
       WHERE       NUM_CTA_COR = p_NUM_CTA_COR
               AND COD_BIC_BCO = p_COD_BIC_BCO
               AND COD_DVI = p_COD_DVI;

      COMMIT;

      P_ERROR := PKG_PAB_CONSTANTES.V_OK;
      PKG_PAB_CAJA.SP_PAB_ELI_SLD_CRPSL (p_COD_BIC_BCO,
                                         p_NUM_CTA_COR,
                                         p_COD_DVI,
                                         P_ERROR);
   EXCEPTION
      WHEN DUP_VAL_ON_INDEX
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         P_ERROR := PKG_PAB_CONSTANTES.V_ERROR;
         PKG_PAB_TRACKING.SP_PAB_INS_LOG_BD (err_code,
                                             V_NOM_PCK,
                                             err_msg,
                                             V_NOM_SP);
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END SP_PAB_ELI_CRPSL_ALMOT;


   --
   -- ***********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_CON_CRPSL_COR_ALMOT
   -- Objetivo: Procedimiento consulta los corresponsales que no estan en las tabls de altos montos
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_CENTA_BANCO_CRPSL,  TCDTBAI, TGEN_0112, TCDT040
   -- Fecha: 12/DIC/2016
   -- Autor: Santander
   -- Input:
   -- p_RUT_BCO -> Corresponde al numero de rut del banco
   -- p_NOM_BCO -> Corresponde al nombre del banco
   -- p_COD_BIC -> Corresponde al codigo bic del banco
   -- p_COD_PAIS -> Codigo del pais
   -- Output:
   -- p_CURSOR
   -- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   PROCEDURE SP_PAB_CON_CRPSL_COR_ALMOT (p_COD_BIC    IN     CHAR,
                                         p_COD_PAIS   IN     CHAR,
                                         p_RUT_BCO    IN     CHAR,
                                         p_NOM_BCO    IN     CHAR,
                                         p_CURSOR        OUT SYS_REFCURSOR,
                                         p_ERROR         OUT NUMBER)
   IS
      --Seteo de Variables
      V_NOM_SP     VARCHAR2 (30) := 'SP_PAB_CON_CRPSL_COR_ALMOT';
      v_COD_BIC    CHAR (11);
      v_COD_PAIS   VARCHAR2 (2);
      v_RUT_BCO    VARCHAR2 (11);
      v_NOM_BCO    VARCHAR2 (40);
   BEGIN
      BEGIN
         P_ERROR := PKG_PAB_CONSTANTES.V_OK;
         v_COD_BIC := p_COD_BIC;
         v_COD_PAIS := p_COD_PAIS;
         v_NOM_BCO := UPPER (p_NOM_BCO);

         OPEN p_CURSOR FOR
            SELECT   TBAI.TGCDSWSA AS COD_BIC_BCO,
                     TBAI.DES_BCO AS NOM_BCO,
                     PAIS.TCMPAIS AS PAIS,
                     TBAI.DES_CD AS CUIDAD,
                     TBAI.FLG_COR AS ESTADO
              FROM   TCDTBAI TBAI, TGEN_0112 PAIS, PABS_DT_BANCO_ALMOT BCOAM
             WHERE       BCOAM.COD_BIC_BCO = TBAI.TGCDSWSA
                     AND BCOAM.FLG_VGN = PKG_PAB_CONSTANTES.V_FLG_VGN_SI --Banco este habilitado
                     AND TBAI.FLG_COR = 'S'
                     AND TBAI.COD_PAIS = PAIS.CODEPAIS -- comentado temporalmente por problemas de datos
                     AND TBAI.TGCDSWSA = NVL (v_COD_BIC, TBAI.TGCDSWSA)
                     AND PAIS.TCCPAIS4 = NVL (v_COD_PAIS, PAIS.TCCPAIS4)
                     AND UPPER (TBAI.des_bco) LIKE
                           NVL (v_NOM_BCO, UPPER (TBAI.des_bco)) || '%';
      EXCEPTION
         WHEN DUP_VAL_ON_INDEX
         THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            P_ERROR := PKG_PAB_CONSTANTES.V_ERROR;
            PKG_PAB_TRACKING.SP_PAB_INS_LOG_BD (err_code,
                                                V_NOM_PCK,
                                                err_msg,
                                                V_NOM_SP);
         WHEN OTHERS
         THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            p_s_mensaje := err_code || '-' || err_msg;
            RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
      END;
   END SP_PAB_CON_CRPSL_COR_ALMOT;

   --
   --**********************************************************************************************
   -- Funcion/Procedimiento: SP_ PAB_OBT_SIS_ENTSAL
   -- Objetivo: Procedimiento que entrega lista de canales segun el tipo ingresado
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_SISTM_ENTRD_SALID
   -- Fecha: 30/11/16
   -- Autor: Santander
   -- Input:
   -- p_COD_SIS_ENT_SAL -> Codigo de canal
   -- Output:
   -- p_CURSOR ->  Lista de horarios de inicio o cierre de canales
   -- p_ERROR -> Indicador de errores (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de utimos cambios>
   --***********************************************************************************************
   PROCEDURE SP_PAB_OBT_SIS_ENTSAL (p_COD_SIS_ENT_SAL   IN     CHAR,
                                    p_CURSOR               OUT SYS_REFCURSOR,
                                    p_ERROR                OUT NUMBER)
   IS
      v_NOM_SP         VARCHAR2 (30) := 'SP_PAB_OBT_SIS_ENTSAL';
      v_IN_OUT_NOVAL   NUMBER := 2;
   BEGIN
      OPEN p_CURSOR FOR
           SELECT   SISTENSA.COD_SIS_ENT_SAL AS CANAL,
                    SISTENSA.DSC_SIS_ENT_SAL AS GLS_CANAL,
                    SISTENSA.FLG_SIS_ENT_SAL AS TIPO_IN_OUT, -- 1 OUT // 0 IN  // OTROS PARA COMBOS
                    SISTENSA.HOR_ICO_SIS AS HORA_IN,
                    SISTENSA.HOR_CRR_SIS AS HORA_OUT,
                    SISTENSA.FLG_VGN AS FLG_HABILITADO -- 0 HABILITADO // 1 INHABILITADO
             FROM   PABS_DT_SISTM_ENTRD_SALID SISTENSA
            WHERE   SISTENSA.COD_SIS_ENT_SAL =
                       NVL (p_COD_SIS_ENT_SAL, SISTENSA.COD_SIS_ENT_SAL)
                    AND SISTENSA.FLG_SIS_ENT_SAL IN
                             (PKG_PAB_CONSTANTES.V_FLG_SIS_ENT,
                              PKG_PAB_CONSTANTES.V_FLG_SIS_SAL)
         ORDER BY   SISTENSA.DSC_SIS_ENT_SAL;

      p_ERROR := PKG_PAB_CONSTANTES.v_OK;
   --                                                                                                                                                                                                                                                                                                                                                                                                                                --Procedimiento ejecutado de forma correcta
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg :=
            'No se han encontrado registos para canal de salida/entrada : '
            || p_COD_SIS_ENT_SAL;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   --
   END SP_PAB_OBT_SIS_ENTSAL;


   --**********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_ACT_SIS_ENTSAL
   -- Objetivo: Procedimiento que entrega detale de canal seleccionado
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_SISTM_ENTRD_SALID
   -- Fecha: 30/11/16
   -- Autor: Santander
   -- Input:
   -- p_COD_SIS_ENT_SAL -> Codigo de canal
   -- p_COD_USR_ULT_MOD
   -- p_HOR_ICO_SIS
   -- p_HOR_CRR_SIS
   -- p_FLG_VGN
   -- Output:
   -- p_ERROR -> Indicador de errores (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de utimos cambios>
   --***********************************************************************************************
   PROCEDURE SP_PAB_ACT_SIS_ENTSAL (p_COD_SIS_ENT_SAL   IN     CHAR,
                                    p_COD_USR_ULT_MOD   IN     CHAR,
                                    p_HOR_ICO_SIS       IN     NUMBER,
                                    p_HOR_CRR_SIS       IN     NUMBER,
                                    p_FLG_VGN           IN     NUMBER,
                                    p_ERROR                OUT NUMBER)
   IS
      v_NOM_SP   VARCHAR2 (30) := 'SP_PAB_ACT_SIS_ENTSAL';
   BEGIN
      UPDATE   PABS_DT_SISTM_ENTRD_SALID SISTENSA
         SET   SISTENSA.HOR_ICO_SIS = p_HOR_ICO_SIS,
               SISTENSA.HOR_CRR_SIS = p_HOR_CRR_SIS,
               SISTENSA.FLG_VGN = p_FLG_VGN,
               SISTENSA.FEC_ULT_MOD = SYSDATE,
               SISTENSA.COD_USR_ULT_MOD = p_COD_USR_ULT_MOD
       WHERE   SISTENSA.COD_SIS_ENT_SAL = p_COD_SIS_ENT_SAL;

      COMMIT;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg :=
            'No se han encontrado registos para canal de salida/entrada : '
            || p_COD_SIS_ENT_SAL;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         RAISE;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         p_s_mensaje := err_code || '-' || err_msg;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END SP_PAB_ACT_SIS_ENTSAL;

   --***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_COMBO_BAN_ALMTO_MAN
   -- Objetivo: PROCEDIMIENTO ALMACENADO QUE CONSULTA Y ENTREGA TODOS LOS BANCOS DEL SISTEMA (HABILITADOS/DESHABILITADOS)
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_BANCO_ALMOT
   -- Fecha: 21/06/16
   -- Autor: Santander
   -- Input:
   -- N/A
   -- Output:
   -- p_CURSOR -> SYS_REFCURSOR, resultados de consulta con los bancos
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************
   PROCEDURE Sp_PAB_COMBO_BAN_ALMTO_MAN (p_CURSOR OUT SYS_REFCURSOR)
   IS
      v_NOM_SP   VARCHAR2 (30) := 'Sp_PAB_COMBO_BAN_ALMTO_MAN';
   --

   BEGIN
      OPEN p_CURSOR FOR
           SELECT   BAN.COD_BIC_BCO AS CODIGO, BAN.COD_BIC_BCO AS GLS_BAN_DEST
             FROM   PABS_DT_BANCO_ALMOT BAN
            WHERE   BAN.FLG_VGN IN
                          (PKG_PAB_CONSTANTES.v_REG_VIG,
                           PKG_PAB_CONSTANTES.V_REG_NOVIG)
         ORDER BY   BAN.COD_BIC_BCO;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         ERR_CODE := SQLCODE;
         ERR_MSG :=
               'No se han encontrado registos BANCO cuyo flag es : '
            || PKG_PAB_CONSTANTES.v_REG_VIG
            || ' o '
            || PKG_PAB_CONSTANTES.V_REG_NOVIG;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (ERR_CODE,
                                             v_NOM_PCK,
                                             ERR_MSG,
                                             v_NOM_SP);
      WHEN OTHERS
      THEN
         ERR_CODE := SQLCODE;
         ERR_MSG := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (ERR_CODE,
                                             v_NOM_PCK,
                                             ERR_MSG,
                                             v_NOM_SP);
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END Sp_PAB_COMBO_BAN_ALMTO_MAN;

   --***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_COMBO_BAN_NUE_ALMTO
   -- Objetivo: PROCEDIMIENTO ALMACENADO QUE CONSULTA Y ENTREGA TODOS LOS BANCOS DEL SISTEMA (HABILITADOS/DESHABILITADOS)
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_BANCO_ALMOT
   -- Fecha: 21/06/16
   -- Autor: Santander
   -- Input:
   -- N/A
   -- Output:
   -- p_CURSOR -> SYS_REFCURSOR, resultados de consulta con los bancos
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************
   PROCEDURE Sp_PAB_COMBO_BAN_NUE_ALMTO (p_CURSOR OUT SYS_REFCURSOR)
   IS
      v_NOM_SP   VARCHAR2 (30) := 'Sp_PAB_COMBO_BAN_NUE_ALMTO';
   --

   BEGIN
      OPEN p_CURSOR FOR
           SELECT   TBAI.TGCDSWSA AS COD_BIC
             FROM   TCDTBAI TBAI
            WHERE   TBAI.TGCDSWSA NOT IN
                          (SELECT   bcoal.COD_BIC_BCO
                             FROM   PABS_DT_BANCO_ALMOT bcoal
                            WHERE   bcoal.FLG_VGN IN
                                          (PKG_PAB_CONSTANTES.v_REG_VIG,
                                           PKG_PAB_CONSTANTES.V_REG_NOVIG))
         ORDER BY   TBAI.TGCDSWSA ASC;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         ERR_CODE := SQLCODE;
         ERR_MSG :=
               'No se han encontrado registos BANCO cuyo flag es : '
            || PKG_PAB_CONSTANTES.v_REG_VIG
            || ' o '
            || PKG_PAB_CONSTANTES.V_REG_NOVIG;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (ERR_CODE,
                                             v_NOM_PCK,
                                             ERR_MSG,
                                             v_NOM_SP);
      WHEN OTHERS
      THEN
         ERR_CODE := SQLCODE;
         ERR_MSG := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (ERR_CODE,
                                             v_NOM_PCK,
                                             ERR_MSG,
                                             v_NOM_SP);
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END Sp_PAB_COMBO_BAN_NUE_ALMTO;

   -- ***********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_COMBO_TPO_MT_CRTLA
   -- Objetivo: Procedimiento que retorne tipo de mt para cartola 940 � 950
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_TIPO_MT_SWIFT
   -- Fecha: 05/09/2017
   -- Autor: Santander- CAH
   -- Input:
   -- N/A
   -- Output:
   -- p_CURSOR -> Cursor con los registros de los 2 estados
   -- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   -- ***********************************************************************************************
   PROCEDURE SP_PAB_COMBO_TPO_MT_CRTLA (p_CURSOR   OUT SYS_REFCURSOR,
                                        p_ERROR    OUT NUMBER)
   IS
      V_NOM_SP   VARCHAR2 (30) := 'SP_PAB_COMBO_TPO_MT_CRTLA';
   BEGIN
      P_ERROR := PKG_PAB_CONSTANTES.V_OK;

      OPEN p_CURSOR FOR
           SELECT   DISTINCT (COD_MT_SWF)
             FROM   PABS_DT_TIPO_MT_SWIFT MT
            WHERE   MT.COD_MT_SWF IN
                          (PKG_PAB_CONSTANTES.V_COD_MT940,
                           PKG_PAB_CONSTANTES.V_COD_MT950)
         ORDER BY   COD_MT_SWF;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg :=
               'No se han encontrado registos SWIFT cuyo mensaje es : '
            || PKG_PAB_CONSTANTES.V_COD_MT940
            || ' o '
            || PKG_PAB_CONSTANTES.V_COD_MT950;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);

         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END SP_PAB_COMBO_TPO_MT_CRTLA;
END PKG_PAB_MANTENEDOR;
/


CREATE SYNONYM USR_PAB.PKG_PAB_MANTENEDOR FOR DBO_PAB.PKG_PAB_MANTENEDOR
/


GRANT EXECUTE ON DBO_PAB.PKG_PAB_MANTENEDOR TO R_APP_PAB
/

GRANT EXECUTE ON DBO_PAB.PKG_PAB_MANTENEDOR TO USR_PAB
/

