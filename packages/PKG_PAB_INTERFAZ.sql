CREATE OR REPLACE /* Formatted on 18-08-2020 17:45:39 (QP5 v5.115.810.9015) */
PACKAGE DBO_PAB.PKG_PAB_INTERFAZ
AS
   V_NOM_PCK     VARCHAR2 (30) := 'PKG_PAB_INTERFAZ';
   ERR_CODE      NUMBER := 0;
   ERR_MSG       VARCHAR2 (300);
   V_DES         VARCHAR2 (300);
   V_DES_BIT     VARCHAR2 (100);
   V_BANCO       VARCHAR2 (9) := '970150005';

   TYPE T_NUM_FOL_NMN
   IS
      TABLE OF NUMBER
         INDEX BY PLS_INTEGER;

   p_s_mensaje   VARCHAR2 (400);        --BORRA CUANDO SE PASE A PCKS ORIGINAL

   v_dia_ini     DATE := TRUNC (SYSDATE) + INTERVAL '1' MINUTE;
   v_dia_fin DATE
         := TRUNC (SYSDATE) + INTERVAL '23' HOUR + INTERVAL '59' MINUTE ;


   --**********************************************************************************************
   -- Funcion/Procedimiento: SP_GEN_ID_TRANSF
   -- Objetivo: Genera interfaz ID de transferencia.
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01
   -- Tablas Usadas: PABS_DT_DETLL_NOMNA
   -- Fecha: 24/05/2017
   -- Autor: Santander
   -- Input:
   -- p_FEC_PROC : Fecha de proceso de generacion ID
   -- Output:
   -- p_CURSOR : Cursor de respuesta de datos
   -- p_ERROR : indicador de ejecutor
   -- Input/Output:
   -- N/A.
   -- Retorno: N/A
   -- Observaciones: Fecha del dia
   --***********************************************************************************************
   PROCEDURE SP_GEN_ID_TRANSF (p_FEC_PROC   IN     VARCHAR2,
                               p_CURSOR        OUT SYS_REFCURSOR,
                               p_ERROR         OUT NUMBER);

   --***********************************************************************************************
   -- Funcion/Procedimiento: SP_GEN_VVV
   -- Objetivo: Genera interfaz de plazo.
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01
   -- Tablas Usadas: PABS_DT_DETLL_NOMNA
   -- Fecha: 06/06/2017
   -- Autor: Santander
   -- Input:
   -- p_FEC_ING : Fecha de proceso de generacion ID
   -- Output:
   -- p_OPERACIONES : Cursor de respuesta de datos
   -- p_CANTIDAD_OPE: cantidad de operaciones procesadas
   -- p_ERROR : indicador de ejecutor
   -- Input/Output:
   -- N/A.
   -- Retorno: N/A
   -- Observaciones: Fecha del dia
   --***********************************************************************************************
   PROCEDURE SP_GEN_VVV (p_FEC_ING        IN     VARCHAR2,
                         p_OPERACIONES       OUT SYS_REFCURSOR,
                         p_CANTIDAD_OPE      OUT NUMBER,
                         p_ERROR             OUT NUMBER);

   --***********************************************************************************************
   -- Funcion/Procedimiento: SP_GEN_INT_CTB
   -- Objetivo: Procedimiento que retorna cursor con operaciones de contabilidad
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_TIPO_FRMPG
   -- Fecha: 02-08-2019
   -- Autor: Santander CAFF
   -- Input:
   -- N/A
   -- Output:
   -- p_CURSOR -> RESULTADOS DE CONSULTA.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************
   PROCEDURE SP_GEN_INT_CTB (p_FEC DATE, p_CURSOR OUT SYS_REFCURSOR);
END PKG_PAB_INTERFAZ;
/


CREATE OR REPLACE /* Formatted on 18-08-2020 17:45:39 (QP5 v5.115.810.9015) */
PACKAGE BODY DBO_PAB.PKG_PAB_INTERFAZ
AS
   --***********************************************************************************************
   -- Funcion/Procedimiento: SP_GEN_ID_TRANSF
   -- Objetivo: Genera interfaz ID de transferencia.
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01
   -- Tablas Usadas: PABS_DT_DETLL_NOMNA
   -- Fecha: 24/05/2017
   -- Autor: Santander
   -- Input:
   -- p_FEC_PROC : Fecha de proceso de generacion ID
   -- Output:
   -- p_CURSOR : Cursor de respuesta de datos
   -- p_ERROR : indicador de ejecutor
   -- Input/Output:
   -- N/A.
   -- Retorno: N/A
   -- Observaciones: Fecha del dia
   --***********************************************************************************************
   PROCEDURE SP_GEN_ID_TRANSF (p_FEC_PROC   IN     VARCHAR2,
                               p_CURSOR        OUT SYS_REFCURSOR,
                               p_ERROR         OUT NUMBER)
   IS
      --
      v_NOM_SP        VARCHAR2 (30) := 'SP_GEN_ID_TRANSF';
      V_NUM_FOL_OPE   NUMBER (12);
      V_FEC_ISR_OPE   DATE;
      v_REF_CTB       CHAR (1) := '0';
   BEGIN
      BEGIN
         OPEN p_CURSOR FOR
            SELECT   PKG_PAB_CONSTANTES.V_BIC_BANCO_SANTANDER || ';'
                        AS BANCO_ORIGEN,
                     DET.COD_BCO_DTN || ';' AS BANCO_DESTINO,
                     DET.COD_DVI || ';' AS MONEDA,
                     LPAD (DET.IMP_OPE, 24, '0') || ';' AS MONTO,
                     RPAD (TO_CHAR (DET.FEC_ISR_OPE, 'YYYYMMDD'), 10, ' ')
                     || ';'
                        AS FECHA_OPE,
                     RPAD (TO_CHAR (DET.FEC_VTA, 'YYYYMMDD'), 10, ' ') || ';'
                        AS FECHA_VTA,
                     RPAD (
                        NVL (REPLACE (DET.NUM_REF_SWF, CHR (10), ''), ' '),
                        16,
                        ' '
                     )
                     || ';'
                        AS NUM_REF,
                     LPAD (NVL (REPLACE (DET.NUM_FOL_OPE, CHR (10), ''), 0),
                           16,
                           ' ')
                     || ';'
                        AS NUM_OPE,
                     RPAD (
                        NVL (REPLACE (DET.NUM_REF_EXT, CHR (10), ''), ' '),
                        16,
                        ' '
                     )
                     || ';'
                        AS NUM_REF_RLD,
                     RPAD (
                        NVL (
                           REPLACE (REPLACE (OPC.OBS_OPC_SWF, CHR (10), ''),
                                    CHR (13),
                                    ''),
                           ' '
                        ),
                        140,
                        ' '
                     )
                     || ';'
                        AS DETALLE,
                     LPAD (
                        NVL (
                           TRIM (OPC.NUM_DOC_ODN) || TRIM (OPC.COD_TPD_ODN),
                           '0'
                        ),
                        11,
                        '0'
                     )
                     || ';'
                        AS RUT_ORD,
                     RPAD (
                        NVL (TRIM (REPLACE (OPC.NOM_ODN, CHR (10), '')), ' '),
                        70,
                        ' '
                     )
                     || ';'
                        AS NOM_ORD,
                     LPAD (
                        NVL (REPLACE (OPC.NUM_CTA_ODN, CHR (10), ''), '0'),
                        34,
                        '0'
                     )
                     || ';'
                        AS CTA_ORD,
                     LPAD (
                        NVL (
                           TRIM (REPLACE (DET.NUM_DOC_BFC, '.', ''))
                           || TRIM (DET.COD_TPD_BFC),
                           '0'
                        ),
                        11,
                        '0'
                     )
                     || ';'
                        AS RUT_BEN,
                     RPAD (NVL (REPLACE (DET.NOM_BFC, CHR (10), ''), ' '),
                           70,
                           ' ')
                     || ';'
                        AS NOM_BEN,
                     LPAD (NVL (REPLACE (DET.NUM_CTA_BFC, CHR (10), ''), ''),
                           34,
                           '0')
                        AS CTA_BEN
              FROM   PABS_DT_DETLL_OPRCN DET,
                     PABS_DT_OPRCN_INFCN_OPCON OPC,
                     PABS_DT_RGTRO_OPRCN_CARGO_ABON ABN
             WHERE       DET.NUM_FOL_OPE = OPC.NUM_FOL_OPE
                     AND ABN.NUM_MOV_CTB > v_REF_CTB -- VALIDAMOS QUE NO TENGA REFERENCIA VACIA
                     AND ABN.NUM_FOL_OPE = DET.NUM_FOL_OPE --SE AGREGA TABLA DE
                     AND ABN.FEC_ISR_OPE = DET.FEC_ISR_OPE
                     AND DET.FEC_ISR_OPE = OPC.FEC_ISR_OPE
                     AND DET.FLG_MRD_UTZ_SWF = 0       -- SOLO MONEDA NACIONAL
                     AND COD_MT_SWF IN
                              (PKG_PAB_CONSTANTES.V_COD_MT103,
                               PKG_PAB_CONSTANTES.V_COD_MT202) -- MOVIMIENTOS CUENTAS CORRIENTES, SE INCLUYE 202DEVFDOS
                     AND DET.FEC_ISR_OPE BETWEEN TO_DATE (
                                                    p_FEC_PROC || ' 00:00:00',
                                                    'DD-MM-YYYY hh24:mi:ss'
                                                 )
                                             AND  TO_DATE (
                                                     p_FEC_PROC
                                                     || ' 23:59:59',
                                                     'DD-MM-YYYY hh24:mi:ss'
                                                  )           -- Fecha del Dia
                     AND COD_EST_AOS IN (12, 15)           -- PAGADA Y ABONADA
            UNION
            SELECT   PKG_PAB_CONSTANTES.V_BIC_BANCO_SANTANDER || ';'
                        AS BANCO_ORIGEN,
                     RPAD (DET.COD_BCO_DTN, 11, ' ') || ';' AS BANCO_DESTINO,
                     DET.COD_DVI || ';' AS MONEDA,
                     LPAD (DET.IMP_OPE, 24, '0') || ';' AS MONTO,
                     RPAD (TO_CHAR (DET.FEC_ING_OPE, 'YYYYMMDD'), 10, ' ')
                     || ';'
                        AS FECHA_OPE,
                     RPAD (TO_CHAR (DET.FEC_VTA, 'YYYYMMDD'), 10, ' ') || ';'
                        AS FECHA_VTA,
                     RPAD (
                        NVL (REPLACE (DET.NUM_REF_SWF, CHR (10), ''), ' '),
                        16,
                        ' '
                     )
                     || ';'
                        AS NUM_REF,
                     LPAD (NVL (REPLACE (DET.NUM_FOL_OPE, CHR (10), ''), 0),
                           16,
                           ' ')
                     || ';'
                        AS NUM_OPE,
                     RPAD (
                        NVL (REPLACE (DET.NUM_REF_EXT, CHR (10), ''), ' '),
                        16,
                        ' '
                     )
                     || ';'
                        AS NUM_REF_RLD,
                     RPAD (
                        NVL (
                           REPLACE (REPLACE (DET.GLS_TRN, CHR (10), ''),
                                    CHR (13),
                                    ''),
                           ' '
                        ),
                        140,
                        ' '
                     )
                     || ';'
                        AS DETALLE,
                     LPAD (
                        NVL (
                           TRIM (DET.NUM_DOC_ODN) || TRIM (DET.COD_TPD_ODN),
                           '0'
                        ),
                        11,
                        '0'
                     )
                     || ';'
                        AS RUT_ORD,
                     RPAD (
                        NVL (TRIM (REPLACE (DET.NOM_ODN, CHR (10), '')), ' '),
                        70,
                        ' '
                     )
                     || ';'
                        AS NOM_ORD,
                     LPAD (NVL (DET.NUM_CTA_ODN, '0'), 34, '0') || ';'
                        AS CTA_ORD,
                     LPAD (
                        NVL (
                           TRIM (REPLACE (DET.NUM_DOC_BFC, '.', ''))
                           || TRIM (DET.COD_TPD_BFC),
                           '0'
                        ),
                        11,
                        '0'
                     )
                     || ';'
                        AS RUT_BEN,
                     RPAD (NVL (REPLACE (DET.NOM_BFC, CHR (10), ''), ' '),
                           70,
                           ' ')
                     || ';'
                        AS NOM_BEN,
                     LPAD (NVL (REPLACE (DET.NUM_CTA_CTE, CHR (10), ''), ''),
                           34,
                           '0')
                        AS CTA_BEN
              FROM   PABS_DT_DETLL_CARGO_ABONO_CAJA DET,
                     PABS_DT_RGTRO_OPRCN_CARGO_ABON ABN
             WHERE   DET.NUM_FOL_OPE = ABN.NUM_FOL_OPE
                     AND DET.FEC_ING_OPE BETWEEN TO_DATE (
                                                    p_FEC_PROC || ' 00:00:00',
                                                    'DD-MM-YYYY hh24:mi:ss'
                                                 )
                                             AND  TO_DATE (
                                                     p_FEC_PROC
                                                     || ' 23:59:59',
                                                     'DD-MM-YYYY hh24:mi:ss'
                                                  );
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            err_code := SQLCODE;
            err_msg := 'No se encuentran registros para fecha ' || p_FEC_PROC;
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            RAISE;
         WHEN OTHERS
         THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            p_s_mensaje := err_code || '-' || err_msg;
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
      END;
   END SP_GEN_ID_TRANSF;

   --***********************************************************************************************
   -- Funcion/Procedimiento: SP_GEN_VVV
   -- Objetivo: Genera interfaz de plazo.
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01
   -- Tablas Usadas: PABS_DT_DETLL_NOMNA
   -- Fecha: 06/06/2017
   -- Autor: Santander
   -- Input:
   -- p_FEC_ING : Fecha de proceso de generacion ID
   -- Output:
   -- p_OPERACIONES : Cursor de respuesta de datos
   -- p_CANTIDAD_OPE: cantidad de operaciones procesadas
   -- p_ERROR : indicador de ejecutor
   -- Input/Output:
   -- N/A.
   -- Retorno: N/A
   -- Observaciones: Fecha del dia
   --***********************************************************************************************
   PROCEDURE SP_GEN_VVV (p_FEC_ING        IN     VARCHAR2,
                         p_OPERACIONES       OUT SYS_REFCURSOR,
                         p_CANTIDAD_OPE      OUT NUMBER,
                         p_ERROR             OUT NUMBER)
   IS
      --Seteo de Variables
      v_NOM_SP            VARCHAR2 (30) := 'SP_GEN_VVV';
      v_fechaHoy          DATE := SYSDATE;
      V_COD_TPO_OPE_AOS   CHAR (8) := 'VVT';
   BEGIN
      PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (
         '0',
         v_NOM_PCK,
         'LA FECHA A PROCESAR ES :' || p_FEC_ING,
         v_NOM_SP
      );

      OPEN p_OPERACIONES FOR
         SELECT   TO_CHAR (FEC_VTA, 'YYYYMMDD') AS FEC_VTA,
                  LPAD (TRIM (NUM_OPE_SIS_ENT), 12, '0') AS NUM_VLE_VST,
                  '00001' AS NUM_SEC,
                  LPAD (IMP_OPE, 13, '0') || '00' AS IMP_OPE,
                  LPAD (COD_MT_SWF, 40, ' ') AS MSG_SWF,
                  LPAD ('0035', 4, ' ') AS ENTIDAD,
                  LPAD (COD_SUC, 4, '0') AS CENTRO,
                  LPAD ('OPERFIN', 8, ' ') AS USER_ID,
                  LPAD ('OPERFIN', 8, ' ') AS NETNAME
           FROM   PABS_DT_DETLL_OPRCN
          WHERE   FEC_VTA = TO_DATE (p_FEC_ING, 'DD/MM/YYYY')
                  AND COD_SIS_ENT IN
                           (PKG_PAB_CONSTANTES.V_COD_TIP_OB,
                            PKG_PAB_CONSTANTES.V_COD_TIP_CAJA)
                  AND COD_EST_AOS IN (12, 6, 18, 4)
         UNION
         SELECT   TO_CHAR (TRUNC (FEC_ISR_OPE_ITR), 'YYYYMMDD') AS FEC_VTA,
                  LPAD (TRIM (NUM_OPE_SIS_ENT_ITR), 12, '0') AS NUM_VLE_VST,
                  '00001' AS NUM_SEC,
                  LPAD (IMP_OPE_ITR, 13, '0') || '00' AS IMP_OPE,
                  LPAD ('103', 40, ' ') AS MSG_SWF,
                  LPAD ('0035', 4, ' ') AS ENTIDAD,
                  LPAD (COD_SUC_ITR, 4, '0') AS CENTRO,
                  LPAD ('OPERFIN', 8, ' ') AS USER_ID,
                  LPAD ('OPERFIN', 8, ' ') AS NETNAME
           FROM   PABS_DT_DETLL_OPRCN_INTER
          WHERE   TRUNC (FEC_ISR_OPE_ITR) = TO_DATE (p_FEC_ING, 'DD/MM/YYYY')
                  AND COD_SIS_ENT_ITR IN
                           (PKG_PAB_CONSTANTES.V_COD_TIP_OB,
                            PKG_PAB_CONSTANTES.V_COD_TIP_CAJA)
                  AND COD_EST_AOS IN (12, 6, 18, 4, 15);

      p_CANTIDAD_OPE := 0;                                   -- Se debe llenar
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg := 'No se encuentran registros para fecha ' || p_FEC_ING;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END SP_GEN_VVV;

   --***********************************************************************************************
   -- Funcion/Procedimiento: SP_GEN_INT_CTB
   -- Objetivo: Procedimiento que retorna cursor con operaciones de contabilidad
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_TIPO_FRMPG
   -- Fecha: 02-08-2019
   -- Autor: Santander CAFF
   -- Input:
   -- N/A
   -- Output:
   -- p_CURSOR -> RESULTADOS DE CONSULTA.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************
   PROCEDURE SP_GEN_INT_CTB (p_FEC IN DATE, p_CURSOR OUT SYS_REFCURSOR)
   IS
      v_NOM_SP     VARCHAR2 (30) := 'SP_PAB_INT_CTB';
      v_FECHA      DATE := p_FEC;         --TRUNC(SYSDATE)-2; -- FECHA DE AYER
      v_COD_PAIS   CHAR (2) := 'SN';


      FECHA_AUX DATE
            := (TRUNC (SYSDATE) - PKG_PAB_CONSTANTES.V_DIA_RES_HIS) ;
   -- BUSCAMOS EL REGISTRO.

   BEGIN
      IF (v_FECHA < FECHA_AUX)
      THEN
         OPEN p_CURSOR FOR
            SELECT   TO_NUMBER (CTB.NUM_CNP_CTA_CTB) AS CUENTA_CUADRE,
                     CTB.GLS_IFM_MVT_CTB AS NOMBRE_AREA,
                     RPAD (
                        REGEXP_REPLACE (TRIM (OPE.NUM_REF_EXT),
                                        '[^a-z_A-Z ]'),
                        16
                        - LENGTH(REGEXP_REPLACE (TRIM (OPE.NUM_REF_EXT),
                                                 '[^0-9]')),
                        ' '
                     )
                     || REGEXP_REPLACE (TRIM (OPE.NUM_REF_EXT), '[^0-9]')
                        AS REFERENCIA,
                     RPAD (
                        REGEXP_REPLACE (TRIM (OPE.NUM_REF_EXT),
                                        '[^a-z_A-Z ]'),
                        16
                        - LENGTH(REGEXP_REPLACE (TRIM (OPE.NUM_REF_EXT),
                                                 '[^0-9]')),
                        ' '
                     )
                     || REGEXP_REPLACE (TRIM (OPE.NUM_REF_EXT), '[^0-9]')
                        AS REFERENCIA_EXTERNA,
                     OPE.COD_MT_SWF AS OMT,
                     CASE
                        WHEN CTB.COD_DVI <> PKG_PAB_CONSTANTES.V_STR_DVI_CLP
                        THEN
                           TO_CHAR (CTB.IMP_DVI_DEB, 'fm9999999999990.00')
                        ELSE
                           TO_CHAR (CTB.IMP_MON_BSE_DEB,
                                    'fm9999999999990.00')
                     END
                        AS MONTO_DR,
                     CASE
                        WHEN CTB.COD_DVI <> PKG_PAB_CONSTANTES.V_STR_DVI_CLP
                        THEN
                           TO_CHAR (CTB.IMP_DVI_HAB, 'fm9999999999990.00')
                        ELSE
                           TO_CHAR (CTB.IMP_MON_BSE_HAB,
                                    'fm9999999999990.00')
                     END
                        AS MONTO_HB,
                     CTB.COD_DVI MONEDA,
                     OPE.COD_BCO_ORG AS CONTRAPARTE,
                     '' AS RUT,
                     OPE.NUM_CTA_CTE AS CTA_CTE,
                     '' AS NOMBRE,
                     '' AS OBS_OPC_SWF,
                     '' EMPRESA,
                     CTB.NUM_FOL_OPE NUM_OPE,
                     CTB.COD_SUC_DTN SUCURSAL
              FROM   DBO_PAB.PABS_DT_CNTBL_ALMOT CTB,
                     DBO_PAB.PABS_DT_DETLL_CARGO_ABONO_CAJA OPE
             WHERE       CTB.NUM_FOL_OPE = OPE.NUM_FOL_OPE
                     AND TRUNC (CTB.FEC_PCS_REG_CTB) = TRUNC (OPE.FEC_VTA)
                     AND TRUNC (OPE.FEC_VTA) = v_FECHA
                     AND OPE.COD_MT_SWF = PKG_PAB_CONSTANTES.V_COD_MT910
                     AND OPE.COD_DVI = PKG_PAB_CONSTANTES.V_STR_DVI_CLP
            UNION ALL
            SELECT   TO_NUMBER (CTB.NUM_CNP_CTA_CTB) AS CUENTA_CUADRE,
                     CTB.GLS_IFM_MVT_CTB AS NOMBRE_AREA,
                     RPAD (
                        REGEXP_REPLACE (TRIM (OPE.NUM_REF_SWF),
                                        '[^a-z_A-Z ]'),
                        16
                        - LENGTH(REGEXP_REPLACE (TRIM (OPE.NUM_REF_SWF),
                                                 '[^0-9]')),
                        ' '
                     )
                     || REGEXP_REPLACE (TRIM (OPE.NUM_REF_SWF), '[^0-9]')
                        AS REFERENCIA,
                     NVL (
                        RPAD (
                           REGEXP_REPLACE (TRIM (OPE.NUM_REF_EXT),
                                           '[^a-z_A-Z ]'),
                           16
                           - LENGTH(REGEXP_REPLACE (TRIM (OPE.NUM_REF_EXT),
                                                    '[^0-9]')),
                           ' '
                        )
                        || REGEXP_REPLACE (TRIM (OPE.NUM_REF_EXT), '[^0-9]'),
                        RPAD (
                           REGEXP_REPLACE (TRIM (OPE.NUM_REF_SWF),
                                           '[^a-z_A-Z ]'),
                           16
                           - LENGTH(REGEXP_REPLACE (TRIM (OPE.NUM_REF_SWF),
                                                    '[^0-9]')),
                           ' '
                        )
                        || REGEXP_REPLACE (TRIM (OPE.NUM_REF_SWF), '[^0-9]')
                     )
                        AS REFERENCIA_EXTERNA,
                     OPE.COD_MT_SWF AS OMT,
                     CASE
                        WHEN CTB.COD_DVI <> PKG_PAB_CONSTANTES.V_STR_DVI_CLP
                        THEN
                           TO_CHAR (CTB.IMP_DVI_DEB, 'fm9999999999990.00')
                        ELSE
                           TO_CHAR (CTB.IMP_MON_BSE_DEB,
                                    'fm9999999999990.00')
                     END
                        AS MONTO_DR,
                     CASE
                        WHEN CTB.COD_DVI <> PKG_PAB_CONSTANTES.V_STR_DVI_CLP
                        THEN
                           TO_CHAR (CTB.IMP_DVI_HAB, 'fm9999999999990.00')
                        ELSE
                           TO_CHAR (CTB.IMP_MON_BSE_HAB,
                                    'fm9999999999990.00')
                     END
                        AS MONTO_HB,
                     CTB.COD_DVI MONEDA,
                     CASE FLG_EGR_ING
                        WHEN 0 THEN OPE.COD_BCO_DTN
                        WHEN 1 THEN OPE.COD_BCO_ORG
                        ELSE 'SIN BANCO INFORMADO'
                     END
                        AS CONTRAPARTE,
                     TRIM (OPE.NUM_DOC_BFC) || ' - ' || OPE.COD_TPD_BFC
                        AS RUT,
                     OPE.NUM_CTA_BFC AS CTA_CTE,
                     OPE.NOM_BFC AS NOMBRE,
                     (SELECT   OBS_OPC_SWF
                        FROM   DBO_PAB.PABS_DT_OPRCN_OPCON_HTRCA
                       WHERE       NUM_FOL_OPE = OPE.NUM_FOL_OPE
                               AND FEC_ISR_OPE = OPE.FEC_ISR_OPE
                               AND FEC_CGA_HIS = OPE.FEC_CGA_HIS)
                        AS OBS_OPC_SWF,
                     NVL (
                        (SELECT   TCNIFENT
                           FROM   TCDT040
                          WHERE   TRIM (TGCDSWSA) = TRIM (OPE.COD_BCO_ORG)
                                  AND STBANCO <> 37),
                        'SIN RUT'
                     )
                        AS EMPRESA,
                     CTB.NUM_FOL_OPE NUM_OPE,
                     CTB.COD_SUC_DTN SUCURSAL
              FROM   DBO_PAB.PABS_DT_CNTBL_ALMOT CTB,
                     DBO_PAB.PABS_DT_DETLL_OPRCN_HTRCA OPE
             WHERE       CTB.NUM_FOL_OPE = OPE.NUM_FOL_OPE
                     AND TRUNC (CTB.FEC_PCS_REG_CTB) = TRUNC (OPE.FEC_VTA)
                     AND FLG_DEB_HAB = FLG_EGR_ING
                     AND TRUNC (OPE.FEC_VTA) = v_FECHA
                     AND OPE.COD_DVI = PKG_PAB_CONSTANTES.V_STR_DVI_CLP
                     AND OPE.COD_EST_AOS NOT IN (4)
                     AND NVL (TRIM (OPE.NUM_REF_SWF), '#') <> '#';
      ELSE
         BEGIN
            OPEN p_CURSOR FOR
               SELECT   TO_NUMBER (CTB.NUM_CNP_CTA_CTB) AS CUENTA_CUADRE,
                        CTB.GLS_IFM_MVT_CTB AS NOMBRE_AREA,
                        RPAD (
                           REGEXP_REPLACE (TRIM (OPE.NUM_REF_EXT),
                                           '[^a-z_A-Z ]'),
                           16
                           - LENGTH(REGEXP_REPLACE (TRIM (OPE.NUM_REF_EXT),
                                                    '[^0-9]')),
                           ' '
                        )
                        || REGEXP_REPLACE (TRIM (OPE.NUM_REF_EXT), '[^0-9]')
                           AS REFERENCIA,
                        RPAD (
                           REGEXP_REPLACE (TRIM (OPE.NUM_REF_EXT),
                                           '[^a-z_A-Z ]'),
                           16
                           - LENGTH(REGEXP_REPLACE (TRIM (OPE.NUM_REF_EXT),
                                                    '[^0-9]')),
                           ' '
                        )
                        || REGEXP_REPLACE (TRIM (OPE.NUM_REF_EXT), '[^0-9]')
                           AS REFERENCIA_EXTERNA,
                        OPE.COD_MT_SWF AS OMT,
                        CASE
                           WHEN CTB.COD_DVI <>
                                   PKG_PAB_CONSTANTES.V_STR_DVI_CLP
                           THEN
                              TO_CHAR (CTB.IMP_DVI_DEB, 'fm9999999999990.00')
                           ELSE
                              TO_CHAR (CTB.IMP_MON_BSE_DEB,
                                       'fm9999999999990.00')
                        END
                           AS MONTO_DR,
                        CASE
                           WHEN CTB.COD_DVI <>
                                   PKG_PAB_CONSTANTES.V_STR_DVI_CLP
                           THEN
                              TO_CHAR (CTB.IMP_DVI_HAB, 'fm9999999999990.00')
                           ELSE
                              TO_CHAR (CTB.IMP_MON_BSE_HAB,
                                       'fm9999999999990.00')
                        END
                           AS MONTO_HB,
                        CTB.COD_DVI MONEDA,
                        OPE.COD_BCO_ORG AS CONTRAPARTE,
                        '' AS RUT,
                        OPE.NUM_CTA_CTE AS CTA_CTE,
                        '' AS NOMBRE,
                        '' AS OBS_OPC_SWF,
                        '' EMPRESA,
                        CTB.NUM_FOL_OPE NUM_OPE,
                        CTB.COD_SUC_DTN SUCURSAL
                 FROM   DBO_PAB.PABS_DT_CNTBL_ALMOT CTB,
                        DBO_PAB.PABS_DT_DETLL_CARGO_ABONO_CAJA OPE
                WHERE       CTB.NUM_FOL_OPE = OPE.NUM_FOL_OPE
                        AND TRUNC (CTB.FEC_ISR_OPE) = TRUNC (OPE.FEC_VTA)
                        AND TRUNC (OPE.FEC_VTA) = v_FECHA
                        AND OPE.COD_MT_SWF = PKG_PAB_CONSTANTES.V_COD_MT910
                        AND OPE.COD_DVI = PKG_PAB_CONSTANTES.V_STR_DVI_CLP
               UNION ALL
               SELECT   TO_NUMBER (CTB.NUM_CNP_CTA_CTB) AS CUENTA_CUADRE,
                        CTB.GLS_IFM_MVT_CTB AS NOMBRE_AREA,
                        RPAD (
                           REGEXP_REPLACE (TRIM (OPE.NUM_REF_SWF),
                                           '[^a-z_A-Z ]'),
                           16
                           - LENGTH(REGEXP_REPLACE (TRIM (OPE.NUM_REF_SWF),
                                                    '[^0-9]')),
                           ' '
                        )
                        || REGEXP_REPLACE (TRIM (OPE.NUM_REF_SWF), '[^0-9]')
                           AS REFERENCIA,
                        NVL (
                           RPAD (
                              REGEXP_REPLACE (TRIM (OPE.NUM_REF_EXT),
                                              '[^a-z_A-Z ]'),
                              16
                              - LENGTH(REGEXP_REPLACE (
                                          TRIM (OPE.NUM_REF_EXT),
                                          '[^0-9]'
                                       )),
                              ' '
                           )
                           || REGEXP_REPLACE (TRIM (OPE.NUM_REF_EXT),
                                              '[^0-9]'),
                           RPAD (
                              REGEXP_REPLACE (TRIM (OPE.NUM_REF_SWF),
                                              '[^a-z_A-Z ]'),
                              16
                              - LENGTH(REGEXP_REPLACE (
                                          TRIM (OPE.NUM_REF_SWF),
                                          '[^0-9]'
                                       )),
                              ' '
                           )
                           || REGEXP_REPLACE (TRIM (OPE.NUM_REF_SWF),
                                              '[^0-9]')
                        )
                           AS REFERENCIA_EXTERNA,
                        OPE.COD_MT_SWF AS OMT,
                        CASE
                           WHEN CTB.COD_DVI <>
                                   PKG_PAB_CONSTANTES.V_STR_DVI_CLP
                           THEN
                              TO_CHAR (CTB.IMP_DVI_DEB, 'fm9999999999990.00')
                           ELSE
                              TO_CHAR (CTB.IMP_MON_BSE_DEB,
                                       'fm9999999999990.00')
                        END
                           AS MONTO_DR,
                        CASE
                           WHEN CTB.COD_DVI <>
                                   PKG_PAB_CONSTANTES.V_STR_DVI_CLP
                           THEN
                              TO_CHAR (CTB.IMP_DVI_HAB, 'fm9999999999990.00')
                           ELSE
                              TO_CHAR (CTB.IMP_MON_BSE_HAB,
                                       'fm9999999999990.00')
                        END
                           AS MONTO_HB,
                        CTB.COD_DVI MONEDA,
                        CASE FLG_EGR_ING
                           WHEN 0 THEN OPE.COD_BCO_DTN
                           WHEN 1 THEN OPE.COD_BCO_ORG
                           ELSE 'SIN BANCO INFORMADO'
                        END
                           AS CONTRAPARTE,
                        TRIM (OPE.NUM_DOC_BFC) || ' - ' || OPE.COD_TPD_BFC
                           AS RUT,
                        OPE.NUM_CTA_BFC AS CTA_CTE,
                        OPE.NOM_BFC AS NOMBRE,
                        NVL (OPEO.OBS_OPC_SWF, 'SIN INFORMACION')
                           AS OBS_OPC_SWF,
                        NVL (
                           (SELECT   TCNIFENT
                              FROM   TCDT040
                             WHERE   TRIM (TGCDSWSA) = TRIM (OPE.COD_BCO_ORG)
                                     AND STBANCO <> 37),
                           'SIN RUT'
                        )
                           AS EMPRESA,
                        CTB.NUM_FOL_OPE NUM_OPE,
                        CTB.COD_SUC_DTN SUCURSAL
                 FROM   DBO_PAB.PABS_DT_CNTBL_ALMOT CTB,
                        DBO_PAB.PABS_DT_OPRCN_INFCN_OPCON OPC,
                           DBO_PAB.PABS_DT_DETLL_OPRCN OPE
                        RIGHT OUTER JOIN
                           PABS_DT_OPRCN_INFCN_OPCON OPEO
                        ON OPE.NUM_FOL_OPE = OPEO.NUM_FOL_OPE
                WHERE       CTB.NUM_FOL_OPE = OPE.NUM_FOL_OPE
                        AND OPE.NUM_FOL_OPE = OPC.NUM_FOL_OPE
                        AND CTB.FEC_ISR_OPE = OPC.FEC_ISR_OPE
                        AND TRUNC (CTB.FEC_PCS_REG_CTB) = TRUNC (OPE.FEC_VTA)
                        AND FLG_DEB_HAB = FLG_EGR_ING
                        AND TRUNC (OPE.FEC_VTA) = v_FECHA
                        AND OPE.COD_DVI = PKG_PAB_CONSTANTES.V_STR_DVI_CLP
                        AND OPE.COD_EST_AOS NOT IN (4)
                        AND NVL (TRIM (OPE.NUM_REF_SWF), '#') <> '#';
         END;
      END IF;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END SP_GEN_INT_CTB;
END PKG_PAB_INTERFAZ;
/


CREATE SYNONYM USR_PAB.PKG_PAB_INTERFAZ FOR DBO_PAB.PKG_PAB_INTERFAZ
/


GRANT EXECUTE ON DBO_PAB.PKG_PAB_INTERFAZ TO R_APP_PAB
/

GRANT EXECUTE ON DBO_PAB.PKG_PAB_INTERFAZ TO USR_PAB
/

