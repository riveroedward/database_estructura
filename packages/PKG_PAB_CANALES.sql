CREATE OR REPLACE /* Formatted on 18-08-2020 17:45:32 (QP5 v5.115.810.9015) */
PACKAGE DBO_PAB.PKG_PAB_CANALES
AS
   /**  Package el cual contrendra todas las funciones y procedimientos relacionados con los perfiles
   -- @Santander */

   /***************************VARIABLE GLOBALES************************************/
   v_NOM_PCK       VARCHAR2 (30) := 'PKG_PAB_CANALES';
   v_NOM_SP        VARCHAR2 (30);
   ERR_CODE        NUMBER := 0;
   ERR_MSG         VARCHAR2 (300);
   v_DES           VARCHAR2 (300);
   v_DES_BIT       VARCHAR2 (100);
   v_COD_USU       CHAR (11);
   p_s_mensaje     VARCHAR2 (400);
   v_BANCO         VARCHAR2 (9) := '970150005';
   v_largoCuenta   NUMBER := 14;
   v_RESULT        NUMBER;
   v_dia_ini       DATE := TRUNC (SYSDATE) + INTERVAL '1' MINUTE;
   v_dia_fin DATE
         := TRUNC (SYSDATE) + INTERVAL '23' HOUR + INTERVAL '59' MINUTE ;

   /**************************FIN VARIABLE GLOBALES********************************/

   --***********************************************************************************************
   -- Funcion/Procedimiento: SP_EST_OPE_AOS
   -- Objetivo: Procedimiento que retorna informacion de una operacion DCV en Opreaciones
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: SP_EST_OPE_AOS
   -- Fecha: 25/07/17
   -- Autor: Santander
   -- Input:
   -- p_CVL_NEG -> Clave de negocios informada para DCV
   -- p_MOV_CAN -> Movimiento de operaci�n
   -- p_IMP_MTO -> Monto de la operaci�n
   -- p_NUM_OPE -> Numero de Operacion
   -- Output:
   -- p_EST_PROC      -> Estado de proceso
   -- p_NUM_RESP_PROC -> Codigo Respuesta si existe del proceso
   -- p_NUM_DOC_BEN   -> Mensaje de error / Confirmacion de datos
   -- p_NUM_RESP_DOC  -> Codigo Respuesta si existe el Numero de Operacion
   -- p_CTA_BEN       -> Mensaje de error / Confirmacion de datos
   -- p_NUM_RESP_CTA  -> Codigo Respuesta si existe el Numero de Operacion
   -- p_ERR_CODE      -> Codigo de error de ejecucion (retorno para TIBCO)
   -- p_ERR_MSG       -> mensaje de error de ejecucion (retorno para TIBCO)
   -- p_ERROR         -> codigo de error de ejecucion de PL (para log)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de utimos cambios>
   --***********************************************************************************************
   PROCEDURE SP_EST_OPE_AOS (p_CVL_NEG         IN     VARCHAR2,
                             p_MOV_CAN         IN     CHAR,
                             p_IMP_MTO         IN     NUMBER,
                             p_NUM_OPE         IN     VARCHAR2,
                             p_EST_PROC           OUT VARCHAR2,
                             p_NUM_RESP_PROC      OUT NUMBER,
                             p_NUM_DOC_BEN        OUT VARCHAR2,
                             p_NUM_RESP_DOC       OUT NUMBER,
                             p_CTA_BEN            OUT VARCHAR2,
                             p_NUM_RESP_CTA       OUT NUMBER,
                             p_ERR_CODE           OUT VARCHAR2,
                             p_ERR_MSG            OUT VARCHAR2,
                             p_ERROR              OUT NUMBER);

   --***********************************************************************************************
   -- Funcion/Procedimiento: SP_ING_DVP_AEN
   -- Objetivo: Procedimiento que ingresa operaciones provenientes de CUSTODIA (CARMEN)
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: SP_ING_DVP_AEN
   -- Fecha: 25/07/17
   -- Autor: Santander
   -- Input:
   -- p_NUM_OPE        -> numero de Operacion
   -- p_NUM_DOC_BNF    -> numero de rut beneficiario
   -- p_NOM_BNF        -> nombre beneficiario
   -- p_CTA_BNF        -> cuenta beneficiario
   -- p_NUM_DOC_ORD    -> numero de rut ordenante
   -- p_NOM_ORD        -> nombre ordenante
   -- p_CTA_ORD        -> cuenta ordenante
   -- p_FEC_PAG        -> fecha de pago
   -- p_FEC_VAL        -> fecha de valuta
   -- p_MTO_EGR        -> monto egreso
   -- p_MTO_ING        -> monto ingreso
   -- p_COD_DVI        -> codigo divisa
   -- p_COD_SIS_ORI    -> codifo sistema origen
   -- p_DOC_EMP        -> rut empresa
   -- p_REF_EXT        -> referencia externa
   -- p_COD_REF        -> codigo referencia
   -- p_RUT_BCO_DTN    -> rut banco destino
   -- p_COD_SWF_DTN    -> codigo swift destino
   -- p_NUM_SUC        -> numero de sucursar
   -- p_GLOSA          -> glosa operacion
   -- p_CANAL_ENT      -> canal de entrada
   -- p_FORMA_PAGO     -> forma de pago operacion
   -- p_ClaveNeg01     -> clave de negocio (1)
   -- p_ClaveNeg02     -> clave de negocio (2)
   -- p_ClaveNeg03     -> clave de negocio (3)
   -- p_ClaveNeg04     -> clave de negocio (4)
   -- p_ClaveNeg05     -> clave de negocio (5)
   -- p_ClaveNeg06     -> clave de negocio (6)
   -- p_ClaveNeg07     -> clave de negocio (7)
   -- p_ClaveNeg08     -> clave de negocio (8)
   -- p_ClaveNeg09     -> clave de negocio (9)
   -- p_ClaveNeg10     -> clave de negocio (10)
   -- p_CtaDcv_Ben     -> cuenta dvp beneficiario
   -- p_CtaDcv_Ord     -> cuenta dvp ordenante
   -- p_COD_USR        -> codigo usuario
   -- p_Num_SUC_Dest   -> sucursal destino
   -- p_Cod_TransPago  -> codigo transferencia de pago
   -- p_Msg_Rel        -> mensaje relacionado
   -- Output:
   -- p_ERR_CODE      -> codigo de error de ejecucion (retorno para TIBCO)
   -- p_ERR_MSG       -> mensaje de error de ejecucion (retorno para TIBCO)
   -- p_ERROR         -> codigo de error de ejecucion de PL (para log)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de utimos cambios>
   --***********************************************************************************************
   PROCEDURE SP_ING_DVP_AEN (p_NUM_OPE         IN     VARCHAR2,
                             p_NUM_DOC_BNF     IN     VARCHAR2,
                             p_NOM_BNF         IN     VARCHAR2,
                             p_CTA_BNF         IN     VARCHAR2,
                             p_NUM_DOC_ORD     IN     VARCHAR2,
                             p_NOM_ORD         IN     VARCHAR2,
                             p_CTA_ORD         IN     VARCHAR2,
                             p_FEC_PAG         IN     VARCHAR2,
                             p_FEC_VAL         IN     VARCHAR2,
                             p_MTO_EGR         IN     CHAR,
                             p_MTO_ING         IN     CHAR,
                             p_COD_DVI         IN     CHAR,
                             p_COD_SIS_ORI     IN     CHAR,
                             p_DOC_EMP         IN     VARCHAR2,
                             p_REF_EXT         IN     VARCHAR2,
                             p_COD_REF         IN     CHAR,
                             p_RUT_BCO_DTN     IN     VARCHAR2,
                             p_COD_SWF_DTN     IN     VARCHAR2,
                             p_NUM_SUC         IN     VARCHAR2,
                             p_GLOSA           IN     VARCHAR2,
                             p_CANAL_ENT       IN     VARCHAR2,
                             p_FORMA_PAGO      IN     VARCHAR2,
                             p_ClaveNeg01      IN     CHAR,
                             p_ClaveNeg02      IN     CHAR,
                             p_ClaveNeg03      IN     CHAR,
                             p_ClaveNeg04      IN     CHAR,
                             p_ClaveNeg05      IN     CHAR,
                             p_ClaveNeg06      IN     CHAR,
                             p_ClaveNeg07      IN     CHAR,
                             p_ClaveNeg08      IN     CHAR,
                             p_ClaveNeg09      IN     CHAR,
                             p_ClaveNeg10      IN     CHAR,
                             p_CtaDcv_Ben      IN     VARCHAR2,
                             p_CtaDcv_Ord      IN     VARCHAR2,
                             p_COD_USR         IN     VARCHAR2,
                             p_Num_SUC_Dest    IN     VARCHAR2,
                             p_Cod_TransPago   IN     CHAR,
                             p_Msg_Rel         IN     CHAR,
                             p_ERR_CODE           OUT VARCHAR2,
                             p_ERR_MSG            OUT VARCHAR2,
                             p_ERROR              OUT NUMBER);

   --***********************************************************************************************
   -- Funcion/Procedimiento: SP_ACT_EST_OPE_DVP
   -- Objetivo: Procedimiento que actualiza en Altos Montos los estados de las operaciones DVP
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: SP_ACT_EST_OPE_DVP
   -- Fecha: 25/07/17
   -- Autor: Santander
   -- Input:
   -- p_NUM_OPE    --> Numero de operacion Origen
   -- p_FEC_ING    --> Fecha de ingreso operacion
   -- p_IMP_MTO    --> Monto de la operacion
   -- p_CLV_NGC    --> Clave de negocio
   -- p_REF_EXT    --> Referencia Externa
   -- p_COD_EST    --> Estado a actualizar
   -- Output:
   -- p_ERR_CODE      -> codigo de error de ejecucion (retorno para TIBCO)
   -- p_ERR_MSG       -> mensaje de error de ejecucion (retorno para TIBCO)
   -- p_ERROR         -> codigo de error de ejecucion de PL (para log)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de utimos cambios>
   PROCEDURE SP_ACT_EST_OPE_DVP (p_NUM_OPE    IN     VARCHAR2,
                                 p_FEC_ING    IN     VARCHAR2,
                                 p_IMP_MTO    IN     VARCHAR2,
                                 p_CLV_NGC    IN     VARCHAR2,
                                 p_REF_EXT    IN     VARCHAR2,
                                 p_COD_EST    IN     VARCHAR2,
                                 p_ERR_CODE      OUT VARCHAR2,
                                 p_ERR_MSG       OUT VARCHAR2,
                                 p_ERROR         OUT NUMBER);

   --***********************************************************************************************
   -- Funcion/Procedimiento: SP_BUS_OPE_DVP
   -- Objetivo: Procedimiento que busca en Altos Montos las operaciones DVP
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: SP_ACT_EST_OPE_DVP
   -- Fecha: 25/07/17
   -- Autor: Santander
   -- Input:
   -- p_FEC_ING        -> Fecha de ingreso de la operacion
   -- p_NUM_DOC_CLI    -> numero de documento del cliente DVP
   -- p_CANT_REG       -> Cantidad de registros por pagina
   -- p_IND_RECALL     -> indicador si es rellamada.
   -- Output:
   -- p_OPERACIONES    -> Cursor con las operaciones encontradas
   -- p_id_recall      -> indicar si existen mas operaciones
   -- p_num_paginas    -> indicador de paginas
   -- p_ERR_CODE       -> codigo de error de ejecucion (retorno para TIBCO)
   -- p_ERR_MSG        -> mensaje de error de ejecucion (retorno para TIBCO)
   -- p_ERROR          -> codigo de error de ejecucion de PL (para log)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de utimos cambios>
   --***********************************************************************************************
   PROCEDURE SP_BUS_OPE_DVP (p_FEC_ING       IN     VARCHAR2,
                             p_NUM_DOC_CLI   IN     VARCHAR2,
                             p_CANT_REG      IN     NUMBER,
                             p_IND_RECALL    IN     NUMBER,
                             p_OPERACIONES      OUT SYS_REFCURSOR,
                             p_id_recall        OUT NUMBER,
                             p_num_paginas      OUT NUMBER,
                             p_ERR_CODE         OUT VARCHAR2,
                             p_ERR_MSG          OUT VARCHAR2,
                             p_ERROR            OUT NUMBER);

   --***********************************************************************************************
   -- Funcion/Procedimiento: SP_VAL_DUP_DVP
   -- Objetivo: Procedimiento que busca operaciones duplicadas en Altos Montos
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: SP_ACT_EST_OPE_DVP
   -- Fecha: 25/07/17
   -- Autor: Santander
   -- Input:
   -- p_FEC_ING        -> Fecha de ingreso de la operacion
   -- p_IMP_MTO        -> Monto de la operacion
   -- p_CLV_NEG        -> Clave de negocio DVP
   -- Output:
   -- p_ERR_CODE       -> codigo de error de ejecucion (retorno para TIBCO)
   -- p_ERR_MSG        -> mensaje de error de ejecucion (retorno para TIBCO)
   -- p_ERROR          -> codigo de error de ejecucion de PL (para log)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de utimos cambios>
   --***********************************************************************************************
   PROCEDURE SP_VAL_DUP_DVP (p_FEC_ING    IN     VARCHAR2,
                             p_IMP_MTO    IN     NUMBER,
                             p_CLV_NEG    IN     VARCHAR2,
                             p_ERR_CODE      OUT VARCHAR2,
                             p_ERR_MSG       OUT VARCHAR2,
                             p_ERROR         OUT NUMBER);

   --***********************************************************************************************
   -- Funcion/Procedimiento: SP_CON_OFF_BKN
   -- Objetivo: Procedimiento que consulta operaciones provenientes de OfficeBanking
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: SP_ACT_EST_OPE_DVP
   -- Fecha: 25/07/17
   -- Autor: Santander
   -- Input:
   -- p_COD_USR        -> codigo usuario
   -- p_NUM_DOC_USU    -> rut empresa
   -- p_NUM_DOC_CLI    -> rut cliente
   -- p_FEC_DSD        -> fecha desde
   -- p_FEC_HST        -> fecha hasta
   -- p_COD_SIS_ENT    -> codigo sistema de entrada
   -- p_NUM_CTA_CTE    -> numero de cuenta
   -- p_EST_AOS        -> estado operacion
   -- p_TIP_TRS        -> tipo de transaccion
   -- p_TIP_FLJ        -> tipo de flujo
   -- p_REG_PAG_SAL    -> numero de paginas de salida
   -- p_NUM_REG_PAG_SAL-> registros por pagina
   -- p_NUM_PAG_SAL    -> numero de paginas de salida
   -- Output:
   -- p_DET_OPE        -> Cursor con las operaciones encontradas
   -- p_REG_PAG        -> indicar si existen mas operaciones
   -- p_NUM_PAG        -> indicador de paginas
   -- p_ERR_CODE       -> codigo de error de ejecucion (retorno para TIBCO)
   -- p_ERR_MSG        -> mensaje de error de ejecucion (retorno para TIBCO)
   -- p_ERROR          -> codigo de error de ejecucion de PL (para log)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de utimos cambios>
   --***********************************************************************************************
   PROCEDURE SP_CON_OFF_BKN (p_COD_USR           IN     VARCHAR2,
                             p_NUM_DOC_USU       IN     VARCHAR2,
                             p_NUM_DOC_CLI       IN     VARCHAR2,
                             p_FEC_DSD           IN     VARCHAR2,
                             p_FEC_HST           IN     VARCHAR2,
                             p_COD_SIS_ENT       IN     VARCHAR2,
                             p_NUM_CTA_CTE       IN     VARCHAR2,
                             p_EST_AOS           IN     NUMERIC,
                             p_TIP_TRS           IN     VARCHAR2,
                             p_TIP_FLJ           IN     VARCHAR2,
                             p_REG_PAG_SAL       IN     NUMERIC,
                             p_NUM_REG_PAG_SAL   IN     NUMERIC,
                             p_NUM_PAG_SAL       IN     NUMERIC,
                             p_DET_OPE              OUT SYS_REFCURSOR,
                             p_REG_PAG              OUT NUMERIC,
                             p_NUM_PAG              OUT NUMERIC,
                             p_ERR_CODE             OUT VARCHAR2,
                             p_ERR_MSG              OUT VARCHAR2,
                             p_ERROR                OUT NUMERIC);

   --***********************************************************************************************
   -- Funcion/Procedimiento: SP_ING_PAG_DVP
   -- Objetivo: Procedimiento que ingresa operaciones tipo DVP
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: SP_ACT_EST_OPE_DVP
   -- Fecha: 25/07/17
   -- Autor: Santander
   -- Input:
   -- p_NUM_DOC_BCO    -> Numero documento empresa
   -- p_COD_DVI        -> Codigo divisa
   -- p_IMP_MTO        -> Monto operacion
   -- p_FEC_ING        -> fecha de ingreso
   -- p_FEC_VTA        -> fecha de valuta
   -- p_NUM_DOC_BFN    -> numero documento beneficiario
   -- p_NOM_BFN        -> nombre beneficiario
   -- p_CTA_BFC        -> cuenta beneficiario
   -- p_NUM_DOC_ORD    -> numero documento ordenante
   -- p_NOM_ORD        -> nombre ordenante
   -- p_CTA_ORD        -> cuenta ordenante
   -- p_ID_DCVORO      -> Id DCV
   -- p_COD_SEMAT      -> codigo SEMA
   -- p_SUC_ORI        -> Sucursal origen
   -- p_COD_TSN        -> Codigo transaccion
   -- p_NUM_OPE        -> numero operacion
   -- p_REF_EXT        -> referencia externa
   -- p_GLS_ADC        -> glosa adicional
   -- p_CLV_NEG_01     -> Clave de negicio 1
   -- p_CLV_NEG_02     -> Clave de negicio 2
   -- p_CLV_NEG_03     -> Clave de negicio 3
   -- p_CLV_NEG_04     -> Clave de negicio 4
   -- p_CLV_NEG_05     -> Clave de negicio 5
   -- p_CLV_NEG_06     -> Clave de negicio 6
   -- p_CLV_NEG_07     -> Clave de negicio 7
   -- p_CLV_NEG_08     -> Clave de negicio 8
   -- p_CLV_NEG_09     -> Clave de negicio 9
   -- p_CLV_NEG_10     -> Clave de negicio 10
   -- Output:
   -- p_ERR_CODE       -> codigo de error de ejecucion (retorno para TIBCO)
   -- p_ERR_MSG        -> mensaje de error de ejecucion (retorno para TIBCO)
   -- p_ERROR          -> codigo de error de ejecucion de PL (para log)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de utimos cambios>
   --***********************************************************************************************
   PROCEDURE SP_ING_PAG_DVP (p_NUM_DOC_BCO   IN     VARCHAR2,
                             p_COD_DVI       IN     VARCHAR2,
                             p_IMP_MTO       IN     NUMERIC,
                             p_FEC_ING       IN     VARCHAR2,
                             p_FEC_VTA       IN     VARCHAR2,
                             p_NUM_DOC_BFN   IN     VARCHAR2,
                             p_NOM_BFN       IN     VARCHAR2,
                             p_CTA_BFC       IN     VARCHAR2,
                             p_NUM_DOC_ORD   IN     VARCHAR2,
                             p_NOM_ORD       IN     VARCHAR2,
                             p_CTA_ORD       IN     VARCHAR2,
                             p_ID_DCVORO     IN     VARCHAR2,
                             p_COD_SEMAT     IN     VARCHAR2,
                             p_SUC_ORI       IN     VARCHAR2,
                             p_COD_TSN       IN     VARCHAR2,
                             p_NUM_OPE       IN     VARCHAR2,
                             p_REF_EXT       IN     VARCHAR2,
                             p_GLS_ADC       IN     VARCHAR2,
                             p_CLV_NEG_01    IN     VARCHAR2,
                             p_CLV_NEG_02    IN     VARCHAR2,
                             p_CLV_NEG_03    IN     VARCHAR2,
                             p_CLV_NEG_04    IN     VARCHAR2,
                             p_CLV_NEG_05    IN     VARCHAR2,
                             p_CLV_NEG_06    IN     VARCHAR2,
                             p_CLV_NEG_07    IN     VARCHAR2,
                             p_CLV_NEG_08    IN     VARCHAR2,
                             p_CLV_NEG_09    IN     VARCHAR2,
                             p_CLV_NEG_10    IN     VARCHAR2,
                             p_ERR_CODE         OUT VARCHAR2,
                             p_ERR_MSG          OUT VARCHAR2,
                             p_ERROR            OUT NUMBER);

   --***********************************************************************************************
   -- Funcion/Procedimiento: SP_ING_PAG
   -- Objetivo: Procedimiento que ingresa operaciones tipo LBTR
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: SP_ACT_EST_OPE_DVP
   -- Fecha: 25/07/17
   -- Autor: Santander
   -- Input:
   -- p_NUM_DOC_BCO    -> Numero documento empresa
   -- p_COD_DVI        -> Codigo divisa
   -- p_IMP_MTO        -> Monto operacion
   -- p_FEC_ING        -> fecha de ingreso
   -- p_FEC_VTA        -> fecha de valuta
   -- p_NUM_DOC_BFN    -> numero documento beneficiario
   -- p_NOM_BFN        -> nombre beneficiario
   -- p_CTA_BFC        -> cuenta beneficiario
   -- p_NUM_DOC_ORD    -> numero documento ordenante
   -- p_NOM_ORD        -> nombre ordenante
   -- p_CTA_ORD        -> cuenta ordenante
   -- p_COD_SIS_ORI    -> Codigo sistema origen
   -- p_SUC_ORI        -> Sucursal origen
   -- p_COD_TR_PAGO    -> Codigo transaccion
   -- p_NUM_OPE        -> numero operacion
   -- p_COD_RFR        -> refenrencia externa
   -- p_REF_EXT        -> Codifo referencia
   -- p_CAN_PAG        -> Cantidad de paginas
   -- p_CTA_CTB        -> referencia externa
   -- p_GLS_ADC        -> glosa adicional
   -- Output:
   -- p_ERR_CODE       -> codigo de error de ejecucion (retorno para TIBCO)
   -- p_ERR_MSG        -> mensaje de error de ejecucion (retorno para TIBCO)
   -- p_ERROR          -> codigo de error de ejecucion de PL (para log)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de utimos cambios>
   --***********************************************************************************************
   PROCEDURE SP_ING_PAG (p_NUM_DCO_BCO   IN     VARCHAR2,
                         p_COD_DVI       IN     VARCHAR2,
                         p_IMP_MTO       IN     NUMERIC,
                         p_FEC_ING       IN     VARCHAR2,
                         p_FEC_VTA       IN     VARCHAR2,
                         p_NUM_DCO_BNF   IN     VARCHAR2,
                         p_NOM_BNF       IN     VARCHAR2,
                         p_CTA_BNF       IN     VARCHAR2,
                         p_NUM_DCO_ODN   IN     VARCHAR2,
                         p_NOM_ODN       IN     VARCHAR2,
                         p_CTA_ODN       IN     VARCHAR2,
                         p_COD_SIS_ORI   IN     VARCHAR2,
                         p_SUC_ORI       IN     NUMERIC,
                         p_COD_TR_PAGO   IN     VARCHAR2,
                         p_NUM_OPE       IN     VARCHAR2,
                         p_COD_RFR       IN     VARCHAR2,
                         p_REF_EXT       IN     VARCHAR2,
                         p_CAN_PAG       IN     NUMERIC,
                         p_CTA_CTB       IN     VARCHAR2,
                         p_GLS_ADIC      IN     VARCHAR2,
                         p_ERR_CODE         OUT VARCHAR2,
                         p_ERR_MSG          OUT VARCHAR2,
                         p_ERROR            OUT NUMBER);

   --***********************************************************************************************
   -- Funcion/Procedimiento: SP_ING_OPE_PLZ
   -- Objetivo: Procedimiento que ingresa numero de vale vista relacionado a operacion de officebanking
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: SP_ACT_EST_OPE_DVP
   -- Fecha: 25/07/17
   -- Autor: Santander
   -- Input:
   -- p_NUM_OPE        -> numero de OfficeBankin
   -- p_NUM_OPE_ORI    -> numero VV plazo
   -- p_COD_DVI        -> codigo divisa
   -- p_IMP_MTO        -> monto operacion
   -- p_FEC_ING        -> fecha de ingreso
   -- p_CLV_NEG        -> clave de negocio
   -- Output:
   -- p_ERR_CODE       -> codigo de error de ejecucion (retorno para TIBCO)
   -- p_ERR_MSG        -> mensaje de error de ejecucion (retorno para TIBCO)
   -- p_ERROR          -> codigo de error de ejecucion de PL (para log)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de utimos cambios>
   --***********************************************************************************************
   PROCEDURE SP_ING_OPE_PLZ (p_NUM_OPE       IN     NUMBER,
                             p_NUM_OPE_ORI   IN     NUMBER,
                             p_COD_DVI       IN     CHAR,
                             p_IMP_MTO       IN     NUMBER,
                             p_FEC_ING       IN     VARCHAR2,
                             p_CLV_NEG       IN     VARCHAR2,
                             p_ERR_CODE         OUT VARCHAR2,
                             p_ERR_MSG          OUT VARCHAR2,
                             p_ERROR            OUT NUMBER);

   --***********************************************************************************************
   -- Funcion/Procedimiento: SP_INS_LBTR
   -- Objetivo: Procedimiento que ingresa operacione desde sistema Custodia
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: SP_ACT_EST_OPE_DVP
   -- Fecha: 25/07/17
   -- Autor: Santander
   -- Input:
   -- p_NUM_DOC_BCO_DTN    -> Numero de documento destino
   -- p_COD_DVI            -> codigo divisa
   -- p_IMP_MTO            -> monto
   -- p_FEC_ING_PAG        -> fecha ingreso operacion
   -- p_FEC_VAL            -> fecha de valuta
   -- p_NUM_DOC_BNF        -> numero de documento beneficiario
   -- p_NOM_BNF            -> nombre beneficiario
   -- p_CTA_BNF            -> cuenta beneficiario
   -- p_NUM_DOC_ORD        -> numero documento ordenante
   -- p_NOM_ORD            -> nombre ordenante
   -- p_CTA_ORD            -> cuenta ordenante
   -- p_COD_ORI            -> codigo origen
   -- p_SUC_ORI            -> sucursal origen
   -- p_COD_TR_PA          -> codigo transaccion
   -- p_NUM_OPE            -> numero operacion origen
   -- p_COD_REF            -> codigo referencia
   -- p_REF_EXT            -> referencia externa
   -- p_CAN_CAN            -> cantidad de operaciones
   -- p_CTA_CNT            -> cuena contable
   -- p_COD_PAR            -> Codigo de pago
   -- p_GLO_GLO            -> Glosa operacion
   -- p_soma_01            -> clave negocio 1
   -- p_soma_02            -> clave negocio 2
   -- p_soma_03            -> clave negocio 3
   -- p_soma_04            -> clave negocio 4
   -- p_soma_05            -> clave negocio 5
   -- p_soma_06            -> clave negocio 6
   -- p_soma_07            -> clave negocio 7
   -- p_soma_08            -> clave negocio 8
   -- p_soma_09            -> clave negocio 9
   -- p_soma_10            -> clave negocio 10
   -- Output:
   -- p_ERR_CODE       -> codigo de error de ejecucion (retorno para TIBCO)
   -- p_ERR_MSG        -> mensaje de error de ejecucion (retorno para TIBCO)
   -- p_ERROR          -> codigo de error de ejecucion de PL (para log)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de utimos cambios>
   --***********************************************************************************************
   PROCEDURE SP_INS_LBTR (p_NUM_DOC_BCO_DTN   IN     VARCHAR2,
                          p_COD_DVI           IN     CHAR,
                          p_IMP_MTO           IN     NUMERIC,
                          p_FEC_ING_PAG       IN     NUMERIC,
                          p_FEC_VAL           IN     VARCHAR2,
                          p_NUM_DOC_BNF       IN     VARCHAR2,
                          p_NOM_BNF           IN     VARCHAR2,
                          p_CTA_BNF           IN     VARCHAR2,
                          p_NUM_DOC_ORD       IN     VARCHAR2,
                          p_NOM_ORD           IN     VARCHAR2,
                          p_CTA_ORD           IN     VARCHAR2,
                          p_COD_ORI           IN     VARCHAR2,
                          p_SUC_ORI           IN     VARCHAR2,
                          p_COD_TR_PA         IN     VARCHAR2,
                          p_NUM_OPE           IN     CHAR,
                          p_COD_REF           IN     VARCHAR2,
                          p_REF_EXT           IN     VARCHAR2,
                          p_CAN_CAN           IN     VARCHAR2,
                          p_CTA_CNT           IN     CHAR,
                          p_COD_PAR           IN     VARCHAR2,
                          p_GLO_GLO           IN     VARCHAR2,
                          p_soma_01           IN     VARCHAR2,
                          p_soma_02           IN     VARCHAR2,
                          p_soma_03           IN     VARCHAR2,
                          p_soma_04           IN     VARCHAR2,
                          p_soma_05           IN     VARCHAR2,
                          p_soma_06           IN     VARCHAR2,
                          p_soma_07           IN     VARCHAR2,
                          p_soma_08           IN     VARCHAR2,
                          p_soma_09           IN     VARCHAR2,
                          p_soma_10           IN     VARCHAR2,
                          p_ERR_CODE             OUT VARCHAR2,
                          p_ERR_MSG              OUT VARCHAR2,
                          p_ERROR                OUT NUMBER);

   --***********************************************************************************************
   -- Funcion/Procedimiento: SP_OBT_MSJ_SWF_HST
   -- Objetivo: Procedimiento que ingresa operacione desde sistema Custodia
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: SP_ACT_EST_OPE_DVP
   -- Fecha: 25/07/17
   -- Autor: Santander
   -- Input:
   --.p_NUM_OPE        -> Numero de operacion
   -- Output:
   -- p_NUM_IDF_GTR_DOC-> numero identificatorio gestor documental
   -- p_ERR_CODE       -> codigo de error de ejecucion (retorno para TIBCO)
   -- p_ERR_MSG        -> mensaje de error de ejecucion (retorno para TIBCO)
   -- p_ERROR          -> codigo de error de ejecucion de PL (para log)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de utimos cambios>
   --***********************************************************************************************
   PROCEDURE SP_OBT_MSJ_SWF_HST (p_NUM_OPE           IN     VARCHAR2,
                                 p_NUM_IDF_GTR_DOC      OUT VARCHAR2,
                                 p_ERR_CODE             OUT VARCHAR2,
                                 p_ERR_MSG              OUT VARCHAR2,
                                 p_ERROR                OUT NUMBER);


   --
   /************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_BUS_FLG_MT_TPO
   -- Objetivo: Procedimiento almacenado que Flag segun MT y tipo de OperaCion
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_BIBLI_CAMPO_OBLTO_MT
   --
   -- Fecha: 16/06/17
   -- Autor: Santander
   -- Input: p_COD_MT_SWF      Tipo MT Swift
   --        p_COD_TPO_OPE_AOS Tipo de Operacion
   --         p_FLG_MRD_UTZ_SWF     Tipo Mercado
   -- Output:
   --        p_CURSOR : SYS_REFCURSOR, resultados de consulta.
   --        p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE Sp_PAB_BUS_FLG_MT_TPO (p_COD_MT_SWF        IN     NUMBER,
                                    p_COD_TPO_OPE_AOS   IN     CHAR,
                                    p_FLG_MRD_UTZ_SWF   IN     NUMBER,
                                    p_CURSOR               OUT SYS_REFCURSOR,
                                    p_ERROR                OUT NUMBER);

   --

   /************************************************************************************************
   -- Funcion/Procedimiento: Sp_INS_OPE_DCV_AUT
   -- Objetivo: Procedimiento que ingresa operaciones del DCV automaticos
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_BIBLI_CAMPO_OBLTO_MT
   --
   -- Fecha: 16/06/17
   -- Autor: SII GROUP
   -- Input:
   -- p_NUM_IDF_SWF        -> Numero identificador de swift
   -- p_NUM_SSN_SWF        -> Numero de sesion de swift
   -- p_FEC_ISR_MSJ_SWF    -> numero identificador del mensaje
   -- p_NUM_REF_IDE_DCV    -> numero referencia DCV
   -- p_NUM_MSJ_SWF_RLD    -> numero referencia relacionada
   -- p_FEC_RCP_MSJ        -> numero recepcion mensaje
   -- p_COD_DVI            -> codigo divisa
   -- p_IMP_MTO            -> monto operacion
   -- p_NUM_CTA_ODN        -> numero cuenta ordenante
   -- p_NUM_DOC_ODN        -> numero documento ordenante
   -- p_NUM_CTA_DCV_ODN    -> numero de cuenta dcv ordenante
   -- p_NOM_ODN            -> nombre ordenante
   -- p_COD_BCO_DTN        -> codigo banco destino (BIC)
   -- p_NUM_CTA_BFC        -> numero cuenta beneficiario
   -- p_NUM_DOC_BFC        -> numero documento beneficiario
   -- p_NUM_CTA_DCV_BFC    -> numero cuenta DCV beneficiario
   -- p_NOM_BFC            -> nombre beneficiario
   -- p_NUM_CLV_NGC        -> clave de negocio
   -- p_COD_EST_OFB        -> codifo estado operacion ofice
   -- p_COD_OPE_RSP_MPT    -> codigo de respuesta proceso
   -- p_GLS_OPE_RSP_MPT    -> glosa respuesta proceso
   -- Output:
   -- p_ERROR              -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE Sp_INS_OPE_DCV_AUT (p_NUM_IDF_SWF       IN     NUMBER,
                                 p_NUM_SSN_SWF       IN     NUMBER,
                                 p_FEC_ISR_MSJ_SWF   IN     DATE,
                                 p_NUM_REF_IDE_DCV   IN     VARCHAR2,
                                 p_NUM_MSJ_SWF_RLD   IN     VARCHAR2,
                                 p_FEC_RCP_MSJ       IN     DATE,
                                 p_COD_DVI           IN     CHAR,
                                 p_IMP_MTO           IN     NUMBER,
                                 p_NUM_CTA_ODN       IN     VARCHAR2,
                                 p_NUM_DOC_ODN       IN     CHAR,
                                 p_NUM_CTA_DCV_ODN   IN     VARCHAR2,
                                 p_NOM_ODN           IN     VARCHAR2,
                                 p_COD_BCO_DTN       IN     VARCHAR2,
                                 p_NUM_CTA_BFC       IN     VARCHAR2,
                                 p_NUM_DOC_BFC       IN     CHAR,
                                 p_NUM_CTA_DCV_BFC   IN     VARCHAR2,
                                 p_NOM_BFC           IN     VARCHAR2,
                                 p_NUM_CLV_NGC       IN     VARCHAR2,
                                 p_COD_EST_OFB       IN     CHAR,
                                 p_COD_OPE_RSP_MPT   IN     CHAR,
                                 p_GLS_OPE_RSP_MPT   IN     VARCHAR2,
                                 p_ERROR                OUT NUMBER);

   /************************************************************************************************
   -- Funcion/Procedimiento: Sp_BUS_CAM_OBT_SWF
   -- Objetivo: Procedimiento que ingresa operaciones del DCV automaticos
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_BIBLI_CAMPO_OBLTO_MT
   --
   -- Fecha: 16/06/17
   -- Autor: SII GROUP
   -- Input:
   -- p_COD_MT_SWF         -> Codigo MT a validar
   -- p_FLG_MRD_UTZ_SWF    -> flag de mercado del mensaje a validar
   -- p_COD_TPO_OPE_AOS    -> codigo operacion a validar
   -- Output:
   -- p_CAM_OBT            -> Retorna campos a validar.
   -- p_ERROR              -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE Sp_BUS_CAM_OBT_SWF (p_COD_MT_SWF        IN     NUMBER,
                                 p_FLG_MRD_UTZ_SWF   IN     NUMBER,
                                 p_COD_TPO_OPE_AOS   IN     VARCHAR2,
                                 p_CAM_OBT              OUT SYS_REFCURSOR,
                                 p_ERROR                OUT NUMBER);

   /************************************************************************************************
   -- Funcion/Procedimiento: Sp_BUS_MSJ_OBT_SWF
   -- Objetivo: Procedimiento que busca los mensajes a validar
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_BIBLI_CAMPO_OBLTO_MT
   --
   -- Fecha: 16/06/17
   -- Autor: SII GROUP
   -- Input: N/A
   -- Output:
   -- p_MSJ_OBT            -> Cursor de retorno de mensajes a validar.
   -- p_ERROR              -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE Sp_BUS_MSJ_OBT_SWF (p_MSJ_OBT   OUT SYS_REFCURSOR,
                                 p_ERROR     OUT NUMBER);

   /************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_GES_OPE_AGP
   -- Objetivo: Procedimiento que inserta la informacion que se encuentra en la nomina
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: N/A
   -- Fecha: 02/06/16
   -- Autor: Santander
   -- Input:     p_FEC_ISR_OPE     -> Fecha de insercion de la operaci�n
   --            p_NUM_FOL_OPE     -> Numero Foliador de la operacion
   --            p_NUM_FOL_NMN     -> Numero Foliador de la nomina
   --            p_NUM_FOL_OPE_ORG -> Numero que tiene el numero original de la operacion relacionada
   --            p_NUM_OPE_SIS_ENT -> Numero de operacion de la operacion en el sistema origen
   --            p_FLG_EGR_ING     -> Flag de egreso e ingreso
   --            p_COD_SIS_SAL     -> Codigo de sistema de salida
   --            p_COD_SIS_ENT     -> Codigo de sistema de entrada
   --            p_COD_EST_OPE     -> Estado de la operaci�n
   --            p_COD_TPO_ING     -> Tipo de ingreso
   --            p_COD_MT_SWF      -> Numero del tipo de mensaje swift
   --            p_FLG_MRD_UTZ_SWF -> Flasg de mercado
   --            p_COD_TPO_OPE_AOS -> Tipo de operacion.
   --            p_FEC_VTA         -> Fecha de vencimiento
   --            p_COD_DVI         -> Codigo de la divisa
   --            p_IMp_OPE         -> Importe de la operacion
   --            p_COD_BCO_DTN     -> BIC del banco destino
   --            p_COD_BCO_ORG     -> BIC Banco Origen
   --            p_FEC_ENV_RCP_SWF -> Fecha de envio / recepcion swfit
   --            p_HOR_ENV_RCP_SWF -> Hora de envio / recepcion swfit
   --            p_NUM_REF_SWF     -> Numero de referencia Swift
   --            p_NUM_REF_EXT     -> Numero de referencia swift externa
   --            p_NUM_DOC_BFC     -> Numero rut del beneficiario
   --            p_COD_TPD_BFC     -> Rut tipo de beneficiario
   --            p_NOM_BFC         -> Nombre del Beneficiario
   --            p_NUM_CTA_BFC     -> Numero de cuenta del beneficiario
   --            p_COD_SUC         -> Codigo de la sucursal
   --            p_GLS_ADC_EST     -> Informacion adicional
   --            p_NUM_REF_CTB     -> Numero de referencia contable
   --            p_COD_BCO_BFC     -> BIB Banco Beneficario
   --            p_COD_BCO_ITD     -> BIC Banco Intermediario
   --            p_NUM_CTA_DCv_BFC -> Numero cuenta DCV Beneficiario
   --            p_GLS_DIR_BFC     -> Direccion Beneficario
   --            p_NOM_CDD_BFC     -> Nombre ciudad beneficiario
   --            p_COD_PAS_BFC     -> Codigo pais beneficiario
   --            p_NOM_ODN         -> Nombre ordenante
   --            p_NUM_DOC_ODN     -> Numero rut ordenante
   --            p_COD_TPD_ODN     -> Tipo documento de ordenante
   --            p_NUM_CTA_ODN     -> Numero cuenta ordenante
   --            p_NUM_CTA_DCv_ODN -> Numero cuenta DCV ordenante
   --            p_GLS_DIR_ODN     -> Direccion ordenante
   --            p_NOM_CDD_ODN     -> Nombre ciudad ordenante
   --            p_COD_PAS_ODN     -> Codigo pais ordenante
   --            p_NUM_CLv_NGC     -> Numero de clave de negocio
   --            p_NUM_AGT         -> Numero de agente
   --            p_COD_FND_CCLV    -> Codigo Fondo (Producto CCLV)
   --            p_COD_TPO_SDO     -> Tipo de saldo
   --            p_COD_TPO_CMA     -> Tipo de camara
   --            p_COD_TPO_FND     -> Tipo de fondo
   --            p_FEC_TPO_OPE_CCLV-> Fecha operacion CCLV
   --            p_NUM_CLv_IDF     -> Numero clave identificatorio
   --            p_OBS_OPC_SWF   -> Observacion Opcional Swift
   --            p_NUM_IDF_GTR_DOC -> Identificador gestor documental
   --            p_NUM_MVT         -> Numero de Movimiento (Vigente, Cuenta Corriente)
   --            p_COD_USR     -> rut usuario
   -- Output:
   --          p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE Sp_PAB_GES_OPE_AGP (p_FEC_ISR_OPE        IN     DATE,
                                 p_NUM_FOL_OPE        IN     NUMBER,
                                 p_NUM_FOL_NMN        IN     NUMBER,
                                 p_NUM_FOL_OPE_ORG    IN     NUMBER,
                                 p_NUM_OPE_SIS_ENT    IN     CHAR,
                                 p_FLG_EGR_ING        IN     NUMBER,
                                 p_COD_SIS_SAL        IN     CHAR,
                                 p_COD_SIS_ENT        IN     CHAR,
                                 p_COD_EST_OPE        IN     NUMBER,
                                 p_COD_TPO_ING        IN     NUMBER,
                                 p_COD_MT_SWF         IN     NUMBER,
                                 p_FLG_MRD_UTZ_SWF    IN     NUMBER,
                                 p_COD_TPO_OPE_AOS    IN     CHAR,
                                 p_FEC_VTA            IN     DATE,
                                 p_COD_DVI            IN     CHAR,
                                 p_IMp_OPE            IN     NUMBER,
                                 p_COD_BCO_DTN        IN     CHAR,
                                 p_COD_BCO_ORG        IN     CHAR,
                                 p_FEC_ENV_RCP_SWF    IN     DATE,
                                 p_HOR_ENV_RCP_SWF    IN     NUMBER,
                                 p_NUM_REF_SWF        IN     CHAR,
                                 p_NUM_REF_EXT        IN     CHAR,
                                 ------------Beneficiario------------
                                 p_NUM_DOC_BFC        IN     CHAR,
                                 p_COD_TPD_BFC        IN     CHAR,
                                 p_NOM_BFC            IN     VARCHAR2,
                                 p_NUM_CTA_BFC        IN     VARCHAR2,
                                 -------------------------------------
                                 p_COD_SUC            IN     VARCHAR2,
                                 p_GLS_ADC_EST        IN     VARCHAR2,
                                 p_NUM_REF_CTB        IN     CHAR,
                                 ----------Opcionales--------------
                                 p_COD_BCO_BFC        IN     CHAR,
                                 p_COD_BCO_ITD        IN     CHAR,
                                 p_NUM_CTA_DCv_BFC    IN     VARCHAR2,
                                 p_GLS_DIR_BFC        IN     VARCHAR2,
                                 p_NOM_CDD_BFC        IN     VARCHAR2,
                                 p_COD_PAS_BFC        IN     CHAR,
                                 p_NOM_ODN            IN     VARCHAR2,
                                 p_NUM_DOC_ODN        IN     CHAR,
                                 p_COD_TPD_ODN        IN     CHAR,
                                 p_NUM_CTA_ODN        IN     VARCHAR2,
                                 p_NUM_CTA_DCv_ODN    IN     VARCHAR2,
                                 p_GLS_DIR_ODN        IN     VARCHAR2,
                                 p_NOM_CDD_ODN        IN     VARCHAR2,
                                 p_COD_PAS_ODN        IN     CHAR,
                                 p_NUM_CLv_NGC        IN     VARCHAR2,
                                 p_NUM_AGT            IN     CHAR,
                                 p_COD_FND_CCLV       IN     CHAR,
                                 p_COD_TPO_SDO        IN     CHAR,
                                 p_COD_TPO_CMA        IN     CHAR,
                                 p_COD_TPO_FND        IN     VARCHAR2,
                                 p_FEC_TPO_OPE_CCLV   IN     DATE,
                                 p_NUM_CLv_IDF        IN     VARCHAR2,
                                 p_OBS_OPC_SWF        IN     VARCHAR2,
                                 p_NUM_IDF_GTR_DOC    IN     VARCHAR2,
                                 p_NUM_MVT            IN     CHAR,
                                 --------------------------------
                                 p_COD_USR            IN     CHAR,
                                 p_ERROR                 OUT NUMBER);

   --***********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_BUS_OPE_PLZ
   -- Objetivo: Procedimiento que BUSCA numero de plazo para actualizar estado
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_DETLL_OPRCN_PLAZO
   -- Fecha: 25/07/17
   -- Autor: Santander
   -- Input:
   -- p_NUM_OPE_OFB : numero de operacion OfficeBanking
   -- Output:
   -- p_NUM_OPE_PZO : numero de vale vista Plazo
   -- p_ERR_CODE       -> codigo de error de ejecucion (retorno para TIBCO)
   -- p_ERR_MSG        -> mensaje de error de ejecucion (retorno para TIBCO)
   -- p_ERROR          -> codigo de error de ejecucion de PL (para log)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de utimos cambios>
   --***********************************************************************************************
   PROCEDURE SP_PAB_BUS_OPE_PLZ (p_NUM_OPE_OFB   IN     VARCHAR2,
                                 p_NUM_OPE_PZO      OUT VARCHAR2,
                                 p_ERR_CODE         OUT VARCHAR2,
                                 p_ERR_MSG          OUT VARCHAR2,
                                 p_ERROR            OUT NUMBER);

   /************************************************************************************************
   -- Funcion/Procedimiento: FN_PAB_COR_CTA
   -- Objetivo: Corta la cuenta corriente
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: N/A
   -- Fecha: 02/06/16
   -- Autor: Santander
   -- Input:      p_NUM_CTA: Numero Cuenta Corriente
   --             p_LARGO:  Largo necesario de la cuenta
   -- Output:
   --
   -- Input/Output: N/A
   -- Retorno: Retorna la cuenta cortada segun lo solicitado
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   FUNCTION FN_PAB_COR_CTA (p_NUM_CTA IN VARCHAR2, p_LARGO IN NUMBER)
      RETURN VARCHAR2;

   --***********************************************************************************************
   -- Funcion/Procedimiento: SP_ING_PAG
   -- Objetivo: Procedimiento que ingresa operaciones tipo LBTR
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: SP_ACT_EST_OPE_DVP
   -- Fecha: 25/07/17
   -- Autor: Santander
   -- Input:
   -- p_NUM_DOC_BCO    -> Numero documento empresa
   -- p_COD_DVI        -> Codigo divisa
   -- p_IMP_MTO        -> Monto operacion
   -- p_FEC_ING        -> fecha de ingreso
   -- p_FEC_VTA        -> fecha de valuta
   -- p_NUM_DOC_BFN    -> numero documento beneficiario
   -- p_NOM_BFN        -> nombre beneficiario
   -- p_CTA_BFC        -> cuenta beneficiario
   -- p_NUM_DOC_ORD    -> numero documento ordenante
   -- p_NOM_ORD        -> nombre ordenante
   -- p_CTA_ORD        -> cuenta ordenante
   -- p_COD_SIS_ORI    -> Codigo sistema origen
   -- p_SUC_ORI        -> Sucursal origen
   -- p_COD_TR_PAGO    -> Codigo transaccion
   -- p_NUM_OPE        -> numero operacion
   -- p_COD_RFR        -> refenrencia externa
   -- p_REF_EXT        -> Codifo referencia
   -- p_CAN_PAG        -> Cantidad de paginas
   -- p_CTA_CTB        -> referencia externa
   -- p_GLS_ADC        -> glosa adicional
   -- Output:
   -- p_ERR_CODE       -> codigo de error de ejecucion (retorno para TIBCO)
   -- p_ERR_MSG        -> mensaje de error de ejecucion (retorno para TIBCO)
   -- p_ERROR          -> codigo de error de ejecucion de PL (para log)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de utimos cambios>
   --***********************************************************************************************

   PROCEDURE SP_ING_PAG_OB (p_NUM_DCO_BCO   IN     VARCHAR2,
                            p_COD_DVI       IN     VARCHAR2,
                            p_IMP_MTO       IN     NUMERIC,
                            p_FEC_ING       IN     VARCHAR2,
                            p_FEC_VTA       IN     VARCHAR2,
                            p_NUM_DCO_BNF   IN     VARCHAR2,
                            p_NOM_BNF       IN     VARCHAR2,
                            p_CTA_BNF       IN     VARCHAR2,
                            p_NUM_DCO_ODN   IN     VARCHAR2,
                            p_NOM_ODN       IN     VARCHAR2,
                            p_CTA_ODN       IN     VARCHAR2,
                            p_COD_SIS_ORI   IN     VARCHAR2,
                            p_SUC_ORI       IN     NUMERIC,
                            p_COD_TR_PAGO   IN     VARCHAR2,  -- VVT - OVT ETC
                            p_NUM_OPE       IN     VARCHAR2,
                            p_COD_RFR       IN     VARCHAR2,
                            p_REF_EXT       IN     VARCHAR2,
                            p_CAN_PAG       IN     NUMERIC,
                            p_CTA_CTB       IN     VARCHAR2,
                            p_GLS_ADIC      IN     VARCHAR2,
                            p_NUM_FOL_OPE      OUT NUMBER,
                            p_FEC_ISR_OPE      OUT DATE,
                            p_FOR_PAG          OUT VARCHAR2,
                            p_ERR_CODE         OUT VARCHAR2,
                            p_ERR_MSG          OUT VARCHAR2,
                            p_ERROR            OUT NUMBER);
END PKG_PAB_CANALES;
/


CREATE OR REPLACE /* Formatted on 18-08-2020 17:45:33 (QP5 v5.115.810.9015) */
PACKAGE BODY DBO_PAB.PKG_PAB_CANALES
IS
   /**  Package el cual contrendra todas las funciones y procedimientos relacionados con la comunicacion de canales
   -- @SII GROUP */

   --***********************************************************************************************
   -- Funcion/Procedimiento: SP_EST_OPE_AOS
   -- Objetivo: Procedimiento que retorna informacion de una operacion DCV en Opreaciones
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: SP_EST_OPE_AOS
   -- Fecha: 25/07/17
   -- Autor: Santander
   -- Input:
   -- p_CVL_NEG -> Clave de negocios informada para DCV
   -- p_MOV_CAN -> Movimiento de operaci�n
   -- p_IMP_MTO -> Monto de la operaci�n
   -- p_NUM_OPE -> Numero de Operacion
   -- Output:
   -- p_EST_PROC      -> Estado de proceso
   -- p_NUM_RESP_PROC -> Codigo Respuesta si existe del proceso
   -- p_NUM_DOC_BEN   -> Mensaje de error / Confirmacion de datos
   -- p_NUM_RESP_DOC  -> Codigo Respuesta si existe el Numero de Operacion
   -- p_CTA_BEN       -> Mensaje de error / Confirmacion de datos
   -- p_NUM_RESP_CTA  -> Codigo Respuesta si existe el Numero de Operacion
   -- p_ERR_CODE      -> Codigo de error de ejecucion (retorno para TIBCO)
   -- p_ERR_MSG       -> mensaje de error de ejecucion (retorno para TIBCO)
   -- p_ERROR         -> codigo de error de ejecucion de PL (para log)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de utimos cambios>
   --***********************************************************************************************

   PROCEDURE SP_EST_OPE_AOS (p_CVL_NEG         IN     VARCHAR2,
                             p_MOV_CAN         IN     CHAR,
                             p_IMP_MTO         IN     NUMBER,
                             p_NUM_OPE         IN     VARCHAR2,
                             p_EST_PROC           OUT VARCHAR2,
                             p_NUM_RESP_PROC      OUT NUMBER,
                             p_NUM_DOC_BEN        OUT VARCHAR2,
                             p_NUM_RESP_DOC       OUT NUMBER,
                             p_CTA_BEN            OUT VARCHAR2,
                             p_NUM_RESP_CTA       OUT NUMBER,
                             p_ERR_CODE           OUT VARCHAR2,
                             p_ERR_MSG            OUT VARCHAR2,
                             p_ERROR              OUT NUMBER)
   IS
      --Seteo de Variables
      v_NOM_SP     VARCHAR2 (30) := 'SP_EST_OPE_AOS';
      v_MSG_ERR    VARCHAR2 (30) := 'Operaci�n no Existente';
      v_MSG_EXT    CHAR (2) := 'OK';
      v_CVL_NEG    VARCHAR2 (20) := p_CVL_NEG;
      v_MOV_CAN    CHAR (1) := p_MOV_CAN;
      v_IMP_MTO    NUMBER (18, 2) := p_IMP_MTO;
      v_NUM_OPE    NUMBER (11) := p_NUM_OPE;

      v_fechaHoy   DATE := SYSDATE;

      --DECLARACION DE EXCEPCIONES

      CLAV_NEG EXCEPTION;
      MOV_CANT EXCEPTION;
      IMP_MTO EXCEPTION;
      NUM_OPE EXCEPTION;
   BEGIN
      p_ERROR := PKG_PAB_CONSTANTES.v_ok;

      IF (p_CVL_NEG = ' ') OR (p_CVL_NEG IS NULL)
      THEN
         RAISE CLAV_NEG;
      END IF;

      IF (p_MOV_CAN = ' ') OR (p_MOV_CAN IS NULL)
      THEN
         RAISE MOV_CANT;
      END IF;

      IF (p_IMP_MTO = 0) OR (p_IMP_MTO IS NULL)
      THEN
         RAISE IMP_MTO;
      END IF;

      IF (p_NUM_OPE = ' ') OR (p_NUM_OPE IS NULL)
      THEN
         RAISE NUM_OPE;
      END IF;

      BEGIN
         SELECT   '16',
                  LPAD (DET.NUM_DOC_BFC || DET.COD_TPD_BFC, 11, '0'),
                  DET.NUM_CTA_BFC
           INTO   p_NUM_RESP_PROC, p_NUM_DOC_BEN, p_CTA_BEN
           FROM   PABS_DT_DETLL_OPRCN DET, PABS_DT_OPRCN_INFCN_OPCON OPC
          WHERE       DET.NUM_OPE_SIS_ENT = v_NUM_OPE --NUMERO DE OPERACION ORIGINAL (DESDE OB)
                  AND DET.IMP_OPE = v_IMP_MTO        -- MONTO DE LA OPEREACION
                  AND OPC.NUM_CLV_NGC = v_CVL_NEG          -- CLAVE DE NEGOCIO
                  AND DET.FEC_ISR_OPE = v_fechaHoy -- FECHA DE INGRESO DESDE OB
                  AND DET.NUM_FOL_OPE = OPC.NUM_FOL_OPE  -- OPERACION ORIGINAL
                  AND DET.FEC_ISR_OPE = OPC.FEC_ISR_OPE;     -- FECHA ORIGINAL
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            p_EST_PROC := v_MSG_ERR;
            p_NUM_RESP_PROC := '1';
            p_NUM_DOC_BEN := v_MSG_ERR;
            p_NUM_RESP_DOC := '1';
            p_CTA_BEN := v_MSG_ERR;
            p_NUM_RESP_CTA := '1';
            p_ERR_CODE := '50';
            p_ERR_MSG := v_MSG_ERR;
            p_ERROR := PKG_PAB_CONSTANTES.v_ok;
            err_code := '50';
            err_msg :=
                  'No se encuentran Datos para Operacion: '
               || v_NUM_OPE
               || ', Clave Negocio : '
               || v_CVL_NEG;
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
         WHEN OTHERS
         THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            p_s_mensaje := err_code || '-' || err_msg;
            RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
      END;
   EXCEPTION
      WHEN CLAV_NEG
      THEN
         p_ERR_MSG := 'Clave de Negocio Sin Dato Informado - Obligatorio';
         p_ERR_CODE := 1;
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (p_ERR_CODE,
                                             v_NOM_PCK,
                                             p_ERR_MSG,
                                             v_NOM_SP);
      WHEN MOV_CANT
      THEN
         p_ERR_MSG := 'Movimiento Cantidad Sin Dato Informado - Obligatorio';
         p_ERR_CODE := 1;
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (p_ERR_CODE,
                                             v_NOM_PCK,
                                             p_ERR_MSG,
                                             v_NOM_SP);
      WHEN IMP_MTO
      THEN
         p_ERR_MSG := 'Monto Sin Dato Informado - Obligatorio';
         p_ERR_CODE := 1;
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (p_ERR_CODE,
                                             v_NOM_PCK,
                                             p_ERR_MSG,
                                             v_NOM_SP);
      WHEN NUM_OPE
      THEN
         p_ERR_MSG := 'Numero de Operaci�n Sin Dato Informado - Obligatorio';
         p_ERR_CODE := 1;
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (p_ERR_CODE,
                                             v_NOM_PCK,
                                             p_ERR_MSG,
                                             v_NOM_SP);
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END SP_EST_OPE_AOS;

   --***********************************************************************************************
   -- Funcion/Procedimiento: SP_ING_DVP_AEN
   -- Objetivo: Procedimiento que ingresa operaciones provenientes de CUSTODIA (CARMEN)
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: SP_ING_DVP_AEN
   -- Fecha: 25/07/17
   -- Autor: Santander
   -- Input:
   -- p_NUM_OPE        -> numero de Operacion
   -- p_NUM_DOC_BNF    -> numero de rut beneficiario
   -- p_NOM_BNF        -> nombre beneficiario
   -- p_CTA_BNF        -> cuenta beneficiario
   -- p_NUM_DOC_ORD    -> numero de rut ordenante
   -- p_NOM_ORD        -> nombre ordenante
   -- p_CTA_ORD        -> cuenta ordenante
   -- p_FEC_PAG        -> fecha de pago
   -- p_FEC_VAL        -> fecha de valuta
   -- p_MTO_EGR        -> monto egreso
   -- p_MTO_ING        -> monto ingreso
   -- p_COD_DVI        -> codigo divisa
   -- p_COD_SIS_ORI    -> codifo sistema origen
   -- p_DOC_EMP        -> rut empresa
   -- p_REF_EXT        -> referencia externa
   -- p_COD_REF        -> codigo referencia
   -- p_RUT_BCO_DTN    -> rut banco destino
   -- p_COD_SWF_DTN    -> codigo swift destino
   -- p_NUM_SUC        -> numero de sucursar
   -- p_GLOSA          -> glosa operacion
   -- p_CANAL_ENT      -> canal de entrada
   -- p_FORMA_PAGO     -> forma de pago operacion
   -- p_ClaveNeg01     -> clave de negocio (1)
   -- p_ClaveNeg02     -> clave de negocio (2)
   -- p_ClaveNeg03     -> clave de negocio (3)
   -- p_ClaveNeg04     -> clave de negocio (4)
   -- p_ClaveNeg05     -> clave de negocio (5)
   -- p_ClaveNeg06     -> clave de negocio (6)
   -- p_ClaveNeg07     -> clave de negocio (7)
   -- p_ClaveNeg08     -> clave de negocio (8)
   -- p_ClaveNeg09     -> clave de negocio (9)
   -- p_ClaveNeg10     -> clave de negocio (10)
   -- p_CtaDcv_Ben     -> cuenta dvp beneficiario
   -- p_CtaDcv_Ord     -> cuenta dvp ordenante
   -- p_COD_USR        -> codigo usuario
   -- p_Num_SUC_Dest   -> sucursal destino
   -- p_Cod_TransPago  -> codigo transferencia de pago
   -- p_Msg_Rel        -> mensaje relacionado
   -- Output:
   -- p_ERR_CODE      -> codigo de error de ejecucion (retorno para TIBCO)
   -- p_ERR_MSG       -> mensaje de error de ejecucion (retorno para TIBCO)
   -- p_ERROR         -> codigo de error de ejecucion de PL (para log)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de utimos cambios>
   --***********************************************************************************************
   PROCEDURE SP_ING_DVP_AEN (p_NUM_OPE         IN     VARCHAR2,
                             p_NUM_DOC_BNF     IN     VARCHAR2,
                             p_NOM_BNF         IN     VARCHAR2,
                             p_CTA_BNF         IN     VARCHAR2,
                             p_NUM_DOC_ORD     IN     VARCHAR2,
                             p_NOM_ORD         IN     VARCHAR2,
                             p_CTA_ORD         IN     VARCHAR2,
                             p_FEC_PAG         IN     VARCHAR2,
                             p_FEC_VAL         IN     VARCHAR2,
                             p_MTO_EGR         IN     CHAR,
                             p_MTO_ING         IN     CHAR,
                             p_COD_DVI         IN     CHAR,
                             p_COD_SIS_ORI     IN     CHAR,
                             p_DOC_EMP         IN     VARCHAR2,
                             p_REF_EXT         IN     VARCHAR2,
                             p_COD_REF         IN     CHAR,
                             p_RUT_BCO_DTN     IN     VARCHAR2,
                             p_COD_SWF_DTN     IN     VARCHAR2,
                             p_NUM_SUC         IN     VARCHAR2,
                             p_GLOSA           IN     VARCHAR2,
                             p_CANAL_ENT       IN     VARCHAR2,
                             p_FORMA_PAGO      IN     VARCHAR2,
                             p_ClaveNeg01      IN     CHAR,
                             p_ClaveNeg02      IN     CHAR,
                             p_ClaveNeg03      IN     CHAR,
                             p_ClaveNeg04      IN     CHAR,
                             p_ClaveNeg05      IN     CHAR,
                             p_ClaveNeg06      IN     CHAR,
                             p_ClaveNeg07      IN     CHAR,
                             p_ClaveNeg08      IN     CHAR,
                             p_ClaveNeg09      IN     CHAR,
                             p_ClaveNeg10      IN     CHAR,
                             p_CtaDcv_Ben      IN     VARCHAR2,
                             p_CtaDcv_Ord      IN     VARCHAR2,
                             p_COD_USR         IN     VARCHAR2,
                             p_Num_SUC_Dest    IN     VARCHAR2,
                             p_Cod_TransPago   IN     CHAR,
                             p_Msg_Rel         IN     CHAR,
                             p_ERR_CODE           OUT VARCHAR2,
                             p_ERR_MSG            OUT VARCHAR2,
                             p_ERROR              OUT NUMBER)
   IS
      --Seteo de Variables
      v_NOM_SP                VARCHAR2 (30) := 'SP_ING_DVP_AEN';
      --v_fechaHoy date:= sysdate;
      v_NUM_FOL_OPE           NUMBER (12) := SPK_PAB_PAG_EAM.NEXTVAL; -- obtenemos FOLIO
      v_COD_USR               VARCHAR2 (11) := 'ESB';
      v_COD_MT_SWF            NUMBER (3);
      v_COD_BCO_DTN           CHAR (11);
      V_NUM_DOC_BFC           CHAR (11);
      V_COD_TPD_BFC           CHAR (1);
      V_NUM_DOC_BFC_NUM       NUMBER (10);
      V_NUM_DOC_ODN           CHAR (11);
      V_COD_TPD_ODN           CHAR (1);
      V_NUM_DOC_ODN_NUM       NUMBER (10);
      V_BIC_BANCO_SANTANDER   CHAR (11);
      V_COD_REF               CHAR (8);
      V_COD_SIS_ORI           CHAR (10) := 'CUSTODIA';
      V_EXT_VAL               NUMBER (1);
      v_FEC_VTA               DATE := TO_DATE (p_FEC_VAL, 'YYYY/MM/DD');
      v_MTO_EGR               NUMBER (13, 2);

      -- *** Variables para seccion de validacion de horario sistema y monto importe
      v_FLG_IMP_VAL           NUMBER;
      v_COD_EST_OPE           NUMBER;
      V_GLS_ADC_EST           VARCHAR2 (210);

      --variables nuevas

      --Variables de notificaci�n.
      v_FLG_NTF               NUMBER;
      v_CAN_OPE_NOT           NUMBER;
      p_ADJUNTO               NUMBER;
      p_ID                    VARCHAR2 (10);
      p_MENSAJE_SWF           VARCHAR2 (300);
      p_ASUNTO                VARCHAR2 (300);
      p_CORREO                VARCHAR2 (300);
      v_FEC_ING               DATE := SYSDATE;

      --NONREF MT202
      v_NONREF                CHAR (6) := 'NONREF';
      v_MSG_REL               CHAR (16) := p_Msg_Rel;
      v_NOM_ORD               PABS_DT_OPRCN_INFCN_OPCON.NOM_ODN%TYPE;
      v_CTA_ORD               PABS_DT_OPRCN_INFCN_OPCON.NUM_CTA_ODN%TYPE;

      OP_DUPL EXCEPTION;
      VALIADCION EXCEPTION;
   BEGIN
      p_ERROR := PKG_PAB_CONSTANTES.v_ok;
      p_ERR_CODE := '0';
      p_ERR_MSG := 'Dato Ingresado Correctamente a Altos Montos.';
      v_DES_BIT :=
         'Operacion ingresada desde sistema : '
         || PKG_PAB_UTILITY.FN_PAB_OBT_DSC_SIS_ENT_SAL (V_COD_SIS_ORI);
      --gcp

      V_COD_REF := PKG_PAB_CONSTANTES.V_STR_TPO_OPE_DVP;      -- ASIGNAMOS DVP


      --VALIDACIONES
      BEGIN
         SELECT   COUNT (DET.NUM_FOL_OPE)
           INTO   V_EXT_VAL
           FROM   PABS_DT_DETLL_OPRCN DET, PABS_DT_OPRCN_INFCN_OPCON OPC
          WHERE       DET.NUM_FOL_OPE = OPC.NUM_FOL_OPE
                  AND DET.FEC_ISR_OPE = OPC.FEC_ISR_OPE
                  AND DET.NUM_FOL_OPE_ORG = p_NUM_OPE
                  AND LPAD ( (DET.NUM_DOC_BFC || DET.COD_TPD_BFC), 11, '0') =
                        p_NUM_DOC_BNF
                  AND LPAD ( (OPC.NUM_DOC_ODN || OPC.COD_TPD_ODN), 11, '0') =
                        p_NUM_DOC_ORD
                  AND OPC.NUM_CTA_ODN = p_CTA_ORD
                  AND DET.NUM_CTA_BFC = p_CTA_BNF
                  AND DET.IMP_OPE = p_MTO_EGR
                  AND COD_BCO_DTN = p_COD_SWF_DTN
                  AND DET.FEC_VTA = v_FEC_VTA
                  AND OPC.NUM_CLV_NGC = p_ClaveNeg01;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            p_ERR_MSG :=
                  'No se encontr� registro Operacion Origen :'
               || p_NUM_OPE
               || ' - Clave de negocio : '
               || p_ClaveNeg01;
            p_ERR_CODE := 2;
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (p_ERR_CODE,
                                                v_NOM_PCK,
                                                p_ERR_MSG,
                                                v_NOM_SP);
         WHEN OTHERS
         THEN
            p_ERR_MSG := SUBSTR (SQLERRM, 1, 300);
            p_ERR_CODE := 2;
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (p_ERR_CODE,
                                                v_NOM_PCK,
                                                p_ERR_MSG,
                                                v_NOM_SP);
            RAISE_APPLICATION_ERROR (-20000, p_ERR_MSG);
      END;

      IF V_EXT_VAL = 1
      THEN
         p_ERR_MSG := 'Operaci�n ya recepcionada en Altos Montos';
         p_ERR_CODE := 2;
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (p_ERR_CODE,
                                             v_NOM_PCK,
                                             p_ERR_MSG,
                                             v_NOM_SP);

         RAISE OP_DUPL;
      END IF;


      -- INICIO TRANFORAMCION DE RUTS

      PKG_PAB_UTILITY.Sp_PAB_DESCOMp_RUT (p_NUM_DOC_BNF,
                                          V_NUM_DOC_BFC,
                                          V_COD_TPD_BFC);

      V_NUM_DOC_BFC_NUM := TO_NUMBER (V_NUM_DOC_BFC); -- TRANSDFORMAMOS A NUMERO PARA ELIMINAR 0 EN RUT

      p_ERR_MSG := V_NUM_DOC_BFC_NUM;

      PKG_PAB_UTILITY.Sp_PAB_DESCOMp_RUT (p_NUM_DOC_ORD,
                                          V_NUM_DOC_ODN,
                                          V_COD_TPD_ODN);

      V_NUM_DOC_ODN_NUM := TO_NUMBER (V_NUM_DOC_ODN); -- TRANSDFORMAMOS A NUMERO PARA ELIMINAR 0 EN RUT

      -- FIN TRANSFORMACION DE RUT

      -- TRANSFORMACION DE MONTOS

      v_MTO_EGR := SUBSTR (p_MTO_EGR, 0, LENGTH (p_MTO_EGR) - 2);

      -- FIN TRANSFORMACION DE MONTOS

      --GCP: Si la cuenta beneficiario es 0 se deja como un MT202
      IF TO_NUMBER (P_CTA_BNF) = 0
      THEN                           -- SI CUENTA BENEFICIARIO ES 0, ES UN 202
         v_COD_MT_SWF := PKG_PAB_CONSTANTES.V_COD_MT202;
         v_MSG_REL := v_NONREF; -- MT 202 No tendr� referencia externa (NONREF)
         v_NOM_ORD := 'BANCO SANTANDER';
         V_NUM_DOC_ODN_NUM := PKG_PAB_CONSTANTES.V_NUM_DOC_BANCO_SANTANDER;
         V_COD_TPD_ODN := PKG_PAB_CONSTANTES.V_COD_TPD_BANCO_SANTANDER;
         v_CTA_ORD := PKG_PAB_CONSTANTES.V_CTA_BANCO_SANTANDER;
      ELSE
         v_COD_MT_SWF := PKG_PAB_CONSTANTES.V_COD_MT103;
         v_NOM_ORD := p_NOM_ORD;
         v_CTA_ORD := TO_NUMBER (p_CTA_ORD);
      END IF;


      -- INCIO VALIDACION MONTO MAXIMO

      PKG_PAB_UTILITY.Sp_PAB_VAL_MONDA_ALMOT (p_COD_DVI,
                                              v_MTO_EGR,
                                              v_FLG_IMP_VAL,
                                              p_ERROR);

      IF v_FLG_IMP_VAL = PKG_PAB_CONSTANTES.V_error
      THEN
         v_COD_EST_OPE := PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPVI;
         V_GLS_ADC_EST := 'Operacion Supera Monto Maximo';
      ELSE
         v_COD_EST_OPE := PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEAUT;
         V_GLS_ADC_EST := P_GLOSA;
      END IF;

      -- FIN VALIDACION MONTO MAXIMO

      PKG_PAB_OPERACION.Sp_PAB_INS_OPE (
         v_FEC_ING,                       --Fecha de insercion de la operaci�n
         v_NUM_FOL_OPE,                      --Numero Foliador de la operacion
         0,                                     --Numero Foliador de la nomina
         0,  --Numero que tiene el numero original de la operacion relacionada
         TO_NUMBER (p_NUM_OPE), --Numero de operacion de la operacion en el sistema origen
         PKG_PAB_CONSTANTES.V_FLG_EGR,              --Flag de egreso e ingreso
         PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SAL_CMN, -- * Codigo de sistema de salida
         V_COD_SIS_ORI,                         --Codigo de sistema de entrada
         v_COD_EST_OPE,                               --Estado de la operaci�n
         PKG_PAB_CONSTANTES.V_COD_TIP_ING_DIR,               --Tipo de ingreso
         v_COD_MT_SWF,                      --Numero del tipo de mensaje swift
         PKG_PAB_CONSTANTES.V_FLG_MRD_MN,                   --Flasg de mercado
         V_COD_REF,                                       --Tipo de operacion.
         v_FEC_VTA,                                     --Fecha de vencimiento
         p_COD_DVI,                                      --Codigo de la divisa
         v_MTO_EGR,        --p_MTO_EGR            ,  --Importe de la operacion
         p_COD_SWF_DTN,                                --BIC del banco destino
         PKG_PAB_CONSTANTES.V_BIC_BANCO_SANTANDER,          --BIC Banco Origen
         NULL,                              --Fecha de envio / recepcion swfit
         NULL,                               --Hora de envio / recepcion swfit
         NULL,                                    --Numero de referencia Swift
         v_MSG_REL,                       --Numero de referencia swift externa
         V_NUM_DOC_BFC_NUM,                      --Numero rut del beneficiario
         V_COD_TPD_BFC,                             --Rut tipo de beneficiario
         p_NOM_BNF,                                  --Nombre del Beneficiario
         TO_NUMBER (p_CTA_BNF),            --Numero de cuenta del beneficiario
         LPAD (TO_NUMBER (p_NUM_SUC), 4, '0'),         --Codigo de la sucursal
         V_GLS_ADC_EST,                                --Informacion adicional
         NULL,                                 --Numero de referencia contable
         NULL,                                         --BIB Banco Beneficario
         NULL,                                       --BIC Banco Intermediario
         TO_NUMBER (p_CtaDcv_Ben),            --Numero cuenta DCV Beneficiario
         NULL,                                         --Direccion Beneficario
         NULL,                                    --Nombre ciudad beneficiario
         NULL,                                      --Codigo pais beneficiario
         v_NOM_ORD,                                         --Nombre ordenante
         V_NUM_DOC_ODN_NUM,                             --Numero rut ordenante
         V_COD_TPD_ODN,                          --Tipo documento de ordenante
         v_CTA_ORD,                                  --Numero cuenta ordenante
         TO_NUMBER (p_CtaDcv_Ord),               --Numero cuenta DCV ordenante
         NULL,                                           --Direccion ordenante
         NULL,                                       --Nombre ciudad ordenante
         NULL,                                         --Codigo pais ordenante
         p_ClaveNeg01,                            --Numero de clave de negocio
         NULL,                                              --Numero de agente
         NULL,                                  --Codigo Fondo (Producto CCLV)
         NULL,                                                 --Tipo de saldo
         NULL,                                                --Tipo de camara
         NULL,                                                 --Tipo de fondo
         NULL,                                          --Fecha operacion CCLV
         P_FORMA_PAGO,                          --Numero clave identificatorio
         P_GLOSA,                                 --Observacion Opcional Swift
         NULL,                               --Identificador gestor documental
         NULL,                                 --Numero de Movimiento (Vigente
         v_COD_USR,                                              --rut usuario
         p_ERROR
      );                                                                    --


      --gcp Guardamos bitacora
      PKG_PAB_TRACKING.Sp_PAB_INS_BIT_OPE (v_NUM_FOL_OPE,
                                           v_FEC_ING,
                                           v_COD_USR,
                                           v_COD_EST_OPE,
                                           v_COD_EST_OPE,
                                           v_DES_BIT,
                                           v_RESULT);

      IF v_COD_EST_OPE = PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPVI
      THEN
         --Notificaci�n del �rea Mesa
         PKG_PAB_NOTIFICACION.Sp_PAB_BUS_OPE_SIN_CBO (
            PKG_PAB_CONSTANTES.V_STR_EST_AOS_OPEPVI,
            v_FLG_NTF,
            v_CAN_OPE_NOT,
            p_ERROR
         );
         PKG_PAB_NOTIFICACION.SP_PAB_DET_CRRO_TIBCO (NULL,
                                                     NULL,
                                                     NULL,
                                                     'PVIS',
                                                     v_CAN_OPE_NOT, -- v_cont_est_vis,
                                                     NULL,    --'MESA',--NULL,
                                                     NULL,
                                                     NULL,
                                                     NULL,
                                                     p_ADJUNTO,
                                                     p_ID,
                                                     p_MENSAJE_SWF,
                                                     p_ASUNTO,
                                                     p_CORREO,
                                                     p_ERROR);
      END IF;
   EXCEPTION
      WHEN OP_DUPL
      THEN
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (p_ERR_CODE,
                                             v_NOM_PCK,
                                             p_ERR_MSG,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN DUP_VAL_ON_INDEX
      THEN
         p_ERR_MSG := 'Operacion ya existe en Altos Montos';
         p_ERR_CODE := 2;
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (p_ERR_CODE,
                                             v_NOM_PCK,
                                             p_ERR_MSG,
                                             v_NOM_SP);
      WHEN NO_DATA_FOUND
      THEN
         p_ERR_MSG := 'No se encontr� registro asociado';
         p_ERR_CODE := 2;
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (p_ERR_CODE,
                                             v_NOM_PCK,
                                             p_ERR_MSG,
                                             v_NOM_SP);
      WHEN OTHERS
      THEN
         p_ERR_MSG := SUBSTR (SQLERRM, 1, 300);
         p_ERR_CODE := 2;
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (p_ERR_CODE,
                                             v_NOM_PCK,
                                             p_ERR_MSG,
                                             v_NOM_SP);
         RAISE_APPLICATION_ERROR (-20000, p_ERR_MSG);
   END SP_ING_DVP_AEN;

   --***********************************************************************************************
   -- Funcion/Procedimiento: SP_ACT_EST_OPE_DVP
   -- Objetivo: Procedimiento que actualiza en Altos Montos los estados de las operaciones DVP
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: SP_ACT_EST_OPE_DVP
   -- Fecha: 25/07/17
   -- Autor: Santander
   -- Input:
   -- p_NUM_OPE    --> Numero de operacion Origen
   -- p_FEC_ING    --> Fecha de ingreso operacion
   -- p_IMP_MTO    --> Monto de la operacion
   -- p_CLV_NGC    --> Clave de negocio
   -- p_REF_EXT    --> Referencia Externa
   -- p_COD_EST    --> Estado a actualizar
   -- Output:
   -- p_ERR_CODE      -> codigo de error de ejecucion (retorno para TIBCO)
   -- p_ERR_MSG       -> mensaje de error de ejecucion (retorno para TIBCO)
   -- p_ERROR         -> codigo de error de ejecucion de PL (para log)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de utimos cambios>
   PROCEDURE SP_ACT_EST_OPE_DVP (p_NUM_OPE    IN     VARCHAR2,
                                 p_FEC_ING    IN     VARCHAR2,
                                 p_IMP_MTO    IN     VARCHAR2,
                                 p_CLV_NGC    IN     VARCHAR2,
                                 p_REF_EXT    IN     VARCHAR2,
                                 p_COD_EST    IN     VARCHAR2,
                                 p_ERR_CODE      OUT VARCHAR2,
                                 p_ERR_MSG       OUT VARCHAR2,
                                 p_ERROR         OUT NUMBER)
   IS
      --Seteo de Variables
      v_NOM_SP            VARCHAR2 (30) := 'SP_ACT_EST_OPE_DVP';
      --v_fechaHoy date:= sysdate;
      v_act_ok            CHAR (1) := '0';
      v_rec_comp          CHAR (1) := '1';
      v_rec_sldo          CHAR (1) := '2';
      v_num_ses           CHAR (4);
      v_num_isn           CHAR (6);
      v_cod_rec           NUMBER (4);
      v_NUM_REF_IDE_DCV   VARCHAR2 (20);
      v_FEC_INF           CHAR (6);
      v_COD_EST_OFB       CHAR (4);
      v_NUM_CLV_NGC       VARCHAR2 (10);
      v_GLD_EST_OFB       VARCHAR2 (50);
      v_FEC_ISR_MSJ_PRZ   DATE := SYSDATE;
      v_NUM_FOL_MSJ_PRZ   NUMBER;
      v_GLS_TXT_LBE_MSJ   VARCHAR2 (300);
      v_BIC_DTN           CHAR (11) := 'DCVVCLRM';
      v_REF_ORI           VARCHAR2 (16);
      v_REF_EXT           VARCHAR2 (20);



      -- DECLARACION DE EXCEPCION
      VALIDACION EXCEPTION;
      ERROR_UPD EXCEPTION;
   BEGIN
      v_REF_EXT := p_REF_EXT;

      p_ERROR := PKG_PAB_CONSTANTES.v_ok;
      p_ERR_CODE := '0';
      p_ERR_MSG := 'Dato Actualziado Correctamente en Altos Montos.';

      -- COMIENZO VAIDACIONES
      IF ( (TRIM (p_NUM_OPE) = '') OR (p_NUM_OPE IS NULL))
      THEN
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_ERR_CODE := '1';
         p_ERR_MSG := 'Numero de operacion no informado.-ALTOS MONTOS';

         RAISE VALIDACION;
      END IF;

      IF ( (TRIM (LENGTH (p_NUM_OPE)) > 10))
      THEN
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_ERR_CODE := '50';
         p_ERR_MSG := 'Numero de operacion no v�lido.-ALTOS MONTOS';

         RAISE VALIDACION;
      END IF;

      IF ( (TRIM (p_FEC_ING) = '') OR (p_FEC_ING IS NULL))
      THEN
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_ERR_CODE := '1';
         p_ERR_MSG := 'Fecha no informada.-ALTOS MONTOS';

         RAISE VALIDACION;
      END IF;

      IF ( (TRIM (p_IMP_MTO) = '') OR (p_IMP_MTO IS NULL))
      THEN
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_ERR_CODE := '50';
         p_ERR_MSG := 'Monto con datos invalidos - ALTOS MONTOS';

         RAISE VALIDACION;
      END IF;

      IF ( (TRIM (p_IMP_MTO) = '') OR (p_IMP_MTO IS NULL))
      THEN
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_ERR_CODE := '1';
         p_ERR_MSG := 'Monto no informado - ALTOS MONTOS';

         RAISE VALIDACION;
      END IF;

      IF ( (TRIM (p_CLV_NGC) = '') OR (p_CLV_NGC IS NULL))
      THEN
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_ERR_CODE := '1';
         p_ERR_MSG := 'Clave de medio de pago no informada.-ALTOS MONTOS';

         RAISE VALIDACION;
      END IF;

      IF (   (TRIM (p_COD_EST) = '')
          OR (p_COD_EST IS NULL)
          OR p_COD_EST NOT IN ('0', '1', '2'))
      THEN
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_ERR_CODE := '1';
         p_ERR_MSG := 'Estado de actualizacion invalido.-ALTOS MONTOS';

         RAISE VALIDACION;
      END IF;

      -- FIN VAIDACIONES

      -- COMIENZO LOGICA DE ACTUALIZACION

      v_num_ses := TO_NUMBER (SUBSTR (p_NUM_OPE, 0, 4));
      v_num_isn := TO_NUMBER (SUBSTR (p_NUM_OPE, 5, 10));


      IF (p_COD_EST = v_act_ok)
      THEN
         UPDATE   PABS_DT_DETLL_OPRCN_DCV
            SET   COD_OPE_RSP_MPT = p_COD_EST,
                  GLS_OPE_RSP_MPT =
                     'Operacion recibida correctamente por OfficeBank'
          WHERE       NUM_SSN_SWF = v_num_ses
                  AND NUM_IDF_SWF = v_num_isn
                  AND TO_CHAR (FEC_ISR_MSJ_SWF, 'DD/MM/YYYY') = p_FEC_ING
                  AND NUM_CLV_NGC = p_CLV_NGC
                  AND IMP_MTO = p_IMP_MTO;
      END IF;

      IF (p_COD_EST = v_rec_comp)
      THEN
         UPDATE   PABS_DT_DETLL_OPRCN_DCV
            SET   COD_OPE_RSP_MPT = p_COD_EST,
                  GLS_OPE_RSP_MPT =
                     'Operacion rechazada por OfficeBank rechazada comprador',
                  COD_EST_OFB = '1202'
          WHERE   NUM_SSN_SWF = v_num_ses AND NUM_IDF_SWF = v_num_isn --AND     FEC_ISR_MSJ_SWF = p_FEC_ING
                  AND NUM_CLV_NGC = p_CLV_NGC AND IMP_MTO = p_IMP_MTO;

         IF SQL%NOTFOUND
         THEN
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            p_ERR_CODE := '1';
            p_ERR_MSG :=
               'No se encontraron registros con los datos informados.- ALTOS MONTOS';
            RAISE ERROR_UPD;
         END IF;


         -- BUSCAMOS LOS DATOS PARA REALIZAR LA CONCATENACION DE LAS LINEAS
         BEGIN
            SELECT   DCV.NUM_REF_IDE_DCV,
                     TO_CHAR (SYSDATE, 'YYMMDD') AS FEC_INF,
                     DCV.COD_EST_OFB,
                     DCV.NUM_CLV_NGC,
                     OFB.GLD_EST_OFB
              INTO   v_NUM_REF_IDE_DCV,
                     v_FEC_INF,
                     v_COD_EST_OFB,
                     v_NUM_CLV_NGC,
                     v_GLD_EST_OFB
              FROM   PABS_DT_DETLL_OPRCN_DCV DCV, PABS_DT_ESTDO_OPRCN_DCV OFB
             WHERE       DCV.NUM_SSN_SWF = v_num_ses
                     AND DCV.NUM_IDF_SWF = v_num_isn
                     AND DCV.NUM_CLV_NGC = p_CLV_NGC
                     AND DCV.IMP_MTO = p_IMP_MTO
                     AND DCV.COD_EST_OFB = OFB.COD_EST_OFB;
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               p_ERR_MSG := 'No se encontr� registro ';
               p_ERR_CODE := 2;
               p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            WHEN OTHERS
            THEN
               p_ERR_MSG := SUBSTR (SQLERRM, 1, 300);
               p_ERR_CODE := 2;
               p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
               RAISE_APPLICATION_ERROR (-20000, p_ERR_MSG);
         END;

         --SE DEBE INGRESAR EL REGISTRO DE RECHAZO DE LA OPERACION PARA EL MT298

         --SOLICITAMOS FOLIO
         v_NUM_FOL_MSJ_PRZ := SPK_PAB_PAG_EAM.NEXTVAL;

         v_REF_ORI :=
            PKG_PAB_OPERACION.FN_PAB_GEN_REF_SWF (
               PKG_PAB_CONSTANTES.v_SRT_COD_SIS_SAL_LBTR,
               'BCO',
               v_NUM_FOL_MSJ_PRZ,
               PKG_PAB_CONSTANTES.V_COD_MT298
            );

         PKG_PAB_PROTOCOLO.Sp_PAB_INS_MSN_PRTCZ (
            PKG_PAB_CONSTANTES.V_COD_MT557,                --p_NUM_SUB_MSJ_RLD
            TRIM (v_NUM_REF_IDE_DCV),                      --p_NUM_REF_SWF_RLD
            v_REF_ORI,                                         --p_NUM_REF_SWF
            v_NUM_FOL_MSJ_PRZ,                            -- v_NUM_FOL_MSJ_PRZ
            NULL,                                          --p_HOR_ENV_RCP_SWF
            NULL,                                              --p_GLS_EST_RCH
            0,                                             --p_FLG_MRD_UTZ_SWF
            0,                                                 --p_FLG_EGR_ING
            v_FEC_ISR_MSJ_PRZ,                             --p_FEC_ISR_MSJ_PRZ
            NULL,                                          --p_FEC_ENV_RCP_SWF
            PKG_PAB_CONSTANTES.V_COD_MT298,                     --p_COD_MT_SWF
            PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEAUT,           --p_COD_EST_AOS
            v_BIC_DTN,                                         --p_COD_BCO_BFC
            NULL,                                              --p_COD_ADC_EST
            NULL,                                          --p_NUM_FOL_OPE_ORI
            NULL,                           --FECHA INSERCION OPERACION ORIGEN
            NULL,                                              --p_COD_TPO_OPE
            p_ERROR
         );

         --AHORA INGRESAMOS EL DETALLE
         v_GLS_TXT_LBE_MSJ :=
               ':21:'
            || v_NUM_REF_IDE_DCV
            || CHR (13)
            || ':11S:'
            || v_FEC_INF
            || CHR (13)
            || ':16R:RSN '
            || CHR (13)
            || ':M01:'
            || v_COD_EST_OFB
            || '/'
            || v_NUM_CLV_NGC
            || CHR (13)
            || ':M02:'
            || v_GLD_EST_OFB
            || CHR (13)
            || ':16S:RSN';

         PKG_PAB_PROTOCOLO.Sp_PAB_INS_DETLL_MSN_PRTCZ (v_NUM_FOL_MSJ_PRZ,
                                                       v_FEC_ISR_MSJ_PRZ,
                                                       v_GLS_TXT_LBE_MSJ,
                                                       p_ERROR);
      END IF;

      IF (p_COD_EST = v_rec_sldo)
      THEN
         UPDATE   PABS_DT_DETLL_OPRCN_DCV
            SET   COD_OPE_RSP_MPT = p_COD_EST,
                  GLS_OPE_RSP_MPT =
                     'Operacion recibida correctamente por OfficeBank',
                  COD_EST_OFB = '1209'
          WHERE   NUM_SSN_SWF = v_num_ses AND NUM_IDF_SWF = v_num_isn --AND     FEC_ISR_MSJ_SWF = p_FEC_ING
                  AND NUM_CLV_NGC = p_CLV_NGC AND IMP_MTO = p_IMP_MTO;


         --SE DEBE INGRESAR EL REGISTRO DE RECHAZO DE LA OPERACION PARA EL MT298

         --SOLICITAMOS FOLIO
         v_NUM_FOL_MSJ_PRZ := SPK_PAB_PAG_EAM.NEXTVAL;

         v_REF_ORI :=
            PKG_PAB_OPERACION.FN_PAB_GEN_REF_SWF (
               PKG_PAB_CONSTANTES.v_SRT_COD_SIS_SAL_LBTR,
               'BCO',
               v_NUM_FOL_MSJ_PRZ,
               PKG_PAB_CONSTANTES.V_COD_MT298
            );

         PKG_PAB_PROTOCOLO.Sp_PAB_INS_MSN_PRTCZ (
            PKG_PAB_CONSTANTES.V_COD_MT557,                --p_NUM_SUB_MSJ_RLD
            TRIM (v_NUM_REF_IDE_DCV),                      --p_NUM_REF_SWF_RLD
            v_REF_ORI,                                         --p_NUM_REF_SWF
            v_NUM_FOL_MSJ_PRZ,                            -- v_NUM_FOL_MSJ_PRZ
            NULL,                                          --p_HOR_ENV_RCP_SWF
            NULL,                                              --p_GLS_EST_RCH
            0,                                             --p_FLG_MRD_UTZ_SWF
            0,                                                 --p_FLG_EGR_ING
            v_FEC_ISR_MSJ_PRZ,                             --p_FEC_ISR_MSJ_PRZ
            NULL,                                          --p_FEC_ENV_RCP_SWF
            PKG_PAB_CONSTANTES.V_COD_MT298,                     --p_COD_MT_SWF
            PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEAUT,           --p_COD_EST_AOS
            v_BIC_DTN,                                         --p_COD_BCO_BFC
            NULL,                                              --p_COD_ADC_EST
            NULL,                                          --p_NUM_FOL_OPE_ORI
            NULL,                          -- FECHA INSERCION OPERACION ORIGEN
            NULL,                                              --p_COD_TPO_OPE
            p_ERROR
         );

         --AHORA INGRESAMOS EL DETALLE
         v_GLS_TXT_LBE_MSJ :=
               ':21:'
            || v_NUM_REF_IDE_DCV
            || CHR (13)
            || ':11S:'
            || v_FEC_INF
            || CHR (13)
            || ':16R:RSN '
            || CHR (13)
            || ':M01:'
            || v_COD_EST_OFB
            || '/'
            || v_NUM_CLV_NGC
            || CHR (13)
            || ':M02:'
            || v_GLD_EST_OFB
            || CHR (13)
            || ':16S:RSN';

         PKG_PAB_PROTOCOLO.Sp_PAB_INS_DETLL_MSN_PRTCZ (v_NUM_FOL_MSJ_PRZ,
                                                       v_FEC_ISR_MSJ_PRZ,
                                                       v_GLS_TXT_LBE_MSJ,
                                                       p_ERROR);
      END IF;
   -- FIN LOGICA DE ACTUALIZICION



   EXCEPTION
      WHEN VALIDACION
      THEN
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (p_ERR_CODE,
                                             v_NOM_PCK,
                                             p_ERR_MSG,
                                             v_NOM_SP);
      WHEN ERROR_UPD
      THEN
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (p_ERR_CODE,
                                             v_NOM_PCK,
                                             p_ERR_MSG,
                                             v_NOM_SP);
      WHEN OTHERS
      THEN
         p_ERR_CODE := SQLCODE;
         p_ERR_MSG := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (p_ERR_CODE,
                                             v_NOM_PCK,
                                             p_ERR_MSG,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_s_mensaje := p_ERR_CODE || '-' || p_ERR_MSG;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END SP_ACT_EST_OPE_DVP;

   --***********************************************************************************************
   -- Funcion/Procedimiento: SP_BUS_OPE_DVP
   -- Objetivo: Procedimiento que busca en Altos Montos las operaciones DVP
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: SP_ACT_EST_OPE_DVP
   -- Fecha: 25/07/17
   -- Autor: Santander
   -- Input:
   -- p_FEC_ING        -> Fecha de ingreso de la operacion
   -- p_NUM_DOC_CLI    -> numero de documento del cliente DVP
   -- p_CANT_REG       -> Cantidad de registros por pagina
   -- p_IND_RECALL     -> indicador si es rellamada.
   -- Output:
   -- p_OPERACIONES    -> Cursor con las operaciones encontradas
   -- p_id_recall      -> indicar si existen mas operaciones
   -- p_num_paginas    -> indicador de paginas
   -- p_ERR_CODE       -> codigo de error de ejecucion (retorno para TIBCO)
   -- p_ERR_MSG        -> mensaje de error de ejecucion (retorno para TIBCO)
   -- p_ERROR          -> codigo de error de ejecucion de PL (para log)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de utimos cambios>
   --***********************************************************************************************
   PROCEDURE SP_BUS_OPE_DVP (p_FEC_ING       IN     VARCHAR2,
                             p_NUM_DOC_CLI   IN     VARCHAR2,
                             p_CANT_REG      IN     NUMBER,
                             p_IND_RECALL    IN     NUMBER,
                             p_OPERACIONES      OUT SYS_REFCURSOR,
                             p_id_recall        OUT NUMBER,
                             p_num_paginas      OUT NUMBER,
                             p_ERR_CODE         OUT VARCHAR2,
                             p_ERR_MSG          OUT VARCHAR2,
                             p_ERROR            OUT NUMBER)
   IS
      --Seteo de Variables
      v_NOM_SP        VARCHAR2 (30) := 'SP_BUS_OPE_DVP';
      --v_fechaHoy date:= sysdate;
      V_NUM_DOC_CLI   CHAR (11);
      V_CANT_REG      NUMBER (3);
      V_IND_RECALL    NUMBER (3);
      l_rec           NUMBER;
      v_COD_EST_OFB   CHAR (4) := 'OK';

      TYPE gencurtyp IS REF CURSOR;

      VALIDACION EXCEPTION;
   BEGIN
      p_id_recall := 0;
      p_num_paginas := 0;
      p_ERR_CODE := '00';
      p_ERR_MSG := 'Datos Existentes en PAB';
      p_ERROR := 0;



      IF (TRIM (p_IND_RECALL) = '')
      THEN
         V_IND_RECALL := 0;
      END IF;

      IF ( (TRIM (p_FEC_ING) = '') OR (p_FEC_ING IS NULL))
      THEN
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_ERR_CODE := '1';
         p_ERR_MSG := 'Fecha no informado.-ALTOS MONTOS';

         RAISE VALIDACION;
      END IF;

      IF (   (TRIM (p_NUM_DOC_CLI) = '')
          OR (p_NUM_DOC_CLI IS NULL)
          OR (LENGTH (p_NUM_DOC_CLI) = 0))
      THEN
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_ERR_CODE := '1';
         p_ERR_MSG := 'Rut No Informado.-ALTOS MONTOS';

         RAISE VALIDACION;
      END IF;

      V_NUM_DOC_CLI := LPAD (TRIM (p_NUM_DOC_CLI), 11, '0');   -- SETEO DE RUT

      BEGIN
         SELECT   COUNT (NUM_MSJ_SWF_RLD)
           INTO   l_rec
           FROM   PABS_DT_DETLL_OPRCN_DCV
          WHERE   NUM_DOC_ODN = V_NUM_DOC_CLI
                  AND TO_CHAR (FEC_RCP_MSJ, 'YYYY/MM/DD') = p_FEC_ING;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            p_ERR_MSG := 1;
            p_ERR_CODE :=
                  'No se encuentra el registro asociado A RUT '
               || V_NUM_DOC_CLI
               || ' - FECHA - '
               || p_FEC_ING;
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (p_ERR_MSG,
                                                v_NOM_PCK,
                                                p_ERR_MSG,
                                                v_NOM_SP);
         WHEN OTHERS
         THEN
            p_ERR_CODE := SQLCODE;
            p_ERR_MSG := SUBSTR (SQLERRM, 1, 300);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;

            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (p_ERR_MSG,
                                                v_NOM_PCK,
                                                p_ERR_MSG,
                                                v_NOM_SP);

            p_s_mensaje := p_ERR_MSG || '-' || p_ERR_CODE;
            RAISE_APPLICATION_ERROR (-20000, p_ERR_MSG);
      END;

      IF l_rec = 0
      THEN
         p_ERR_CODE := '1';
         p_ERR_MSG :=
               'No existen datos asociados al RUT: '
            || V_NUM_DOC_CLI
            || ' y FECHA '
            || p_FEC_ING
            || ' en ALTOS MONTOS.-';
         p_ERROR := 0;
         RAISE VALIDACION;
      END IF;



      BEGIN
         OPEN p_OPERACIONES FOR
            SELECT   NUM_MSJ_SWF_RLD AS Mensaje,
                     TRIM (COD_BCO_DTN) AS Banco,
                     COD_DVI AS Moneda,
                     IMP_MTO AS Monto,
                     TO_CHAR (FEC_ISR_MSJ_SWF, 'DD/MM/YYYY') AS Fecha_Pago,
                     TO_CHAR (FEC_RCP_MSJ, 'DD/MM/YYYY') AS Fecha_Valor,
                     --BENEFICIARIO
                     NUM_DOC_BFC AS Rut_Beneficiario,
                     NOM_BFC AS Nombre_Beneficiario,
                     NUM_CTA_BFC AS Cuenta_Beneficiario,
                     NUM_CTA_DCV_BFC AS Cuenta_DCV_Beneficiario,
                     --ORDENANTE
                     NUM_DOC_ODN AS Rut_Ordenante,
                     NOM_ODN AS Nombre_Ordenante,
                     NUM_CTA_ODN AS Cuenta_Ordenante,
                     NUM_CTA_DCV_ODN AS Cuenta_DCV_Ordenante,
                     '' AS Sistema_Origen,
                     '174' AS Sucursal,
                     'DVPDCV' AS Codigo_Trans,
                     NUM_SSN_SWF || NUM_IDF_SWF AS Numope,
                     TRIM (NUM_REF_IDE_DCV) AS Referencia_Ext,
                     LPAD (TCDT.STBANCO, 3, '0') AS Glosa,
                     NUM_CLV_NGC AS ClaveNeg01
              FROM   PABS_DT_DETLL_OPRCN_DCV DCV, TCDT040 TCDT
             WHERE       NUM_DOC_ODN = V_NUM_DOC_CLI
                     AND DCV.COD_BCO_DTN = TCDT.TGCDSWSA
                     AND TO_CHAR (FEC_RCP_MSJ, 'YYYY/MM/DD') = p_FEC_ING
                     AND COD_EST_OFB = v_COD_EST_OFB
                     AND COD_OPE_RSP_MPT = 40; -- NO OPERACIONES YA TOMADAS POR OFFICEBANK
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;

            p_ERR_CODE := 1;
            p_ERR_MSG :=
                  'No se encuentra el registro asociado A RUT '
               || V_NUM_DOC_CLI
               || ' - FECHA - '
               || p_FEC_ING;

            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (p_ERR_CODE,
                                                v_NOM_PCK,
                                                p_ERR_MSG,
                                                v_NOM_SP);
         WHEN OTHERS
         THEN
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;

            p_ERR_CODE := SQLCODE;
            p_ERR_MSG := SUBSTR (SQLERRM, 1, 300);

            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (p_ERR_CODE,
                                                v_NOM_PCK,
                                                p_ERR_MSG,
                                                v_NOM_SP);

            p_s_mensaje := p_ERR_MSG || '-' || p_ERR_CODE;
            RAISE_APPLICATION_ERROR (-20000, p_ERR_MSG);
      END;

      p_ERROR := PKG_PAB_CONSTANTES.v_ok;
   EXCEPTION
      WHEN VALIDACION
      THEN
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (p_ERR_CODE,
                                             v_NOM_PCK,
                                             p_ERR_MSG,
                                             v_NOM_SP);
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END SP_BUS_OPE_DVP;

   --***********************************************************************************************
   -- Funcion/Procedimiento: SP_VAL_DUP_DVP
   -- Objetivo: Procedimiento que busca operaciones duplicadas en Altos Montos
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: SP_ACT_EST_OPE_DVP
   -- Fecha: 25/07/17
   -- Autor: Santander
   -- Input:
   -- p_FEC_ING        -> Fecha de ingreso de la operacion
   -- p_IMP_MTO        -> Monto de la operacion
   -- p_CLV_NEG        -> Clave de negocio DVP
   -- Output:
   -- p_ERR_CODE       -> codigo de error de ejecucion (retorno para TIBCO)
   -- p_ERR_MSG        -> mensaje de error de ejecucion (retorno para TIBCO)
   -- p_ERROR          -> codigo de error de ejecucion de PL (para log)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de utimos cambios>
   --***********************************************************************************************
   PROCEDURE SP_VAL_DUP_DVP (p_FEC_ING    IN     VARCHAR2,
                             p_IMP_MTO    IN     NUMBER,
                             p_CLV_NEG    IN     VARCHAR2,
                             p_ERR_CODE      OUT VARCHAR2,
                             p_ERR_MSG       OUT VARCHAR2,
                             p_ERROR         OUT NUMBER)
   IS
      --Seteo de Variables
      v_NOM_SP        VARCHAR2 (30) := 'SP_VAL_DUP_DVP';
      --v_fechaHoy date:= sysdate;
      V_NUM_SSN_SWF   NUMBER (4);
      VALIDACION EXCEPTION;
      v_FEC_ING       DATE := TO_DATE (p_FEC_ING, 'YYYY/MM/DD');
   BEGIN
      p_ERROR := PKG_PAB_CONSTANTES.v_ok;


      IF ( (TRIM (p_FEC_ING) = '') OR (p_FEC_ING IS NULL))
      THEN
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_ERR_CODE := '1';
         p_ERR_MSG := 'Fecha no informado.-ALTOS MONTOS';

         RAISE VALIDACION;
      END IF;

      IF (   (TRIM (p_IMP_MTO) = '')
          OR (p_IMP_MTO IS NULL)
          OR (TRIM (p_IMP_MTO) = 0))
      THEN
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_ERR_CODE := '1';
         p_ERR_MSG := 'Monto no informado.-ALTOS MONTOS';

         RAISE VALIDACION;
      END IF;

      IF ( (TRIM (p_CLV_NEG) = '') OR (p_CLV_NEG IS NULL))
      THEN
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_ERR_CODE := '1';
         p_ERR_MSG := 'Clave de Negocio no informado.-ALTOS MONTOS';

         RAISE VALIDACION;
      END IF;

      BEGIN
         SELECT   NUM_SSN_SWF
           INTO   V_NUM_SSN_SWF
           FROM   PABS_DT_DETLL_OPRCN_DCV
          WHERE       FEC_ISR_MSJ_SWF = v_FEC_ING
                  AND NUM_CLV_NGC = p_CLV_NEG
                  AND IMP_MTO = p_IMP_MTO;

         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_ERR_CODE := '1';
         p_ERR_MSG := 'Registro ya ingresado en Altos montos..-ALTOS MONTOS';

         RAISE VALIDACION;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            p_ERR_CODE := 0;
            p_ERR_MSG :=
                  'No se encuentra el registro asociado A FECHA - '
               || p_FEC_ING
               || '- CLAVE NEGOCIO - '
               || p_CLV_NEG
               || ' - MONTO - '
               || p_IMP_MTO;
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (p_ERR_CODE,
                                                v_NOM_PCK,
                                                p_ERR_MSG,
                                                v_NOM_SP);
         WHEN OTHERS
         THEN
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;


            p_ERR_CODE := SQLCODE;
            p_ERR_MSG := SUBSTR (SQLERRM, 1, 300);

            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (p_ERR_CODE,
                                                v_NOM_PCK,
                                                p_ERR_MSG,
                                                v_NOM_SP);

            p_s_mensaje := p_ERR_MSG || '-' || p_ERR_CODE;
            RAISE_APPLICATION_ERROR (-20000, p_ERR_MSG);
      END;
   EXCEPTION
      WHEN VALIDACION
      THEN
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (p_ERR_CODE,
                                             v_NOM_PCK,
                                             p_ERR_MSG,
                                             v_NOM_SP);
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END SP_VAL_DUP_DVP;


   --***********************************************************************************************
   -- Funcion/Procedimiento: SP_CON_OFF_BKN
   -- Objetivo: Procedimiento que consulta operaciones provenientes de OfficeBanking
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: SP_ACT_EST_OPE_DVP
   -- Fecha: 25/07/17
   -- Autor: Santander
   -- Input:
   -- p_COD_USR        -> codigo usuario
   -- p_NUM_DOC_USU    -> rut empresa
   -- p_NUM_DOC_CLI    -> rut cliente
   -- p_FEC_DSD        -> fecha desde
   -- p_FEC_HST        -> fecha hasta
   -- p_COD_SIS_ENT    -> codigo sistema de entrada
   -- p_NUM_CTA_CTE    -> numero de cuenta
   -- p_EST_AOS        -> estado operacion
   -- p_TIP_TRS        -> tipo de transaccion
   -- p_TIP_FLJ        -> tipo de flujo
   -- p_REG_PAG_SAL    -> numero de paginas de salida
   -- p_NUM_REG_PAG_SAL-> registros por pagina
   -- p_NUM_PAG_SAL    -> numero de paginas de salida
   -- Output:
   -- p_DET_OPE        -> Cursor con las operaciones encontradas
   -- p_REG_PAG        -> indicar si existen mas operaciones
   -- p_NUM_PAG        -> indicador de paginas
   -- p_ERR_CODE       -> codigo de error de ejecucion (retorno para TIBCO)
   -- p_ERR_MSG        -> mensaje de error de ejecucion (retorno para TIBCO)
   -- p_ERROR          -> codigo de error de ejecucion de PL (para log)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de utimos cambios>
   --***********************************************************************************************
   PROCEDURE SP_CON_OFF_BKN (p_COD_USR           IN     VARCHAR2,
                             p_NUM_DOC_USU       IN     VARCHAR2,
                             p_NUM_DOC_CLI       IN     VARCHAR2,
                             p_FEC_DSD           IN     VARCHAR2,
                             p_FEC_HST           IN     VARCHAR2,
                             p_COD_SIS_ENT       IN     VARCHAR2,
                             p_NUM_CTA_CTE       IN     VARCHAR2,
                             p_EST_AOS           IN     NUMERIC,
                             p_TIP_TRS           IN     VARCHAR2,
                             p_TIP_FLJ           IN     VARCHAR2,
                             p_REG_PAG_SAL       IN     NUMERIC,
                             p_NUM_REG_PAG_SAL   IN     NUMERIC,
                             p_NUM_PAG_SAL       IN     NUMERIC,
                             p_DET_OPE              OUT SYS_REFCURSOR,
                             p_REG_PAG              OUT NUMERIC,
                             p_NUM_PAG              OUT NUMERIC,
                             p_ERR_CODE             OUT VARCHAR2,
                             p_ERR_MSG              OUT VARCHAR2,
                             p_ERROR                OUT NUMERIC)
   IS
      --Seteo de Variables
      v_NOM_SP        VARCHAR2 (30) := 'SP_CON_OFF_BKN';
      v_FEC_DSD       DATE;
      --v_fechaHoy date:= sysdate;
      v_TIP_FLJ       NUMBER;
      V_EST_AOS       NUMBER := 0;
      V_TPO_OPE_AOS   CHAR (10) := p_COD_SIS_ENT;
      V_COD_SIS_ENT   CHAR (10);
      V_NUM_CTA_CTE   CHAR (14);


      -- EXCEPCIONES}

      VALIDACION EXCEPTION;
   BEGIN
      p_REG_PAG := 0;
      p_NUM_PAG := 0;
      p_ERR_CODE := '00';
      --p_ERR_MSG := 'No se encuentra el registro asociado A RUT ' || p_NUM_DOC_CLI || ' - CUENTA - '|| p_NUM_CTA_CTE ||' FECHA DESDE - '|| p_FEC_DSD || ' - FECHA HASTA - ' || p_FEC_HST;
      p_ERR_MSG := '';
      p_ERROR := 0;

      IF (p_EST_AOS IS NULL) OR (p_EST_AOS = '') OR (p_EST_AOS = 0)
      THEN
         V_EST_AOS := '';
      ELSE
         V_EST_AOS := p_EST_AOS;
      END IF;

      IF (p_NUM_DOC_CLI = ' ') OR (p_NUM_DOC_CLI IS NULL)
      THEN
         p_ERR_MSG := 'Rut de Cliente Sin Dato Informado - Obligatorio';
         p_ERR_CODE := 1;
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         RAISE VALIDACION;
      END IF;


      IF (p_REG_PAG_SAL = 0) OR (p_REG_PAG_SAL IS NULL)
      THEN
         p_ERR_MSG := 'Cantidad de Registros Sin dato Informado - Obligatorio';
         p_ERR_CODE := 7;
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         RAISE VALIDACION;
      END IF;

      /*Valida Fecha de Proceso*/
      IF (p_FEC_DSD = '') OR (p_FEC_DSD = '0') OR (LENGTH (p_FEC_DSD) < 8)
      THEN
         p_ERR_MSG := 'Fecha Inicio Sin Dato Informado - Obligatorio';
         p_ERR_CODE := 1;
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         RAISE VALIDACION;
      ELSE
         v_FEC_DSD := TO_DATE (p_FEC_DSD, 'YYYY/MM/DD');
      END IF;


      IF (TO_NUMBER (TO_CHAR (v_FEC_DSD, 'YYYY')) >
             TO_NUMBER (TO_CHAR (SYSDATE + INTERVAL '2' YEAR, 'YYYY')))
      THEN                                    /* Suma 2 a�os a Fecha proceso*/
         p_ERR_MSG :=
            'Fecha Inicio supera en 2 a�os fecha proceso - Rango de fecha Invalido.';
         p_ERR_CODE := 1;
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         RAISE VALIDACION;
      END IF;

      IF ( (TO_NUMBER (TO_CHAR (v_FEC_DSD, 'MM')) > 12)
          OR TO_NUMBER (TO_CHAR (v_FEC_DSD, 'MM')) < 1)
      THEN                                    /* Suma 2 a�os a Fecha proceso*/
         p_ERR_MSG := 'Mes de Fecha Valor Invalido (AAAAMMDD) - Obligatorio';
         p_ERR_CODE := 1;
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         RAISE VALIDACION;
      END IF;

      IF ( (TO_NUMBER (TO_CHAR (v_FEC_DSD, 'DD')) > 28)
          AND TO_NUMBER (TO_CHAR (v_FEC_DSD, 'MM')) = 2)
      THEN
         p_ERR_MSG := 'Mes de Fecha Valor Invalido (AAAAMMDD) - Obligatorio';
         p_ERR_CODE := 1;
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         RAISE VALIDACION;
      END IF;

      IF ( (TO_NUMBER (TO_CHAR (v_FEC_DSD, 'DD')) > 30)
          AND TO_NUMBER (TO_CHAR (v_FEC_DSD, 'MM')) IN (4, 6, 9, 11))
      THEN                                    /* Suma 2 a�os a Fecha proceso*/
         p_ERR_MSG := 'Mes de Fecha Valor Invalido (AAAAMMDD) - Obligatorio';
         p_ERR_CODE := 1;
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         RAISE VALIDACION;
      END IF;

      IF ( (TO_NUMBER (TO_CHAR (v_FEC_DSD, 'DD')) > 31))
      THEN                                    /* Suma 2 a�os a Fecha proceso*/
         p_ERR_MSG := 'Mes de Fecha Valor Invalido (AAAAMMDD) - Obligatorio';
         p_ERR_CODE := 1;
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         RAISE VALIDACION;
      END IF;

      IF ( (p_TIP_FLJ = ' ') OR (p_TIP_FLJ IS NULL))
      THEN
         p_ERR_MSG := 'Tipo de Entrada sin dato informado - Obligatorio';
         p_ERR_CODE := 2;
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         RAISE VALIDACION;
      END IF;

      IF ( (p_TIP_FLJ <> 'R') AND (p_TIP_FLJ <> 'E'))
      THEN
         p_ERR_MSG := 'Tipo de Entrada ingresada no valida - Obligatorio';
         p_ERR_CODE := 2;
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         RAISE VALIDACION;
      END IF;



      --AGREGAR MANEJO DE LBTR Y DVP

      IF p_TIP_TRS = 'DVP'
      THEN                                                              --LBTR
         V_TPO_OPE_AOS := PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SAL_CMN;
      END IF;

      IF p_TIP_TRS = 'LBTR'
      THEN                                                              --DVOP
         V_TPO_OPE_AOS := PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SAL_LBTR;
      ELSE
         V_TPO_OPE_AOS := NULL;
      END IF;

      /*MANEJO DE CUENTA */

      IF NVL (p_NUM_CTA_CTE, '#') <> '#'
      THEN                                         -- SIGNIFICA QUE NO ES NULO
         V_NUM_CTA_CTE :=
            PKG_PAB_CANALES.FN_PAB_COR_CTA (p_NUM_CTA_CTE, v_largoCuenta);
      END IF;

      IF p_TIP_FLJ = 'E'
      THEN                                                           -- EGRESO
         v_TIP_FLJ := PKG_PAB_CONSTANTES.V_FLG_EGR;     -- PARA BUSCAR EGRESOS


         BEGIN
            IF V_EST_AOS = 97
            THEN
               SELECT   COUNT (DET.NUM_FOL_OPE) AS CtaOrigen
                 INTO   p_REG_PAG
                 FROM   PABS_DT_DETLL_OPRCN DET,
                        PABS_DT_OPRCN_INFCN_OPCON OPC,
                        PABS_DT_ESTDO_ALMOT EST
                WHERE       DET.NUM_FOL_OPE = OPC.NUM_FOL_OPE
                        AND DET.FEC_ISR_OPE = OPC.FEC_ISR_OPE
                        AND DET.COD_EST_AOS = EST.COD_EST_AOS
                        AND DET.COD_MT_SWF = '103'
                        AND DET.COD_DVI = 'CLP'
                        AND DET.FLG_EGR_ING = v_TIP_FLJ
                        AND LPAD (
                              TRIM (OPC.NUM_DOC_ODN)
                              || TRIM (OPC.COD_TPD_ODN),
                              11,
                              '0'
                           ) = p_NUM_DOC_CLI
                        AND PKG_PAB_CANALES.FN_PAB_COR_CTA (OPC.NUM_CTA_ODN,
                                                            v_largoCuenta) =
                              NVL (
                                 V_NUM_CTA_CTE,
                                 PKG_PAB_CANALES.FN_PAB_COR_CTA (
                                    OPC.NUM_CTA_ODN,
                                    v_largoCuenta
                                 )
                              )
                        AND DET.COD_SIS_ENT =
                              NVL (V_COD_SIS_ENT, DET.COD_SIS_ENT)
                        AND DET.COD_EST_AOS IN (7, 8, 10, 13)
                        AND DET.COD_SIS_SAL =
                              NVL (V_TPO_OPE_AOS, DET.COD_SIS_SAL)
                        AND TO_CHAR (DET.FEC_ISR_OPE, 'YYYYMMDD') BETWEEN p_FEC_DSD
                                                                      AND  p_FEC_HST;
            ELSE
               SELECT   COUNT (DET.NUM_FOL_OPE) AS CtaOrigen
                 INTO   p_REG_PAG
                 FROM   PABS_DT_DETLL_OPRCN DET,
                        PABS_DT_OPRCN_INFCN_OPCON OPC,
                        PABS_DT_ESTDO_ALMOT EST
                WHERE       DET.NUM_FOL_OPE = OPC.NUM_FOL_OPE
                        AND DET.FEC_ISR_OPE = OPC.FEC_ISR_OPE
                        AND DET.COD_EST_AOS = EST.COD_EST_AOS
                        AND DET.COD_MT_SWF = '103'
                        AND DET.COD_DVI = 'CLP'
                        AND DET.FLG_EGR_ING = v_TIP_FLJ
                        AND LPAD (
                              TRIM (OPC.NUM_DOC_ODN)
                              || TRIM (OPC.COD_TPD_ODN),
                              11,
                              '0'
                           ) = p_NUM_DOC_CLI
                        AND PKG_PAB_CANALES.FN_PAB_COR_CTA (OPC.NUM_CTA_ODN,
                                                            v_largoCuenta) =
                              NVL (
                                 V_NUM_CTA_CTE,
                                 PKG_PAB_CANALES.FN_PAB_COR_CTA (
                                    OPC.NUM_CTA_ODN,
                                    v_largoCuenta
                                 )
                              )
                        AND DET.COD_SIS_ENT =
                              NVL (V_COD_SIS_ENT, DET.COD_SIS_ENT)
                        AND DET.COD_EST_AOS =
                              NVL (V_EST_AOS, DET.COD_EST_AOS)
                        AND DET.COD_SIS_SAL =
                              NVL (V_TPO_OPE_AOS, DET.COD_SIS_SAL)
                        AND TO_CHAR (DET.FEC_ISR_OPE, 'YYYYMMDD') BETWEEN p_FEC_DSD
                                                                      AND  p_FEC_HST;
            END IF;
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;

               p_ERR_CODE := 1;
               p_ERR_MSG :=
                     'No se encuentra el registro asociado A RUT '
                  || p_NUM_DOC_CLI
                  || ' - CUENTA - '
                  || p_NUM_CTA_CTE
                  || ' - SISTEMA ENTRADA - '
                  || V_COD_SIS_ENT
                  || ' - ESTADO - '
                  || V_EST_AOS
                  || ' - FECHA DESDE - '
                  || p_FEC_DSD
                  || ' - FECHA HASTA - '
                  || p_FEC_HST;

               PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (p_ERR_CODE,
                                                   v_NOM_PCK,
                                                   p_ERR_MSG,
                                                   v_NOM_SP);
            WHEN OTHERS
            THEN
               p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;

               p_ERR_CODE := SQLCODE;
               p_ERR_MSG := SUBSTR (SQLERRM, 1, 300);

               PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (p_ERR_CODE,
                                                   v_NOM_PCK,
                                                   p_ERR_MSG,
                                                   v_NOM_SP);

               p_s_mensaje := p_ERR_MSG || '-' || p_ERR_CODE;
               RAISE_APPLICATION_ERROR (-20000, p_ERR_MSG);
         END;



         BEGIN
            IF V_EST_AOS = 97
            THEN
               OPEN p_DET_OPE FOR
                  SELECT   TO_CHAR (DET.FEC_VTA, 'DD/MM/YYYY') AS FechaValor,
                           TO_CHAR (DET.FEC_ISR_OPE, 'HH24:MI')
                              AS HoraMovimiento,
                           OPC.NUM_CTA_ODN AS CtaOrigen,
                           DET.COD_SIS_ENT AS SistemaOrigen,
                           DET.NUM_CTA_BFC AS CtaCteBeneficiario,
                           LPAD ( (SELECT   TCNIFENT
                                     FROM   TCDT040
                                    WHERE   TGCDSWSA = DET.COD_BCO_DTN),
                                 11,
                                 '0')
                              AS BancoDestino,          -- SACAR DE LA TCDT040
                           LPAD (
                              TRIM (DET.NUM_DOC_BFC)
                              || TRIM (DET.COD_TPD_BFC),
                              11,
                              '0'
                           )
                              AS RutBeneficiario,
                           DET.NOM_BFC AS NombreBeneficiario,
                           DET.IMP_OPE AS MontoPago,
                           OPC.OBS_OPC_SWF AS GlosaPago,
                           TO_CHAR (SYSDATE, 'DD/MM/YYYY') AS FechaProceso,
                           OPC.NUM_CLV_NGC AS ClavesdeNegocio,
                           EST.DSC_EST_AOS AS EstadoPago,
                           OPC.NUM_CTA_DCV_ODN AS CtaDCVOrdenante,
                           OPC.NUM_CTA_DCV_BFC AS CtaDCVBeneficiario,
                           DET.NUM_OPE_SIS_ENT AS NumeroOperacion
                    FROM   PABS_DT_DETLL_OPRCN DET,
                           PABS_DT_OPRCN_INFCN_OPCON OPC,
                           PABS_DT_ESTDO_ALMOT EST
                   WHERE       DET.NUM_FOL_OPE = OPC.NUM_FOL_OPE
                           AND DET.FEC_ISR_OPE = OPC.FEC_ISR_OPE
                           AND DET.COD_EST_AOS = EST.COD_EST_AOS
                           AND DET.COD_MT_SWF = '103'
                           AND DET.COD_DVI = 'CLP'
                           AND DET.FLG_EGR_ING = v_TIP_FLJ
                           AND LPAD (
                                 TRIM (OPC.NUM_DOC_ODN)
                                 || TRIM (OPC.COD_TPD_ODN),
                                 11,
                                 '0'
                              ) = p_NUM_DOC_CLI
                           AND PKG_PAB_CANALES.FN_PAB_COR_CTA (
                                 OPC.NUM_CTA_ODN,
                                 v_largoCuenta
                              ) =
                                 NVL (
                                    V_NUM_CTA_CTE,
                                    PKG_PAB_CANALES.FN_PAB_COR_CTA (
                                       OPC.NUM_CTA_ODN,
                                       v_largoCuenta
                                    )
                                 )
                           AND DET.COD_SIS_ENT =
                                 NVL (V_COD_SIS_ENT, DET.COD_SIS_ENT)
                           AND DET.COD_EST_AOS IN (7, 8, 10, 13)
                           AND DET.COD_SIS_SAL =
                                 NVL (V_TPO_OPE_AOS, DET.COD_SIS_SAL) -- cambiar nombre variable
                           AND TO_CHAR (DET.FEC_ISR_OPE, 'YYYYMMDD') BETWEEN p_FEC_DSD
                                                                         AND  p_FEC_HST;
            ELSE
               OPEN p_DET_OPE FOR
                  SELECT   TO_CHAR (DET.FEC_VTA, 'DD/MM/YYYY') AS FechaValor,
                           TO_CHAR (DET.FEC_ISR_OPE, 'HH24:MI')
                              AS HoraMovimiento,
                           OPC.NUM_CTA_ODN AS CtaOrigen,
                           DET.COD_SIS_ENT AS SistemaOrigen,
                           DET.NUM_CTA_BFC AS CtaCteBeneficiario,
                           LPAD ( (SELECT   TCNIFENT
                                     FROM   TCDT040
                                    WHERE   TGCDSWSA = DET.COD_BCO_DTN),
                                 11,
                                 '0')
                              AS BancoDestino,          -- SACAR DE LA TCDT040
                           LPAD (
                              TRIM (DET.NUM_DOC_BFC)
                              || TRIM (DET.COD_TPD_BFC),
                              11,
                              '0'
                           )
                              AS RutBeneficiario,
                           DET.NOM_BFC AS NombreBeneficiario,
                           DET.IMP_OPE AS MontoPago,
                           OPC.OBS_OPC_SWF AS GlosaPago,
                           TO_CHAR (SYSDATE, 'DD/MM/YYYY') AS FechaProceso,
                           OPC.NUM_CLV_NGC AS ClavesdeNegocio,
                           EST.DSC_EST_AOS AS EstadoPago,
                           OPC.NUM_CTA_DCV_ODN AS CtaDCVOrdenante,
                           OPC.NUM_CTA_DCV_BFC AS CtaDCVBeneficiario,
                           DET.NUM_OPE_SIS_ENT AS NumeroOperacion
                    FROM   PABS_DT_DETLL_OPRCN DET,
                           PABS_DT_OPRCN_INFCN_OPCON OPC,
                           PABS_DT_ESTDO_ALMOT EST
                   WHERE       DET.NUM_FOL_OPE = OPC.NUM_FOL_OPE
                           AND DET.FEC_ISR_OPE = OPC.FEC_ISR_OPE
                           AND DET.COD_EST_AOS = EST.COD_EST_AOS
                           AND DET.COD_MT_SWF = '103'
                           AND DET.COD_DVI = 'CLP'
                           AND DET.FLG_EGR_ING = v_TIP_FLJ
                           AND LPAD (
                                 TRIM (OPC.NUM_DOC_ODN)
                                 || TRIM (OPC.COD_TPD_ODN),
                                 11,
                                 '0'
                              ) = p_NUM_DOC_CLI
                           AND PKG_PAB_CANALES.FN_PAB_COR_CTA (
                                 OPC.NUM_CTA_ODN,
                                 v_largoCuenta
                              ) =
                                 NVL (
                                    V_NUM_CTA_CTE,
                                    PKG_PAB_CANALES.FN_PAB_COR_CTA (
                                       OPC.NUM_CTA_ODN,
                                       v_largoCuenta
                                    )
                                 )
                           AND DET.COD_SIS_ENT =
                                 NVL (V_COD_SIS_ENT, DET.COD_SIS_ENT)
                           AND DET.COD_EST_AOS =
                                 NVL (V_EST_AOS, DET.COD_EST_AOS)
                           AND DET.COD_SIS_SAL =
                                 NVL (V_TPO_OPE_AOS, DET.COD_SIS_SAL) -- cambiar nombre variable
                           AND TO_CHAR (DET.FEC_ISR_OPE, 'YYYYMMDD') BETWEEN p_FEC_DSD
                                                                         AND  p_FEC_HST;
            END IF;
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;

               p_ERR_MSG :=
                     'No se encuentra el registro asociado A RUT '
                  || p_NUM_DOC_CLI
                  || ' - CUENTA - '
                  || p_NUM_CTA_CTE
                  || ' - SISTEMA ENTRADA - '
                  || V_COD_SIS_ENT
                  || ' - ESTADO - '
                  || V_EST_AOS
                  || ' - FECHA DESDE - '
                  || p_FEC_DSD
                  || ' - FECHA HASTA - '
                  || p_FEC_HST;
               p_ERR_CODE := SQLCODE;

               PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (p_ERR_CODE,
                                                   v_NOM_PCK,
                                                   p_ERR_MSG,
                                                   v_NOM_SP);
            WHEN OTHERS
            THEN
               p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;

               p_ERR_CODE := SQLCODE;
               p_ERR_MSG := SUBSTR (SQLERRM, 1, 300);


               PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (p_ERR_CODE,
                                                   v_NOM_PCK,
                                                   p_ERR_MSG,
                                                   v_NOM_SP);


               p_s_mensaje := p_ERR_MSG || '-' || p_ERR_CODE;
               RAISE_APPLICATION_ERROR (-20000, p_ERR_MSG);
         END;
      END IF;


      IF p_TIP_FLJ = 'R'
      THEN                                                         --RECIBIDO.
         v_TIP_FLJ := PKG_PAB_CONSTANTES.V_FLG_ING;    -- PARA BUSCAR INGRESOS

         BEGIN
            OPEN p_DET_OPE FOR
               SELECT   TO_CHAR (DET.FEC_VTA, 'DD/MM/YYYY') AS FechaValor,
                        TO_CHAR (DET.FEC_ISR_OPE, 'HH24:MI')
                           AS HoraMovimiento,
                        OPC.NUM_CTA_ODN AS CtaOrigen,
                        DET.COD_SIS_ENT AS SistemaOrigen,
                        DET.NUM_CTA_BFC AS CtaCteBeneficiario,
                        LPAD ( (SELECT   TCNIFENT
                                  FROM   TCDT040
                                 WHERE   TGCDSWSA = DET.COD_BCO_DTN),
                              11,
                              '0')
                           AS BancoDestino,             -- SACAR DE LA TCDT040
                        LPAD (
                           TRIM (DET.NUM_DOC_BFC) || TRIM (DET.COD_TPD_BFC),
                           11,
                           '0'
                        )
                           AS RutBeneficiario,
                        DET.NOM_BFC AS NombreBeneficiario,
                        DET.IMP_OPE AS MontoPago,
                        OPC.OBS_OPC_SWF AS GlosaPago,
                        TO_CHAR (SYSDATE, 'DD/MM/YYYY') AS FechaProceso,
                        OPC.NUM_CLV_NGC AS ClavesdeNegocio,
                        EST.DSC_EST_AOS AS EstadoPago,
                        OPC.NUM_CTA_DCV_ODN AS CtaDCVOrdenante,
                        OPC.NUM_CTA_DCV_BFC AS CtaDCVBeneficiario,
                        DET.NUM_OPE_SIS_ENT AS NumeroOperacion
                 FROM   PABS_DT_DETLL_OPRCN DET,
                        PABS_DT_OPRCN_INFCN_OPCON OPC,
                        PABS_DT_ESTDO_ALMOT EST
                WHERE       DET.NUM_FOL_OPE = OPC.NUM_FOL_OPE
                        AND DET.FEC_ISR_OPE = OPC.FEC_ISR_OPE
                        AND DET.COD_EST_AOS = EST.COD_EST_AOS
                        AND DET.COD_MT_SWF = '103'
                        AND DET.COD_DVI = 'CLP'
                        AND DET.FLG_EGR_ING = v_TIP_FLJ
                        AND LPAD (
                              TRIM (OPC.NUM_DOC_ODN)
                              || TRIM (OPC.COD_TPD_ODN),
                              11,
                              '0'
                           ) = p_NUM_DOC_CLI                   --p_NUM_DOC_CLI
                        AND OPC.NUM_CTA_ODN =
                              NVL (p_NUM_CTA_CTE, OPC.NUM_CTA_ODN)
                        AND DET.COD_SIS_ENT =
                              NVL (V_COD_SIS_ENT, DET.COD_SIS_ENT)
                        AND DET.COD_EST_AOS =
                              NVL (V_EST_AOS, DET.COD_EST_AOS)
                        AND DET.COD_SIS_SAL =
                              NVL (V_TPO_OPE_AOS, DET.COD_SIS_SAL) -- cambiar nombre variable
                        AND TO_CHAR (DET.FEC_ISR_OPE, 'YYYYMMDD') BETWEEN p_FEC_DSD
                                                                      AND  p_FEC_HST;
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;

               p_ERR_MSG :=
                     'No se encuentra el registro asociado A RUT '
                  || p_NUM_DOC_CLI
                  || ' - CUENTA - '
                  || p_NUM_CTA_CTE
                  || ' - SISTEMA ENTRADA - '
                  || V_COD_SIS_ENT
                  || ' - ESTADO - '
                  || V_EST_AOS
                  || ' - FECHA DESDE - '
                  || p_FEC_DSD
                  || ' - FECHA HASTA - '
                  || p_FEC_HST;
               p_ERR_CODE := SQLCODE;

               PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (p_ERR_CODE,
                                                   v_NOM_PCK,
                                                   p_ERR_MSG,
                                                   v_NOM_SP);
            WHEN OTHERS
            THEN
               p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;

               p_ERR_CODE := SQLCODE;
               p_ERR_MSG := SUBSTR (SQLERRM, 1, 300);

               PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (p_ERR_CODE,
                                                   v_NOM_PCK,
                                                   p_ERR_MSG,
                                                   v_NOM_SP);


               p_s_mensaje := p_ERR_MSG || '-' || p_ERR_CODE;
               RAISE_APPLICATION_ERROR (-20000, p_ERR_MSG);
         END;
      END IF;
   EXCEPTION
      WHEN VALIDACION
      THEN
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (p_ERR_CODE,
                                             v_NOM_PCK,
                                             p_ERR_MSG,
                                             v_NOM_SP);
      WHEN DUP_VAL_ON_INDEX
      THEN
         p_ERR_CODE := '1';
         p_ERR_MSG := 'Valor ya ingresado en Altos Montos';
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (p_ERR_CODE,
                                             v_NOM_PCK,
                                             p_ERR_MSG,
                                             v_NOM_SP);
      WHEN NO_DATA_FOUND
      THEN
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;

         p_ERR_CODE := '1';
         p_ERR_MSG := 'No se encuentra el registro asociado';

         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (p_ERR_CODE,
                                             v_NOM_PCK,
                                             p_ERR_MSG,
                                             v_NOM_SP);
      WHEN OTHERS
      THEN
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;

         p_ERR_CODE := SQLCODE;
         p_ERR_MSG := SUBSTR (SQLERRM, 1, 300);

         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (p_ERR_CODE,
                                             v_NOM_PCK,
                                             p_ERR_MSG,
                                             v_NOM_SP);

         p_s_mensaje := p_ERR_MSG || '-' || p_ERR_CODE;
         RAISE_APPLICATION_ERROR (-20000, p_ERR_MSG);
   END SP_CON_OFF_BKN;

   --***********************************************************************************************
   -- Funcion/Procedimiento: SP_ING_PAG_DVP
   -- Objetivo: Procedimiento que ingresa operaciones tipo DVP
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: SP_ACT_EST_OPE_DVP
   -- Fecha: 25/07/17
   -- Autor: Santander
   -- Input:
   -- p_NUM_DOC_BCO    -> Numero documento empresa
   -- p_COD_DVI        -> Codigo divisa
   -- p_IMP_MTO        -> Monto operacion
   -- p_FEC_ING        -> fecha de ingreso
   -- p_FEC_VTA        -> fecha de valuta
   -- p_NUM_DOC_BFN    -> numero documento beneficiario
   -- p_NOM_BFN        -> nombre beneficiario
   -- p_CTA_BFC        -> cuenta beneficiario
   -- p_NUM_DOC_ORD    -> numero documento ordenante
   -- p_NOM_ORD        -> nombre ordenante
   -- p_CTA_ORD        -> cuenta ordenante
   -- p_ID_DCVORO      -> Id DCV
   -- p_COD_SEMAT      -> codigo SEMA
   -- p_SUC_ORI        -> Sucursal origen
   -- p_COD_TSN        -> Codigo transaccion
   -- p_NUM_OPE        -> numero operacion
   -- p_REF_EXT        -> referencia externa
   -- p_GLS_ADC        -> glosa adicional
   -- p_CLV_NEG_01     -> Clave de negicio 1
   -- p_CLV_NEG_02     -> Clave de negicio 2
   -- p_CLV_NEG_03     -> Clave de negicio 3
   -- p_CLV_NEG_04     -> Clave de negicio 4
   -- p_CLV_NEG_05     -> Clave de negicio 5
   -- p_CLV_NEG_06     -> Clave de negicio 6
   -- p_CLV_NEG_07     -> Clave de negicio 7
   -- p_CLV_NEG_08     -> Clave de negicio 8
   -- p_CLV_NEG_09     -> Clave de negicio 9
   -- p_CLV_NEG_10     -> Clave de negicio 10
   -- Output:
   -- p_ERR_CODE       -> codigo de error de ejecucion (retorno para TIBCO)
   -- p_ERR_MSG        -> mensaje de error de ejecucion (retorno para TIBCO)
   -- p_ERROR          -> codigo de error de ejecucion de PL (para log)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de utimos cambios>
   --***********************************************************************************************
   PROCEDURE SP_ING_PAG_DVP (p_NUM_DOC_BCO   IN     VARCHAR2,
                             p_COD_DVI       IN     VARCHAR2,
                             p_IMP_MTO       IN     NUMERIC,
                             p_FEC_ING       IN     VARCHAR2,
                             p_FEC_VTA       IN     VARCHAR2,
                             p_NUM_DOC_BFN   IN     VARCHAR2,
                             p_NOM_BFN       IN     VARCHAR2,
                             p_CTA_BFC       IN     VARCHAR2,
                             p_NUM_DOC_ORD   IN     VARCHAR2,
                             p_NOM_ORD       IN     VARCHAR2,
                             p_CTA_ORD       IN     VARCHAR2,
                             p_ID_DCVORO     IN     VARCHAR2,
                             p_COD_SEMAT     IN     VARCHAR2,
                             p_SUC_ORI       IN     VARCHAR2,
                             p_COD_TSN       IN     VARCHAR2,
                             p_NUM_OPE       IN     VARCHAR2,
                             p_REF_EXT       IN     VARCHAR2,
                             p_GLS_ADC       IN     VARCHAR2,
                             p_CLV_NEG_01    IN     VARCHAR2,
                             p_CLV_NEG_02    IN     VARCHAR2,
                             p_CLV_NEG_03    IN     VARCHAR2,
                             p_CLV_NEG_04    IN     VARCHAR2,
                             p_CLV_NEG_05    IN     VARCHAR2,
                             p_CLV_NEG_06    IN     VARCHAR2,
                             p_CLV_NEG_07    IN     VARCHAR2,
                             p_CLV_NEG_08    IN     VARCHAR2,
                             p_CLV_NEG_09    IN     VARCHAR2,
                             p_CLV_NEG_10    IN     VARCHAR2,
                             p_ERR_CODE         OUT VARCHAR2,
                             p_ERR_MSG          OUT VARCHAR2,
                             p_ERROR            OUT NUMBER)
   IS
      --Seteo de Variables
      v_NOM_SP            VARCHAR2 (30) := 'SP_ING_PAG_DVP';
      --v_fechaHoy date:= sysdate;
      v_NUM_FOL_OPE       NUMBER (12) := SPK_PAB_PAG_EAM.NEXTVAL; -- obtenemos FOLIO
      v_COD_USR           VARCHAR2 (11) := 'ESB';

      v_COD_MT_SWF        NUMBER (3) := 103;              -- por ahora en duro
      v_COD_BCO_DTN       CHAR (11);
      V_NUM_DOC_BFC       CHAR (11);
      V_COD_TPD_BFC       CHAR (1);
      V_NUM_DOC_BFC_NUM   NUMBER (10);
      V_NUM_DOC_ODN       CHAR (11);
      V_COD_TPD_ODN       CHAR (1);
      V_NUM_DOC_ODN_NUM   NUMBER (10);
      V_NUM_OPE_ORI       NUMBER (16);
      v_FEC_VTA           DATE := TO_DATE (p_FEC_VTA, 'YYYY/MM/DD');
      v_FEC_ING           DATE := SYSDATE;

      -- *** Variables para seccion de validacion de horario sistema y monto importe
      v_FLG_IMP_VAL       NUMBER;
      v_COD_EST_OPE       NUMBER;
      V_GLS_ADC_EST       VARCHAR2 (210);
      v_IMP_MON           NUMBER;
      v_DSC_VAR           VARCHAR2 (50);
      v_CTA_CTE_16        VARCHAR2 (16);

      --Variables de notificaci�n.
      v_FLG_NTF           NUMBER;
      v_CAN_OPE_NOT       NUMBER;
      p_ADJUNTO           NUMBER;
      p_ID                VARCHAR2 (10);
      p_MENSAJE_SWF       VARCHAR2 (300);
      p_ASUNTO            VARCHAR2 (300);
      p_CORREO            VARCHAR2 (300);
      p_CANAL_SAL         CHAR (10);
      v_RUT_ITAU          VARCHAR2 (11) := '0076645030K';
      v_RUT_CORP          VARCHAR2 (11) := '00970230009';
      v_NUM_DCO_BCO       VARCHAR2 (11);

      op_REG EXCEPTION;
      v_VAL_OPE           INTEGER;

      v_NOM_BNF           VARCHAR2 (100);
      v_NOM_ODN           VARCHAR2 (100);
      v_PRO_NOM           VARCHAR2 (100) := 'SIN NOMBRE INFORMADO';
   BEGIN
      /*SE AGREGA VALIDACION DE OPERACIONES DESDE EL ORIGEN */

      v_VAL_OPE :=
         PKG_PAB_UTILITY.FN_PAB_BUS_VVV (TRIM (TO_CHAR (p_COD_SEMAT)));

      IF v_VAL_OPE = 1
      THEN
         RAISE op_REG;
      END IF;

      /*SE AGREGA VALIDACION DE TEXTO NO VALIDO. */

      v_NOM_BNF := PKG_PAB_UTILITY.FN_PAB_VLD_TXT (p_NOM_BFN);

      IF NVL (TRIM (v_NOM_BNF), '#') = '#'
      THEN
         v_NOM_BNF := v_PRO_NOM;
      ELSE
         v_NOM_BNF := p_NOM_BFN;
      END IF;

      v_NOM_ODN := PKG_PAB_UTILITY.FN_PAB_VLD_TXT (p_NOM_ORD);


      IF NVL (TRIM (v_NOM_ODN), '#') = '#'
      THEN
         v_NOM_ODN := v_PRO_NOM;
      ELSE
         v_NOM_ODN := p_NOM_ORD;
      END IF;

      v_CTA_CTE_16 := SUBSTR (p_CTA_ORD, 9, 12);

      p_ERROR := PKG_PAB_CONSTANTES.v_ok;
      p_ERR_CODE := '0';
      p_ERR_MSG := 'Dato Ingresado Correctamente';
      v_DES_BIT :=
         'Operacion ingresada desde sistema : '
         || PKG_PAB_UTILITY.FN_PAB_OBT_DSC_SIS_ENT_SAL (p_COD_SEMAT);


      IF p_NUM_DOC_BCO = v_RUT_ITAU
      THEN
         v_NUM_DCO_BCO := v_RUT_CORP;
      ELSE
         v_NUM_DCO_BCO := p_NUM_DOC_BCO;
      END IF;


      BEGIN
         SELECT   TGCDSWSA
           INTO   v_COD_BCO_DTN
           FROM   TCDT040
          WHERE   LPAD (TCNIFENT, 11, '0') = v_NUM_DCO_BCO
                  AND TGCDSWSA IS NOT NULL;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            p_ERR_CODE := '1';
            p_ERR_MSG :=
               'No se encuentra el registro asociado al RUT '
               || p_NUM_DOC_BCO;
            --p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (p_ERR_CODE,
                                                v_NOM_PCK,
                                                p_ERR_MSG,
                                                v_NOM_SP);
      END;


      -- INICIO TRANFORAMCION DE RUTS
      PKG_PAB_UTILITY.Sp_PAB_DESCOMp_RUT (p_NUM_DOC_BFN,
                                          V_NUM_DOC_BFC,
                                          V_COD_TPD_BFC);

      V_NUM_DOC_BFC_NUM := TO_NUMBER (V_NUM_DOC_BFC); -- TRANSDFORMAMOS A NUMERO PARA ELIMINAR 0 EN RUT

      p_ERR_MSG := V_NUM_DOC_BFC_NUM;

      PKG_PAB_UTILITY.Sp_PAB_DESCOMp_RUT (p_NUM_DOC_ORD,
                                          V_NUM_DOC_ODN,
                                          V_COD_TPD_ODN);

      V_NUM_DOC_ODN_NUM := TO_NUMBER (V_NUM_DOC_ODN); -- TRANSDFORMAMOS A NUMERO PARA ELIMINAR 0 EN RUT

      -- FIN TRANSFORMACION DE RUT


      v_IMP_MON := SUBSTR (p_IMP_MTO, 0, LENGTH (p_IMP_MTO) - 2);      --MONTO
      -- INCIO VALIDACION MONTO MAXIMO

      PKG_PAB_UTILITY.Sp_PAB_VAL_MONDA_ALMOT (p_COD_DVI,
                                              v_IMP_MON,
                                              v_FLG_IMP_VAL,
                                              p_ERROR);


      IF v_FLG_IMP_VAL = PKG_PAB_CONSTANTES.V_error
      THEN
         v_COD_EST_OPE := PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPVI;
         V_GLS_ADC_EST := 'Operacion Supera Monto Maximo';
      ELSE
         v_COD_EST_OPE := PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEAUT;
         V_GLS_ADC_EST := p_GLS_ADC;
      END IF;

      -- FIN VALIDACION MONTO MAXIMO

      -- VALIDACION DE BANCO DESTINO.

      p_CANAL_SAL := PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SAL_CMN;



      PKG_PAB_OPERACION.Sp_PAB_INS_OPE (
         SYSDATE,                         --Fecha de insercion de la operaci�n
         v_NUM_FOL_OPE,                      --Numero Foliador de la operacion
         0,                                     --Numero Foliador de la nomina
         0,  --Numero que tiene el numero original de la operacion relacionada
         TO_NUMBER (TRIM (p_NUM_OPE)), --Numero de operacion de la operacion en el sistema origen
         PKG_PAB_CONSTANTES.V_FLG_EGR,              --Flag de egreso e ingreso
         p_CANAL_SAL,                         -- * Codigo de sistema de salida
         p_COD_SEMAT,                           --Codigo de sistema de entrada
         v_COD_EST_OPE,                               --Estado de la operaci�n
         PKG_PAB_CONSTANTES.V_COD_TIP_ING_DIR,               --Tipo de ingreso
         v_COD_MT_SWF,                      --Numero del tipo de mensaje swift
         PKG_PAB_CONSTANTES.V_FLG_MRD_MN,                   --Flasg de mercado
         PKG_PAB_CONSTANTES.V_STR_TPO_OPE_DVP, --'DVPDCV', --p_COD_TSN   ,  --Tipo de operacion.
         v_FEC_VTA,                                     --Fecha de vencimiento
         p_COD_DVI,                                      --Codigo de la divisa
         v_IMP_MON,                                  --Importe de la operacion
         v_COD_BCO_DTN,                                --BIC del banco destino
         PKG_PAB_CONSTANTES.V_BIC_BANCO_SANTANDER,          --BIC Banco Origen
         NULL,                              --Fecha de envio / recepcion swfit
         NULL,                               --Hora de envio / recepcion swfit
         NULL,                                    --Numero de referencia Swift
         NULL,                            --Numero de referencia swift externa
         V_NUM_DOC_BFC_NUM,                      --Numero rut del beneficiario
         V_COD_TPD_BFC,                             --Rut tipo de beneficiario
         v_NOM_BNF,                                  --Nombre del Beneficiario
         --TO_NUMBER(TRIM(p_CTA_BFC))        ,  --Numero de cuenta del beneficiario
         --SUBSTR(LPAD(TRIM(p_CTA_BFC),16,'0'),-16)        ,
         FN_PAB_COR_CTA (p_CTA_BFC, v_largoCuenta),
         LPAD (TO_NUMBER (p_SUC_ORI), 4, '0'),         --Codigo de la sucursal
         V_GLS_ADC_EST,                                --Informacion adicional
         NULL,                                 --Numero de referencia contable
         NULL,                                         --BIB Banco Beneficario
         NULL,                                       --BIC Banco Intermediario
         TRIM (p_REF_EXT),                    --Numero cuenta DCV Beneficiario
         NULL,                                         --Direccion Beneficario
         NULL,                                    --Nombre ciudad beneficiario
         NULL,                                      --Codigo pais beneficiario
         v_NOM_ODN,                                         --Nombre ordenante
         V_NUM_DOC_ODN_NUM,                             --Numero rut ordenante
         V_COD_TPD_ODN,                          --Tipo documento de ordenante
         TO_NUMBER (TRIM (v_CTA_CTE_16)), --p_CTA_ORD        ,  --Numero cuenta ordenante
         p_ID_DCVORO,                            --Numero cuenta DCV ordenante
         NULL,                                           --Direccion ordenante
         NULL,                                       --Nombre ciudad ordenante
         NULL,                                         --Codigo pais ordenante
         p_CLV_NEG_01,                            --Numero de clave de negocio
         NULL,                                              --Numero de agente
         NULL,                                  --Codigo Fondo (Producto CCLV)
         NULL,                                                 --Tipo de saldo
         NULL,                                                --Tipo de camara
         NULL,                                                 --Tipo de fondo
         NULL,                                          --Fecha operacion CCLV
         NULL,         --p_CLV_NEG_01        ,  --Numero clave identificatorio
         V_GLS_ADC_EST,                           --Observacion Opcional Swift
         NULL,                               --Identificador gestor documental
         NULL,                                 --Numero de Movimiento (Vigente
         v_COD_USR,                                              --rut usuario
         p_ERROR
      );                                                                    --


      --gcp Guardamos bitacora
      PKG_PAB_TRACKING.Sp_PAB_INS_BIT_OPE (v_NUM_FOL_OPE,
                                           v_FEC_ING,
                                           v_COD_USR,
                                           v_COD_EST_OPE,
                                           v_COD_EST_OPE,
                                           v_DES_BIT,
                                           v_RESULT);


      IF v_COD_EST_OPE = PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPVI
      THEN
         --Notificaci�n del �rea Mesa
         PKG_PAB_NOTIFICACION.Sp_PAB_BUS_OPE_SIN_CBO (
            PKG_PAB_CONSTANTES.V_STR_EST_AOS_OPEPVI,
            v_FLG_NTF,
            v_CAN_OPE_NOT,
            p_ERROR
         );
         PKG_PAB_NOTIFICACION.SP_PAB_DET_CRRO_TIBCO (NULL,
                                                     NULL,
                                                     NULL,
                                                     'PVIS',
                                                     v_CAN_OPE_NOT, -- v_cont_est_vis,
                                                     NULL,    --'MESA',--NULL,
                                                     NULL,
                                                     NULL,
                                                     NULL,
                                                     p_ADJUNTO,
                                                     p_ID,
                                                     p_MENSAJE_SWF,
                                                     p_ASUNTO,
                                                     p_CORREO,
                                                     p_ERROR);
      END IF;
   EXCEPTION
      WHEN op_REG
      THEN
         p_ERR_CODE := '1';
         p_ERR_MSG :=
               'Operacion Origen N� '
            || p_COD_SEMAT
            || 'ya se encuentra en Altos Montos';
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (p_ERR_CODE,
                                             v_NOM_PCK,
                                             p_ERR_MSG,
                                             v_NOM_SP);
      WHEN DUP_VAL_ON_INDEX
      THEN
         p_ERR_CODE := '1';
         p_ERR_MSG := 'Valor ya ingresado en Altos Montos';
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (p_ERR_CODE,
                                             v_NOM_PCK,
                                             p_ERR_MSG,
                                             v_NOM_SP);
      WHEN NO_DATA_FOUND
      THEN
         p_ERR_CODE := '1';
         p_ERR_MSG := 'No se encuentra el registro asociado';
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (p_ERR_CODE,
                                             v_NOM_PCK,
                                             p_ERR_MSG,
                                             v_NOM_SP);
      WHEN OTHERS
      THEN
         p_ERR_MSG := SUBSTR (SQLERRM, 1, 300);
         p_ERR_CODE := SQLCODE;
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (p_ERR_CODE,
                                             v_NOM_PCK,
                                             p_ERR_MSG,
                                             v_NOM_SP);

         p_s_mensaje := p_ERR_MSG || '-' || p_ERR_CODE;
         RAISE_APPLICATION_ERROR (-20000, p_ERR_MSG);
   END SP_ING_PAG_DVP;

   --***********************************************************************************************
   -- Funcion/Procedimiento: SP_ING_PAG
   -- Objetivo: Procedimiento que ingresa operaciones tipo LBTR
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: SP_ACT_EST_OPE_DVP
   -- Fecha: 25/07/17
   -- Autor: Santander
   -- Input:
   -- p_NUM_DOC_BCO    -> Numero documento empresa
   -- p_COD_DVI        -> Codigo divisa
   -- p_IMP_MTO        -> Monto operacion
   -- p_FEC_ING        -> fecha de ingreso
   -- p_FEC_VTA        -> fecha de valuta
   -- p_NUM_DOC_BFN    -> numero documento beneficiario
   -- p_NOM_BFN        -> nombre beneficiario
   -- p_CTA_BFC        -> cuenta beneficiario
   -- p_NUM_DOC_ORD    -> numero documento ordenante
   -- p_NOM_ORD        -> nombre ordenante
   -- p_CTA_ORD        -> cuenta ordenante
   -- p_COD_SIS_ORI    -> Codigo sistema origen
   -- p_SUC_ORI        -> Sucursal origen
   -- p_COD_TR_PAGO    -> Codigo transaccion
   -- p_NUM_OPE        -> numero operacion
   -- p_COD_RFR        -> refenrencia externa
   -- p_REF_EXT        -> Codifo referencia
   -- p_CAN_PAG        -> Cantidad de paginas
   -- p_CTA_CTB        -> referencia externa
   -- p_GLS_ADC        -> glosa adicional
   -- Output:
   -- p_ERR_CODE       -> codigo de error de ejecucion (retorno para TIBCO)
   -- p_ERR_MSG        -> mensaje de error de ejecucion (retorno para TIBCO)
   -- p_ERROR          -> codigo de error de ejecucion de PL (para log)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de utimos cambios>
   --***********************************************************************************************
   PROCEDURE SP_ING_PAG (p_NUM_DCO_BCO   IN     VARCHAR2,
                         p_COD_DVI       IN     VARCHAR2,
                         p_IMP_MTO       IN     NUMERIC,
                         p_FEC_ING       IN     VARCHAR2,
                         p_FEC_VTA       IN     VARCHAR2,
                         p_NUM_DCO_BNF   IN     VARCHAR2,
                         p_NOM_BNF       IN     VARCHAR2,
                         p_CTA_BNF       IN     VARCHAR2,
                         p_NUM_DCO_ODN   IN     VARCHAR2,
                         p_NOM_ODN       IN     VARCHAR2,
                         p_CTA_ODN       IN     VARCHAR2,
                         p_COD_SIS_ORI   IN     VARCHAR2,
                         p_SUC_ORI       IN     NUMERIC,
                         p_COD_TR_PAGO   IN     VARCHAR2,     -- VVT - OVT ETC
                         p_NUM_OPE       IN     VARCHAR2,
                         p_COD_RFR       IN     VARCHAR2,
                         p_REF_EXT       IN     VARCHAR2,
                         p_CAN_PAG       IN     NUMERIC,
                         p_CTA_CTB       IN     VARCHAR2,
                         p_GLS_ADIC      IN     VARCHAR2,
                         p_ERR_CODE         OUT VARCHAR2,
                         p_ERR_MSG          OUT VARCHAR2,
                         p_ERROR            OUT NUMBER)
   IS
      --Seteo de Variables
      v_NOM_SP            VARCHAR2 (30) := 'SP_ING_PAG';
      v_NUM_FOL_OPE       NUMBER (12) := SPK_PAB_PAG_EAM.NEXTVAL; -- obtenemos FOLIO
      --v_fechaHoy date:= sysdate;

      v_COD_USR           VARCHAR2 (11) := 'ESB';

      v_COD_MT_SWF        NUMBER (3) := 103;              -- por ahora en duro
      v_COD_BCO_DTN       CHAR (11);
      V_NUM_DOC_BFC       CHAR (11);
      V_COD_TPD_BFC       CHAR (1);
      V_NUM_DOC_BFC_NUM   NUMBER (10);
      V_NUM_DOC_ODN       CHAR (11);
      V_COD_TPD_ODN       CHAR (1);
      V_NUM_DOC_ODN_NUM   NUMBER (10);

      v_FEC_VTA           DATE := TO_DATE (p_FEC_VTA, 'YYYY/MM/DD');

      v_CTA_CTE_16        VARCHAR2 (16);

      -- *** Variables para seccion de validacion de horario sistema y monto importe
      v_FLG_IMP_VAL       NUMBER;
      v_COD_EST_OPE       NUMBER;
      V_GLS_ADC_EST       VARCHAR2 (210);
      v_IMP_MON           NUMBER;
      v_FEC_ING           DATE := SYSDATE;
      v_DES_BIT           VARCHAR2 (80);
      v_RESULT            NUMBER;
      v_COD_TR_PAGO       VARCHAR2 (8);

      --Variables de notificaci�n.
      v_FLG_NTF           NUMBER;
      v_CAN_OPE_NOT       NUMBER;
      p_ADJUNTO           NUMBER;
      p_ID                VARCHAR2 (10);
      p_MENSAJE_SWF       VARCHAR2 (300);
      p_ASUNTO            VARCHAR2 (300);
      p_CORREO            VARCHAR2 (300);
      p_CANAL_SAL         CHAR (10);
      v_RUT_ITAU          VARCHAR2 (11) := '0076645030K';
      v_RUT_CORP          VARCHAR2 (11) := '00970230009';
      v_NUM_DCO_BCO       VARCHAR2 (11);
      op_REG EXCEPTION;
      v_VAL_OPE           INTEGER;

      v_NOM_BNF           VARCHAR2 (100);
      v_NOM_ODN           VARCHAR2 (100);
      v_PRO_NOM           VARCHAR2 (100) := 'SIN NOMBRE INFORMADO';
   BEGIN
      /*SE AGREGA VALIDACION DE OPERACIONES DESDE EL ORIGEN */

      v_VAL_OPE :=
         PKG_PAB_UTILITY.FN_PAB_BUS_VVV (TO_NUMBER (TRIM (p_NUM_OPE)));

      IF v_VAL_OPE = 1
      THEN
         RAISE op_REG;
      END IF;


      /*SE AGREGA VALIDACION DE TEXTO NO VALIDO. */

      v_NOM_BNF := PKG_PAB_UTILITY.FN_PAB_VLD_TXT (p_NOM_BNF);

      IF NVL (TRIM (v_NOM_BNF), '#') = '#'
      THEN
         v_NOM_BNF := v_PRO_NOM;
      ELSE
         v_NOM_BNF := p_NOM_BNF;
      END IF;

      v_NOM_ODN := PKG_PAB_UTILITY.FN_PAB_VLD_TXT (p_NOM_ODN);


      IF NVL (TRIM (v_NOM_ODN), '#') = '#'
      THEN
         v_NOM_ODN := v_PRO_NOM;
      ELSE
         v_NOM_ODN := p_NOM_ODN;
      END IF;


      /*SE AGREGA VALIDACION DE OPERACIONES DESDE EL ORIGEN */

      p_ERROR := PKG_PAB_CONSTANTES.v_ok;
      p_ERR_CODE := '0';
      p_ERR_MSG := 'Dato Ingresado Correctamente';
      v_DES_BIT :=
         'Operacion ingresada desde sistema : '
         || PKG_PAB_UTILITY.FN_PAB_OBT_DSC_SIS_ENT_SAL (p_COD_SIS_ORI);

      v_CTA_CTE_16 := SUBSTR (p_CTA_ODN, 9, 12);

      --CAFF: CAMBIO BIC PARA ITAHU
      IF p_NUM_DCO_BCO = v_RUT_ITAU
      THEN
         v_NUM_DCO_BCO := v_RUT_CORP;
      ELSE
         v_NUM_DCO_BCO := p_NUM_DCO_BCO;
      END IF;

      BEGIN
         SELECT   TGCDSWSA
           INTO   v_COD_BCO_DTN
           FROM   TCDT040
          WHERE   LPAD (TCNIFENT, 11, '0') = v_NUM_DCO_BCO
                  AND TGCDSWSA IS NOT NULL;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            p_ERR_CODE := SQLCODE;
            p_ERR_MSG :=
               'No se encuentra banco asociado a numero de documento : '
               || p_NUM_DCO_BCO;
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (p_ERR_CODE,
                                                v_NOM_PCK,
                                                p_ERR_MSG,
                                                v_NOM_SP);
         WHEN OTHERS
         THEN
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;

            p_ERR_MSG := SUBSTR (SQLERRM, 1, 300);
            p_ERR_CODE := SQLCODE;

            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (p_ERR_CODE,
                                                v_NOM_PCK,
                                                p_ERR_MSG,
                                                v_NOM_SP);

            p_s_mensaje := p_ERR_MSG || '-' || p_ERR_CODE;
            RAISE_APPLICATION_ERROR (-20000, p_ERR_MSG);
      END;


      -- INICIO TRANFORAMCION DE RUTS
      PKG_PAB_UTILITY.Sp_PAB_DESCOMp_RUT (p_NUM_DCO_BNF,
                                          V_NUM_DOC_BFC,
                                          V_COD_TPD_BFC);

      V_NUM_DOC_BFC_NUM := TO_NUMBER (V_NUM_DOC_BFC); -- TRANSDFORMAMOS A NUMERO PARA ELIMINAR 0 EN RUT

      p_ERR_MSG := V_NUM_DOC_BFC_NUM;

      PKG_PAB_UTILITY.Sp_PAB_DESCOMp_RUT (p_NUM_DCO_ODN,
                                          V_NUM_DOC_ODN,
                                          V_COD_TPD_ODN);

      V_NUM_DOC_ODN_NUM := TO_NUMBER (V_NUM_DOC_ODN); -- TRANSDFORMAMOS A NUMERO PARA ELIMINAR 0 EN RUT

      -- FIN TRANSFORMACION DE RUT

      -- INCIO VALIDACION MONTO MAXIMO

      v_IMP_MON := SUBSTR (p_IMP_MTO, 0, LENGTH (p_IMP_MTO) - 2);      --MONTO

      PKG_PAB_UTILITY.Sp_PAB_VAL_MONDA_ALMOT (p_COD_DVI,
                                              v_IMP_MON,
                                              v_FLG_IMP_VAL,
                                              p_ERROR);


      IF v_FLG_IMP_VAL = PKG_PAB_CONSTANTES.V_error
      THEN
         v_COD_EST_OPE := PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPVI;
         V_GLS_ADC_EST := 'Operacion Supera Monto Maximo';
      ELSE
         v_COD_EST_OPE := PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEAUT;
         V_GLS_ADC_EST := p_GLS_ADIC;
      END IF;

      -- FIN VALIDACION MONTO MAXIMO

      -- SE AGREGA PROCESO DE VALIDACION DE PRODUCTO CAJA


      /*IF (p_COD_TR_PAGO = 'VVTTGR') THEN-- IMPUESTOS

         v_COD_TR_PAGO := 'IMPTOS';
         p_CANAL_SAL := PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SAL_LBTR;

      ELSE

         v_COD_TR_PAGO := 'BCO';
         p_CANAL_SAL := PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SAL_CMN;

      END IF;*/

      -- FIN AGREGA PROCESO DE VALIDACION DE PRODUCTO CAJA


      -- Se crea procedimiento que busca Canal de Salida
      PKG_PAB_OPERACION.Sp_PAB_BUS_CANAL_SALIDA (v_COD_BCO_DTN, --p_COD_BCO_DTN,
                                                 v_COD_MT_SWF,
                                                 NULL, --p_MONTO_OPE, Campo se debe modificar
                                                 v_COD_TR_PAGO,
                                                 p_COD_DVI,
                                                 p_CANAL_SAL,
                                                 p_ERROR);

      IF (p_COD_TR_PAGO = 'VVTTGR')
      THEN                                                        -- IMPUESTOS
         v_COD_TR_PAGO := 'IMPTOS';
         p_CANAL_SAL := PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SAL_LBTR;
      END IF;

      /* IF (v_COD_BCO_DTN =  PKG_PAB_CONSTANTES.V_BIC_BANCO_SANTANDER) OR (v_COD_BCO_DTN = PKG_PAB_CONSTANTES.V_BIC_BANCO_SANTANDER_HOM) THEN

          --SIGNIFICA QUE EL BANCO DESTINO ES SANTANDER, POR LO QUE EL CANAL ES CUENTA CORRIENTE

        p_CANAL_SAL := PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SAL_CTACTE;

        /*ELSE

        p_CANAL_SAL := PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SAL_LBTR;

        END IF; */
      --

      -- VALIDACION DE CANAL DE ENTRADA PARA OPERACION INTERNA

      PKG_PAB_OPERACION.Sp_PAB_INS_OPE (
         v_FEC_ING,                       --Fecha de insercion de la operaci�n
         v_NUM_FOL_OPE,                      --Numero Foliador de la operacion
         0,                                     --Numero Foliador de la nomina
         0,  --Numero que tiene el numero original de la operacion relacionada
         TO_NUMBER (TRIM (p_NUM_OPE)), --Numero de operacion de la operacion en el sistema origen
         PKG_PAB_CONSTANTES.V_FLG_EGR,              --Flag de egreso e ingreso
         p_CANAL_SAL,                         -- * Codigo de sistema de salida
         p_COD_SIS_ORI,                         --Codigo de sistema de entrada
         v_COD_EST_OPE,                               --Estado de la operaci�n
         PKG_PAB_CONSTANTES.V_COD_TIP_ING_DIR,               --Tipo de ingreso
         v_COD_MT_SWF,                      --Numero del tipo de mensaje swift
         PKG_PAB_CONSTANTES.V_FLG_MRD_MN,                   --Flasg de mercado
         v_COD_TR_PAGO,              --p_COD_TR_PAGO   ,  --Tipo de operacion.
         v_FEC_VTA,                                     --Fecha de vencimiento
         p_COD_DVI,                                      --Codigo de la divisa
         v_IMP_MON,                                  --Importe de la operacion
         v_COD_BCO_DTN,                                --BIC del banco destino
         PKG_PAB_CONSTANTES.V_BIC_BANCO_SANTANDER,          --BIC Banco Origen
         NULL,                              --Fecha de envio / recepcion swfit
         NULL,                               --Hora de envio / recepcion swfit
         p_COD_RFR,                               --Numero de referencia Swift
         TRIM (p_REF_EXT),                --Numero de referencia swift externa
         V_NUM_DOC_BFC_NUM,                      --Numero rut del beneficiario
         V_COD_TPD_BFC,                             --Rut tipo de beneficiario
         v_NOM_BNF,        --p_NOM_BNF            ,  --Nombre del Beneficiario
         --TO_NUMBER(TRIM(p_CTA_BNF)),  --Numero de cuenta del beneficiario
         --SUBSTR(LPAD(TRIM(p_CTA_BNF),16,'0'),-16) ,
         PKG_PAB_CANALES.FN_PAB_COR_CTA (p_CTA_BNF, v_largoCuenta),
         LPAD (TO_NUMBER (p_SUC_ORI), 4, '0'),         --Codigo de la sucursal
         V_GLS_ADC_EST,                                --Informacion adicional
         0,        --TRIM(p_CTA_CTB)        ,  --Numero de referencia contable
         NULL,                                         --BIB Banco Beneficario
         NULL,                                       --BIC Banco Intermediario
         NULL,                                --Numero cuenta DCV Beneficiario
         NULL,                                         --Direccion Beneficario
         NULL,                                    --Nombre ciudad beneficiario
         NULL,                                      --Codigo pais beneficiario
         v_NOM_ODN,               --p_NOM_ODN            ,  --Nombre ordenante
         V_NUM_DOC_ODN_NUM,                             --Numero rut ordenante
         V_COD_TPD_ODN,                          --Tipo documento de ordenante
         TO_NUMBER (TRIM (v_CTA_CTE_16)),            --Numero cuenta ordenante
         NULL,                                   --Numero cuenta DCV ordenante
         NULL,                                           --Direccion ordenante
         NULL,                                       --Nombre ciudad ordenante
         NULL,                                         --Codigo pais ordenante
         NULL,                                    --Numero de clave de negocio
         NULL,                                              --Numero de agente
         NULL,                                  --Codigo Fondo (Producto CCLV)
         NULL,                                                 --Tipo de saldo
         NULL,                                                --Tipo de camara
         NULL,                                                 --Tipo de fondo
         NULL,                                          --Fecha operacion CCLV
         NULL,                                  --Numero clave identificatorio
         V_GLS_ADC_EST,                           --Observacion Opcional Swift
         NULL,                               --Identificador gestor documental
         NULL,                                 --Numero de Movimiento (Vigente
         v_COD_USR,                                              --rut usuario
         p_ERROR
      );                                                                    --



      PKG_PAB_TRACKING.Sp_PAB_INS_BIT_OPE (v_NUM_FOL_OPE,
                                           v_FEC_ING,
                                           v_COD_USR,
                                           v_COD_EST_OPE,
                                           v_COD_EST_OPE,
                                           v_DES_BIT,
                                           v_RESULT);


      IF v_COD_EST_OPE = PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPVI
      THEN
         --Notificaci�n del �rea Mesa
         PKG_PAB_NOTIFICACION.Sp_PAB_BUS_OPE_SIN_CBO (
            PKG_PAB_CONSTANTES.V_STR_EST_AOS_OPEPVI,
            v_FLG_NTF,
            v_CAN_OPE_NOT,
            p_ERROR
         );
         PKG_PAB_NOTIFICACION.SP_PAB_DET_CRRO_TIBCO (NULL,
                                                     NULL,
                                                     NULL,
                                                     'PVIS',
                                                     v_CAN_OPE_NOT, -- v_cont_est_vis,
                                                     NULL,    --'MESA',--NULL,
                                                     NULL,
                                                     NULL,
                                                     NULL,
                                                     p_ADJUNTO,
                                                     p_ID,
                                                     p_MENSAJE_SWF,
                                                     p_ASUNTO,
                                                     p_CORREO,
                                                     p_ERROR);
      END IF;
   EXCEPTION
      WHEN op_REG
      THEN
         p_ERR_CODE := '1';
         p_ERR_MSG :=
               'Operacion Origen N� '
            || TO_NUMBER (TRIM (p_NUM_OPE))
            || ' ya se encuentra en Altos Montos';
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (p_ERR_CODE,
                                             v_NOM_PCK,
                                             p_ERR_MSG,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN DUP_VAL_ON_INDEX
      THEN
         p_ERR_CODE := '1';
         p_ERR_MSG := 'Valor ya ingresado en Altos Montos';
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (p_ERR_CODE,
                                             v_NOM_PCK,
                                             p_ERR_MSG,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN NO_DATA_FOUND
      THEN
         p_ERR_CODE := '1';
         p_ERR_MSG := 'No se encuentra el registro asociado';
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (p_ERR_CODE,
                                             v_NOM_PCK,
                                             p_ERR_MSG,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;

         p_ERR_MSG := SUBSTR (SQLERRM, 1, 300);
         p_ERR_CODE := SQLCODE;


         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (p_ERR_CODE,
                                             v_NOM_PCK,
                                             p_ERR_MSG,
                                             v_NOM_SP);

         p_s_mensaje := p_ERR_MSG || '-' || p_ERR_CODE;
         RAISE_APPLICATION_ERROR (-20000, p_ERR_MSG);
   END SP_ING_PAG;

   --***********************************************************************************************
   -- Funcion/Procedimiento: SP_ING_OPE_PLZ
   -- Objetivo: Procedimiento que ingresa numero de vale vista relacionado a operacion de officebanking
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: SP_ACT_EST_OPE_DVP
   -- Fecha: 25/07/17
   -- Autor: Santander
   -- Input:
   -- p_NUM_OPE        -> numero de OfficeBankin
   -- p_NUM_OPE_ORI    -> numero VV plazo
   -- p_COD_DVI        -> codigo divisa
   -- p_IMP_MTO        -> monto operacion
   -- p_FEC_ING        -> fecha de ingreso
   -- p_CLV_NEG        -> clave de negocio
   -- Output:
   -- p_ERR_CODE       -> codigo de error de ejecucion (retorno para TIBCO)
   -- p_ERR_MSG        -> mensaje de error de ejecucion (retorno para TIBCO)
   -- p_ERROR          -> codigo de error de ejecucion de PL (para log)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de utimos cambios>
   --***********************************************************************************************
   PROCEDURE SP_ING_OPE_PLZ (p_NUM_OPE       IN     NUMBER,
                             p_NUM_OPE_ORI   IN     NUMBER,
                             p_COD_DVI       IN     CHAR,
                             p_IMP_MTO       IN     NUMBER,
                             p_FEC_ING       IN     VARCHAR2,
                             p_CLV_NEG       IN     VARCHAR2,
                             p_ERR_CODE         OUT VARCHAR2,
                             p_ERR_MSG          OUT VARCHAR2,
                             p_ERROR            OUT NUMBER)
   IS
      --Seteo de Variables
      v_NOM_SP     VARCHAR2 (30) := 'SP_ING_OPE_PLZ';
      v_fechaHoy   DATE := SYSDATE;
   BEGIN
      p_ERROR := PKG_PAB_CONSTANTES.v_ok;
      p_ERR_CODE := '0';
      p_ERR_MSG := 'Dato Ingresado Correctamente';


      INSERT INTO PABS_DT_DETLL_OPRCN_PLAZO (NUM_OPE_PZO,
                                             NUM_OPE_OFB,
                                             COD_DVI,
                                             IMP_MTO,
                                             FEC_ING_OPE_PZO,
                                             NUM_CLV_NGC)
        VALUES   (p_NUM_OPE,
                  p_NUM_OPE_ORI,
                  p_COD_DVI,
                  p_IMP_MTO,
                  v_fechaHoy,
                  p_CLV_NEG);
   EXCEPTION
      WHEN DUP_VAL_ON_INDEX
      THEN
         p_ERR_CODE := SQLCODE;
         p_ERR_MSG := 'Operacion ya existe en AAMM en Altos Montos';
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END SP_ING_OPE_PLZ;

   --***********************************************************************************************
   -- Funcion/Procedimiento: SP_INS_LBTR
   -- Objetivo: Procedimiento que ingresa operacione desde sistema Custodia
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: SP_ACT_EST_OPE_DVP
   -- Fecha: 25/07/17
   -- Autor: Santander
   -- Input:
   -- p_NUM_DOC_BCO_DTN    -> Numero de documento destino
   -- p_COD_DVI            -> codigo divisa
   -- p_IMP_MTO            -> monto
   -- p_FEC_ING_PAG        -> fecha ingreso operacion
   -- p_FEC_VAL            -> fecha de valuta
   -- p_NUM_DOC_BNF        -> numero de documento beneficiario
   -- p_NOM_BNF            -> nombre beneficiario
   -- p_CTA_BNF            -> cuenta beneficiario
   -- p_NUM_DOC_ORD        -> numero documento ordenante
   -- p_NOM_ORD            -> nombre ordenante
   -- p_CTA_ORD            -> cuenta ordenante
   -- p_COD_ORI            -> codigo origen
   -- p_SUC_ORI            -> sucursal origen
   -- p_COD_TR_PA          -> codigo transaccion
   -- p_NUM_OPE            -> numero operacion origen
   -- p_COD_REF            -> codigo referencia
   -- p_REF_EXT            -> referencia externa
   -- p_CAN_CAN            -> cantidad de operaciones
   -- p_CTA_CNT            -> cuena contable
   -- p_COD_PAR            -> Codigo de pago
   -- p_GLO_GLO            -> Glosa operacion
   -- p_soma_01            -> clave negocio 1
   -- p_soma_02            -> clave negocio 2
   -- p_soma_03            -> clave negocio 3
   -- p_soma_04            -> clave negocio 4
   -- p_soma_05            -> clave negocio 5
   -- p_soma_06            -> clave negocio 6
   -- p_soma_07            -> clave negocio 7
   -- p_soma_08            -> clave negocio 8
   -- p_soma_09            -> clave negocio 9
   -- p_soma_10            -> clave negocio 10
   -- Output:
   -- p_ERR_CODE       -> codigo de error de ejecucion (retorno para TIBCO)
   -- p_ERR_MSG        -> mensaje de error de ejecucion (retorno para TIBCO)
   -- p_ERROR          -> codigo de error de ejecucion de PL (para log)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de utimos cambios>
   --***********************************************************************************************
   PROCEDURE SP_INS_LBTR (p_NUM_DOC_BCO_DTN   IN     VARCHAR2,
                          p_COD_DVI           IN     CHAR,
                          p_IMP_MTO           IN     NUMERIC,
                          p_FEC_ING_PAG       IN     NUMERIC,
                          p_FEC_VAL           IN     VARCHAR2,
                          p_NUM_DOC_BNF       IN     VARCHAR2,
                          p_NOM_BNF           IN     VARCHAR2,
                          p_CTA_BNF           IN     VARCHAR2,
                          p_NUM_DOC_ORD       IN     VARCHAR2,
                          p_NOM_ORD           IN     VARCHAR2,
                          p_CTA_ORD           IN     VARCHAR2,
                          p_COD_ORI           IN     VARCHAR2,
                          p_SUC_ORI           IN     VARCHAR2,
                          p_COD_TR_PA         IN     VARCHAR2,
                          p_NUM_OPE           IN     CHAR,
                          p_COD_REF           IN     VARCHAR2,
                          p_REF_EXT           IN     VARCHAR2,
                          p_CAN_CAN           IN     VARCHAR2,
                          p_CTA_CNT           IN     CHAR,
                          p_COD_PAR           IN     VARCHAR2,
                          p_GLO_GLO           IN     VARCHAR2,
                          p_soma_01           IN     VARCHAR2,
                          p_soma_02           IN     VARCHAR2,
                          p_soma_03           IN     VARCHAR2,
                          p_soma_04           IN     VARCHAR2,
                          p_soma_05           IN     VARCHAR2,
                          p_soma_06           IN     VARCHAR2,
                          p_soma_07           IN     VARCHAR2,
                          p_soma_08           IN     VARCHAR2,
                          p_soma_09           IN     VARCHAR2,
                          p_soma_10           IN     VARCHAR2,
                          p_ERR_CODE             OUT VARCHAR2,
                          p_ERR_MSG              OUT VARCHAR2,
                          p_ERROR                OUT NUMBER)
   IS
      v_COD_USR           VARCHAR2 (11) := 'ESB';
      v_COD_MT_SWF        NUMBER (3) := PKG_PAB_CONSTANTES.V_COD_MT103; -- por ahora en duro
      v_COD_BCO_DTN       CHAR (11);
      V_NUM_DOC_BFC       CHAR (11);
      V_COD_TPD_BFC       CHAR (1);
      V_NUM_DOC_BFC_NUM   NUMBER (10);
      V_NUM_DOC_ODN       CHAR (11);
      V_COD_TPD_ODN       CHAR (1);
      V_NUM_DOC_ODN_NUM   NUMBER (10);

      v_FEC_VTA           DATE := TO_DATE (p_FEC_VAL, 'YYYY/MM/DD');
      v_NUM_FOL_OPE       NUMBER (12) := SPK_PAB_PAG_EAM.NEXTVAL; -- obtenemos FOLIO

      -- *** Variables para seccion de validacion de horario sistema y monto importe
      v_FLG_IMP_VAL       NUMBER;
      v_COD_EST_OPE       NUMBER;
      V_GLS_ADC_EST       VARCHAR2 (210);
      v_IMP_MON           NUMBER;
      v_VAR_CAN           VARCHAR2 (50);
      v_VAR_NUM           NUMBER;

      --Variables de notificaci�n.
      v_FLG_NTF           NUMBER;
      v_CAN_OPE_NOT       NUMBER;
      p_ADJUNTO           NUMBER;
      p_ID                VARCHAR2 (10);
      p_MENSAJE_SWF       VARCHAR2 (300);
      p_ASUNTO            VARCHAR2 (300);
      p_CORREO            VARCHAR2 (300);
      p_CANAL_SAL         CHAR (10);

      v_RUT_ITAU          VARCHAR2 (11) := '0076645030K';
      v_RUT_CORP          VARCHAR2 (11) := '00970230009';
      v_NUM_DCO_BCO       VARCHAR2 (11);

      v_FEC_ING           DATE := SYSDATE;

      v_NOM_BNF           VARCHAR2 (100);
      v_NOM_ODN           VARCHAR2 (100);
      v_PRO_NOM           VARCHAR2 (100) := 'SIN NOMBRE INFORMADO';

      op_REG EXCEPTION;
      v_VAL_OPE           INTEGER;
   BEGIN
      /*SE AGREGA VALIDACION DE OPERACIONES DESDE EL ORIGEN */

      v_VAL_OPE :=
         PKG_PAB_UTILITY.FN_PAB_BUS_VVV (TO_NUMBER (TRIM (p_COD_ORI)));

      IF v_VAL_OPE = 1
      THEN
         RAISE op_REG;
      END IF;

      /*SE AGREGA VALIDACION DE TEXTO NO VALIDO. */

      v_NOM_BNF := PKG_PAB_UTILITY.FN_PAB_VLD_TXT (p_NOM_BNF);

      IF NVL (TRIM (v_NOM_BNF), '#') = '#'
      THEN
         v_NOM_BNF := v_PRO_NOM;
      ELSE
         v_NOM_BNF := p_NOM_BNF;
      END IF;

      v_NOM_ODN := PKG_PAB_UTILITY.FN_PAB_VLD_TXT (p_NOM_ORD);


      IF NVL (TRIM (v_NOM_ODN), '#') = '#'
      THEN
         v_NOM_ODN := v_PRO_NOM;
      ELSE
         v_NOM_ODN := p_NOM_ORD;
      END IF;


      p_ERROR := PKG_PAB_CONSTANTES.v_ok;
      p_ERR_CODE := '0';
      p_ERR_MSG := 'Dato Ingresado Correctamente';

      IF p_NUM_DOC_BCO_DTN = v_RUT_ITAU
      THEN
         v_NUM_DCO_BCO := v_RUT_CORP;
      ELSE
         v_NUM_DCO_BCO := p_NUM_DOC_BCO_DTN;
      END IF;

      BEGIN
         SELECT   TGCDSWSA
           INTO   v_COD_BCO_DTN
           FROM   TCDT040
          WHERE   LPAD (TCNIFENT, 11, '0') = v_NUM_DCO_BCO
                  AND TGCDSWSA IS NOT NULL;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            p_ERR_CODE := SQLCODE;
            p_ERR_MSG :=
               'No se encuentra banco asociado a numero de documento : '
               || p_NUM_DOC_BCO_DTN;

            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (p_ERR_CODE,
                                                v_NOM_PCK,
                                                p_ERR_MSG,
                                                v_NOM_SP);

            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         WHEN OTHERS
         THEN
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;

            p_ERR_MSG := SUBSTR (SQLERRM, 1, 300);
            p_ERR_CODE := SQLCODE;

            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (p_ERR_CODE,
                                                v_NOM_PCK,
                                                p_ERR_MSG,
                                                v_NOM_SP);

            p_s_mensaje := p_ERR_MSG || '-' || p_ERR_CODE;
            RAISE_APPLICATION_ERROR (-20000, p_ERR_MSG);
      END;

      -- FALTAN VALIDACIONES


      -- INICIO TRANFORAMCION DE RUTS
      PKG_PAB_UTILITY.Sp_PAB_DESCOMp_RUT (p_NUM_DOC_BNF,
                                          V_NUM_DOC_BFC,
                                          V_COD_TPD_BFC);

      V_NUM_DOC_BFC_NUM := TO_NUMBER (V_NUM_DOC_BFC); -- TRANSDFORMAMOS A NUMERO PARA ELIMINAR 0 EN RUT

      p_ERR_MSG := V_NUM_DOC_BFC_NUM;

      PKG_PAB_UTILITY.Sp_PAB_DESCOMp_RUT (p_NUM_DOC_ORD,
                                          V_NUM_DOC_ODN,
                                          V_COD_TPD_ODN);

      V_NUM_DOC_ODN_NUM := TO_NUMBER (V_NUM_DOC_ODN); -- TRANSDFORMAMOS A NUMERO PARA ELIMINAR 0 EN RUT

      -- FIN TRANSFORMACION DE RUT

      -- INCIO VALIDACION MONTO MAXIMO
      v_IMP_MON := SUBSTR (p_IMP_MTO, 0, LENGTH (p_IMP_MTO) - 2);      --MONTO

      PKG_PAB_UTILITY.Sp_PAB_VAL_MONDA_ALMOT (p_COD_DVI,
                                              v_IMP_MON,
                                              v_FLG_IMP_VAL,
                                              p_ERROR);


      IF v_FLG_IMP_VAL = PKG_PAB_CONSTANTES.V_error
      THEN
         v_COD_EST_OPE := PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPVI;
         V_GLS_ADC_EST := 'Operacion Supera Monto Maximo';
      ELSE
         v_COD_EST_OPE := PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEAUT;
         V_GLS_ADC_EST := p_GLO_GLO;
      END IF;

      -- FIN VALIDACION MONTO MAXIMO


      -- VALIDACION BANCO DESTINO

      IF (v_COD_BCO_DTN = PKG_PAB_CONSTANTES.V_BIC_BANCO_SANTANDER)
         OR (v_COD_BCO_DTN = PKG_PAB_CONSTANTES.V_BIC_BANCO_SANTANDER_HOM)
      THEN
         --SIGNIFICA QUE EL BANCO DESTINO ES SANTANDER, POR LO QUE EL CANAL ES CUENTA CORRIENTE

         p_CANAL_SAL := PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SAL_CTACTE;
      ELSE
         p_CANAL_SAL := PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SAL_LBTR;
      END IF;

      PKG_PAB_OPERACION.Sp_PAB_INS_OPE (
         SYSDATE,                         --Fecha de insercion de la operaci�n
         v_NUM_FOL_OPE,                      --Numero Foliador de la operacion
         0,                                     --Numero Foliador de la nomina
         0,  --Numero que tiene el numero original de la operacion relacionada
         TO_NUMBER (TRIM (p_NUM_OPE)), --Numero de operacion de la operacion en el sistema origen
         PKG_PAB_CONSTANTES.V_FLG_EGR,              --Flag de egreso e ingreso
         p_CANAL_SAL,                         -- * Codigo de sistema de salida
         p_COD_ORI,                             --Codigo de sistema de entrada
         v_COD_EST_OPE,                               --Estado de la operaci�n
         PKG_PAB_CONSTANTES.V_COD_TIP_ING_DIR,               --Tipo de ingreso
         v_COD_MT_SWF,                      --Numero del tipo de mensaje swift
         PKG_PAB_CONSTANTES.V_FLG_MRD_MN,                   --Flasg de mercado
         p_COD_REF,                    --p_COD_TR_PA   ,  --Tipo de operacion.
         v_FEC_VTA,                                     --Fecha de vencimiento
         p_COD_DVI,                                      --Codigo de la divisa
         v_IMP_MON,                                  --Importe de la operacion
         v_COD_BCO_DTN,                                --BIC del banco destino
         PKG_PAB_CONSTANTES.V_BIC_BANCO_SANTANDER,          --BIC Banco Origen
         NULL,                              --Fecha de envio / recepcion swfit
         NULL,                               --Hora de envio / recepcion swfit
         NULL,              --p_COD_REF        ,  --Numero de referencia Swift
         TRIM (p_REF_EXT),                --Numero de referencia swift externa
         V_NUM_DOC_BFC_NUM,                      --Numero rut del beneficiario
         V_COD_TPD_BFC,                             --Rut tipo de beneficiario
         v_NOM_BNF,                                  --Nombre del Beneficiario
         --SUBSTR(LPAD(TRIM(p_CTA_BNF),16,'0'),-16)      ,  --Numero de cuenta del beneficiario
         PKG_PAB_CANALES.FN_PAB_COR_CTA (p_CTA_BNF, v_largoCuenta),
         LPAD (TO_NUMBER (p_SUC_ORI), 4, '0'),         --Codigo de la sucursal
         p_GLO_GLO,                                    --Informacion adicional
         p_CTA_CNT,                            --Numero de referencia contable
         NULL,                                         --BIB Banco Beneficario
         NULL,                                       --BIC Banco Intermediario
         NULL,                                --Numero cuenta DCV Beneficiario
         NULL,                                         --Direccion Beneficario
         NULL,                                    --Nombre ciudad beneficiario
         NULL,                                      --Codigo pais beneficiario
         v_NOM_ODN,                                         --Nombre ordenante
         V_NUM_DOC_ODN_NUM,                             --Numero rut ordenante
         V_COD_TPD_ODN,                          --Tipo documento de ordenante
         p_CTA_ORD,                                  --Numero cuenta ordenante
         NULL,                                   --Numero cuenta DCV ordenante
         NULL,                                           --Direccion ordenante
         NULL,                                       --Nombre ciudad ordenante
         NULL,                                         --Codigo pais ordenante
         NULL,              --p_soma_01        ,  --Numero de clave de negocio
         NULL,                                              --Numero de agente
         NULL,                                  --Codigo Fondo (Producto CCLV)
         NULL,                                                 --Tipo de saldo
         NULL,                                                --Tipo de camara
         NULL,                                                 --Tipo de fondo
         NULL,                                          --Fecha operacion CCLV
         p_soma_01,                             --Numero clave identificatorio
         p_GLO_GLO,                               --Observacion Opcional Swift
         NULL,                               --Identificador gestor documental
         NULL,                                 --Numero de Movimiento (Vigente
         v_COD_USR,                                              --rut usuario
         p_ERROR
      );                                                                    --



      IF v_COD_EST_OPE = PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPVI
      THEN
         --Notificaci�n del �rea Mesa
         PKG_PAB_NOTIFICACION.Sp_PAB_BUS_OPE_SIN_CBO (
            PKG_PAB_CONSTANTES.V_STR_EST_AOS_OPEPVI,
            v_FLG_NTF,
            v_CAN_OPE_NOT,
            p_ERROR
         );
         PKG_PAB_NOTIFICACION.SP_PAB_DET_CRRO_TIBCO (NULL,
                                                     NULL,
                                                     NULL,
                                                     'PVIS',
                                                     v_CAN_OPE_NOT, -- v_cont_est_vis,
                                                     NULL,    --'MESA',--NULL,
                                                     NULL,
                                                     NULL,
                                                     NULL,
                                                     p_ADJUNTO,
                                                     p_ID,
                                                     p_MENSAJE_SWF,
                                                     p_ASUNTO,
                                                     p_CORREO,
                                                     p_ERROR);
      END IF;
   EXCEPTION
      WHEN op_REG
      THEN
         p_ERR_CODE := '1';
         p_ERR_MSG :=
               'Operacion Origen N� '
            || p_COD_ORI
            || 'ya se encuentra en Altos Montos';
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (p_ERR_CODE,
                                             v_NOM_PCK,
                                             p_ERR_MSG,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN DUP_VAL_ON_INDEX
      THEN
         p_ERR_MSG := 'Valor ya ingresado en Altos Montos';
         p_ERR_CODE := '1';
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;

         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (p_ERR_CODE,
                                             v_NOM_PCK,
                                             p_ERR_MSG,
                                             v_NOM_SP);
      WHEN NO_DATA_FOUND
      THEN
         p_ERR_MSG := 'No se encuentra el registro asociado';
         p_ERR_CODE := '1';
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;

         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (p_ERR_CODE,
                                             v_NOM_PCK,
                                             p_ERR_MSG,
                                             v_NOM_SP);
      WHEN OTHERS
      THEN
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;

         p_ERR_MSG := SUBSTR (SQLERRM, 1, 300);
         p_ERR_CODE := SQLCODE;

         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (p_ERR_CODE,
                                             v_NOM_PCK,
                                             p_ERR_MSG,
                                             v_NOM_SP);

         p_s_mensaje := p_ERR_MSG || '-' || p_ERR_CODE;
         RAISE_APPLICATION_ERROR (-20000, p_ERR_MSG);
   END SP_INS_LBTR;

   --***********************************************************************************************
   -- Funcion/Procedimiento: SP_OBT_MSJ_SWF_HST
   -- Objetivo: Procedimiento que ingresa operacione desde sistema Custodia
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: SP_ACT_EST_OPE_DVP
   -- Fecha: 25/07/17
   -- Autor: Santander
   -- Input:
   --.p_NUM_OPE        -> Numero de operacion
   -- Output:
   -- p_NUM_IDF_GTR_DOC-> numero identificatorio gestor documental
   -- p_ERR_CODE       -> codigo de error de ejecucion (retorno para TIBCO)
   -- p_ERR_MSG        -> mensaje de error de ejecucion (retorno para TIBCO)
   -- p_ERROR          -> codigo de error de ejecucion de PL (para log)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de utimos cambios>
   --***********************************************************************************************
   PROCEDURE SP_OBT_MSJ_SWF_HST (p_NUM_OPE           IN     VARCHAR2,
                                 p_NUM_IDF_GTR_DOC      OUT VARCHAR2,
                                 p_ERR_CODE             OUT VARCHAR2,
                                 p_ERR_MSG              OUT VARCHAR2,
                                 p_ERROR                OUT NUMBER)
   IS
      --Seteo de Variables
      v_NOM_SP            VARCHAR2 (30) := 'SP_OBT_MSJ_SWF_HST';
      v_fechaHoy          DATE := SYSDATE;
      v_NUM_OPE           CHAR (20) := p_NUM_OPE;
      v_NUM_OPE_ORI       CHAR (20);
      v_NUM_OPE_ORI_NUM   NUMBER;
   BEGIN
      p_ERROR := PKG_PAB_CONSTANTES.v_ok;
      p_ERR_CODE := '0';
      p_ERR_MSG := 'Dato Encontrado!';


      BEGIN
         --BUSCO NUMERO DE VV DE ACUERDO A NUMERO DE OPERACION OB
         SELECT   TO_NUMBER (TRIM (SUBSTR (NUM_OPE_OFB, -12)))
           INTO   v_NUM_OPE_ORI
           FROM   PABS_DT_DETLL_OPRCN_PLAZO
          WHERE   NUM_OPE_PZO = p_NUM_OPE;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            p_ERR_MSG :=
               'Operacion no se encuenta en Altos Montos. ' || p_NUM_OPE;
            p_ERR_CODE := 2;
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         WHEN OTHERS
         THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            p_s_mensaje := err_code || '-' || err_msg;
            RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
      END;


      BEGIN
         --BUSCO ID DEL GESTSOR
         SELECT   NUM_IDF_GTR_DOC
           INTO   p_NUM_IDF_GTR_DOC
           FROM   PABS_DT_DETLL_OPRCN
          WHERE   NUM_OPE_SIS_ENT = v_NUM_OPE_ORI;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            p_ERR_MSG :=
               'Operacion no se encuenta en Altos Montos. ' || p_NUM_OPE;
            p_ERR_CODE := 2;
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         WHEN OTHERS
         THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            p_s_mensaje := err_code || '-' || err_msg;
            RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
      END;
   EXCEPTION
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END SP_OBT_MSJ_SWF_HST;

   /************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_BUS_FLG_MT_TPO
   -- Objetivo: Procedimiento almacenado que Flag segun MT y tipo de OperaCion
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_BIBLI_CAMPO_OBLTO_MT
   --
   -- Fecha: 16/06/17
   -- Autor: Santander
   -- Input: p_COD_MT_SWF      Tipo MT Swift
   --        p_COD_TPO_OPE_AOS Tipo de Operacion
   --         p_FLG_MRD_UTZ_SWF     Tipo Mercado
   -- Output:
   --        p_CURSOR : SYS_REFCURSOR, resultados de consulta.
   --        p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE Sp_PAB_BUS_FLG_MT_TPO (p_COD_MT_SWF        IN     NUMBER,
                                    p_COD_TPO_OPE_AOS   IN     CHAR,
                                    p_FLG_MRD_UTZ_SWF   IN     NUMBER,
                                    p_CURSOR               OUT SYS_REFCURSOR,
                                    p_ERROR                OUT NUMBER)
   IS
      v_NOM_SP            VARCHAR2 (30) := 'Sp_PAB_BUS_FLG_MT_TPO';
      v_COD_MT_SWF        NUMBER (3) := p_COD_MT_SWF;
      v_COD_TPO_OPE_AOS   CHAR (8) := p_COD_TPO_OPE_AOS;
      v_FLG_MRD_UTZ_SWF   NUMBER (2) := p_FLG_MRD_UTZ_SWF;
   BEGIN
      OPEN p_CURSOR FOR
         SELECT   ETR.NOM_TAG_CAM_ARC AS NOM_CAM_TAG,
                  BI.COD_CAM_ARC AS COD_CAM_ARC
           FROM   PABS_DT_BIBLI_CAMPO_OBLTO_MT BI,
                  PABS_DT_ESTRT_ARCHV_ENTRD ETR
          WHERE       BI.COD_MT_SWF = v_COD_MT_SWF
                  AND BI.COD_TPO_OPE_AOS = v_COD_TPO_OPE_AOS
                  AND BI.FLG_MRD_UTZ_SWF = v_FLG_MRD_UTZ_SWF
                  AND BI.COD_CAM_ARC = ETR.COD_CAM_ARC;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg :=
               'No se encuentran los campos obligatorios para MENSAJE '
            || p_COD_MT_SWF
            || ' - MERCADO '
            || p_FLG_MRD_UTZ_SWF
            || ' - CODIGO OPERACION '
            || v_COD_TPO_OPE_AOS;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);

         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;

         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END Sp_PAB_BUS_FLG_MT_TPO;

   /************************************************************************************************
   -- Funcion/Procedimiento: Sp_INS_OPE_DCV_AUT
   -- Objetivo: Procedimiento que ingresa operaciones del DCV automaticos
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_BIBLI_CAMPO_OBLTO_MT
   --
   -- Fecha: 16/06/17
   -- Autor: SII GROUP
   -- Input:
   -- p_NUM_IDF_SWF        -> Numero identificador de swift
   -- p_NUM_SSN_SWF        -> Numero de sesion de swift
   -- p_FEC_ISR_MSJ_SWF    -> numero identificador del mensaje
   -- p_NUM_REF_IDE_DCV    -> numero referencia DCV
   -- p_NUM_MSJ_SWF_RLD    -> numero referencia relacionada
   -- p_FEC_RCP_MSJ        -> numero recepcion mensaje
   -- p_COD_DVI            -> codigo divisa
   -- p_IMP_MTO            -> monto operacion
   -- p_NUM_CTA_ODN        -> numero cuenta ordenante
   -- p_NUM_DOC_ODN        -> numero documento ordenante
   -- p_NUM_CTA_DCV_ODN    -> numero de cuenta dcv ordenante
   -- p_NOM_ODN            -> nombre ordenante
   -- p_COD_BCO_DTN        -> codigo banco destino (BIC)
   -- p_NUM_CTA_BFC        -> numero cuenta beneficiario
   -- p_NUM_DOC_BFC        -> numero documento beneficiario
   -- p_NUM_CTA_DCV_BFC    -> numero cuenta DCV beneficiario
   -- p_NOM_BFC            -> nombre beneficiario
   -- p_NUM_CLV_NGC        -> clave de negocio
   -- p_COD_EST_OFB        -> codifo estado operacion ofice
   -- p_COD_OPE_RSP_MPT    -> codigo de respuesta proceso
   -- p_GLS_OPE_RSP_MPT    -> glosa respuesta proceso
   -- Output:
   -- p_ERROR              -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/

   PROCEDURE Sp_INS_OPE_DCV_AUT (p_NUM_IDF_SWF       IN     NUMBER,
                                 p_NUM_SSN_SWF       IN     NUMBER,
                                 p_FEC_ISR_MSJ_SWF   IN     DATE,
                                 p_NUM_REF_IDE_DCV   IN     VARCHAR2,
                                 p_NUM_MSJ_SWF_RLD   IN     VARCHAR2,
                                 p_FEC_RCP_MSJ       IN     DATE,
                                 p_COD_DVI           IN     CHAR,
                                 p_IMP_MTO           IN     NUMBER,
                                 p_NUM_CTA_ODN       IN     VARCHAR2,
                                 p_NUM_DOC_ODN       IN     CHAR,
                                 p_NUM_CTA_DCV_ODN   IN     VARCHAR2,
                                 p_NOM_ODN           IN     VARCHAR2,
                                 p_COD_BCO_DTN       IN     VARCHAR2,
                                 p_NUM_CTA_BFC       IN     VARCHAR2,
                                 p_NUM_DOC_BFC       IN     CHAR,
                                 p_NUM_CTA_DCV_BFC   IN     VARCHAR2,
                                 p_NOM_BFC           IN     VARCHAR2,
                                 p_NUM_CLV_NGC       IN     VARCHAR2,
                                 p_COD_EST_OFB       IN     CHAR,
                                 p_COD_OPE_RSP_MPT   IN     CHAR,
                                 p_GLS_OPE_RSP_MPT   IN     VARCHAR2,
                                 p_ERROR                OUT NUMBER)
   IS
      --Seteo de Variables
      v_NOM_SP            VARCHAR2 (30) := 'Sp_INS_OPE_DCV_AUT';
      v_COD_EST_OFB       CHAR (4) := 'OK';                      -- CARGADA OK
      v_COD_OPE_RSP_MPT   CHAR (4) := 40;            -- CORRECTAMENTE DESDE SW
      v_GLS_OPE_RSP_MPT VARCHAR2 (50)
            := 'Cargado correctamente desde Swift' ;
      v_NUM_DOC_ODN       CHAR (11);
      v_NUM_REF_IDE_DCV   CHAR (15); -- SE DEBE LIMITAR EL LARGO DE LA OPERACION A 15
      v_COD_BCO_DTN       CHAR (11);
      v_NUM_CTA_DCV_ODN   VARCHAR2 (30);
      v_NUM_CTA_DCV_BFC   VARCHAR2 (30);
      v_NOM_BFC           VARCHAR2 (100);
   BEGIN
      v_NUM_DOC_ODN := LPAD (TRIM (p_NUM_DOC_ODN), 11, '0');
      v_NUM_REF_IDE_DCV := SUBSTR (TRIM (p_NUM_REF_IDE_DCV), -6);
      v_COD_BCO_DTN :=
         REPLACE (REPLACE (p_COD_BCO_DTN, CHR (10), ''), CHR (13), '');

      v_NUM_CTA_DCV_ODN :=
         REPLACE (REPLACE (p_NUM_CTA_DCV_ODN, CHR (10), ''), CHR (13), '');
      v_NUM_CTA_DCV_BFC :=
         REPLACE (REPLACE (p_NUM_CTA_DCV_BFC, CHR (10), ''), CHR (13), '');
      v_NOM_BFC := REPLACE (REPLACE (p_NOM_BFC, CHR (10), ''), CHR (13), '');


      err_code := '1';
      err_msg := 'el banco con espacios es ' || v_COD_BCO_DTN || 'informado';
      PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                          v_NOM_PCK,
                                          err_msg,
                                          v_NOM_SP);

      INSERT INTO PABS_DT_DETLL_OPRCN_DCV (NUM_IDF_SWF,
                                           NUM_SSN_SWF,
                                           FEC_ISR_MSJ_SWF,
                                           NUM_REF_IDE_DCV,
                                           NUM_MSJ_SWF_RLD,
                                           FEC_RCP_MSJ,
                                           COD_DVI,
                                           IMP_MTO,
                                           NUM_CTA_ODN,
                                           NUM_DOC_ODN,
                                           NUM_CTA_DCV_ODN,
                                           NOM_ODN,
                                           COD_BCO_DTN,
                                           NUM_CTA_BFC,
                                           NUM_DOC_BFC,
                                           NUM_CTA_DCV_BFC,
                                           NOM_BFC,
                                           NUM_CLV_NGC,
                                           COD_EST_OFB,
                                           COD_OPE_RSP_MPT,
                                           GLS_OPE_RSP_MPT)
        VALUES   (p_NUM_SSN_SWF,
                  p_NUM_IDF_SWF,
                  p_FEC_ISR_MSJ_SWF,
                  v_NUM_REF_IDE_DCV,
                  p_NUM_MSJ_SWF_RLD,
                  p_FEC_RCP_MSJ,
                  p_COD_DVI,
                  p_IMP_MTO,
                  p_NUM_CTA_ODN,
                  v_NUM_DOC_ODN,
                  p_NUM_CTA_DCV_ODN,
                  p_NOM_ODN,
                  v_COD_BCO_DTN,
                  p_NUM_CTA_BFC,
                  p_NUM_DOC_BFC,
                  p_NUM_CTA_DCV_BFC,
                  p_NOM_BFC,
                  p_NUM_CLV_NGC,
                  v_COD_EST_OFB,
                  v_COD_OPE_RSP_MPT,
                  v_GLS_OPE_RSP_MPT);
   EXCEPTION
      WHEN DUP_VAL_ON_INDEX
      THEN
         err_code := SQLCODE;
         err_msg := 'Operacio ya registrada en Altos Montos';
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         p_s_mensaje := err_code || '-' || err_msg;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END Sp_INS_OPE_DCV_AUT;

   /************************************************************************************************
   -- Funcion/Procedimiento: Sp_BUS_CAM_OBT_SWF
   -- Objetivo: Procedimiento que ingresa operaciones del DCV automaticos
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_BIBLI_CAMPO_OBLTO_MT
   --
   -- Fecha: 16/06/17
   -- Autor: SII GROUP
   -- Input:
   -- p_COD_MT_SWF         -> Codigo MT a validar
   -- p_FLG_MRD_UTZ_SWF    -> flag de mercado del mensaje a validar
   -- p_COD_TPO_OPE_AOS    -> codigo operacion a validar
   -- Output:
   -- p_CAM_OBT            -> Retorna campos a validar.
   -- p_ERROR              -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE Sp_BUS_CAM_OBT_SWF (p_COD_MT_SWF        IN     NUMBER,
                                 p_FLG_MRD_UTZ_SWF   IN     NUMBER,
                                 p_COD_TPO_OPE_AOS   IN     VARCHAR2,
                                 p_CAM_OBT              OUT SYS_REFCURSOR,
                                 p_ERROR                OUT NUMBER)
   IS
      v_NOM_SP            VARCHAR2 (30) := 'Sp_BUS_CAM_OBT_SWF';
      v_fechaHoy          DATE := SYSDATE;
      v_COD_TPO_OPE_AOS   CHAR (8) := p_COD_TPO_OPE_AOS;
   BEGIN
      OPEN p_CAM_OBT FOR
         SELECT   TAG.NOM_TAG_CAM_ARC, BIB.COD_CAM_ARC
           FROM   PABS_DT_ESTRT_ARCHV_ENTRD TAG,
                  PABS_DT_BIBLI_CAMPO_OBLTO_MT BIB
          WHERE       TAG.COD_CAM_ARC = BIB.COD_CAM_ARC
                  AND BIB.COD_MT_SWF = p_COD_MT_SWF
                  AND BIB.FLG_MRD_UTZ_SWF = p_FLG_MRD_UTZ_SWF
                  AND BIB.COD_TPO_OPE_AOS = v_COD_TPO_OPE_AOS
                  AND BIB.FLG_VGN = 1;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg :=
               'No se encuentran los campos obligatorios para MENSAJE '
            || p_COD_MT_SWF
            || ' - MERCADO '
            || p_FLG_MRD_UTZ_SWF
            || ' - CODIGO OPERACION '
            || v_COD_TPO_OPE_AOS;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;

         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);

         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);

         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END Sp_BUS_CAM_OBT_SWF;

   /************************************************************************************************
   -- Funcion/Procedimiento: Sp_BUS_MSJ_OBT_SWF
   -- Objetivo: Procedimiento que busca los mensajes a validar
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_BIBLI_CAMPO_OBLTO_MT
   --
   -- Fecha: 16/06/17
   -- Autor: SII GROUP
   -- Input: N/A
   -- Output:
   -- p_MSJ_OBT            -> Cursor de retorno de mensajes a validar.
   -- p_ERROR              -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE Sp_BUS_MSJ_OBT_SWF (p_MSJ_OBT   OUT SYS_REFCURSOR,
                                 p_ERROR     OUT NUMBER)
   IS
      v_NOM_SP   VARCHAR2 (30) := 'Sp_BUS_MSJ_OBT_SWF';
   BEGIN
      OPEN p_MSJ_OBT FOR
         SELECT   COD_MT_SWF, COD_TPO_OPE_AOS, FLG_MRD_UTZ_SWF
           FROM   PABS_DT_BIBLI_CAMPO_OBLTO_MT
          WHERE   COD_CAM_ARC = 1;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg := 'No se encuentran los campos por mensajeria disponibles';
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         p_s_mensaje := err_code || '-' || err_msg;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END Sp_BUS_MSJ_OBT_SWF;

   /************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_GES_OPE_AGP
   -- Objetivo: Procedimiento que inserta la informacion que se encuentra en la nomina
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_DETLL_OPRCN
   -- Fecha: 02/06/16
   -- Autor: Santander
   -- Input:     p_FEC_ISR_OPE     -> Fecha de insercion de la operaci�n
   --            p_NUM_FOL_OPE     -> Numero Foliador de la operacion
   --            p_NUM_FOL_NMN     -> Numero Foliador de la nomina
   --            p_NUM_FOL_OPE_ORG -> Numero que tiene el numero original de la operacion relacionada
   --            p_NUM_OPE_SIS_ENT -> Numero de operacion de la operacion en el sistema origen
   --            p_FLG_EGR_ING     -> Flag de egreso e ingreso
   --            p_COD_SIS_SAL     -> Codigo de sistema de salida
   --            p_COD_SIS_ENT     -> Codigo de sistema de entrada
   --            p_COD_EST_OPE     -> Estado de la operaci�n
   --            p_COD_TPO_ING     -> Tipo de ingreso
   --            p_COD_MT_SWF      -> Numero del tipo de mensaje swift
   --            p_FLG_MRD_UTZ_SWF -> Flasg de mercado
   --            p_COD_TPO_OPE_AOS -> Tipo de operacion.
   --            p_FEC_VTA         -> Fecha de vencimiento
   --            p_COD_DVI         -> Codigo de la divisa
   --            p_IMp_OPE         -> Importe de la operacion
   --            p_COD_BCO_DTN     -> BIC del banco destino
   --            p_COD_BCO_ORG     -> BIC Banco Origen
   --            p_FEC_ENV_RCP_SWF -> Fecha de envio / recepcion swfit
   --            p_HOR_ENV_RCP_SWF -> Hora de envio / recepcion swfit
   --            p_NUM_REF_SWF     -> Numero de referencia Swift
   --            p_NUM_REF_EXT     -> Numero de referencia swift externa
   --            p_NUM_DOC_BFC     -> Numero rut del beneficiario
   --            p_COD_TPD_BFC     -> Rut tipo de beneficiario
   --            p_NOM_BFC         -> Nombre del Beneficiario
   --            p_NUM_CTA_BFC     -> Numero de cuenta del beneficiario
   --            p_COD_SUC         -> Codigo de la sucursal
   --            p_GLS_ADC_EST     -> Informacion adicional
   --            p_NUM_REF_CTB     -> Numero de referencia contable
   --            p_COD_BCO_BFC     -> BIB Banco Beneficario
   --            p_COD_BCO_ITD     -> BIC Banco Intermediario
   --            p_NUM_CTA_DCv_BFC -> Numero cuenta DCV Beneficiario
   --            p_GLS_DIR_BFC     -> Direccion Beneficario
   --            p_NOM_CDD_BFC     -> Nombre ciudad beneficiario
   --            p_COD_PAS_BFC     -> Codigo pais beneficiario
   --            p_NOM_ODN         -> Nombre ordenante
   --            p_NUM_DOC_ODN     -> Numero rut ordenante
   --            p_COD_TPD_ODN     -> Tipo documento de ordenante
   --            p_NUM_CTA_ODN     -> Numero cuenta ordenante
   --            p_NUM_CTA_DCv_ODN -> Numero cuenta DCV ordenante
   --            p_GLS_DIR_ODN     -> Direccion ordenante
   --            p_NOM_CDD_ODN     -> Nombre ciudad ordenante
   --            p_COD_PAS_ODN     -> Codigo pais ordenante
   --            p_NUM_CLv_NGC     -> Numero de clave de negocio
   --            p_NUM_AGT         -> Numero de agente
   --            p_COD_FND_CCLV    -> Codigo Fondo (Producto CCLV)
   --            p_COD_TPO_SDO     -> Tipo de saldo
   --            p_COD_TPO_CMA     -> Tipo de camara
   --            p_COD_TPO_FND     -> Tipo de fondo
   --            p_FEC_TPO_OPE_CCLV-> Fecha operacion CCLV
   --            p_NUM_CLv_IDF     -> Numero clave identificatorio
   --            p_OBS_OPC_SWF   -> Observacion Opcional Swift
   --            p_NUM_IDF_GTR_DOC -> Identificador gestor documental
   --            p_NUM_MVT         -> Numero de Movimiento (Vigente, Cuenta Corriente)
   --            p_COD_USR     -> rut usuario
   -- Output:
   --          p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE Sp_PAB_GES_OPE_AGP (p_FEC_ISR_OPE        IN     DATE,
                                 p_NUM_FOL_OPE        IN     NUMBER,
                                 p_NUM_FOL_NMN        IN     NUMBER,
                                 p_NUM_FOL_OPE_ORG    IN     NUMBER,
                                 p_NUM_OPE_SIS_ENT    IN     CHAR,
                                 p_FLG_EGR_ING        IN     NUMBER,
                                 p_COD_SIS_SAL        IN     CHAR,
                                 p_COD_SIS_ENT        IN     CHAR,
                                 p_COD_EST_OPE        IN     NUMBER,
                                 p_COD_TPO_ING        IN     NUMBER,
                                 p_COD_MT_SWF         IN     NUMBER,
                                 p_FLG_MRD_UTZ_SWF    IN     NUMBER,
                                 p_COD_TPO_OPE_AOS    IN     CHAR,
                                 p_FEC_VTA            IN     DATE,
                                 p_COD_DVI            IN     CHAR,
                                 p_IMp_OPE            IN     NUMBER,
                                 p_COD_BCO_DTN        IN     CHAR,
                                 p_COD_BCO_ORG        IN     CHAR,
                                 p_FEC_ENV_RCP_SWF    IN     DATE,
                                 p_HOR_ENV_RCP_SWF    IN     NUMBER,
                                 p_NUM_REF_SWF        IN     CHAR,
                                 p_NUM_REF_EXT        IN     CHAR,
                                 ----------Beneficiario------------
                                 p_NUM_DOC_BFC        IN     CHAR,
                                 p_COD_TPD_BFC        IN     CHAR,
                                 p_NOM_BFC            IN     VARCHAR2,
                                 p_NUM_CTA_BFC        IN     VARCHAR2,
                                 -------------------------------------
                                 p_COD_SUC            IN     VARCHAR2,
                                 p_GLS_ADC_EST        IN     VARCHAR2,
                                 p_NUM_REF_CTB        IN     CHAR,
                                 ----------Opcionales--------------
                                 p_COD_BCO_BFC        IN     CHAR,
                                 p_COD_BCO_ITD        IN     CHAR,
                                 p_NUM_CTA_DCv_BFC    IN     VARCHAR2,
                                 p_GLS_DIR_BFC        IN     VARCHAR2,
                                 p_NOM_CDD_BFC        IN     VARCHAR2,
                                 p_COD_PAS_BFC        IN     CHAR,
                                 p_NOM_ODN            IN     VARCHAR2,
                                 p_NUM_DOC_ODN        IN     CHAR,
                                 p_COD_TPD_ODN        IN     CHAR,
                                 p_NUM_CTA_ODN        IN     VARCHAR2,
                                 p_NUM_CTA_DCv_ODN    IN     VARCHAR2,
                                 p_GLS_DIR_ODN        IN     VARCHAR2,
                                 p_NOM_CDD_ODN        IN     VARCHAR2,
                                 p_COD_PAS_ODN        IN     CHAR,
                                 p_NUM_CLv_NGC        IN     VARCHAR2,
                                 p_NUM_AGT            IN     CHAR,
                                 p_COD_FND_CCLV       IN     CHAR,
                                 p_COD_TPO_SDO        IN     CHAR,
                                 p_COD_TPO_CMA        IN     CHAR,
                                 p_COD_TPO_FND        IN     VARCHAR2,
                                 p_FEC_TPO_OPE_CCLV   IN     DATE,
                                 p_NUM_CLv_IDF        IN     VARCHAR2,
                                 p_OBS_OPC_SWF        IN     VARCHAR2,
                                 p_NUM_IDF_GTR_DOC    IN     VARCHAR2,
                                 p_NUM_MVT            IN     CHAR,
                                 --------------------------------
                                 p_COD_USR            IN     CHAR,
                                 p_ERROR                 OUT NUMBER)
   IS
      --Seteo de Variables
      v_NOM_SP              VARCHAR2 (30) := 'Sp_PAB_GES_OPE_AGP';
      v_FEC_ISR_OPE         DATE;
      v_cod_bco_org         CHAR (11);
      v_FEC_AZA_SWF         DATE;                 --(Fecha Autorizo operaci�n)
      v_HOR_AZA_SWF         NUMBER;                --(Hora Autorizo operaci�n)
      v_COD_USR             CHAR (11);
      v_COD_SIS_SAL_FIN     CHAR (10);
      v_RESP_VAL            NUMBER;
      v_COD_EST_OPE         NUMBER;
      V_GLS_ADC_EST         VARCHAR2 (210);
      v_RESULT              NUMBER;
      v_NUM_CTA_BFC         VARCHAR2 (30);
      V_RUT_BANCO_DESTINO   CHAR (9);
      v_NUM_DOC_BFC         CHAR (11);
      v_COD_TPD_BFC         CHAR (1);
      v_COD_BCO_DTN         CHAR (11);
      v_COD_BCO_BFC         CHAR (11);
      v_COD_BCO_ITD         CHAR (11);
      v_FLG_IMP_VAL         NUMBER;
      v_NOM_ODN             VARCHAR2 (100);
      v_NUM_DOC_ODN         CHAR (11);
      v_COD_TPD_ODN         CHAR (1);
      v_NUM_CTA_ODN         VARCHAR2 (30);
      v_COD_TPO_OPE_AOS     CHAR (8);
      --CCLV
      v_NUM_AGT             VARCHAR2 (4);
      v_COD_FND_CCLV        CHAR (1);
      v_COD_TPO_SDO         CHAR (2);
      v_COD_TPO_CMA         CHAR (2);
      v_FEC_TPO_OPE_CCLV    DATE;
      v_COD_SUC             CHAR (4);
      v_FLG_MRD_UTZ_SWF     NUMBER := PKG_PAB_CONSTANTES.V_FLG_MRD_MX;

      --Variables de notificaci�n.
      v_FLG_NTF             NUMBER;
      v_CAN_OPE_NOT         NUMBER;
      p_ADJUNTO             NUMBER;
      p_ID                  VARCHAR2 (10);
      p_MENSAJE_SWF         VARCHAR2 (300);
      p_ASUNTO              VARCHAR2 (300);
      p_CORREO              VARCHAR2 (300);

      op_REG EXCEPTION;
      v_VAL_OPE             INTEGER;
   BEGIN
      /*SE AGREGA VALIDACION DE OPERACIONES DESDE EL ORIGEN */

      v_VAL_OPE := PKG_PAB_UTILITY.FN_PAB_BUS_VVV (p_NUM_OPE_SIS_ENT);

      IF v_VAL_OPE = 1
      THEN
         RAISE op_REG;
      END IF;

      v_COD_USR := p_COD_USR;
      p_ERROR := PKG_PAB_CONSTANTES.v_OK;

      v_COD_BCO_BFC := UPPER (p_COD_BCO_BFC);
      v_COD_BCO_DTN := UPPER (p_COD_BCO_DTN);
      v_COD_BCO_ITD := UPPER (p_COD_BCO_ITD);

      /*IF (p_COD_DVI = PKG_PAB_CONSTANTES.V_STR_DVI_CLP) THEN
          --Parchar temporal utilizado para AGP Desarrollo (Eliminar)
          v_COD_BCO_DTN := SUBSTR(p_COD_BCO_DTN,0,7) || '0';

      ELSE

          v_COD_BCO_BFC := SUBSTR(p_COD_BCO_BFC,0,7) || '0' || SUBSTR(p_COD_BCO_BFC,9,3);
          v_COD_BCO_DTN := UPPER(p_COD_BCO_DTN);
          v_COD_BCO_ITD := UPPER(p_COD_BCO_ITD);

      END IF;*/

      -- Se crea procedimiento que busca Canal de Salida
      PKG_PAB_OPERACION.Sp_PAB_BUS_CANAL_SALIDA (v_COD_BCO_DTN, --p_COD_BCO_DTN,
                                                 p_COD_MT_SWF,
                                                 NULL, --p_MONTO_OPE, Campo se debe modificar
                                                 p_COD_TPO_OPE_AOS,
                                                 p_COD_DVI,
                                                 v_COD_SIS_SAL_FIN,
                                                 p_ERROR);

      --Indicamos una glosa que la operaci�n viene del AGP
      v_DES_BIT := 'Operaci�n de origen AGP';
      V_GLS_ADC_EST := v_DES_BIT;
      v_COD_EST_OPE := PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEAUT;

      PKG_PAB_TRACKING.Sp_PAB_INS_BIT_OPE (p_NUM_FOL_OPE,
                                           p_FEC_ISR_OPE,
                                           p_COD_USR,
                                           v_COD_EST_OPE,            --Antiguo
                                           v_COD_EST_OPE,              --Nuevo
                                           v_DES_BIT,
                                           v_RESULT);

      --Validamos el horario / monto
      PKG_PAB_UTILITY.Sp_PAB_VAL_MONDA_ALMOT (p_COD_DVI,
                                              p_IMp_OPE,
                                              v_FLG_IMP_VAL,
                                              p_ERROR);

      IF (v_FLG_IMP_VAL = PKG_PAB_CONSTANTES.V_error)
      THEN
         --Dejamos la operaci�n por visar
         v_COD_EST_OPE := PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPVI;
         v_DES_BIT := 'Operacion Supera Monto Maximo';
         --V_GLS_ADC_EST := v_DES_BIT;

         --Se llama al procedimiento de bitacora
         PKG_PAB_TRACKING.Sp_PAB_INS_BIT_OPE (
            p_NUM_FOL_OPE,
            p_FEC_ISR_OPE,
            p_COD_USR,
            PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEAUT,                 --Antiguo
            v_COD_EST_OPE,                                             --Nuevo
            v_DES_BIT,
            v_RESULT
         );
      END IF;

      --Seteo
      v_NUM_CTA_BFC := NVL (TRIM (p_NUM_CTA_BFC), 0);
      v_NUM_DOC_BFC := p_NUM_DOC_BFC;
      v_COD_TPD_BFC := p_COD_TPD_BFC;
      v_NOM_ODN := p_NOM_ODN;
      v_NUM_DOC_ODN := p_NUM_DOC_ODN;
      v_COD_TPD_ODN := p_COD_TPD_ODN;
      v_NUM_CTA_ODN := NVL (TRIM (p_NUM_CTA_ODN), 0);
      v_COD_SUC := LPAD (p_COD_SUC, 4, 0);

      --CCLV
      v_COD_FND_CCLV := p_COD_FND_CCLV;
      v_NUM_AGT := p_NUM_AGT;
      v_COD_TPO_SDO := p_COD_TPO_SDO;
      v_COD_TPO_CMA := p_COD_TPO_CMA;
      v_FEC_TPO_OPE_CCLV := p_FEC_TPO_OPE_CCLV;

      --Tratamiento de la informaci�n
      IF ( (p_COD_MT_SWF = PKG_PAB_CONSTANTES.V_COD_MT202
            OR p_COD_MT_SWF = PKG_PAB_CONSTANTES.V_COD_MT205)
          AND p_COD_DVI = PKG_PAB_CONSTANTES.V_STR_DVI_CLP)
      THEN
         BEGIN
            --Obtenemos rut del banco destino
            SELECT   TCNIFENT
              INTO   V_RUT_BANCO_DESTINO
              FROM   tcdt040
             WHERE   TGCDSWSA = v_COD_BCO_DTN                  --p_COD_BCO_DTN
                                             AND TCNIFENT(+) <> v_BANCO;

            --SEPARAMOS RUT
            PKG_PAB_UTILITY.Sp_PAB_DESCOMp_RUT (V_RUT_BANCO_DESTINO,
                                                v_NUM_DOC_BFC,
                                                v_COD_TPD_BFC);
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               err_code := SQLCODE;
               err_msg :=
                  'No se pudo obtener el rut del banco:' || p_COD_BCO_DTN;
               PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                   v_NOM_PCK,
                                                   err_msg,
                                                   v_NOM_SP);
               p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            WHEN OTHERS
            THEN
               err_code := SQLCODE;
               err_msg := SUBSTR (SQLERRM, 1, 300);
               PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                   v_NOM_PCK,
                                                   err_msg,
                                                   v_NOM_SP);
               p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
               p_s_mensaje := err_code || '-' || err_msg;
               RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
         END;
      END IF;

      --Verificamos el tipo de operaci�n
      BEGIN
         SELECT   COD_TPO_OPE_AOS
           INTO   v_COD_TPO_OPE_AOS
           FROM   PABS_DT_TIPO_OPRCN_ALMOT
          WHERE   COD_TPO_OPE_AOS = p_COD_TPO_OPE_AOS;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            v_COD_TPO_OPE_AOS := TRIM (P_COD_TPO_OPE_AOS);

            --CLP
            IF (p_COD_DVI = PKG_PAB_CONSTANTES.V_STR_DVI_CLP)
            THEN
               --En caso de no encontrarlo
               CASE
                  WHEN v_COD_TPO_OPE_AOS = 'CCS'
                  THEN
                     v_COD_TPO_OPE_AOS := 'VIRS';
                  WHEN v_COD_TPO_OPE_AOS = 'NDF'
                  THEN
                     v_COD_TPO_OPE_AOS := 'VFUT';
                  ELSE
                     v_COD_TPO_OPE_AOS := 'BCO';
               END CASE;
            ELSE
               --MX

               --En caso de no encontrarlo
               CASE
                  WHEN v_COD_TPO_OPE_AOS = 'T'
                  THEN
                     v_COD_TPO_OPE_AOS := 'TRANSF';
                  ELSE
                     v_COD_TPO_OPE_AOS := 'TRANSF';
               END CASE;
            END IF;
         WHEN OTHERS
         THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            p_s_mensaje := err_code || '-' || err_msg;
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);

            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
      END;

      --Beneficiario
      IF NVL (TO_CHAR (p_NUM_CTA_BFC), '#') = '#'
      THEN
         v_NUM_CTA_BFC := 0;
      END IF;

      IF (p_COD_DVI = PKG_PAB_CONSTANTES.V_STR_DVI_CLP)
      THEN
         --Mercado Nacional
         v_FLG_MRD_UTZ_SWF := PKG_PAB_CONSTANTES.V_FLG_MRD_MN;

         --Nombre Ordenante
         IF NVL (TO_CHAR (v_NOM_ODN), '#') = '#'
         THEN
            v_NOM_ODN := 'Banco Santander';
         END IF;

         --Rut Ordenante
         IF NVL (TO_CHAR (v_NUM_DOC_ODN), '#') = '#'
            OR TRIM (v_NUM_DOC_ODN) = '0'
         THEN
            v_NUM_DOC_ODN := PKG_PAB_CONSTANTES.V_NUM_DOC_BANCO_SANTANDER;
            v_COD_TPD_ODN := PKG_PAB_CONSTANTES.V_COD_TPD_BANCO_SANTANDER;
         END IF;

         --Cuenta Ordenante
         IF NVL (TO_CHAR (v_NUM_CTA_ODN), '#') = '#'
            OR TRIM (v_NUM_CTA_ODN) = '0'
         THEN
            v_NUM_CTA_ODN := 0;
         END IF;
      END IF;

      --CCLV
      IF (p_COD_TPO_OPE_AOS = PKG_PAB_CONSTANTES.V_STR_TPO_OPE_CCLV)
      THEN
         v_NUM_AGT := REPLACE (v_NUM_AGT, 'X', '');
         v_COD_FND_CCLV := 'X';
         v_COD_TPO_SDO := SUBSTR (p_OBS_OPC_SWF, 0, 2);
         v_COD_TPO_CMA := SUBSTR (p_OBS_OPC_SWF, 3, 2);
         v_FEC_TPO_OPE_CCLV :=
            TO_DATE (SUBSTR (p_OBS_OPC_SWF, 5, 6), 'YYMMDD');
      END IF;

      --Insertamos la Operacion
      PKG_PAB_OPERACION.Sp_PAB_INS_OPE (p_FEC_ISR_OPE,
                                        p_NUM_FOL_OPE,
                                        p_NUM_FOL_NMN,
                                        p_NUM_FOL_OPE_ORG,
                                        p_NUM_OPE_SIS_ENT,
                                        p_FLG_EGR_ING,
                                        v_COD_SIS_SAL_FIN,
                                        p_COD_SIS_ENT,
                                        v_COD_EST_OPE,
                                        p_COD_TPO_ING,
                                        p_COD_MT_SWF,
                                        v_FLG_MRD_UTZ_SWF, --p_FLG_MRD_UTZ_SWF    ,
                                        v_COD_TPO_OPE_AOS, --p_COD_TPO_OPE_AOS    ,
                                        TRUNC (p_FEC_VTA),
                                        p_COD_DVI,
                                        p_IMp_OPE,
                                        v_COD_BCO_DTN, --p_COD_BCO_DTN        ,
                                        p_COD_BCO_ORG,
                                        NULL,
                                        NULL,
                                        NULL,
                                        p_NUM_REF_EXT,
                                        ----------Beneficiario------------
                                        v_NUM_DOC_BFC,
                                        v_COD_TPD_BFC,
                                        p_NOM_BFC,
                                        v_NUM_CTA_BFC,
                                        -------------------------------------
                                        v_COD_SUC,    --p_COD_SUC            ,
                                        V_GLS_ADC_EST,
                                        p_NUM_REF_CTB,
                                        ----------Opcionales--------------
                                        v_COD_BCO_BFC, --p_COD_BCO_BFC        ,
                                        v_COD_BCO_ITD, --p_COD_BCO_ITD        ,
                                        p_NUM_CTA_DCv_BFC,
                                        p_GLS_DIR_BFC,
                                        p_NOM_CDD_BFC,
                                        p_COD_PAS_BFC,
                                        v_NOM_ODN,    --p_NOM_ODN            ,
                                        p_NUM_DOC_ODN,
                                        p_COD_TPD_ODN,
                                        V_NUM_CTA_ODN, --p_NUM_CTA_ODN        ,
                                        p_NUM_CTA_DCv_ODN,
                                        p_GLS_DIR_ODN,
                                        p_NOM_CDD_ODN,
                                        p_COD_PAS_ODN,
                                        p_NUM_CLv_NGC,
                                        v_NUM_AGT,    --p_NUM_AGT            ,
                                        v_COD_FND_CCLV, --p_COD_FND_CCLV       ,
                                        v_COD_TPO_SDO, --p_COD_TPO_SDO        ,
                                        v_COD_TPO_CMA, --p_COD_TPO_CMA        ,
                                        p_COD_TPO_FND,
                                        v_FEC_TPO_OPE_CCLV, --p_FEC_TPO_OPE_CCLV   ,
                                        p_NUM_CLv_IDF,
                                        p_OBS_OPC_SWF,
                                        p_NUM_IDF_GTR_DOC,
                                        p_NUM_MVT,
                                        --------------------------------
                                        p_COD_USR,
                                        p_ERROR);

      --Se verifica si notificamos el visar
      IF (v_FLG_IMP_VAL = PKG_PAB_CONSTANTES.V_error)
      THEN
         -- Notificaci�n del �rea Mesa
         PKG_PAB_NOTIFICACION.Sp_PAB_BUS_OPE_SIN_CBO (
            PKG_PAB_CONSTANTES.V_STR_EST_AOS_OPEPVI,
            v_FLG_NTF,
            v_CAN_OPE_NOT,
            p_ERROR
         );

         PKG_PAB_NOTIFICACION.SP_PAB_DET_CRRO_TIBCO (NULL,
                                                     NULL,
                                                     NULL,
                                                     'PVIS',
                                                     v_CAN_OPE_NOT, -- v_cont_est_vis,
                                                     NULL,    --'MESA',--NULL,
                                                     NULL,
                                                     NULL,
                                                     NULL,
                                                     p_ADJUNTO,
                                                     p_ID,
                                                     p_MENSAJE_SWF,
                                                     p_ASUNTO,
                                                     p_CORREO,
                                                     p_ERROR);
      /*******************************************************************/

      END IF;

      p_ERROR := PKG_PAB_CONSTANTES.v_OK;
   --      COMMIT;

   EXCEPTION
      WHEN op_REG
      THEN
         err_code := SQLCODE;
         err_msg :=
               'Operacion Origen N� '
            || p_NUM_OPE_SIS_ENT
            || 'ya se encuentra en Altos Montos';
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN DUP_VAL_ON_INDEX
      THEN
         err_code := SQLCODE;
         err_msg :=
               'Ya existe operaci�n'
            || p_NUM_FOL_OPE
            || ' '
            || TO_CHAR (v_FEC_ISR_OPE, 'dd-mm-yyyy hh24:mi:ss');
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         p_s_mensaje := err_code || '-' || err_msg;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);

         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END Sp_PAB_GES_OPE_AGP;

   --***********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_BUS_OPE_PLZ
   -- Objetivo: Procedimiento que BUSCA numero de plazo para actualizar estado
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_DETLL_OPRCN_PLAZO
   -- Fecha: 25/07/17
   -- Autor: Santander
   -- Input:
   -- p_NUM_OPE_OFB : numero de operacion OfficeBanking
   -- Output:
   -- p_NUM_OPE_PZO : numero de vale vista Plazo
   -- p_ERR_CODE       -> codigo de error de ejecucion (retorno para TIBCO)
   -- p_ERR_MSG        -> mensaje de error de ejecucion (retorno para TIBCO)
   -- p_ERROR          -> codigo de error de ejecucion de PL (para log)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de utimos cambios>
   --***********************************************************************************************
   PROCEDURE SP_PAB_BUS_OPE_PLZ (p_NUM_OPE_OFB   IN     VARCHAR2,
                                 p_NUM_OPE_PZO      OUT VARCHAR2,
                                 p_ERR_CODE         OUT VARCHAR2,
                                 p_ERR_MSG          OUT VARCHAR2,
                                 p_ERROR            OUT NUMBER)
   IS
      --Seteo de Variables
      v_NOM_SP            VARCHAR2 (30) := 'SP_BUS_OPE_PLZ';
      --v_fechaHoy date:= sysdate;
      v_NUM_OPE_OFB       CHAR (20) := p_NUM_OPE_OFB;
      v_NUM_OPE_ORI       CHAR (20);
      v_NUM_OPE_OFB_NUM   NUMBER (12) := TO_NUMBER (p_NUM_OPE_OFB);
   BEGIN
      p_ERROR := PKG_PAB_CONSTANTES.v_ok;
      p_ERR_CODE := '00';
      p_ERR_MSG := 'Dato Encontrado!';

      --BUSCO NUMERO DE OB DE ACUERDO A NUMERO DE OPERACION
      SELECT   TRIM (NUM_OPE_PZO)
        INTO   p_NUM_OPE_PZO
        FROM   PABS_DT_DETLL_OPRCN_PLAZO
       WHERE   TO_NUMBER (TRIM (SUBSTR (NUM_OPE_OFB, -12))) =
                  TRIM (p_NUM_OPE_OFB);
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         -- PUEDE QUE LLEGUE NUMERO DE OPERACION

         BEGIN
            SELECT   NUM_OPE_SIS_ENT
              INTO   v_NUM_OPE_ORI
              FROM   PABS_DT_DETLL_OPRCN
             WHERE   NUM_FOL_OPE = v_NUM_OPE_OFB_NUM;
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               p_ERR_MSG :=
                  'Operacion no se encuenta en Altos Montos. '
                  || p_NUM_OPE_OFB;
               p_ERR_CODE := '16';
               p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         END;

         BEGIN
            SELECT   TRIM (NUM_OPE_PZO)
              INTO   p_NUM_OPE_PZO
              FROM   PABS_DT_DETLL_OPRCN_PLAZO
             WHERE   TO_NUMBER (TRIM (SUBSTR (NUM_OPE_OFB, -12))) =
                        TRIM (v_NUM_OPE_ORI);
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               p_ERR_MSG :=
                  'Operacion no se encuenta en Altos Montos. '
                  || p_NUM_OPE_OFB;
               p_ERR_CODE := '16';
               p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         END;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END SP_PAB_BUS_OPE_PLZ;

   /************************************************************************************************
   -- Funcion/Procedimiento: FN_PAB_COR_CTA
   -- Objetivo: Corta la cuenta corriente
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: N/A
   -- Fecha: 02/06/16
   -- Autor: Santander
   -- Input:      p_NUM_CTA: Numero Cuenta Corriente
   --             p_LARGO:  Largo necesario de la cuenta
   -- Output:
   --
   -- Input/Output: N/A
   -- Retorno: Retorna la cuenta cortada segun lo solicitado
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   FUNCTION FN_PAB_COR_CTA (p_NUM_CTA IN VARCHAR2, p_LARGO IN NUMBER)
      RETURN VARCHAR2
   IS
      --Seteo de Variables
      v_NOM_SP   VARCHAR2 (30) := 'FN_PAB_COR_CTA';
      v_TAMA�O   NUMBER;
      v_TEXTO    VARCHAR2 (30);
      v_FINAL    VARCHAR2 (16);
   BEGIN
      v_TAMA�O := LENGTH (p_NUM_CTA);

      IF (v_TAMA�O > p_LARGO)
      THEN
         v_FINAL := SUBSTR (p_NUM_CTA, -p_LARGO);
      ELSE
         v_FINAL := LPAD (p_NUM_CTA, p_LARGO, '0');
      END IF;

      RETURN v_FINAL;
   EXCEPTION
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         RETURN NULL;
         p_s_mensaje := err_code || '-' || err_msg;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END;


   --***********************************************************************************************
   -- Funcion/Procedimiento: SP_ING_PAG_OB
   -- Objetivo: Procedimiento que ingresa operaciones tipo LBTR para OB
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: SP_ACT_EST_OPE_DVP
   -- Fecha: 25/07/17
   -- Autor: Santander
   -- Input:
   -- p_NUM_DOC_BCO    -> Numero documento empresa
   -- p_COD_DVI        -> Codigo divisa
   -- p_IMP_MTO        -> Monto operacion
   -- p_FEC_ING        -> fecha de ingreso
   -- p_FEC_VTA        -> fecha de valuta
   -- p_NUM_DOC_BFN    -> numero documento beneficiario
   -- p_NOM_BFN        -> nombre beneficiario
   -- p_CTA_BFC        -> cuenta beneficiario
   -- p_NUM_DOC_ORD    -> numero documento ordenante
   -- p_NOM_ORD        -> nombre ordenante
   -- p_CTA_ORD        -> cuenta ordenante
   -- p_COD_SIS_ORI    -> Codigo sistema origen
   -- p_SUC_ORI        -> Sucursal origen
   -- p_COD_TR_PAGO    -> Codigo transaccion
   -- p_NUM_OPE        -> numero operacion
   -- p_COD_RFR        -> refenrencia externa
   -- p_REF_EXT        -> Codifo referencia
   -- p_CAN_PAG        -> Cantidad de paginas
   -- p_CTA_CTB        -> referencia externa
   -- p_GLS_ADC        -> glosa adicional
   -- Output:
   -- p_NUM_FOL_OPE    -> numero de operacion ingresada
   -- p_FEC_ISR_OPE    -> fecha de ingreso de la operacion
   -- p_FOR_PAG OUT    -> forma de pago de la operacion
   -- p_ERR_CODE       -> codigo de error de ejecucion (retorno para TIBCO)
   -- p_ERR_MSG        -> mensaje de error de ejecucion (retorno para TIBCO)
   -- p_ERROR          -> codigo de error de ejecucion de PL (para log)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de utimos cambios>
   --***********************************************************************************************
   PROCEDURE SP_ING_PAG_OB (p_NUM_DCO_BCO   IN     VARCHAR2,
                            p_COD_DVI       IN     VARCHAR2,
                            p_IMP_MTO       IN     NUMERIC,
                            p_FEC_ING       IN     VARCHAR2,
                            p_FEC_VTA       IN     VARCHAR2,
                            p_NUM_DCO_BNF   IN     VARCHAR2,
                            p_NOM_BNF       IN     VARCHAR2,
                            p_CTA_BNF       IN     VARCHAR2,
                            p_NUM_DCO_ODN   IN     VARCHAR2,
                            p_NOM_ODN       IN     VARCHAR2,
                            p_CTA_ODN       IN     VARCHAR2,
                            p_COD_SIS_ORI   IN     VARCHAR2,
                            p_SUC_ORI       IN     NUMERIC,
                            p_COD_TR_PAGO   IN     VARCHAR2,  -- VVT - OVT ETC
                            p_NUM_OPE       IN     VARCHAR2,
                            p_COD_RFR       IN     VARCHAR2,
                            p_REF_EXT       IN     VARCHAR2,
                            p_CAN_PAG       IN     NUMERIC,
                            p_CTA_CTB       IN     VARCHAR2,
                            p_GLS_ADIC      IN     VARCHAR2,
                            p_NUM_FOL_OPE      OUT NUMBER,
                            p_FEC_ISR_OPE      OUT DATE,
                            p_FOR_PAG          OUT VARCHAR2,
                            p_ERR_CODE         OUT VARCHAR2,
                            p_ERR_MSG          OUT VARCHAR2,
                            p_ERROR            OUT NUMBER)
   IS
      --Seteo de Variables
      v_NOM_SP              VARCHAR2 (30) := 'SP_ING_PAG_OB';
      v_NUM_FOL_OPE         NUMBER (12) := SPK_PAB_PAG_EAM.NEXTVAL; -- obtenemos FOLIO
      --v_fechaHoy date:= sysdate;

      v_COD_USR             VARCHAR2 (11) := 'ESB';

      v_COD_MT_SWF          NUMBER (3) := 103;            -- por ahora en duro
      v_COD_BCO_DTN         CHAR (11);
      V_NUM_DOC_BFC         CHAR (11);
      V_COD_TPD_BFC         CHAR (1);
      V_NUM_DOC_BFC_NUM     NUMBER (10);
      V_NUM_DOC_ODN         CHAR (11);
      V_COD_TPD_ODN         CHAR (1);
      V_NUM_DOC_ODN_NUM     NUMBER (10);

      v_FEC_VTA             DATE := TO_DATE (p_FEC_VTA, 'YYYY/MM/DD');

      v_CTA_CTE_16          VARCHAR2 (16);

      -- *** Variables para seccion de validacion de horario sistema y monto importe
      v_FLG_IMP_VAL         NUMBER;
      v_COD_EST_OPE         NUMBER;
      V_GLS_ADC_EST         VARCHAR2 (210);
      v_IMP_MON             NUMBER;
      v_FEC_ING             DATE := SYSDATE;
      v_DES_BIT             VARCHAR2 (80);
      v_RESULT              NUMBER;
      v_COD_TR_PAGO         VARCHAR2 (8);

      --Variables de notificaci�n.
      v_FLG_NTF             NUMBER;
      v_CAN_OPE_NOT         NUMBER;
      p_ADJUNTO             NUMBER;
      p_ID                  VARCHAR2 (10);
      p_MENSAJE_SWF         VARCHAR2 (300);
      p_ASUNTO              VARCHAR2 (300);
      p_CORREO              VARCHAR2 (300);
      p_CANAL_SAL           CHAR (10);
      v_RUT_ITAU            VARCHAR2 (11) := '0076645030K';
      v_RUT_CORP            VARCHAR2 (11) := '00970230009';
      v_NUM_DCO_BCO         VARCHAR2 (11);
      p_CANAL_SAL_VAL       CHAR (10);
      p_COD_EST_AOS         NUMBER;
      p_CANAL_SAL_CUENPUE   CHAR (10);
      op_REG EXCEPTION;
      v_VAL_OPE             INTEGER;
   BEGIN
      /*SE AGREGA VALIDACION DE OPERACIONES DESDE EL ORIGEN */

      v_VAL_OPE := PKG_PAB_UTILITY.FN_PAB_BUS_VVV (p_COD_SIS_ORI);

      IF v_VAL_OPE = 1
      THEN
         RAISE op_REG;
      END IF;

      /*SE AGREGA VALIDACION DE OPERACIONES DESDE EL ORIGEN */

      p_ERROR := PKG_PAB_CONSTANTES.v_ok;
      p_ERR_CODE := '0';
      p_ERR_MSG := 'Dato Ingresado Correctamente';
      v_DES_BIT :=
         'Operacion ingresada desde sistema : '
         || PKG_PAB_UTILITY.FN_PAB_OBT_DSC_SIS_ENT_SAL (p_COD_SIS_ORI);

      v_CTA_CTE_16 := SUBSTR (p_CTA_ODN, 9, 12);

      IF p_NUM_DCO_BCO = v_RUT_ITAU
      THEN
         v_NUM_DCO_BCO := v_RUT_CORP;
      ELSE
         v_NUM_DCO_BCO := p_NUM_DCO_BCO;
      END IF;


      BEGIN
         SELECT   TGCDSWSA
           INTO   v_COD_BCO_DTN
           FROM   TCDT040
          WHERE   LPAD (TCNIFENT, 11, '0') = v_NUM_DCO_BCO
                  AND TGCDSWSA IS NOT NULL;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            p_ERR_CODE := SQLCODE;
            p_ERR_MSG :=
               'No se encuentra banco asociado a numero de documento : '
               || p_NUM_DCO_BCO;
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (p_ERR_CODE,
                                                v_NOM_PCK,
                                                p_ERR_MSG,
                                                v_NOM_SP);
         WHEN OTHERS
         THEN
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;

            p_ERR_MSG := SUBSTR (SQLERRM, 1, 300);
            p_ERR_CODE := SQLCODE;

            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (p_ERR_CODE,
                                                v_NOM_PCK,
                                                p_ERR_MSG,
                                                v_NOM_SP);

            p_s_mensaje := p_ERR_MSG || '-' || p_ERR_CODE;
            RAISE_APPLICATION_ERROR (-20000, p_ERR_MSG);
      END;


      -- INICIO TRANFORAMCION DE RUTS
      PKG_PAB_UTILITY.Sp_PAB_DESCOMp_RUT (p_NUM_DCO_BNF,
                                          V_NUM_DOC_BFC,
                                          V_COD_TPD_BFC);

      V_NUM_DOC_BFC_NUM := TO_NUMBER (V_NUM_DOC_BFC); -- TRANSDFORMAMOS A NUMERO PARA ELIMINAR 0 EN RUT

      p_ERR_MSG := V_NUM_DOC_BFC_NUM;

      PKG_PAB_UTILITY.Sp_PAB_DESCOMp_RUT (p_NUM_DCO_ODN,
                                          V_NUM_DOC_ODN,
                                          V_COD_TPD_ODN);

      V_NUM_DOC_ODN_NUM := TO_NUMBER (V_NUM_DOC_ODN); -- TRANSDFORMAMOS A NUMERO PARA ELIMINAR 0 EN RUT

      -- FIN TRANSFORMACION DE RUT

      -- INCIO VALIDACION MONTO MAXIMO

      v_IMP_MON := SUBSTR (p_IMP_MTO, 0, LENGTH (p_IMP_MTO) - 2);      --MONTO

      PKG_PAB_UTILITY.Sp_PAB_VAL_MONDA_ALMOT (p_COD_DVI,
                                              v_IMP_MON,
                                              v_FLG_IMP_VAL,
                                              p_ERROR);


      IF v_FLG_IMP_VAL = PKG_PAB_CONSTANTES.V_error
      THEN
         v_COD_EST_OPE := PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPVI;
         V_GLS_ADC_EST := 'Operacion Supera Monto Maximo';
      ELSE
         v_COD_EST_OPE := PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEAUT;
         V_GLS_ADC_EST := p_GLS_ADIC;
      END IF;

      -- FIN VALIDACION MONTO MAXIMO

      -- SE AGREGA PROCESO DE VALIDACION DE PRODUCTO CAJA


      IF (p_COD_TR_PAGO = 'VVTTGR')
      THEN                                                        -- IMPUESTOS
         v_COD_TR_PAGO := 'IMPTOS';
         p_CANAL_SAL := PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SAL_LBTR;
      ELSE
         v_COD_TR_PAGO := 'BCO';
         p_CANAL_SAL := PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SAL_CMN;
      END IF;

      -- FIN AGREGA PROCESO DE VALIDACION DE PRODUCTO CAJA


      -- SE AGREGA VALIDACION PARA CUANDO SEA BANCO DESTINO SANTANDER, QUEDE A CTAS CORRIENTES

      IF (v_COD_BCO_DTN = PKG_PAB_CONSTANTES.V_BIC_BANCO_SANTANDER)
         OR (v_COD_BCO_DTN = PKG_PAB_CONSTANTES.V_BIC_BANCO_SANTANDER_HOM)
      THEN
         --SIGNIFICA QUE EL BANCO DESTINO ES SANTANDER, POR LO QUE EL CANAL ES CUENTA CORRIENTE

         p_CANAL_SAL := PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SAL_CTACTE;
         p_CANAL_SAL_VAL := PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SAL_CTACTE;
         p_COD_EST_AOS := PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEING;
         p_CANAL_SAL_CUENPUE := PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SAL_CTACTE;

         IF V_NUM_DOC_BFC_NUM =
               TO_NUMBER (
                  TRIM (PKG_PAB_CONSTANTES.V_NUM_DOC_BANCO_SANTANDER)
               )
            AND TO_NUMBER (p_CTA_BNF) = 0
         THEN
            p_ERR_CODE := '1';
            p_ERR_MSG := 'Cumple Condicion de Beneficiario Cuenta 0. ';
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (p_ERR_CODE,
                                                v_NOM_PCK,
                                                p_ERR_MSG,
                                                v_NOM_SP);

            p_CANAL_SAL := PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SAL_CTACTE;
            p_CANAL_SAL_VAL := PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SE_OOFF;
            p_COD_EST_AOS := PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEING;
            p_CANAL_SAL_CUENPUE := PKG_PAB_CONSTANTES.V_COD_TIP_FPA_CPU;
         END IF;
      /*ELSE

      p_CANAL_SAL := PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SAL_LBTR;*/

      END IF;


      -- SE AGREGA VALIDACION PARA QUE CUANDO SEA RUT SANTANDER, EL DEFECTO SEA ABONAR A OOFF.

      /*IF  V_NUM_DOC_BFC_NUM = TO_NUMBER(TRIM(PKG_PAB_CONSTANTES.V_NUM_DOC_BANCO_SANTANDER)) AND TO_NUMBER(p_CTA_BNF) = 0  THEN

       p_CANAL_SAL := PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SAL_CTACTE;
       p_CANAL_SAL_VAL := PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SE_OOFF;
       p_COD_EST_AOS := PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAB;
       p_CANAL_SAL_CUENPUE := PKG_PAB_CONSTANTES.V_COD_TIP_FPA_CPU;

      END IF;*/


      --

      -- VALIDACION DE CANAL DE ENTRADA PARA OPERACION INTERNA

      IF p_CANAL_SAL = PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SAL_CTACTE
      THEN
         PKG_PAB_OPERACION_INT.SP_PAB_INS_OPE_ITR (
            v_FEC_ING,                    --Fecha de insercion de la operaci�n
            v_NUM_FOL_OPE,                   --Numero Foliador de la operacion
            v_COD_TR_PAGO,
            p_COD_SIS_ORI,
            p_CANAL_SAL_VAL,
            p_COD_EST_AOS,          --PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEING,
            PKG_PAB_CONSTANTES.V_COD_TIP_ING_DIR,
            0,
            PKG_PAB_CONSTANTES.V_FLG_EGR,
            TO_NUMBER (TRIM (p_NUM_OPE)),
            p_COD_DVI,
            v_IMP_MON,
            V_NUM_DOC_BFC_NUM,                   --Numero rut del beneficiario
            V_COD_TPD_BFC,
            PKG_PAB_CANALES.FN_PAB_COR_CTA (p_CTA_BNF, v_largoCuenta),
            LPAD (TO_NUMBER (p_SUC_ORI), 4, '0'),
            0,
            0,
            NULL,
            0,
            0,
            v_DES_BIT,
            p_NOM_BNF,
            '',
            p_NOM_ODN,
            V_NUM_DOC_ODN_NUM,
            V_COD_TPD_ODN,
            TO_NUMBER (TRIM (v_CTA_CTE_16)),
            TRIM (p_CANAL_SAL),
            NULL,
            p_ERROR
         );


         /* RETORNAMOS LOS VALORES NECESARIOS PARA ACTUALIZAR POSTERIORMENTE*/

         p_NUM_FOL_OPE := v_NUM_FOL_OPE;
         p_FEC_ISR_OPE := v_FEC_ING;
         p_FOR_PAG := p_CANAL_SAL;
      ELSE
         PKG_PAB_OPERACION.Sp_PAB_INS_OPE (
            v_FEC_ING,                    --Fecha de insercion de la operaci�n
            v_NUM_FOL_OPE,                   --Numero Foliador de la operacion
            0,                                  --Numero Foliador de la nomina
            0, --Numero que tiene el numero original de la operacion relacionada
            TO_NUMBER (TRIM (p_NUM_OPE)), --Numero de operacion de la operacion en el sistema origen
            PKG_PAB_CONSTANTES.V_FLG_EGR,           --Flag de egreso e ingreso
            p_CANAL_SAL,                      -- * Codigo de sistema de salida
            p_COD_SIS_ORI,                      --Codigo de sistema de entrada
            v_COD_EST_OPE,                            --Estado de la operaci�n
            PKG_PAB_CONSTANTES.V_COD_TIP_ING_DIR,            --Tipo de ingreso
            v_COD_MT_SWF,                   --Numero del tipo de mensaje swift
            PKG_PAB_CONSTANTES.V_FLG_MRD_MN,                --Flasg de mercado
            v_COD_TR_PAGO,           --p_COD_TR_PAGO   ,  --Tipo de operacion.
            v_FEC_VTA,                                  --Fecha de vencimiento
            p_COD_DVI,                                   --Codigo de la divisa
            v_IMP_MON,                               --Importe de la operacion
            v_COD_BCO_DTN,                             --BIC del banco destino
            PKG_PAB_CONSTANTES.V_BIC_BANCO_SANTANDER,       --BIC Banco Origen
            NULL,                           --Fecha de envio / recepcion swfit
            NULL,                            --Hora de envio / recepcion swfit
            p_COD_RFR,                            --Numero de referencia Swift
            TRIM (p_REF_EXT),             --Numero de referencia swift externa
            V_NUM_DOC_BFC_NUM,                   --Numero rut del beneficiario
            V_COD_TPD_BFC,                          --Rut tipo de beneficiario
            p_NOM_BNF,                               --Nombre del Beneficiario
            --TO_NUMBER(TRIM(p_CTA_BNF)),  --Numero de cuenta del beneficiario
            --SUBSTR(LPAD(TRIM(p_CTA_BNF),16,'0'),-16) ,
            PKG_PAB_CANALES.FN_PAB_COR_CTA (p_CTA_BNF, v_largoCuenta),
            LPAD (TO_NUMBER (p_SUC_ORI), 4, '0'),      --Codigo de la sucursal
            V_GLS_ADC_EST,                             --Informacion adicional
            0,     --TRIM(p_CTA_CTB)        ,  --Numero de referencia contable
            NULL,                                      --BIB Banco Beneficario
            NULL,                                    --BIC Banco Intermediario
            NULL,                             --Numero cuenta DCV Beneficiario
            NULL,                                      --Direccion Beneficario
            NULL,                                 --Nombre ciudad beneficiario
            NULL,                                   --Codigo pais beneficiario
            p_NOM_ODN,                                      --Nombre ordenante
            V_NUM_DOC_ODN_NUM,                          --Numero rut ordenante
            V_COD_TPD_ODN,                       --Tipo documento de ordenante
            TO_NUMBER (TRIM (v_CTA_CTE_16)),         --Numero cuenta ordenante
            NULL,                                --Numero cuenta DCV ordenante
            NULL,                                        --Direccion ordenante
            NULL,                                    --Nombre ciudad ordenante
            NULL,                                      --Codigo pais ordenante
            NULL,                                 --Numero de clave de negocio
            NULL,                                           --Numero de agente
            NULL,                               --Codigo Fondo (Producto CCLV)
            NULL,                                              --Tipo de saldo
            NULL,                                             --Tipo de camara
            NULL,                                              --Tipo de fondo
            NULL,                                       --Fecha operacion CCLV
            NULL,                               --Numero clave identificatorio
            V_GLS_ADC_EST,                        --Observacion Opcional Swift
            NULL,                            --Identificador gestor documental
            NULL,                              --Numero de Movimiento (Vigente
            v_COD_USR,                                           --rut usuario
            p_ERROR
         );                                                                 --
      END IF;

      PKG_PAB_TRACKING.Sp_PAB_INS_BIT_OPE (v_NUM_FOL_OPE,
                                           v_FEC_ING,
                                           v_COD_USR,
                                           v_COD_EST_OPE,
                                           v_COD_EST_OPE,
                                           v_DES_BIT,
                                           v_RESULT);


      IF v_COD_EST_OPE = PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPVI
      THEN
         --Notificaci�n del �rea Mesa
         PKG_PAB_NOTIFICACION.Sp_PAB_BUS_OPE_SIN_CBO (
            PKG_PAB_CONSTANTES.V_STR_EST_AOS_OPEPVI,
            v_FLG_NTF,
            v_CAN_OPE_NOT,
            p_ERROR
         );
         PKG_PAB_NOTIFICACION.SP_PAB_DET_CRRO_TIBCO (NULL,
                                                     NULL,
                                                     NULL,
                                                     'PVIS',
                                                     v_CAN_OPE_NOT, -- v_cont_est_vis,
                                                     NULL,    --'MESA',--NULL,
                                                     NULL,
                                                     NULL,
                                                     NULL,
                                                     p_ADJUNTO,
                                                     p_ID,
                                                     p_MENSAJE_SWF,
                                                     p_ASUNTO,
                                                     p_CORREO,
                                                     p_ERROR);
      END IF;
   EXCEPTION
      WHEN op_REG
      THEN
         p_ERR_CODE := '1';
         p_ERR_MSG :=
               'Operacion Origen N� '
            || p_COD_SIS_ORI
            || 'ya se encuentra en Altos Montos';
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (p_ERR_CODE,
                                             v_NOM_PCK,
                                             p_ERR_MSG,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN DUP_VAL_ON_INDEX
      THEN
         p_ERR_CODE := '1';
         p_ERR_MSG := 'Valor ya ingresado en Altos Montos';
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (p_ERR_CODE,
                                             v_NOM_PCK,
                                             p_ERR_MSG,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN NO_DATA_FOUND
      THEN
         p_ERR_CODE := '1';
         p_ERR_MSG := 'No se encuentra el registro asociado';
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (p_ERR_CODE,
                                             v_NOM_PCK,
                                             p_ERR_MSG,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;

         p_ERR_MSG := SUBSTR (SQLERRM, 1, 300);
         p_ERR_CODE := SQLCODE;


         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (p_ERR_CODE,
                                             v_NOM_PCK,
                                             p_ERR_MSG,
                                             v_NOM_SP);

         p_s_mensaje := p_ERR_MSG || '-' || p_ERR_CODE;
         RAISE_APPLICATION_ERROR (-20000, p_ERR_MSG);
   END SP_ING_PAG_OB;
END PKG_PAB_CANALES;
/


GRANT EXECUTE ON DBO_PAB.PKG_PAB_CANALES TO R_APP_PAB
/

GRANT EXECUTE ON DBO_PAB.PKG_PAB_CANALES TO USR_PAB
/

