CREATE OR REPLACE /* Formatted on 18-08-2020 17:45:50 (QP5 v5.115.810.9015) */
PACKAGE DBO_PAB.PKG_PAB_PROTOCOLO
IS
   -- Package de Pl's desarrollados o modificados por GSD
   -- @Santander
   ---- **************************VARIABLE GLOBALES************************************
   v_NOM_PCK              VARCHAR2 (30) := 'PKG_PAB_PROTOCOLO';
   ERR_CODE               NUMBER := 0;
   ERR_MSG                VARCHAR2 (30000);
   v_DES                  VARCHAR2 (300);
   v_DES_BIT              VARCHAR2 (100);
   v_BANCO                VARCHAR2 (9) := '970150005';
   p_s_mensaje            VARCHAR2 (400);
   TOPE_MSJ_BLQ           NUMBER := 300;
   v_dia_ini              DATE := TRUNC (SYSDATE) + INTERVAL '1' MINUTE;
   v_dia_fin DATE
         := TRUNC (SYSDATE) + INTERVAL '23' HOUR + INTERVAL '59' MINUTE ;
   v_COD_TPO_OPE_BCOEXT   CHAR (8) := 'BCOEXT  ';

   TYPE T_NUM_FOL_NMN
   IS
      TABLE OF NUMBER
         INDEX BY PLS_INTEGER;

   -- **************************FIN VARIABLE GLOBALES************************************************


   /************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_ACT_EST_MNSJE_PRTCZ
   -- Objetivo: Procedimiento que actualiza el estado de un mensaje protocolo
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_MNSJE_PRTCZ
   -- Fecha: 31/03/17
   -- Autor: Santander
   -- Input:
   -- p_NUM_FOL_MSJ_PRZ       --> Folio mensaje protocolo
   -- p_FEC_ISR_MSJ_PRZ       --> Fecha de inserci�n
   -- p_COD_EST_AOS           --> Estado de mensaje
   -- p_GLS_EST_RCH           --> Glosa de estado rechazo
   -- p_COD_USR               --> C�digo usuario
   -- Output:
   -- p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE Sp_PAB_ACT_EST_MNSJE_PRTCZ (p_NUM_FOL_MSJ_PRZ   IN     NUMBER,
                                         p_FEC_ISR_MSJ_PRZ   IN     DATE,
                                         p_COD_EST_AOS       IN     VARCHAR2,
                                         p_GLS_EST_RCH       IN     VARCHAR2,
                                         p_COD_USR           IN     VARCHAR2,
                                         p_ERROR                OUT NUMBER);



   --**********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_BUS_MNSJE_PRTCZ
   -- Objetivo: Funcion para realizar busqueda de mensajerias de protocolo
   -- 799 / 199 / 299/ 298 / 198
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_MNSJE_PRTCZ, PABS_DT_DETLL_MNSJE_PRTCZ, PABS_DT_ESTDO_ALMOT
   -- Fecha: 31/03/17
   -- Autor: Santander
   -- Input:
   -- p_NUM_FOL_MSJ_PRZ     -->  Folio mensaje protocolo
   -- p_COD_EST_AOS         -->  C�digo estado mensaje
   -- p_NUM_REF_SWF         -->  Referencia de mensaje
   -- p_COD_MT_SWF          -->  C�digo mensaje
   -- p_COD_BCO_DTN         -->  C�digo banco destino
   -- p_FLG_EGR_ING         -->  Flag ingreso / egreso
   -- p_FEC_DESDE           -->  Fecha incio b�squeda
   -- p_FEC_HASTA           -->  Fecha fin b�squeda
   -- p_REF_ORI             -->  Referencia operaci�n original
   -- Output:
   -- p_CURSOR ->  Lista los mensajes de protocolo
   -- p_ERROR -> Indicada si el procedimiento
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de utimos cambios>
   --***********************************************************************************************
   PROCEDURE SP_PAB_BUS_MNSJE_PRTCZ (
      p_NUM_FOL_MSJ_PRZ   IN     NUMBER,
      p_COD_EST_AOS       IN     VARCHAR2,
      p_NUM_REF_SWF       IN     CHAR,
      p_COD_MT_SWF        IN     NUMBER,
      p_COD_BCO_DTN       IN     CHAR,
      p_FLG_EGR_ING       IN     NUMBER,
      p_FEC_DESDE         IN     DATE,
      p_FEC_HASTA         IN     DATE,
      p_REF_ORI           IN     CHAR,
      p_CURSOR               OUT SYS_REFCURSOR,
      p_ERROR                OUT NUMBER
   );

   --******************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_ACT_MSN_PRT
   -- Objetivo: Procedimiento que actualiza mensaje de protocolo
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_MNSJE_PRTCZ, PABS_DT_DETLL_MNSJE_PRTCZ
   -- Fecha: 31/03/17
   -- Autor: Santander
   -- Input:
   --p_NUM_FOL_MSJ_PRZ    --> Numero foliador mensajeria protocolo
   --p_COD_BCO_BFC        --> Codigo banco beneficiario
   --p_NUM_REF_SWF        --> Numero de referencia
   --p_COD_EST_AOS        --> C�digo estado
   --p_FEC_ISR_MSJ_PRZ    --> Fecha insercion mensajeria protocolo
   --p_NUM_FOL_OPE_ORI    --> Numero folio operacion original
   --p_COD_TPO_OPE        --> Estado tipo operaci�n
   -- Output:
   -- p_ERROR             --> Indicador de errores (0 Sin errores :: 1 existen errores). Se ven en la tabla Log estos errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ?ltimos cambios>
   --***********************************************************************************************
   PROCEDURE Sp_PAB_ACT_MSN_PRT (p_NUM_FOL_MSJ_PRZ   IN     NUMBER,
                                 p_COD_BCO_BFC       IN     CHAR,
                                 p_NUM_REF_SWF       IN     CHAR,
                                 p_COD_EST_AOS       IN     VARCHAR2,
                                 p_FEC_ISR_MSJ_PRZ   IN     DATE,
                                 p_NUM_FOL_OPE_ORI   IN     NUMBER,
                                 p_COD_TPO_OPE       IN     CHAR,
                                 p_ERROR                OUT NUMBER);


   --************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_GES_MSJ_EDIT_PROT
   -- Objetivo: Gestiona la edici�n de mensaje de protocolo.
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas:
   -- Fecha: 31/03/17
   -- Autor: Santander
   -- p_NUM_REF_SWF            --> Numero de referencia
   -- p_NUM_FOL_MSJ_PRZ        --> Numero de folio de mensajeria
   -- p_FEC_ISR_MSJ_PRZ        --> Fecha de inserci�n
   -- p_COD_MT_SWF             --> Codigo mensaje
   -- p_COD_EST_AOS            --> Codigo estado
   -- p_COD_BCO_BFC            --> Bic banco beneficiario
   -- p_NUM_FOL_OPE_ORI        --> Numero folio operacion origen
   -- p_COD_TPO_OPE            --> Codigo tipo operacion
   -- p_GLS_TXT_LBE_MSJ         --> Glosa del texto de 298
   -- p_COD_USR                --> Codigo usuario
   -- Output:
   -- p_ERROR          --> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores
   -- Retorno: N/A.
   --***********************************************************************************************
   PROCEDURE Sp_PAB_GES_MSJ_EDIT_PROT (p_NUM_REF_SWF       IN     CHAR,
                                       p_NUM_FOL_MSJ_PRZ   IN     NUMBER,
                                       p_FEC_ISR_MSJ_PRZ   IN     DATE,
                                       p_COD_MT_SWF        IN     CHAR,
                                       p_COD_EST_AOS       IN     VARCHAR2,
                                       p_COD_BCO_BFC       IN     CHAR,
                                       p_NUM_FOL_OPE_ORI   IN     NUMBER,
                                       p_COD_TPO_OPE       IN     CHAR,
                                       p_GLS_TXT_LBE_MSJ   IN     VARCHAR2,
                                       p_COD_USR           IN     VARCHAR2,
                                       p_ERROR                OUT NUMBER);

   --***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_MENSAJE_MOTIVO_SWIFT
   -- Objetivo: PROCEDIMIENTO ALMACENADO QUE DEVUELVE LA DESCRICI�N DE MEJSAJES 298 SEGUN TIPO DE OPERACION.
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_DETLL_OPRCN, PABS_DT_TIPO_OPRCN
   -- Fecha: 31/03/17
   -- Autor: Santander
   -- Input:
   -- p_NUM_OPE         --> N�mero de folio
   -- p_FECHA_INS       --> Fecha de inserci�n
   -- p_COD_TPO_OPE     --> Tipo de operaci�n
   -- Output:
   -- MENSAJE_SWF       --> Mensaje de 298
   -- p_ERROR           --> Indicador de errores, maneja dos estados, 0 sin errores 1 existen errores
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************
   PROCEDURE Sp_PAB_MENSAJE_MOTIVO_SWIFT (p_NUM_OPE       IN     NUMBER,
                                          p_FECHA_INS     IN     DATE,
                                          p_COD_TPO_OPE   IN     CHAR,
                                          MENSAJE_SWF        OUT VARCHAR2,
                                          p_ERROR            OUT NUMBER);

   --**********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_MNSJE_PRTCZ_HIST
   -- Objetivo: Funcion para realizar busqueda de mensajerias de protocolo
   -- 799 / 199 / 298 / 198
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_TIPO_OPRCN, PABS_DT_DETLL_MNSJE_PRTCZ_HTRC, PABS_DT_MNSJE_PRTCZ_HTRCA, PABS_DT_DETLL_OPRCN_HTRCA
   -- Fecha: 31/03/17
   -- Autor: Santander
   -- Input:
   -- p_NUM_FOL_MSJ_PRZ     -> Folio mensaje
   -- p_FEC_ISR_MSJ_PRZ     -> Fecha inserci�n
   -- p_FEC_CARGA           -> Fecha carga
   -- p_ORIGEN              -> Origen de b�squeda de mensaje
   -- Output:
   -- p_CURSOR              -> Lista los mensajes de protocolo
   -- p_ERROR               -> Indicador de errores (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de utimos cambios>
   --***********************************************************************************************--
   PROCEDURE SP_PAB_MNSJE_PRTCZ_HIST (
      p_NUM_FOL_MSJ_PRZ   IN     NUMBER,
      p_FEC_ISR_MSJ_PRZ   IN     DATE,
      p_FEC_CARGA         IN     DATE,
      p_ORIGEN            IN     CHAR,
      p_CURSOR               OUT SYS_REFCURSOR,
      p_ERROR                OUT NUMBER
   );

   --**********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_MNSJE_PRTCZ
   -- Objetivo: Funcion para realizar busqueda de mensajerias de protocolo
   -- 799 / 199 / 299 / 298 / 198
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_TIPO_OPRCN, PABS_DT_DETLL_MNSJE_PRTCZ, PABS_DT_MNSJE_PRTCZ, PABS_DT_DETLL_OPRCN
   -- Fecha: 31/03/17
   -- Autor: Santander
   -- Input:
   -- p_NUM_FOL_MSJ_PRZ  -> Numero folio mensaje
   -- p_FEC_ISR_MSJ_PRZ  -> Fecha de inserci�n
   -- Output:
   -- p_CURSOR          ->  Lista de mensaje de protocolo
   -- p_ERROR           -> Indicador de errores (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de utimos cambios>
   --***********************************************************************************************
   PROCEDURE SP_PAB_MNSJE_PRTCZ (p_NUM_FOL_MSJ_PRZ   IN     NUMBER,
                                 p_FEC_ISR_MSJ_PRZ   IN     DATE,
                                 p_CURSOR               OUT SYS_REFCURSOR,
                                 p_ERROR                OUT NUMBER);


   --***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_COMBO_MOTIVO_SWIFT
   -- Objetivo: PROCEDIMIENTO ALMACENADO QUE DEVUELVE MOTIVOS ASOCIADOS A UN MENSAJE.
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_TIPO_OPRCN
   -- Fecha: 29/12/16
   -- Autor: Santander
   -- Input:
   -- N/A
   -- Output:
   -- p_CURSOR          -> Lista de motivos para mensajes
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************
   PROCEDURE Sp_PAB_COMBO_MOTIVO_SWIFT (p_CURSOR OUT SYS_REFCURSOR);


   --**********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_BUS_OPE_SWF
   -- Objetivo: Procedimiento que realiza b�squeda de operaciones Swift para asociar a mensajer�a de protocolo
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_DETLL_OPRCN
   -- Fecha: 03/01/2017
   -- Autor: Santander
   -- Input:
   -- p_STR_FLG_MRD       -> Flag
   -- p_COD_BCO_DTN       -> Bic banco destino
   -- p_NUM_REF_SWF       -> Referencia mensaje
   -- p_FEC_VTA           -> Fecha valuta
   -- p_IMp_OPE           -> Monto
   -- p_FLG_EGR_ING       -> Flag egreso ingreso
   -- p_COD_EST_AOS       -> Estado
   -- Output:
   -- p_CURSOR_DETLL_OPE  -> Devuelve operaciones para relicionar a un mensaje 298
   -- p_ERROR -> Indicador de errores (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de utimos cambios>
   --***********************************************************************************************
   PROCEDURE SP_PAB_BUS_OPE_SWF (p_STR_FLG_MRD        IN     NUMBER,
                                 p_COD_BCO_DTN        IN     CHAR,
                                 p_COD_BCO_BNF        IN     CHAR,
                                 p_NUM_REF_SWF        IN     CHAR,
                                 p_FEC_VTA            IN     DATE,
                                 p_IMp_OPE            IN     NUMBER,
                                 p_FLG_EGR_ING        IN     NUMBER,
                                 p_COD_EST_AOS        IN     NUMBER,
                                 p_CURSOR_DETLL_OPE      OUT SYS_REFCURSOR,
                                 p_ERROR                 OUT NUMBER);


   --******************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_INS_MSN_PRTCZ
   -- Objetivo: Procedimiento que inserta menjase de protocolo
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_MNSJE_PRTCZ
   -- Fecha: 04/01/17
   -- Autor: Santander
   -- Input:
   --p_NUM_SUB_MSJ_RLD   -> Numero de submenaje
   --p_NUM_REF_SWF_RLD   -> referencia relacionada
   --p_NUM_REF_SWF       -> referencia mensaje
   --p_NUM_FOL_MSJ_PRZ   -> Folio mensaje
   --p_HOR_ENV_RCP_SWF   -> Hora swift
   --p_GLS_EST_RCH       -> Glosa rechazo
   --p_FLG_MRD_UTZ_SWF   -> Flag de divisa
   --p_FLG_EGR_ING       -> Falg egreso ingreso
   --p_FEC_ISR_MSJ_PRZ   -> Fecha inserci�n mensaje
   --p_FEC_ENV_RCP_SWF   -> Fecha swift
   --p_COD_MT_SWF        -> Tipo de memsaje
   --p_COD_EST_AOS       -> Estado de mensaje
   --p_COD_BCO_BFC       -> Bic beneficiario
   --p_COD_ADC_EST       -> Codigo estado adicional
   --p_NUM_FOL_OPE_ORI   -> Folio operaci�n original
   --p_COD_TPO_OPE       -> Tipo operaci�n
   -- Output:
   -- p_ERROR            -> Indicador de errores (0 Sin errores :: 1 existen errores). Se ven en la tabla Log estos errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ?ltimos cambios>
   --***********************************************************************************************
   PROCEDURE Sp_PAB_INS_MSN_PRTCZ (p_NUM_SUB_MSJ_RLD   IN     CHAR,
                                   p_NUM_REF_SWF_RLD   IN     CHAR,
                                   p_NUM_REF_SWF       IN     CHAR,
                                   p_NUM_FOL_MSJ_PRZ   IN     NUMBER,
                                   p_HOR_ENV_RCP_SWF   IN     NUMBER,
                                   p_GLS_EST_RCH       IN     VARCHAR2,
                                   p_FLG_MRD_UTZ_SWF   IN     NUMBER,
                                   p_FLG_EGR_ING       IN     NUMBER,
                                   p_FEC_ISR_MSJ_PRZ   IN     DATE,
                                   p_FEC_ENV_RCP_SWF   IN     DATE,
                                   p_COD_MT_SWF        IN     CHAR,
                                   p_COD_EST_AOS       IN     NUMBER,
                                   p_COD_BCO_BFC       IN     CHAR,
                                   p_COD_ADC_EST       IN     CHAR,
                                   p_NUM_FOL_OPE_ORI   IN     NUMBER,
                                   p_FEC_ISR_OPE_ORI   IN     DATE,
                                   p_COD_TPO_OPE       IN     CHAR,
                                   p_ERROR                OUT NUMBER);

   --******************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_INS_DETLL_MSN_PRTCZ
   -- Objetivo: Procedimiento que inserta detalle del menjase de protocolo
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_DETLL_MNSJE_PRTCZ
   -- Fecha: 04/01/17
   -- Autor: Santander
   -- Input:
   -- p_NUM_FOL_MSJ_PRZ     -> Folio mensaje
   -- p_FEC_ISR_MSJ_PRZ     -> Fecha inserci�n
   -- p_GLS_TXT_LBE_MSJ     -> Detalle mensaje
   -- Output:
   -- p_ERROR               -> Indicador de errores (0 Sin errores :: 1 existen errores). Se ven en la tabla Log estos errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ?ltimos cambios>
   --***********************************************************************************************
   PROCEDURE Sp_PAB_INS_DETLL_MSN_PRTCZ (p_NUM_FOL_MSJ_PRZ   IN     NUMBER,
                                         p_FEC_ISR_MSJ_PRZ   IN     DATE,
                                         p_GLS_TXT_LBE_MSJ   IN     VARCHAR2,
                                         p_ERROR                OUT NUMBER);


   /********************************************************************************************************
    FUNCION/PROCEDIMIENTO: Sp_PAB_COMBO_MT_SWIFT_PRO
    OBJETIVO             : MUESTRA FILTRO EN
                            MENSAJES -> ENVIADO - PROTOCOLO -> CREAR.
                            MENSAJES -> ENVIADO - PROTOCOLO -> CURSAR.
                            MENSAJES -> ENVIADO - PROTOCOLO -> AUTORIZAR.
    SISTEMA              : DBO_PAB.
    BASE DE DATOS        : DBO_PAB.
    TABLAS USADAS        : PABS_DT_TIPO_MT_SWIFT.
    FECHA                : 27/12/16
    AUTOR                :
    INPUT                : P_CANAL  := CANAL.
    OUTPUT               : P_CURSOR := DATOS CURSOR.
    OBSERVACIONES
    FECHA       USUARIO    DETALLE
    20-09-2018  VARAYA     SE MODIFICA FILTRO.
    ********************************************************************************************************/
   PROCEDURE Sp_PAB_COMBO_MT_SWIFT_PRO (p_CURSOR OUT SYS_REFCURSOR);

   /********************************************************************************************************
   FUNCION/PROCEDIMIENTO: PKG_PAB_PROTOCOLO.SP_PAB_COMBO_AVISOS_PROT.
   OBJETIVO             : MUESTRA FILTRO EN
                           MENSAJES -> ENVIADO - PROTOCOLO -> CREAR.
                           MENSAJES -> ENVIADO - PROTOCOLO -> CURSAR.
                           MENSAJES -> ENVIADO - PROTOCOLO -> AUTORIZAR.
                           CONTINGENCIA -> ENVIADO -> COMBANC UPLOAD - DESCARGAR ARCHIVO.
   SISTEMA              : DBO_PAB.
   BASE DE DATOS        : DBO_PAB.
   TABLAS USADAS        : PABS_DT_TIPO_MT_SWIFT.
   FECHA                : 20/09/2018.
   AUTOR                : VARAYA.
   INPUT                : P_CANAL  := CANAL.
   OUTPUT               : P_CURSOR := DATOS CURSOR.
   OBSERVACIONES
   FECHA       USUARIO    DETALLE
   20-09-2018  VARAYA     CREACI�N.
   ********************************************************************************************************/
   PROCEDURE SP_PAB_COMBO_AVISOS_PROT (P_CURSOR OUT SYS_REFCURSOR);

   --**********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_BUS_MNSJE_PRTCZ_HIST
   -- Objetivo: Funci�n para realizar b�squeda de mensajerias hist�rica de protocolo
   -- 799 / 199 / 299 / 298 / 198
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_MNSJE_PRTCZ_HTRCA, PABS_DT_ESTDO_ALMOT, PABS_DT_MNSJE_PRTCZ
   -- Fecha: 30/11/16
   -- Autor: Santander
   -- Input:
   -- p_NUM_FOL_MSJ_PRZ          Folio mensaje
   -- p_COD_EST_AOS              Estado mensaje
   -- p_NUM_REF_SWF              Ref mensaje
   -- p_COD_DVI                  Mercado
   -- p_COD_MT_SWF               Tipo mensaje
   -- p_COD_BCO_DTN              Bic banco
   -- p_FLG_EGR_ING              Flujo
   -- p_FEC_DESDE                Fecha rango desde
   -- p_FEC_HASTA                Fecha rango hasta
   -- Output:
   -- p_CURSOR                    Lista de Mensajes de protocolo
   -- p_ERROR                    Indicador de errores (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de utimos cambios>
   --***********************************************************************************************
   PROCEDURE SP_PAB_BUS_MNSJE_PRTCZ_HIST (
      p_NUM_FOL_MSJ_PRZ   IN     NUMBER,
      p_COD_EST_AOS       IN     NUMBER,
      p_NUM_REF_SWF       IN     CHAR,
      p_COD_DVI           IN     CHAR,
      p_COD_MT_SWF        IN     NUMBER,
      p_COD_BCO_DTN       IN     CHAR,
      p_COD_BCO_ORI       IN     CHAR,
      p_FLG_EGR_ING       IN     NUMBER,
      p_FEC_DESDE         IN     DATE,
      p_FEC_HASTA         IN     DATE,
      p_CURSOR               OUT SYS_REFCURSOR,
      p_ERROR                OUT NUMBER
   );

   --************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_GES_MSJ_PROT
   -- Objetivo: Gestiona la inserci�n de mensaje de protocolo.
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_DETLL_OPRCN
   -- Fecha: 16/03/2017
   -- Autor: Santander
   -- p_NUM_SUB_MSJ_RLD  -> N�mero submensaje
   -- p_NUM_REF_SWF_RLD  -> Referencia relacionada
   -- p_NUM_REF_SWF      -> Referencia de mensaje
   -- p_HOR_ENV_RCP_SWF  -> Hora swift
   -- p_GLS_EST_RCH      -> Glosa rechazo
   -- p_FLG_MRD_UTZ_SWF  -> Flag de mercado
   -- p_FLG_EGR_ING      -> Flag flujo
   -- p_FEC_ENV_RCP_SWF  -> Fecha wsift
   -- p_COD_MT_SWF       -> Tipo de mensaje
   -- p_COD_EST_AOS      -> Estado mensaje
   -- p_COD_BCO_BFC      -> Bic Beneficiario
   -- p_COD_ADC_EST      -> Estado adicional
   -- p_NUM_FOL_OPE_ORI  -> N�mero de operaci�n
   -- p_FEC_OPE_ORI      -> N�mero de operaci�n
   -- p_COD_TPO_OPE      -> Tipo de operaci�n
   -- p_NUM_CAM_REG      -> N�mero campo por registro
   -- p_GLS_TXT_LBE_MSJ  -> Glosa texto libre
   -- p_COD_USR          -> C�digo usuario
   -- Output:
   -- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores
   -- p_NUM_FOL_MSJ_PRZ  -> Folio mensaje
   -- p_FEC_ISR_MSJ_PRZ  -> fecha mensaje
   -- Retorno: N/A.
   --***********************************************************************************************
   --
   PROCEDURE Sp_PAB_GES_MSJ_PROT (p_NUM_SUB_MSJ_RLD   IN     CHAR,
                                  p_NUM_REF_SWF_RLD   IN     CHAR,
                                  p_NUM_REF_SWF       IN     CHAR,
                                  p_HOR_ENV_RCP_SWF   IN     NUMBER,
                                  p_GLS_EST_RCH       IN     VARCHAR2,
                                  p_FLG_MRD_UTZ_SWF   IN     NUMBER,
                                  p_FLG_EGR_ING       IN     NUMBER,
                                  p_FEC_ENV_RCP_SWF   IN     DATE,
                                  p_COD_MT_SWF        IN     CHAR,
                                  p_COD_EST_AOS       IN     VARCHAR2,
                                  p_COD_BCO_BFC       IN     CHAR,
                                  p_COD_ADC_EST       IN     CHAR,
                                  p_NUM_FOL_OPE_ORI   IN     NUMBER,
                                  p_FEC_OPE_ORI       IN     DATE,
                                  p_COD_TPO_OPE       IN     CHAR,
                                  p_NUM_CAM_REG       IN     NUMBER,
                                  p_GLS_TXT_LBE_MSJ   IN     VARCHAR2,
                                  p_COD_USR           IN     VARCHAR2,
                                  p_NUM_FOL_MSJ_PRZ      OUT NUMBER,
                                  p_FEC_ISR_MSJ_PRZ      OUT VARCHAR2,
                                  p_ERROR                OUT NUMBER);

   /************************************************************************************************
   -- Funcion/Procedimiento: FN_PAB_GEN_REF_SWF_PRT
   -- Objetivo: Fusion que retorne el codifo de referencia Swift para env�o  mensajeria de protocolo
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: N/A
   -- Fecha: 02/06/16
   -- Autor: Santander
   -- Input:      p_COD_TPO_OPE:      Tipo de mensaje de protocolo
   -- Output:
   --          Retorna el codigo de referencia Swift Protocolo
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   FUNCTION FN_PAB_GEN_REF_SWF_PRT (p_COD_TPO_OPE IN CHAR)
      RETURN VARCHAR2;


   --******************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_ACT_MSN_PRT_SWF
   -- Objetivo: Procedimiento que actualiza mensaje de protocolo tras envio swift
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_MNSJE_PRTCZ,
   -- Fecha: 31/03/17
   -- Autor: Santander
   -- Input:
   --p_NUM_FOL_MSJ_PRZ    --> Numero foliador mensajeria protocolo
   --p_FEC_ISR_MSJ_PRZ    --> Fecha insercion mensajeria protocolo
   --p_NUM_REF_SWF        --> Numero de referencia
   --p_COD_EST_AOS        -->  C�digo estado
   --p_FEC_ENV_RCP_SWF    -->  Fecha recepci�n swift
   --p_HOR_ENV_RCP_SWF    -->  Hora recepci�n swift
   --p_FLG_ENV_RCP        -->  Flag env�o (0) o recpeci�n (1)
   --p_MSJ_EST_RCH        -->  Mensaje rechazo u observaci�n
   --p_COD_USR            -->  Rut usuario
   -- Output:
   -- p_ERROR             --> Indicador de errores (0 Sin errores :: 1 existen errores). Se ven en la tabla Log estos errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ?ltimos cambios>
   --***********************************************************************************************

   PROCEDURE Sp_PAB_ACT_MSN_PRT_SWF (p_NUM_FOL_MSJ_PRZ   IN     NUMBER,
                                     p_FEC_ISR_MSJ_PRZ   IN     DATE,
                                     p_NUM_REF_SWF       IN     CHAR,
                                     p_COD_EST_AOS       IN     VARCHAR2,
                                     p_FEC_ENV_RCP_SWF   IN     DATE,
                                     p_HOR_ENV_RCP_SWF   IN     NUMBER,
                                     p_FLG_ENV_RCP       IN     NUMBER,
                                     p_MSJ_EST_RCH       IN     VARCHAR2,
                                     p_COD_USR           IN     VARCHAR2,
                                     p_ERROR                OUT NUMBER);

   --**********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_BUS_MNSJE_ING
   -- Objetivo: Procedimiento que retorna los mt298 de ingreso
   --
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_MNSJE_PRTCZ, PABS_DT_DETLL_MNSJE_PRTCZ, PABS_DT_ESTDO_ALMOT
   -- Fecha: 12-01-2018
   -- Autor: Santander- CAH
   -- Input:
   -- p_NUM_REF_SWF         -->  Referencia de mensaje.
   -- p_COD_BCO_DTN         -->  C�digo banco origen.
   -- p_FEC_DESDE           -->  Fecha incio b�squeda.
   -- p_FEC_HASTA           -->  Fecha fin b�squeda.
   -- Output:
   -- p_CURSOR ->  Lista los mensajes de protocolo
   -- p_ERROR -> Indicada si el procedimiento
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de utimos cambios>
   --***********************************************************************************************
   PROCEDURE SP_PAB_BUS_MNSJE_ING (p_NUM_REF_SWF   IN     CHAR,
                                   p_COD_BCO_ORI   IN     CHAR,
                                   p_FEC_DESDE     IN     DATE,
                                   p_FEC_HASTA     IN     DATE,
                                   p_CURSOR           OUT SYS_REFCURSOR,
                                   p_ERROR            OUT NUMBER);

   --**********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_BUS_MNSJE_PRTCZ_HIST2
   -- Objetivo: Funci�n para realizar b�squeda de mensajerias hist�rica de protocolo
   -- 799 / 199 / 299 / 298 / 198
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_MNSJE_PRTCZ_HTRCA, PABS_DT_ESTDO_ALMOT, PABS_DT_MNSJE_PRTCZ
   -- Fecha: 30/11/16
   -- Autor: Santander
   -- Input:
   -- p_NUM_FOL_MSJ_PRZ          Folio mensaje
   -- p_COD_EST_AOS              Estado mensaje
   -- p_NUM_REF_SWF              Ref mensaje
   -- p_COD_DVI                  Mercado
   -- p_COD_MT_SWF               Tipo mensaje
   -- p_COD_BCO_DTN              Bic banco
   -- p_FLG_EGR_ING              Flujo
   -- p_FEC_DESDE                Fecha rango desde
   -- p_FEC_HASTA                Fecha rango hasta
   -- Output:
   -- p_CURSOR                    Lista de Mensajes de protocolo
   -- p_ERROR                    Indicador de errores (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de utimos cambios>
   --***********************************************************************************************
   PROCEDURE SP_PAB_BUS_MNSJE_910_900_HIST (p_FOL_OPE   IN     NUMBER,
                                            p_FEC_OPE   IN     DATE,
                                            p_CURSOR       OUT SYS_REFCURSOR,
                                            p_ERROR        OUT NUMBER);

   /***********************************************************************************************
     FUNCION/PROCEDIMIENTO: SP_PAB_ACT_COD_ENT_SAL.
     OBJETIVO             : RECORRE LOS MT 900/910 SELECCIONADOS PARA CAMBIAR EL CANAL DE ENTRADA
                            O SALIDA Y LOS ACTUALIZA.
     SISTEMA              : DBO_PAB.
     BASE DE DATOS        : DBO_PAB.
     TABLAS USADAS        : PABS_DT_DETLL_CARGO_ABONO_CAJA.
     FECHA                : 28/08/2018.
     AUTOR                : VARAYA.
     INPUT                : P_REG_OPER    := 1� REGISTRO := FECHA.
                                             2� REGISTRO := N�MERO FOLIO.
                                             3� REGISTRO := C�DIGO MT.
                            P_COD_ENT_SAL := CANAL SELECCIONADO.
                            P_COD_USR     := USUARIO GESTOR.
     OUTPUT               : P_ERROR       := INDICADOR DE ERRORES:
                                             0 := SIN ERRORES.
                                             1 := CON ERRORES.
     OBSERVACIONES        : 28-08-2018, CREACI�N SP.
   ***********************************************************************************************/
   PROCEDURE SP_PAB_ACT_COD_ENT_SAL (P_REG_OPER      IN     REG_OPER,
                                     P_COD_ENT_SAL   IN     CHAR,
                                     P_COD_USR       IN     CHAR,
                                     P_ERROR            OUT NUMBER);

   /***********************************************************************************************
     FUNCION/PROCEDIMIENTO: SP_PAB_BUS_CAMBIAR_CANAL.
     OBJETIVO             : BUSCA LOS MENSAJES DE PROTOCOLO QUE NO
     SISTEMA              : DBO_PAB.
     BASE DE DATOS        : DBO_PAB.
     TABLAS USADAS        : PABS_DT_DETLL_CARGO_ABONO_CAJA
     FECHA                : 28/08/2018.
     AUTOR                : SANTANDER
     INPUT                : p_COD_MT_SWF -> Codigo MT
                          : p_NUM_REF_SWF -> Numero de referencia Swift
                          : p_COD_TPO_OPE_AOS -> Tipo operaci�n
                          : P_COD_SIS_SAL -> Codigo sistema salida
                          : P_COD_SIS_ENT -> Codigo sistema entrada
                          : p_COD_BCO_ORG -> Bic Banco Origen
     OUTPUT               : p_CURSOR     -> Cursor con operaciones de protocolo que cumplen los filtros
                          : P_ERROR       := INDICADOR DE ERRORES:
                                             0 := SIN ERRORES.
                                             1 := CON ERRORES.
     OBSERVACIONES        : 28-08-2018, CREACI�N SP.
   ***********************************************************************************************/
   PROCEDURE SP_PAB_BUS_CAMBIAR_CANAL (
      p_COD_MT_SWF        IN     NUMBER,
      p_NUM_REF_SWF       IN     CHAR,
      p_COD_TPO_OPE_AOS   IN     CHAR,
      P_COD_SIS_SAL       IN     CHAR,
      P_COD_SIS_ENT       IN     CHAR,
      p_COD_BCO_ORG       IN     CHAR,
      p_CURSOR               OUT SYS_REFCURSOR,
      p_ERROR                OUT NUMBER
   );

   /********************************************************************************************************
   FUNCION/PROCEDIMIENTO: SP_PAB_CMB_CNLENT_CMBCNLSAL
   OBJETIVO             : Combo Canal entrada de protocolos de la pantalla cambio canal de salida
   SISTEMA              : DBO_PAB.
   BASE DE DATOS        : DBO_PAB.
   TABLAS USADAS        : PABS_DT_SISTM_ENTRD_SALID
   FECHA                : 02/06/17
   AUTOR                : SANTANDER.
   INPUT                : N/A
   OUTPUT               : P_CURSOR := DATOS CURSOR.
   OBSERVACIONES
   FECHA       USUARIO    DETALLE
   ********************************************************************************************************/
   PROCEDURE SP_PAB_CMB_CNLENT_CMBCNLSAL (P_CURSOR OUT SYS_REFCURSOR);

   /********************************************************************************************************
   FUNCION/PROCEDIMIENTO: SP_PAB_CMB_CNLSAL_CMBCNLSAL
   OBJETIVO             : Combo Canal salida de protocolos de la pantalla cambio canal de salida
   SISTEMA              : DBO_PAB.
   BASE DE DATOS        : DBO_PAB.
   TABLAS USADAS        : PABS_DT_SISTM_ENTRD_SALID
   FECHA                : 02/06/17
   AUTOR                : SANTANDER.
   INPUT                : N/A
   OUTPUT               : P_CURSOR := DATOS CURSOR.
   OBSERVACIONES
   FECHA       USUARIO    DETALLE
   ********************************************************************************************************/
   PROCEDURE SP_PAB_CMB_CNLSAL_CMBCNLSAL (P_CURSOR OUT SYS_REFCURSOR);

   /********************************************************************************************************
   FUNCION/PROCEDIMIENTO: SP_PAB_CMB_CAMBIO_CANAL
   OBJETIVO             : Combo Canal salida de protocolos de la pantalla cambio canal de salida
   SISTEMA              : DBO_PAB.
   BASE DE DATOS        : DBO_PAB.
   TABLAS USADAS        : PABS_DT_SISTM_ENTRD_SALID
   FECHA                : 02/06/17
   AUTOR                : SANTANDER.
   INPUT                : N/A
   OUTPUT               : P_CURSOR := DATOS CURSOR.
   OBSERVACIONES
   FECHA       USUARIO    DETALLE
   ********************************************************************************************************/
   PROCEDURE SP_PAB_CMB_CAMBIO_CANAL (P_CURSOR OUT SYS_REFCURSOR);

   /********************************************************************************************************
   FUNCION/PROCEDIMIENTO: PKG_PAB_PROTOCOLO.SP_PAB_COMBO_TODOS_PROT.
   OBJETIVO             : MUESTRA FILTRO EN
                           MENSAJES -> ENVIADO - PROTOCOLO -> CREAR.
                           MENSAJES -> ENVIADO - PROTOCOLO -> CURSAR.
                           MENSAJES -> ENVIADO - PROTOCOLO -> AUTORIZAR.
                           CONTINGENCIA -> ENVIADO -> COMBANC UPLOAD - DESCARGAR ARCHIVO.
   SISTEMA              : DBO_PAB.
   BASE DE DATOS        : DBO_PAB.
   TABLAS USADAS        : PABS_DT_TIPO_MT_SWIFT.
   FECHA                : 20/09/2018.
   AUTOR                : VARAYA.
   INPUT                : P_CANAL  := CANAL.
   OUTPUT               : P_CURSOR := DATOS CURSOR.
   OBSERVACIONES
   FECHA       USUARIO    DETALLE
   20-09-2018  VARAYA     CREACI�N.
   ********************************************************************************************************/
   PROCEDURE SP_PAB_COMBO_TODOS_PROT (P_CURSOR OUT SYS_REFCURSOR);
END PKG_PAB_PROTOCOLO;
/


CREATE OR REPLACE /* Formatted on 18-08-2020 17:45:50 (QP5 v5.115.810.9015) */
PACKAGE BODY DBO_PAB.PKG_PAB_PROTOCOLO
IS
   /************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_ACT_EST_MNSJE_PRTCZ
   -- Objetivo: Procedimiento que actualiza el estado de un mensaje protocolo
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_MNSJE_PRTCZ
   -- Fecha: 31/03/17
   -- Autor: Santander
   -- Input:
   -- p_NUM_FOL_MSJ_PRZ       --> Folio mensaje protocolo
   -- p_FEC_ISR_MSJ_PRZ       --> Fecha de inserci�n
   -- p_COD_EST_AOS           --> Estado de mensaje
   -- p_GLS_EST_RCH           --> Glosa de estado rechazo
   -- p_COD_USR               --> C�digo usuario
   -- Output:
   -- p_ERROR: indicador de errores, maneja dos estados, 0 sin errores 1 existen errores. (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************/
   PROCEDURE Sp_PAB_ACT_EST_MNSJE_PRTCZ (p_NUM_FOL_MSJ_PRZ   IN     NUMBER,
                                         p_FEC_ISR_MSJ_PRZ   IN     DATE,
                                         p_COD_EST_AOS       IN     VARCHAR2,
                                         p_GLS_EST_RCH       IN     VARCHAR2,
                                         p_COD_USR           IN     VARCHAR2,
                                         p_ERROR                OUT NUMBER)
   IS
      v_NOM_SP            VARCHAR2 (30) := 'Sp_PAB_ACT_EST_MNSJE_PRTCZ';
      v_COD_EST_AOS       NUMBER;
      v_COD_EST_AOS_ACT   NUMBER;
      v_RESULT            NUMBER;
      V_GLS_ADC_EST       VARCHAR2 (200) := NULL;
      v_FLG_EGR_ING       NUMBER;
      v_COD_TPO_OPE       CHAR (8);
      v_NUM_REF_SWF       CHAR (16);
      ERROR_REFSWF EXCEPTION;
   BEGIN
      --Obtenci�n de c�digo de Estado
      v_COD_EST_AOS :=
         PKG_PAB_UTILITY.FN_PAB_OBT_COD_DSC_EST (
            p_COD_EST_AOS,
            PKG_PAB_CONSTANTES.V_IND_OPE
         );

      BEGIN
         --Rescatamos estado actual
         SELECT   COD_EST_AOS,
                  FLG_EGR_ING,
                  COD_TPO_OPE,
                  NUM_REF_SWF
           INTO   v_COD_EST_AOS_ACT,
                  v_FLG_EGR_ING,
                  v_COD_TPO_OPE,
                  v_NUM_REF_SWF
           FROM   PABS_DT_MNSJE_PRTCZ PT
          WHERE   NUM_FOL_MSJ_PRZ = p_NUM_FOL_MSJ_PRZ
                  AND FEC_ISR_MSJ_PRZ = p_FEC_ISR_MSJ_PRZ;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            err_code := SQLCODE;
            err_msg :=
               'No se pudo obtener la informaci�n de la operaci�n origen:'
               || p_NUM_FOL_MSJ_PRZ;
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            RAISE;
         WHEN OTHERS
         THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            p_s_mensaje := err_code || '-' || err_msg;
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
      END;

      IF (v_COD_EST_AOS_ACT IS NULL)
      THEN
         RAISE ERROR_REFSWF;
      END IF;

      --Actualizamos estado nuevo
      UPDATE   PABS_DT_MNSJE_PRTCZ
         SET   COD_EST_AOS = v_COD_EST_AOS, GLS_EST_RCH = p_GLS_EST_RCH
       WHERE   NUM_FOL_MSJ_PRZ = p_NUM_FOL_MSJ_PRZ
               AND FEC_ISR_MSJ_PRZ = p_FEC_ISR_MSJ_PRZ;

      --Llamamos al procedimiento de Bitacora
      v_DES_BIT := 'Cambio de estado operacion mensaje protocolo';
      PKG_PAB_TRACKING.Sp_PAB_INS_BIT_OPE (p_NUM_FOL_MSJ_PRZ,
                                           p_FEC_ISR_MSJ_PRZ,
                                           p_COD_USR,
                                           v_COD_EST_AOS_ACT,
                                           v_COD_EST_AOS,
                                           v_DES_BIT,
                                           v_RESULT);

      IF v_COD_EST_AOS = PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPECUR
      THEN
         err_msg := 'Generando notificacion cursada';
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (NULL,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);

         --Notificaci�n por autorizar
         PKG_PAB_NOTIFICACION.Sp_PAB_GES_NTFCC_ALMOT (NULL,
                                                      p_NUM_FOL_MSJ_PRZ,
                                                      p_FEC_ISR_MSJ_PRZ,
                                                      NULL,
                                                      'MNCUR',
                                                      'OOFF',
                                                      p_ERROR);
      ELSIF v_COD_EST_AOS = PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEREC
      THEN
         --Notificaci�n por autorizar
         PKG_PAB_NOTIFICACION.Sp_PAB_GES_NTFCC_ALMOT (NULL,
                                                      p_NUM_FOL_MSJ_PRZ,
                                                      p_FEC_ISR_MSJ_PRZ,
                                                      NULL,
                                                      'MNREC',
                                                      'OOFF',
                                                      p_ERROR);

         err_msg := 'Generando notificacion rechazada';
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (NULL,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
      END IF;

      COMMIT;
      p_ERROR := PKG_PAB_CONSTANTES.v_OK;
   EXCEPTION
      WHEN ERROR_REFSWF
      THEN
         err_msg :=
            'No se encontro mensaje con el codigo de referencia:'
            || p_NUM_FOL_MSJ_PRZ;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (NULL,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         p_s_mensaje := err_code || '-' || err_msg;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END Sp_PAB_ACT_EST_MNSJE_PRTCZ;

   --**********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_BUS_MNSJE_PRTCZ
   -- Objetivo: Funcion para realizar busqueda de mensajerias de protocolo
   -- 799 / 199 / 299/ 298 / 198
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_MNSJE_PRTCZ, PABS_DT_DETLL_MNSJE_PRTCZ, PABS_DT_ESTDO_ALMOT
   -- Fecha: 31/03/17
   -- Autor: Santander
   -- Input:
   -- p_NUM_FOL_MSJ_PRZ     -->  Folio mensaje protocolo
   -- p_COD_EST_AOS         -->  C�digo estado mensaje
   -- p_NUM_REF_SWF         -->  Referencia de mensaje
   -- p_COD_MT_SWF          -->  C�digo mensaje
   -- p_COD_BCO_DTN         -->  C�digo banco destino
   -- p_FLG_EGR_ING         -->  Flag ingreso / egreso
   -- p_FEC_DESDE           -->  Fecha incio b�squeda
   -- p_FEC_HASTA           -->  Fecha fin b�squeda
   -- p_REF_ORI             -->  Referencia operaci�n original
   -- Output:
   -- p_CURSOR ->  Lista los mensajes de protocolo
   -- p_ERROR -> Indicada si el procedimiento
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de utimos cambios>
   --***********************************************************************************************
   --
   PROCEDURE SP_PAB_BUS_MNSJE_PRTCZ (
      p_NUM_FOL_MSJ_PRZ   IN     NUMBER,
      p_COD_EST_AOS       IN     VARCHAR2,
      p_NUM_REF_SWF       IN     CHAR,
      p_COD_MT_SWF        IN     NUMBER,
      p_COD_BCO_DTN       IN     CHAR,
      p_FLG_EGR_ING       IN     NUMBER,
      p_FEC_DESDE         IN     DATE,
      p_FEC_HASTA         IN     DATE,
      p_REF_ORI           IN     CHAR,
      p_CURSOR               OUT SYS_REFCURSOR,
      p_ERROR                OUT NUMBER
   )
   IS
      v_NOM_SP                   VARCHAR2 (30) := 'SP_PAB_BUS_MNSJE_PRTCZ';
      v_IN_OUT_NOVAL             NUMBER := 2;
      v_COD_EST_AOS              NUMBER;
      v_REF_ORI                  CHAR (16) := p_REF_ORI;
      v_COD_BCO_DTN              CHAR (11) := p_COD_BCO_DTN;
      V_COD_EST_AOS_OPENDE_BUS   NUMBER;
      V_COD_EST_AOS_OPEASO_BUS   NUMBER;
      v_NUM_REF_SWF              VARCHAR2 (16) := UPPER (p_NUM_REF_SWF);
      v_NUM_REF_SWF2             VARCHAR2 (16);
      v_FEC_DESDE                DATE;
      v_FEC_HASTA                DATE;
   BEGIN
      IF p_COD_EST_AOS IS NOT NULL
      THEN
         v_COD_EST_AOS :=
            PKG_PAB_UTILITY.FN_PAB_OBT_COD_DSC_EST (
               p_COD_EST_AOS,
               PKG_PAB_CONSTANTES.V_IND_OPE
            );
      END IF;

      --Diferenciamos si esta buscando mt298 recibidos
      IF p_REF_ORI IS NULL
      THEN
         V_COD_EST_AOS_OPENDE_BUS := PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEASO;
         V_COD_EST_AOS_OPEASO_BUS := PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPENDE;
      ELSE
         --Mostrar Detalle operacion 298 solicitud de devolucion
         V_COD_EST_AOS_OPENDE_BUS := -1;
         V_COD_EST_AOS_OPEASO_BUS := -1;
      END IF;

      IF (p_FEC_DESDE IS NOT NULL)
      THEN
         v_FEC_DESDE := p_FEC_DESDE + INTERVAL '1' MINUTE;
      ELSE
         v_FEC_DESDE := v_dia_ini;          --Dia de sistema mas hora 00:00:01
      END IF;

      IF (p_FEC_HASTA IS NOT NULL)
      THEN
         v_FEC_HASTA :=
            p_FEC_HASTA + INTERVAL '23' HOUR + INTERVAL '59' MINUTE;
      ELSE
         v_FEC_HASTA := v_dia_fin;          --Dia de sistema mas hora 23:59:00
      END IF;

      IF v_COD_EST_AOS = PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEREC
      THEN
         v_NUM_REF_SWF2 := NULL;
      END IF;

      --Buscamos las operaciones
      OPEN p_CURSOR FOR
           SELECT   DETOPE.NUM_FOL_MSJ_PRZ AS FOLIO,
                    DETOPE.COD_EST_AOS AS EST_OPE,
                    EST.DSC_EST_AOS AS DSC_EST,
                    DETOPE.FEC_ISR_MSJ_PRZ AS FECHA,
                    DETOPE.COD_MT_SWF AS MT,
                    DETOPE.NUM_REF_SWF AS REF_SWIFT_ORI,
                    DECODE (DETOPE.FLG_EGR_ING,
                            PKG_PAB_CONSTANTES.V_FLG_ENV,
                            PKG_PAB_CONSTANTES.V_STR_FLG_ENV,
                            PKG_PAB_CONSTANTES.V_STR_FLG_REC)
                       AS FLUJO,
                    PKG_PAB_UTILITY.FN_PAB_OBT_DSC_BCO (DETOPE.COD_BCO_BFC)
                       AS DSC_BCO_DTN,
                    --AS BCO_DTN,--AS DSC_BCO_DTN,
                    DECODE (DETOPE.FLG_EGR_ING,
                            PKG_PAB_CONSTANTES.V_FLG_ENV, DETOPE.COD_BCO_BFC,
                            DETOPE.COD_BCO_ORI)
                       AS COD_BCO,
                    --DETOPE.COD_BCO_BFC          AS COD_BCO,
                    DETOPE.COD_TPO_OPE AS TIP_OP,
                    DETOPE.NUM_FOL_MSJ_PRZ AS NUM_OPE_298,
                    DETOPE.FEC_ISR_MSJ_PRZ AS FEC_OPE_298,
                    DETOPE.NUM_REF_SWF_RLD AS REF_ORI,
                    DECODE (DETOPE.FLG_MRD_UTZ_SWF,
                            PKG_PAB_CONSTANTES.v_FLG_MRD_MN,
                            'Mercado Nacional',
                            'Mercado Extranjero')
                       AS MERCADO,
                    DETOPE.COD_TPO_OPE AS MOTIVO,
                    --rtrim(xmlagg(xmlelement(PABS_DT_DETLL_MNSJE_PRTCZ,GLS_TXT_LBE_MSJ || CHR(13) || CHR(10))).extract('//text()'),CHR(13) || CHR(10))
                    '' AS CAMPO77,
                    DETOPE.NUM_FOL_OPE_ORI AS NUM_FOL_OPE_ORI,
                    DETOPE.FEC_ISR_OPE_ORG AS FEC_ISR_OPE_ORI,
                    DETOPE.GLS_EST_RCH AS GLOSA_RECHAZO
             FROM   PABS_DT_MNSJE_PRTCZ DETOPE,
                    PABS_DT_DETLL_MNSJE_PRTCZ MSJPRT,
                    PABS_DT_ESTDO_ALMOT EST
            WHERE       DETOPE.NUM_FOL_MSJ_PRZ = MSJPRT.NUM_FOL_MSJ_PRZ
                    AND DETOPE.FEC_ISR_MSJ_PRZ = MSJPRT.FEC_ISR_MSJ_PRZ
                    AND DETOPE.COD_EST_AOS = EST.COD_EST_AOS
                    AND DETOPE.NUM_FOL_MSJ_PRZ =
                          NVL (p_NUM_FOL_MSJ_PRZ, DETOPE.NUM_FOL_MSJ_PRZ)
                    AND DETOPE.FEC_ISR_MSJ_PRZ BETWEEN v_FEC_DESDE
                                                   AND  v_FEC_HASTA
                    AND NVL (UPPER (DETOPE.NUM_REF_SWF), '#') LIKE
                          NVL (v_NUM_REF_SWF,
                               NVL (UPPER (DETOPE.NUM_REF_SWF), '#'))
                          || '%'
                    --AND DETOPE.NUM_REF_SWF IS NULL
                    AND DETOPE.COD_MT_SWF =
                          NVL (p_COD_MT_SWF, DETOPE.COD_MT_SWF)
                    AND NVL (DETOPE.COD_BCO_ORI, '#') =
                          NVL (v_COD_BCO_DTN, DETOPE.COD_BCO_ORI)
                    AND DETOPE.FLG_EGR_ING =
                          NVL (p_FLG_EGR_ING, DETOPE.FLG_EGR_ING)
                    AND DETOPE.COD_EST_AOS =
                          NVL (v_COD_EST_AOS, DETOPE.COD_EST_AOS)
                    AND NVL (DETOPE.NUM_REF_SWF_RLD, '#') LIKE
                          NVL (v_REF_ORI, NVL (DETOPE.NUM_REF_SWF_RLD, '#'))
                          || '%'
                    AND DETOPE.COD_EST_AOS NOT IN
                             (V_COD_EST_AOS_OPENDE_BUS,
                              V_COD_EST_AOS_OPEASO_BUS)
         GROUP BY   DETOPE.NUM_FOL_MSJ_PRZ,
                    DETOPE.COD_EST_AOS,
                    EST.DSC_EST_AOS,
                    DETOPE.FEC_ISR_MSJ_PRZ,
                    DETOPE.COD_MT_SWF,
                    DETOPE.NUM_REF_SWF,
                    DETOPE.FLG_EGR_ING,
                    DETOPE.COD_BCO_BFC,
                    DETOPE.COD_BCO_ORI,
                    DETOPE.COD_TPO_OPE,
                    DETOPE.NUM_FOL_MSJ_PRZ,
                    DETOPE.FEC_ISR_MSJ_PRZ,
                    DETOPE.NUM_REF_SWF_RLD,
                    DETOPE.FLG_MRD_UTZ_SWF,
                    DETOPE.COD_TPO_OPE,
                    DETOPE.NUM_FOL_OPE_ORI,
                    DETOPE.FEC_ISR_OPE_ORG,
                    DETOPE.GLS_EST_RCH;

      p_ERROR := PKG_PAB_CONSTANTES.v_OK;
   --Procedimiento ejecutado de forma correcta
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg :=
            'No se han encontrado registos asociados a la busqueda por numero de folio'
            || p_NUM_FOL_MSJ_PRZ
            || ' estado '
            || p_COD_EST_AOS
            || ' referencia swift '
            || p_NUM_REF_SWF
            || ' numero mensaje '
            || p_COD_MT_SWF
            || ' banco '
            || p_COD_BCO_DTN
            || ' egreso/ingreso '
            || p_FLG_EGR_ING
            || ' fecha desde '
            || p_FEC_DESDE
            || ' fecha hasta '
            || p_FEC_HASTA;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   --
   END SP_PAB_BUS_MNSJE_PRTCZ;

   --
   --
   --******************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_ACT_MSN_PRT
   -- Objetivo: Procedimiento que actualiza mensaje de protocolo
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_MNSJE_PRTCZ, PABS_DT_DETLL_MNSJE_PRTCZ
   -- Fecha: 31/03/17
   -- Autor: Santander
   -- Input:
   --p_NUM_FOL_MSJ_PRZ    --> Numero foliador mensajeria protocolo
   --p_COD_BCO_BFC        --> Codigo banco beneficiario
   --p_NUM_REF_SWF        --> Numero de referencia
   --p_COD_EST_AOS        --> C�digo estado
   --p_FEC_ISR_MSJ_PRZ    --> Fecha insercion mensajeria protocolo
   --p_NUM_FOL_OPE_ORI    --> Numero folio operacion original
   --p_COD_TPO_OPE        --> Estado tipo operaci�n
   -- Output:
   -- p_ERROR             --> Indicador de errores (0 Sin errores :: 1 existen errores). Se ven en la tabla Log estos errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ?ltimos cambios>
   --***********************************************************************************************
   PROCEDURE Sp_PAB_ACT_MSN_PRT (p_NUM_FOL_MSJ_PRZ   IN     NUMBER,
                                 p_COD_BCO_BFC       IN     CHAR,
                                 p_NUM_REF_SWF       IN     CHAR,
                                 p_COD_EST_AOS       IN     VARCHAR2,
                                 p_FEC_ISR_MSJ_PRZ   IN     DATE,
                                 p_NUM_FOL_OPE_ORI   IN     NUMBER,
                                 p_COD_TPO_OPE       IN     CHAR,
                                 p_ERROR                OUT NUMBER)
   IS
      --Seteo de Variables
      v_NOM_SP        VARCHAR2 (30) := 'Sp_PAB_ACT_MSN_PRT';
      v_NUM_CAM_REG   VARCHAR2 (100);
      ERROR_MSJ EXCEPTION;
   BEGIN
      UPDATE   PABS_DT_MNSJE_PRTCZ MNS_PRTCZ
         SET   MNS_PRTCZ.NUM_REF_SWF = p_NUM_REF_SWF,
               MNS_PRTCZ.COD_BCO_BFC = p_COD_BCO_BFC
       WHERE   MNS_PRTCZ.NUM_FOL_MSJ_PRZ = p_NUM_FOL_MSJ_PRZ
               AND MNS_PRTCZ.FEC_ISR_MSJ_PRZ = p_FEC_ISR_MSJ_PRZ;

      BEGIN
         -- Consultamos mensaje
         SELECT   COUNT (NUM_CAM_REG)
           INTO   v_NUM_CAM_REG
           FROM   PABS_DT_DETLL_MNSJE_PRTCZ
          WHERE   NUM_FOL_MSJ_PRZ = p_NUM_FOL_MSJ_PRZ
                  AND FEC_ISR_MSJ_PRZ = p_FEC_ISR_MSJ_PRZ;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            err_code := SQLCODE;
            err_msg :=
                  'No se pudo obtener la informaci�n del mensaje: '
               || p_NUM_FOL_MSJ_PRZ
               || ' con fecha: '
               || p_FEC_ISR_MSJ_PRZ;
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            RAISE;
      END;

      -- Validadmos la existencia de mensaje
      IF (v_NUM_CAM_REG IS NULL)
      THEN
         RAISE ERROR_MSJ;
      END IF;

      DELETE FROM   PABS_DT_DETLL_MNSJE_PRTCZ
            WHERE   NUM_FOL_MSJ_PRZ = p_NUM_FOL_MSJ_PRZ
                    AND FEC_ISR_MSJ_PRZ = p_FEC_ISR_MSJ_PRZ;

      p_ERROR := PKG_PAB_CONSTANTES.v_OK;
   EXCEPTION
      WHEN ERROR_MSJ
      THEN
         err_msg :=
            'No se encontro mensaje asociado a mensaje:' || p_NUM_FOL_MSJ_PRZ;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (NULL,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN DUP_VAL_ON_INDEX
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END Sp_PAB_ACT_MSN_PRT;



   --************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_GES_MSJ_EDIT_PROT
   -- Objetivo: Gestiona la edici�n de mensaje de protocolo.
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas:
   -- Fecha: 31/03/17
   -- Autor: Santander
   -- p_NUM_REF_SWF            --> Numero de referencia
   -- p_NUM_FOL_MSJ_PRZ        --> Numero de folio de mensajeria
   -- p_FEC_ISR_MSJ_PRZ        --> Fecha de inserci�n
   -- p_COD_MT_SWF             --> Codigo mensaje
   -- p_COD_EST_AOS            --> Codigo estado
   -- p_COD_BCO_BFC            --> Bic banco beneficiario
   -- p_NUM_FOL_OPE_ORI        --> Numero folio operacion origen
   -- p_COD_TPO_OPE            --> Codigo tipo operacion
   -- p_GLS_TXT_LBE_MSJ         --> Glosa del texto de 298
   -- p_COD_USR                --> Codigo usuario
   -- Output:
   -- p_ERROR          --> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores
   -- Retorno: N/A.
   --***********************************************************************************************
   PROCEDURE Sp_PAB_GES_MSJ_EDIT_PROT (p_NUM_REF_SWF       IN     CHAR,
                                       p_NUM_FOL_MSJ_PRZ   IN     NUMBER,
                                       p_FEC_ISR_MSJ_PRZ   IN     DATE,
                                       p_COD_MT_SWF        IN     CHAR,
                                       p_COD_EST_AOS       IN     VARCHAR2,
                                       p_COD_BCO_BFC       IN     CHAR,
                                       p_NUM_FOL_OPE_ORI   IN     NUMBER,
                                       p_COD_TPO_OPE       IN     CHAR,
                                       p_GLS_TXT_LBE_MSJ   IN     VARCHAR2,
                                       p_COD_USR           IN     VARCHAR2,
                                       p_ERROR                OUT NUMBER)
   IS
      --Seteo de Variables
      v_NOM_SP            VARCHAR2 (30) := 'Sp_PAB_GES_MSJ_EDIT_PROT';
      v_FEC_ACT_MSJ_PRZ   DATE := SYSDATE;
      v_COD_EST_AOS       VARCHAR2 (30);
      v_COD_EST_AOS_ACT   NUMBER;
      v_RESULT            NUMBER;
   BEGIN
      -- Inicializaci�n de variables y par�metros
      p_ERROR := 0;
      v_COD_EST_AOS :=
         PKG_PAB_UTILITY.FN_PAB_OBT_COD_DSC_EST (
            p_COD_EST_AOS,
            PKG_PAB_CONSTANTES.V_IND_OPE
         );
      v_COD_EST_AOS_ACT := PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEVAL;

      -- Edita de cabecera de mensaje de protocolo;
      PKG_PAB_PROTOCOLO.Sp_PAB_ACT_MSN_PRT (p_NUM_FOL_MSJ_PRZ,
                                            p_COD_BCO_BFC,
                                            p_NUM_REF_SWF,
                                            v_COD_EST_AOS,
                                            p_FEC_ISR_MSJ_PRZ,
                                            p_NUM_FOL_OPE_ORI,
                                            p_COD_TPO_OPE,
                                            p_ERROR);

      -- Inserta detalle de mensaje de protocolo luego de ser eliminado en proceso anterior
      PKG_PAB_PROTOCOLO.Sp_PAB_INS_DETLL_MSN_PRTCZ (p_NUM_FOL_MSJ_PRZ,
                                                    p_FEC_ISR_MSJ_PRZ,
                                                    p_GLS_TXT_LBE_MSJ,
                                                    p_ERROR);

      -- Llamamos al procedimiento de Bitacora
      v_DES_BIT := 'Edici�n de mensaje protocolo ' || p_NUM_FOL_MSJ_PRZ;
      PKG_PAB_TRACKING.Sp_PAB_INS_BIT_OPE (p_NUM_FOL_MSJ_PRZ,
                                           v_FEC_ACT_MSJ_PRZ,
                                           p_COD_USR,
                                           v_COD_EST_AOS_ACT,
                                           v_COD_EST_AOS,
                                           v_DES_BIT,
                                           v_RESULT);

      COMMIT;
      p_ERROR := PKG_PAB_CONSTANTES.v_OK;
   --
   EXCEPTION
      WHEN DUP_VAL_ON_INDEX
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END Sp_PAB_GES_MSJ_EDIT_PROT;

   --***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_MENSAJE_MOTIVO_SWIFT
   -- Objetivo: PROCEDIMIENTO ALMACENADO QUE DEVUELVE LA DESCRICI�N DE MEJSAJES 298 SEGUN TIPO DE OPERACION.
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_DETLL_OPRCN, PABS_DT_TIPO_OPRCN
   -- Fecha: 31/03/17
   -- Autor: Santander
   -- Input:
   -- p_NUM_OPE         --> N�mero de folio
   -- p_FECHA_INS       --> Fecha de inserci�n
   -- p_COD_TPO_OPE     --> Tipo de operaci�n
   -- Output:
   -- MENSAJE_SWF       --> Mensaje de 298
   -- p_ERROR           --> Indicador de errores, maneja dos estados, 0 sin errores 1 existen errores
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************
   PROCEDURE Sp_PAB_MENSAJE_MOTIVO_SWIFT (p_NUM_OPE       IN     NUMBER,
                                          p_FECHA_INS     IN     DATE,
                                          p_COD_TPO_OPE   IN     CHAR,
                                          MENSAJE_SWF        OUT VARCHAR2,
                                          p_ERROR            OUT NUMBER)
   IS
      v_NOM_SP          VARCHAR2 (30) := 'Sp_PAB_MENSAJE_MOTIVO_SWIFT';
      v_REF_SWIFT_ORI   VARCHAR2 (30);
      v_REF_SWIFT       VARCHAR2 (30);
      v_COD_MT          VARCHAR2 (30);
      v_MONTO_TOTAL     VARCHAR2 (30);
      v_BIC_DESTINO     VARCHAR2 (30);
      v_FECHA_PAGO      VARCHAR2 (100);
      v_BENEFICIARIO    VARCHAR2 (100);
      v_NUM_OPE         VARCHAR2 (30);
      v_NUM_NOM         VARCHAR2 (30);
      v_MERCADO         VARCHAR2 (30);
      v_MENSAJE         VARCHAR2 (1000);
      v_NUM_DOC_BFC     VARCHAR2 (30);
      v_NUM_CTA_BFC     VARCHAR2 (30);
      v_COD_DVI         VARCHAR2 (30);
   BEGIN
      BEGIN
         --Obtenemos los datos de la operaci�n
         SELECT   NUM_REF_SWF,
                  NUM_REF_EXT,
                  COD_MT_SWF,
                  IMp_OPE,
                  COD_BCO_DTN,
                  FEC_VTA,
                  NOM_BFC,
                  NUM_FOL_OPE,
                  NUM_FOL_NMN,
                  FLG_MRD_UTZ_SWF,
                  NUM_DOC_BFC,
                  NUM_CTA_BFC,
                  COD_DVI
           INTO   v_REF_SWIFT_ORI,
                  v_REF_SWIFT,
                  v_COD_MT,
                  v_MONTO_TOTAL,
                  v_BIC_DESTINO,
                  v_FECHA_PAGO,
                  v_BENEFICIARIO,
                  v_NUM_OPE,
                  v_NUM_NOM,
                  v_MERCADO,
                  v_NUM_DOC_BFC,
                  v_NUM_CTA_BFC,
                  v_COD_DVI
           FROM   (SELECT   DETOPE.NUM_REF_SWF,
                            DETOPE.NUM_REF_EXT,
                            DETOPE.COD_MT_SWF,
                            DETOPE.IMp_OPE,
                            DETOPE.COD_BCO_DTN,
                            DETOPE.FEC_VTA,
                            DETOPE.NOM_BFC,
                            DETOPE.NUM_FOL_OPE,
                            DETOPE.NUM_FOL_NMN,
                            DETOPE.FLG_MRD_UTZ_SWF,
                               TRIM (DETOPE.NUM_DOC_BFC)
                            || '-'
                            || TRIM (DETOPE.COD_TPD_BFC)
                               AS NUM_DOC_BFC,
                            DETOPE.NUM_CTA_BFC,
                            DETOPE.COD_DVI
                     FROM   PABS_DT_DETLL_OPRCN DETOPE
                    WHERE   DETOPE.NUM_FOL_OPE = p_NUM_OPE
                            AND DETOPE.FEC_ISR_OPE = p_FECHA_INS
                   UNION ALL
                   SELECT   DETOPE.NUM_REF_SWF,
                            DETOPE.NUM_REF_EXT,
                            DETOPE.COD_MT_SWF,
                            DETOPE.IMp_OPE,
                            DETOPE.COD_BCO_DTN,
                            DETOPE.FEC_VTA,
                            DETOPE.NOM_BFC,
                            DETOPE.NUM_FOL_OPE,
                            DETOPE.NUM_FOL_NMN,
                            DETOPE.FLG_MRD_UTZ_SWF,
                               TRIM (DETOPE.NUM_DOC_BFC)
                            || '-'
                            || TRIM (DETOPE.COD_TPD_BFC)
                               AS NUM_DOC_BFC,
                            DETOPE.NUM_CTA_BFC,
                            DETOPE.COD_DVI
                     FROM   PABS_DT_DETLL_OPRCN_HTRCA DETOPE
                    WHERE   DETOPE.NUM_FOL_OPE = p_NUM_OPE
                            AND DETOPE.FEC_ISR_OPE = p_FECHA_INS);
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            err_code :=
                  'No se encotro la informacion de la operaci�n Folio:'
               || p_NUM_OPE
               || ' Fecha:'
               || TO_CHAR (p_FECHA_INS, 'DD-MM-YYYY HH24:MI:SS');
            err_msg := SUBSTR (SQLERRM, 1, 300);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
            RAISE;
         WHEN OTHERS
         THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            p_s_mensaje := err_code || '-' || err_msg;
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
            RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
      END;

      BEGIN
         -- Buscamos el mensaje por defecto segun el motivo del mensaje
         SELECT   GLS_MSJ_OPE
           INTO   v_MENSAJE
           FROM   PABS_DT_TIPO_OPRCN
          WHERE   COD_TPO_OPE = p_COD_TPO_OPE;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            err_code :=
               'No se encotro el motivo del mensaje por defecto para el valor:'
               || p_COD_TPO_OPE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
         WHEN OTHERS
         THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            p_s_mensaje := err_code || '-' || err_msg;
            p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
            RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
      END;

      --Asignaci�n de valores a variables de mensajes
      v_MENSAJE := REPLACE (v_MENSAJE, '%REFER', NVL (v_REF_SWIFT_ORI, '---'));
      v_MENSAJE := REPLACE (v_MENSAJE, '%FECHA', NVL (v_FECHA_PAGO, ' --- '));
      v_MENSAJE :=
         REPLACE (v_MENSAJE, '%BANCO_VENDEDOR', NVL (v_BIC_DESTINO, ' --- '));
      v_MENSAJE := REPLACE (v_MENSAJE, '%BNF', NVL (v_BENEFICIARIO, ' --- '));
      v_MENSAJE := REPLACE (v_MENSAJE, '%MONTO', NVL (v_MONTO_TOTAL, ' ---'));
      v_MENSAJE :=
         REPLACE (v_MENSAJE, '%MONTO_FX', NVL (v_MONTO_TOTAL, ' ---'));
      v_MENSAJE :=
         REPLACE (v_MENSAJE, '%MONTO_CLP', NVL (v_MONTO_TOTAL, ' ---'));
      v_MENSAJE :=
         REPLACE (v_MENSAJE, '%MONTO_TC', NVL (v_MONTO_TOTAL, ' ---'));
      v_MENSAJE := REPLACE (v_MENSAJE, '%RUT', NVL (v_NUM_DOC_BFC, ' ---'));
      v_MENSAJE := REPLACE (v_MENSAJE, '%CTA', NVL (v_NUM_CTA_BFC, ' ---'));
      v_MENSAJE := REPLACE (v_MENSAJE, '%MT', NVL (v_COD_MT, ' --- '));
      v_MENSAJE := REPLACE (v_MENSAJE, '%COD_DVI', NVL (v_COD_DVI, ' ---'));
      v_MENSAJE := REPLACE (v_MENSAJE, '%FECHA_VALOR_CLP', NVL ('', ' ---'));
      v_MENSAJE := REPLACE (v_MENSAJE, '%FECHA_VALOR_FX', NVL ('', ' ---'));

      MENSAJE_SWF := v_MENSAJE;

      P_ERROR := PKG_PAB_CONSTANTES.V_OK; --Procedimiento ejecutado de forma correcta
   EXCEPTION
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         p_s_mensaje := err_code || '-' || err_msg;
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END Sp_PAB_MENSAJE_MOTIVO_SWIFT;

   --**********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_MNSJE_PRTCZ
   -- Objetivo: Funcion para realizar busqueda de mensajerias de protocolo
   -- 799 / 199 / 299 / 298 / 198
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_TIPO_OPRCN, PABS_DT_DETLL_MNSJE_PRTCZ, PABS_DT_MNSJE_PRTCZ, PABS_DT_DETLL_OPRCN
   -- Fecha: 31/03/17
   -- Autor: Santander
   -- Input:
   -- p_NUM_FOL_MSJ_PRZ  -> Numero folio mensaje
   -- p_FEC_ISR_MSJ_PRZ  -> Fecha de inserci�n
   -- Output:
   -- p_CURSOR          ->  Lista de mensaje de protocolo
   -- p_ERROR           -> Indicador de errores (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de utimos cambios>
   --***********************************************************************************************
   PROCEDURE SP_PAB_MNSJE_PRTCZ (p_NUM_FOL_MSJ_PRZ   IN     NUMBER,
                                 p_FEC_ISR_MSJ_PRZ   IN     DATE,
                                 p_CURSOR               OUT SYS_REFCURSOR,
                                 p_ERROR                OUT NUMBER)
   IS
      v_NOM_SP        VARCHAR2 (30) := 'SP_PAB_MNSJE_PRTCZ';
      v_GLS_TPO_OPE   VARCHAR2 (200);
      v_CANTIDAD      NUMBER;
      v_CTOR          NUMBER;
      v_MENSAJE       VARCHAR2 (300);
      v_MENSAJE2      VARCHAR2 (9000);
      v_MENSAJE3      VARCHAR2 (4000);
      v_MENSAJE4      VARCHAR2 (4000);
      v_MENSAJE5      VARCHAR2 (4000);
      v_TPO_ORI_N     CHAR := 'N';
      v_TPO_ORI_H     CHAR := 'H';
      ERR_COD_INEX EXCEPTION;
   BEGIN
      BEGIN
         SELECT   COUNT (1)
           INTO   v_CANTIDAD
           FROM   PABS_DT_DETLL_MNSJE_PRTCZ
          WHERE   NUM_FOL_MSJ_PRZ = p_NUM_FOL_MSJ_PRZ
                  AND FEC_ISR_MSJ_PRZ = p_FEC_ISR_MSJ_PRZ;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
         WHEN OTHERS
         THEN
            err_code := SQLCODE;
            err_msg := SUBSTR (SQLERRM, 1, 300);
            PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                v_NOM_PCK,
                                                err_msg,
                                                v_NOM_SP);
            p_s_mensaje := err_code || '-' || err_msg;
            RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
      END;

      WHILE v_CANTIDAD > 0
      LOOP
         SELECT   GLS_TXT_LBE_MSJ
           INTO   v_MENSAJE
           FROM   PABS_DT_DETLL_MNSJE_PRTCZ
          WHERE       NUM_FOL_MSJ_PRZ = p_NUM_FOL_MSJ_PRZ
                  AND FEC_ISR_MSJ_PRZ = p_FEC_ISR_MSJ_PRZ
                  AND NUM_CAM_REG = v_CANTIDAD;

         v_MENSAJE2 := v_MENSAJE || v_MENSAJE2;
         v_CANTIDAD := v_CANTIDAD - 1;
      END LOOP;


      IF LENGTH (v_MENSAJE2) > 8000
      THEN
         v_MENSAJE3 :=
            PKG_PAB_UTILITY.FN_PAB_VLD_TXT (SUBSTR (v_MENSAJE2, 0, 3999));  --
         v_MENSAJE4 :=
            PKG_PAB_UTILITY.FN_PAB_VLD_TXT (SUBSTR (v_MENSAJE2, 4000, 7999));
         v_MENSAJE5 :=
            PKG_PAB_UTILITY.FN_PAB_VLD_TXT (SUBSTR (v_MENSAJE2, 8000));
      END IF;

      IF LENGTH (v_MENSAJE2) > 4000 AND LENGTH (v_MENSAJE2) < 8000
      THEN
         v_MENSAJE3 :=
            PKG_PAB_UTILITY.FN_PAB_VLD_TXT (SUBSTR (v_MENSAJE2, 0, 3999));  --
         v_MENSAJE4 :=
            PKG_PAB_UTILITY.FN_PAB_VLD_TXT (SUBSTR (v_MENSAJE2, 4000, 7999));
         v_MENSAJE5 := '';
      ELSE
         v_MENSAJE3 :=
            PKG_PAB_UTILITY.FN_PAB_VLD_TXT (SUBSTR (v_MENSAJE2, 0, 3999));
         v_MENSAJE4 := '';
         v_MENSAJE5 := '';
      END IF;



      OPEN p_CURSOR FOR
         SELECT   MJEPRT.COD_MT_SWF AS MT_MSG,
                  MJEPRT.NUM_FOL_MSJ_PRZ AS NUM_OPE_MSG,
                  MJEPRT.FEC_ISR_MSJ_PRZ AS FEC_OPE_MSG,
                  MJEPRT.COD_EST_AOS AS EST_OPE_MSG,
                  EST.DSC_EST_AOS AS DSC_EST,
                  MJEPRT.NUM_SUB_MSJ_RLD AS NUM_SUB_MSG,
                  MJEPRT.GLS_EST_RCH AS MSJ_GLS_ADC,
                  DECODE (MJEPRT.FLG_EGR_ING,
                          PKG_PAB_CONSTANTES.V_FLG_ENV,
                          PKG_PAB_CONSTANTES.V_STR_FLG_ENV,
                          PKG_PAB_CONSTANTES.V_STR_FLG_REC)
                     AS TPO_FJO_MSG,
                  TO_CHAR (MJEPRT.FEC_ENV_RCP_SWF, 'DD/MM/YYYY')
                     AS FEC_RCP_SW,
                  MJEPRT.HOR_ENV_RCP_SWF AS HORA_RCP_SW,
                  'Operaciones Financieras' AS CANAL,
                  '0174' AS SUCURSAL,
                  TIPOPRT.GLS_TPO_OPE AS MOTIVO,
                  MJEPRT.NUM_REF_SWF_RLD AS REF_SWIFT_ORI,
                  MJEPRT.NUM_REF_SWF AS REF_SWIFT,
                  MJEPRT.COD_BCO_BFC AS BIC_DESTINO,
                  MJEPRT.COD_BCO_ORI AS BIC_ORIGEN,
                  DECODE (MJEPRT.FLG_MRD_UTZ_SWF,
                          PKG_PAB_CONSTANTES.v_FLG_MRD_MN,
                          'Mercado Nacional',
                          'Mercado Extranjero')
                     AS MERCADO,
                  MJEPRT.COD_TPO_OPE AS TIPO_OPE,
                     TO_CLOB (v_MENSAJE3)
                  || TO_CLOB (v_MENSAJE4)
                  || TO_CLOB (v_MENSAJE5)
                     AS MENSAJE, --TO_CLOB(PKG_PAB_UTILITY.FN_PAB_VLD_TXT(v_MENSAJE2)) AS MENSAJE,
                  '' AS FLG_ENV_ING
           FROM   PABS_DT_MNSJE_PRTCZ MJEPRT,
                  PABS_DT_TIPO_OPRCN TIPOPRT,
                  PABS_DT_ESTDO_ALMOT EST
          WHERE       MJEPRT.NUM_FOL_MSJ_PRZ = p_NUM_FOL_MSJ_PRZ
                  AND MJEPRT.FEC_ISR_MSJ_PRZ = p_FEC_ISR_MSJ_PRZ
                  AND MJEPRT.COD_TPO_OPE = TIPOPRT.COD_TPO_OPE
                  AND MJEPRT.COD_EST_AOS = EST.COD_EST_AOS;


      p_ERROR := PKG_PAB_CONSTANTES.v_OK;
   --                                                                                                                                                                                                                                                                                                                                                                                                                                --Procedimiento ejecutado de forma correcta
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg :=
               SUBSTR (SQLERRM, 1, 300)
            || 'No se encontro operaci�n folio:'
            || p_NUM_FOL_MSJ_PRZ
            || ' Fecha:'
            || TO_CHAR (p_FEC_ISR_MSJ_PRZ, 'dd-mm-yyyy hh24:mm:ss');
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   --
   END SP_PAB_MNSJE_PRTCZ;


   --***********************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_COMBO_MOTIVO_SWIFT
   -- Objetivo: PROCEDIMIENTO ALMACENADO QUE DEVUELVE MOTIVOS ASOCIADOS A UN MENSAJE.
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_TIPO_OPRCN
   -- Fecha: 29/12/16
   -- Autor: Santander
   -- Input:
   -- N/A
   -- Output:
   -- p_CURSOR          -> Lista de motivos para mensajes
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ultimos cambios>
   --***********************************************************************************************
   PROCEDURE Sp_PAB_COMBO_MOTIVO_SWIFT (p_CURSOR OUT SYS_REFCURSOR)
   IS
      --
      v_NOM_SP   VARCHAR2 (30) := 'Sp_PAB_COMBO_MOTIVO_SWIFT';
   --
   BEGIN
      --
      OPEN p_CURSOR FOR
           -- Tabla con no m�s de 10 registros  Access Full
           SELECT   COD_TPO_OPE TIPO_OPERACION, GLS_TPO_OPE MOTIVO
             FROM   PABS_DT_TIPO_OPRCN
            WHERE   COD_TPO_OPE <> v_COD_TPO_OPE_BCOEXT
         ORDER BY   GLS_TPO_OPE;
   --
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg :=
            'No se han encontrado registos asociados a la busqueda '
            || v_COD_TPO_OPE_BCOEXT;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   --
   END Sp_PAB_COMBO_MOTIVO_SWIFT;


   --**********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_BUS_OPE_SWF
   -- Objetivo: Procedimiento que realiza b�squeda de operaciones Swift para asociar a mensajer�a de protocolo
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_DETLL_OPRCN
   -- Fecha: 03/01/2017
   -- Autor: Santander
   -- Input:
   -- p_STR_FLG_MRD       -> Flag
   -- p_COD_BCO_DTN       -> Bic banco destino
   -- p_NUM_REF_SWF       -> Referencia mensaje
   -- p_FEC_VTA           -> Fecha valuta
   -- p_IMp_OPE           -> Monto
   -- p_FLG_EGR_ING       -> Flag egreso ingreso
   -- p_COD_EST_AOS       -> Estado
   -- Output:
   -- p_CURSOR_DETLL_OPE  -> Devuelve operaciones para relicionar a un mensaje 298
   -- p_ERROR -> Indicador de errores (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: 2019-11-27 Se agrega busqueda en tabla historica (union all)
   --***********************************************************************************************
   PROCEDURE SP_PAB_BUS_OPE_SWF (p_STR_FLG_MRD        IN     NUMBER,
                                 p_COD_BCO_DTN        IN     CHAR,
                                 p_COD_BCO_BNF        IN     CHAR,
                                 p_NUM_REF_SWF        IN     CHAR,
                                 p_FEC_VTA            IN     DATE,
                                 p_IMp_OPE            IN     NUMBER,
                                 p_FLG_EGR_ING        IN     NUMBER,
                                 p_COD_EST_AOS        IN     NUMBER,
                                 p_CURSOR_DETLL_OPE      OUT SYS_REFCURSOR,
                                 p_ERROR                 OUT NUMBER)
   IS
      v_NOM_SP        VARCHAR2 (30) := 'SP_PAB_BUS_OPE_SWF';
      v_STR_FLG_MRD   VARCHAR2 (3);
      v_COD_BCO_DTN   CHAR (11) := p_COD_BCO_DTN;
      v_NUM_REF_SWF   CHAR (16) := p_NUM_REF_SWF;
      v_COD_EST_AOS   NUMBER;
      v_COD_BCO_ORI   CHAR (11) := p_COD_BCO_DTN;
      v_EST_AOS_PAB   CHAR (8);
      v_EST_AOS_ABN   CHAR (8);
      v_EST_AOS_ING   CHAR (8);
      v_EST_AOS_PAG   CHAR (8);
   BEGIN
      v_STR_FLG_MRD := p_STR_FLG_MRD;
      v_COD_EST_AOS := p_COD_EST_AOS;

      --Salida
      IF p_FLG_EGR_ING = 0
      THEN
         v_COD_BCO_ORI := NULL;
         v_EST_AOS_PAB := NULL;
         v_EST_AOS_ABN := NULL;
         v_EST_AOS_ING := NULL;
         v_EST_AOS_PAG := PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAG;
      ELSE
         v_COD_BCO_DTN := NULL;
         v_EST_AOS_PAB := PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEPAB;
         v_EST_AOS_ABN := PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEABN;
         v_EST_AOS_ING := PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEING;
         v_EST_AOS_PAG := NULL;
      END IF;

      OPEN p_CURSOR_DETLL_OPE FOR
         SELECT   DETOPE.NUM_REF_SWF AS REF_SWIFT,
                  DETOPE.NUM_REF_EXT AS REF_SWIFT_ORI,
                  DETOPE.COD_MT_SWF AS COD_MT,
                  DETOPE.IMp_OPE AS MONTO,
                  DETOPE.COD_BCO_DTN AS BIC_DESTINO,
                  DETOPE.COD_BCO_ORG AS BIC_ORIGEN,
                  DETOPE.FEC_VTA AS FECHA_PAGO,
                  DETOPE.NOM_BFC AS BENEFICIARIO,
                  DETOPE.NUM_FOL_OPE AS NUM_OPE,
                  DETOPE.NUM_FOL_NMN AS NUM_NOM,
                  DETOPE.COD_DVI AS MONEDA,
                  DETOPE.FLG_MRD_UTZ_SWF AS MERCADO,
                  DETOPE.COD_SIS_SAL AS SIST_SAL,
                  DETOPE.FEC_ISR_OPE AS FEC_ISR,
                  DETOPE.FEC_ISR_OPE AS FEC_PCS
           FROM   PABS_DT_DETLL_OPRCN DETOPE
          WHERE   DETOPE.COD_BCO_ORG =
                     NVL (v_COD_BCO_ORI, DETOPE.COD_BCO_ORG)
                  AND DETOPE.COD_BCO_DTN =
                        NVL (v_COD_BCO_DTN, DETOPE.COD_BCO_DTN)
                  AND    -- consultar si en otra query se utilza como opcional
                     DETOPE.FEC_VTA = NVL (p_FEC_VTA, DETOPE.FEC_VTA)
                  AND DETOPE.IMp_OPE = NVL (p_IMp_OPE, DETOPE.IMp_OPE)
                  AND DETOPE.FLG_EGR_ING =
                        NVL (p_FLG_EGR_ING, DETOPE.FLG_EGR_ING)
                  AND DETOPE.FLG_MRD_UTZ_SWF =
                        NVL (v_STR_FLG_MRD, DETOPE.FLG_MRD_UTZ_SWF)
                  AND NVL (DETOPE.NUM_REF_SWF, '#') =
                        NVL (v_NUM_REF_SWF, NVL (DETOPE.NUM_REF_SWF, '#'))
                  AND DETOPE.COD_EST_AOS IN
                           (v_EST_AOS_PAB,
                            v_EST_AOS_ABN,
                            v_EST_AOS_ING,
                            v_EST_AOS_PAG)
         UNION ALL
         SELECT   DETOPEHIST.NUM_REF_SWF AS REF_SWIFT,
                  DETOPEHIST.NUM_REF_EXT AS REF_SWIFT_ORI,
                  DETOPEHIST.COD_MT_SWF AS COD_MT,
                  DETOPEHIST.IMp_OPE AS MONTO,
                  DETOPEHIST.COD_BCO_DTN AS BIC_DESTINO,
                  DETOPEHIST.COD_BCO_ORG AS BIC_ORIGEN,
                  DETOPEHIST.FEC_VTA AS FECHA_PAGO,
                  DETOPEHIST.NOM_BFC AS BENEFICIARIO,
                  DETOPEHIST.NUM_FOL_OPE AS NUM_OPE,
                  DETOPEHIST.NUM_FOL_NMN AS NUM_NOM,
                  DETOPEHIST.COD_DVI AS MONEDA,
                  DETOPEHIST.FLG_MRD_UTZ_SWF AS MERCADO,
                  DETOPEHIST.COD_SIS_SAL AS SIST_SAL,
                  DETOPEHIST.FEC_ISR_OPE AS FEC_ISR,
                  DETOPEHIST.FEC_ISR_OPE AS FEC_PCS
           FROM   PABS_DT_DETLL_OPRCN_HTRCA DETOPEHIST
          WHERE   DETOPEHIST.COD_BCO_ORG =
                     NVL (v_COD_BCO_ORI, DETOPEHIST.COD_BCO_ORG)
                  AND DETOPEHIST.COD_BCO_DTN =
                        NVL (v_COD_BCO_DTN, DETOPEHIST.COD_BCO_DTN)
                  AND    -- consultar si en otra query se utilza como opcional
                     DETOPEHIST.FEC_VTA =
                        NVL (p_FEC_VTA, DETOPEHIST.FEC_VTA)
                  AND DETOPEHIST.IMp_OPE =
                        NVL (p_IMp_OPE, DETOPEHIST.IMp_OPE)
                  AND DETOPEHIST.FLG_EGR_ING =
                        NVL (p_FLG_EGR_ING, DETOPEHIST.FLG_EGR_ING)
                  AND DETOPEHIST.FLG_MRD_UTZ_SWF =
                        NVL (v_STR_FLG_MRD, DETOPEHIST.FLG_MRD_UTZ_SWF)
                  AND NVL (DETOPEHIST.NUM_REF_SWF, '#') =
                        NVL (v_NUM_REF_SWF,
                             NVL (DETOPEHIST.NUM_REF_SWF, '#'))
                  AND DETOPEHIST.COD_EST_AOS IN
                           (v_EST_AOS_PAB,
                            v_EST_AOS_ABN,
                            v_EST_AOS_ING,
                            v_EST_AOS_PAG);

      P_ERROR := PKG_PAB_CONSTANTES.V_OK; --Procedimiento ejecutado de forma correcta
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg :=
               'No se han encontrado registos asociados a la banco origen '
            || v_COD_BCO_ORI
            || ' banco destino '
            || v_COD_BCO_DTN
            || 'fecha valuta '
            || p_FEC_VTA
            || ' importe '
            || p_IMp_OPE;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         RAISE;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         p_s_mensaje := err_code || '-' || err_msg;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END SP_PAB_BUS_OPE_SWF;


   --******************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_INS_MSN_PRTCZ
   -- Objetivo: Procedimiento que inserta menjase de protocolo
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_MNSJE_PRTCZ
   -- Fecha: 04/01/17
   -- Autor: Santander
   -- Input:
   --p_NUM_SUB_MSJ_RLD   -> Numero de submenaje
   --p_NUM_REF_SWF_RLD   -> referencia relacionada
   --p_NUM_REF_SWF       -> referencia mensaje
   --p_NUM_FOL_MSJ_PRZ   -> Folio mensaje
   --p_HOR_ENV_RCP_SWF   -> Hora swift
   --p_GLS_EST_RCH       -> Glosa rechazo
   --p_FLG_MRD_UTZ_SWF   -> Flag de divisa
   --p_FLG_EGR_ING       -> Falg egreso ingreso
   --p_FEC_ISR_MSJ_PRZ   -> Fecha inserci�n mensaje
   --p_FEC_ENV_RCP_SWF   -> Fecha swift
   --p_COD_MT_SWF        -> Tipo de memsaje
   --p_COD_EST_AOS       -> Estado de mensaje
   --p_COD_BCO_BFC       -> Bic beneficiario
   --p_COD_ADC_EST       -> Codigo estado adicional
   --p_NUM_FOL_OPE_ORI   -> Folio operaci�n original
   --p_COD_TPO_OPE       -> Tipo operaci�n
   -- Output:
   -- p_ERROR            -> Indicador de errores (0 Sin errores :: 1 existen errores). Se ven en la tabla Log estos errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ?ltimos cambios>
   --***********************************************************************************************
   PROCEDURE Sp_PAB_INS_MSN_PRTCZ (p_NUM_SUB_MSJ_RLD   IN     CHAR,
                                   p_NUM_REF_SWF_RLD   IN     CHAR,
                                   p_NUM_REF_SWF       IN     CHAR,
                                   p_NUM_FOL_MSJ_PRZ   IN     NUMBER,
                                   p_HOR_ENV_RCP_SWF   IN     NUMBER,
                                   p_GLS_EST_RCH       IN     VARCHAR2,
                                   p_FLG_MRD_UTZ_SWF   IN     NUMBER,
                                   p_FLG_EGR_ING       IN     NUMBER,
                                   p_FEC_ISR_MSJ_PRZ   IN     DATE,
                                   p_FEC_ENV_RCP_SWF   IN     DATE,
                                   p_COD_MT_SWF        IN     CHAR,
                                   p_COD_EST_AOS       IN     NUMBER,
                                   p_COD_BCO_BFC       IN     CHAR,
                                   p_COD_ADC_EST       IN     CHAR,
                                   p_NUM_FOL_OPE_ORI   IN     NUMBER,
                                   p_FEC_ISR_OPE_ORI   IN     DATE,
                                   p_COD_TPO_OPE       IN     CHAR,
                                   p_ERROR                OUT NUMBER)
   IS
      --Seteo de Variables
      v_NOM_SP        VARCHAR2 (30) := 'Sp_PAB_INS_MSN_PRTCZ';
      v_COD_BCO_ORI   CHAR (11);
      v_COD_BCO_BFC   CHAR (11);
      v_FEC_ISR_OPE   DATE;
   --v_GLS_EST_ASO   VARCHAR2(30) := 'Operaci�n asociada por devoluci�n';

   BEGIN
      IF p_FLG_EGR_ING = PKG_PAB_CONSTANTES.V_FLG_ING
      THEN
         v_COD_BCO_ORI := p_COD_BCO_BFC;
         v_COD_BCO_BFC := PKG_PAB_CONSTANTES.V_BIC_BANCO_SANTANDER;
      ELSE
         v_COD_BCO_ORI := PKG_PAB_CONSTANTES.V_BIC_BANCO_SANTANDER;
         v_COD_BCO_BFC := p_COD_BCO_BFC;
      END IF;

      INSERT INTO PABS_DT_MNSJE_PRTCZ (NUM_SUB_MSJ_RLD,
                                       NUM_REF_SWF_RLD,
                                       NUM_REF_SWF,
                                       NUM_FOL_MSJ_PRZ,
                                       HOR_ENV_RCP_SWF,
                                       GLS_EST_RCH,
                                       FLG_MRD_UTZ_SWF,
                                       FLG_EGR_ING,
                                       FEC_ISR_MSJ_PRZ,
                                       FEC_ENV_RCP_SWF,
                                       COD_MT_SWF,
                                       COD_EST_AOS,
                                       COD_BCO_BFC,
                                       COD_ADC_EST,
                                       NUM_FOL_OPE_ORI,
                                       FEC_ISR_OPE_ORG,
                                       COD_TPO_OPE,
                                       COD_BCO_ORI)
        VALUES   (p_NUM_SUB_MSJ_RLD,
                  p_NUM_REF_SWF_RLD,
                  p_NUM_REF_SWF,
                  p_NUM_FOL_MSJ_PRZ,
                  p_HOR_ENV_RCP_SWF,
                  p_GLS_EST_RCH,
                  p_FLG_MRD_UTZ_SWF,
                  p_FLG_EGR_ING,
                  p_FEC_ISR_MSJ_PRZ,
                  p_FEC_ENV_RCP_SWF,
                  p_COD_MT_SWF,
                  p_COD_EST_AOS,
                  v_COD_BCO_BFC,
                  p_COD_ADC_EST,
                  p_NUM_FOL_OPE_ORI,
                  p_FEC_ISR_OPE_ORI,
                  p_COD_TPO_OPE,
                  v_COD_BCO_ORI);
   EXCEPTION
      WHEN DUP_VAL_ON_INDEX
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END Sp_PAB_INS_MSN_PRTCZ;



   --******************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_INS_DETLL_MSN_PRTCZ
   -- Objetivo: Procedimiento que inserta detalle del menjase de protocolo
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_DETLL_MNSJE_PRTCZ
   -- Fecha: 04/01/17
   -- Autor: Santander
   -- Input:
   -- p_NUM_FOL_MSJ_PRZ     -> Folio mensaje
   -- p_FEC_ISR_MSJ_PRZ     -> Fecha inserci�n
   -- p_GLS_TXT_LBE_MSJ     -> Detalle mensaje
   -- Output:
   -- p_ERROR               -> Indicador de errores (0 Sin errores :: 1 existen errores). Se ven en la tabla Log estos errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ?ltimos cambios>
   --***********************************************************************************************
   PROCEDURE Sp_PAB_INS_DETLL_MSN_PRTCZ (p_NUM_FOL_MSJ_PRZ   IN     NUMBER,
                                         p_FEC_ISR_MSJ_PRZ   IN     DATE,
                                         p_GLS_TXT_LBE_MSJ   IN     VARCHAR2,
                                         p_ERROR                OUT NUMBER)
   IS
      --Seteo de Variables
      v_NOM_SP    VARCHAR2 (30) := 'Sp_PAB_INS_DETLL_MSN_PRTCZ';
      TEXTO_ORI   VARCHAR2 (9000) := p_GLS_TXT_LBE_MSJ;
      TEXTO       VARCHAR2 (2000);
      LARGO       VARCHAR2 (2000);
      FIN         NUMBER;
      CANTIDAD    NUMBER;
      INICIO      NUMBER := 1;
   -- TOPE_MSJ_BLQ  AGREGAR A PKG PROTOCOLO


   BEGIN
      LARGO := LENGTH (TEXTO_ORI);
      CANTIDAD := CEIL (LARGO / TOPE_MSJ_BLQ);
      FIN := TOPE_MSJ_BLQ;

      FOR i IN 1 .. CANTIDAD
      LOOP
         TEXTO := SUBSTR (TEXTO_ORI, INICIO, FIN);


         INSERT INTO PABS_DT_DETLL_MNSJE_PRTCZ (NUM_FOL_MSJ_PRZ,
                                                NUM_CAM_REG,
                                                GLS_TXT_LBE_MSJ,
                                                FEC_ISR_MSJ_PRZ)
           VALUES   (p_NUM_FOL_MSJ_PRZ,
                     i,
                     TEXTO,
                     p_FEC_ISR_MSJ_PRZ);

         INICIO := INICIO + TOPE_MSJ_BLQ;
      END LOOP;

      p_ERROR := PKG_PAB_CONSTANTES.v_OK;
   EXCEPTION
      WHEN DUP_VAL_ON_INDEX
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END Sp_PAB_INS_DETLL_MSN_PRTCZ;

   /********************************************************************************************************
    FUNCION/PROCEDIMIENTO: Sp_PAB_COMBO_MT_SWIFT_PRO
    OBJETIVO             : MUESTRA FILTRO EN
                            MENSAJES -> ENVIADO - PROTOCOLO -> CREAR.      COMBO: MT SWIFT
                            MENSAJES -> ENVIADO - PROTOCOLO -> CURSAR.     COMBO: TIPO MT
                            MENSAJES -> ENVIADO - PROTOCOLO -> AUTORIZAR.  COMBO: TIPO MT
    SISTEMA              : DBO_PAB.
    BASE DE DATOS        : DBO_PAB.
    TABLAS USADAS        : PABS_DT_TIPO_MT_SWIFT.
    FECHA                : 27/12/16
    AUTOR                : SANTANDER.
    OUTPUT               : P_CURSOR := DATOS CURSOR.
    OBSERVACIONES
    FECHA       USUARIO    DETALLE
    20-09-2018  VARAYA     SE MODIFICA FILTRO.
    ********************************************************************************************************/
   PROCEDURE Sp_PAB_COMBO_MT_SWIFT_PRO (P_CURSOR OUT SYS_REFCURSOR)
   IS
      ---------------------------------------DECLARACI�N DE VARIABLES Y CONSTANTES----------------------------
      V_NOM_SP   VARCHAR2 (30) := 'Sp_PAB_COMBO_MT_SWIFT_PRO';
   --------------------------------------------------------------------------------------------------------
   BEGIN
      OPEN P_CURSOR FOR
           SELECT   DISTINCT (COD_MT_SWF) AS MT_SWT
             FROM   PABS_DT_TIPO_MT_SWIFT
            WHERE   FLG_VGN = PKG_PAB_CONSTANTES.V_REG_VIG
                    AND FLG_TPO_MT = PKG_PAB_CONSTANTES.V_NUM_MT_PROTOCOLO
         ORDER BY   COD_MT_SWF;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         ERR_CODE := SQLCODE;
         ERR_MSG :=
            'NO SE HAN ENCONTRADO REGISTOS ASOCIADOS A LOS SWIFT CON ESTADO VIGENCIA '
            || PKG_PAB_CONSTANTES.V_REG_VIG
            || ' Y MENSAJE NUMERO '
            || PKG_PAB_CONSTANTES.V_NUM_MT_PROTOCOLO;
         PKG_PAB_TRACKING.SP_PAB_INS_LOG_BD (ERR_CODE,
                                             V_NOM_PCK,
                                             ERR_MSG,
                                             V_NOM_SP);
      WHEN OTHERS
      THEN
         ERR_CODE := SQLCODE;
         ERR_MSG := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.SP_PAB_INS_LOG_BD (ERR_CODE,
                                             V_NOM_PCK,
                                             ERR_MSG,
                                             V_NOM_SP);
         P_S_MENSAJE := ERR_CODE || '-' || ERR_MSG;
         RAISE_APPLICATION_ERROR (-20000, P_S_MENSAJE);
   END Sp_PAB_COMBO_MT_SWIFT_PRO;

   /********************************************************************************************************
   FUNCION/PROCEDIMIENTO: PKG_PAB_PROTOCOLO.SP_PAB_COMBO_AVISOS_PROT.
   OBJETIVO             : MUESTRA FILTRO EN
                           MENSAJES -> ENVIADO - PROTOCOLO -> CREAR.
                           MENSAJES -> ENVIADO - PROTOCOLO -> CURSAR.
                           MENSAJES -> ENVIADO - PROTOCOLO -> AUTORIZAR.
                           CONTINGENCIA -> ENVIADO -> COMBANC UPLOAD - DESCARGAR ARCHIVO.
   SISTEMA              : DBO_PAB.
   BASE DE DATOS        : DBO_PAB.
   TABLAS USADAS        : PABS_DT_TIPO_MT_SWIFT.
   FECHA                : 20/09/2018.
   AUTOR                : VARAYA.
   OUTPUT               : P_CURSOR := DATOS CURSOR.
   OBSERVACIONES
   FECHA       USUARIO    DETALLE
   20-09-2018  VARAYA     CREACI�N.
   ********************************************************************************************************/
   PROCEDURE SP_PAB_COMBO_AVISOS_PROT (P_CURSOR OUT SYS_REFCURSOR)
   IS
      ---------------------------------------DECLARACI�N DE VARIABLES Y CONSTANTES----------------------------
      V_NOM_SP   VARCHAR2 (30) := 'SP_PAB_COMBO_AVISOS_PROT';
   --------------------------------------------------------------------------------------------------------
   BEGIN
      OPEN P_CURSOR FOR
           SELECT   DISTINCT (COD_MT_SWF) AS MT_SWT
             FROM   PABS_DT_TIPO_MT_SWIFT
            WHERE   FLG_VGN = PKG_PAB_CONSTANTES.V_REG_VIG
                    AND FLG_TPO_MT = PKG_PAB_CONSTANTES.V_NUM_MT_AVISOS_PRO
         ORDER BY   COD_MT_SWF;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         ERR_CODE := SQLCODE;
         ERR_MSG :=
            'NO SE HAN ENCONTRADO REGISTOS ASOCIADOS A LOS SWIFT CON ESTADO VIGENCIA '
            || PKG_PAB_CONSTANTES.V_REG_VIG
            || ' Y MENSAJE NUMERO '
            || PKG_PAB_CONSTANTES.V_NUM_MT_AVISOS_PRO;
         PKG_PAB_TRACKING.SP_PAB_INS_LOG_BD (ERR_CODE,
                                             V_NOM_PCK,
                                             ERR_MSG,
                                             V_NOM_SP);
      WHEN OTHERS
      THEN
         ERR_CODE := SQLCODE;
         ERR_MSG := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.SP_PAB_INS_LOG_BD (ERR_CODE,
                                             V_NOM_PCK,
                                             ERR_MSG,
                                             V_NOM_SP);
         P_S_MENSAJE := ERR_CODE || '-' || ERR_MSG;
         RAISE_APPLICATION_ERROR (-20000, P_S_MENSAJE);
   END SP_PAB_COMBO_AVISOS_PROT;

   --**********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_BUS_MNSJE_PRTCZ_HIST
   -- Objetivo: Funci�n para realizar b�squeda de mensajerias hist�rica de protocolo
   -- 799 / 199 / 299 / 298 / 198
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_MNSJE_PRTCZ_HTRCA, PABS_DT_ESTDO_ALMOT, PABS_DT_MNSJE_PRTCZ
   -- Fecha: 30/11/16
   -- Autor: Santander
   -- Input:
   -- p_NUM_FOL_MSJ_PRZ          Folio mensaje
   -- p_COD_EST_AOS              Estado mensaje
   -- p_NUM_REF_SWF              Ref mensaje
   -- p_COD_DVI                  Mercado
   -- p_COD_MT_SWF               Tipo mensaje
   -- p_COD_BCO_DTN              Bic banco
   -- p_FLG_EGR_ING              Flujo
   -- p_FEC_DESDE                Fecha rango desde
   -- p_FEC_HASTA                Fecha rango hasta
   -- Output:
   -- p_CURSOR                    Lista de Mensajes de protocolo
   -- p_ERROR                    Indicador de errores (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de utimos cambios>
   --***********************************************************************************************
   PROCEDURE SP_PAB_BUS_MNSJE_PRTCZ_HIST (
      p_NUM_FOL_MSJ_PRZ   IN     NUMBER,
      p_COD_EST_AOS       IN     NUMBER,
      p_NUM_REF_SWF       IN     CHAR,
      p_COD_DVI           IN     CHAR,
      p_COD_MT_SWF        IN     NUMBER,
      p_COD_BCO_DTN       IN     CHAR,
      p_COD_BCO_ORI       IN     CHAR,
      p_FLG_EGR_ING       IN     NUMBER,
      p_FEC_DESDE         IN     DATE,
      p_FEC_HASTA         IN     DATE,
      p_CURSOR               OUT SYS_REFCURSOR,
      p_ERROR                OUT NUMBER
   )
   IS
      v_NOM_SP           VARCHAR2 (30) := 'SP_PAB_BUS_MNSJE_PRTCZ_HIST';
      v_FLG_MRD_UTZ_SW   CHAR (1) := p_COD_DVI;
      v_COD_BCO_DTN      CHAR (11) := p_COD_BCO_DTN;
      v_COD_BCO_ORI      CHAR (11) := p_COD_BCO_ORI;
      v_NUM_REF_SWF      VARCHAR2 (16) := UPPER (p_NUM_REF_SWF);
      v_FEC_DESDE        DATE;
      v_FEC_HASTA        DATE;
      v_FLG_EGR_ING      NUMBER;
   BEGIN
      IF (p_FEC_DESDE IS NOT NULL)
      THEN
         v_FEC_DESDE := p_FEC_DESDE + INTERVAL '1' MINUTE;
      ELSE
         v_FEC_DESDE := v_dia_ini;          --Dia de sistema mas hora 00:00:01
      END IF;

      IF (p_FEC_HASTA IS NOT NULL)
      THEN
         v_FEC_HASTA :=
            p_FEC_HASTA + INTERVAL '23' HOUR + INTERVAL '59' MINUTE;
      ELSE
         v_FEC_HASTA := v_dia_fin;          --Dia de sistema mas hora 23:59:00
      END IF;

      -- Si es 900 o 910 y viene en tipo de flujo 1 se cambia valor
      IF (p_COD_MT_SWF = PKG_PAB_CONSTANTES.V_COD_MT900
          OR p_COD_MT_SWF = PKG_PAB_CONSTANTES.V_COD_MT910
            AND p_FLG_EGR_ING = 1)
      THEN
         v_FLG_EGR_ING := 3;
      ELSE
         v_FLG_EGR_ING := p_FLG_EGR_ING;
      END IF;

      OPEN p_CURSOR FOR
         SELECT   DETOPE.NUM_FOL_MSJ_PRZ AS FOLIO,
                  DETOPE.COD_EST_AOS AS EST_OPE,
                  EST.DSC_EST_AOS AS DSC_EST,
                  DETOPE.FEC_ISR_MSJ_PRZ AS FECHA,
                  DETOPE.COD_MT_SWF AS MT,
                  DECODE (DETOPE.FLG_MRD_UTZ_SWF,
                          0, 'Mercado Nacional',
                          'Mercado Extranjero')
                     AS MERCADO,
                  DETOPE.NUM_REF_SWF AS REF_SWIFT_ORI,
                  DECODE (DETOPE.FLG_EGR_ING, 0, 'Enviado', 'Recibido')
                     AS FLUJO,
                  DETOPE.COD_BCO_BFC AS BCO_DTN,
                  0 AS MONTO,
                  '' AS COD_DVI,
                  DECODE (DETOPE.FLG_EGR_ING,
                          0, TI.DES_BCO,
                          'BANCO SANTANDER')
                     AS DSC_BCO_DTN,
                  DETOPE.COD_BCO_ORI AS BCO_ORI,
                  PKG_PAB_UTILITY.FN_PAB_OBT_DSC_BCO (DETOPE.COD_BCO_ORI)
                     AS DSC_BCO_ORI,
                  --DECODE(DETOPE.FLG_EGR_ING,PKG_PAB_CONSTANTES.V_FLG_ING, (SELECT TI.DES_BCO
                  --                                  FROM PABS_DT_MNSJE_PRTCZ DETOPE,
                  --                                  TCDTBAI TI
                  --                                 WHERE TI.TGCDSWSA(+)  =  DETOPE.COD_BCO_ORI
                  --                                  AND DETOPE.FLG_EGR_ING =PKG_PAB_CONSTANTES.V_FLG_ING)  ,'BANCO SANTANDER')
                  --                                    AS DSC_BCO_ORI,
                  DETOPE.COD_TPO_OPE AS TPO_OPE,
                  'H' AS ORIGEN,
                  FEC_CGA_HIS AS FEC_CARGA
           FROM   PABS_DT_MNSJE_PRTCZ_HTRCA DETOPE,
                  PABS_DT_ESTDO_ALMOT EST,
                  TCDTBAI TI
          WHERE   DETOPE.NUM_FOL_MSJ_PRZ =
                     NVL (p_NUM_FOL_MSJ_PRZ, DETOPE.NUM_FOL_MSJ_PRZ)
                  AND DETOPE.COD_EST_AOS =
                        NVL (p_COD_EST_AOS, DETOPE.COD_EST_AOS)
                  AND DETOPE.COD_EST_AOS = EST.COD_EST_AOS
                  AND NVL (UPPER (DETOPE.NUM_REF_SWF), '#') =
                        NVL (v_NUM_REF_SWF,
                             NVL (UPPER (DETOPE.NUM_REF_SWF), '#'))
                  AND DETOPE.FLG_MRD_UTZ_SWF =
                        NVL (v_FLG_MRD_UTZ_SW, DETOPE.FLG_MRD_UTZ_SWF)
                  AND DETOPE.COD_MT_SWF =
                        NVL (p_COD_MT_SWF, DETOPE.COD_MT_SWF)
                  AND DETOPE.COD_BCO_BFC =
                        NVL (v_COD_BCO_DTN, DETOPE.COD_BCO_BFC)
                  AND DETOPE.COD_BCO_ORI =
                        NVL (v_COD_BCO_ORI, DETOPE.COD_BCO_ORI)
                  AND DETOPE.FLG_EGR_ING =
                        NVL (v_FLG_EGR_ING, DETOPE.FLG_EGR_ING)
                  AND DETOPE.FEC_ISR_MSJ_PRZ BETWEEN v_FEC_DESDE
                                                 AND  v_FEC_HASTA
                  AND TI.TGCDSWSA(+) = DETOPE.COD_BCO_BFC
         --AND DETOPE.COD_MT_SWF          <> PKG_PAB_CONSTANTES.V_COD_MT299
         UNION
         SELECT   DETOPE.NUM_FOL_MSJ_PRZ AS FOLIO,
                  DETOPE.COD_EST_AOS AS EST_OPE,
                  EST.DSC_EST_AOS AS DSC_EST,
                  DETOPE.FEC_ISR_MSJ_PRZ AS FECHA,
                  DETOPE.COD_MT_SWF AS MT,
                  DECODE (DETOPE.FLG_MRD_UTZ_SWF,
                          0, 'Mercado Nacional',
                          'Mercado Extranjero')
                     AS MERCADO,
                  DETOPE.NUM_REF_SWF AS REF_SWIFT_ORI,
                  DECODE (DETOPE.FLG_EGR_ING, 0, 'Enviado', 'Recibido')
                     AS FLUJO,
                  DETOPE.COD_BCO_BFC AS BCO_DTN,
                  0 AS MONTO,
                  '' AS COD_DVI,
                  DECODE (DETOPE.FLG_EGR_ING,
                          0, TI.DES_BCO,
                          'BANCO SANTANDER')
                     AS DES_BCO_DTN,
                  DETOPE.COD_BCO_ORI AS BCO_ORI,
                  PKG_PAB_UTILITY.FN_PAB_OBT_DSC_BCO (DETOPE.COD_BCO_ORI)
                     AS DES_BCO_ORI,
                  --DECODE(DETOPE.FLG_EGR_ING,PKG_PAB_CONSTANTES.V_FLG_ING, (SELECT TI.DES_BCO
                  --                              FROM PABS_DT_MNSJE_PRTCZ DETOPE,
                  --                              TCDTBAI TI
                  --                              WHERE TI.TGCDSWSA(+)  =  DETOPE.COD_BCO_ORI
                  --                             AND DETOPE.FLG_EGR_ING =PKG_PAB_CONSTANTES.V_FLG_ING)  ,'BANCO SANTANDER')
                  --                              AS DES_ORI,
                  DETOPE.COD_TPO_OPE AS TPO_OPE,
                  'N' AS ORIGEN,
                  SYSDATE AS FEC_CARGA
           FROM   PABS_DT_MNSJE_PRTCZ DETOPE,
                  PABS_DT_ESTDO_ALMOT EST,
                  TCDTBAI TI
          WHERE   DETOPE.NUM_FOL_MSJ_PRZ =
                     NVL (p_NUM_FOL_MSJ_PRZ, DETOPE.NUM_FOL_MSJ_PRZ)
                  AND DETOPE.COD_EST_AOS =
                        NVL (p_COD_EST_AOS, DETOPE.COD_EST_AOS)
                  AND NVL (UPPER (DETOPE.NUM_REF_SWF), '#') LIKE
                        NVL (v_NUM_REF_SWF,
                             NVL (UPPER (DETOPE.NUM_REF_SWF), '#'))
                        || '%'
                  AND DETOPE.FLG_MRD_UTZ_SWF =
                        NVL (v_FLG_MRD_UTZ_SW, DETOPE.FLG_MRD_UTZ_SWF)
                  AND DETOPE.COD_EST_AOS = EST.COD_EST_AOS
                  AND DETOPE.COD_MT_SWF =
                        NVL (p_COD_MT_SWF, DETOPE.COD_MT_SWF)
                  AND DETOPE.COD_BCO_BFC =
                        NVL (v_COD_BCO_DTN, DETOPE.COD_BCO_BFC)
                  AND DETOPE.COD_BCO_ORI =
                        NVL (v_COD_BCO_ORI, DETOPE.COD_BCO_ORI)
                  AND DETOPE.FLG_EGR_ING =
                        NVL (v_FLG_EGR_ING, DETOPE.FLG_EGR_ING)
                  AND DETOPE.FEC_ISR_MSJ_PRZ BETWEEN v_FEC_DESDE
                                                 AND  v_FEC_HASTA
                  AND TI.TGCDSWSA(+) = DETOPE.COD_BCO_BFC
         --AND DETOPE.COD_MT_SWF           <>      PKG_PAB_CONSTANTES.V_COD_MT299

         UNION
         SELECT   DETOPE.NUM_FOL_OPE AS FOLIO,
                  3 AS EST_OPE,
                  'Valida' AS DSC_EST,
                  DETOPE.FEC_ING_OPE AS FECHA,
                  TO_CHAR (DETOPE.COD_MT_SWF) AS MT,
                  DECODE (DETOPE.FLG_MRD_UTZ_SWF,
                          0, 'Mercado Nacional',
                          'Mercado Extranjero')
                     AS MERCADO,
                  DETOPE.NUM_REF_SWF AS REF_SWIFT_ORI,
                  'Recibido' AS FLUJO,
                  DETOPE.COD_BCO_DTN AS BCO_DTN,
                  DETOPE.IMP_OPE AS MONTO,
                  DETOPE.COD_DVI AS COD_DVI,
                  DECODE (DETOPE.FLG_CGO_ABN,
                          0, TI.DES_BCO,
                          'BANCO SANTANDER')
                     AS DES_BCO_DTN,
                  DETOPE.COD_BCO_ORG AS BCO_ORI,
                  PKG_PAB_UTILITY.FN_PAB_OBT_DSC_BCO (DETOPE.COD_BCO_ORG)
                     AS DES_BCO_ORI,
                  '' AS TPO_OPE,
                  'N' AS ORIGEN,
                  SYSDATE AS FEC_CARGA
           FROM   PABS_DT_DETLL_CARGO_ABONO_CAJA DETOPE, TCDTBAI TI
          WHERE   DETOPE.COD_MT_SWF IN
                        (PKG_PAB_CONSTANTES.V_COD_MT900,
                         PKG_PAB_CONSTANTES.V_COD_MT910)
                  AND DETOPE.NUM_FOL_OPE =
                        NVL (p_NUM_FOL_MSJ_PRZ, DETOPE.NUM_FOL_OPE)
                  AND NVL (UPPER (DETOPE.NUM_REF_SWF), '#') LIKE
                        NVL (v_NUM_REF_SWF,
                             NVL (UPPER (DETOPE.NUM_REF_SWF), '#'))
                        || '%'
                  AND DETOPE.FLG_MRD_UTZ_SWF =
                        NVL (v_FLG_MRD_UTZ_SW, DETOPE.FLG_MRD_UTZ_SWF)
                  AND DETOPE.COD_MT_SWF =
                        NVL (p_COD_MT_SWF, DETOPE.COD_MT_SWF)
                  AND DETOPE.COD_BCO_DTN =
                        NVL (v_COD_BCO_DTN, DETOPE.COD_BCO_DTN)
                  AND DETOPE.COD_BCO_ORG =
                        NVL (v_COD_BCO_ORI, DETOPE.COD_BCO_ORG)
                  AND DETOPE.FLG_CGO_ABN =
                        NVL (v_FLG_EGR_ING, DETOPE.FLG_CGO_ABN)
                  AND DETOPE.FEC_ING_OPE BETWEEN v_FEC_DESDE AND v_FEC_HASTA
                  AND TI.TGCDSWSA(+) = DETOPE.COD_BCO_DTN
         ORDER BY   FOLIO;

      p_ERROR := PKG_PAB_CONSTANTES.v_OK;
   --Procedimiento ejecutado de forma correcta
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg :=
               SUBSTR (SQLERRM, 1, 300)
            || 'No se encontro operaci�n folio:'
            || p_NUM_FOL_MSJ_PRZ;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   --
   END SP_PAB_BUS_MNSJE_PRTCZ_HIST;


   --**********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_MNSJE_PRTCZ_HIST
   -- Objetivo: Funcion para realizar busqueda de mensajerias de protocolo
   -- 799 / 199 / 298 / 198
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_TIPO_OPRCN, PABS_DT_DETLL_MNSJE_PRTCZ_HTRC, PABS_DT_MNSJE_PRTCZ_HTRCA, PABS_DT_DETLL_OPRCN_HTRCA
   -- Fecha: 31/03/17
   -- Autor: Santander
   -- Input:
   -- p_NUM_FOL_MSJ_PRZ     -> Folio mensaje
   -- p_FEC_ISR_MSJ_PRZ     -> Fecha inserci�n
   -- p_FEC_CARGA           -> Fecha carga
   -- p_ORIGEN              -> Origen de b�squeda de mensaje
   -- Output:
   -- p_CURSOR              -> Lista los mensajes de protocolo
   -- p_ERROR               -> Indicador de errores (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de utimos cambios>
   --***********************************************************************************************--
   PROCEDURE SP_PAB_MNSJE_PRTCZ_HIST (
      p_NUM_FOL_MSJ_PRZ   IN     NUMBER,
      p_FEC_ISR_MSJ_PRZ   IN     DATE,
      p_FEC_CARGA         IN     DATE,
      p_ORIGEN            IN     CHAR,
      p_CURSOR               OUT SYS_REFCURSOR,
      p_ERROR                OUT NUMBER
   )
   IS
      v_NOM_SP        VARCHAR2 (30) := 'SP_PAB_MNSJE_PRTCZ_HIST';
      v_GLS_TPO_OPE   VARCHAR2 (200);
      v_CANTIDAD      NUMBER;
      v_CTOR          NUMBER;
      v_MENSAJE       VARCHAR2 (300);
      v_MENSAJE2      VARCHAR2 (2000);
      v_TPO_ORI_N     CHAR := 'N';
      v_TPO_ORI_H     CHAR := 'H';
      ERR_COD_INEX EXCEPTION;
   BEGIN
      CASE
         --Cuando la operacion se encuentra en la tabla Historica
         WHEN p_ORIGEN = v_TPO_ORI_H
         THEN
            --Tablas Historicas
            BEGIN
               SELECT   COUNT (NUM_FOL_MSJ_PRZ)
                 INTO   v_CANTIDAD
                 FROM   PABS_DT_DETLL_MNSJE_PRTCZ_HTRC
                WHERE       NUM_FOL_MSJ_PRZ = p_NUM_FOL_MSJ_PRZ
                        AND FEC_ISR_MSJ_PRZ = p_FEC_ISR_MSJ_PRZ
                        AND FEC_CGA_HIS = p_FEC_CARGA;
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  err_code := SQLCODE;
                  err_msg :=
                     'No se encontro informaci�n con los valores p_NUM_FOL_MSJ_PRZ:'
                     || p_NUM_FOL_MSJ_PRZ
                     || ' p_FEC_ISR_MSJ_PRZ:'
                     || TO_CHAR (p_FEC_ISR_MSJ_PRZ, 'dd-mm-yyyy hh24:mi:ss');
                  PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                      v_NOM_PCK,
                                                      err_msg,
                                                      v_NOM_SP);
               WHEN OTHERS
               THEN
                  err_code := SQLCODE;
                  err_msg := SUBSTR (SQLERRM, 1, 300);
                  PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                      v_NOM_PCK,
                                                      err_msg,
                                                      v_NOM_SP);
                  p_s_mensaje := err_code || '-' || err_msg;
                  RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
            END;


            BEGIN
               WHILE v_CANTIDAD > 0
               LOOP
                  SELECT   GLS_TXT_LBE_MSJ
                    INTO   v_MENSAJE
                    FROM   PABS_DT_DETLL_MNSJE_PRTCZ_HTRC
                   WHERE       NUM_FOL_MSJ_PRZ = p_NUM_FOL_MSJ_PRZ
                           AND FEC_ISR_MSJ_PRZ = p_FEC_ISR_MSJ_PRZ
                           AND NUM_CAM_REG = v_CANTIDAD;

                  v_MENSAJE2 := v_MENSAJE || v_MENSAJE2;
                  v_CANTIDAD := v_CANTIDAD - 1;
               END LOOP;
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  err_code := SQLCODE;
                  err_msg :=
                     'Loop: No se encontro informaci�n con los valores p_NUM_FOL_MSJ_PRZ:'
                     || p_NUM_FOL_MSJ_PRZ
                     || ' p_FEC_ISR_MSJ_PRZ:'
                     || TO_CHAR (p_FEC_ISR_MSJ_PRZ, 'dd-mm-yyyy hh24:mi:ss');
                  PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                      v_NOM_PCK,
                                                      err_msg,
                                                      v_NOM_SP);
               WHEN OTHERS
               THEN
                  err_code := SQLCODE;
                  err_msg := SUBSTR (SQLERRM, 1, 300);
                  PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                      v_NOM_PCK,
                                                      err_msg,
                                                      v_NOM_SP);
                  p_s_mensaje := err_code || '-' || err_msg;
                  RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
            END;

            OPEN p_CURSOR FOR
               SELECT   MJEPRT.COD_MT_SWF AS MT_MSG,
                        MJEPRT.NUM_FOL_MSJ_PRZ AS NUM_OPE_MSG,
                        MJEPRT.FEC_ISR_MSJ_PRZ AS FEC_OPE_MSG,
                        MJEPRT.COD_EST_AOS AS EST_OPE_MSG,
                        EST.DSC_EST_AOS AS DSC_EST,
                        MJEPRT.NUM_SUB_MSJ_RLD AS NUM_SUB_MSG,
                        MJEPRT.GLS_EST_RCH AS MSJ_GLS_ADC,
                        DECODE (MJEPRT.FLG_EGR_ING,
                                PKG_PAB_CONSTANTES.V_FLG_ENV,
                                PKG_PAB_CONSTANTES.V_STR_FLG_ENV,
                                PKG_PAB_CONSTANTES.V_STR_FLG_REC)
                           AS TPO_FJO_MSG,
                        TO_CHAR (MJEPRT.FEC_ENV_RCP_SWF, 'DD/MM/YYYY')
                           AS FEC_RCP_SW,
                        MJEPRT.HOR_ENV_REC_SWF AS HORA_RCP_SW,
                        'Operaciones Financieras' AS CANAL,
                        '0174' AS SUCURSAL,
                        TIPOPRT.GLS_TPO_OPE AS MOTIVO,
                        MJEPRT.NUM_REF_SWF_RLD AS REF_SWIFT_ORI,
                        MJEPRT.NUM_REF_SWF AS REF_SWIFT,
                        MJEPRT.COD_BCO_BFC AS BIC_DESTINO,
                        MJEPRT.COD_BCO_ORI AS BIC_ORIGEN,
                        DECODE (MJEPRT.FLG_MRD_UTZ_SWF,
                                PKG_PAB_CONSTANTES.v_FLG_MRD_MN,
                                'Mercado Nacional',
                                'Mercado Extranjero')
                           AS MERCADO,
                        MJEPRT.COD_TPO_OPE AS TIPO_OPE,
                        v_MENSAJE2 AS MENSAJE,
                        DECODE (MJEPRT.FLG_EGR_ING,
                                PKG_PAB_CONSTANTES.V_FLG_EGR, 'Enviado',
                                'Ingreso')
                           AS FLG_ENV_ING
                 FROM   PABS_DT_MNSJE_PRTCZ_HTRCA MJEPRT,
                        PABS_DT_TIPO_OPRCN TIPOPRT,
                        PABS_DT_ESTDO_ALMOT EST
                WHERE       MJEPRT.NUM_FOL_MSJ_PRZ = p_NUM_FOL_MSJ_PRZ
                        AND MJEPRT.FEC_ISR_MSJ_PRZ = p_FEC_ISR_MSJ_PRZ
                        AND MJEPRT.FEC_CGA_HIS = p_FEC_CARGA
                        AND MJEPRT.COD_TPO_OPE = TIPOPRT.COD_TPO_OPE;
         --Tablas Normales
         WHEN p_ORIGEN = v_TPO_ORI_N
         THEN
            BEGIN
               SELECT   COUNT (1)
                 INTO   v_CANTIDAD
                 FROM   PABS_DT_DETLL_MNSJE_PRTCZ
                WHERE   NUM_FOL_MSJ_PRZ = p_NUM_FOL_MSJ_PRZ
                        AND FEC_ISR_MSJ_PRZ = p_FEC_ISR_MSJ_PRZ;
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  err_code := SQLCODE;
                  err_msg :=
                     'No se encontro informaci�n con los valores p_NUM_FOL_MSJ_PRZ:'
                     || p_NUM_FOL_MSJ_PRZ
                     || ' p_FEC_ISR_MSJ_PRZ:'
                     || TO_CHAR (p_FEC_ISR_MSJ_PRZ, 'dd-mm-yyyy hh24:mi:ss');
                  PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                      v_NOM_PCK,
                                                      err_msg,
                                                      v_NOM_SP);
               WHEN OTHERS
               THEN
                  err_code := SQLCODE;
                  err_msg := SUBSTR (SQLERRM, 1, 300);
                  PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                      v_NOM_PCK,
                                                      err_msg,
                                                      v_NOM_SP);
                  p_s_mensaje := err_code || '-' || err_msg;
                  RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
            END;

            BEGIN
               WHILE v_CANTIDAD > 0
               LOOP
                  SELECT   GLS_TXT_LBE_MSJ
                    INTO   v_MENSAJE
                    FROM   PABS_DT_DETLL_MNSJE_PRTCZ
                   WHERE       NUM_FOL_MSJ_PRZ = p_NUM_FOL_MSJ_PRZ
                           AND FEC_ISR_MSJ_PRZ = p_FEC_ISR_MSJ_PRZ
                           AND NUM_CAM_REG = v_CANTIDAD;

                  v_MENSAJE2 := v_MENSAJE || v_MENSAJE2;
                  v_CANTIDAD := v_CANTIDAD - 1;
               END LOOP;
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  err_code := SQLCODE;
                  err_msg :=
                     'Loop: No se encontro informaci�n con los valores p_NUM_FOL_MSJ_PRZ:'
                     || p_NUM_FOL_MSJ_PRZ
                     || ' p_FEC_ISR_MSJ_PRZ:'
                     || TO_CHAR (p_FEC_ISR_MSJ_PRZ, 'dd-mm-yyyy hh24:mi:ss');
                  PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                      v_NOM_PCK,
                                                      err_msg,
                                                      v_NOM_SP);
               WHEN OTHERS
               THEN
                  err_code := SQLCODE;
                  err_msg := SUBSTR (SQLERRM, 1, 300);
                  PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                      v_NOM_PCK,
                                                      err_msg,
                                                      v_NOM_SP);
                  p_s_mensaje := err_code || '-' || err_msg;
                  RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
            END;

            OPEN p_CURSOR FOR
               SELECT   MJEPRT.COD_MT_SWF AS MT_MSG,
                        MJEPRT.NUM_FOL_MSJ_PRZ AS NUM_OPE_MSG,
                        MJEPRT.FEC_ISR_MSJ_PRZ AS FEC_OPE_MSG,
                        MJEPRT.COD_EST_AOS AS EST_OPE_MSG,
                        EST.DSC_EST_AOS AS DSC_EST,
                        MJEPRT.NUM_SUB_MSJ_RLD AS NUM_SUB_MSG,
                        MJEPRT.GLS_EST_RCH AS MSJ_GLS_ADC,
                        DECODE (MJEPRT.FLG_EGR_ING,
                                PKG_PAB_CONSTANTES.V_FLG_ENV,
                                PKG_PAB_CONSTANTES.V_STR_FLG_ENV,
                                PKG_PAB_CONSTANTES.V_STR_FLG_REC)
                           AS TPO_FJO_MSG,
                        TO_CHAR (MJEPRT.FEC_ENV_RCP_SWF, 'DD/MM/YYYY')
                           AS FEC_RCP_SW,
                        MJEPRT.HOR_ENV_RCP_SWF AS HORA_RCP_SW,
                        'Operaciones Financieras' AS CANAL,
                        '0174' AS SUCURSAL,
                        TIPOPRT.GLS_TPO_OPE AS MOTIVO,
                        MJEPRT.NUM_REF_SWF_RLD AS REF_SWIFT_ORI,
                        MJEPRT.NUM_REF_SWF AS REF_SWIFT,
                        MJEPRT.COD_BCO_BFC AS BIC_DESTINO,
                        MJEPRT.COD_BCO_ORI AS BIC_ORIGEN,
                        DECODE (MJEPRT.FLG_MRD_UTZ_SWF,
                                PKG_PAB_CONSTANTES.v_FLG_MRD_MN,
                                'Mercado Nacional',
                                'Mercado Extranjero')
                           AS MERCADO,
                        MJEPRT.COD_TPO_OPE AS TIPO_OPE,
                        v_MENSAJE2 AS MENSAJE,
                        DECODE (MJEPRT.FLG_EGR_ING,
                                PKG_PAB_CONSTANTES.V_FLG_EGR, 'Enviado',
                                'Ingreso')
                           AS FLG_ENV_ING
                 FROM   PABS_DT_MNSJE_PRTCZ MJEPRT,
                        PABS_DT_TIPO_OPRCN TIPOPRT,
                        PABS_DT_ESTDO_ALMOT EST
                WHERE       MJEPRT.COD_EST_AOS = EST.COD_EST_AOS
                        AND MJEPRT.NUM_FOL_MSJ_PRZ = p_NUM_FOL_MSJ_PRZ
                        AND MJEPRT.FEC_ISR_MSJ_PRZ = p_FEC_ISR_MSJ_PRZ
                        AND MJEPRT.COD_TPO_OPE = TIPOPRT.COD_TPO_OPE;
         --AND DETOPE.COD_EST_AOS      =  EST.COD_EST_AOS;

         --            FROM  PABS_DT_MNSJE_PRTCZ MJEPRT,
         --            PABS_DT_DETLL_OPRCN DETOPE,
         --            PABS_DT_TIPO_OPRCN TIPOPRT,
         --            PABS_DT_ESTDO_ALMOT EST
         --            WHERE
         --            MJEPRT.NUM_REF_SWF_RLD = DETOPE.NUM_REF_SWF(+) AND
         --            MJEPRT.COD_EST_AOS =  EST.COD_EST_AOS and
         --            MJEPRT.NUM_FOL_MSJ_PRZ =  929 and--p_NUM_FOL_MSJ_PRZ AND
         --            MJEPRT.FEC_ISR_MSJ_PRZ = to_date('05-01-2018 15:32:58','dd-mm-yyyy hh24:mi:ss') --05/01/2018 15:32   and p_FEC_ISR_MSJ_PRZ  AND
         --            and MJEPRT.COD_TPO_OPE = TIPOPRT.COD_TPO_OPE



         ELSE
            RAISE ERR_COD_INEX;
      END CASE;

      p_ERROR := PKG_PAB_CONSTANTES.v_OK;
   --                                                                                                                                                                                                                                                                                                                                                                                                                                --Procedimiento ejecutado de forma correcta
   EXCEPTION
      WHEN ERR_COD_INEX
      THEN
         err_msg :=
            'Se esta invocando con codigo historificacion inexistente:'
            || p_ORIGEN;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (NULL,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg :=
               SUBSTR (SQLERRM, 1, 300)
            || 'No se encontro operaci�n folio:'
            || p_NUM_FOL_MSJ_PRZ
            || ' Fecha:'
            || TO_CHAR (p_FEC_ISR_MSJ_PRZ, 'dd-mm-yyyy hh24:mm:ss');
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   --
   END SP_PAB_MNSJE_PRTCZ_HIST;



   --************************************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_GES_MSJ_PROT
   -- Objetivo: Gestiona la inserci�n de mensaje de protocolo.
   -- Sistema: DBO_PAB.
   -- Base de Datos: DBO_PAB.
   -- Tablas Usadas: PABS_DT_DETLL_OPRCN
   -- Fecha: 16/03/2017
   -- Autor: Santander
   -- p_NUM_SUB_MSJ_RLD  -> N�mero submensaje
   -- p_NUM_REF_SWF_RLD  -> Referencia relacionada
   -- p_NUM_REF_SWF      -> Referencia de mensaje
   -- p_HOR_ENV_RCP_SWF  -> Hora swift
   -- p_GLS_EST_RCH      -> Glosa rechazo
   -- p_FLG_MRD_UTZ_SWF  -> Flag de mercado
   -- p_FLG_EGR_ING      -> Flag flujo
   -- p_FEC_ENV_RCP_SWF  -> Fecha wsift
   -- p_COD_MT_SWF       -> Tipo de mensaje
   -- p_COD_EST_AOS      -> Estado mensaje
   -- p_COD_BCO_BFC      -> Bic Beneficiario
   -- p_COD_ADC_EST      -> Estado adicional
   -- p_NUM_FOL_OPE_ORI  -> N�mero de operaci�n
   -- p_FEC_OPE_ORI      -> N�mero de operaci�n
   -- p_COD_TPO_OPE      -> Tipo de operaci�n
   -- p_NUM_CAM_REG      -> N�mero campo por registro
   -- p_GLS_TXT_LBE_MSJ  -> Glosa texto libre
   -- p_COD_USR          -> C�digo usuario
   -- Output:
   -- p_ERROR -> indicador de errores, maneja dos estados, 0 sin errores 1 existen errores
   -- p_NUM_FOL_MSJ_PRZ  -> Folio mensaje
   -- p_FEC_ISR_MSJ_PRZ  -> fecha mensaje
   -- Retorno: N/A.
   --***********************************************************************************************
   --
   PROCEDURE Sp_PAB_GES_MSJ_PROT (p_NUM_SUB_MSJ_RLD   IN     CHAR,
                                  p_NUM_REF_SWF_RLD   IN     CHAR,
                                  p_NUM_REF_SWF       IN     CHAR,
                                  p_HOR_ENV_RCP_SWF   IN     NUMBER,
                                  p_GLS_EST_RCH       IN     VARCHAR2,
                                  p_FLG_MRD_UTZ_SWF   IN     NUMBER,
                                  p_FLG_EGR_ING       IN     NUMBER,
                                  p_FEC_ENV_RCP_SWF   IN     DATE,
                                  p_COD_MT_SWF        IN     CHAR,
                                  p_COD_EST_AOS       IN     VARCHAR2,
                                  p_COD_BCO_BFC       IN     CHAR,
                                  p_COD_ADC_EST       IN     CHAR,
                                  p_NUM_FOL_OPE_ORI   IN     NUMBER,
                                  p_FEC_OPE_ORI       IN     DATE,
                                  p_COD_TPO_OPE       IN     CHAR,
                                  p_NUM_CAM_REG       IN     NUMBER,
                                  p_GLS_TXT_LBE_MSJ   IN     VARCHAR2,
                                  p_COD_USR           IN     VARCHAR2,
                                  p_NUM_FOL_MSJ_PRZ      OUT NUMBER,
                                  p_FEC_ISR_MSJ_PRZ      OUT VARCHAR2,
                                  p_ERROR                OUT NUMBER)
   IS
      --Seteo de Variables
      v_NOM_SP            VARCHAR2 (30) := 'Sp_PAB_GES_MSJ_PROT';
      v_FEC_ISR_MSJ_PRZ   DATE := SYSDATE;
      v_COD_EST_AOS       VARCHAR2 (30);
      v_COD_EST_AOS_ACT   NUMBER;
      v_RESULT            NUMBER;
      v_NUM_FOL_MSJ_PRZ   NUMBER;
      v_FEC_ISR_OPE       DATE;
      v_GLS_EST_ASO       VARCHAR2 (30) := 'Operaci�n asociada a MT298';
      v_COD_TPO_OPE       CHAR (8);
      --Respuesta para bot�n Confirmar 298
      v_GLS_MSJ_RES       VARCHAR2 (9000);
   BEGIN
      -- INICIALIZACION DE VARIABLES Y PARAMETROS

      IF p_FLG_EGR_ING <> PKG_PAB_CONSTANTES.V_FLG_ING
      THEN
         v_NUM_FOL_MSJ_PRZ := SPK_PAB_MSJ_EAM.NEXTVAL;
         v_COD_TPO_OPE := p_COD_TPO_OPE;
      ELSE
         v_NUM_FOL_MSJ_PRZ := SPK_PAB_MSJ_IAM.NEXTVAL;
         v_COD_TPO_OPE := v_COD_TPO_OPE_BCOEXT;
      END IF;

      p_ERROR := 0;
      v_COD_EST_AOS :=
         PKG_PAB_UTILITY.FN_PAB_OBT_COD_DSC_EST (
            p_COD_EST_AOS,
            PKG_PAB_CONSTANTES.V_IND_OPE
         );
      v_COD_EST_AOS_ACT := PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEVAL;


      --Inserci�n de cabecera de mensaje de protocolo;
      Sp_PAB_INS_MSN_PRTCZ (p_NUM_SUB_MSJ_RLD,
                            p_NUM_REF_SWF_RLD,
                            p_NUM_REF_SWF,
                            v_NUM_FOL_MSJ_PRZ,
                            p_HOR_ENV_RCP_SWF,
                            p_GLS_EST_RCH,
                            p_FLG_MRD_UTZ_SWF,
                            p_FLG_EGR_ING,
                            v_FEC_ISR_MSJ_PRZ,
                            p_FEC_ENV_RCP_SWF,
                            p_COD_MT_SWF,
                            v_COD_EST_AOS,
                            p_COD_BCO_BFC,
                            p_COD_ADC_EST,
                            p_NUM_FOL_OPE_ORI,
                            p_FEC_OPE_ORI,
                            v_COD_TPO_OPE,
                            p_ERROR);

      IF p_COD_MT_SWF = PKG_PAB_CONSTANTES.V_COD_MT299
      THEN
         IF p_NUM_FOL_OPE_ORI IS NULL
         THEN
            v_GLS_MSJ_RES :=
                  'Su mensaje MT298:'
               || p_NUM_REF_SWF
               || ', ha sido recepcionado';
         ELSE
            v_GLS_MSJ_RES :=
                  'Su mensaje MT298:'
               || p_NUM_REF_SWF
               || ', solicitud ha sido recepcionada';
         END IF;
      --ELSIF v_COD_EST_AOS = PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEVAL THEN
      --   v_GLS_MSJ_RES := p_GLS_TXT_LBE_MSJ;

      END IF;


      v_GLS_MSJ_RES := p_GLS_TXT_LBE_MSJ;

      --'Iniciando proceso grabaci�n de mensaje de protocolo 298 ';
      Sp_PAB_INS_DETLL_MSN_PRTCZ (v_NUM_FOL_MSJ_PRZ,
                                  v_FEC_ISR_MSJ_PRZ,
                                  v_GLS_MSJ_RES,
                                  p_ERROR);               -- p_GLS_TXT_LBE_MSJ

      --Llamamos al procedimiento de Bitacora
      v_DES_BIT := 'Inserci�n de mensaje protocolo';
      PKG_PAB_TRACKING.Sp_PAB_INS_BIT_OPE (v_NUM_FOL_MSJ_PRZ,
                                           v_FEC_ISR_MSJ_PRZ,
                                           p_COD_USR,
                                           v_COD_EST_AOS,
                                           v_COD_EST_AOS,
                                           v_DES_BIT,
                                           v_RESULT);

      --Variables de salida de la operaci�n recien creada
      p_NUM_FOL_MSJ_PRZ := v_NUM_FOL_MSJ_PRZ;
      p_FEC_ISR_MSJ_PRZ := TO_CHAR (SYSDATE, 'DD-MM-YYYY HH24:MI:SS');
      p_ERROR := PKG_PAB_CONSTANTES.v_OK;

      COMMIT;
   EXCEPTION
      WHEN DUP_VAL_ON_INDEX
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END Sp_PAB_GES_MSJ_PROT;



    /************************************************************************************************
-- Funcion/Procedimiento: FN_PAB_GEN_REF_SWF_PRT
-- Objetivo: Fusion que retorne el codifo de referencia Swift para env�o  mensajeria de protocolo
-- Sistema: DBO_PAB.
-- Base de Datos: DBO_PAB.
-- Tablas Usadas: N/A
-- Fecha: 02/06/16
-- Autor: Santander
-- Input:      p_COD_TPO_OPE:      Tipo de mensaje de protocolo
-- Output:
-- v_REF_SWF Retorna el codigo de referencia Swift Protocolo
-- Input/Output: N/A
-- Retorno: N/A.
-- Observaciones: <Fecha y Detalle de ultimos cambios>
--***********************************************************************************************/
   FUNCTION FN_PAB_GEN_REF_SWF_PRT (p_COD_TPO_OPE IN CHAR)
      RETURN VARCHAR2
   IS
      v_COD_TPO_OPE   VARCHAR2 (16) := p_COD_TPO_OPE;
      v_REF_SWF       VARCHAR2 (16);
      v_NOM_SP        VARCHAR2 (30) := 'FN_PAB_GEN_REF_SWF_PRT';
   BEGIN
      v_REF_SWF :=
         'MSJ' || SUBSTR (LPAD (SPK_PAB_COD_REF_LBTR.NEXTVAL, 6, '0'), 1, 6);


      RETURN v_REF_SWF;
   EXCEPTION
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         p_s_mensaje := err_code || '-' || err_msg;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END FN_PAB_GEN_REF_SWF_PRT;


   --******************************************************************************
   -- Funcion/Procedimiento: Sp_PAB_ACT_MSN_PRT_SWF
   -- Objetivo: Procedimiento que actualiza mensaje de protocolo tras envio swift
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_MNSJE_PRTCZ,
   -- Fecha: 31/03/17
   -- Autor: Santander
   -- Input:
   --p_NUM_FOL_MSJ_PRZ    --> Numero foliador mensajeria protocolo
   --p_FEC_ISR_MSJ_PRZ    --> Fecha insercion mensajeria protocolo
   --p_NUM_REF_SWF        --> Numero de referencia
   --p_COD_EST_AOS        -->  C�digo estado
   --p_FEC_ENV_RCP_SWF    -->  Fecha recepci�n swift
   --p_HOR_ENV_RCP_SWF    -->  Hora recepci�n swift
   --p_FLG_ENV_RCP        -->  Flag env�o (0) o recpeci�n (1)
   --p_MSJ_EST_RCH        -->  Mensaje rechazo u observaci�n
   --p_COD_USR            -->  Rut usuario
   -- Output:
   -- p_ERROR             --> Indicador de errores (0 Sin errores :: 1 existen errores). Se ven en la tabla Log estos errores.
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de ?ltimos cambios>
   --***********************************************************************************************

   PROCEDURE Sp_PAB_ACT_MSN_PRT_SWF (p_NUM_FOL_MSJ_PRZ   IN     NUMBER,
                                     p_FEC_ISR_MSJ_PRZ   IN     DATE,
                                     p_NUM_REF_SWF       IN     CHAR,
                                     p_COD_EST_AOS       IN     VARCHAR2,
                                     p_FEC_ENV_RCP_SWF   IN     DATE,
                                     p_HOR_ENV_RCP_SWF   IN     NUMBER,
                                     p_FLG_ENV_RCP       IN     NUMBER,
                                     p_MSJ_EST_RCH       IN     VARCHAR2,
                                     p_COD_USR           IN     VARCHAR2,
                                     p_ERROR                OUT NUMBER)
   IS
      --Seteo de Variables
      v_NOM_SP            VARCHAR2 (30) := 'Sp_PAB_ACT_MSN_PRT_SWF';
      v_NUM_FOL_MSJ_PRZ   NUMBER;
      v_FEC_ISR_MSJ_PRZ   DATE;
      v_NUM_REF_SWF       CHAR (16) := p_NUM_REF_SWF;
      v_COD_EST_AOS       NUMBER;
      ERROR_MSJ EXCEPTION;
   BEGIN
      IF p_FLG_ENV_RCP = PKG_PAB_CONSTANTES.V_FLG_ENV
      THEN
         BEGIN
            UPDATE   PABS_DT_MNSJE_PRTCZ MNS_PRTCZ
               SET   MNS_PRTCZ.FEC_ENV_RCP_SWF = p_FEC_ENV_RCP_SWF,
                     MNS_PRTCZ.HOR_ENV_RCP_SWF = p_HOR_ENV_RCP_SWF,
                     MNS_PRTCZ.NUM_REF_SWF = v_NUM_REF_SWF
             WHERE   MNS_PRTCZ.NUM_FOL_MSJ_PRZ = p_NUM_FOL_MSJ_PRZ
                     AND MNS_PRTCZ.FEC_ISR_MSJ_PRZ = p_FEC_ISR_MSJ_PRZ;

            COMMIT;
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               err_code := SQLCODE;
               err_msg :=
                     SUBSTR (SQLERRM, 1, 300)
                  || 'Error en actualizaci�n de fecha y hora swift. Mensaje '
                  || p_NUM_FOL_MSJ_PRZ
                  || 'Ref:'
                  || v_NUM_REF_SWF;
               PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                   v_NOM_PCK,
                                                   err_msg,
                                                   v_NOM_SP);
               p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
               RAISE;
            WHEN OTHERS
            THEN
               err_code := SQLCODE;
               err_msg := SUBSTR (SQLERRM, 1, 300);
               p_s_mensaje := err_code || '-' || err_msg;
               PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                   v_NOM_PCK,
                                                   err_msg,
                                                   v_NOM_SP);
               p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
               RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
         END;
      ELSE
         --Actulaizaci�n de estado tras respuesta Swift ( ACK/NACK )
         BEGIN
            SELECT   MNS_PRTCZ.NUM_FOL_MSJ_PRZ, MNS_PRTCZ.FEC_ISR_MSJ_PRZ
              INTO   v_NUM_FOL_MSJ_PRZ, v_FEC_ISR_MSJ_PRZ
              FROM   PABS_DT_MNSJE_PRTCZ MNS_PRTCZ
             WHERE   MNS_PRTCZ.NUM_REF_SWF = v_NUM_REF_SWF;

            --Obtenci�n de c�digo de Estado
            v_COD_EST_AOS :=
               PKG_PAB_UTILITY.FN_PAB_OBT_COD_DSC_EST (
                  p_COD_EST_AOS,
                  PKG_PAB_CONSTANTES.V_IND_OPE
               );

            UPDATE   PABS_DT_MNSJE_PRTCZ MNS_PRTCZ
               SET   MNS_PRTCZ.COD_EST_AOS = v_COD_EST_AOS,
                     MNS_PRTCZ.GLS_EST_RCH = p_MSJ_EST_RCH
             WHERE   MNS_PRTCZ.NUM_FOL_MSJ_PRZ = v_NUM_FOL_MSJ_PRZ
                     AND MNS_PRTCZ.FEC_ISR_MSJ_PRZ = v_FEC_ISR_MSJ_PRZ;

            COMMIT;
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               err_code := SQLCODE;
               err_msg :=
                  'No se encontro informaci�n con los valores v_NUM_REF_SWF:'
                  || v_NUM_REF_SWF;
               PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                   v_NOM_PCK,
                                                   err_msg,
                                                   v_NOM_SP);
               p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
            WHEN OTHERS
            THEN
               err_code := SQLCODE;
               err_msg := SUBSTR (SQLERRM, 1, 300);
               PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                                   v_NOM_PCK,
                                                   err_msg,
                                                   v_NOM_SP);
               p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
               p_s_mensaje := err_code || '-' || err_msg;
               RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
         END;
      END IF;

      p_ERROR := PKG_PAB_CONSTANTES.v_OK;
   EXCEPTION
      WHEN ERROR_MSJ
      THEN
         err_msg :=
            'No se encontro mensaje asociado a mensaje:' || p_NUM_FOL_MSJ_PRZ;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (NULL,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN DUP_VAL_ON_INDEX
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END Sp_PAB_ACT_MSN_PRT_SWF;


   --**********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_BUS_MNSJE_ING
   -- Objetivo: Procedimiento que retorna los mt298 de ingreso
   --
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_MNSJE_PRTCZ, PABS_DT_DETLL_MNSJE_PRTCZ, PABS_DT_ESTDO_ALMOT
   -- Fecha: 12-01-2018
   -- Autor: Santander- CAH
   -- Input:
   -- p_NUM_REF_SWF         -->  Referencia de mensaje.
   -- p_COD_BCO_DTN         -->  C�digo banco origen.
   -- p_FEC_DESDE           -->  Fecha incio b�squeda.
   -- p_FEC_HASTA           -->  Fecha fin b�squeda.
   -- Output:
   -- p_CURSOR ->  Lista los mensajes de protocolo MT298
   -- p_ERROR -> Indicada si el procedimiento
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: 2019-11-26: Se le agrega busqueda en tabla historica para identificr 103 antiguos
   --*************************************************************************************************
   PROCEDURE SP_PAB_BUS_MNSJE_ING (p_NUM_REF_SWF   IN     CHAR,
                                   p_COD_BCO_ORI   IN     CHAR,
                                   p_FEC_DESDE     IN     DATE,
                                   p_FEC_HASTA     IN     DATE,
                                   p_CURSOR           OUT SYS_REFCURSOR,
                                   p_ERROR            OUT NUMBER)
   IS
      v_NOM_SP        VARCHAR2 (30) := 'SP_PAB_BUS_MNSJE_ING';
      v_COD_EST_AOS   NUMBER;
      v_COD_BCO_ORI   CHAR (11) := p_COD_BCO_ORI;
      v_NUM_REF_SWF   VARCHAR2 (16) := UPPER (p_NUM_REF_SWF);
      v_FEC_DESDE     DATE;
      v_FEC_HASTA     DATE;
   BEGIN
      IF (p_FEC_DESDE IS NOT NULL)
      THEN
         v_FEC_DESDE := p_FEC_DESDE + INTERVAL '1' MINUTE;
      ELSE
         v_FEC_DESDE := v_dia_ini;          --Dia de sistema mas hora 00:00:01
      END IF;

      IF (p_FEC_HASTA IS NOT NULL)
      THEN
         v_FEC_HASTA :=
            p_FEC_HASTA + INTERVAL '23' HOUR + INTERVAL '59' MINUTE;
      ELSE
         v_FEC_HASTA := v_dia_fin;          --Dia de sistema mas hora 23:59:00
      END IF;


      --Buscamos los mensajes de protocolo de ingreso.
      OPEN p_CURSOR FOR
           SELECT   DETOPE.NUM_FOL_MSJ_PRZ AS FOLIO,
                    DETOPE.COD_EST_AOS AS EST_OPE,
                    EST.DSC_EST_AOS AS DSC_EST,
                    DETOPE.FEC_ISR_MSJ_PRZ AS FECHA,
                    DETOPE.COD_MT_SWF AS MT,
                    DETOPE.NUM_REF_SWF AS REF_SWIFT,
                    DECODE (DETOPE.FLG_EGR_ING,
                            PKG_PAB_CONSTANTES.V_FLG_ENV,
                            PKG_PAB_CONSTANTES.V_STR_FLG_ENV,
                            PKG_PAB_CONSTANTES.V_STR_FLG_REC)
                       AS FLUJO,
                    PKG_PAB_UTILITY.FN_PAB_OBT_DSC_BCO (DETOPE.COD_BCO_ORI)
                       AS DSC_BCO_ORI,
                    DETOPE.COD_BCO_ORI AS BCO_ORI,
                    --DETOPE.COD_BCO_BFC          AS COD_BCO,
                    DETOPE.COD_TPO_OPE AS TIP_OP,
                    DETOPE.NUM_FOL_MSJ_PRZ AS NUM_OPE_298,
                    DETOPE.FEC_ISR_MSJ_PRZ AS FEC_REC,       --AS FEC_OPE_298,
                    DETOPE.NUM_REF_SWF_RLD AS REF_ORI,
                    DECODE (DETOPE.FLG_MRD_UTZ_SWF,
                            PKG_PAB_CONSTANTES.v_FLG_MRD_MN,
                            'Mercado Nacional',
                            'Mercado Extranjero')
                       AS MERCADO,
                    DETOPE.COD_TPO_OPE AS MOTIVO,
                    RTRIM (
                       XMLAGG(XMLELEMENT (
                                 PABS_DT_DETLL_MNSJE_PRTCZ,
                                 GLS_TXT_LBE_MSJ || CHR (13) || CHR (10)
                              )).EXTRACT ('//text()'),
                       CHR (13) || CHR (10)
                    )
                       AS CAMPO77,
                    DETOPE.NUM_FOL_OPE_ORI AS NUM_FOL_OPE_ORI,
                    DETOPE.FEC_ISR_OPE_ORG AS FEC_ISR_OPE_ORI,
                    DETOPE.GLS_EST_RCH AS GLOSA_RECHAZO
             FROM   PABS_DT_MNSJE_PRTCZ DETOPE,
                    PABS_DT_DETLL_MNSJE_PRTCZ MSJPRT,
                    PABS_DT_ESTDO_ALMOT EST
            WHERE       DETOPE.NUM_FOL_MSJ_PRZ = MSJPRT.NUM_FOL_MSJ_PRZ
                    AND DETOPE.FEC_ISR_MSJ_PRZ = MSJPRT.FEC_ISR_MSJ_PRZ
                    AND DETOPE.COD_EST_AOS = EST.COD_EST_AOS
                    AND NVL (UPPER (DETOPE.NUM_REF_SWF), '#') LIKE
                          NVL (v_NUM_REF_SWF,
                               NVL (UPPER (DETOPE.NUM_REF_SWF), '#'))
                          || '%'
                    AND NVL (DETOPE.COD_BCO_ORI, '#') =
                          NVL (v_COD_BCO_ORI, DETOPE.COD_BCO_ORI)
                    AND DETOPE.FEC_ISR_MSJ_PRZ BETWEEN v_FEC_DESDE
                                                   AND  v_FEC_HASTA
                    AND DETOPE.FLG_EGR_ING = PKG_PAB_CONSTANTES.V_FLG_ING
                    AND DETOPE.COD_MT_SWF = PKG_PAB_CONSTANTES.V_COD_MT298
                    AND DETOPE.COD_EST_AOS NOT IN
                             (PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPENDE,
                              PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEASO)
         GROUP BY   DETOPE.NUM_FOL_MSJ_PRZ,
                    DETOPE.COD_EST_AOS,
                    EST.DSC_EST_AOS,
                    DETOPE.FEC_ISR_MSJ_PRZ,
                    DETOPE.COD_MT_SWF,
                    DETOPE.NUM_REF_SWF,
                    DETOPE.FLG_EGR_ING,
                    DETOPE.COD_BCO_ORI,
                    DETOPE.COD_BCO_BFC,
                    DETOPE.COD_TPO_OPE,
                    DETOPE.NUM_REF_SWF_RLD,
                    DETOPE.FLG_MRD_UTZ_SWF,
                    DETOPE.NUM_FOL_OPE_ORI,
                    DETOPE.FEC_ISR_OPE_ORG,
                    DETOPE.GLS_EST_RCH
         UNION ALL
           SELECT   DETOPEHIST.NUM_FOL_MSJ_PRZ AS FOLIO,
                    DETOPEHIST.COD_EST_AOS AS EST_OPE,
                    EST.DSC_EST_AOS AS DSC_EST,
                    DETOPEHIST.FEC_ISR_MSJ_PRZ AS FECHA,
                    DETOPEHIST.COD_MT_SWF AS MT,
                    DETOPEHIST.NUM_REF_SWF AS REF_SWIFT,
                    DECODE (DETOPEHIST.FLG_EGR_ING,
                            PKG_PAB_CONSTANTES.V_FLG_ENV,
                            PKG_PAB_CONSTANTES.V_STR_FLG_ENV,
                            PKG_PAB_CONSTANTES.V_STR_FLG_REC)
                       AS FLUJO,
                    PKG_PAB_UTILITY.FN_PAB_OBT_DSC_BCO (DETOPEHIST.COD_BCO_ORI)
                       AS DSC_BCO_ORI,
                    DETOPEHIST.COD_BCO_ORI AS BCO_ORI,
                    --DETOPEHIST.COD_BCO_BFC          AS COD_BCO,
                    DETOPEHIST.COD_TPO_OPE AS TIP_OP,
                    DETOPEHIST.NUM_FOL_MSJ_PRZ AS NUM_OPE_298,
                    DETOPEHIST.FEC_ISR_MSJ_PRZ AS FEC_REC,   --AS FEC_OPE_298,
                    DETOPEHIST.NUM_REF_SWF_RLD AS REF_ORI,
                    DECODE (DETOPEHIST.FLG_MRD_UTZ_SWF,
                            PKG_PAB_CONSTANTES.v_FLG_MRD_MN,
                            'Mercado Nacional',
                            'Mercado Extranjero')
                       AS MERCADO,
                    DETOPEHIST.COD_TPO_OPE AS MOTIVO,
                    RTRIM (
                       XMLAGG(XMLELEMENT (
                                 PABS_DT_DETLL_MNSJE_PRTCZ,
                                 GLS_TXT_LBE_MSJ || CHR (13) || CHR (10)
                              )).EXTRACT ('//text()'),
                       CHR (13) || CHR (10)
                    )
                       AS CAMPO77,
                    DETOPEHIST.NUM_FOL_OPE_ORG AS NUM_FOL_OPE_ORI,
                    DETOPEHIST.FEC_ISR_OPE_ORG AS FEC_ISR_OPE_ORI,
                    DETOPEHIST.GLS_EST_RCH AS GLOSA_RECHAZO
             FROM   PABS_DT_MNSJE_PRTCZ_HTRCA DETOPEHIST,
                    PABS_DT_DETLL_MNSJE_PRTCZ_HTRC MSJPRT,
                    PABS_DT_ESTDO_ALMOT EST
            WHERE       DETOPEHIST.NUM_FOL_MSJ_PRZ = MSJPRT.NUM_FOL_MSJ_PRZ
                    AND DETOPEHIST.FEC_ISR_MSJ_PRZ = MSJPRT.FEC_ISR_MSJ_PRZ
                    AND DETOPEHIST.COD_EST_AOS = EST.COD_EST_AOS
                    AND NVL (UPPER (DETOPEHIST.NUM_REF_SWF), '#') LIKE
                          NVL (v_NUM_REF_SWF,
                               NVL (UPPER (DETOPEHIST.NUM_REF_SWF), '#'))
                          || '%'
                    AND NVL (DETOPEHIST.COD_BCO_ORI, '#') =
                          NVL (v_COD_BCO_ORI, DETOPEHIST.COD_BCO_ORI)
                    AND DETOPEHIST.FEC_ISR_MSJ_PRZ BETWEEN v_FEC_DESDE
                                                       AND  v_FEC_HASTA
                    AND DETOPEHIST.FLG_EGR_ING = PKG_PAB_CONSTANTES.V_FLG_ING
                    AND DETOPEHIST.COD_MT_SWF = PKG_PAB_CONSTANTES.V_COD_MT298
                    AND DETOPEHIST.COD_EST_AOS NOT IN
                             (PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPENDE,
                              PKG_PAB_CONSTANTES.V_COD_EST_AOS_OPEASO)
         GROUP BY   DETOPEHIST.NUM_FOL_MSJ_PRZ,
                    DETOPEHIST.COD_EST_AOS,
                    EST.DSC_EST_AOS,
                    DETOPEHIST.FEC_ISR_MSJ_PRZ,
                    DETOPEHIST.COD_MT_SWF,
                    DETOPEHIST.NUM_REF_SWF,
                    DETOPEHIST.FLG_EGR_ING,
                    DETOPEHIST.COD_BCO_ORI,
                    DETOPEHIST.COD_BCO_BFC,
                    DETOPEHIST.COD_TPO_OPE,
                    DETOPEHIST.NUM_REF_SWF_RLD,
                    DETOPEHIST.FLG_MRD_UTZ_SWF,
                    DETOPEHIST.NUM_FOL_OPE_ORG,
                    DETOPEHIST.FEC_ISR_OPE_ORG,
                    DETOPEHIST.GLS_EST_RCH;

      p_ERROR := PKG_PAB_CONSTANTES.v_OK;
   --                                                                                                                                                                                                                                                                                                                                                                                                                                --Procedimiento ejecutado de forma correcta
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg :=
            'No se encontro informaci�n con los valores v_NUM_REF_SWF:'
            || v_NUM_REF_SWF;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   --
   END SP_PAB_BUS_MNSJE_ING;

   --**********************************************************************************************
   -- Funcion/Procedimiento: SP_PAB_BUS_MNSJE_910_900_HIST
   -- Objetivo: Funci�n para realizar b�squeda de mensajerias hist�rica de protocolo
   -- 799 / 199 / 299 / 298 / 198
   -- Sistema: PAB
   -- Base de Datos: DGBMSEGDB01_NGALTM
   -- Tablas Usadas: PABS_DT_MNSJE_PRTCZ_HTRCA, PABS_DT_ESTDO_ALMOT, PABS_DT_MNSJE_PRTCZ
   -- Fecha: 30/11/16
   -- Autor: Santander
   -- Input:
   -- p_FOL_OPE
   -- p_FEC_OPE
   -- Output:
   -- p_CURSOR                    Lista de Mensajes de protocolo
   -- p_ERROR                    Indicador de errores (Se ven en la tabla Log estos errores)
   -- Input/Output: N/A
   -- Retorno: N/A.
   -- Observaciones: <Fecha y Detalle de utimos cambios>
   --***********************************************************************************************
   PROCEDURE SP_PAB_BUS_MNSJE_910_900_HIST (p_FOL_OPE   IN     NUMBER,
                                            p_FEC_OPE   IN     DATE,
                                            p_CURSOR       OUT SYS_REFCURSOR,
                                            p_ERROR        OUT NUMBER)
   IS
      v_NOM_SP    VARCHAR2 (30) := 'SP_PAB_BUS_MNSJE_910_900_HIST';
      v_FEC_OPE   DATE := TRUNC (p_FEC_OPE);
   BEGIN
      OPEN p_CURSOR FOR
         SELECT   NUM_FOL_OPE,
                  FEC_ING_OPE,
                  ABOCAJ.FLG_MRD_UTZ_SWF,
                  ABOCAJ.COD_MT_SWF,
                  COD_DVI,
                  COD_BCO_DTN,
                  COD_BCO_ORG,
                  COD_BCO_ITD,
                  DECODE (FLG_CGO_ABN,
                          PKG_PAB_CONSTANTES.V_FLG_MOV_ABN,
                          'Abono',
                          PKG_PAB_CONSTANTES.V_FLG_MOV_CGO,
                          'Cargo')
                     AS FLG_CGO_ABN,
                  NUM_SEQ_IFD,
                  FEC_ING_IFD,
                  HOR_ING_IFD,
                  NUM_REF_SWF,
                  NUM_CTA_CTE,
                  FEC_VTA,
                  IMP_OPE,
                  GLS_TRN,
                  NUM_REF_EXT,
                  SIS_ENT.DSC_SIS_ENT_SAL DSC_SIS_ENT,
                  SIS_SAL.DSC_SIS_ENT_SAL DSC_SIS_SAL,
                  TIPO_OPE.DSC_TPO_OPE_AOS,
                  COD_SIS_ENT COD_SIS_ENT,
                  COD_SIS_SAL COD_SIS_SAL,
                  ABOCAJ.COD_TPO_OPE_AOS COD_TPO_OPE_AOS
           FROM   PABS_DT_DETLL_CARGO_ABONO_CAJA ABOCAJ,
                  PABS_DT_SISTM_ENTRD_SALID SIS_ENT,
                  PABS_DT_SISTM_ENTRD_SALID SIS_SAL,
                  PABS_DT_TIPO_OPRCN_ALMOT TIPO_OPE
          WHERE       ABOCAJ.COD_SIS_ENT = SIS_ENT.COD_SIS_ENT_SAL(+)
                  AND ABOCAJ.COD_SIS_SAL = SIS_SAL.COD_SIS_ENT_SAL(+)
                  AND TIPO_OPE.COD_TPO_OPE_AOS = ABOCAJ.COD_TPO_OPE_AOS(+)
                  AND NUM_FOL_OPE = p_FOL_OPE
                  AND TRUNC (FEC_ING_OPE) = v_FEC_OPE;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         err_code := SQLCODE;
         err_msg :=
               SUBSTR (SQLERRM, 1, 300)
            || 'No se encontro operaci�n folio:'
            || p_FOL_OPE;
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.Sp_PAB_INS_LOG_BD (err_code,
                                             v_NOM_PCK,
                                             err_msg,
                                             v_NOM_SP);
         p_ERROR := PKG_PAB_CONSTANTES.v_ERROR;
         p_s_mensaje := err_code || '-' || err_msg;
         RAISE_APPLICATION_ERROR (-20000, p_s_mensaje);
   END SP_PAB_BUS_MNSJE_910_900_HIST;

   /********************************************************************************************************
   FUNCION/PROCEDIMIENTO: SP_PAB_ACT_COD_ENT_SAL.
   OBJETIVO             : RECORRE LOS MT 900/910 SELECCIONADOS PARA CAMBIAR EL CANAL DE ENTRADA
                          O SALIDA Y LOS ACTUALIZA.
   SISTEMA              : DBO_PAB.
   BASE DE DATOS        : DBO_PAB.
   TABLAS USADAS        : PABS_DT_DETLL_CARGO_ABONO_CAJA.
                          PABS_DT_SALDO_AREA.
   FECHA                : 28/08/2018.
   AUTOR                : VARAYA.
   INPUT                : P_REG_OPER    := 1� REGISTRO := FECHA.
                                           2� REGISTRO := N�MERO FOLIO.
                                           3� REGISTRO := C�DIGO MT.
                          P_COD_ENT_SAL := CANAL SELECCIONADO.
                          P_COD_USR     := USUARIO GESTOR.
   OUTPUT               : P_ERROR       := INDICADOR DE ERRORES:
                                           0 := SIN ERRORES.
                                           1 := CON ERRORES.
   OBSERVACIONES
   FECHA       USUARIO  DETALLE
   28-08-2018  VARAYA   CREACI�N SP.
   03-09-2018  VARAYA   SE INCORPORAN SP'S QUE REGULARIZA TABLA PABS_DT_SALDO_AREA:
                          1. PKG_PAB_CAJA_MN.SP_BUS_ACT_AREA_MN (SP ACTUALIZA SALDOS POR �REA)
                          2. PKG_PAB_CAJA_MN.SP_BUS_CALCULA_AREA_MN (SP CALCULA SALDOS POR �REA).
   04-10-2018  VARAYA   SE INCORPORA CONDICI�N PARA LA ACTUALIZACI�N DE SALDOS POR �REA (SP_BUS_ACT_AREA_MN)
                        CUANDO EL CANAL DE SALIDA ANTERIOR SEA NULO.
   ********************************************************************************************************/
   PROCEDURE SP_PAB_ACT_COD_ENT_SAL (P_REG_OPER      IN     REG_OPER,
                                     P_COD_ENT_SAL   IN     CHAR,
                                     P_COD_USR       IN     CHAR,
                                     P_ERROR            OUT NUMBER)
   IS
      ---------------------------------------DECLARACI�N DE VARIABLES Y CONSTANTES----------------------------
      V_FEC_ING_OPE        DATE;
      V_HORA_ACTUAL        NUMBER (4) := TO_NUMBER (TO_CHAR (SYSDATE, 'HH24MI'));
      V_NOM_SP             VARCHAR2 (31) := 'SP_PAB_ACT_COD_ENT_SAL';
      V_GLS_DET_PCS        VARCHAR2 (300);
      V_COD_DVI            PABS_DT_DETLL_CARGO_ABONO_CAJA.COD_DVI%TYPE;
      V_IMP_OPE            PABS_DT_DETLL_CARGO_ABONO_CAJA.IMP_OPE%TYPE;
      V_COD_MT_SWF         PABS_DT_DETLL_CARGO_ABONO_CAJA.COD_MT_SWF%TYPE;
      V_NUM_FOL_OPE        PABS_DT_DETLL_CARGO_ABONO_CAJA.NUM_FOL_OPE%TYPE;
      COD_SIS_ENTSAL_OLD   PABS_DT_DETLL_CARGO_ABONO_CAJA.COD_SIS_SAL%TYPE;
   --------------------------------------------------------------------------------------------------------
   BEGIN
      --CONSULTO SI HAY CANALES A MODIFICAR.
      IF P_REG_OPER.COUNT > PKG_PAB_CONSTANTES.V_CANT_0
      THEN
         --CONSULTO SI LA HORA EXCEDI� EL L�MITE DIARIO.
         IF PKG_PAB_CONSTANTES.V_HORA_LIMITE > v_HORA_ACTUAL
         THEN
            --RECORRO REGISTRO POR REGISTRO Y LO GUARDO EN VARIABLES SEG�N TIPO.
            FOR I IN P_REG_OPER.FIRST .. P_REG_OPER.LAST
            LOOP
               V_FEC_ING_OPE := P_REG_OPER (I).FEC_ISR_OPE; --1� REGISTRO := FECHA.
               V_NUM_FOL_OPE := P_REG_OPER (I).NUM_FOL_OPE; --2� REGISTRO := N�MERO FOLIO.

               --GUARDO LOS VALORES ANTERIORES ANTES DE MODIFICAR.
               SELECT   COD_DVI,
                        COD_MT_SWF,
                        CASE
                           WHEN COD_MT_SWF = PKG_PAB_CONSTANTES.V_COD_MT900
                           THEN
                              COD_SIS_ENT
                           ELSE
                              COD_SIS_SAL
                        END,
                        IMP_OPE
                 INTO   V_COD_DVI,
                        V_COD_MT_SWF,
                        COD_SIS_ENTSAL_OLD,
                        V_IMP_OPE
                 FROM   PABS_DT_DETLL_CARGO_ABONO_CAJA
                WHERE   COD_MT_SWF IN
                              (PKG_PAB_CONSTANTES.V_COD_MT900,
                               PKG_PAB_CONSTANTES.V_COD_MT910)
                        AND COD_DVI = PKG_PAB_CONSTANTES.V_STR_DVI_CLP
                        AND TO_CHAR (FEC_ING_OPE, 'DD-MM-YYYY HH24:MI:SS') =
                              TO_CHAR (V_FEC_ING_OPE,
                                       'DD-MM-YYYY HH24:MI:SS')
                        AND NUM_FOL_OPE = V_NUM_FOL_OPE;

               --CONSULTO SI ES MT900 Y ACTUALIZO.
               IF V_COD_MT_SWF = PKG_PAB_CONSTANTES.V_COD_MT900
               THEN
                  BEGIN
                     UPDATE   PABS_DT_DETLL_CARGO_ABONO_CAJA
                        SET   COD_SIS_ENT = P_COD_ENT_SAL
                      WHERE   TO_CHAR (FEC_ING_OPE, 'DD-MM-YYYY HH24:MI:SS') =
                                 TO_CHAR (V_FEC_ING_OPE,
                                          'DD-MM-YYYY HH24:MI:SS')
                              AND NUM_FOL_OPE = V_NUM_FOL_OPE
                              AND COD_MT_SWF = V_COD_MT_SWF;
                  EXCEPTION
                     WHEN OTHERS
                     THEN
                        ERR_CODE := SQLCODE;
                        ERR_MSG := SUBSTR (SQLERRM, 1, 300);
                        PKG_PAB_TRACKING.SP_PAB_INS_LOG_BD (ERR_CODE,
                                                            V_NOM_PCK,
                                                            ERR_MSG,
                                                            V_NOM_SP);
                        P_ERROR := PKG_PAB_CONSTANTES.V_ERROR;
                        P_S_MENSAJE := ERR_CODE || '-' || ERR_MSG;
                        RAISE_APPLICATION_ERROR (-20000, P_S_MENSAJE);
                  END;

                  ERR_CODE := SQLCODE;
                  V_GLS_DET_PCS :=
                        'Se modific� folio: '
                     || V_NUM_FOL_OPE
                     || ' Usuario: '
                     || P_COD_USR
                     || ' Fecha: '
                     || SYSDATE
                     || ' Canal anterior: '
                     || NVL (COD_SIS_ENTSAL_OLD, 'Sin asignar')
                     || ' Canal nuevo: '
                     || P_COD_ENT_SAL;
                  PKG_PAB_TRACKING.SP_PAB_INS_LOG_BD (ERR_CODE,
                                                      V_NOM_PCK,
                                                      V_GLS_DET_PCS,
                                                      V_NOM_SP);
               --CONSULTO SI ES MT910 Y ACTUALIZO.
               ELSIF V_COD_MT_SWF = PKG_PAB_CONSTANTES.V_COD_MT910
               THEN
                  BEGIN
                     UPDATE   PABS_DT_DETLL_CARGO_ABONO_CAJA
                        SET   COD_SIS_SAL = P_COD_ENT_SAL
                      WHERE   TO_CHAR (FEC_ING_OPE, 'DD-MM-YYYY HH24:MI:SS') =
                                 TO_CHAR (V_FEC_ING_OPE,
                                          'DD-MM-YYYY HH24:MI:SS')
                              AND NUM_FOL_OPE = V_NUM_FOL_OPE
                              AND COD_MT_SWF = V_COD_MT_SWF;
                  EXCEPTION
                     WHEN OTHERS
                     THEN
                        ERR_CODE := SQLCODE;
                        ERR_MSG := SUBSTR (SQLERRM, 1, 300);
                        PKG_PAB_TRACKING.SP_PAB_INS_LOG_BD (ERR_CODE,
                                                            V_NOM_PCK,
                                                            ERR_MSG,
                                                            V_NOM_SP);
                        P_ERROR := PKG_PAB_CONSTANTES.V_ERROR;
                        P_S_MENSAJE := ERR_CODE || '-' || ERR_MSG;
                        RAISE_APPLICATION_ERROR (-20000, P_S_MENSAJE);
                  END;

                  ERR_CODE := SQLCODE;
                  V_GLS_DET_PCS :=
                        'Se modific� folio: '
                     || V_NUM_FOL_OPE
                     || ' Usuario: '
                     || P_COD_USR
                     || ' Fecha: '
                     || SYSDATE
                     || ' Canal anterior: '
                     || NVL (COD_SIS_ENTSAL_OLD, 'Sin asignar')
                     || ' Canal nuevo: '
                     || P_COD_ENT_SAL;
                  PKG_PAB_TRACKING.SP_PAB_INS_LOG_BD (ERR_CODE,
                                                      V_NOM_PCK,
                                                      V_GLS_DET_PCS,
                                                      V_NOM_SP);
               END IF;

               --CONSULTO SI TEN�A CANAL ASIGNADO ANTERIORMENTE
               IF COD_SIS_ENTSAL_OLD IS NOT NULL
               THEN
                  --ACTUALIZO (RESTO) SALDO POR �REA
                  PKG_PAB_CAJA_MN.SP_BUS_ACT_AREA_MN (
                     COD_SIS_ENTSAL_OLD,
                     CASE
                        WHEN V_COD_MT_SWF = PKG_PAB_CONSTANTES.V_COD_MT910
                        THEN
                           V_IMP_OPE
                        ELSE
                           PKG_PAB_CONSTANTES.V_CANT_0
                     END,
                     CASE
                        WHEN V_COD_MT_SWF = PKG_PAB_CONSTANTES.V_COD_MT900
                        THEN
                           V_IMP_OPE
                        ELSE
                           PKG_PAB_CONSTANTES.V_CANT_0
                     END,
                     P_ERROR
                  );
               END IF;

               --ACTUALIZO (SUMO) SALDO POR �REA
               PKG_PAB_CAJA_MN.SP_BUS_CALCULA_AREA_MN (
                  P_COD_ENT_SAL,
                  CASE
                     WHEN V_COD_MT_SWF = PKG_PAB_CONSTANTES.V_COD_MT910
                     THEN
                        V_IMP_OPE
                     ELSE
                        PKG_PAB_CONSTANTES.V_CANT_0
                  END,
                  CASE
                     WHEN V_COD_MT_SWF = PKG_PAB_CONSTANTES.V_COD_MT900
                     THEN
                        V_IMP_OPE
                     ELSE
                        PKG_PAB_CONSTANTES.V_CANT_0
                  END,
                  P_ERROR
               );
            END LOOP;
         ELSE
            P_ERROR := PKG_PAB_CONSTANTES.V_COD_CMB_CNL_NOHOR;
         END IF;
      END IF;

      P_ERROR := PKG_PAB_CONSTANTES.V_OK;
      COMMIT;
   EXCEPTION
      WHEN OTHERS
      THEN
         ERR_CODE := SQLCODE;
         ERR_MSG := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.SP_PAB_INS_LOG_BD (ERR_CODE,
                                             V_NOM_PCK,
                                             ERR_MSG,
                                             V_NOM_SP);
         P_ERROR := PKG_PAB_CONSTANTES.V_ERROR;
         P_S_MENSAJE := ERR_CODE || '-' || ERR_MSG;
   END SP_PAB_ACT_COD_ENT_SAL;


   /***********************************************************************************************
     FUNCION/PROCEDIMIENTO: SP_PAB_BUS_CAMBIAR_CANAL.
     OBJETIVO             : BUSCA LOS MENSAJES DE PROTOCOLO QUE NO
     SISTEMA              : DBO_PAB.
     BASE DE DATOS        : DBO_PAB.
     TABLAS USADAS        : PABS_DT_DETLL_CARGO_ABONO_CAJA
     FECHA                : 28/08/2018.
     AUTOR                : SANTANDER
     INPUT                : p_COD_MT_SWF -> Codigo MT
                          : p_NUM_REF_SWF -> Numero de referencia Swift
                          : p_COD_TPO_OPE_AOS -> Tipo operaci�n
                          : P_COD_SIS_SAL -> Codigo sistema salida
                          : P_COD_SIS_ENT -> Codigo sistema entrada
                          : p_COD_BCO_ORG -> Bic Banco Origen
     OUTPUT               : p_CURSOR     -> Cursor con operaciones de protocolo que cumplen los filtros
                          : P_ERROR       := INDICADOR DE ERRORES:
                                             0 := SIN ERRORES.
                                             1 := CON ERRORES.
     OBSERVACIONES        : 28-08-2018, CREACI�N SP.
   ***********************************************************************************************/
   PROCEDURE SP_PAB_BUS_CAMBIAR_CANAL (
      P_COD_MT_SWF        IN     NUMBER,
      P_NUM_REF_SWF       IN     CHAR,
      P_COD_TPO_OPE_AOS   IN     CHAR,
      P_COD_SIS_SAL       IN     CHAR,
      P_COD_SIS_ENT       IN     CHAR,
      P_COD_BCO_ORG       IN     CHAR,
      P_CURSOR               OUT SYS_REFCURSOR,
      P_ERROR                OUT NUMBER
   )
   IS
      ---------------------------------------DECLARACI�N DE VARIABLES Y CONSTANTES----------------------------
      V_NOM_SP            VARCHAR2 (30) := 'SP_PAB_BUS_CAMBIAR_CANAL';
      V_COD_MT_SWF        PABS_DT_DETLL_CARGO_ABONO_CAJA.COD_MT_SWF%TYPE;
      V_COD_BCO_ORG       PABS_DT_DETLL_CARGO_ABONO_CAJA.COD_BCO_ORG%TYPE;
      V_COD_SIS_SAL       PABS_DT_DETLL_CARGO_ABONO_CAJA.COD_SIS_SAL%TYPE;
      V_COD_SIS_ENT       PABS_DT_DETLL_CARGO_ABONO_CAJA.COD_SIS_ENT%TYPE;
      V_COD_TPO_OPE_AOS   PABS_DT_DETLL_CARGO_ABONO_CAJA.COD_TPO_OPE_AOS%TYPE;
      V_NUM_REF_SWF PABS_DT_DETLL_CARGO_ABONO_CAJA.NUM_REF_SWF%TYPE
            := UPPER (P_NUM_REF_SWF) ;
   --------------------------------------------------------------------------------------------------------
   BEGIN
      --ASIGNACI�N DE PAR�METROS DE ENTRADA A VARIABLES.
      --C�DIGO SWIFT.
      IF P_COD_MT_SWF = -1
      THEN
         V_COD_MT_SWF := NULL;
      ELSE
         V_COD_MT_SWF := P_COD_MT_SWF;
      END IF;

      --N�MERO REFERENCIA SWIFT.
      IF P_NUM_REF_SWF = '-1'
      THEN
         V_NUM_REF_SWF := NULL;
      ELSE
         V_NUM_REF_SWF := P_NUM_REF_SWF;
      END IF;

      --C�DIGO TIPO DE OPERACI�N AOS.
      IF P_COD_TPO_OPE_AOS = '-1'
      THEN
         V_COD_TPO_OPE_AOS := NULL;
      ELSE
         V_COD_TPO_OPE_AOS := P_COD_TPO_OPE_AOS;
      END IF;

      --C�DIGO SISTEMA SALIDA.
      IF P_COD_SIS_SAL = '-1'
      THEN
         V_COD_SIS_SAL := NULL;
      ELSE
         V_COD_SIS_SAL := P_COD_SIS_SAL;
      END IF;

      --C�DIGO SISTEMA ENTRADA.
      IF P_COD_SIS_ENT = '-1'
      THEN
         V_COD_SIS_ENT := NULL;
      ELSE
         V_COD_SIS_ENT := P_COD_SIS_ENT;
      END IF;

      --C�DIGO BANCO ORIGINAL.
      IF P_COD_BCO_ORG = '-1'
      THEN
         V_COD_BCO_ORG := NULL;
      ELSE
         V_COD_BCO_ORG := P_COD_BCO_ORG;
      END IF;

      --CURSOR MUESTRA LA INFORMACI�N NECESARIA PARA EFECTUAR CAMBIOS A TRAV�S DEL FRONT (RUTA EN OBJETIVO).

      IF (P_COD_SIS_SAL = 'SINASIG' AND P_COD_SIS_ENT = 'SINASIG')
      THEN
         OPEN P_CURSOR FOR
              SELECT   TO_CHAR (DETOPE.COD_MT_SWF) TIPO_MT,
                       DETOPE.NUM_REF_SWF REF_SWIFT,
                       DETOPE.NUM_REF_EXT REF_SWIFT_EXT,
                       DETOPE.IMP_OPE MONTO,
                       DETOPE.NUM_FOL_OPE NUMERO_OPE,
                       DETOPE.FEC_ING_OPE FECHA_OPE,
                       DETOPE.COD_TPO_OPE_AOS TIPO_OPE,
                       CE.DSC_SIS_ENT_SAL CANAL_ENT,
                       CS.DSC_SIS_ENT_SAL CANAL_SAL,
                       DETOPE.COD_BCO_ORG BIC_ORIGEN,
                       DETOPE.COD_BCO_DTN BIC_DESTINO
                FROM   PABS_DT_DETLL_CARGO_ABONO_CAJA DETOPE,
                       PABS_DT_SISTM_ENTRD_SALID CE,
                       PABS_DT_SISTM_ENTRD_SALID CS
               WHERE   DETOPE.COD_MT_SWF IN
                             (PKG_PAB_CONSTANTES.V_COD_MT900,
                              PKG_PAB_CONSTANTES.V_COD_MT910)
                       AND NVL (DETOPE.COD_SIS_SAL, '#') NOT IN
                                (PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SAL_CTACTE) --SI ES NULO LO ELIMINA
                       AND DETOPE.COD_MT_SWF =
                             NVL (V_COD_MT_SWF, DETOPE.COD_MT_SWF)
                       AND NVL (DETOPE.NUM_REF_SWF, '#') LIKE
                             NVL (V_NUM_REF_SWF, NVL (DETOPE.NUM_REF_SWF, '#'))
                             || '%'
                       AND DETOPE.COD_TPO_OPE_AOS =
                             NVL (V_COD_TPO_OPE_AOS, DETOPE.COD_TPO_OPE_AOS)
                       AND (DETOPE.COD_SIS_ENT IS NULL
                            OR DETOPE.COD_SIS_SAL IS NULL)
                       AND DETOPE.COD_BCO_ORG =
                             NVL (V_COD_BCO_ORG, DETOPE.COD_BCO_ORG)
                       AND DETOPE.COD_SIS_ENT = CE.COD_SIS_ENT_SAL(+)
                       AND DETOPE.COD_SIS_SAL = CS.COD_SIS_ENT_SAL(+)
                       AND DETOPE.FEC_VTA = TRUNC (SYSDATE)
                       AND DETOPE.COD_DVI = PKG_PAB_CONSTANTES.V_STR_DVI_CLP
            ORDER BY   NUM_FOL_OPE;
      ELSE
         IF (P_COD_SIS_SAL = 'SINASIG' AND P_COD_SIS_ENT <> 'SINASIG')
         THEN
            OPEN P_CURSOR FOR
                 SELECT   TO_CHAR (DETOPE.COD_MT_SWF) TIPO_MT,
                          DETOPE.NUM_REF_SWF REF_SWIFT,
                          DETOPE.NUM_REF_EXT REF_SWIFT_EXT,
                          DETOPE.IMP_OPE MONTO,
                          DETOPE.NUM_FOL_OPE NUMERO_OPE,
                          DETOPE.FEC_ING_OPE FECHA_OPE,
                          DETOPE.COD_TPO_OPE_AOS TIPO_OPE,
                          CE.DSC_SIS_ENT_SAL CANAL_ENT,
                          CS.DSC_SIS_ENT_SAL CANAL_SAL,
                          DETOPE.COD_BCO_ORG BIC_ORIGEN,
                          DETOPE.COD_BCO_DTN BIC_DESTINO
                   FROM   PABS_DT_DETLL_CARGO_ABONO_CAJA DETOPE,
                          PABS_DT_SISTM_ENTRD_SALID CE,
                          PABS_DT_SISTM_ENTRD_SALID CS
                  WHERE   DETOPE.COD_MT_SWF IN
                                (PKG_PAB_CONSTANTES.V_COD_MT900,
                                 PKG_PAB_CONSTANTES.V_COD_MT910)
                          AND NVL (DETOPE.COD_SIS_SAL, '#') NOT IN
                                   (PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SAL_CTACTE) --SI ES NULO LO ELIMINA
                          AND DETOPE.COD_MT_SWF =
                                NVL (V_COD_MT_SWF, DETOPE.COD_MT_SWF)
                          AND NVL (DETOPE.NUM_REF_SWF, '#') LIKE
                                NVL (V_NUM_REF_SWF,
                                     NVL (DETOPE.NUM_REF_SWF, '#'))
                                || '%'
                          AND DETOPE.COD_TPO_OPE_AOS =
                                NVL (V_COD_TPO_OPE_AOS, DETOPE.COD_TPO_OPE_AOS)
                          AND DETOPE.COD_SIS_SAL IS NULL
                          AND DETOPE.COD_BCO_ORG =
                                NVL (V_COD_BCO_ORG, DETOPE.COD_BCO_ORG)
                          AND DETOPE.COD_SIS_ENT = CE.COD_SIS_ENT_SAL(+)
                          AND DETOPE.COD_SIS_SAL = CS.COD_SIS_ENT_SAL(+)
                          AND DETOPE.FEC_VTA = TRUNC (SYSDATE)
                          AND DETOPE.COD_DVI = PKG_PAB_CONSTANTES.V_STR_DVI_CLP
               ORDER BY   NUM_FOL_OPE;
         ELSE
            IF (P_COD_SIS_SAL <> 'SINASIG' AND P_COD_SIS_ENT = 'SINASIG')
            THEN
               OPEN P_CURSOR FOR
                    SELECT   TO_CHAR (DETOPE.COD_MT_SWF) TIPO_MT,
                             DETOPE.NUM_REF_SWF REF_SWIFT,
                             DETOPE.NUM_REF_EXT REF_SWIFT_EXT,
                             DETOPE.IMP_OPE MONTO,
                             DETOPE.NUM_FOL_OPE NUMERO_OPE,
                             DETOPE.FEC_ING_OPE FECHA_OPE,
                             DETOPE.COD_TPO_OPE_AOS TIPO_OPE,
                             CE.DSC_SIS_ENT_SAL CANAL_ENT,
                             CS.DSC_SIS_ENT_SAL CANAL_SAL,
                             DETOPE.COD_BCO_ORG BIC_ORIGEN,
                             DETOPE.COD_BCO_DTN BIC_DESTINO
                      FROM   PABS_DT_DETLL_CARGO_ABONO_CAJA DETOPE,
                             PABS_DT_SISTM_ENTRD_SALID CE,
                             PABS_DT_SISTM_ENTRD_SALID CS
                     WHERE   DETOPE.COD_MT_SWF IN
                                   (PKG_PAB_CONSTANTES.V_COD_MT900,
                                    PKG_PAB_CONSTANTES.V_COD_MT910)
                             AND NVL (DETOPE.COD_SIS_SAL, '#') NOT IN
                                      (PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SAL_CTACTE) --SI ES NULO LO ELIMINA
                             AND DETOPE.COD_MT_SWF =
                                   NVL (V_COD_MT_SWF, DETOPE.COD_MT_SWF)
                             AND NVL (DETOPE.NUM_REF_SWF, '#') LIKE
                                   NVL (V_NUM_REF_SWF,
                                        NVL (DETOPE.NUM_REF_SWF, '#'))
                                   || '%'
                             AND DETOPE.COD_TPO_OPE_AOS =
                                   NVL (V_COD_TPO_OPE_AOS,
                                        DETOPE.COD_TPO_OPE_AOS)
                             AND DETOPE.COD_SIS_ENT IS NULL
                             AND DETOPE.COD_BCO_ORG =
                                   NVL (V_COD_BCO_ORG, DETOPE.COD_BCO_ORG)
                             AND DETOPE.COD_SIS_ENT = CE.COD_SIS_ENT_SAL(+)
                             AND DETOPE.COD_SIS_SAL = CS.COD_SIS_ENT_SAL(+)
                             AND DETOPE.FEC_VTA = TRUNC (SYSDATE)
                             AND DETOPE.COD_DVI =
                                   PKG_PAB_CONSTANTES.V_STR_DVI_CLP
                  ORDER BY   NUM_FOL_OPE;
            ELSE
               OPEN P_CURSOR FOR
                    SELECT   TO_CHAR (DETOPE.COD_MT_SWF) TIPO_MT,
                             DETOPE.NUM_REF_SWF REF_SWIFT,
                             DETOPE.NUM_REF_EXT REF_SWIFT_EXT,
                             DETOPE.IMP_OPE MONTO,
                             DETOPE.NUM_FOL_OPE NUMERO_OPE,
                             DETOPE.FEC_ING_OPE FECHA_OPE,
                             DETOPE.COD_TPO_OPE_AOS TIPO_OPE,
                             CE.DSC_SIS_ENT_SAL CANAL_ENT,
                             CS.DSC_SIS_ENT_SAL CANAL_SAL,
                             DETOPE.COD_BCO_ORG BIC_ORIGEN,
                             DETOPE.COD_BCO_DTN BIC_DESTINO
                      FROM   PABS_DT_DETLL_CARGO_ABONO_CAJA DETOPE,
                             PABS_DT_SISTM_ENTRD_SALID CE,
                             PABS_DT_SISTM_ENTRD_SALID CS
                     WHERE   DETOPE.COD_MT_SWF IN
                                   (PKG_PAB_CONSTANTES.V_COD_MT900,
                                    PKG_PAB_CONSTANTES.V_COD_MT910)
                             AND NVL (DETOPE.COD_SIS_SAL, '#') NOT IN
                                      (PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SAL_CTACTE) --SI ES NULO LO ELIMINA
                             AND DETOPE.COD_MT_SWF =
                                   NVL (V_COD_MT_SWF, DETOPE.COD_MT_SWF)
                             AND NVL (DETOPE.NUM_REF_SWF, '#') LIKE
                                   NVL (V_NUM_REF_SWF,
                                        NVL (DETOPE.NUM_REF_SWF, '#'))
                                   || '%'
                             AND DETOPE.COD_TPO_OPE_AOS =
                                   NVL (V_COD_TPO_OPE_AOS,
                                        DETOPE.COD_TPO_OPE_AOS)
                             AND NVL (DETOPE.COD_SIS_ENT, '#') =
                                   NVL (V_COD_SIS_ENT,
                                        NVL (DETOPE.COD_SIS_ENT, '#'))
                             AND NVL (DETOPE.COD_SIS_SAL, '#') =
                                   NVL (V_COD_SIS_SAL,
                                        NVL (DETOPE.COD_SIS_SAL, '#'))
                             AND DETOPE.COD_BCO_ORG =
                                   NVL (V_COD_BCO_ORG, DETOPE.COD_BCO_ORG)
                             AND DETOPE.COD_SIS_ENT = CE.COD_SIS_ENT_SAL(+)
                             AND DETOPE.COD_SIS_SAL = CS.COD_SIS_ENT_SAL(+)
                             AND DETOPE.FEC_VTA = TRUNC (SYSDATE)
                             AND DETOPE.COD_DVI =
                                   PKG_PAB_CONSTANTES.V_STR_DVI_CLP
                  ORDER BY   NUM_FOL_OPE;
            END IF;
         END IF;
      END IF;

      P_ERROR := PKG_PAB_CONSTANTES.V_OK;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         ERR_CODE := SQLCODE;
         ERR_MSG :=
            SUBSTR (SQLERRM, 1, 300) || 'NO SE ENCONTRO OPERACI�N FOLIO:';
         PKG_PAB_TRACKING.SP_PAB_INS_LOG_BD (ERR_CODE,
                                             V_NOM_PCK,
                                             ERR_MSG,
                                             V_NOM_SP);
         P_ERROR := PKG_PAB_CONSTANTES.V_ERROR;
      WHEN OTHERS
      THEN
         ERR_CODE := SQLCODE;
         ERR_MSG := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.SP_PAB_INS_LOG_BD (ERR_CODE,
                                             V_NOM_PCK,
                                             ERR_MSG,
                                             V_NOM_SP);
         P_ERROR := PKG_PAB_CONSTANTES.V_ERROR;
         P_S_MENSAJE := ERR_CODE || '-' || ERR_MSG;
         RAISE_APPLICATION_ERROR (-20000, P_S_MENSAJE);
   END SP_PAB_BUS_CAMBIAR_CANAL;


   /********************************************************************************************************
   FUNCION/PROCEDIMIENTO: SP_PAB_CMB_CNLENT_CMBCNLSAL
   OBJETIVO             : Combo Canal entrada de protocolos de la pantalla cambio canal de salida
   SISTEMA              : DBO_PAB.
   BASE DE DATOS        : DBO_PAB.
   TABLAS USADAS        : PABS_DT_SISTM_ENTRD_SALID
   FECHA                : 02/06/17
   AUTOR                : SANTANDER.
   INPUT                : N/A
   OUTPUT               : P_CURSOR := DATOS CURSOR.
   OBSERVACIONES
   FECHA       USUARIO    DETALLE
   ********************************************************************************************************/
   PROCEDURE SP_PAB_CMB_CNLENT_CMBCNLSAL (P_CURSOR OUT SYS_REFCURSOR)
   IS
      -------------------------------------DECLARACI�N DE VARIABLES Y CONSTANTES------------------------------
      V_NOM_SP   VARCHAR2 (30) := 'SP_PAB_COMBO_CANAL_ENT_SAL';
   --------------------------------------------------------------------------------------------------------
   BEGIN
      OPEN P_CURSOR FOR
         SELECT   COD_SIS_ENT_SAL, DSC_SIS_ENT_SAL
           FROM   PABS_DT_SISTM_ENTRD_SALID EN
          WHERE   (EN.FLG_SIS_ENT_SAL IN (6, 1)
                   OR EN.COD_SIS_ENT_SAL IN
                           (PKG_PAB_CONSTANTES.V_COD_TIP_ABOMAS,
                            PKG_PAB_CONSTANTES.V_COD_TIP_TESCEN,
                            PKG_PAB_CONSTANTES.V_COD_TIP_CANJE,
                            PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SE_OOFF,
                            PKG_PAB_CONSTANTES.V_COD_TIP_LBTRCOMB))
                  AND EN.COD_SIS_ENT_SAL NOT IN
                           (PKG_PAB_CONSTANTES.V_COD_TIP_ABO_CTA)
         UNION
         SELECT   'SINASIG' AS COD_SIS_ENT_SAL,
                  'Sin asignar' AS DSC_SIS_ENT_SAL
           FROM   DUAL
         ORDER BY   DSC_SIS_ENT_SAL;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         ERR_CODE := SQLCODE;
         ERR_MSG := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.SP_PAB_INS_LOG_BD (ERR_CODE,
                                             V_NOM_PCK,
                                             ERR_MSG,
                                             V_NOM_SP);
      WHEN OTHERS
      THEN
         ERR_CODE := SQLCODE;
         ERR_MSG := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.SP_PAB_INS_LOG_BD (ERR_CODE,
                                             V_NOM_PCK,
                                             ERR_MSG,
                                             V_NOM_SP);
         P_S_MENSAJE := ERR_CODE || '-' || ERR_MSG;
         RAISE_APPLICATION_ERROR (-20000, P_S_MENSAJE);
   END SP_PAB_CMB_CNLENT_CMBCNLSAL;

   /********************************************************************************************************
   FUNCION/PROCEDIMIENTO: SP_PAB_CMB_CNLSAL_CMBCNLSAL
   OBJETIVO             : Combo Canal salida de protocolos de la pantalla cambio canal de salida
   SISTEMA              : DBO_PAB.
   BASE DE DATOS        : DBO_PAB.
   TABLAS USADAS        : PABS_DT_SISTM_ENTRD_SALID
   FECHA                : 02/06/17
   AUTOR                : SANTANDER.
   INPUT                : N/A
   OUTPUT               : P_CURSOR := DATOS CURSOR.
   OBSERVACIONES
   FECHA       USUARIO    DETALLE
   ********************************************************************************************************/
   PROCEDURE SP_PAB_CMB_CNLSAL_CMBCNLSAL (P_CURSOR OUT SYS_REFCURSOR)
   IS
      -------------------------------------DECLARACI�N DE VARIABLES Y CONSTANTES------------------------------
      V_NOM_SP   VARCHAR2 (30) := 'SP_PAB_CMB_CNLSAL_CMBCNLSAL';
   --------------------------------------------------------------------------------------------------------
   BEGIN
      OPEN P_CURSOR FOR
         SELECT   COD_SIS_ENT_SAL, DSC_SIS_ENT_SAL
           FROM   PABS_DT_SISTM_ENTRD_SALID EN
          WHERE   (EN.FLG_SIS_ENT_SAL IN (6, 1)
                   OR EN.COD_SIS_ENT_SAL IN
                           (PKG_PAB_CONSTANTES.V_COD_TIP_ABOMAS,
                            PKG_PAB_CONSTANTES.V_COD_TIP_TESCEN,
                            PKG_PAB_CONSTANTES.V_COD_TIP_CANJE,
                            PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SE_OOFF,
                            PKG_PAB_CONSTANTES.V_COD_TIP_LBTRCOMB,
                            PKG_PAB_CONSTANTES.V_COD_TIP_HIPOTE))
                  AND EN.COD_SIS_ENT_SAL NOT IN
                           (PKG_PAB_CONSTANTES.V_COD_TIP_OPHIPO,
                            PKG_PAB_CONSTANTES.V_COD_TIP_SWF,
                            PKG_PAB_CONSTANTES.V_COD_TIP_ABO_CTA)
         UNION
         SELECT   'SINASIG' AS COD_SIS_ENT_SAL,
                  'Sin asignar' AS DSC_SIS_ENT_SAL
           FROM   DUAL
         ORDER BY   DSC_SIS_ENT_SAL;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         ERR_CODE := SQLCODE;
         ERR_MSG := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.SP_PAB_INS_LOG_BD (ERR_CODE,
                                             V_NOM_PCK,
                                             ERR_MSG,
                                             V_NOM_SP);
      WHEN OTHERS
      THEN
         ERR_CODE := SQLCODE;
         ERR_MSG := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.SP_PAB_INS_LOG_BD (ERR_CODE,
                                             V_NOM_PCK,
                                             ERR_MSG,
                                             V_NOM_SP);
         P_S_MENSAJE := ERR_CODE || '-' || ERR_MSG;
         RAISE_APPLICATION_ERROR (-20000, P_S_MENSAJE);
   END SP_PAB_CMB_CNLSAL_CMBCNLSAL;


   /********************************************************************************************************
   FUNCION/PROCEDIMIENTO: SP_PAB_CMB_CAMBIO_CANAL
   OBJETIVO             : Combo Canal salida de protocolos de la pantalla cambio canal de salida
   SISTEMA              : DBO_PAB.
   BASE DE DATOS        : DBO_PAB.
   TABLAS USADAS        : PABS_DT_SISTM_ENTRD_SALID
   FECHA                : 02/06/17
   AUTOR                : SANTANDER.
   INPUT                : N/A
   OUTPUT               : P_CURSOR := DATOS CURSOR.
   OBSERVACIONES
   FECHA       USUARIO    DETALLE
   ********************************************************************************************************/
   PROCEDURE SP_PAB_CMB_CAMBIO_CANAL (P_CURSOR OUT SYS_REFCURSOR)
   IS
      -------------------------------------DECLARACI�N DE VARIABLES Y CONSTANTES------------------------------
      V_NOM_SP   VARCHAR2 (30) := 'SP_PAB_CMB_CAMBIO_CANAL';
   --------------------------------------------------------------------------------------------------------
   BEGIN
      OPEN P_CURSOR FOR
           SELECT   COD_SIS_ENT_SAL CODIGO, DSC_SIS_ENT_SAL GLOSA
             FROM   PABS_DT_SISTM_ENTRD_SALID EN
            WHERE   EN.FLG_SIS_ENT_SAL IN (6)
                    OR EN.COD_SIS_ENT_SAL IN
                            (PKG_PAB_CONSTANTES.V_COD_TIP_ABOMAS,
                             PKG_PAB_CONSTANTES.V_COD_TIP_TESCEN,
                             PKG_PAB_CONSTANTES.V_COD_TIP_CANJE,
                             PKG_PAB_CONSTANTES.V_SRT_COD_SIS_SE_OOFF,
                             PKG_PAB_CONSTANTES.V_COD_TIP_LBTRCOMB,
                             PKG_PAB_CONSTANTES.V_COD_TIP_HIPOTE)
         ORDER BY   DSC_SIS_ENT_SAL;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         ERR_CODE := SQLCODE;
         ERR_MSG := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.SP_PAB_INS_LOG_BD (ERR_CODE,
                                             V_NOM_PCK,
                                             ERR_MSG,
                                             V_NOM_SP);
      WHEN OTHERS
      THEN
         ERR_CODE := SQLCODE;
         ERR_MSG := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.SP_PAB_INS_LOG_BD (ERR_CODE,
                                             V_NOM_PCK,
                                             ERR_MSG,
                                             V_NOM_SP);
         P_S_MENSAJE := ERR_CODE || '-' || ERR_MSG;
         RAISE_APPLICATION_ERROR (-20000, P_S_MENSAJE);
   END SP_PAB_CMB_CAMBIO_CANAL;

   /********************************************************************************************************
   FUNCION/PROCEDIMIENTO: PKG_PAB_PROTOCOLO.SP_PAB_COMBO_TODOS_PROT.
   OBJETIVO             : MUESTRA FILTRO EN
                           CONSULTAS -> SWIFT -> MENSAJES PROTOCOLO   COMBO: TIPO MT
   SISTEMA              : DBO_PAB.
   BASE DE DATOS        : DBO_PAB.
   TABLAS USADAS        : PABS_DT_TIPO_MT_SWIFT.
   FECHA                : 20/09/2018.
   AUTOR                : SANTANDER.
   INPUT                : NA.
   OUTPUT               : P_CURSOR := DATOS CURSOR.
   OBSERVACIONES
   FECHA       USUARIO    DETALLE
   20-09-2018  SANTANDER  CREACI�N.
   ********************************************************************************************************/
   PROCEDURE SP_PAB_COMBO_TODOS_PROT (P_CURSOR OUT SYS_REFCURSOR)
   IS
      ---------------------------------------DECLARACI�N DE VARIABLES Y CONSTANTES----------------------------
      V_NOM_SP   VARCHAR2 (30) := 'SP_PAB_COMBO_TODOS_PROT';
   --------------------------------------------------------------------------------------------------------
   BEGIN
      OPEN P_CURSOR FOR
           SELECT   DISTINCT (COD_MT_SWF) AS MT_SWT
             FROM   PABS_DT_TIPO_MT_SWIFT
            WHERE   FLG_VGN = PKG_PAB_CONSTANTES.V_REG_VIG
                    AND FLG_TPO_MT IN
                             (PKG_PAB_CONSTANTES.V_NUM_MT_PROTOCOLO,
                              PKG_PAB_CONSTANTES.V_NUM_MT_AVISOS_PRO)
         ORDER BY   COD_MT_SWF;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         ERR_CODE := SQLCODE;
         ERR_MSG :=
            'NO SE HAN ENCONTRADO REGISTOS ASOCIADOS A LOS SWIFT CON ESTADO VIGENCIA '
            || PKG_PAB_CONSTANTES.V_REG_VIG
            || ' Y MENSAJE NUMERO '
            || PKG_PAB_CONSTANTES.V_NUM_MT_AVISOS_PRO
            || ' '
            || PKG_PAB_CONSTANTES.V_NUM_MT_AVISOS_PRO;

         PKG_PAB_TRACKING.SP_PAB_INS_LOG_BD (ERR_CODE,
                                             V_NOM_PCK,
                                             ERR_MSG,
                                             V_NOM_SP);
      WHEN OTHERS
      THEN
         ERR_CODE := SQLCODE;
         ERR_MSG := SUBSTR (SQLERRM, 1, 300);
         PKG_PAB_TRACKING.SP_PAB_INS_LOG_BD (ERR_CODE,
                                             V_NOM_PCK,
                                             ERR_MSG,
                                             V_NOM_SP);
         P_S_MENSAJE := ERR_CODE || '-' || ERR_MSG;
         RAISE_APPLICATION_ERROR (-20000, P_S_MENSAJE);
   END SP_PAB_COMBO_TODOS_PROT;
END PKG_PAB_PROTOCOLO;
/


CREATE SYNONYM USR_PAB.PKG_PAB_PROTOCOLO FOR DBO_PAB.PKG_PAB_PROTOCOLO
/


GRANT EXECUTE ON DBO_PAB.PKG_PAB_PROTOCOLO TO R_APP_PAB
/

GRANT EXECUTE ON DBO_PAB.PKG_PAB_PROTOCOLO TO USR_PAB
/

