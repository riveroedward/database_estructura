CREATE TABLE DBO_PAB.PABS_DT_CBCRA_NOMNA_INTER
(
  NUM_FOL_NMN_ITR  NUMBER(12)                   NOT NULL,
  COD_SIS_ENT_ITR  CHAR(10 BYTE),
  COD_USR_ITR      CHAR(11 BYTE),
  COD_EST_AOS_ITR  NUMBER(5),
  FLG_DVI_ITR      NUMBER(1),
  IMP_TOT_NMN_ITR  NUMBER(18,2),
  FEC_ISR_NMN_ITR  DATE,
  HOR_ISR_NMN_ITR  NUMBER(4),
  FEC_ACT_NMN_ITR  DATE,
  NOM_NMN_ITR      VARCHAR2(200 BYTE),
  GLS_MTV_EST_ITR  VARCHAR2(300 BYTE)
)
TABLESPACE TSD_D_PAB
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING
/


CREATE INDEX DBO_PAB.IX_01_CBCRA_NOMNA_INTER ON DBO_PAB.PABS_DT_CBCRA_NOMNA_INTER
(NUM_FOL_NMN_ITR)
LOGGING
TABLESPACE TSI_D_PAB
NOPARALLEL
/


ALTER TABLE DBO_PAB.PABS_DT_CBCRA_NOMNA_INTER ADD (
  CONSTRAINT PK_DT_CBCRA_NOMNA_INTER
 PRIMARY KEY
 (NUM_FOL_NMN_ITR))
/


GRANT DELETE, INSERT, SELECT, UPDATE ON DBO_PAB.PABS_DT_CBCRA_NOMNA_INTER TO R_APP_PAB
/

