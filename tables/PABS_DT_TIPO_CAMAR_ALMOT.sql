CREATE TABLE DBO_PAB.PABS_DT_TIPO_CAMAR_ALMOT
(
  COD_TPO_CMA      CHAR(2 BYTE)                 NOT NULL,
  GLS_TPO_CMA      VARCHAR2(20 BYTE)            NOT NULL,
  FLG_VGN          NUMBER(1)                    NOT NULL,
  COD_USR_CRC      CHAR(8 BYTE)                 NOT NULL,
  FEC_CRC          DATE                         NOT NULL,
  COD_USR_ULT_MOD  CHAR(8 BYTE),
  FEC_ULT_MOD      DATE
)
TABLESPACE TSD_D_PAB
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING
/


CREATE INDEX DBO_PAB.IX_01_TIPO_CAMAR_ALMOT ON DBO_PAB.PABS_DT_TIPO_CAMAR_ALMOT
(COD_TPO_CMA)
LOGGING
TABLESPACE TSI_D_PAB
NOPARALLEL
/


CREATE SYNONYM USR_PAB.PABS_DT_TIPO_CAMAR_ALMOT FOR DBO_PAB.PABS_DT_TIPO_CAMAR_ALMOT
/


ALTER TABLE DBO_PAB.PABS_DT_TIPO_CAMAR_ALMOT ADD (
  PRIMARY KEY
 (COD_TPO_CMA))
/


GRANT DELETE, INSERT, SELECT, UPDATE ON DBO_PAB.PABS_DT_TIPO_CAMAR_ALMOT TO R_APP_PAB
/

