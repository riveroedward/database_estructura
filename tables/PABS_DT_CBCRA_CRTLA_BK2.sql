CREATE TABLE DBO_PAB.PABS_DT_CBCRA_CRTLA_BK2
(
  FEC_ING_CTL      DATE                         NOT NULL,
  HOR_ING_CTL      NUMBER(4)                    NOT NULL,
  NUM_FOL_CTL      NUMBER(12)                   NOT NULL,
  NUM_REF_SWF_CTL  CHAR(16 BYTE)                NOT NULL,
  COD_MT_SWF       NUMBER(3)                    NOT NULL,
  COD_DVI          CHAR(3 BYTE)                 NOT NULL,
  NUM_CTA_CTE      VARCHAR2(35 BYTE)            NOT NULL,
  COD_BCO_ORG      CHAR(11 BYTE)                NOT NULL,
  FLG_DEB_CRE_INI  NUMBER(1)                    NOT NULL,
  IMP_ING_CTL      NUMBER(15,2)                 NOT NULL,
  IMP_EGR_CTL      NUMBER(15,2)                 NOT NULL,
  IMP_INI_CTL      NUMBER(15,2)                 NOT NULL,
  IMP_FNL_CTL      NUMBER(15,2)                 NOT NULL,
  FLG_DEB_CRE_FNL  NUMBER(1)                    NOT NULL,
  FLG_ING_EGR      NUMBER(1)
)
TABLESPACE TSD_D_PAB
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING
/


