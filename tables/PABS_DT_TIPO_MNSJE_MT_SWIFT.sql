CREATE TABLE DBO_PAB.PABS_DT_TIPO_MNSJE_MT_SWIFT
(
  COD_MT_SWF   NUMBER(3)                        NOT NULL,
  COD_TPO_OPE  CHAR(8 BYTE)                     NOT NULL,
  GLS_MSJ_OPE  VARCHAR2(300 BYTE)
)
TABLESPACE TSD_D_PAB
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING
/


CREATE INDEX DBO_PAB.IX_01_DT_TIPO_MNSJE_MT_SWIFT ON DBO_PAB.PABS_DT_TIPO_MNSJE_MT_SWIFT
(COD_MT_SWF, COD_TPO_OPE)
LOGGING
TABLESPACE TSI_D_PAB
NOPARALLEL
/


CREATE SYNONYM USR_PAB.PABS_DT_TIPO_MNSJE_MT_SWIFT FOR DBO_PAB.PABS_DT_TIPO_MNSJE_MT_SWIFT
/


ALTER TABLE DBO_PAB.PABS_DT_TIPO_MNSJE_MT_SWIFT ADD (
  PRIMARY KEY
 (COD_MT_SWF, COD_TPO_OPE))
/


GRANT DELETE, INSERT, SELECT, UPDATE ON DBO_PAB.PABS_DT_TIPO_MNSJE_MT_SWIFT TO R_APP_PAB
/

