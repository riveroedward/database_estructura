CREATE TABLE DBO_PAB.PABS_DT_ENCAB_CRTLA_FLIAL_BK1
(
  NUM_DOC_FIL  CHAR(11 BYTE)                    NOT NULL,
  COD_TPO_FIL  CHAR(1 BYTE)                     NOT NULL,
  NUM_CTA_CTE  VARCHAR2(25 BYTE)                NOT NULL,
  FEC_CTL_CTA  TIMESTAMP(6)                     NOT NULL,
  NUM_EPS_FIL  CHAR(4 BYTE)                     NOT NULL,
  GLS_TPO_CTL  CHAR(12 BYTE)                    NOT NULL,
  FEC_INI_CTL  DATE                             NOT NULL,
  FEC_FNL_CTL  DATE                             NOT NULL,
  NUM_CTL_FIL  NUMBER(12)                       NOT NULL,
  NUM_MVT_CTL  NUMBER(12)                       NOT NULL,
  NUM_LCD_CTA  CHAR(12 BYTE)                    NOT NULL,
  IMP_INI_CTL  NUMBER(15,2)                     NOT NULL,
  IMP_TOT_ING  NUMBER(15,2)                     NOT NULL,
  IMP_TOT_EGR  NUMBER(15,2)                     NOT NULL,
  IMP_FNL_CTL  NUMBER(15,2)                     NOT NULL,
  IMP_RTN_CTL  NUMBER(15,2)                     NOT NULL,
  IMP_LCD_CTL  NUMBER(15,2)                     NOT NULL,
  IMP_LCD_USO  NUMBER(15,2)                     NOT NULL,
  IMP_LCD_DIP  NUMBER(15,2)                     NOT NULL,
  IMP_TOT_DIP  NUMBER(15,2)                     NOT NULL,
  IMP_ING_DIA  NUMBER(15,2)                     NOT NULL,
  IMP_EGR_DIA  NUMBER(15,2)                     NOT NULL,
  IMP_TOT_DIA  NUMBER(15,2)                     NOT NULL,
  IMP_ING_24   NUMBER(15,2)                     NOT NULL,
  IMP_EGR_24   NUMBER(15,2)                     NOT NULL,
  IMP_TOT_24   NUMBER(15,2)                     NOT NULL,
  IMP_ING_48   NUMBER(15,2)                     NOT NULL,
  IMP_EGR_48   NUMBER(15,2)                     NOT NULL,
  IMP_TOT_48   NUMBER(15,2)                     NOT NULL,
  NUM_SUC_ORG  CHAR(4 BYTE)                     NOT NULL
)
TABLESPACE TSD_D_PAB
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING
/


