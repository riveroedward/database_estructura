CREATE TABLE DBO_PAB.PABS_DT_BTCOR_DETLL_PRCSO
(
  FEC_ICO_PCS      TIMESTAMP(6)                 NOT NULL,
  FEC_ISR_DET_BTC  TIMESTAMP(6)                 NOT NULL,
  COD_TPO_PSC      CHAR(3 BYTE)                 NOT NULL,
  COD_ERR_BIB      CHAR(6 BYTE),
  NUM_FOL_OPE      NUMBER(12),
  GLS_DET_PCS      VARCHAR2(4000 BYTE)
)
TABLESPACE TSD_D_PAB
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING
/


CREATE INDEX DBO_PAB.IX_01_BTCOR_DETLL_PRCSO ON DBO_PAB.PABS_DT_BTCOR_DETLL_PRCSO
(FEC_ICO_PCS, FEC_ISR_DET_BTC, COD_TPO_PSC)
LOGGING
TABLESPACE TSI_D_PAB
NOPARALLEL
/


CREATE SYNONYM USR_PAB.PABS_DT_BTCOR_DETLL_PRCSO FOR DBO_PAB.PABS_DT_BTCOR_DETLL_PRCSO
/


ALTER TABLE DBO_PAB.PABS_DT_BTCOR_DETLL_PRCSO ADD (
  PRIMARY KEY
 (FEC_ICO_PCS, FEC_ISR_DET_BTC, COD_TPO_PSC))
/


GRANT DELETE, INSERT, SELECT, UPDATE ON DBO_PAB.PABS_DT_BTCOR_DETLL_PRCSO TO R_APP_PAB
/

