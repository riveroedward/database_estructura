CREATE TABLE DBO_PAB.PABS_DT_DETLL_OPRCN_PLAZO
(
  NUM_OPE_PZO      VARCHAR2(20 BYTE)            NOT NULL,
  NUM_OPE_OFB      VARCHAR2(20 BYTE)            NOT NULL,
  COD_DVI          CHAR(3 BYTE),
  IMP_MTO          NUMBER(18,2),
  FEC_ING_OPE_PZO  DATE,
  NUM_CLV_NGC      CHAR(8 BYTE)
)
TABLESPACE TSD_D_PAB
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING
/


CREATE INDEX DBO_PAB.IX_01_DETLL_OPRCN_PLAZO ON DBO_PAB.PABS_DT_DETLL_OPRCN_PLAZO
(NUM_OPE_PZO, NUM_OPE_OFB)
LOGGING
TABLESPACE TSI_D_PAB
NOPARALLEL
/


ALTER TABLE DBO_PAB.PABS_DT_DETLL_OPRCN_PLAZO ADD (
  PRIMARY KEY
 (NUM_OPE_PZO, NUM_OPE_OFB))
/


GRANT DELETE, INSERT, SELECT, UPDATE ON DBO_PAB.PABS_DT_DETLL_OPRCN_PLAZO TO R_APP_PAB
/

