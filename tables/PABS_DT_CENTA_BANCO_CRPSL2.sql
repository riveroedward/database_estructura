CREATE TABLE DBO_PAB.PABS_DT_CENTA_BANCO_CRPSL2
(
  NUM_CTA_COR      VARCHAR2(30 BYTE)            NOT NULL,
  COD_BIC_BCO      CHAR(11 BYTE)                NOT NULL,
  COD_DVI          CHAR(3 BYTE)                 NOT NULL,
  IMP_TOT_LCD      NUMBER(17,2)                 NOT NULL,
  IMP_SDO_CTA_COR  NUMBER(17,2)                 NOT NULL,
  FLG_VGN          NUMBER(1)                    NOT NULL,
  COD_USR_CRC      CHAR(11 BYTE)                NOT NULL,
  FEC_CRC          DATE                         NOT NULL,
  COD_USR_ULT_MOD  CHAR(11 BYTE),
  FEC_ULT_MOD      DATE,
  COD_MT_SWF       NUMBER(3),
  NUM_PDI          CHAR(3 BYTE),
  POR_INT_LNA_CDT  NUMBER(5,2),
  FLG_CJA          NUMBER(1)                    NOT NULL
)
TABLESPACE TSD_D_PAB
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING
/


