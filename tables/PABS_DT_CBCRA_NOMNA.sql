CREATE TABLE DBO_PAB.PABS_DT_CBCRA_NOMNA
(
  NUM_FOL_NMN  NUMBER(12)                       NOT NULL,
  NOM_NMN      VARCHAR2(200 BYTE)               NOT NULL,
  COD_SIS_ENT  CHAR(10 BYTE)                    NOT NULL,
  COD_USR      CHAR(11 BYTE),
  COD_EST_AOS  NUMBER(5),
  GLS_MTV_EST  VARCHAR2(300 BYTE),
  FLG_DVI      NUMBER(1)                        NOT NULL,
  IMP_TOT_NMN  NUMBER(18,2),
  FEC_ISR_NMN  DATE                             NOT NULL,
  HOR_ISR_NMN  NUMBER(4)                        NOT NULL,
  FEC_ACT_OPE  DATE
)
TABLESPACE TSD_D_PAB
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING
/


CREATE INDEX DBO_PAB.IX_01_CBCRA_NOMNA ON DBO_PAB.PABS_DT_CBCRA_NOMNA
(NUM_FOL_NMN)
LOGGING
TABLESPACE TSI_D_PAB
NOPARALLEL
/


CREATE INDEX DBO_PAB.IX_02_CBC_FEC_ISR_NMN ON DBO_PAB.PABS_DT_CBCRA_NOMNA
(FEC_ISR_NMN)
LOGGING
TABLESPACE TSI_D_PAB
NOPARALLEL
/


CREATE INDEX DBO_PAB.IX_02_CBCRA_NOMNA ON DBO_PAB.PABS_DT_CBCRA_NOMNA
(COD_EST_AOS)
LOGGING
TABLESPACE TSI_D_PAB
NOPARALLEL
/


CREATE SYNONYM USR_PAB.PABS_DT_CBCRA_NOMNA FOR DBO_PAB.PABS_DT_CBCRA_NOMNA
/


ALTER TABLE DBO_PAB.PABS_DT_CBCRA_NOMNA ADD (
  PRIMARY KEY
 (NUM_FOL_NMN))
/


GRANT DELETE, INSERT, SELECT, UPDATE ON DBO_PAB.PABS_DT_CBCRA_NOMNA TO R_APP_PAB
/

