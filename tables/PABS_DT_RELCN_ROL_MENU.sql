CREATE TABLE DBO_PAB.PABS_DT_RELCN_ROL_MENU
(
  COD_TPO_ROL_AOS  NUMBER(2)                    NOT NULL,
  COD_SIS_ENT      CHAR(10 BYTE)                NOT NULL,
  COD_MNS_AOS      NUMBER(3)                    NOT NULL,
  FLG_VGN          NUMBER(1)                    NOT NULL,
  COD_USR_CRC      CHAR(8 BYTE)                 NOT NULL,
  FEC_CRC          DATE                         NOT NULL,
  COD_USR_ULT_MOD  CHAR(8 BYTE),
  FEC_ULT_MOD      DATE
)
TABLESPACE TSD_D_PAB
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING
/


CREATE INDEX DBO_PAB.IX_01_RELCN_ROL_MENU ON DBO_PAB.PABS_DT_RELCN_ROL_MENU
(COD_TPO_ROL_AOS, COD_SIS_ENT, COD_MNS_AOS)
LOGGING
TABLESPACE TSI_D_PAB
NOPARALLEL
/


CREATE SYNONYM USR_PAB.PABS_DT_RELCN_ROL_MENU FOR DBO_PAB.PABS_DT_RELCN_ROL_MENU
/


ALTER TABLE DBO_PAB.PABS_DT_RELCN_ROL_MENU ADD (
  PRIMARY KEY
 (COD_TPO_ROL_AOS, COD_SIS_ENT, COD_MNS_AOS))
/


GRANT DELETE, INSERT, SELECT, UPDATE ON DBO_PAB.PABS_DT_RELCN_ROL_MENU TO R_APP_PAB
/

