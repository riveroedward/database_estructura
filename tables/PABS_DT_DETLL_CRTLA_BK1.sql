CREATE TABLE DBO_PAB.PABS_DT_DETLL_CRTLA_BK1
(
  FEC_ING_CTL      DATE                         NOT NULL,
  HOR_ING_CTL      NUMBER(4)                    NOT NULL,
  NUM_FOL_CTL      NUMBER(12)                   NOT NULL,
  NUM_FOL_OPE      NUMBER(12)                   NOT NULL,
  FEC_TRN_OPE      TIMESTAMP(6)                 NOT NULL,
  NUM_REF_SWF_OPE  CHAR(16 BYTE)                NOT NULL,
  COD_TRN_OPE      CHAR(8 BYTE)                 NOT NULL,
  NUM_REF_ITN_CTA  CHAR(16 BYTE)                NOT NULL,
  FLG_DEB_CRE      NUMBER(1)                    NOT NULL,
  IMP_TRN          NUMBER(15,2)                 NOT NULL,
  GLS_TRN          VARCHAR2(100 BYTE)
)
TABLESPACE TSD_D_PAB
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING
/


