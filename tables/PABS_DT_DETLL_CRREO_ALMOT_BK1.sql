CREATE TABLE DBO_PAB.PABS_DT_DETLL_CRREO_ALMOT_BK1
(
  COD_TPO_CRO  CHAR(5 BYTE)                     NOT NULL,
  GLS_TPO_CRO  VARCHAR2(20 BYTE)                NOT NULL,
  GLS_MSJ_CRO  VARCHAR2(300 BYTE)               NOT NULL,
  GLS_ENC_CRO  VARCHAR2(50 BYTE)                NOT NULL,
  FLG_ADJ_CRO  NUMBER(1)                        NOT NULL,
  GLS_NOT_USR  VARCHAR2(300 BYTE),
  GLS_CRO_NTF  VARCHAR2(100 BYTE),
  FLG_VGN_NTF  NUMBER(1)
)
TABLESPACE TSD_D_PAB
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING
/


