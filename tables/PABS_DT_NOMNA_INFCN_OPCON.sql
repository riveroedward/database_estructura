CREATE TABLE DBO_PAB.PABS_DT_NOMNA_INFCN_OPCON
(
  FEC_ISR_OPE       DATE                        NOT NULL,
  NUM_FOL_OPE       NUMBER(12)                  NOT NULL,
  OBS_OPC_SWF       VARCHAR2(200 BYTE),
  COD_BCO_BFC       CHAR(11 BYTE),
  COD_BCO_ITD       CHAR(11 BYTE),
  NUM_DOC_BFC       CHAR(11 BYTE),
  COD_TPD_BFC       CHAR(1 BYTE),
  NUM_CTA_DCV_BFC   VARCHAR2(30 BYTE),
  GLS_DIR_BFC       VARCHAR2(60 BYTE),
  NOM_CDD_BFC       VARCHAR2(50 BYTE),
  COD_PAS_BFC       CHAR(2 BYTE),
  NOM_ODN           VARCHAR2(100 BYTE),
  NUM_DOC_ODN       CHAR(11 BYTE),
  COD_TPD_ODN       CHAR(1 BYTE),
  NUM_CTA_ODN       VARCHAR2(30 BYTE),
  NUM_CTA_DCV_ODN   VARCHAR2(30 BYTE),
  GLS_DIR_ODN       VARCHAR2(60 BYTE),
  NOM_CDD_ODN       VARCHAR2(50 BYTE),
  COD_PAS_ODN       CHAR(2 BYTE),
  NUM_CLV_NGC       VARCHAR2(20 BYTE),
  NUM_AGT           CHAR(3 BYTE),
  COD_FND_CCLV      CHAR(1 BYTE),
  COD_TPO_SDO       CHAR(2 BYTE),
  COD_TPO_CMA       CHAR(2 BYTE),
  COD_TPO_FND       CHAR(3 BYTE),
  FEC_TPO_OPE_CCLV  DATE,
  NUM_CLV_IDF       VARCHAR2(20 BYTE),
  GLS_EST_RCH       VARCHAR2(300 BYTE),
  GLS_MTV_VSD       VARCHAR2(50 BYTE)
)
TABLESPACE TSD_D_PAB
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING
/


CREATE INDEX DBO_PAB.IX_01_NOMNA_INFCN_OPCON ON DBO_PAB.PABS_DT_NOMNA_INFCN_OPCON
(FEC_ISR_OPE, NUM_FOL_OPE)
LOGGING
TABLESPACE TSI_D_PAB
NOPARALLEL
/


CREATE SYNONYM USR_PAB.PABS_DT_NOMNA_INFCN_OPCON FOR DBO_PAB.PABS_DT_NOMNA_INFCN_OPCON
/


ALTER TABLE DBO_PAB.PABS_DT_NOMNA_INFCN_OPCON ADD (
  PRIMARY KEY
 (FEC_ISR_OPE, NUM_FOL_OPE))
/


GRANT DELETE, INSERT, SELECT, UPDATE ON DBO_PAB.PABS_DT_NOMNA_INFCN_OPCON TO R_APP_PAB
/

