CREATE TABLE DBO_PAB.PABS_DT_SALDO_HORA_ITDIA
(
  FEC_REG_SDO         DATE                      NOT NULL,
  HOR_REG_SDO         NUMBER(4)                 NOT NULL,
  COD_DVI             CHAR(3 BYTE)              NOT NULL,
  IMP_NET_ING         NUMBER(18,2)              NOT NULL,
  IMP_NET_EGR         NUMBER(18,2)              NOT NULL,
  IMP_SDO_RAL_AOS     NUMBER(18,2)              NOT NULL,
  IMP_NET_RAL         NUMBER(18,2)              NOT NULL,
  IMP_NET_ING_CPR     NUMBER(18,2)              NOT NULL,
  IMP_NET_EGR_CPR     NUMBER(18,2)              NOT NULL,
  IMP_NET_PYN         NUMBER(18,2)              NOT NULL,
  IMP_TOT_PYN_24_HOR  NUMBER(18,2),
  IMP_TOT_PYN_48_HOR  NUMBER(18,2)
)
TABLESPACE TSD_D_PAB
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING
/


CREATE INDEX DBO_PAB.XIE1PABS_DT_SALDO_HORA_ITDIA ON DBO_PAB.PABS_DT_SALDO_HORA_ITDIA
(FEC_REG_SDO, HOR_REG_SDO, COD_DVI)
LOGGING
TABLESPACE TSD_D_PAB
NOPARALLEL
/


ALTER TABLE DBO_PAB.PABS_DT_SALDO_HORA_ITDIA ADD (
  CONSTRAINT PK_DT_SALDO_HORA_ITDIA
 PRIMARY KEY
 (FEC_REG_SDO, HOR_REG_SDO, COD_DVI))
/


GRANT DELETE, INSERT, SELECT, UPDATE ON DBO_PAB.PABS_DT_SALDO_HORA_ITDIA TO R_APP_PAB
/

