CREATE TABLE DBO_PAB.PABS_DT_DETLL_OPRCN_HTRCA2
(
  FEC_ISR_OPE      DATE                         NOT NULL,
  NUM_FOL_OPE      NUMBER(12)                   NOT NULL,
  FEC_CGA_HIS      DATE                         NOT NULL,
  NOM_BFC          VARCHAR2(100 BYTE)           NOT NULL,
  NUM_FOL_NMN      NUMBER(12)                   NOT NULL,
  NUM_FOL_OPE_ORG  NUMBER(12)                   NOT NULL,
  NUM_OPE_SIS_ENT  CHAR(15 BYTE)                NOT NULL,
  FLG_EGR_ING      NUMBER(1)                    NOT NULL,
  COD_SIS_SAL      CHAR(10 BYTE),
  COD_SIS_ENT      CHAR(10 BYTE),
  COD_EST_AOS      NUMBER(5)                    NOT NULL,
  COD_TPO_ING      NUMBER(2)                    NOT NULL,
  FEC_ENV_RCP_SWF  DATE,
  HOR_ENV_RCP_SWF  NUMBER(4),
  COD_MT_SWF       NUMBER(3)                    NOT NULL,
  FLG_MRD_UTZ_SWF  NUMBER(1)                    NOT NULL,
  COD_TPO_OPE_AOS  CHAR(8 BYTE)                 NOT NULL,
  FEC_VTA          DATE                         NOT NULL,
  COD_DVI          CHAR(3 BYTE)                 NOT NULL,
  IMP_OPE          NUMBER(15,2)                 NOT NULL,
  COD_BCO_DTN      CHAR(11 BYTE)                NOT NULL,
  NUM_DOC_BFC      CHAR(11 BYTE),
  COD_TPD_BFC      CHAR(1 BYTE),
  NUM_CTA_BFC      VARCHAR2(30 BYTE),
  COD_SUC          VARCHAR2(4 BYTE),
  NUM_REF_SWF      CHAR(16 BYTE),
  NUM_REF_EXT      CHAR(16 BYTE),
  GLS_ADC_EST      VARCHAR2(210 BYTE),
  NUM_REF_CTB      CHAR(12 BYTE),
  COD_BCO_ORG      CHAR(11 BYTE),
  FEC_ACT_OPE      DATE,
  NUM_IDF_GTR_DOC  VARCHAR2(40 BYTE),
  NUM_MVT_CTB      CHAR(12 BYTE),
  FEC_CFM_SWF      DATE,
  HOR_CFM_SWF      NUMBER(4),
  FEC_AZA_SWF      DATE,
  HOR_AZA_SWF      NUMBER(4)
)
TABLESPACE TSD_D_PAB
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING
/


