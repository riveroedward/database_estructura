CREATE TABLE DBO_PAB.PABS_DT_CAJA_FLIAL
(
  NUM_DOC_FIL  CHAR(11 BYTE)                    NOT NULL,
  COD_TPO_FIL  CHAR(1 BYTE)                     NOT NULL,
  NUM_CTA_CTE  VARCHAR2(25 BYTE)                NOT NULL,
  NUM_EPS_FIL  CHAR(4 BYTE)                     NOT NULL,
  GLS_EPS_FIL  VARCHAR2(70 BYTE)                NOT NULL,
  COD_DVI      CHAR(3 BYTE),
  NUM_LCD_CTA  VARCHAR2(25 BYTE),
  COD_BIC_BCO  CHAR(11 BYTE)                    NOT NULL,
  GLS_CTA_CTE  VARCHAR2(50 BYTE),
  NUM_SUC_ORI  CHAR(4 BYTE)                     NOT NULL
)
TABLESPACE TSD_D_PAB
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING
/


CREATE INDEX DBO_PAB.IX_01_CAJA_FLIAL ON DBO_PAB.PABS_DT_CAJA_FLIAL
(NUM_DOC_FIL, NUM_CTA_CTE)
LOGGING
TABLESPACE TSI_D_PAB
NOPARALLEL
/


ALTER TABLE DBO_PAB.PABS_DT_CAJA_FLIAL ADD (
  PRIMARY KEY
 (NUM_DOC_FIL, NUM_CTA_CTE))
/


GRANT DELETE, INSERT, SELECT, UPDATE ON DBO_PAB.PABS_DT_CAJA_FLIAL TO R_APP_PAB
/

